sap.ui.define([
	"valvoline/dash/DashWCMT/controller/Base.controller"
], function(Base) {
	"use strict";

	return Base.extend("valvoline.dash.DashWCMT.controller.Home", {
		/** @module Home */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function() {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("home").attachPatternMatched(this._onObjectMatched, this);
		},
		/**
		 * Check which pages the user is authorized to access
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function() {
			this.getRouter().navTo("cardManager");
		}
	});
});