sap.ui.define([
	"sap/m/MessageBox",
	"valvoline/dash/DashWCMT/controller/Base.controller"
], function (MessageBox, Base) {
	"use strict";
	var _oRouter, i18n, res, action = "";
	var fld1 = 0,
		fld2 = 0,
		fld3 = 0,
		fld4 = 0,
		fld5 = 0,
		fld6 = 0,
		fld7 = 0,
		fld8 = 0,
		fld9 = 0,
		fld10 = 0,
		fld11 = 0,
		fld12 = 0,
		fld13 = 0,
		fld14 = 0,
		fld15 = 0,
		fld15_1 = 0,
		fld15_2 = 0,
		fld15_3 = 0,
		blnFlag = false;
	var _oController, defaultVal;

	return Base.extend("valvoline.dash.DashWCMT.controller.HeroCardForm", {
		/** @module HeroCardForm */
		/**
		 * Initializes the controller.
		 * Initializes the global variables.
		 * Attaches an event to everytime someone navs to the hero card form page.
		 * Initializes local models, and gets core models and associates them to the view.
		 * Configures date pickers to not allow users to choose dates from the past.
		 * @function onInit
		 */
		onInit: function () {
			_oController = this;
			_oRouter = this.getRouter();
			_oRouter.getRoute("herocardform").attachPatternMatched(this._onObjectMatched, this);

			// Load i18n model
			i18n = _oController.getOwnerComponent().getModel("i18n").getResourceBundle();

		},

		/**
		 * Is fired everytime a user navs to Hero Card Form page.
		 * Checks if the user is authorized to access this page.
		 * Checks if it is supposed to be an editing form or a creation form and makes the neccessary adjustments.
		 * @param {Object} oEvent - the event that fired the function	 
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function (oEvent) {
			this.checkAuthorization();

			// Check if the local model was loaded
			if (sap.ui.getCore().getModel("local") === undefined) {
				this.getRouter().navTo("cardManager");
				return;
			}
			var oLocal = new sap.ui.model.json.JSONModel();
			this.getView().setModel(oLocal, "local");

			//Get Domain model
			var domain = sap.ui.getCore().getModel("domain");
			this.getView().setModel(domain, "domain");
			//End of Get Domain

			//Get CardHeader model
			var CardHeader = sap.ui.getCore().getModel("CardHeader");
			this.getView().setModel(CardHeader, "CardHeader");
			//End of Get CardHeader model

			// Configure Date Picker
			this.configureDatePickers(_oController);

			var userModel = sap.ui.getCore().getModel("userapi");
			this.getView().setModel(userModel, "userapi");
			var id = oEvent.getParameter("arguments").id;
			action = oEvent.getParameter("arguments").actionId;
			var Item = sap.ui.getCore().getModel("local").getProperty("/ContentCardItem/" + oEvent.getParameter("arguments").id);
			res = id;
			if (id !== "New") {
				if (action && action === "Copy") {
					this.getView().byId('saveButton').setText("Create");
				} else {
					this.getView().byId('saveButton').setText("Update");
				}
				this.getView().byId("saveButton").setEnabled(false);
				if (Item === undefined) {
					Item = sap.ui.getCore().getModel("local").getProperty("/ContentCardItem/" + res);
				}
			} else {
				this.getView().byId('saveButton').setText("Create");
				this.getView().byId("saveButton").setEnabled(false);
			}

			//remove feed, insights and products from possible locations
			var oFilters = new sap.ui.model.Filter({
				filters: [
					new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.NE, '1'),
					new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.NE, '2'),
					new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.NE, '3'),
					new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.NE, '9'),
					new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.NE, '10'),
					new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.NE, '11')
				],
				and: true
			});
			this.getView().byId("idLocation").getBinding("items").filter(oFilters);

			this.getView().getModel("local").setProperty("/cardCategory", null);
			this.getView().getModel("local").setProperty("/cardType", "500");

			if (Item !== undefined) {
				if (action && action === "Copy") {
					var oItemCopy = {};
					for (var key in Item) {
						oItemCopy[key] = Item[key];
					}
					oItemCopy.name = "";
					oItemCopy.id = "";
					this.getView().getModel("local").setData(oItemCopy);
					fld8 = 0;
				} else {
					this.getView().getModel("local").setData(Item);
					fld8 = 1;
				}
				fld1 = 1;
				fld2 = 1;
				fld3 = 1;
				fld4 = 1;
				fld5 = 1;
				fld6 = 1;
				fld7 = 1;
				fld9 = 1;
				fld10 = 1;
				fld11 = 1;
				fld12 = 1;
				fld13 = 1;
				fld14 = 1;
				fld15 = 1;
				fld15_1 = 1;
				fld15_2 = 1;
				fld15_3 = 1;
				this.fnDisplay(Item);
				this._processStatus(this);
				blnFlag = true;
			} else {
				this.getView().byId('CardTitle').setText(i18n.getText("CASECText3TXT"));
				this.getView().byId('CardSubTitle').setText(i18n.getText("GENCAText1TXT"));
				this.getView().byId('CardDesc').setText(i18n.getText("FEEDCText1TXT"));
				this.getView().byId('CardLink').setText(i18n.getText("HEROCText1LK"));
				this.getView().byId("idDispEDate").setValueState(sap.ui.core.ValueState.None);

				defaultVal = "../webapp/resources/img/HeroCardTemplate.png";
				$('.heroCardTemplate').css({
					'background-image': 'url("' + defaultVal + '")'
				});
				fld1 = 0;
				fld2 = 0;
				fld3 = 0;
				fld4 = 0;
				fld5 = 0;
				fld6 = 0;
				fld7 = 0;
				fld8 = 0;
				fld9 = 0;
				fld10 = 0;
				fld11 = 0;
				fld12 = 0;
				fld13 = 0;
				fld14 = 0;
				fld15 = 0;
				fld15_1 = 0;
				fld15_2 = 0;
				fld15_3 = 0;
				
				this.getView().byId("idDispSDate").setValueState(sap.ui.core.ValueState.None);

				//set ExpressCare checkBoxes state
				_oController.getView().byId("idExpressCare").setSelected(false);
				_oController.getView().byId("idExpressCareBranded").setSelected(true).setVisible(false);
				_oController.getView().byId("idExpressCareNonBranded").setSelected(true).setVisible(false);
			}
			//Get Segments and set model to the view
			this.getSegments(this, id);
		},
		/**
		 * Sets the background image of the hero card on the view
		 * @function onAfterRendering
		 */
		onAfterRendering: function () {
			if (blnFlag) {
				$('.heroCardTemplate').css({
					'background-image': 'url("' + defaultVal + '")'
				});
			}
		},

		/**
		 * Convenience method for displaying the info of the template objects.
		 * @param {Object} item - the item to be displayed
		 * @function fnDisplay
		 */
		fnDisplay: function (item) {
			this.getView().byId('CardTitle').setText(item.title);
			this.getView().byId('CardSubTitle').setText(item.subtitle);
			this.getView().byId('CardDesc').setText(item.shortText);
			this.getView().byId('CardLink').setText(item.callToActionLabel);
			defaultVal = item.mediaURL;
			$('.heroCardTemplate').css({
				'background-image': 'url("' + defaultVal + '")'
			});
		},

		/**
		 * 1st Form
		 * Handle Live change Name
		 * @parameter (oEvent)
		 */
		handleLiveChangeName: function (oEvent) {
			var newValue = oEvent.getParameter("value");
			newValue = newValue.trim();
			if (newValue === "") {
				fld8 = 0;
			} else {
				fld8 = 1;
			}
			this.onConfigureSaveButton();
		},

		handleLiveChangeCustomerOverride: function (oEvent) {
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Location / Category
		 * @(parameter) (oEvent)
		 */
		handleLiveChangeLocation: function (oEvent) {
			fld11 = this.handleLocationChange(oEvent, this);
			switch (fld11) {
			case 0:
				fld11 = 0;
				break;
			case 1:
				fld11 = 1;
				fld15_1 = 1;
				fld15_2 = 0;
				fld15_3 = 0;
				this.byId("idExpressCare").setEnabled(true);
				this.byId("idIgnoreIndustry").setEnabled(true);
				this.byId("idSegment").setEnabled(true);
				break;
			case "10000":
				//Main Feed Exception
				fld11 = 1;
				fld15_1 = 1;
				this.byId("idExpressCare").setEnabled(false);
				this.byId("idExpressCare").setSelected(false);
				_oController.byId("idExpressCareBranded").setSelected(false).setVisible(false);
				_oController.byId("idExpressCareNonBranded").setSelected(false).setVisible(false);
				this.byId("idIgnoreIndustry").setEnabled(false);
				this.byId("idIgnoreIndustry").setSelected(true);
				this.byId("idSegment").setEnabled(false);
				this.byId("idSegment").setSelectedKeys();
				break;
			default:
				break;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Priority
		 * Checks the Priority
		 * @(parameter) (oEvent)
		 */
		handleLiveChangePriority: function (oEvent) {
			var newValue = oEvent.getParameters("selectedItem");
			newValue = newValue.selectedItem.getKey();
			if (newValue === "") {
				fld12 = 0;
			} else {
				fld12 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Ignore SAP Industry
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeIgonreSAPIndustry: function (oEvent) {
			fld15 = 0;
			fld15_1 = this.handleSapIndustryKeyChange(oEvent, this);
			switch (fld15_1) {
			case -1:
				var removeSegmentsDialog = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.removeSegmentsDialog");
				removeSegmentsDialog.open();
				sap.ui.getCore().byId("removeSegmentsConfirmation").attachPress(this.removeSegmentsConfirmation);
				sap.ui.getCore().byId("removeSegmentsDecline").attachPress(this.onKeepSegments);
				sap.ui.getCore().byId("idRemoveSegments").setText(i18n.getText("RSDText2_1TXT"));
				break;
			case 0: 
				fld15_1 = 1;
				fld15_2 = 0;
				fld15_3 = 0;
				break;
			case 1:
				fld15_1 = 1;
				fld15_2 = 0;
				fld15_3 = 0;
				break;
			default:
				break;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Delete Card confirmation.
		 * @function deleteCardConfirmation
		 * @param {object} Card Id to be deleted
		 */
		removeSegmentsConfirmation: function () {
			var dialog = sap.ui.getCore().byId("removeSegmentsDialog");
			dialog.close();
			dialog.destroy();
			fld15 = 0;
			fld15_1 = 1;
			fld15_2 = 0;
			fld15_3 = 0;
			_oController.byId("idSegment").setSelectedKeys();
			_oController.byId("idExpressCare").setSelected(false);
			_oController.byId("idExpressCareBranded").setSelected(false).setVisible(false);
			_oController.byId("idExpressCareNonBranded").setSelected(false).setVisible(false);
			_oController.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Closes the remove segments message box
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		onKeepSegments: function () {
			var dialog = sap.ui.getCore().byId("removeSegmentsDialog");
			dialog.close();
			dialog.destroy();
			fld15 = 0;
			fld15_1 = 0;
			_oController.getView().byId("idIgnoreIndustry").setSelected(false);
			_oController.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Segments
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeSegments: function (oEvent) {
			fld15 = 0;
			fld15_1 = 0;
			fld15_2 = this.checkSegmentField(oEvent, this);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Express Care
		 * @function handleLiveChangeExpressCare
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeExpressCare: function (oEvent) {
			var newValue = oEvent.getParameter("selected");
			if (newValue) {
				_oController.getView().byId("idExpressCareBranded").setSelected(true).setVisible(true);
				_oController.getView().byId("idExpressCareNonBranded").setSelected(true).setVisible(true);
			} else {
				_oController.getView().byId("idExpressCareBranded").setSelected(true).setVisible(false);
				_oController.getView().byId("idExpressCareNonBranded").setSelected(true).setVisible(false);
			}
			fld15 = 0;
			fld15_1 = 0;
			fld15_3 = 1;
			this.byId("idIgnoreIndustry").setSelected(false);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Express Care Branded
		 * @function handleChangeExpressCareBranded
		 * Forces the selected value to true if Non-Branded is not selected
		 * @(parameter) oEvent
		 */
		handleChangeExpressCareBranded: function (oEvent) {
			if (!oEvent.getParameter("selected") && !_oController.getView().byId("idExpressCareNonBranded").getSelected()) {
				_oController.getView().byId("idExpressCareNonBranded").setSelected(true);
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * handleLiveChangeLanguage
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeLanguage: function (oEvent) {
			if (oEvent.getParameter("selectedItem").getKey()) {
				fld10 = 1;
			} else {
				fld10 = 0;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Express Care Non-Branded
		 * @function handleChangeExpressCareNonBranded
		 * Forces the selected value to true if Branded is not selected
		 * @(parameter) oEvent
		 */
		handleChangeExpressCareNonBranded: function (oEvent) {
			if (!oEvent.getParameter("selected") && !_oController.getView().byId("idExpressCareBranded").getSelected()) {
				_oController.getView().byId("idExpressCareBranded").setSelected(true);
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Date Start
		 * Checks the start and end dates
		 * @(parameter) (oEvent)
		 */

		handleLiveChangeDateSt: function (oEvent) {
			fld13 = this.handleDateChange(oEvent, this);
			if (fld13) {
				fld14 = 1;
			}
			fld9 = this._processStatus(this);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Date End
		 * Checks the start and end dates
		 * @(parameter) (oEvent)
		 */

		handleLiveChangeDateEn: function (oEvent) {
			fld14 = this.handleDateChange(oEvent, this);
			if (fld14) {
				fld13 = 1;
			}
			fld9 = this._processStatus(this);
			this.onConfigureSaveButton();
		},

		/**
		 * 2nd Form
		 * It is fired everytime a change is made in the form and adjusts the view according to the changes.
		 * @function handleLiveChange
		 * @param {Object} oEvent - the event that fired the function
		 */

		handleLiveChange: function (oEvent) {
			var newValue = oEvent.getParameter("value");
			var oId = oEvent.getParameter("id");
			var objId = oId.split("--");
			switch (objId[1]) {
			case "HCTitle":
				newValue = oEvent.getParameter("value");
				newValue = newValue.trim();
				if (newValue === "") {
					this.getView().byId('CardTitle').setText(this.getResourceBundle().getText("CASECText3TXT"));
					fld1 = 0;
				} else {
					this.getView().byId('CardTitle').setText(newValue);
					fld1 = 1;
				}
				break;
			case "HCSubTitle":
				newValue = oEvent.getParameter("value");
				newValue = newValue.trim();
				if (newValue === "") {
					this.getView().byId('CardSubTitle').setText(this.getResourceBundle().getText("GENCAText1TXT"));
					fld2 = 0;
				} else {
					this.getView().byId('CardSubTitle').setText(newValue);
					fld2 = 1;
				}
				break;
			case "HCDesc":
				newValue = oEvent.getParameter("value");
				newValue = newValue.trim();
				if (newValue === "") {
					this.getView().byId('CardDesc').setText(this.getResourceBundle().getText("FEEDCText1TXT"));
					fld3 = 0;
				} else {
					this.getView().byId('CardDesc').setText(newValue);
					fld3 = 1;
				}
				break;
			case "HCLinkLabel":
				newValue = oEvent.getParameter("value");
				newValue = newValue.trim();
				if (newValue === "") {
					this.getView().byId('CardLink').setText(this.getResourceBundle().getText("HEROCText1LK"));
					fld4 = 0;
				} else {
					this.getView().byId('CardLink').setText(newValue);
					fld4 = 1;
				}
				break;
			case "HCImgVid":
				defaultVal = "../webapp/resources/img/HeroCardTemplate.png";
				if (newValue === "") {
					$('.heroCardTemplate').css({
						'background-image': 'url("' + defaultVal + '")'
					});
					fld5 = 0;
				} else {
					$('.heroCardTemplate').css({
						'background-image': 'url("' + newValue + '")'
					});
					this.getView().getModel("local").setProperty("/ContentCardItem/mediaURL", newValue);
					fld5 = 1;
				}
				break;
			case "HCURLTarget":
				newValue = oEvent.getParameters("selectedItem");
				newValue = newValue.selectedItem.getKey();
				if (newValue === "") {
					fld6 = 0;
				} else {
					fld6 = 1;
				}
				break;
			case "HCLinkTarget":
				newValue = oEvent.getParameter("value");
				newValue = newValue.trim();
				if (newValue === "") {
					fld7 = 0;
				} else {
					fld7 = 1;
				}
				break;
			case "checkBoxBackToDash":
				var check = oEvent.getSource().getSelected();
				if (check === true) {
					this.getView().getModel("local").setProperty("/isBackToDash", "1");
				} else {
					this.getView().getModel("local").setProperty("/isBackToDash", "0");
				}
				break;
			default:
				break;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * Configures the Save Button
		 */
		onConfigureSaveButton: function () {
			var oSaveBtn = this.getView().byId("saveButton");
			// If one of the Ignore SAP Industry or Segment or Express Care selected is select
			if (fld15_1 === 1 || fld15_2 === 1 || fld15_3 === 1 || fld15 === 1) {
				fld15 = 1;
			} else {
				fld15 = 0;
			}
			var inputCtr = fld1 + fld2 + fld3 + fld4 + fld5 + fld6 + fld7 + fld8 + fld9 + fld10 + fld11 + fld12 + fld13 + fld14 + fld15;
			if (inputCtr === 15) {
				oSaveBtn.setEnabled(true);
			} else {
				oSaveBtn.setEnabled(false);
			}
		},

		/**
		 * Fired when the user clicks on the update/create button.
		 * Checks if the name of the card already exists, if so shows a error message, and doesn't allow the change.
		 * Asks the user to confirm he really wants to make the change.
		 * @function onConfirmation
		 */
		onConfirmation: function () {
			var existingCards = sap.ui.getCore().getModel("local").getData().ContentCardItem;
			var existingCardsLength = existingCards.length;
			var oLocalName = _oController.getView().getModel("local").getData().name;
			var oLocalId = _oController.getView().getModel("local").getData().id;
			var exist = 0;

			if (res === "New" || action === "Copy") {
				for (var i = 0; i < existingCardsLength; i++) {
					if (oLocalName === existingCards[i].name) {
						exist = 1;
					}
				}
				if (exist === 0) {
					var messagedialogFragment = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.messagedialog");
					messagedialogFragment.open();
					sap.ui.getCore().byId("yes").attachPress(this.onSaveEntryForm);
					sap.ui.getCore().byId("idConfirm").setText(i18n.getText("EFCONText3TXT"));
				} else {
					MessageBox.error(i18n.getText("GEFCONText1TXT"));
				}
			} else {
				for (i = 0; i < existingCardsLength; i++) {
					if (oLocalName === existingCards[i].name && existingCards[i].id !== oLocalId) {
						exist = 1;
					}
				}
				if (exist === 0) {
					messagedialogFragment = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.messagedialog");
					messagedialogFragment.open();
					sap.ui.getCore().byId("yes").attachPress(this.onUpdateEntryForm);
					sap.ui.getCore().byId("idConfirm").setText(i18n.getText("EFCONText1TXT"));
				} else {
					MessageBox.error("GEFCONText1TXT");
				}
			}
			sap.ui.getCore().byId("no").attachPress(this.onReturn);
		},

		/**
		 * Closes the error/warning message box
		 * @function onReturn
		 */
		onReturn: function () {
			var dialog = sap.ui.getCore().byId("msgdialog");
			dialog.close();
			dialog.destroy();
		},

		/**
		 * Fired when the user confirms he wants to create a new card and call generic create card function.
		 * @function onSaveEntryForm
		 */
		onSaveEntryForm: function () {
			_oController.onCreate(_oController);
		},

		/**
		 * Fired when the user confirms he wants to update a card.
		 * Retrieves the information from the form and updates the card in the database.
		 * If something goes wrong, shows a error message.
		 * If the card is successfully updated, navs back to the main page.
		 * @function onUpdateEntryForm
		 */
		onUpdateEntryForm: function () {
			_oController.onUpdate(_oController);
		}

	});
});