sap.ui.define([
	'sap/m/MessageBox',
	"valvoline/dash/DashWCMT/controller/Base.controller"
], function (MessageBox, Base) {
	"use strict";
	var _oRouter, i18n, oController, oThis, _oView, _oLocal;
	var fld = [];
	var ordfld = [];

	return Base.extend("valvoline.dash.DashWCMT.controller.FeedSettings", {
		/** @module FeedSettings */
		/**
		 * Attaches an event to everytime someone navs to the feed settings page.
		 * Initializes the global variables.
		 * Initializes the fragments.
		 * Gets the core models and associates them to the view.
		 * @function onInit
		 */
		onInit: function () {
			oController = this;
			i18n = oController.getOwnerComponent().getModel("i18n").getResourceBundle();
			_oRouter = this.getRouter();
			_oRouter.getRoute("feedsettings").attachPatternMatched(this._onObjectMatched, this);
			oThis = this;
			_oView = this.getView();
			this.initFragments();
		},
		/**
		 * Is fired everytime a user navs to the feed settings page.
		 * Checks if the user is authorized to access this page.
		 * Gets the orders and cases feed cards info
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {
			this.checkAuthorization();

			// Check if the local model was loaded
			if (sap.ui.getCore().getModel("local") === undefined) {
				this.getRouter().navTo("cardManager");
				return;
			}

			var domain = sap.ui.getCore().getModel("domain");
			this.getView().setModel(domain, "domain");
			var userModel = sap.ui.getCore().getModel("userapi");
			this.getView().setModel(userModel, "userapi");
			this.loadTransCards();
			sap.ui.getCore().getModel("oLocal").setProperty("/CaseQuantity", this.getByGlobalParam("NumberOfCasesDisplayed"));
			sap.ui.getCore().getModel("oLocal").setProperty("/OrderQuantity", this.getByGlobalParam("NumberOfOrdersDisplayed"));
			sap.ui.getCore().getModel("oLocal").refresh();

		},

		/**
		 * Gets the card list to show on the table, deals with multi-section cards, returning only the first one
		 * @function loadTransCards
		 * @returns the list of cards
		 */
		loadTransCards: function () {
			// Create Local Model
			var oTransCards = new sap.ui.model.json.JSONModel();
			sap.ui.getCore().setModel(oTransCards, "oLocal");
			_oView.setModel(sap.ui.getCore().getModel("oLocal"), "oLocal");
			_oLocal = sap.ui.getCore().getModel("oLocal");

			var allCards = sap.ui.getCore().getModel("local").getData().Cards;
			for (var i = 0; i < allCards.length; i++) {
				if (allCards[i].contentCards.cardCategory === "4000" && allCards[i].contentCards.cardType === "100" && allCards[i].contentCards.lang === "en") {
					sap.ui.getCore().getModel("oLocal").setProperty("/Cases", allCards[i].contentCards);
				} else if (allCards[i].contentCards.cardCategory === "3000" && allCards[i].contentCards.cardType === "100" && allCards[i].contentCards.lang === "en") {
					sap.ui.getCore().getModel("oLocal").setProperty("/Orders", allCards[i].contentCards);
				} else {
					continue;
				}
			}
			sap.ui.getCore().getModel("oLocal").refresh();
			oThis.fnDisplay();
		},

		/**
		 * Calls service to retrieve a global parameters by their nameID.
		 * @function getByGlobalParam
		 * @param {string} value - the nameID of the parameter
		 * @returns the global parameter
		 */
		getByGlobalParam: function (value) {

			var xsURL = "/core/parameterListByNameID?nameID=" + value;
			var oData = oController.handleAjaxJSONCall(oController, false, xsURL, "GET", oController.onGetByGlobalParamSuccess,
				oController.onGetByGlobalParamError);
			return oData;

		},

		onGetByGlobalParamSuccess: function (oContext, oData) {
			return oData[0];
		},

		onGetByGlobalParamError: function (oContext, oError) {
			// show the error message from server
			oContext.showServerErrorMsg(oError.status, oError.statusText, oError.responseText);
		},

		/**
		 * Convenience method for initializing framgents for Cases and Orders.
		 * @function initFragments
		 */
		initFragments: function () {
			var oSF = this.getView().byId("feedSettingsContainer");
			var Frag = new sap.ui.xmlfragment("idCaseFrag", "valvoline.dash.DashWCMT.fragment.casesCard", this);
			oSF.addItem(Frag);

			var Frag2 = new sap.ui.xmlfragment("idOrderFrag", "valvoline.dash.DashWCMT.fragment.ordersCard", this);
			oSF.addItem(Frag2);
		},

		/**
		 * Convenience method for displaying the cases and orders info to the template objects.
		 * @function fnDisplay
		 */
		fnDisplay: function () {
			// Set Case Info
			var oCases = _oLocal.getProperty("/Cases");
			var oOrders = _oLocal.getProperty("/Orders");
			sap.ui.core.Fragment.byId("idCaseFrag", "idCasesTitle").setText(oCases.title);
			sap.ui.core.Fragment.byId("idCaseFrag", "idCasesUrl").setSrc(oCases.mediaURL);
			sap.ui.core.Fragment.byId("idCaseFrag", "idCasesSubtitle").setText(oCases.subtitle);

			// Set Order Info
			sap.ui.core.Fragment.byId("idOrderFrag", "idOrdersTitle").setText(oOrders.title);
			sap.ui.core.Fragment.byId("idOrderFrag", "idOrdersUrl").setSrc(oOrders.mediaURL);
			sap.ui.core.Fragment.byId("idOrderFrag", "idOrdersSubtitle").setText(oOrders.subtitle);
			
			//Disabled Update buttons
			sap.ui.core.Fragment.byId("idCaseFrag", "CaseSaveButton").setEnabled(false);
			sap.ui.core.Fragment.byId("idOrderFrag", "OrdersSaveButton").setEnabled(false);
		},

		/**
		 * Convenience method for deleting an item of an array.
		 * @function delArrayVal
		 * @param {array} oArr - the array from where you want to delete
		 * @param {string} fldName - the item you want to delete
		 */
		delArrayVal: function (oArr, fldName) {
			var intLen = oArr.length;
			for (var x = 0; x <= intLen; x++) {
				if (oArr[x - 1] === fldName) {
					oArr.splice(x - 1, 1);
				}
			}
		},

		/**
		 * Convenience method for handling live updates for the cases input fields and adjusting the view accordingly.
		 * @function handleCaseLiveChande
		 * @param {Object} oEvent - the event that fired the function
		 */
		handleCaseLiveChange: function (oEvent) {
			var intLen;
			var newValue = oEvent.getParameter("value");
			var oId = oEvent.getParameter("id");
			var objId = oId.split("--");
			switch (objId[1]) {
			case "CasesTitle":
				newValue = oEvent.getParameter("value");
				newValue = newValue.trim();
				if (newValue === "") {
					sap.ui.core.Fragment.byId("idCaseFrag", "idCasesTitle").setText(this.getResourceBundle().getText("CASECText3TXT"));
					fld.push("CasesTitle");
				} else {
					sap.ui.core.Fragment.byId("idCaseFrag", "idCasesTitle").setText(newValue);
					intLen = fld.length;
					if (intLen > 0) {
						this.delArrayVal(fld, "CasesTitle");
					}
				}
				break;
			case "CasesSubTitle":
				newValue = oEvent.getParameter("value");
				newValue = newValue.trim();
				if (newValue === "") {
					sap.ui.core.Fragment.byId("idCaseFrag", "idCasesSubtitle").setText(this.getResourceBundle().getText("GENCAText1TXT"));
					fld.push("CasesSubTitle");
				} else {
					sap.ui.core.Fragment.byId("idCaseFrag", "idCasesSubtitle").setText(newValue);
					intLen = fld.length;
					if (intLen > 0) {
						this.delArrayVal(fld, "CasesTitle");
					}
				}
				break;
			case "CasesImg":
				if (newValue === "") {
					sap.ui.core.Fragment.byId("idCaseFrag", "idCasesUrl").setSrc("resources/img/imageonly.png");
					fld.push("CasesImg");
				} else {
					sap.ui.core.Fragment.byId("idCaseFrag", "idCasesUrl").setSrc(newValue);
					intLen = fld.length;
					if (intLen > 0) {
						this.delArrayVal(fld, "CasesImg");
					}
				}
				break;
			case "CasesCaseDisp":
				newValue = oEvent.getParameters("selectedItem").selectedItem.getKey();

				if (newValue === "") {
					fld.push("CasesCaseDisp");
				} else {
					intLen = fld.length;
					if (intLen > 0) {
						this.delArrayVal(fld, "CasesCaseDisp");
					}
				}
				break;
			case "CasesDispPri":
				newValue = oEvent.getParameters("selectedItem");
				newValue = newValue.selectedItem.getKey();
				if (newValue === "") {
					fld.push("CasesDispPri");
				} else {
					intLen = fld.length;
					if (intLen > 0) {
						this.delArrayVal(fld, "CasesDispPri");
					}
				}
				break;
			default:
				break;
			}
			// End of Switch
			intLen = fld.length;
			var oSaveBtn = sap.ui.core.Fragment.byId("idCaseFrag", "CaseSaveButton");
			if (intLen === 0) {
				oSaveBtn.setEnabled(true);
			} else {
				oSaveBtn.setEnabled(false);
			}
		},

		/**
		 * Convenience method for handling live updates for the orders input fields and adjusting the view accordingly.
		 * @function handleOrderLiveChange
		 * @param {Object} oEvent - the event that fired the function
		 */
		handleOrderLiveChange: function (oEvent) {
			var intLen;
			var newValue = oEvent.getParameter("value");
			var oId = oEvent.getParameter("id");
			var objId = oId.split("--");
			switch (objId[1]) {
			case "OrdersTitle":
				newValue = oEvent.getParameter("value");
				newValue = newValue.trim();
				if (newValue === "") {
					sap.ui.core.Fragment.byId("idOrderFrag", "idOrdersTitle").setText(this.getResourceBundle().getText("CASECText3TXT"));
					ordfld.push("OrdersTitle");
				} else {
					sap.ui.core.Fragment.byId("idOrderFrag", "idOrdersTitle").setText(newValue);
					intLen = ordfld.length;
					if (intLen > 0) {
						this.delArrayVal(ordfld, "OrdersTitle");
					}
				}
				break;
			case "OrdersSubTitle":
				newValue = oEvent.getParameter("value");
				newValue = newValue.trim();
				if (newValue === "") {
					sap.ui.core.Fragment.byId("idOrderFrag", "idOrdersSubtitle").setText(this.getResourceBundle().getText("GENCAText1TXT"));
					ordfld.push("OrdersSubTitle");
				} else {
					sap.ui.core.Fragment.byId("idOrderFrag", "idOrdersSubtitle").setText(newValue);
					intLen = ordfld.length;
					if (intLen > 0) {
						this.delArrayVal(ordfld, "OrdersSubTitle");
					}
				}
				break;
			case "OrdersImg":
				if (newValue === "") {
					sap.ui.core.Fragment.byId("idOrderFrag", "idOrdersUrl").setSrc("resources/img/imageonly.png");
					ordfld.push("OrdersImg");
				} else {
					sap.ui.core.Fragment.byId("idOrderFrag", "idOrdersUrl").setSrc(newValue);
					intLen = ordfld.length;
					if (intLen > 0) {
						this.delArrayVal(ordfld, "OrdersImg");
					}
				}
				break;
			case "OrdersOrdDisp":
				newValue = oEvent.getParameters("selectedItem");
				newValue = newValue.selectedItem.getKey();
				if (newValue === "") {
					ordfld.push("OrdersOrdDisp");
				} else {
					intLen = ordfld.length;
					if (intLen > 0) {
						this.delArrayVal(ordfld, "OrdersOrdDisp");
					}
				}
				break;
			case "OrdersDispPri":
				newValue = oEvent.getParameters("selectedItem");
				newValue = newValue.selectedItem.getKey();
				if (newValue === "") {
					ordfld.push("OrdersDispPri");
				} else {
					intLen = ordfld.length;
					if (intLen > 0) {
						this.delArrayVal(ordfld, "OrdersDispPri");
					}
				}
				break;
			default:
				break;
			}
			// End of Switch
			intLen = ordfld.length;
			var oSaveBtn = sap.ui.core.Fragment.byId("idOrderFrag", "OrdersSaveButton");
			if (intLen === 0) {
				oSaveBtn.setEnabled(true);
			} else {
				oSaveBtn.setEnabled(false);
			}
		},

		/**
		 * Fired when the user clicks the update button. It shows a dialog box asking for user confirmation.
		 * @function onConfirmation
		 * @param {Object} oEvent - the event that fired the function
		 */
		onConfirmation: function (oEvent) {
			var messagedialogFragment = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.messagedialog");
			messagedialogFragment.open();
			var oId = oEvent.getParameter("id");
			var objId = oId.split("--");
			if (objId[1] === "CaseSaveButton") {
				sap.ui.getCore().byId("yes").attachPress(this.onUpdateCaseForm);
				sap.ui.getCore().byId("idConfirm").setText(i18n.getText("EFCONText1TXT"));
			} else if (objId[1] === "OrdersSaveButton") {
				sap.ui.getCore().byId("yes").attachPress(this.onUpdateOrderForm);
				sap.ui.getCore().byId("idConfirm").setText(i18n.getText("EFCONText1TXT"));
			}
			sap.ui.getCore().byId("no").attachPress(this.onReturn);
		},

		/**
		 * Closes the confirmation message dialog
		 * @function onReturn
		 */
		onReturn: function () {
			var dialog = sap.ui.getCore().byId("msgdialog");
			dialog.close();
			dialog.destroy();
		},

		/**
		 * Updates a global parameter in the database
		 * @function updateByValue
		 * @param {string} nameID - the nameID of the global parameter
		 * @param {string} value - the new value of the global parameter
		 */

		updateByValue: function (nameID, value, oPiggyBack) {
			var xsURL = "/core/updateGlobalParameter?nameID=" + nameID + "&value=" + value;
			var oData = oController.handleAjaxJSONCall(oController, false, xsURL, "GET", oController.updateByValueSuccess,
				oController.updateByValueError, null, oPiggyBack);
			return oData;

		},

		updateByValueSuccess: function (oContext, oData, oPiggyBack) {
			sap.ui.core.Fragment.byId(oPiggyBack.frag, oPiggyBack.button).setEnabled(false);
			return oData;
		},

		updateByValueError: function (oError) {
			MessageBox.error("An Error Occurred! " + oError);
		},

		/**
		 * Updates the case feed card on the database. 
		 * If something goes wrong an error message is displayed and the update is not made.
		 * @function onUpdateCaseForm
		 */
		onUpdateCaseForm: function () {
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			msgdialog.setBusyIndicatorDelay(0);
			msgdialog.setBusy(true);
			var oLocalData = oController.getView().getModel("oLocal").getData();

			var oEntry = {};
			oEntry.id = oLocalData.Cases.id;
			oEntry.title = oLocalData.Cases.title;
			oEntry.name = oLocalData.Cases.name;
			oEntry.status = oLocalData.Cases.status;
			oEntry.displayPriority = parseInt(oLocalData.Cases.displayPriority, 10);

			oEntry.isRoleBased = "F";
			oEntry.cardType = oLocalData.Cases.cardType;
			oEntry.cardCategory = oLocalData.Cases.cardCategory;
			oEntry.section = "1";
			oEntry.subtitle = oLocalData.Cases.subtitle;
			oEntry.shortText = oLocalData.Cases.shortText;
			oEntry.contentCardId = oLocalData.Cases.contentCard;
			oEntry.callToActionLabel = oLocalData.Cases.callToActionLabel;
			oEntry.callToActionURL = oLocalData.Cases.callToActionURL;
			oEntry.mediaURL = oLocalData.Cases.mediaURL;
			oEntry.urlTarget = oLocalData.Cases.urlTarget;
			var xsURL = "/core/contentCards/" + oLocalData.Cases.id;
			var body = {
				"contentCards": oEntry
			};
			
			oController.handleAjaxJSONCall(oController, false, xsURL, "PUT", oController.onUpdateCaseFormSuccess,
				oController.onUpdateCaseFormError, body);

		},
		onUpdateCaseFormSuccess: function () {
			var oLocalData = oController.getView().getModel("oLocal").getData();
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			var nameID = oLocalData.CaseQuantity.nameID;
			var value = oLocalData.CaseQuantity.value;
			msgdialog.setBusy(false);
			msgdialog.close();
			msgdialog.destroy();
			var oPiggyBack = {
				"frag": "idCaseFrag",
				"button":"CaseSaveButton"
				};
			oThis.updateByValue(nameID, value, oPiggyBack);
		},
		onUpdateCaseFormError: function (oError) {
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			msgdialog.setBusy(false);
			msgdialog.close();
			msgdialog.destroy();
			var errRes = oController.getError(oError);
			MessageBox.error("An Error Occurred! " + errRes);
		},

		/**
		 * Updates the order feed card on the database. 
		 * If something goes wrong an error message is displayed and the update is not made.
		 * @function onUpdateOrderForm
		 */
		onUpdateOrderForm: function () {
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			msgdialog.setBusyIndicatorDelay(0);
			msgdialog.setBusy(true);
			var oLocalData = oController.getView().getModel("oLocal").getData();

			var oEntry = {};
			oEntry.id = oLocalData.Orders.id;
			oEntry.title = oLocalData.Orders.title;
			oEntry.name = oLocalData.Orders.name;
			oEntry.status = oLocalData.Orders.status;
			oEntry.displayPriority = parseInt(oLocalData.Orders.displayPriority, 10);

			oEntry.isRoleBased = "F";
			oEntry.cardType = oLocalData.Orders.cardType;
			oEntry.cardCategory = oLocalData.Orders.cardCategory;
			oEntry.section = "1";
			oEntry.subtitle = oLocalData.Orders.subtitle;
			oEntry.shortText = oLocalData.Orders.shortText;
			oEntry.callToActionLabel = oLocalData.Orders.callToActionLabel;
			oEntry.callToActionURL = oLocalData.Orders.callToActionURL;
			oEntry.mediaURL = oLocalData.Orders.mediaURL;
			oEntry.urlTarget = oLocalData.Orders.urlTarget;
			var body = { 
				"contentCards": oEntry
			};
			var xsURL = "/core/contentCards/" + oLocalData.Orders.id;
			oController.handleAjaxJSONCall(oController, false, xsURL, "PUT", oController.onUpdateOrderFormSuccess,
				oController.onUpdateOrderFormError, body);

		},

		onUpdateOrderFormSuccess: function () {
			var oLocalData = oController.getView().getModel("oLocal").getData();
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			var nameID = oLocalData.OrderQuantity.nameID;
			var value = oLocalData.OrderQuantity.value;
			msgdialog.setBusy(false);
			msgdialog.close();
			msgdialog.destroy();
			var oPiggyBack = {
				"frag": "idOrderFrag",
				"button":"OrdersSaveButton"
				};
			oThis.updateByValue(nameID, value, oPiggyBack);
		},
		onUpdateOrderFormError: function (oError) {
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			msgdialog.setBusy(false);
			msgdialog.close();
			msgdialog.destroy();
			var errRes = oController.getError(oError);
			MessageBox.error("An Error Occurred! " + errRes);
		}

	});
});