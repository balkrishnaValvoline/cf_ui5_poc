sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/m/MessageBox"
], function (Controller, History, MessageBox) {
	"use strict";

	var bSessionTimeOut = false;
	return Controller.extend("valvoline.dash.DashWCMT.controller.Base", {
		/** @module Base **/
		/**
		 * gets the Router from sap.ui.core
		 * @function getRouter
		 */

		/**
		 * Generic function to handle Ajax JSON Calls
		 * @function handleAjaxJSONCall
		 * @param {Object} oController - Sets the context for the code.
		 * @param {Bolean} aSync - Indicates if the call should be asynchronous or not.
		 * @param {String} sEndPoint - Call endPoint and url parameters.
		 * @param {String} sCallType - Indicates if it is a POST or a GET Call.
		 * @param {Function} fSuccess - Function to execute if the call is successfull.
		 * @param {Function} fError - Function to execute if the call is unsuccessfull.
		 * @param {Object} oDataToSend - Data to be sent if it is a POST call, undefined otherwise.
		 * @param {Object] oPiggyBack - Auxiliary object to transport data from the calling function to the success function
		 **/
		handleAjaxJSONCall: function (oController, aSync, sEndPoint, sCallType, fSuccess, fError, oDataToSend, oPiggyBack) {
			var oReturn;
			//Reset the service call idle time counter
			window.nServiceIdleTime = 0;
			$.ajax({
				beforeSend: function (request) {
					request.setRequestHeader("Content-Type", "application/json");
					// Enables XSS filtering. Rather than sanitizing the page, the browser will prevent rendering of the page if an attack is detected.
					request.setRequestHeader("X-Xss-Protection", " 1; mode=block");
					//Stops the browser from trying to MIME-sniff the content type and forces it to stick with the declared content-type
					request.setRequestHeader("X-Content-Type-Options", "nosniff");
				},
				type: sCallType,
				data: JSON.stringify(oDataToSend),
				url: "/service-layer-dash" + sEndPoint,
				async: aSync,
				success: function (oData, textStatus, xhr) {
					// Service TimeOut Check
					if (oController.checkSessionTimeoutServices(xhr)) {
						oReturn = true;
						return;
					}
					if (oPiggyBack === undefined) {
						oPiggyBack = {};
					}
					oPiggyBack.textStatus = textStatus;
					oPiggyBack.xhr = xhr;
					oReturn = fSuccess(oController, oData, oPiggyBack);
				},
				error: function (oError) {
					// Service TimeOut Check
					if (oController.checkSessionTimeoutServices(oError)) {
						oReturn = true;
						return;
					}
					oReturn = fError(oController, oError, oPiggyBack);
				}
			});
			if (!aSync) {
				return oReturn;
			}
		},

		/**
		 * Check the SAML session timeout for Services used in the error function of the ajax call
		 * @function checkSessionTimeoutServices
		 * @param {Object} oError - Service error
		 */
		checkSessionTimeoutServices: function (oError) {
			var isTimeout = false;
			if (oError.getResponseHeader("com.sap.cloud.security.login")) {
				isTimeout = true;
				if (!bSessionTimeOut) {
					sap.m.MessageBox.show(
						"Session is expired, page will be reloaded!", {
							icon: sap.m.MessageBox.Icon.INFORMATION,
							title: "Information",
							actions: [sap.m.MessageBox.Action.CLOSE],
							onClose: function () {
								window.location.reload();
							}
						}
					);
				}
				bSessionTimeOut = true;
			}
			return isTimeout;
		},

		getRouter: function () {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		/**
		 * redirects user to the logout page when logout button is pressed
		 * @function logUserOut
		 */
		logUserOut: function () {
			sap.m.URLHelper.redirect("/do/logout?appid=WCMTTest", false);
		},

		/**
		 * Convenience method to navigate to card catalog page.
		 * @function toCardCatalog
		 */
		toCardCatalog: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			this.getRouter().navTo("cardCatalog");
		},

		/**
		 * Allows access to the i18n model
		 * @function getResourceBundle
		 */
		getResourceBundle: function () {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

		/**
		 * Convenience method for navigating back to main page.
		 * @function backToCardManager
		 */
		backToCardManager: function () {
			this.getRouter().navTo("cardManager");
		},

		/**
		 * Configure date pickers to not allow users to choose dates from the past
		 * @ parameter (controller)
		 */
		configureDatePickers: function (controller) {
			var dateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
				pattern: "MM/dd/yyyy"
			});
			var dateToday = dateFormat.format(new Date());
			var date = dateToday.split("/");
			var month = parseInt(date[0], 10) - 1;
			var day = parseInt(date[1], 10);
			var year = parseInt(date[2], 10);

			controller.byId("idDispSDate").setMinDate(new Date(year, month, day));
			controller.byId("idDispEDate").setMinDate(new Date(year, month, day));

			var oDP1 = controller.getView().byId("idDispSDate");
			oDP1.addEventDelegate({
				onAfterRendering: function () {
					var oDateInner = this.$().find('.sapMInputBaseInner');
					var oID = oDateInner[0].id;
					$('#' + oID).attr("disabled", "disabled");
				}
			}, oDP1);

			var oDP2 = controller.getView().byId("idDispEDate");
			oDP2.addEventDelegate({
				onAfterRendering: function () {
					var oDateInner = this.$().find('.sapMInputBaseInner');
					var oID = oDateInner[0].id;
					$('#' + oID).attr("disabled", "disabled");
				}
			}, oDP2);
		},

		/**
		 * Calls service to get a list of domains with a given name
		 * @function getByName
		 * @param {string} value - the name of the domains
		 * @returns the list of domains
		 */
		getByName: function (value) {
			var controller = this;
			var xsURL = "/core/domainListByName?name=" + value;
			var oData = controller.handleAjaxJSONCall(controller, false, xsURL, "GET", controller.getByNameSuccess,
				controller.getByNameError);
			return oData;
		},

		getByNameSuccess: function (oContext, data) {
			return data;
		},
		getByNameError: function (oContext, oError) {
			// show the error message from server
			oContext.getError(oError);
		},

		/**
		 * Parses a service response to retrieve its error message
		 * @function getError
		 * @param {Object} response - the response from the server
		 * @returns the error message
		 */
		getError: function (response) {
			var messageText;
			try {
				// Try to parse as a JSON string
				// Parse JSON and extract fault string
				var oMessage = JSON.parse(response.responseText);
				return oMessage.fault.faultstring;
			} catch (err) {
				try { // Whoops - not JSON, check if this is XML
					switch (typeof response.responseText) {
					case "string": // XML or simple text
						if (response.responseText.indexOf("<?xml") == 0) {
							var oXML = jQuery.parseXML(response.responseText);
							var oXMLMsg = oXML.querySelector("message");
							if (oXMLMsg) {
								messageText = oXMLMsg.textContent;
							}
						} else {
							// Determine if the String body has HTML text
							if (messageText.substring(0, 15) === "<!doctype html>") {
								// Extract the title text of the error message
								var errHTML = messageText.substring(44);
								var titleTagIndex = errHTML.indexOf("</title>");
								messageText = errHTML.substring(0, titleTagIndex);
							} else {
								// return the string
								messageText = response.responseText;
							}
						}
						break;
					case "object": // Exception
						messageText = response.responseText.toString();
						break;
					default:
						break;
					}
				} catch (_) {
					messageText = "An unknown error occurred";
				}
			}

			return messageText;
		},

		/**
		 * Takes a date in JSON format and turns it into a js Date
		 * @function parseJsonDate
		 * @param {string} jsonDate - the jsonDate to parse
		 * @returns the parsed date
		 */
		parseJsonDate: function (jsonDate) {
			var offset = new Date().getTimezoneOffset();
			var parts = /\/Date\((-?\d+)([+-]\d{2})?(\d{2})?.*/.exec(jsonDate);

			if (parts[2] == undefined) {
				parts[2] = 0;
			}

			if (parts[3] == undefined) {
				parts[3] = 0;
			}

			return new Date(+parts[1] + offset + parts[2] * 3600000 + parts[3] * 60000);
		},
		/**
		 * Calls a service to see if the user is authorized to the WCMT. If he isn't, redirects him to the not Authorized page
		 * @function checkAuthorization
		 */
		checkAuthorization: function () {
			var controller = this;
			var xsURL = "/auth/dashAccessList";

			controller.handleAjaxJSONCall(controller, false, xsURL, "GET", controller.checkAuthorizationSuccess,
				controller.checkAuthorizationError);
		},

		checkAuthorizationSuccess: function (oContext, data) {
			if (data == undefined) {
				oContext.getRouter().navTo("notAuthorized");
			} else {
				if (data.AccessList.indexOf("WCMT_PAGE") === -1) {
					oContext.getRouter().navTo("notAuthorized");
				}
			}
		},
		checkAuthorizationError: function (oContext, oError) {
			if (oError.status === 403) {
				oContext.getRouter().navTo("notAuthorized");
			} else {
				oContext.getRouter().navTo("unexpectedError");
			}
		},

		/**
		 * Ignore Sap Industry
		 */
		onIgnoreIndustry: function () {
			var ignoreIndustryCheckBox = this.byId("idIgnoreIndustry");
			var fld;
			//Called when check box is clicked
			var isSelected = ignoreIndustryCheckBox.getSelected();
			if (isSelected) {
				this.byId("idSegment").setSelectedKeys();
				this.byId("idExpressCare").setSelected(false);
				fld = 1;
			} else {
				fld = 0;
			}
			return fld;
		},

		/**
		 * Calls service to get a list of domains with a given name
		 * @function getSegments
		 * @param {object} controller - controller of page calling the service
		 * @param {object} contentCardId - Card Id
		 */
		getSegments: function (controller, contentCardId) {
			var model = new sap.ui.model.json.JSONModel();
			var segmentAll;
			//get list of segments
			if (contentCardId === "New") {
				var xsURL = "/core/accountSegments";
				controller.handleAjaxJSONCall(controller, false, xsURL, "GET", controller.onGetSegmentsSuccess,
					controller.onGetSegmentsError);

			}
			//get Segments association with card
			else {
				var data = sap.ui.getCore().getModel("local").getProperty("/ContentCardSegments/" + contentCardId);
				// Order Data Ascending
				data = controller.onFilteroModel(data, "ascending", "segmentName");

				var countAll = 0;
				for (var j = 0; j < data.length; j++) {
					if (data[j].isOfCard) {
						controller.getView().byId("idSegment").addSelectedKeys(data[j].segmentId.toString());
						countAll++;
					}
				}
				if (countAll === data.length) {
					controller.getView().byId("idSegment").addSelectedKeys("0");
					segmentAll = {
						segmentId: 0,
						segmentName: "ALL",
						isOfCard: true
					};
				} else {
					segmentAll = {
						segmentId: 0,
						segmentName: "ALL",
						isOfCard: false
					};
				}
				data.splice(0, 0, segmentAll);
				model.setData(data);
				controller.getView().setModel(model, "cardSegments");
			}

		},

		onGetSegmentsSuccess: function (oController, oData) {
			// Order Data Ascending
			oData = oController.onFilteroModel(oData, "ascending", "name");
			var model = new sap.ui.model.json.JSONModel();
			var segmentList = [];
			var segment;
			var segmentAll;
			for (var i = 0; i < oData.length; i++) {
				segment = {
					segmentId: oData[i].id,
					segmentName: oData[i].name,
					isOfCard: false
				};
				segmentList.push(segment);
			}

			segmentAll = {
				segmentId: 0,
				segmentName: "ALL",
				isOfCard: false
			};

			segmentList.splice(0, 0, segmentAll);
			model.setData(segmentList);
			oController.getView().setModel(model, "cardSegments");
		},
		onGetSegmentsError: function (oController, oError) {
			// show the error message from server
			oController.getError(oError);
		},

		/**
		 * Ordering Algorithm
		 @ parameter (oModel,orderMode,objectName)
		 */
		onFilteroModel: function (oData, orderMode, objectName) {
			oData.sort(function (a, b) {
				switch (orderMode) {
				case "ascending":
					if (a.valueOf()[objectName] < b.valueOf()[objectName]) {
						return -1;
					} else if (a.valueOf()[objectName] > b.valueOf()[objectName]) {
						return 1;
					} else {
						return 0;
					}
					break;
				case "descending":
					if (a.valueOf()[objectName] < b.valueOf()[objectName]) {
						return 1;
					} else if (a.valueOf()[objectName] > b.valueOf()[objectName]) {
						return -1;
					} else {
						return 0;
					}
					break;
				default:
					break;
				}
			});
			return oData;
		},

		/**
		 * Calls service to associate segments to a card
		 * @function associateSegments
		 * @param {array} segments - segments to associate to the card
		 * @param {object} cardId - Card Id
		 */
		updateSegments: function (segments, cardId) {
			var controller = this;
			var xsURL = "/core/accountSegments?cardId=" + cardId;
			controller.handleAjaxJSONCall(controller, true, xsURL, "PUT", controller.updateSegmentsSuccess,
				controller.updateSegmentsError);
		},

		updateSegmentsSuccess: function (oController) {
			oController.getRouter().navTo("cardManager");
		},
		updateSegmentsError: function (oController, oError) {
			// show the error message from server
			oController.getError(oError);
		},

		/**
		 * Configure card segments array to be sent to service layer
		 * @function configCardSegment
		 * @param {array} cardSegments - selected segments ids
		 */
		configCardSegment: function (cardSegments) {
			var segmentsArray = [];
			if (!cardSegments) {
				return segmentsArray;
			} else {
				for (var i = 0; i < cardSegments.length; i++) {
					if (cardSegments[i] !== "0") {
						segmentsArray.push({
							segmentId: cardSegments[i]
						});
					}
				}

				return segmentsArray;

			}
		},

		/**
		 * Configure account properties array to be sent to service layer
		 * @function configAccountProperties
		 * @param {array} accountProperty - selected account property ids
		 * @param {string} name - selected account property name
		 */
		configAccountProperties: function (accountProperty, name) {
			var accountsArray = [];
			if (!accountProperty) {
				return accountsArray;
			} else {
				for (var i = 0; i < accountProperty.length; i++) {
					if (accountProperty[i] !== "0") {
						var obj = {};
						obj[name] = accountProperty[i];
						accountsArray.push(obj);
					}
				}

				return accountsArray;

			}
		},

		/**
		 * Check if card is Ignore SAP industry and returns the value
		 * @function configTrueFalseState
		 * @param {bolean} isIgnoreIndustry - Is Ignore Industry
		 *
		 */
		configTrueFalseState: function (boolean) {
			var ignoreValue;
			if (boolean) {
				ignoreValue = "T";
			} else {
				ignoreValue = "F";
			}
			return ignoreValue;
		},

		formatExpressCareCheckBox: function (code) {
			if (code === "T") {
				return true;
			} else {
				return false;
			}
		},

		/**
		 * formatter function to return boolean.
		 * @function formatIsCPLCheckBox
		 * @param {string} code - Parameter's isCPLChecked.
		 * @returns true or false
		 */
		formatIsCPLCheckBox: function (code) {
			if (code === "T") {
				return true;
			} else {
				return false;
			}
		},
		/**
		 *  Gets the global parameter info given its name.
		 * @function getGlobalParamete
		 * @param {string} nameId - Parameter's nameID.
		 * @returns Global parameter data
		 */
		getGlobalParameter: function (nameId) {
			var controller = this;
			var xsURL = "/core/parameterListByNameID?nameID=" + nameId;
			var oData = controller.handleAjaxJSONCall(controller, false, xsURL, "GET", controller.getGlobalParameterSuccess,
				controller.getGlobalParameterError);
			return oData;

		},
		getGlobalParameterSuccess: function (oController, data) {
			return data;
		},
		getGlobalParameterError: function (oContext, oError) {
			oContext.getError(oError);
		},

		/**
		 * Function to check if there is segment selected and logic for express care enable/disable
		 * @function checkSegmentField
		 * @param {string} nameId - Parameter's nameID.
		 * @returns Global parameter data
		 */
		checkSegmentField: function (oEvent, controller) {
			var fld15;
			var selectedKeys = oEvent.getSource().getSelectedKeys().length;
			// no segments selected
			if (selectedKeys === 0) {
				fld15 = 0;
			} else {
				fld15 = 1;
				if (controller.getView().byId("idIgnoreIndustry")) {
					controller.getView().byId("idIgnoreIndustry").setSelected(false);
				}
				//when the segment clicked was "ALL"
				if (oEvent.getParameters().changedItem.getProperty("key") === "0") {
					// "ALL selected"
					if (oEvent.getParameters().selected) {
						oEvent.getSource().setSelectedItems(oEvent.getSource().getItems());
					}
					// "ALL deselected"
					else {
						oEvent.getSource().removeAllSelectedItems();
						fld15 = 0;
						return fld15;
					}
				} else {
					if (oEvent.getSource().getSelectedKeys().indexOf(oEvent.getParameters().changedItem.getProperty("key")) === -1) {
						oEvent.getSource().removeSelectedKeys(["0"]);
					}
				}
				var selectedItems = oEvent.getSource().getSelectedItems();
				if (selectedItems.length === oEvent.getSource().getItems().length - 1 && oEvent.getSource().getSelectedKeys().indexOf("0") ===
					-1) {
					oEvent.getSource().addSelectedKeys("0");
				}
			}
			return fld15;
		},

		/**
		 * Function to check if there are account properties selected
		 * @function checkAccountPropertiesField
		 * @param {string} nameId - Parameter's nameID.
		 * @returns Global parameter data
		 */
		checkAccountPropertiesField: function (oEvent, controller) {
			var fld15;
			var selectedKeys = oEvent.getSource().getSelectedKeys().length;
			// no customerGroup5 selected
			if (selectedKeys === 0) {
				fld15 = 0;
			} else {
				fld15 = 1;
				//when the customerGroup5 clicked was "ALL"
				if (oEvent.getParameters().changedItem.getProperty("key") === "0") {
					// "ALL selected"
					if (oEvent.getParameters().selected) {
						oEvent.getSource().setSelectedItems(oEvent.getSource().getItems());
					}
					// "ALL deselected"
					else {
						oEvent.getSource().removeAllSelectedItems();
						fld15 = 0;
						return fld15;
					}
				} else {
					if (oEvent.getSource().getSelectedKeys().indexOf(oEvent.getParameters().changedItem.getProperty("key")) === -1) {
						oEvent.getSource().removeSelectedKeys(["0"]);
					}
				}
				var selectedItems = oEvent.getSource().getSelectedItems();
				if (selectedItems.length === oEvent.getSource().getItems().length - 1 && oEvent.getSource().getSelectedKeys().indexOf("0") ===
					-1) {
					oEvent.getSource().addSelectedKeys("0");
				}
			}
			return fld15;
		},

		/**
		 * Transforms a date to format MM/dd/yy hh:mm:ssaa
		 * @function formatDate
		 * @param {string} sDate - the date to format
		 * @returns The formatted date
		 */
		formatDate: function (dateUTCString) {
			if (dateUTCString === "UNCONFIRMED") {
				return dateUTCString;
			} else if (!dateUTCString) {
				return "N/A";
			} else {
				var newdate = new Date(dateUTCString);
				var localDate = new Date(newdate.valueOf() - (newdate.getTimezoneOffset()) * 60000);
				var options = {
					year: "numeric",
					month: "2-digit",
					day: "2-digit",
					hour: "2-digit",
					minute: "2-digit",
					second: "2-digit"
				};
				var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
				var datestring = localDate.toLocaleTimeString(oLocale, options);
				return datestring;
			}
		},

		/**
		 * Transforms a date to format MM/dd/yy and matches the style of the Datepicker
		 * @function formatDate
		 * @param {string} sDate - the date to format
		 * @returns The formatted date
		 */
		formatDateForDATEPICKER: function (date) {
			if (date) {
				jQuery.sap.require("sap.ui.core.format.DateFormat");
				var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
					pattern: "MM/dd/yyyy"
				});
				return oDateFormat.format(new Date(date));
			}
		},

		/**
		 * Transforms a date to format MM/dd/yy hh:mm:ssaa
		 * @function formatDate
		 * @param {string} sDate - the date to format
		 * @returns The formatted date
		 */
		formatDateForPicker: function (dateUTCString) {
			if (dateUTCString === "UNCONFIRMED") {
				return "";
			} else if (!dateUTCString) {
				return "";
			} else {
				var newdate = new Date(dateUTCString);
				var localDate = new Date(newdate.valueOf() - (newdate.getTimezoneOffset()) * 60000);
				var options = {
					year: "numeric",
					month: "2-digit",
					day: "2-digit"
				};
				var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
				var datestring = localDate.toLocaleDateString(oLocale, options);
				return datestring;
			}
		},

		/**
		 * Transforms a string in bolean for IgnoreSapIndustry checkbox
		 * @function formatTrueFalse
		 * @param {string} Ignore - "T" or "F"
		 * @returns a boolean for check box
		 */
		formatTrueFalse: function (value) {
			if (value === "T") {
				return true;
			} else {
				return false;
			}
		},

		/**
		 * Fired when the user confirms he wants to create a new card.
		 * Retrieves the information from the form and saves the card to the database.
		 * If something goes wrong, shows a error message.
		 * If the card is successfully inserted, navs back to the main page.
		 * @function onCreate
		 * @param  {Object} _oController - controller of page where the card is being created
		 */
		onCreate: function (_oController) {

			var controller = this;
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			msgdialog.setBusyIndicatorDelay(0);
			msgdialog.setBusy(true);

			// Get the Local Model
			var oLocalData = _oController.getView().getModel("local").getData();
			// Start of Form Model
			var oEntry = {};
			oEntry.name = oLocalData.name;
			oEntry.status = oLocalData.status;
			oEntry.section = oLocalData.section;
			oEntry.cardType = oLocalData.cardType;
			if (oEntry.cardType === "900") {
				if (oEntry.userLevel) {
					oEntry.userLevel = _oController._configArrayToString(oLocalData.UserLevel);
				}
				oEntry.isAdminChecked = _oController.configTrueFalseState(_oController.getView().byId("idAdminChecked").getSelected());
				oEntry.callToActionType = oLocalData.callToActionType;
				oEntry.displayStartDate = controller.parseDisplayDatesForAjax(new Date());
			} else {
				oEntry.cardCategory = oLocalData.cardCategory;
				oEntry.displayStartDate = controller.parseDisplayDatesForAjax(_oController.getView().byId("idDispSDate").getValue());
				oEntry.displayEndDate = controller.parseDisplayDatesForAjax(_oController.getView().byId("idDispEDate").getValue());
				oEntry.customerOverride = oLocalData.customerOverride;
				oEntry.isIgnoreSapIndustry = _oController.configTrueFalseState(_oController.getView().byId("idIgnoreIndustry").getSelected());
				oEntry.isExpressCare = _oController.getView().byId("idExpressCare").getSelected();
				if (oEntry.isExpressCare) {
					if (_oController.getView().byId("idExpressCareBranded").getSelected()) {
						oEntry.isBranded = "T";
					} else {
						oEntry.isBranded = "F";
					}
					if (_oController.getView().byId("idExpressCareNonBranded").getSelected()) {
						oEntry.isUnBranded = "T";
					} else {
						oEntry.isUnBranded = "F";
					}
					oEntry.isExpressCare = "T";
				} else {
					oEntry.isExpressCare = "F";
					oEntry.isBranded = "F";
					oEntry.isUnBranded = "F";
				}

				if (oLocalData.title !== undefined) {
					oEntry.title = oLocalData.title;
				} else {
					oEntry.title = "General Content";
				}
				oEntry.subtitle = oLocalData.subtitle;

				oEntry.shortText = oLocalData.shortText;
				oEntry.isBackToDash = "0"; //	oEntry.isBackToDash = oLocalData.backToDash; Disable BackToDash

			}

			if (_oController.getView().byId("idRoleBased")) {
				oEntry.isRoleBased = controller.configTrueFalseState(_oController.getView().byId("idRoleBased").getSelected());
			}
			oEntry.displayPriority = parseInt(oLocalData.displayPriority, 10);

			var productCodes = [];
			// Since this is only for the FEED card type, it must be validated
			if (_oController.getView().byId("idCPLChecked")) {
				oEntry.isCPLChecked = _oController.getView().byId("idCPLChecked").getSelected();
				if (oEntry.isCPLChecked) {
					oEntry.isCPLChecked = "T";
					productCodes = _oController.configProductCodes(_oController, "idProductCode");
				} else {
					oEntry.isCPLChecked = "F";
				}
			}

			oEntry.lang = _oController.getView().byId("idLanguage").getSelectedKey();
			oEntry.mediaURL = oLocalData.mediaURL;
			if (oLocalData.mediaType !== undefined) {
				oEntry.mediaType = oLocalData.mediaType.toString();
			}

			oEntry.callToActionLabel = oLocalData.callToActionLabel;
			oEntry.callToActionURL = oLocalData.callToActionURL;
			oEntry.urlTarget = oLocalData.urlTarget;
			// End of Form Model

			var cardSegments = _oController.configCardSegment(oLocalData.cardSegment);
			var customerGroup5 = _oController.configAccountProperties(oLocalData.customerGroup5, "customerGroup5Id");
			var salesDistrict = _oController.configAccountProperties(oLocalData.salesDistrict, "salesDistrictId");
			var salesOffice = _oController.configAccountProperties(oLocalData.salesOffice, "salesOfficeId");
			var salesOrg = _oController.configAccountProperties(oLocalData.salesOrg, "salesOrgId");
			var salesGroup = _oController.configAccountProperties(oLocalData.salesGroup, "salesGroupId");
			var shipToAccounts = _oController.configAccountProperties(oLocalData.shipToAccounts, "shipToId");
			var functionalRoles = [];
			if (oLocalData.role) {
				functionalRoles = _oController.configAccountProperties(typeof oLocalData.role === "object" ? oLocalData.role : [oLocalData.role],
					"functionalRoleValue");
			}
			if (oEntry.cardType === "900") {
				if (cardSegments.length > 0) {
					oEntry.isIgnoreSapIndustry = "F";
				} else {
					oEntry.isIgnoreSapIndustry = "T";
				}
			}
			var xsURL = "/core/contentCards";
			var body = {
				"cardCustomersGroup5": customerGroup5,
				"cardSalesDistrict": salesDistrict,
				"cardSalesOffice": salesOffice,
				"cardSalesOrg": salesOrg,
				"cardSalesGroup": salesGroup,
				"cardShipTo": shipToAccounts,
				"cardSegments": cardSegments,
				"cardProductCodes": productCodes,
				"cardFunctionalRole": functionalRoles,
				"contentCards": oEntry
			};

			_oController.handleAjaxJSONCall(_oController, false, xsURL, "POST", _oController.onCreateSuccess,
				_oController.onCreateError, body);

		},
		onCreateSuccess: function (_oController) {
			_oController.getRouter().navTo("cardManager");
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			msgdialog.setBusy(false);
			msgdialog.close();
			msgdialog.destroy();
		},
		onCreateError: function (_oController, oError) {
			var i18n = sap.ui.getCore().getModel("i18n").getResourceBundle();
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			var errRes = _oController.getError(oError);
			MessageBox.error(i18n.getText("REQErrorTXT") + " " + errRes);
			msgdialog.setBusy(false);

		},

		/**
		 * Fired when the user confirms he wants to update a card.
		 * Retrieves the information from the form and saves the card to the database.
		 * If something goes wrong, shows a error message.
		 * If the card is successfully inserted, navs back to the main page.
		 * @function onUpdate
		 * @param  {Object} _oController - controller of page where the card is being created
		 */
		onUpdate: function (_oController) {
			var controller = this;
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			msgdialog.setBusyIndicatorDelay(0);
			msgdialog.setBusy(true);
			_oController.getView().getModel("local").setProperty("/backToDash", "0");
			var oLocalData = _oController.getView().getModel("local").getData();

			// Start of Form Model
			var oEntry = {};
			oEntry.name = oLocalData.name;
			oEntry.status = oLocalData.status;
			oEntry.section = oLocalData.section;
			oEntry.cardType = oLocalData.cardType;

			if (oEntry.cardType === "900") {
				oEntry.userLevel = _oController._configArrayToString(oLocalData.UserLevel);
				oEntry.isAdminChecked = _oController.configTrueFalseState(_oController.getView().byId("idAdminChecked").getSelected());
				oEntry.callToActionType = oLocalData.callToActionType;
				oEntry.displayStartDate = controller.parseDisplayDatesForAjax(new Date());
			} else {
				oEntry.cardCategory = oLocalData.cardCategory;
				oEntry.displayStartDate = controller.parseDisplayDatesForAjax(_oController.getView().byId("idDispSDate").getValue());
				oEntry.displayEndDate = controller.parseDisplayDatesForAjax(_oController.getView().byId("idDispEDate").getValue());
				oEntry.customerOverride = oLocalData.customerOverride;
				oEntry.isIgnoreSapIndustry = _oController.configTrueFalseState(_oController.getView().byId("idIgnoreIndustry").getSelected());
				oEntry.isExpressCare = _oController.getView().byId("idExpressCare").getSelected();
				if (oEntry.isExpressCare) {
					if (_oController.getView().byId("idExpressCareBranded").getSelected()) {
						oEntry.isBranded = "T";
					} else {
						oEntry.isBranded = "F";
					}
					if (_oController.getView().byId("idExpressCareNonBranded").getSelected()) {
						oEntry.isUnBranded = "T";
					} else {
						oEntry.isUnBranded = "F";
					}
					oEntry.isExpressCare = "T";
				} else {
					oEntry.isExpressCare = "F";
					oEntry.isBranded = "F";
					oEntry.isUnBranded = "F";
				}

				if (oLocalData.title !== undefined) {
					oEntry.title = oLocalData.title;
				} else {
					oEntry.title = "General Content";
				}
				oEntry.subtitle = oLocalData.subtitle;

				oEntry.shortText = oLocalData.shortText;
				oEntry.isBackToDash = "0"; //	oEntry.isBackToDash = oLocalData.backToDash; Disable BackToDash

			}
			// Since this is only for the FEED card type, it must be validated
			if (_oController.getView().byId("idRoleBased")) {
				oEntry.isRoleBased = controller.configTrueFalseState(_oController.getView().byId("idRoleBased").getSelected());
			}
			oEntry.displayPriority = parseInt(oLocalData.displayPriority, 10);

			var productCodes = [];
			// Since this is only for the FEED card type, it must be validated
			if (_oController.getView().byId("idCPLChecked")) {
				oEntry.isCPLChecked = _oController.getView().byId("idCPLChecked").getSelected();
				if (oEntry.isCPLChecked) {
					oEntry.isCPLChecked = "T";
					productCodes = _oController.configProductCodes(_oController, "idProductCode");
				} else {
					oEntry.isCPLChecked = "F";
				}
			}
			oEntry.lang = _oController.getView().byId("idLanguage").getSelectedKey();
			oEntry.mediaURL = oLocalData.mediaURL;
			if (oLocalData.mediaType !== undefined) {
				oEntry.mediaType = oLocalData.mediaType.toString();
			}
			oEntry.callToActionLabel = oLocalData.callToActionLabel;
			oEntry.callToActionURL = oLocalData.callToActionURL;
			oEntry.urlTarget = oLocalData.urlTarget;
			// End of Form Model

			var cardSegments = _oController.configCardSegment(oLocalData.cardSegment, oLocalData.id);
			var customerGroup5 = _oController.configAccountProperties(oLocalData.customerGroup5, "customerGroup5Id");
			var salesDistrict = _oController.configAccountProperties(oLocalData.salesDistrict, "salesDistrictId");
			var salesOffice = _oController.configAccountProperties(oLocalData.salesOffice, "salesOfficeId");
			var salesOrg = _oController.configAccountProperties(oLocalData.salesOrg, "salesOrgId");
			var salesGroup = _oController.configAccountProperties(oLocalData.salesGroup, "salesGroupId");
			var shipToAccounts = _oController.configAccountProperties(oLocalData.shipToAccounts, "shipToId");
			var functionalRoles = [];
			if (oLocalData.role) {
				functionalRoles = _oController.configAccountProperties(typeof oLocalData.role === "object" ? oLocalData.role : [oLocalData.role],
					"functionalRoleValue");
			}
			if (oEntry.cardType === "900") {
				if (cardSegments.length > 0) {
					oEntry.isIgnoreSapIndustry = "F";
				} else {
					oEntry.isIgnoreSapIndustry = "T";
				}
			}
			var body;
			if (oEntry.isIgnoreSapIndustry === "T") {
				body = {
					"cardSegments": [],
					"cardCustomersGroup5": customerGroup5,
					"cardSalesDistrict": salesDistrict,
					"cardSalesOffice": salesOffice,
					"cardSalesOrg": salesOrg,
					"cardSalesGroup": salesGroup,
					"cardShipTo": shipToAccounts,
					"cardProductCodes": productCodes,
					"cardFunctionalRole": functionalRoles,
					"contentCards": oEntry
				};
			} else {
				body = {
					"cardSegments": cardSegments,
					"cardCustomersGroup5": customerGroup5,
					"cardSalesDistrict": salesDistrict,
					"cardSalesOffice": salesOffice,
					"cardSalesOrg": salesOrg,
					"cardSalesGroup": salesGroup,
					"cardShipTo": shipToAccounts,
					"cardProductCodes": productCodes,
					"cardFunctionalRole": functionalRoles,
					"contentCards": oEntry
				};
			}

			var xsURL = "/core/contentCards/" + oLocalData.id;
			_oController.handleAjaxJSONCall(_oController, false, xsURL, "PUT", _oController.onUpdateSuccess,
				_oController.onUpdateError, body);
		},
		onUpdateSuccess: function (_oController) {
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			_oController.getRouter().navTo("cardManager");
			msgdialog.setBusy(false);
			msgdialog.close();
			msgdialog.destroy();
		},
		onUpdateError: function (_oController, oError) {
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			var i18n = sap.ui.getCore().getModel("i18n").getResourceBundle();
			var errRes = _oController.getError(oError);
			MessageBox.error(i18n.getText("REQErrorTXT") + " " + errRes);
			msgdialog.setBusy(false);
		},

		/**
		 * Fired when the user confirms he wants to create a new carousel card.
		 * Retrieves the information from the form and saves the card to the database.
		 * If something goes wrong, shows a error message.
		 * If the card is successfully inserted, navs back to the main page.
		 * @function onCreateCarousel
		 * @param {Object} oEntry
		 * @param {Object} oMediaMdlFragData
		 */
		onCreateCarousel: function (_oController, _MediaMdl, cardType) {
			var controller = this;
			_oController.getView().getModel("local").setProperty("/backToDash", "0");
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			msgdialog.setBusyIndicatorDelay(0);
			msgdialog.setBusy(true);
			var oMediaMdlData = _MediaMdl.getData();
			var oMediaMdlFragData = _MediaMdl.getProperty("/FragmentsToAdd");

			var DisplayStartDate = controller.parseDisplayDatesForAjax(_oController.getView().byId("idDispSDate").getValue());
			var DisplayEndDate = controller.parseDisplayDatesForAjax(_oController.getView().byId("idDispEDate").getValue());

			var sections = [];
			var i = 0;
			for (var x in oMediaMdlFragData) {

				var oEntry = {};
				oEntry.title = oMediaMdlData.title;
				oEntry.name = (i === 0) ? oMediaMdlData.name : oMediaMdlData.name + i;
				oEntry.status = oMediaMdlData.status;
				oEntry.displayPriority = parseInt(oMediaMdlData.displayPriority, 10);
				oEntry.displayStartDate = DisplayStartDate;
				oEntry.displayEndDate = DisplayEndDate;
				oEntry.lang = _oController.getView().byId("idLanguage").getSelectedKey();

				oEntry.cardType = cardType;
				oEntry.cardCategory = null;
				oEntry.section = oMediaMdlData.section;
				oEntry.subtitle = oMediaMdlFragData[x].subtitle;
				oEntry.shortText = oMediaMdlFragData[x].shortText;
				oEntry.callToActionLabel = oMediaMdlFragData[x].callToActionLabel;
				oEntry.callToActionURL = oMediaMdlFragData[x].callToActionURL;
				oEntry.urlTarget = oMediaMdlFragData[x].urlTarget;
				oEntry.isBackToDash = oMediaMdlFragData[x].backToDash;
				oEntry.customerOverride = oMediaMdlData.customerOverride;
				oEntry.isExpressCare = _oController.getView().byId("idExpressCare").getSelected();
				if (oEntry.isExpressCare) {
					if (_oController.getView().byId("idExpressCareBranded").getSelected()) {
						oEntry.isBranded = "T";
					} else {
						oEntry.isBranded = "F";
					}
					if (_oController.getView().byId("idExpressCareNonBranded").getSelected()) {
						oEntry.isUnBranded = "T";
					} else {
						oEntry.isUnBranded = "F";
					}
					oEntry.isExpressCare = "T";
				} else {
					oEntry.isExpressCare = "F";
					oEntry.isBranded = "F";
					oEntry.isUnBranded = "F";
				}
				oEntry.isIgnoreSapIndustry = _oController.configTrueFalseState(_oController.getView().byId("idIgnoreIndustry").getSelected());
				oEntry.mediaURL = oMediaMdlFragData[x].mediaURL;
				if (oMediaMdlFragData[x].mediaType !== undefined) {
					oEntry.mediaType = oMediaMdlFragData[x].mediaType.toString();
				}
				i++;

				sections.push(oEntry);
			}

			var cardSegments = _oController.configCardSegment(oMediaMdlData.cardSegment);
			var body = [];
			for (i = 0; i < sections.length; i++) {
				body.push({
					"cardSegments": cardSegments,
					"contentCards": sections[i]
				});
			}
			var xsURL = "/core/contentCardsGroup";
			_oController.handleAjaxJSONCall(_oController, false, xsURL, "POST", _oController.onCreateCarouselSuccess,
				_oController.onCreateCarouselError, body);
		},
		onCreateCarouselSuccess: function (oController) {
			var _oCarousel = oController.getView().byId("idCarousel");
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			if (msgdialog.isOpen()) {
				oController.getRouter().navTo("cardManager");
				msgdialog.setBusy(false);
				msgdialog.close();
				msgdialog.destroy();
			}
			_oCarousel.removeAllPages();
		},
		onCreateCarouselError: function (oController, oError) {
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			var i18n = sap.ui.getCore().getModel("i18n").getResourceBundle();
			var errRes = oController.getError(oError);
			MessageBox.error(i18n.getText("REQErrorTXT") + " " + errRes);
			msgdialog.setBusy(false);
		},

		/**
		 * Update Carousel Card.
		 * @function onUpdateCarousel
		 * @param {Object} oEntry
		 * @param {Object} oMediaMdlFragData
		 */
		onUpdateCarousel: function (_oController, _MediaMdl) {
			var controller = this;
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			_oController.getView().getModel("local").setProperty("/backToDash", "0");
			msgdialog.setBusyIndicatorDelay(0);
			msgdialog.setBusy(true);
			var oMediaMdlData = _MediaMdl.getData();
			var oMediaMdlFragData = _MediaMdl.getProperty("/Fragments");
			var DisplayStartDate = controller.parseDisplayDatesForAjax(_oController.getView().byId("idDispSDate").getValue());
			var DisplayEndDate = controller.parseDisplayDatesForAjax(_oController.getView().byId("idDispEDate").getValue());
			var aSections = [];
			var i = 0;

			for (var x in oMediaMdlFragData) {
				var oEntry = {};
				oEntry.id = oMediaMdlFragData[x].id;
				oEntry.cardGroup = oMediaMdlFragData[x].cardGroup;
				oEntry.title = oMediaMdlData.title;
				oEntry.name = oMediaMdlFragData[x].name;
				oEntry.status = oMediaMdlData.status;
				oEntry.displayPriority = parseInt(oMediaMdlData.displayPriority, 10);
				oEntry.displayStartDate = DisplayStartDate;
				oEntry.displayEndDate = DisplayEndDate;
				oEntry.lang = _oController.getView().byId("idLanguage").getSelectedKey();

				oEntry.cardType = oMediaMdlData.cardType;
				oEntry.cardCategory = null;
				oEntry.section = oMediaMdlData.section;
				oEntry.subtitle = oMediaMdlFragData[x].subtitle;
				oEntry.shortText = oMediaMdlFragData[x].shortText;
				oEntry.callToActionLabel = oMediaMdlFragData[x].callToActionLabel;
				oEntry.callToActionURL = oMediaMdlFragData[x].callToActionURL;
				oEntry.urlTarget = oMediaMdlFragData[x].urlTarget;
				oEntry.isBackToDash = oMediaMdlFragData[x].backToDash;
				oEntry.customerOverride = oMediaMdlData.customerOverride;
				oEntry.isExpressCare = _oController.getView().byId("idExpressCare").getSelected();
				if (oEntry.isExpressCare) {
					if (_oController.getView().byId("idExpressCareBranded").getSelected()) {
						oEntry.isBranded = "T";
					} else {
						oEntry.isBranded = "F";
					}
					if (_oController.getView().byId("idExpressCareNonBranded").getSelected()) {
						oEntry.isUnBranded = "T";
					} else {
						oEntry.isUnBranded = "F";
					}
					oEntry.isExpressCare = "T";
				} else {
					oEntry.isExpressCare = "F";
					oEntry.isBranded = "F";
					oEntry.isUnBranded = "F";
				}
				oEntry.isIgnoreSapIndustry = _oController.configTrueFalseState(_oController.getView().byId("idIgnoreIndustry").getSelected());
				oEntry.mediaURL = oMediaMdlFragData[x].mediaURL;
				if (oMediaMdlFragData[x].mediaType !== undefined) {
					oEntry.mediaType = oMediaMdlFragData[x].mediaType.toString();
				}
				i++;
				aSections.push(oEntry);
			}

			var cardSegments = _oController.configCardSegment(oMediaMdlData.cardSegment, oMediaMdlData.id);
			var xsURL;
			var body;
			_oController._newSectionToAddFlag = _MediaMdl.getProperty("/FragmentsToAdd") && _MediaMdl.getProperty("/FragmentsToAdd").length > 0 ?
				true : false;
			_oController._updateCallCount = 0;
			_oController._updateTotalCount = aSections.length;
			for (i = 0; i < aSections.length; i++) {
				if (oEntry && oEntry.isIgnoreSapIndustry === "T") {
					body = {
						"cardSegments": [],
						"contentCards": aSections[i]
					};
				} else {
					body = {
						"cardSegments": cardSegments,
						"contentCards": aSections[i]
					};
				}

				xsURL = "/core/contentCards/" + aSections[i].id;

				_oController.handleAjaxJSONCall(_oController, false, xsURL, "PUT", _oController.onUpdateCarouselSuccess,
					_oController.onUpdateCarouselError, body, aSections);
			}
			if (_oController._newSectionToAddFlag) {
				_oController.onUpdatePostCarousel(_oController, _MediaMdl, aSections);
			}

		},

		onUpdatePostCarousel: function (_oController, _MediaMdl, aSections) {
			var oTextMdlFragDataA = _MediaMdl.getProperty("/FragmentsToAdd");
			var idEncrypt = aSections.length;
			var cardGroup = aSections[0].cardGroup;
			var oMediaMdlData = _MediaMdl.getData();
			var DisplayStartDate = _oController.parseDisplayDatesForAjax(_oController.getView().byId("idDispSDate").getValue());
			var DisplayEndDate = _oController.parseDisplayDatesForAjax(_oController.getView().byId("idDispEDate").getValue());
			var i = 0;
			var aSectionsToAdd = [];
			if (!cardGroup) {
				return;
			}
			for (var x in oTextMdlFragDataA) {

				var oEntry = {};
				oEntry.cardGroup = cardGroup;
				oEntry.title = oMediaMdlData.title;
				oEntry.name = oMediaMdlData.name + (parseInt(x, 10) + idEncrypt);
				oEntry.status = oMediaMdlData.status;
				oEntry.displayPriority = parseInt(oMediaMdlData.displayPriority, 10);
				oEntry.displayStartDate = DisplayStartDate;
				oEntry.displayEndDate = DisplayEndDate;
				oEntry.lang = _oController.getView().byId("idLanguage").getSelectedKey();
				oEntry.cardType = oMediaMdlData.cardType;
				oEntry.cardCategory = null;
				oEntry.section = oMediaMdlData.section;
				oEntry.subtitle = oTextMdlFragDataA[x].subtitle;
				oEntry.shortText = oTextMdlFragDataA[x].shortText;
				oEntry.callToActionLabel = oTextMdlFragDataA[x].callToActionLabel;
				oEntry.callToActionURL = oTextMdlFragDataA[x].callToActionURL;
				oEntry.urlTarget = oTextMdlFragDataA[x].urlTarget;
				oEntry.isBackToDash = oTextMdlFragDataA[x].backToDash;
				oEntry.customerOverride = oMediaMdlData.customerOverride;
				oEntry.isIgnoreSapIndustry = _oController.configTrueFalseState(_oController.getView().byId("idIgnoreIndustry").getSelected());
				oEntry.isExpressCare = _oController.getView().byId("idExpressCare").getSelected();
				if (oEntry.isExpressCare) {
					if (_oController.getView().byId("idExpressCareBranded").getSelected()) {
						oEntry.isBranded = "T";
					} else {
						oEntry.isBranded = "F";
					}
					if (_oController.getView().byId("idExpressCareNonBranded").getSelected()) {
						oEntry.isUnBranded = "T";
					} else {
						oEntry.isUnBranded = "F";
					}
					oEntry.isExpressCare = "T";
				} else {
					oEntry.isExpressCare = "F";
					oEntry.isBranded = "F";
					oEntry.isUnBranded = "F";
				}
				oEntry.cardSegment = oMediaMdlData.cardSegment;
				oEntry.mediaURL = oTextMdlFragDataA[x].mediaURL;
				if (oTextMdlFragDataA[x].mediaType !== undefined) {
					oEntry.mediaType = oTextMdlFragDataA[x].mediaType.toString();
				}
				i++;

				aSectionsToAdd.push(oEntry);
			}
			var body;
			var xsURL;
			var cardSegments = _oController.configCardSegment(oMediaMdlData.cardSegment);
			_oController._postCarouselCalls = aSectionsToAdd.length;
			_oController._postCarouselCallsExecuted = 0;
			if (aSectionsToAdd.length > 0) {
				for (i = 0; i < aSectionsToAdd.length; i++) {
					body = {
						"cardSegments": cardSegments,
						"contentCards": aSectionsToAdd[i]

					};

					xsURL = "/core/contentCards/";
					_oController.handleAjaxJSONCall(_oController, false, xsURL, "POST", _oController.onUpdateCarouselPostSuccess,
						_oController.onUpdateCarouselPostError, body, aSectionsToAdd);

				}
			}
		},

		onUpdateCarouselSuccess: function (oController) {
			oController._updateCallCount += 1;
			if (!oController._newSectionToAddFlag) {
				if (oController._updateCallCount === oController._updateTotalCount) {
					var _oCarousel = oController.getView().byId("idCarousel");
					var msgdialog = sap.ui.getCore().byId("msgdialog");
					if (msgdialog.isOpen()) {
						msgdialog.setBusy(false);
						msgdialog.close();
						msgdialog.destroy();
					}
					_oCarousel.removeAllPages();
					oController.getRouter().navTo("cardManager");
				}
			}
		},

		onUpdateCarouselError: function (oController, oError) {
			var i18n = sap.ui.getCore().getModel("i18n").getResourceBundle();
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			var errRes = oController.getError(oError);
			MessageBox.error(i18n.getText("REQErrorTXT") + " " + errRes);
			msgdialog.setBusy(false);
		},

		onUpdateCarouselPostSuccess: function (oController, oDataToSend, oPiggyBack) {
			oController._postCarouselCallsExecuted += 1;
			if (oController._postCarouselCallsExecuted === oController._postCarouselCalls) {
				var _oCarousel = oController.getView().byId("idCarousel");
				var msgdialog = sap.ui.getCore().byId("msgdialog");
				if (msgdialog.isOpen()) {
					msgdialog.setBusy(false);
					msgdialog.close();
					msgdialog.destroy();
				}
				_oCarousel.removeAllPages();
				oController.getRouter().navTo("cardManager");
			}
		},

		onUpdateCarouselPostError: function (controller, oError) {
			var i18n = sap.ui.getCore().getModel("i18n").getResourceBundle();
			var msgdialog = sap.ui.getCore().byId("msgdialog");
			msgdialog.setBusy(false);
			var errRes = controller.getError(oError);
			MessageBox.error(i18n.getText("REQErrorTXT") + " " + errRes);
		},

		/**
		 * Converts the data to the formated needed to send the request
		 * @ parameter (displayDate)
		 * return parsedDate
		 */
		parseDisplayDatesForAjax: function (displayDate) {
			var converted = new Date(displayDate);
			var parsedDate = converted.getFullYear() + "-" + (converted.getMonth() + 1) + "-" +
				converted.getDate();
			return parsedDate;
		},

		/**
		 * Checks if the begin date is before the end date, if it isn't sets a red warning on the dates box
		 * @function checkDateStauts
		 */
		checkDateStatus: function (controller, oEvent) {
			var DisplayStartDate = new Date(controller.getView().byId("idDispSDate").getValue());
			var DisplayEndDate = new Date(controller.getView().byId("idDispEDate").getValue());

			var changedPicker = oEvent.getSource();
			var changedPickerValue = new Date(oEvent.getParameter("value"));

			if (isNaN(changedPickerValue)) {
				changedPicker.setValueState(sap.ui.core.ValueState.Error);
				return false;
			} else {
				if (isNaN(DisplayStartDate) !== true || isNaN(DisplayEndDate) !== true) {
					if (DisplayStartDate <= DisplayEndDate) {
						controller.getView().byId("idDispSDate").setValueState(sap.ui.core.ValueState.None);
						controller.getView().byId("idDispEDate").setValueState(sap.ui.core.ValueState.None);
						return true;
					} else { // DisplayStartDate > DisplayEndDate
						if (isNaN(DisplayStartDate) !== true && isNaN(DisplayEndDate) !== true) {
							controller.getView().byId("idDispSDate").setValueState(sap.ui.core.ValueState.Error);
						}
						return false;
					}
				} else {
					return true;
				}
			}
		},

		/**
		 * Handle Express Care
		 * @ parameter (oEvent, controller)
		 */
		handleExpressCare: function (oEvent, controller) {
			var fld;
			controller.byId("idIgnoreIndustry").setSelected(false);
			var salesOfficeSelected = this.getView().byId("idSalesOffice").getSelectedKeys();
			if (salesOfficeSelected.length > 0) {
				fld = -1;
			} else {
				if (oEvent.getSource().getSelected()) {
					fld = 1;
				} else {
					fld = 0;
				}

			}

			return fld;
		},

		/**
		 * Handle Date Change
		 * @ parameter (oEvent, controller)
		 */
		handleDateChange: function (oEvent, controller) {
			var newValue = oEvent.getParameter("value");
			var fld;
			if (newValue === "") {
				fld = 0;
			} else {
				var oDP = oEvent.getSource();
				var bValid = oEvent.getParameter("valid");
				if (bValid) {
					if (!controller.checkDateStatus(controller, oEvent)) {
						fld = 0;
					} else {
						fld = 1;
						oDP.setValueState(sap.ui.core.ValueState.None);
					}
				} else {
					oDP.setValueState(sap.ui.core.ValueState.Error);
					fld = 0;
				}
			}
			return fld;
		},

		/**
		 * handleSapIndustryKeyChange (oEvent)
		 * @ parameter (oEvent, controller)
		 */
		handleSapIndustryKeyChange: function (oEvent, controller) {
			var segSelected = this.getView().byId("idSegment").getSelectedKeys();
			var newValue = oEvent.getSource().getSelected();
			if (segSelected.length > 0 && controller.getView().byId("idIgnoreIndustry").getSelected() || controller.getView().byId(
					"idExpressCare").getSelected()) {
				return -1;
			} else {
				if (newValue) {
					return 1;
				} else {
					return 0;
				}
			}
		},

		/**
		 * handlSalesOfficeChange (oEvent)
		 * @ parameter (oEvent, controller)
		 */
		handlSalesOfficeChange: function (oEvent, controller) {
			var expressCareSelected = this.getView().byId("idExpressCare").getSelected();

			var selectedSalesOffice = this.checkAccountPropertiesField(oEvent, this);
			if (expressCareSelected) {
				return -1;
			} else {
				if (selectedSalesOffice > 0) {
					return 1;
				} else {
					return 0;
				}
			}
		},

		/**
		 * handlSoldToChange (oEvent)
		 * @ parameter (oEvent, controller)
		 */
		handlSoldToChange: function (oEvent, controller) {
			var segSelected = this.getView().byId("idSegment").getSelectedKeys();
			var ignoreIndustrySelected = this.getView().byId("idIgnoreIndustry").getSelected();
			var expressCareSelected = this.getView().byId("idExpressCare").getSelected();
			var customerGroup5Selected = this.getView().byId("idCustomerGroup5").getSelectedKeys();
			var salesDistrictSelected = this.getView().byId("idSalesDistrict").getSelectedKeys();
			var salesOfficeSelected = this.getView().byId("idSalesOffice").getSelectedKeys();
			var salesOrgSelected = this.getView().byId("idSalesOrg").getSelectedKeys();
			var salesGroupSelected = this.getView().byId("idSalesGroup").getSelectedKeys();

			var newValue = oEvent.getSource().getSelected();
			if (segSelected.length > 0 || ignoreIndustrySelected || expressCareSelected || customerGroup5Selected.length > 0 ||
				salesDistrictSelected.length > 0 || salesOfficeSelected.length > 0 || salesOrgSelected.length > 0 || salesGroupSelected.length >
				0
			) {
				return -1;
			} else {
				if (newValue) {
					return 1;
				} else {
					return 0;
				}
			}
		},

		/**
		 * HandleLocationChange (oEvent)
		 * @ parameter (oEvent, controller)
		 */
		handleLocationChange: function (oEvent) {
			var isSelected = 0;
			var newValue = oEvent.getParameters("selectedItem");
			newValue = newValue.selectedItem.getKey();
			switch (newValue) {
			case "":
				break;
			case "10000":
				isSelected = "10000";
				break;
			default:
				isSelected = 1;
				break;
			}
			return isSelected;
		},

		/**
		 * Calls service to get the predefined authorizations
		 * @function getPredefinedAuth
		 * @param {object} controller - controller of page calling the service
		 */
		getPredefinedAuth: function (controller) {
			var xsURL = "/core/predefinedAuth";
			controller.handleAjaxJSONCall(controller, false, xsURL, "GET", controller.getPredefinedAuthSuccess,
				controller.getPredefinedAuthError);
		},

		/**
		 * Success Callback to get the predefined authorizations
		 * @function getPredefinedAuthSuccess
		 * @param {object} controller - controller of page calling the service
		 * @param {object} oData - Data returned by the service
		 */
		getPredefinedAuthSuccess: function (controller, oData) {
			var oPredefinedAuthModel = new sap.ui.model.json.JSONModel();
			oPredefinedAuthModel.setData(oData);
			controller.getView().setModel(oPredefinedAuthModel, "predefinedAuth");
		},

		/**
		 * Error Callback to get the predefined authorizations
		 * @function getPredefinedAuthError
		 * @param {object} controller - controller of page calling the service
		 * @param {object} oError - Error returned by the server
		 */
		getPredefinedAuthError: function (controller, oError) {
			// show the error message from server
			controller.getError(oError);
		},

		/**
		 * Calls service to get the account properties
		 * @function getAccountProperties
		 * @param {object} controller - controller of page calling the service
		 */
		getAccountProperties: function (controller, contentCardId) {
			var xsURL = "/core/accountProperties";
			controller.contentCardId = contentCardId;
			controller.handleAjaxJSONCall(controller, false, xsURL, "GET", controller.getAccountPropertiesSuccess,
				controller.getAccountPropertiesError);

		},

		getAccountPropertiesSuccess: function (controller, oData) {
			// Order Data Ascending
			var oAccountModel = new sap.ui.model.json.JSONModel();
			var oAccountData = {};
			var aCustomerGroup5, aSalesDistrict, aSalesGroup, aSalesOffice, aSalesOrg;
			var oCustomerGroup5All, oSalesDistrictAll, oSalesGroupAll, oSalesOfficeAll, oSalesOrgAll;

			aCustomerGroup5 = controller.onFilteroModel(oData.customerGroup5, "ascending", "sapcustomergroup5desc");
			aSalesDistrict = controller.onFilteroModel(oData.salesDistrict, "ascending", "sapsalesDistrictdesc");
			aSalesGroup = controller.onFilteroModel(oData.salesGroup, "ascending", "sapsalesGroupdesc");
			aSalesOffice = controller.onFilteroModel(oData.salesOffice, "ascending", "sapsalesofficedesc");
			aSalesOrg = controller.onFilteroModel(oData.salesOrg, "ascending", "sapsalesorgdesc");

			var countCustomerGroup5 = 0;
			var countSalesDistrict = 0;
			var countSalesGroup = 0;
			var countSalesOffice = 0;
			var countSalesOrg = 0;
			var dataCustomerGroup5 = [];
			var dataSalesDistrict = [];
			var dataSalesGroup = [];
			var dataSalesOffice = [];
			var dataSalesOrg = [];
			if (controller.contentCardId !== "New") {
				dataCustomerGroup5 = sap.ui.getCore().getModel("local").getProperty("/ContentCardCustomersGroup5/" + controller.contentCardId);
				dataSalesDistrict = sap.ui.getCore().getModel("local").getProperty("/ContentCardSalesDistrict/" + controller.contentCardId);
				dataSalesGroup = sap.ui.getCore().getModel("local").getProperty("/ContentCardSalesGroup/" + controller.contentCardId);
				dataSalesOffice = sap.ui.getCore().getModel("local").getProperty("/ContentCardSalesOffice/" + controller.contentCardId);
				dataSalesOrg = sap.ui.getCore().getModel("local").getProperty("/ContentCardSalesOrg/" + controller.contentCardId);
			}
			var aCustomerGroup5List = [],
				aSalesDistrictList = [],
				aSalesGroupList = [],
				aSalesOfficeList = [],
				aSalesOrgList = [];
			var oCustomerGroup5, oSalesDistrict, oSalesGroup, oSalesOffice, oSalesOrg;

			for (var i = 0; i < aCustomerGroup5.length; i++) {
				oCustomerGroup5 = {
					customerGroup5Id: aCustomerGroup5[i].id,
					customerGroup5Name: aCustomerGroup5[i].sapcustomergroup5desc,
					isOfCard: false
				};
				if (dataCustomerGroup5.length) {
					for (var a = 0; a < dataCustomerGroup5.length; a++) {
						if (dataCustomerGroup5[a].customerGroup5Id === aCustomerGroup5[i].id) {
							controller.getView().byId("idCustomerGroup5").addSelectedKeys(aCustomerGroup5[i].id);
							countCustomerGroup5++;
							break;
						}
					}

				}
				aCustomerGroup5List.push(oCustomerGroup5);
			}
			for (var j = 0; j < aSalesDistrict.length; j++) {
				oSalesDistrict = {
					salesDistrictId: aSalesDistrict[j].id,
					salesDistrictName: aSalesDistrict[j].sapsalesDistrictdesc,
					isOfCard: false
				};
				if (dataSalesDistrict.length) {
					for (var b = 0; b < dataSalesDistrict.length; b++) {
						if (dataSalesDistrict[b].salesDistrictId === aSalesDistrict[j].id) {
							controller.getView().byId("idSalesDistrict").addSelectedKeys(aSalesDistrict[j].id);
							countSalesDistrict++;
							break;
						}
					}
				}
				aSalesDistrictList.push(oSalesDistrict);
			}
			for (var k = 0; k < aSalesGroup.length; k++) {
				oSalesGroup = {
					salesGroupId: aSalesGroup[k].id,
					salesGroupName: aSalesGroup[k].sapsalesGroupdesc,
					isOfCard: false
				};
				if (dataSalesGroup.length) {
					for (var c = 0; c < dataSalesGroup.length; c++) {
						if (dataSalesGroup[c].salesGroupId === aSalesGroup[k].id) {
							controller.getView().byId("idSalesGroup").addSelectedKeys(aSalesGroup[k].id);
							countSalesGroup++;
							break;
						}
					}
				}
				aSalesGroupList.push(oSalesGroup);
			}
			for (var l = 0; l < aSalesOffice.length; l++) {
				oSalesOffice = {
					salesOfficeId: aSalesOffice[l].id,
					salesOfficeName: aSalesOffice[l].sapsalesofficedesc,
					isOfCard: false
				};
				if (dataSalesOffice.length) {
					for (var d = 0; d < dataSalesOffice.length; d++) {
						if (dataSalesOffice[d].salesOfficeId === aSalesOffice[l].id) {
							controller.getView().byId("idSalesOffice").addSelectedKeys(aSalesOffice[l].id);
							countSalesOffice++;
							break;
						}
					}
				}
				aSalesOfficeList.push(oSalesOffice);
			}
			for (var m = 0; m < aSalesOrg.length; m++) {
				oSalesOrg = {
					salesOrgId: aSalesOrg[m].id,
					salesOrgName: aSalesOrg[m].sapsalesorgdesc,
					isOfCard: false
				};
				if (dataSalesOrg.length) {
					for (var e = 0; e < dataSalesOrg.length; e++) {
						if (dataSalesOrg[e].salesOrgId === aSalesOrg[m].id) {
							controller.getView().byId("idSalesOrg").addSelectedKeys(aSalesOrg[m].id);
							countSalesOrg++;
							break;
						}
					}
				}
				aSalesOrgList.push(oSalesOrg);
			}

			oCustomerGroup5All = {
				customerGroup5Id: "0",
				customerGroup5Name: "ALL",
				isOfCard: false
			};
			oSalesDistrictAll = {
				salesDistrictId: "0",
				salesDistrictName: "ALL",
				isOfCard: false
			};
			oSalesGroupAll = {
				salesGroupId: "0",
				salesGroupName: "ALL",
				isOfCard: false
			};
			oSalesOfficeAll = {
				salesOfficeId: "0",
				salesOfficeName: "ALL",
				isOfCard: false
			};
			oSalesOrgAll = {
				salesOrgId: "0",
				salesOrgName: "ALL",
				isOfCard: false
			};

			aCustomerGroup5List.unshift(oCustomerGroup5All);
			aSalesDistrictList.unshift(oSalesDistrictAll);
			aSalesGroupList.unshift(oSalesGroupAll);
			aSalesOfficeList.unshift(oSalesOfficeAll);
			aSalesOrgList.unshift(oSalesOrgAll);

			if (countCustomerGroup5 === aCustomerGroup5List.length - 1) {
				controller.getView().byId("idCustomerGroup5").addSelectedKeys("0");
			}
			if (countSalesDistrict === aSalesDistrictList.length - 1) {
				controller.getView().byId("idSalesDistrict").addSelectedKeys("0");
			}
			if (countSalesGroup === aSalesGroupList.length - 1) {
				controller.getView().byId("idSalesGroup").addSelectedKeys("0");
			}
			if (countSalesOffice === aSalesOfficeList.length - 1) {
				controller.getView().byId("idSalesOffice").addSelectedKeys("0");
			}
			if (countSalesOrg === aSalesOrgList.length - 1) {
				controller.getView().byId("idSalesOrg").addSelectedKeys("0");
			}

			oAccountData.customerGroup5 = aCustomerGroup5List;
			oAccountData.salesDistrict = aSalesDistrictList;
			oAccountData.salesGroup = aSalesGroupList;
			oAccountData.salesOffice = aSalesOfficeList;
			oAccountData.salesOrg = aSalesOrgList;

			oAccountModel.setData(oAccountData);
			oAccountModel.setSizeLimit(Math.max(aCustomerGroup5List.length, aSalesDistrictList.length, aSalesGroupList.length,
				aSalesOfficeList.length, aSalesOrgList.length));
			controller.getView().setModel(oAccountModel, "accountProperties");

		},
		getAccountPropertiesError: function (controller, oError) {
			// show the error message from server
			controller.getError(oError);
		},

		searchAccountsByQuery: function (controller, query) {
			var xsURL = "/usermgmt/searchAccounts?search=" + query;

			controller.handleAjaxJSONCall(controller, false, xsURL, "GET", controller.searchAccountsByQuerySuccess,
				controller.searchAccountsByQueryError);

		},

		searchAccountsByQuerySuccess: function (controller, oData) {
			var oAccountModel = new sap.ui.model.json.JSONModel();
			oData = controller.onFilteroModel(oData, "ascending", "accountname");
			oAccountModel.setData(oData);
			controller.getView().setModel(oAccountModel, "AccountAccess");
			controller.tablePaginationAccountAccess(oAccountModel);
		},
		searchAccountsByQueryError: function (controller, oError) {
			// show the error message from server
			controller.getError(oError);
		},

		/**
		 * Check the SAML session timeout
		 * @function checkSessionTimeout
		 * @returns Timeout state
		 */
		checkSessionTimeout: function () {
			var controller = this;
			var xsURL = "/core/serverTimestamp";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onCheckSessionTimeoutSuccess,
				controller.onCheckSessionTimeoutError);
		},

		onCheckSessionTimeoutSuccess: function (oController, oData, oPiggyBack) {
			//No action needed, timout would already be handled by the handleAjaxJSONCall
		},

		/**
		 * Function that handles the error case of the ajax call that checks the SAML session timeout
		 * @function onCheckSessionTimeoutError
		 */
		onCheckSessionTimeoutError: function (oController, oError, oPiggyBack) {
			//No action needed, timout would already be handled by the handleAjaxJSONCall
		},

		onGetServerTimeError: function (oController, oError, oPiggyBack) {
			return new Date();
		},

		/**
		 * Configure product code array from multiinput control
		 * @function configProductCodes
		 * @param controller
		 * @param controlID
		 */
		configProductCodes: function (controller, controlID) {
			var aTokens = controller.getView().byId(controlID) && controller.getView().byId(controlID).getTokens();
			var newCode;
			var productCodes = [];
			if (aTokens && aTokens.length > 0) {
				for (var i = 0; i < aTokens.length; i++) {
					newCode = {};
					newCode.productCode = aTokens[i].getProperty("text");
					productCodes.push(newCode);
				}
			}
			return productCodes;
		},

		/**
		 * Function to change status
		 * @function _processStatus
		 */
		_processStatus: function (oController) {
			var sSDate = oController.getView().byId("idDispSDate").getValue();
			var sEDate = oController.getView().byId("idDispEDate").getValue();
			var today = new Date();
			today.setHours(0, 0, 0, 0);
			var oStatusSelect = oController.getView().byId("idStatus");
			var oSDate, oEDate;
			var fld9 = 0;

			if (sSDate && sEDate) {
				oSDate = new Date(sSDate);
				oEDate = new Date(sEDate);
				if (oSDate > oEDate) {
					// status blank
					oStatusSelect.setSelectedKey("");
				} else if (today >= oSDate && today <= oEDate) {
					// status is active
					oStatusSelect.setSelectedKey("A");
					fld9 = 1;
				} else if (today > oEDate) {
					// status is expired
					oStatusSelect.setSelectedKey("I");
					fld9 = 1;
				} else {
					// status Pending
					oStatusSelect.setSelectedKey("P");
					fld9 = 1;
				}
			} else {
				// status blank
				oStatusSelect.setSelectedKey("");
			}
			return fld9;
		},

		/**
		 * Function to convert array to comma separated string
		 * @function _processStatus
		 */
		_configArrayToString: function (aData) {
			var sResult = "";
			if (!aData) {
				return sResult;
			}
			for (var i = 0; i < aData.length; i++) {
				if (i !== 0) {
					sResult += ",";
				}
				sResult += aData[i];
			}
			return sResult;
		}

	});
});