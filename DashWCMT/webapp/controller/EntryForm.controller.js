sap.ui.define([
	"sap/m/MessageBox",
	"valvoline/dash/DashWCMT/controller/Base.controller"
], function (MessageBox, Base) {
	"use strict";
	var _oRouter, i18n, _oController, res, action = "";
	var _bSearchAccountFlag = false;
	// Account Access Table Model
	var _oModelAccountAccessTable = new sap.ui.model.json.JSONModel();
	var _oModelAccountAccessSortedDataSaveOnDone = new sap.ui.model.json.JSONModel();
	var _iAccountAccessPageSize = 5,
		_beginAccountAccess = 0;
	var _oModelAccountAccessSortedData = new sap.ui.model.json.JSONModel();
	var fld1 = 0,
		fld2 = 0,
		fld3 = 0,
		fld4 = 0,
		fld5 = 0,
		fld6 = 0,
		fld7 = 0,
		fld8 = 0,
		fld9 = 0,
		fld10 = 0,
		fld11 = 0,
		fld12 = 0,
		fld13 = 0,
		fld14 = 0,
		fld15 = 0,
		fld15_1 = 0,
		fld15_2 = 0,
		fld15_3 = 0,
		fld15_4 = 0,
		fld15_5 = 0,
		fld15_6 = 0,
		fld15_7 = 0,
		fld15_8 = 0,
		fld15_9 = 0,
		fld15_10 = 0,
		fld16 = 0,
		fld16_1 = 0,
		fld16_2 = 0;

	return Base.extend("valvoline.dash.DashWCMT.controller.EntryForm", {
		/** @module EntryForm */
		/**
		 * Initializes the controller.
		 * Initializes the global variables.
		 * Attaches an event to everytime someone navs to the entry form page.
		 * Initializes local models, and gets core models and associates them to the view.
		 * Configures date pickers to not allow users to choose dates from the past
		 * @function onInit
		 */
		onInit: function () {
			_oController = this;
			_oRouter = this.getRouter();
			_oRouter.getRoute("entryform").attachPatternMatched(this._onObjectMatched, this);

			// Load i18n
			i18n = _oController.getOwnerComponent().getModel("i18n").getResourceBundle();

			//initialize local model
			var oLocal = new sap.ui.model.json.JSONModel();
			this.getView().setModel(oLocal, "local");

			// Configure Date Picker
			this.configureDatePickers(_oController);

			// Set Validator to Product Code
			this._setValidator();
		},

		/**
		 * Is fired everytime a user navs to the Entry Form page.
		 * Checks if the user is authorized to access this page.
		 * Checks if it is supposed to be an editing form or a creation form and makes the neccessary adjustments.
		 * @param {Object} oEvent - the event that fired the function
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function (oEvent) {
			this.checkAuthorization();

			// Check if the local model was loaded
			if (sap.ui.getCore().getModel("local") === undefined) {
				this.getRouter().navTo("cardManager");
				return;
			}
			//Get Domain model
			var domain = sap.ui.getCore().getModel("domain");
			this.getView().setModel(domain, "domain");

			var Item;
			var id = oEvent.getParameter("arguments").id;
			action = oEvent.getParameter("arguments").actionId;
			res = id;

			if (id !== "New") {
				if (action && action === "Copy") {
					this.getView().byId('saveButton').setText("Create");
				} else {
					this.getView().byId('saveButton').setText("Update");
				}
				this.getView().byId("saveButton").setEnabled(false);
				Item = sap.ui.getCore().getModel("local").getProperty("/ContentCardItem/" + res);

			} else {
				this.getView().byId('saveButton').setText("Create");
				this.getView().byId("saveButton").setEnabled(false);
				Item = false;
			}
			var shipToAccountsData = [];
			// Create the oLocal model
			var oLocal = new sap.ui.model.json.JSONModel();
			this.getView().setModel(oLocal, "local");
			this.getView().getModel("local").setProperty("/section", "1");
			this.getView().getModel("local").setProperty("/cardType", "100");

			if (Item) {
				Item.mediaType = parseInt(Item.mediaType, 10);
				if (action && action === "Copy") {
					var oItemCopy = {};
					for (var key in Item) {
						oItemCopy[key] = Item[key];
					}
					oItemCopy.name = "";
					oItemCopy.id = "";
					this.getView().getModel("local").setData(oItemCopy);
					fld8 = 0;
				} else {
					this.getView().getModel("local").setData(Item);
					fld8 = 1;
				}

				if (sap.ui.getCore().getModel("local").getProperty("/ContentCardShipTo/" + res).length) {
					shipToAccountsData = sap.ui.getCore().getModel("local").getProperty("/ContentCardShipTo/" + res);
					_oController.byId("idSoldTo").setSelected(true);
					_oController.byId("idSegment").setSelectedKeys().setEnabled(false);
					_oController.byId("idIgnoreIndustry").setSelected(false).setEnabled(false);
					_oController.byId("idExpressCare").setSelected(false).setEnabled(false);
					_oController.byId("idExpressCareBranded").setSelected(false).setVisible(false);
					_oController.byId("idExpressCareNonBranded").setSelected(false).setVisible(false);
					_oController.byId("idCustomerGroup5").setSelectedKeys().setEnabled(false);
					_oController.byId("idSalesDistrict").setSelectedKeys().setEnabled(false);
					_oController.byId("idSalesOffice").setSelectedKeys().setEnabled(false);
					_oController.byId("idSalesOrg").setSelectedKeys().setEnabled(false);
					_oController.byId("idSalesGroup").setSelectedKeys().setEnabled(false);
					_oController.byId("idSelectAccounts").setVisible(true);
				} else {
					_oController.byId("idSoldTo").setSelected(false);
					_oController.byId("idSegment").setEnabled(true);
					_oController.byId("idIgnoreIndustry").setEnabled(true);
					_oController.byId("idExpressCare").setEnabled(true);
					_oController.byId("idCustomerGroup5").setEnabled(true);
					_oController.byId("idSalesDistrict").setEnabled(true);
					_oController.byId("idSalesOffice").setEnabled(true);
					_oController.byId("idSalesOrg").setEnabled(true);
					_oController.byId("idSalesGroup").setEnabled(true);
					_oController.byId("idSelectAccounts").setVisible(false);
				}

				if (Item.isCPLChecked === "T") {
					_oController.byId("idCPLChecked").setSelected(true);
					if (sap.ui.getCore().getModel("local").getProperty("/ContentCardProductCodes/" + res).length) {
						_oController.byId("idProductCode").setVisible(true);
					}
				} else {
					_oController.byId("idCPLChecked").setSelected(false);
					_oController.byId("idProductCode").setVisible(false);
				}

				fld1 = 1;
				fld2 = 1;
				fld3 = 1;
				fld4 = 1;
				fld5 = 1;
				fld6 = 1;
				fld7 = 1;
				fld9 = 1;
				fld10 = 1;
				fld11 = 1;
				fld12 = 1;
				fld13 = 1;
				fld14 = 1;
				fld15 = 1;
				fld15_1 = 1;
				fld15_2 = 1;
				fld15_3 = 1;
				fld15_4 = 1;
				fld15_5 = 1;
				fld15_6 = 1;
				fld15_7 = 1;
				fld15_8 = 1;
				fld15_9 = 1;
				fld15_10 = 1;
				fld16_1 = 0;
				fld16_2 = 0;
				fld16 = 0;
				
				this._processStatus(this);
			} else {
				this.getView().byId('idTitle').setText(i18n.getText("CASECText3TXT"));
				this.getView().byId('idSubtitle').setText(i18n.getText("GENCAText1TXT"));
				this.getView().byId('idDescription').setText(i18n.getText("FEEDCText1TXT"));
				fld1 = 0;
				fld2 = 0;
				fld3 = 0;
				fld4 = 1; // since hiding the input label, need to make this not required
				fld5 = 0;
				fld6 = 0;
				fld7 = 0;
				fld8 = 0;
				fld9 = 0;
				fld10 = 0;
				fld11 = 0;
				fld12 = 0;
				fld13 = 0;
				fld14 = 0;
				fld15 = 0;
				fld15_1 = 0;
				fld15_2 = 0;
				fld15_3 = 0;
				fld15_4 = 0;
				fld15_5 = 0;
				fld15_6 = 0;
				fld15_7 = 0;
				fld15_8 = 0;
				fld15_9 = 0;
				fld15_10 = 0;
				fld16 = 0;
				fld16_1 = 0;
				fld16_2 = 0;
				this.getView().byId('idUrl').setSrc("resources/img/image-or-video.png");
				this.getView().byId("idUrl").setVisible(true);
				this.getView().byId("idHTML").setVisible(false);
				this.getView().byId("idDispSDate").setValueState(sap.ui.core.ValueState.None);

				//set ExpressCare checkBoxes state
				_oController.getView().byId("idExpressCare").setSelected(false);
				_oController.getView().byId("idExpressCareBranded").setSelected(true).setVisible(false);
				_oController.getView().byId("idExpressCareNonBranded").setSelected(true).setVisible(false);

				// Set CPL Check
				_oController.getView().byId("idCPLChecked").setSelected(false);
				_oController.getView().byId("idProductCode").setVisible(false);

				_oController.byId("idSegment").setEnabled(true);
				_oController.byId("idIgnoreIndustry").setEnabled(true);
				_oController.byId("idExpressCare").setEnabled(true);
				_oController.byId("idCustomerGroup5").setEnabled(true);
				_oController.byId("idSalesDistrict").setEnabled(true);
				_oController.byId("idSalesOffice").setEnabled(true);
				_oController.byId("idSalesOrg").setEnabled(true);
				_oController.byId("idSalesGroup").setEnabled(true);
				_oController.byId("idSelectAccounts").setVisible(false);
			}
			var aAccountData = [];
			if (shipToAccountsData.length) {
				for (var i = 0; i < shipToAccountsData.length; i++) {
					var oAccount = {
						id: shipToAccountsData[i].shipToId,
						accountname: shipToAccountsData[i].shipToName,
						accountnum: shipToAccountsData[i].shipToAccountNum,
						sapaccountname2: shipToAccountsData[i].shipToSapaccountname2,
						billingstate: shipToAccountsData[i].shipToBillingstate,
						billingstreet: shipToAccountsData[i].shipToBillingstreet,
						billingcity: shipToAccountsData[i].shipToBillingcity,
						billingpostalcode: shipToAccountsData[i].shipToBillingpostalcode,
						isSelected: true,
						isEnabled: true
					};
					aAccountData.push(oAccount);
				}
			}
			_oModelAccountAccessSortedDataSaveOnDone.setData(aAccountData);

			// Configures fnDisplay
			this.fnDisplay(Item, id);

			// Get Segments and set model to the view
			this.getSegments(this, id);

			// Set Product Codes
			this.setProductCodesModel(id);
			this.loggedUserFunctionalRolesModel();
			// Get Account Properties
			this.getAccountProperties(this, id);
		},

		/**
		 * Gets the Funcional Roles Model of the Logged User
		 * @function loggedUserFunctionalRolesModel
		 */

		loggedUserFunctionalRolesModel: function () {
			if (sap.ui.getCore().getModel("oModelRoles") === undefined) {
				var controller = this;
				var roleSelect = controller.getView().byId("idRoleSelect");
				roleSelect.setBusyIndicatorDelay(0).setBusy(true);
				var xsURL = "/usermgmt/user/functionalRoles";
				controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.loggedUserFunctionalRolesModelSuccess,
					controller.loggedUserFunctionalRolesModelError);
			} else {
				this.getView().setModel(sap.ui.getCore().getModel("oModelRoles"), "oModelRoles");
			}
		},

		loggedUserFunctionalRolesModelSuccess: function (controller, data) {
			var roleSelect = controller.getView().byId("idRoleSelect");
			roleSelect.setBusy(false);
			if (data.length === 0) {
				roleSelect.setEnabled(false);
			}
			var oModelRoles = new sap.ui.model.json.JSONModel();
			oModelRoles.setData(data);
			sap.ui.getCore().setModel(oModelRoles, "oModelRoles");
			controller.getView().setModel(oModelRoles, "oModelRoles");
		},
		loggedUserFunctionalRolesModelError: function () {
			var roleSelect = this.getView().byId("idRoleSelect");
			roleSelect.setBusy(false);
		},

		/**
		 * Convenience method for displaying the info of the template objects.
		 * @param {Object} item - the item to be displayed
		 * @function fnDisplay
		 */
		fnDisplay: function (item, id) {
			if (item) {

				// Titles
				this.getView().byId('idTitle').setText(item.title);
				this.getView().byId('idSubtitle').setText(item.subtitle);
				this.getView().byId('idDescription').setText(item.shortText);

				// Video or Image
				if (item.mediaType === 0) {
					this.getView().byId('idUrl').setSrc(item.mediaURL);
					this.getView().byId("idUrl").setVisible(true);
					this.getView().byId("idHTML").setVisible(false);
				} else {
					var oHTML = this.getView().byId("idHTML");
					oHTML.setContent("");
					oHTML.setContent(
						"<iframe class='videoForStopping' webkitallowfullscreen mozallowfullscreen allowfullscreen" +
						"allowtransparency='true' scrolling='no' src='" + item.mediaURL + "' enablejsapi=1></iframe>"
					);
					this.getView().byId("idUrl").setVisible(false);
					this.getView().byId("idHTML").setVisible(true);
				}

				// Role and Location Related
				if (item.isRoleBased === "T") {
					this.byId("idRoleBased").setSelected(true);
					this.byId("idRoleSelect").setVisible(true);
					this.byId("idLocation").setVisible(false);
					var roles = sap.ui.getCore().getModel("local").getProperty("/ContentCardFunctionalRole/" + id);
					if(roles){
						this.getView().byId("idRoleSelect").setSelectedKey(roles[0].functionalRoleValue);
					}
					
				} else {
					this.byId("idRoleBased").setSelected(false);
					this.byId("idRoleSelect").setVisible(false);
					this.byId("idLocation").setVisible(true);
					this.getView().byId("idLocation").setSelectedKey(item.cardCategory);
				}
			} else {
				this.byId("idRoleBased").setSelected(false);
				this.byId("idRoleSelect").setVisible(false);
				this.byId("idLocation").setVisible(true);
				this.getView().byId("idLocation").setSelectedKey();
			}
		},

		/**
		 * 1st Form
		 * Handle Live change Name
		 * @parameter (oEvent)
		 */
		handleLiveChangeName: function (oEvent) {
			var newValue = oEvent.getParameter("value");
			newValue = newValue.trim();
			if (newValue === "") {
				fld8 = 0;
			} else {
				fld8 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live the Change of the Base Role Box
		 * @(parameter) (oEvent)
		 */
		handleLiveChangeRoleBasedCheckBox: function () {
			var roleCheckBox = this.getView().byId("idRoleBased").getSelected();
			if (roleCheckBox) {
				this.getView().byId("idIgnoreIndustry").setEnabled(true);
				this.byId("idRoleSelect").setVisible(true);
				this.byId("idLocation").setVisible(false);
				this.byId("infoWelcomeCard").setVisible(false);
				this.loggedUserFunctionalRolesModel();
			} else {
				this.byId("idRoleSelect").setVisible(false);
				this.byId("idLocation").setVisible(true);
			}
			this.byId("idRoleSelect").setSelectedKey();
			this.byId("idLocation").setSelectedKey();
			fld11 = 0;
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Select Role
		 * @(parameter) (oEvent)
		 */
		handleLiveChangeRoleBaseSelect: function (oEvent) {
			var newValue = oEvent.getParameters("selectedItem");
			newValue = newValue.selectedItem.getKey();
			if (newValue === "") {
				fld11 = 0;
			} else {
				fld11 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Location / Category
		 * @(parameter) (oEvent)
		 */
		handleLiveChangeLocation: function (oEvent) {
			fld11 = this.handleLocationChange(oEvent, this);
			switch (fld11) {
			case 0:
				fld11 = 0;
				break;
			case 1:
				fld11 = 1;
				this.byId("idExpressCare").setEnabled(true);
				this.byId("idIgnoreIndustry").setEnabled(true);
				this.byId("idSegment").setEnabled(true);
				break;
			case "10000":
				//Main Feed Exception
				fld11 = 1;
				fld15_1 = 1;
				fld15_2 = 0;
				fld15_3 = 0;
				this.byId("idExpressCare").setSelected(false);
				_oController.byId("idExpressCareBranded").setSelected(false).setVisible(false);
				_oController.byId("idExpressCareNonBranded").setSelected(false).setVisible(false);
				this.byId("idIgnoreIndustry").setSelected(true);
				this.byId("idSegment").setSelectedKeys();
				break;
			default:
				break;
			}

			if (oEvent.getSource().getSelectedKey() === "1000") {
				this.getView().byId("infoWelcomeCard").setVisible(true);
			} else {
				this.getView().byId("infoWelcomeCard").setVisible(false);
			}

			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Priority
		 * Checks the Priority
		 * @(parameter) (oEvent)
		 */
		handleLiveChangePriority: function (oEvent) {
			var newValue = oEvent.getParameters("selectedItem");
			newValue = newValue.selectedItem.getKey();
			if (newValue === "") {
				fld12 = 0;
			} else {
				fld12 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Date
		 * Checks the start and end dates
		 * @(parameter) (oEvent)
		 */
		handleLiveChangeDate: function (oEvent) {
			fld13 = this.handleDateChange(oEvent, this);
			fld14 = this.handleDateChange(oEvent, this);
			fld9 = this._processStatus(this);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Ignore SAP Industry
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeSoldTo: function (oEvent) {
			fld15 = 0;
			fld15_4 = this.handlSoldToChange(oEvent, this);
			var removeSegmentsDialog;
			var aData = [];
			switch (fld15_4) {
			case -1:
				removeSegmentsDialog = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.removeSegmentsDialog");
				removeSegmentsDialog.open();
				sap.ui.getCore().byId("removeSegmentsConfirmation").attachPress(this.removePropertiesConfirmation);
				sap.ui.getCore().byId("removeSegmentsDecline").attachPress(this.onKeepProperties);
				sap.ui.getCore().byId("idRemoveSegments").setText(i18n.getText("RSDText2_2TXT"));
				break;
			case 0:
				if (_oModelAccountAccessSortedDataSaveOnDone.getData().length) {
					removeSegmentsDialog = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.removeSegmentsDialog");
					removeSegmentsDialog.open();
					sap.ui.getCore().byId("removeSegmentsConfirmation").attachPress(this.removeAccountConfirmation);
					sap.ui.getCore().byId("removeSegmentsDecline").attachPress(this.onKeepAccounts);
					sap.ui.getCore().byId("idRemoveSegments").setText(i18n.getText("RSDText2_5TXT"));
				} else {
					fld15_1 = 0;
					fld15_2 = 0;
					fld15_3 = 0;
					fld15_4 = 0;
					fld15_5 = 0;
					fld15_6 = 0;
					fld15_7 = 0;
					fld15_8 = 0;
					fld15_9 = 0;
					fld15_10 = 0;
					_oModelAccountAccessSortedDataSaveOnDone.setData(aData);
					_oController.byId("idSegment").setEnabled(true);
					_oController.byId("idIgnoreIndustry").setEnabled(true);
					_oController.byId("idExpressCare").setEnabled(true);
					_oController.byId("idCustomerGroup5").setEnabled(true);
					_oController.byId("idSalesDistrict").setEnabled(true);
					_oController.byId("idSalesOffice").setEnabled(true);
					_oController.byId("idSalesOrg").setEnabled(true);
					_oController.byId("idSalesGroup").setEnabled(true);
					_oController.byId("idSelectAccounts").setVisible(false);
				}
				break;
			case 1:
				fld15_1 = 0;
				fld15_2 = 0;
				fld15_3 = 0;
				fld15_4 = 1;
				fld15_5 = 0;
				fld15_6 = 0;
				fld15_7 = 0;
				fld15_8 = 0;
				fld15_9 = 0;
				fld15_10 = 0;
				_oModelAccountAccessSortedDataSaveOnDone.setData(aData);
				_oController.byId("idSegment").setSelectedKeys().setEnabled(false);
				_oController.byId("idIgnoreIndustry").setSelected(false).setEnabled(false);
				_oController.byId("idExpressCare").setSelected(false).setEnabled(false);
				_oController.byId("idExpressCareBranded").setSelected(false).setVisible(false);
				_oController.byId("idExpressCareNonBranded").setSelected(false).setVisible(false);
				_oController.byId("idCustomerGroup5").setSelectedKeys().setEnabled(false);
				_oController.byId("idSalesDistrict").setSelectedKeys().setEnabled(false);
				_oController.byId("idSalesOffice").setSelectedKeys().setEnabled(false);
				_oController.byId("idSalesOrg").setSelectedKeys().setEnabled(false);
				_oController.byId("idSalesGroup").setSelectedKeys().setEnabled(false);
				_oController.byId("idSelectAccounts").setVisible(true);
				break;
			default:
				break;
			}
			this.onConfigureSaveButton();

		},

		/**
		 * 1st Form
		 * Handle Live change Ignore SAP Industry
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeIgonreSAPIndustry: function (oEvent) {
			fld15 = 0;
			fld15_1 = this.handleSapIndustryKeyChange(oEvent, this);
			switch (fld15_1) {
			case -1:
				var removeSegmentsDialog = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.removeSegmentsDialog");
				removeSegmentsDialog.open();
				sap.ui.getCore().byId("removeSegmentsConfirmation").attachPress(this.removeSegmentsConfirmation);
				sap.ui.getCore().byId("removeSegmentsDecline").attachPress(this.onKeepSegments);
				sap.ui.getCore().byId("idRemoveSegments").setText(i18n.getText("RSDText2_1TXT"));
				break;
			case 0:
				fld15_1 = 0;
				fld15_2 = 0;
				fld15_3 = 0;
				break;
			case 1:
				fld15_1 = 1;
				fld15_2 = 0;
				fld15_3 = 0;
				break;
			default:
				break;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * remove Express Care confirmation.
		 * @function removeSalesOfficeConfirmation
		 */
		removeSalesOfficeConfirmation: function () {
			var dialog = sap.ui.getCore().byId("removeSegmentsDialog");
			dialog.close();
			dialog.destroy();
			fld15 = 0;
			fld15_3 = 1;
			fld15_7 = 0;
			_oController.byId("idSalesOffice").setSelectedKeys();
			_oController.byId("idExpressCare").setSelected(true);
			_oController.byId("idExpressCareBranded").setSelected(true).setVisible(true);
			_oController.byId("idExpressCareNonBranded").setSelected(true).setVisible(true);
			_oController.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * remove Express Care confirmation.
		 * @function removeExpressCareConfirmation
		 */
		removeExpressCareConfirmation: function () {
			var dialog = sap.ui.getCore().byId("removeSegmentsDialog");
			dialog.close();
			dialog.destroy();
			fld15 = 0;
			fld15_3 = 0;
			fld15_7 = 1;
			_oController.byId("idExpressCare").setSelected(false);
			_oController.byId("idExpressCareBranded").setSelected(false).setVisible(false);
			_oController.byId("idExpressCareNonBranded").setSelected(false).setVisible(false);
			_oController.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * remove Properties confirmation.
		 * @function removePropertiesConfirmation
		 */
		removePropertiesConfirmation: function () {
			var dialog = sap.ui.getCore().byId("removeSegmentsDialog");
			dialog.close();
			dialog.destroy();
			fld15 = 0;
			fld15_1 = 0;
			fld15_2 = 0;
			fld15_3 = 0;
			fld15_4 = 1;
			fld15_5 = 0;
			fld15_6 = 0;
			fld15_7 = 0;
			fld15_8 = 0;
			fld15_9 = 0;
			fld15_10 = 0;
			_oController.byId("idSegment").setSelectedKeys().setEnabled(false);
			_oController.byId("idIgnoreIndustry").setSelected(false).setEnabled(false);
			_oController.byId("idExpressCare").setSelected(false).setEnabled(false);
			_oController.byId("idExpressCareBranded").setSelected(false).setVisible(false);
			_oController.byId("idExpressCareNonBranded").setSelected(false).setVisible(false);
			_oController.byId("idCustomerGroup5").setSelectedKeys().setEnabled(false);
			_oController.byId("idSalesDistrict").setSelectedKeys().setEnabled(false);
			_oController.byId("idSalesOffice").setSelectedKeys().setEnabled(false);
			_oController.byId("idSalesOrg").setSelectedKeys().setEnabled(false);
			_oController.byId("idSalesGroup").setSelectedKeys().setEnabled(false);
			_oController.byId("idSoldTo").setSelected(true);
			_oController.byId("idSelectAccounts").setVisible(true);
			_oController.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * remove Account Confirmation
		 * @function removeAccountConfirmation
		 */
		removeAccountConfirmation: function () {
			var dialog = sap.ui.getCore().byId("removeSegmentsDialog");
			dialog.close();
			dialog.destroy();
			var aData = [];
			fld15_1 = 0;
			fld15_2 = 0;
			fld15_3 = 0;
			fld15_4 = 0;
			fld15_5 = 0;
			fld15_6 = 0;
			fld15_7 = 0;
			fld15_8 = 0;
			fld15_9 = 0;
			fld15_10 = 0;
			_oModelAccountAccessSortedDataSaveOnDone.setData(aData);
			_oController.byId("idSegment").setEnabled(true);
			_oController.byId("idIgnoreIndustry").setEnabled(true);
			_oController.byId("idExpressCare").setEnabled(true);
			_oController.byId("idCustomerGroup5").setEnabled(true);
			_oController.byId("idSalesDistrict").setEnabled(true);
			_oController.byId("idSalesOffice").setEnabled(true);
			_oController.byId("idSalesOrg").setEnabled(true);
			_oController.byId("idSalesGroup").setEnabled(true);
			_oController.byId("idSelectAccounts").setVisible(false);
			_oController.onConfigureSaveButton();
		},
		/**
		 * 1st Form
		 * Delete Card confirmation.
		 * @function deleteCardConfirmation
		 * @param {object} Card Id to be deleted
		 */
		removeSegmentsConfirmation: function () {
			var dialog = sap.ui.getCore().byId("removeSegmentsDialog");
			dialog.close();
			dialog.destroy();
			fld15 = 0;
			fld15_1 = 1;
			fld15_2 = 0;
			fld15_3 = 0;
			_oController.byId("idSegment").setSelectedKeys();
			_oController.byId("idExpressCare").setSelected(false);
			_oController.byId("idExpressCareBranded").setSelected(false).setVisible(false);
			_oController.byId("idExpressCareNonBranded").setSelected(false).setVisible(false);
			_oController.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Closes the remove segments message box
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		onKeepSalesOffice: function () {
			var dialog = sap.ui.getCore().byId("removeSegmentsDialog");
			dialog.close();
			dialog.destroy();
			fld15 = 0;
			fld15_3 = 0;
			_oController.byId("idExpressCare").setSelected(false);
			_oController.byId("idExpressCareBranded").setSelected(false).setVisible(false);
			_oController.byId("idExpressCareNonBranded").setSelected(false).setVisible(false);
			_oController.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Closes the remove segments message box
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		onKeepExpressCare: function () {
			var dialog = sap.ui.getCore().byId("removeSegmentsDialog");
			dialog.close();
			dialog.destroy();
			fld15 = 0;
			fld15_7 = 0;
			_oController.byId("idSalesOffice").setSelectedKeys();
			_oController.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Closes the remove segments message box
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		onKeepProperties: function () {
			var dialog = sap.ui.getCore().byId("removeSegmentsDialog");
			dialog.close();
			dialog.destroy();
			fld15 = 0;
			fld15_4 = 0;
			_oController.getView().byId("idSoldTo").setSelected(false);
			_oController.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Closes the remove segments message box
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		onKeepAccounts: function () {
			var dialog = sap.ui.getCore().byId("removeSegmentsDialog");
			dialog.close();
			dialog.destroy();
			fld15 = 0;
			fld15_4 = 1;
			fld15_10 = 1;
			_oController.getView().byId("idSoldTo").setSelected(true);
			_oController.onConfigureSaveButton();
		},
		/**
		 * 1st Form
		 * Closes the remove segments message box
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		onKeepSegments: function () {
			var dialog = sap.ui.getCore().byId("removeSegmentsDialog");
			dialog.close();
			dialog.destroy();
			fld15 = 0;
			fld15_1 = 0;
			_oController.getView().byId("idIgnoreIndustry").setSelected(false);
			_oController.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live Change CustomerGroup5
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeCustomerGroup5: function (oEvent) {
			fld15 = 0;
			fld15_4 = 0;
			fld15_5 = this.checkAccountPropertiesField(oEvent, this);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live Change SalesDistrict
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeSalesDistrict: function (oEvent) {
			fld15 = 0;
			fld15_4 = 0;
			fld15_6 = this.checkAccountPropertiesField(oEvent, this);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live Change SalesOffice
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeSalesOffice: function (oEvent) {
			fld15 = 0;
			fld15_4 = 0;
			fld15_7 = this.handlSalesOfficeChange(oEvent, this);
			switch (fld15_7) {
			case -1:
				var removeSegmentsDialog = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.removeSegmentsDialog");
				removeSegmentsDialog.open();
				sap.ui.getCore().byId("removeSegmentsConfirmation").attachPress(this.removeExpressCareConfirmation);
				sap.ui.getCore().byId("removeSegmentsDecline").attachPress(this.onKeepExpressCare);
				sap.ui.getCore().byId("idRemoveSegments").setText(i18n.getText("RSDText2_3TXT"));
				break;
			case 0:
				fld15_7 = 0;
				break;
			case 1:
				fld15_7 = 1;
				break;
			default:
				break;
			}

			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live Change SalesOrg
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeSalesOrg: function (oEvent) {
			fld15 = 0;
			fld15_4 = 0;
			fld15_8 = this.checkAccountPropertiesField(oEvent, this);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live Change SalesGroup
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeSalesGroup: function (oEvent) {
			fld15 = 0;
			fld15_4 = 0;
			fld15_9 = this.checkAccountPropertiesField(oEvent, this);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * handleLiveChangeLanguage
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeLanguage: function (oEvent) {
			if (oEvent.getParameter("selectedItem").getKey()) {
				fld10 = 1;
			} else {
				fld10 = 0;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Segments
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeSegments: function (oEvent) {
			fld15 = 0;
			fld15_1 = 0;
			fld15_2 = this.checkSegmentField(oEvent, this);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Express Care
		 * @function handleLiveChangeExpressCare
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeExpressCare: function (oEvent) {
			fld15 = 0;
			fld15_1 = 0;
			fld15_3 = this.handleExpressCare(oEvent, this);
			switch (fld15_3) {
			case -1:
				var removeSegmentsDialog = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.removeSegmentsDialog");
				removeSegmentsDialog.open();
				sap.ui.getCore().byId("removeSegmentsConfirmation").attachPress(this.removeSalesOfficeConfirmation);
				sap.ui.getCore().byId("removeSegmentsDecline").attachPress(this.onKeepSalesOffice);
				sap.ui.getCore().byId("idRemoveSegments").setText(i18n.getText("RSDText2_4TXT"));
				break;
			case 0:
				fld15_3 = 0;
				_oController.getView().byId("idExpressCareBranded").setSelected(true).setVisible(false);
				_oController.getView().byId("idExpressCareNonBranded").setSelected(true).setVisible(false);
				break;
			case 1:
				fld15_3 = 1;
				_oController.getView().byId("idExpressCareBranded").setSelected(true).setVisible(true);
				_oController.getView().byId("idExpressCareNonBranded").setSelected(true).setVisible(true);
				break;
			default:
				break;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change CPL
		 * @function handleLiveChangeCPL
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeCPL: function (oEvent) {
			fld16 = 0;
			fld16_1 = 0;
			if (oEvent.getSource().getSelected()) {
				fld16_1 = 1;
				_oController.getView().byId("idProductCode").setVisible(true);
				if (_oController.getView().byId("idProductCode").getTokens().length > 0) {
					fld16_2 = 1;
				}
			} else {
				_oController.getView().byId("idProductCode").setVisible(false);
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Express Care Branded
		 * @function handleChangeExpressCareBranded
		 * Forces the selected value to true if Non-Branded is not selected
		 * @(parameter) oEvent
		 */
		handleChangeExpressCareBranded: function (oEvent) {
			if (!oEvent.getParameter("selected") && !_oController.getView().byId("idExpressCareNonBranded").getSelected()) {
				_oController.getView().byId("idExpressCareNonBranded").setSelected(true);
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Express Care Non-Branded
		 * @function handleChangeExpressCareNonBranded
		 * Forces the selected value to true if Branded is not selected
		 * @(parameter) oEvent
		 */
		handleChangeExpressCareNonBranded: function (oEvent) {
			if (!oEvent.getParameter("selected") && !_oController.getView().byId("idExpressCareBranded").getSelected()) {
				_oController.getView().byId("idExpressCareBranded").setSelected(true);
			}
			this.onConfigureSaveButton();
		},

		/**
		 * Handle Live Change Fragment Title
		 */
		handleLiveChangeTitle: function (oEvent) {
			var newValue = oEvent.getParameter("value");
			newValue = newValue.trim();
			if (newValue === "") {
				this.getView().byId('idTitle').setText(i18n.getText("CASECText3TXT"));
				fld1 = 0;
			} else {
				this.getView().byId('idTitle').setText(newValue);
				fld1 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * Handle Live Change Fragment SubTitle
		 */
		handleLiveChangeSubTitle: function (oEvent) {
			var newValue = oEvent.getParameter("value");
			newValue = newValue.trim();
			if (newValue === "") {
				this.getView().byId('idSubtitle').setText(i18n.getText("GENCAText1TXT"));
				fld2 = 0;
			} else {
				this.getView().byId('idSubtitle').setText(newValue);
				fld2 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * Handle Live change Image Video
		 */
		handleLiveChangeImageVideo: function (oEvent) {
			var newValue = oEvent.getParameter("value");
			var oRBGroup = this.getView().byId("idRBGroup").getProperty("selectedIndex");
			var oHTML = this.getView().byId("idHTML");
			if (newValue === "") {
				if (oRBGroup === 0) {
					this.getView().byId('idUrl').setSrc("resources/img/image-or-video.png");
				} else {
					oHTML.setContent("");
					oHTML.setContent(
						"<div width='100%'>" +
						"<img src='resources/img/image-or-video.png' width='100%' height='176px'/>" +
						"</div>"
					);
				}
				fld5 = 0;
			} else {
				if (oRBGroup === 0) {
					this.getView().byId('idUrl').setSrc(newValue);
					this.getView().getModel("local").setProperty("/mediaType", 0);
				} else {
					oHTML.setContent("");
					oHTML.setContent(
						"<iframe class='videoForStopping' webkitallowfullscreen mozallowfullscreen allowfullscreen" +
						"allowtransparency='true' scrolling='no' src='" + newValue + "' enablejsapi=1></iframe>"
					);
					this.getView().getModel("local").setProperty("/mediaType", 1);
				}
				fld5 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * Handle Live change Descprition
		 */
		handleLiveChangeDescprition: function (oEvent) {
			var newValue = oEvent.getParameter("value");
			newValue = newValue.trim();
			if (newValue === "") {
				this.getView().byId('idDescription').setText(i18n.getText("FEEDCText1TXT"));
				fld3 = 0;
			} else {
				this.getView().byId('idDescription').setText(newValue);
				fld3 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * Handle Live change URL Target
		 */
		handleLiveChangeURLTarget: function (oEvent) {
			var newValue = oEvent.getParameters("selectedItem");
			newValue = newValue.selectedItem.getKey();
			if (newValue === "") {
				fld6 = 0;
			} else {
				fld6 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * Handle Live change Call To Action
		 */
		handleLiveChangeCallToActionURL: function (oEvent) {
			var newValue = oEvent.getParameter("value");
			newValue = newValue.trim();
			if (newValue === "") {
				fld7 = 0;
			} else {
				fld7 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * Handle Live change Back To Dash
		 */
		handleLiveChangeBackToDash: function (oEvent) {
			var check = oEvent.getSource().getSelected();
			if (check === true) {
				this.getView().getModel("local").setProperty("/backToDash", "1");
			} else {
				this.getView().getModel("local").setProperty("/backToDash", "0");
			}
			this.onConfigureSaveButton();
		},

		/**
		 * Configures the Save Button
		 */
		onConfigureSaveButton: function () {
			var oSaveBtn = this.getView().byId("saveButton");
			// If one of the Ignore SAP Industry or Segment or Express Care selected is select
			if (fld15_1 === 1 || fld15_2 === 1 || fld15_3 === 1 || fld15_4 === 1 || fld15_5 === 1 || fld15_6 === 1 || fld15_7 === 1 || fld15_8 ===
				1 || fld15_9 === 1 || fld15 === 1) {
				if (fld15_4 === 1) {
					if (fld15_10 === 1) {
						fld15 = 1;
					} else {
						fld15 = 0;
					}
				} else {
					fld15 = 1;
				}

			} else {
				fld15 = 0;
			}
			if (fld16_1 === 1) {
				if (fld16_2 === 1) {
					fld16 = 1;
				} else {
					fld16 = 0;
				}
			} else {
				fld16 = 1;
			}
			var inputCtr = fld1 + fld2 + fld3 + fld4 + fld5 + fld6 + fld7 + fld8 + fld9 + fld10 + fld11 + fld12 + fld13 + fld14 + fld15 + fld16;
			if (inputCtr === 16) {
				oSaveBtn.setEnabled(true);
			} else {
				oSaveBtn.setEnabled(false);
			}
		},

		/**
		 * Deals with the change of the media type, adjusting the view accordingly
		 * @function selectImageVid
		 * @param {Object} oEvent - the event of changing the media type
		 */
		selectImageVid: function (oEvent) {
			var sValue = oEvent.getParameter("selectedIndex");
			var oHTML = this.getView().byId("idHTML");
			if (sValue === 0) {
				//Image
				this.getView().byId('HCImgVid').setValue("");
				this.getView().byId("idUrl").setSrc("resources/img/image-or-video.png");
				this.getView().byId("imageTile").setVisible(true);
				this.getView().byId("idHTML").setVisible(false);
			} else {
				//Video
				this.getView().byId('HCImgVid').setValue("");
				this.getView().byId("imageTile").setVisible(false);
				oHTML.setVisible(true);
				if (oHTML.getContent() === "") {
					oHTML.setContent(
						"<div width='100%'>" +
						"<img src='resources/img/image-or-video.png' width='100%' height='176px'/>" +
						"</div>"
					);
				}
			}
		},

		/**
		 * Fired when the user clicks on the update/create button.
		 * Checks if the name of the card already exists, if so shows a error message, and doesn't allow the change.
		 * Asks the user to confirm he really wants to make the change.
		 * @function onConfirmation
		 */
		onConfirmation: function () {
			var existingCards = sap.ui.getCore().getModel("local").getData().ContentCardItem;
			var existingCardsLength = existingCards.length;
			var oLocalName = _oController.getView().getModel("local").getData().name;
			var oLocalId = _oController.getView().getModel("local").getData().id;
			var exist = 0;

			if (res === "New" || action === "Copy") {
				for (var i = 0; i < existingCardsLength; i++) {
					if (oLocalName === existingCards[i].name) {
						exist = 1;
					}
				}
				if (exist === 0) {
					var messagedialogFragment = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.messagedialog");
					messagedialogFragment.open();
					sap.ui.getCore().byId("yes").attachPress(this.onSaveEntryForm);
					sap.ui.getCore().byId("idConfirm").setText(i18n.getText("EFCONText3TXT"));
				} else {
					MessageBox.error(i18n.getText("EFCONText2TXT"));
				}
			} else {
				for (i = 0; i < existingCardsLength; i++) {
					if (oLocalName === existingCards[i].name && existingCards[i].id !== oLocalId) {
						exist = 1;
					}
				}
				if (exist === 0) {
					messagedialogFragment = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.messagedialog");
					messagedialogFragment.open();
					sap.ui.getCore().byId("yes").attachPress(this.onUpdateEntryForm);
					sap.ui.getCore().byId("idConfirm").setText(i18n.getText("EFCONText1TXT"));
				} else {
					MessageBox.error(i18n.getText("EFCONText2TXT"));
				}
			}
			sap.ui.getCore().byId("no").attachPress(this.onReturn);
		},

		/**
		 * Closes the error/warning message box
		 * @function onReturn
		 */
		onReturn: function () {
			var dialog = sap.ui.getCore().byId("msgdialog");
			dialog.close();
			dialog.destroy();
		},

		/**
		 * Fired when the user confirms he wants to create a new card and call generic create card function.
		 * @function onSaveEntryForm
		 */
		onSaveEntryForm: function () {
			_oController.onCreate(_oController);
		},

		/**
		 * Fired when the user confirms he wants to update a new card and call generic create card function.
		 * @function onUpdateEntryForm
		 */
		onUpdateEntryForm: function () {
			_oController.onUpdate(_oController);
		},

		/**
		 * Fired when the user press on Select Accounts Links.
		 * @function onSelectAccountsPress
		 */
		onSelectAccountsPress: function () {
			if (this.dialogFragmentAccountAccess) {
				if (this.dialogFragmentAccountAccess.isOpen()) {
					this.dialogFragmentAccountAccess.close();
				}
				this.dialogFragmentAccountAccess.destroy();
			}
			this.dialogFragmentAccountAccess = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.AccountAccess",
				_oController);
			this.dialogFragmentAccountAccess.open();
			_bSearchAccountFlag = false;
			sap.ui.getCore().byId("accountAccessSearchField").setVisible(false);
			sap.ui.getCore().byId("searchAccountButtonId").setVisible(true);
			sap.ui.getCore().byId("AccountAccessTable").setNoDataText(i18n.getText("UMAAText3TBL"));
			if (sap.ui.getCore().byId("accountAccessDialogHBoxId").hasStyleClass("accountAccessDialogHBox")) {
				sap.ui.getCore().byId("accountAccessDialogHBoxId").removeStyleClass("accountAccessDialogHBox");
			}
			_oController.addAccount();
			_oController.tablePaginationAccountAccess(_oModelAccountAccessSortedDataSaveOnDone);
		},

		/**
		 * Fired when the user press on Select Accounts Button.
		 * @function onSearchAccountButtonPress
		 */
		onSearchAccountButtonPress: function () {
			if (this.dialogFragmentAccountAccess) {
				if (this.dialogFragmentAccountAccess.isOpen()) {
					this.dialogFragmentAccountAccess.close();
				}
				this.dialogFragmentAccountAccess.destroy();
			}
			this.dialogFragmentAccountAccess = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.AccountAccess",
				_oController);
			this.dialogFragmentAccountAccess.open();
			_bSearchAccountFlag = true;
			sap.ui.getCore().byId("accountAccessSearchField").setVisible(true);
			sap.ui.getCore().byId("searchAccountButtonId").setVisible(false);
			sap.ui.getCore().byId("AccountAccessTable").setNoDataText(i18n.getText("UMAAText2TBL"));
			if (!sap.ui.getCore().byId("accountAccessDialogHBoxId").hasStyleClass("accountAccessDialogHBox")) {
				sap.ui.getCore().byId("accountAccessDialogHBoxId").addStyleClass("accountAccessDialogHBox");
			}
			_oController.addAccount();

		},
		/**
		 * Fired when the user search on the Select Accounts Dialog.
		 * @function onSearchAccountAccess
		 */
		onSearchAccountAccess: function (oEvent) {
			var sQuery = oEvent.getParameter("query");
			sQuery = sQuery.trim();
			if (sQuery.length >= 3) {
				sap.ui.getCore().byId("AccountAccessTable").setNoDataText(i18n.getText("UMAAText1TBL"));
				if (sQuery[0] !== "*") {
					sQuery = "*" + sQuery;
				}
				if (sQuery[sQuery.length - 1] !== "*") {
					sQuery = sQuery + "*";
				}
				_oController.searchAccountsByQuery(_oController, sQuery);
			} else {
				sap.ui.getCore().byId("AccountAccessTable").setNoDataText(i18n.getText("UMAAText1TBL"));
				_oModelAccountAccessTable.setData({});
				_oController.tablePaginationAccountAccess();
			}

		},
		/**
		 * Closes the AccountAcess Fragment
		 * @function toAccountAccess
		 */
		cancelDialogAccountAccess: function () {
			if (this.dialogFragmentAccountAccess.isOpen()) {
				if (this.dialogFragmentAccountAccess.isOpen()) {
					this.dialogFragmentAccountAccess.close();
				}
			}
			this.dialogFragmentAccountAccess.destroy();
			if (_bSearchAccountFlag) {
				_oController.onSelectAccountsPress();
			} else {
				for (var i = 0; i < _oModelAccountAccessSortedDataSaveOnDone.getData().length; i++) {
					_oModelAccountAccessSortedDataSaveOnDone.setProperty("/" + i + "/isSelected", true);
				}
			}

		},

		sapAccountNameVisibilityFormatter: function (accountName) {
			return (accountName !== undefined && accountName !== "");
		},
		/**
		 * Remove left Zeros
		 * @function removeZeros
		 * @param {number} number - number to parse.
		 */
		removeZeros: function (number) {
			var formattedNumber;
			if (number !== "") {
				formattedNumber = "" + parseInt(number, 10);
				return formattedNumber;
			} else {
				return formattedNumber;
			}

		},

		/**
		 * This function concatenates the account primary address.
		 * @function formatterContactAddress
		 * @param {string} mailingstreet - mailing street of the account
		 * @param {string} mailingcity - mailing city of the account
		 * @param {string} mailingstate - mailing state of the account
		 * @param {string} mailingpostalcode  - mailing postalcode of the account
		 * @returns Full formatted address
		 */
		formatterContactAddress: function (mailingstreet, mailingcity, mailingstate, mailingpostalcode) {
			var locationString = "";
			if (mailingstate && mailingpostalcode) {
				locationString = mailingcity + ", " + mailingstate + " " + mailingpostalcode;
			} else {
				if (mailingstate) {
					locationString = mailingcity + ", " + mailingstate;
				} else if (mailingpostalcode) {
					locationString = mailingcity + " " + mailingpostalcode;
				} else {
					locationString = mailingcity;
				}
			}

			if (mailingstreet) {
				locationString = mailingstreet + "\n " + locationString;
			}

			return locationString;
		},

		/**
		 * Sets the layout of the arrows for the table and controls the date to be loaded in the table
		 * @function tablePaginationAccountAccess
		 */
		tablePaginationAccountAccess: function (oAccountModel) {
			if (oAccountModel) {
				_oModelAccountAccessTable = oAccountModel;
				_oModelAccountAccessSortedData = _oModelAccountAccessTable;
				_beginAccountAccess = 0;
				_oController.addAccount(oAccountModel);

			}
			var oModel;
			if (_bSearchAccountFlag) {
				oModel = _oModelAccountAccessTable;
			} else {
				oModel = _oModelAccountAccessSortedDataSaveOnDone;
			}

			var _iLengthActive = oModel.getData().length ? oModel.getData().length : 0;
			var end = _beginAccountAccess + _iAccountAccessPageSize;
			var oDataTablePaginated = [];
			var oModelTablePaginated = new sap.ui.model.json.JSONModel();

			if (_iLengthActive === 0) {
				sap.ui.getCore().byId("navigationArrowsAccountAccess").setVisible(false);
			} else {
				sap.ui.getCore().byId("navigationArrowsAccountAccess").setVisible(true);
			}

			// Arrows and Number indicator layout
			if (_beginAccountAccess < _iAccountAccessPageSize) {
				sap.ui.getCore().byId("idLeftNavAccountAccess").setEnabled(false);
			} else {
				sap.ui.getCore().byId("idLeftNavAccountAccess").setEnabled(true);
			}
			if (end >= _iLengthActive) {
				end = _iLengthActive;
				sap.ui.getCore().byId("idRightNavAccountAccess").setEnabled(false);
			} else {
				sap.ui.getCore().byId("idRightNavAccountAccess").setEnabled(true);
			}
			if (_iLengthActive < _iAccountAccessPageSize) {
				end = _iLengthActive;
			}
			if (_iAccountAccessPageSize === 1 && _iLengthActive !== 0) {
				sap.ui.getCore().byId("idTextCountAccountAccess").setText(_beginAccountAccess + 1 + " " + _oController.getResourceBundle().getText(
						"of") + " " +
					_iLengthActive);
			} else {
				sap.ui.getCore().byId("idTextCountAccountAccess").setText(_beginAccountAccess + 1 + "-" + end + " " + _oController.getResourceBundle()
					.getText("of") + " " + _iLengthActive);
			}

			// Paginated Model
			for (var i = _beginAccountAccess; i < end; i++) {
				oDataTablePaginated.push(oModel.getProperty("/" + i));
			}
			oModelTablePaginated.setData(oDataTablePaginated);
			sap.ui.getCore().byId("AccountAccessTable").setModel(oModelTablePaginated, "AccountAccess");
		},

		/**
		 * Loads more items into the table
		 * @function toRightAccountAccess
		 */
		toRightAccountAccess: function () {
			_beginAccountAccess = _beginAccountAccess + _iAccountAccessPageSize;
			this.tablePaginationAccountAccess();
		},

		/**
		 * Loads more items into the table
		 * @function toLeftAccountAccess
		 */
		toLeftAccountAccess: function () {
			_beginAccountAccess = _beginAccountAccess - _iAccountAccessPageSize;
			this.tablePaginationAccountAccess();
		},

		doneDialogAccountAccess: function () {
			var modelData = _oModelAccountAccessSortedDataSaveOnDone.getData().length ? _oModelAccountAccessSortedDataSaveOnDone.getData() : [];
			if (_bSearchAccountFlag) {
				for (var i = 0; i < _oModelAccountAccessSortedData.getData().length; i++) {
					if (_oModelAccountAccessSortedData.getProperty("/" + i + "/isSelected")) {
						var account = {
							accountname: _oModelAccountAccessSortedData.getProperty("/" + i + "/accountname"),
							accountnum: _oModelAccountAccessSortedData.getProperty("/" + i + "/accountnum"),
							accountnumber: _oModelAccountAccessSortedData.getProperty("/" + i + "/accountnumber"),
							billingcity: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingcity"),
							billingcountry: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingcountry"),
							billingpostalcode: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingpostalcode"),
							billingstate: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingstate"),
							billingstreet: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingstreet"),
							id: _oModelAccountAccessSortedData.getProperty("/" + i + "/id"),
							isEnabled: _oModelAccountAccessSortedData.getProperty("/" + i + "/isEnabled"),
							isPrimaryAccount: _oModelAccountAccessSortedData.getProperty("/" + i + "/isPrimaryAccount"),
							isSelected: _oModelAccountAccessSortedData.getProperty("/" + i + "/isSelected"),
							shippingstreet: _oModelAccountAccessSortedData.getProperty("/" + i + "/shippingstreet"),
							isRadioEnabled: _oModelAccountAccessSortedData.getProperty("/" + i + "/isRadioEnabled")
						};
						modelData.push(account);
					}
				}
				modelData = _oController.removeDuplicateAccounts(modelData);
				_oModelAccountAccessSortedDataSaveOnDone.setData(modelData);
			} else {
				for (var j = 0; j < _oModelAccountAccessSortedDataSaveOnDone.getData().length; j++) {
					if (!_oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + j + "/isSelected")) {
						_oModelAccountAccessSortedDataSaveOnDone.getData().splice(j, 1);
						j--;
					}
				}
			}

			//reset table model
			_beginAccountAccess = 0; // Sets the arrows to 0
			if (this.dialogFragmentAccountAccess.isOpen()) {
				this.dialogFragmentAccountAccess.close();
			}
			this.dialogFragmentAccountAccess.destroy();
			var selectedAccounts = [];
			if (_oModelAccountAccessSortedDataSaveOnDone.getData().length) {
				fld15_10 = 1;
				for (var k = 0; k < _oModelAccountAccessSortedDataSaveOnDone.getData().length; k++) {
					selectedAccounts.push(_oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + k + "/id"));
				}
			} else {
				fld15_10 = 0;
			}
			_oController.getView().getModel("local").setProperty("/shipToAccounts", selectedAccounts);
			if (_bSearchAccountFlag) {
				_oController.onSelectAccountsPress();
			} else {
				_oController.onConfigureSaveButton();
			}
		},

		selectAllAccountAccess: function (oEvent) {
			var selectAllCheckBoxStatus = oEvent.getParameter("selected");
			if (_bSearchAccountFlag) {
				for (var i = 0; i < _oModelAccountAccessTable.getData().length; i++) {
					_oModelAccountAccessTable.setProperty("/" + i + "/isSelected", selectAllCheckBoxStatus);

				}
			} else {
				for (var j = 0; j < _oModelAccountAccessSortedDataSaveOnDone.getData().length; j++) {
					_oModelAccountAccessSortedDataSaveOnDone.setProperty("/" + j + "/isSelected", selectAllCheckBoxStatus);

				}
			}
			if (_bSearchAccountFlag) {
				sap.ui.getCore().byId("accountAccessDoneButton").setEnabled(selectAllCheckBoxStatus);
			} else {
				sap.ui.getCore().byId("accountAccessDoneButton").setEnabled(true);
			}
			_oController.tablePaginationAccountAccess();
		},

		addAccountAccess: function (oEvent) {
			var oPaginatedModel = sap.ui.getCore().byId("AccountAccessTable").getModel("AccountAccess");
			var selectedAccount = oPaginatedModel.getProperty(oEvent.getSource().getParent().getBindingContextPath());
			var selectAllCheckBoxStatus = oEvent.getParameter("selected");
			if (_bSearchAccountFlag) {
				_oController.addAccount(_oModelAccountAccessTable, selectedAccount, selectAllCheckBoxStatus);

			} else {
				_oController.addAccount(_oModelAccountAccessSortedDataSaveOnDone, selectedAccount, selectAllCheckBoxStatus);
			}
			_oController.tablePaginationAccountAccess();
		},

		addAccount: function (oModel, oValue, isSelected) {
			var bIsSelected = false;
			var bAllSelected = true;
			if (oModel && oModel.getData().length) {
				sap.ui.getCore().byId("allAccountsCheckBoxId").setEnabled(true);
				for (var i = 0; i < oModel.getData().length; i++) {
					if (oValue) {
						if (oModel.getProperty("/" + i + "/id") === oValue.id) {
							oModel.setProperty("/" + i + "/isSelected", isSelected);
						}
					}

					if (oModel.getProperty("/" + i + "/isSelected")) {
						bIsSelected = true;
					} else {
						bAllSelected = false;
					}
				}
			} else {
				sap.ui.getCore().byId("allAccountsCheckBoxId").setEnabled(false);
				bAllSelected = false;
			}
			if (_bSearchAccountFlag) {
				sap.ui.getCore().byId("accountAccessDoneButton").setEnabled(bIsSelected);
			} else {
				sap.ui.getCore().byId("accountAccessDoneButton").setEnabled(true);
			}
			sap.ui.getCore().byId("allAccountsCheckBoxId").setSelected(bAllSelected);
		},

		removeDuplicateAccounts: function (data) {
			data = _oController.onFilteroModel(data, "ascending", "accountname");
			var aDup = [];
			data = data.filter(function (obj) {
				if (aDup.indexOf(obj.id) === -1) {
					aDup.push(obj.id);
					return true;
				}
				return false;
			});
			return data;
		},

		/**
		 * Add Validator to Product Code
		 * @function _setValidator
		 */
		_setValidator: function () {
			var oMultiInputCode = _oController.getView().byId("idProductCode");
			// add validator
			oMultiInputCode.addValidator(function (args) {
				var text = args.text.trim();
				var aText;
				if (text) {
					var aTextCS = text.split(","); // Comma Separated value
					var aTextSS = text.split(" "); // Space Separated value
					if (aTextCS.length > 1) {
						aText = aTextCS;
					} else if (aTextSS.length > 1) {
						aText = aTextSS;
					} else {
						aText = aTextSS;
					}
					for (var i = 0; i < aText.length; i++) {
						aText[i] = aText[i].trim() && aText[i].trim().toUpperCase();
						if (aText[i] && aText[i].length <= 18) {
							args.asyncCallback(new sap.m.Token({
								text: aText[i],
								key: aText[i]
							}));
						}
					}
					return oMultiInputCode.WaitForAsyncValidation;
				}

			});
		},

		/**
		 * Function to handle Product Code update
		 * @function onPCodeUpdate
		 */
		onPCodeUpdate: function (oEvent) {
			if (oEvent.getSource().getTokens() && oEvent.getSource().getTokens().length > 0) {
				if (oEvent.getParameter("removedTokens").length > 0 && oEvent.getParameter("removedTokens").length === oEvent.getSource().getTokens()
					.length) {
					fld16_2 = 0;
				} else {
					fld16_1 = 1;
					fld16_2 = 1;
				}
			} else {
				fld16_2 = 0;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * Function to set Product Code model
		 * @function setProductCodesModel
		 * @param {string} id - card ID
		 */
		setProductCodesModel: function (id) {
			var productCodes = [];
			var model = new sap.ui.model.json.JSONModel();
			if (id !== "New") {
				productCodes = sap.ui.getCore().getModel("local").getProperty("/ContentCardProductCodes/" + id);
			}
			model.setData(productCodes);
			_oController.getView().setModel(model, "cardProductCodes");
		}

		
	});
});