sap.ui.define([
	"valvoline/dash/DashWCMT/controller/Base.controller",
	"sap/m/MessageToast",
	"sap/m/MessageBox"
], function (Base, MessageToast, MessageBox) {
	"use strict";
	var i18n, _selFrag, _oController, count = 1,
		flg = true,
		bool = true,
		page = 0,
		_newFrag, cycle,
		_newPage, _TextMdl, _oCarousel, _Frag, _oRouter, saveCtr, action = "";
	var fld1 = 0,
		fld2 = 0,
		fld3 = 0,
		fld4 = 0,
		fld6 = 0,
		fld7 = 0,
		fld8 = 0,
		fld9 = 0,
		fld10 = 0,
		fld11 = 0,
		fld12 = 0,
		fld13 = 0,
		fld14 = 0,
		fld15 = 0,
		fld15_1 = 0,
		fld15_2 = 0,
		fld15_3 = 0;
	return Base.extend("valvoline.dash.DashWCMT.controller.TextCarouselCardForm", {
		/** @module TextCarouselCardForm */
		/**
		 * Initializes the controller.
		 * Initializes the global variables.
		 * Attaches an event to everytime someone navs to the media carrousel form page.
		 * Initializes local models, and gets core models and associates them to the view.
		 * Configures date pickers to not allow users to choose dates from the past
		 * @function onInit
		 */
		onInit: function () {
			cycle = 0;

			_oController = this;
			_oRouter = this.getRouter();
			_oRouter.getRoute("textcarouselcardform").attachPatternMatched(this._onObjectMatched, this);

			// Load i18n model
			i18n = _oController.getOwnerComponent().getModel("i18n").getResourceBundle();

		},

		/**
		 * Is fired after the rendering of the view. Sets the active carousel page.
		 * @function onAfterRendering
		 */
		onAfterRendering: function () {
			setTimeout(function () {
				if (flg === true) {

					var oPage = _selFrag.split("_").shift(-1).substr(-1);
					if (_oCarousel.getActivePage() !== oPage) {
						_oCarousel.setActivePage("Page" + oPage);
					}
					flg = false;
				}
			}, 0);
		},
		/**
		 * Is fired everytime a user navs to the Entry Form page. 
		 * Checks if the user is authorized to access this page. 
		 * Checks if it is supposed to be an editing form or a creation form and makes the neccessary adjustments.
		 * @param {Object} oEvent - the event that fired the function		 
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function (oEvent) {
			this.checkAuthorization();
			
			count = 1;
			cycle = 0;
			// Check if the local model was loaded
			if (sap.ui.getCore().getModel("local") === undefined) {
				this.getRouter().navTo("cardManager");
				return;
			}
			var oLocal = new sap.ui.model.json.JSONModel();
			this.getView().setModel(oLocal, "local");

			//Get Domain model
			var domain = sap.ui.getCore().getModel("domain");
			this.getView().setModel(domain, "domain");

			//End of Get Domain
			var oTextMdl = new sap.ui.model.json.JSONModel();
			this.getView().setModel(oTextMdl, "textMdl");

			_TextMdl = this.getView().getModel("textMdl");
			_TextMdl.setProperty("/Fragments", {});
			_oCarousel = this.getView().byId("idCarousel");

			// Configure Date Picker
			this.configureDatePickers(_oController);
			
			this.getView().byId("idDispSDate").setValueState(sap.ui.core.ValueState.None);
			this.getView().byId("idDispEDate").setValueState(sap.ui.core.ValueState.None);
 

			var userModel = sap.ui.getCore().getModel("userapi");
			this.getView().setModel(userModel, "userapi");

			var id = oEvent.getParameter("arguments").id;
			action = oEvent.getParameter("arguments").actionId;

			//remove feed, insights and products from possible locations
			var oFilters = new sap.ui.model.Filter({
				filters: [
					new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.NE, '1'),
					new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.NE, '2'),
					new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.NE, '3'),
					new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.NE, '9'),
					new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.NE, '10'),
					new sap.ui.model.Filter("value", sap.ui.model.FilterOperator.NE, '11')
				],
				and: true
			});
			this.getView().byId("idLocation").getBinding("items").filter(oFilters);
		
			if (id !== "New") {
				saveCtr = 6;
				bool = true;
				flg = true;
				_TextMdl.setData({});
				_TextMdl.setProperty("/Fragments", {});
				this.getView().getModel("local").setData({});
				if (action && action === "Copy") {
					this.getView().byId('saveButton').setText("Create");
					fld8 = 0;
				} else {
					this.getView().byId('saveButton').setText("Update");
					fld8 = 1;
				}
				this.getView().byId("saveButton").setEnabled(false);
				this.getView().byId('idSaveCont').setText(i18n.getText("MCCFCONText1TXT"));
				this.getView().byId('idSaveCont').setEnabled(false);
				if (cycle !== 15) {
					this.getView().byId('idAddCont').setEnabled(true);
				} else {
					this.getView().byId('idAddCont').setEnabled(false);
				}

				fld1 = 1;
				fld2 = 1;
				fld3 = 1;
				fld4 = 1;
				fld6 = 1;
				fld7 = 1;
				fld9 = 1;
				fld10 = 1;
				fld11 = 1;
				fld12 = 1;
				fld13 = 1;
				fld14 = 1;
				fld15 = 1;
				fld15_1 = 1;
				fld15_2 = 1;
				fld15_3 = 1;
				saveCtr = 0;

				this.getView().setModel(oLocal, "local");

				var fstSection = sap.ui.getCore().getModel("local").getProperty("/ContentCardItem/" + oEvent.getParameter("arguments").id);
				var AllCards = sap.ui.getCore().getModel("local").getProperty("/Cards/");
				var sections = [];
				if(fstSection.cardGroup && fstSection.cardGroup !== ""){
					for (var j = 0; j < AllCards.length; j++) {
						if (AllCards[j].contentCards.cardGroup === fstSection.cardGroup) {
							sections.push(AllCards[j].contentCards);
						}
					}
				}
				
				_oCarousel.removeAllPages();
				this.populateCarousel(sections);

				var iSecLength = sections.length - 1;
				this.getView().getModel("local").setData(sections[iSecLength]);
				this.getView().getModel("local").updateBindings();
				this._processStatus(this);

			} else {
				saveCtr = 0;
				this.getView().byId('idAddCont').setEnabled(false);
				this.getView().byId("idSaveCont").setEnabled(false);
				this.getView().byId("saveButton").setEnabled(false);
				_oCarousel.removeAllPages();
				this.getView().byId('idTitle').setText(i18n.getText("CASECText3TXT"));
				_TextMdl.setData({});
				_TextMdl.setProperty("/Fragments", {});
				this.getView().getModel("local").setData({});
				this.getView().byId('saveButton').setText("Create");
				this.getView().byId('idSaveCont').setText(i18n.getText("MEDCCText1BT"));
				this._addNewPage();

				fld1 = 0;
				fld2 = 0;
				fld3 = 0;
				fld4 = 0;
				fld6 = 0;
				fld7 = 0;
				fld8 = 0;
				fld9 = 0;
				fld10 = 0;
				fld11 = 0;
				fld12 = 0;
				fld13 = 0;
				fld14 = 0;
				fld15 = 0;
				fld15_1 = 0;
				fld15_2 = 0;
				fld15_3 = 0;
				this.getView().byId('idTitle').setText(i18n.getText("CASECText3TXT"));
				sap.ui.core.Fragment.byId(_selFrag, "CardSubTitle").setText(i18n.getText("GENCAText1TXT"));
				sap.ui.core.Fragment.byId(_selFrag, "CardDesc").setText(i18n.getText("FEEDCText1TXT"));
				sap.ui.core.Fragment.byId(_selFrag, "CardLink").setText(i18n.getText("HEROCText1LK"));

				//set ExpressCare checkBoxes state
				_oController.getView().byId("idExpressCare").setSelected(false);
				_oController.getView().byId("idExpressCareBranded").setSelected(true).setVisible(false);
				_oController.getView().byId("idExpressCareNonBranded").setSelected(true).setVisible(false);
			}
			//Get Segments and set model to the view
			this.getSegments(this, id);
		},

		/**
		 * Convenience method for navigating back to main page.
		 * @function backToMain
		 */
		backToMain: function () {
			_oRouter.navTo("cardManager");
			_oCarousel.removeAllPages();
		},

		/**
		 * Creates pages, populates and paginates the carousel according to the card sections
		 * @function populateCarousel
		 * @param {Object} Item - the card sections that correspond to the carousel pages
		 */
		populateCarousel: function (Item) {
			var counter = 0;
			var oData = Item;
			var oCardItem = Item[0];
			if (action && action === "Copy") {
				var oItemCopy = {};
				for (var key in oCardItem) {
					oItemCopy[key] = oCardItem[key];
				}
				oItemCopy.name = "";
				oItemCopy.id = "";
				oItemCopy.cardGroup = "";
				_TextMdl.setProperty("/", oItemCopy);
				_TextMdl.setProperty("/FragmentsToAdd/", {});
			} else {
				_TextMdl.setProperty("/", oCardItem);
			}
			this.getView().byId('idTitle').setText(oCardItem.title);

			_TextMdl.setProperty("/Fragments/", {});

			var ctr = oData.length - 1;
			for (var i = 0; i < oData.length; i++) {
				var j = i + 4;
				var remainder = (j / 4) % 1;
				if (remainder === 0) {
					page++;
					_newPage = new sap.m.HBox("Page" + page, {
						width: "100%",
						height: "100%",
						justifyContent: "Start"
					});
					_newPage.addStyleClass("carousel-page");
				}

				this._addFrag();

				if (action && action === "Copy") {
					var oItemData = {};
					for (var field in oData[i]) {
						oItemData[field] = oData[i][field];
					}
					oItemData.name += "";
					oItemData.id = "";
					oItemData.cardGroup = "";
					_TextMdl.setProperty("/FragmentsToAdd/" + _selFrag, oItemData);
				} else {
					_TextMdl.setProperty("/Fragments/" + _selFrag, oData[i]);
				}
				sap.ui.core.Fragment.byId(_selFrag, "CardSubTitle").setText(oData[i].subtitle);
				sap.ui.core.Fragment.byId(_selFrag, "CardDesc").setText(oData[i].shortText);
				sap.ui.core.Fragment.byId(_selFrag, "CardLink").setText(oData[i].callToActionLabel);

				counter++;
				_newPage.addItem(_Frag);

				if (i !== ctr) {
					count++;
				}

				var k = counter;
				var rem = (k / 4) % 1;
				if (rem === 0 || i === ctr) {
					if (count > 3 && i !== ctr) {
						count = 1;
					}
					_oCarousel.addPage(_newPage);
				}
			}
		},

		/**
		 * Adds another fragment for showing an additonal page of the carousel
		 * @function _addFrag
		 */
		_addFrag: function () {
			var sFragId = "Frag" + page + "_" + count;
			if (cycle !== 14) {
				_Frag = new sap.ui.xmlfragment(sFragId, "valvoline.dash.DashWCMT.fragment.tCContentCard", this);
				_selFrag = sFragId;
				_newFrag = sFragId;
				this.getView().addDependent(_Frag);
				_Frag.addDelegate({
					onclick: function (oEvent) {
						_oController.onClickFrag(oEvent);
					}
				});
			} else {
				sFragId = "Frag" + page + "_" + count;
				_Frag = new sap.ui.xmlfragment(sFragId, "valvoline.dash.DashWCMT.fragment.tCContentCard", this);
				_selFrag = sFragId;
				_newFrag = sFragId;
				this.getView().addDependent(_Frag);
				_Frag.addDelegate({
					onclick: function (oEvent) {
						_oController.onClickFrag(oEvent);
					}
				});
				this.getView().byId("idAddCont").setEnabled(false);
			}
			cycle++;

		},

		/**
		 * Fired when the user clicks a page of the carousel. Updates bindings.
		 * @function onClickFrag
		 * @param {Object} oEvent - the event that fired the function
		 */
		onClickFrag: function (oEvent) {
			if (saveCtr === 6 && bool === true) {
				var sTempId = oEvent.currentTarget.id;
				var sSelId = sTempId.split("--").shift(-1);
				var oLocal = this.getView().getModel("local");
				var oTextMdl = this.getView().getModel("textMdl");

				if (_selFrag !== sSelId) {
					$('#' + _selFrag + '--idVBoxFrag').css({
						"border": ""
					});
					$('#' + sSelId + '--idVBoxFrag').css({
						"border": "solid 1px gray"
					});
				} else {
					$('#' + _selFrag + '--idVBoxFrag').css({
						"border": "solid 1px gray"
					});
				}

				_selFrag = sSelId;
				_newFrag = sSelId;
				var oData = oTextMdl.getProperty("/Fragments/" + sSelId);
				if (oData) {
					this.getView().getModel("local").setData(oData);
					this.getView().byId('idSaveCont').setText(i18n.getText("MCCFCONText1TXT"));
					this.getView().byId('idSaveCont').setEnabled(false);
				} else {
					this.getView().getModel("local").setData({});
					this.getView().byId('idSaveCont').setText(i18n.getText("MEDCCText1BT"));
					this.getView().byId('idSaveCont').setEnabled(false);

				}
				oLocal.updateBindings();

				this.getView().byId('idSaveCont').setEnabled(false);
				if (cycle !== 15) {
					this.getView().byId('idAddCont').setEnabled(true);
				} else {
					this.getView().byId('idAddCont').setEnabled(false);
				}

			}
		},

		/**
		 * Adds a new page to the caroussel with the data from the current fragment.
		 * @function _addNewPage
		 */
		_addNewPage: function () {
			page++;
			_newPage = new sap.m.HBox("Page" + page, {
				width: "100%",
				height: "100%",
				justifyContent: "Start"
			});
			this._addFrag();
			_newPage.addItem(_Frag);
			_newPage.addStyleClass("carousel-page");
			_oCarousel.addPage(_newPage);
		},

		/**
		 * Adds a new page to the carousel
		 * @function onAddContent
		 */
		onAddContent: function () {
			bool = false;
			if (count > 3) {
				count = 1;
				this._addNewPage();

			} else {
				count++;
				this._addFrag();
				_newPage.addItem(_Frag);
			}

			var oPage = _newFrag.split("_").shift(-1).substr(-1);
			if (_oCarousel.getActivePage() !== oPage) {
				_oCarousel.setActivePage("Page" + oPage);
			}

			_selFrag = _newFrag;
			var checked = this.getView().byId("idIgnoreIndustry").getSelected();
			this.getView().byId('idAddCont').setEnabled(false);
			this.getView().getModel("local").setData({});
			this.getView().byId("idIgnoreIndustry").setSelected(checked);
			this.getView().byId('idSaveCont').setText(i18n.getText("MEDCCText1BT"));
			fld1 = 1;
			fld2 = 0;
			fld3 = 0;
			fld4 = 0;
			fld6 = 0;
			fld7 = 0;
		},

		/**
		 * Fired when the user clicks on the save button. Saves the data into a model.
		 * @function onSaveContent
		 */
		onSaveContent: function () {
			bool = true;
			this.addToModel();

			var sBtnTxt = this.getView().byId('idSaveCont').getText();
			if (_selFrag !== _newFrag) {
				if (sBtnTxt === "Update Content") {
					$('#' + _newFrag + '--idVBoxFrag').css({
						"border": ""
					});
					$('#' + _selFrag + '--idVBoxFrag').css({
						"border": "solid 1px gray"
					});

					var oPage = _selFrag.split("_").shift(-1).substr(-1);
					if (_oCarousel.getActivePage() !== oPage) {
						_oCarousel.setActivePage("Page" + oPage);
					}
					this.getView().byId('idAddCont').setEnabled(false);
					var oData = _TextMdl.getProperty("/Fragments/" + _selFrag);
					if (oData) {
						this.getView().getModel("local").setData(JSON.parse(JSON.stringify(oData)));
						this.getView().byId('idSaveCont').setText(i18n.getText("MCCFCONText1TXT"));
						this.getView().byId('idSaveCont').setEnabled(false);
						if (cycle !== 15) {
							this.getView().byId('idAddCont').setEnabled(true);
						} else {
							this.getView().byId('idAddCont').setEnabled(false);
						}
					} else {
						this.getView().getModel("local").setData({});
						this.getView().byId('idSaveCont').setText(i18n.getText("MEDCCText1BT"));
						this.getView().byId('idSaveCont').setEnabled(false);
						this.getView().byId('idAddCont').setEnabled(false);
					}

					_selFrag = _newFrag;
				}
			} else {
				this.getView().byId('idSaveCont').setText(i18n.getText("MEDCCText1BT"));
				this.getView().byId('idSaveCont').setEnabled(false);
				if (cycle !== 15) {
					this.getView().byId('idAddCont').setEnabled(true);
				} else {
					this.getView().byId('idAddCont').setEnabled(false);
				}
			}

		},

		/**
		 * Adds current fragments to the model
		 * @function addToModel
		 */
		addToModel: function () {
			var oLocal = this.getView().getModel("local");
			var oData = oLocal.getData();

			//verify if it was an update or an addition
			var previousFrags = _TextMdl.getProperty("/Fragments");
			var flag = false;
			for (var x in previousFrags) {
				if (previousFrags[x].id === oData.id) {
					flag = true;
				}
			}

			if (flag) {
				_TextMdl.setProperty("/Fragments/" + _selFrag, oData);
			} else {
				var existingFragments = [];

				for (x in _TextMdl.getProperty("/FragmentsToAdd/")) {
					existingFragments.push(_TextMdl.getProperty("/FragmentsToAdd/")[x]);
				}

				existingFragments.push(oData);
				_TextMdl.setProperty("/FragmentsToAdd/", existingFragments);
			}
		},

		/**
		 * 1st Form
		 * Handle Live change Name
		 * @parameter (oEvent)
		 */
		handleLiveChangeName: function (oEvent) {
			var newValue = oEvent.getParameter("value");
			newValue = newValue.trim();
			if (newValue === "") {
				fld8 = 0;
			} else {
				fld8 = 1;
			}
			this.onConfigureSaveButton();
		},
		/**
		 * 1st Form
		 * handleLiveChangeLanguage
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeLanguage: function (oEvent) {
			if (oEvent.getParameter("selectedItem").getKey()) {
				fld10 = 1;
			} else {
				fld10 = 0;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Location / Category
		 * @(parameter) (oEvent)
		 */
		handleLiveChangeLocation: function (oEvent) {
			fld11 = this.handleLocationChange(oEvent, this);
			switch (fld11) {
			case 0:
				fld11 = 0;
				break;
			case 1:
				fld11 = 1;
				fld15_1 = 1;
				fld15_2 = 0;
				fld15_3 = 0;
				this.byId("idExpressCare").setEnabled(true);
				this.byId("idIgnoreIndustry").setEnabled(true);
				this.byId("idSegment").setEnabled(true);
				break;
			case "10000":
				//Main Feed Exception
				fld11 = 1;
				fld15_1 = 1;
				this.byId("idExpressCare").setEnabled(false);
				this.byId("idExpressCare").setSelected(false);
				_oController.byId("idExpressCareBranded").setSelected(false).setVisible(false);
				_oController.byId("idExpressCareNonBranded").setSelected(false).setVisible(false);
				this.byId("idIgnoreIndustry").setEnabled(false);
				this.byId("idIgnoreIndustry").setSelected(true);
				this.byId("idSegment").setEnabled(false);
				this.byId("idSegment").setSelectedKeys();
				break;
			default:
				break;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Priority
		 * Checks the Priority
		 * @(parameter) (oEvent)
		 */
		handleLiveChangePriority: function (oEvent) {
			var newValue = oEvent.getParameters("selectedItem");
			newValue = newValue.selectedItem.getKey();
			if (newValue === "") {
				fld12 = 0;
			} else {
				fld12 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Date Start
		 * Checks the start and end dates
		 * @(parameter) (oEvent)
		 */
		handleLiveChangeDateSt: function (oEvent) {
			fld13 = this.handleDateChange(oEvent, this);
			if (fld13) {
				fld14 = 1;
			}
			fld9 = this._processStatus(this);
			this.onConfigureSaveButton();
		},
		
		/**
		 * 1st Form
		 * Handle Live change Date End
		 * Checks the start and end dates
		 * @(parameter) (oEvent)
		 */
		handleLiveChangeDateEn: function (oEvent) {
			fld14 = this.handleDateChange(oEvent, this);
			if (fld14) {
				fld13 = 1;
			}
			fld9 = this._processStatus(this);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Ignore SAP Industry
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeIgonreSAPIndustry: function (oEvent) {
			fld15 = 0;
			fld15_1 = this.handleSapIndustryKeyChange(oEvent, this);
			switch (fld15_1) {
			case -1:
				var removeSegmentsDialog = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.removeSegmentsDialog");
				removeSegmentsDialog.open();
				sap.ui.getCore().byId("removeSegmentsConfirmation").attachPress(this.removeSegmentsConfirmation);
				sap.ui.getCore().byId("removeSegmentsDecline").attachPress(this.onKeepSegments);
				sap.ui.getCore().byId("idRemoveSegments").setText(i18n.getText("RSDText2_1TXT"));
				break;
			case 0:
				fld15_1 = 1;
				fld15_2 = 0;
				fld15_3 = 0;
				break;
			case 1:
				fld15_1 = 1;
				fld15_2 = 0;
				fld15_3 = 0;
				break;
			default:
				break;
			}
			this.onConfigureSaveButton();
		},
		/**
		 * 1st Form
		 * Remove segments confirmation.
		 * @function removeSegmentsConfirmation
		 */
		removeSegmentsConfirmation: function () {
			var dialog = sap.ui.getCore().byId("removeSegmentsDialog");
			dialog.close();
			dialog.destroy();
			fld15 = 0;
			fld15_1 = 1;
			fld15_2 = 0;
			fld15_3 = 0;
			_oController.byId("idSegment").setSelectedKeys();
			_oController.byId("idExpressCare").setSelected(false);
			_oController.byId("idExpressCareBranded").setSelected(false).setVisible(false);
			_oController.byId("idExpressCareNonBranded").setSelected(false).setVisible(false);
			_oController.onConfigureSaveButton();
		},
		

		/**
		 * 1st Form
		 * Closes the remove segments message box
		 * Resets the main tag to 0
		 */
		onKeepSegments: function () {
			var dialog = sap.ui.getCore().byId("removeSegmentsDialog");
			dialog.close();
			dialog.destroy();
			fld15 = 0;
			fld15_1 = 0;
			_oController.getView().byId("idIgnoreIndustry").setSelected(false);
			_oController.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Segments
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeSegments: function (oEvent) {
			fld15 = 0;
			fld15_1 = 0;
			fld15_2 = this.checkSegmentField(oEvent, this);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Express Care
		 * @function handleLiveChangeExpressCare
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeExpressCare: function (oEvent) {
			var newValue = oEvent.getParameter("selected");
			if (newValue) {
				_oController.getView().byId("idExpressCareBranded").setSelected(true).setVisible(true);
				_oController.getView().byId("idExpressCareNonBranded").setSelected(true).setVisible(true);
			} else {
				_oController.getView().byId("idExpressCareBranded").setSelected(true).setVisible(false);
				_oController.getView().byId("idExpressCareNonBranded").setSelected(true).setVisible(false);
			}
			fld15 = 0;
			fld15_1 = 0;
			fld15_3 = 1;
			this.byId("idIgnoreIndustry").setSelected(false);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Express Care Branded
		 * @function handleChangeExpressCareBranded
		 * Forces the selected value to true if Non-Branded is not selected
		 * @(parameter) oEvent
		 */
		handleChangeExpressCareBranded: function (oEvent) {
			if (!oEvent.getParameter("selected") && !_oController.getView().byId("idExpressCareNonBranded").getSelected()) {
				_oController.getView().byId("idExpressCareNonBranded").setSelected(true);
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Express Care Non-Branded
		 * @function handleChangeExpressCareNonBranded
		 * Forces the selected value to true if Branded is not selected
		 * @(parameter) oEvent
		 */
		handleChangeExpressCareNonBranded: function (oEvent) {
			if (!oEvent.getParameter("selected") && !_oController.getView().byId("idExpressCareBranded").getSelected()) {
				_oController.getView().byId("idExpressCareBranded").setSelected(true);
			}
			this.onConfigureSaveButton();
		},
		
		/** 1st Form
		 * Handle Live Change Customer Override
		 * @function handleLiveChangeCustomerOverride
		 * @(parameter) oEvent
		 */
		handleLiveChangeCustomerOverride: function (oEvent) {
			this.onConfigureSaveButton();
		},

		/**
		 * 2nd Form
		 * It is fired everytime a change is made in the form and adjusts the view according to the changes.
		 * @function handleLiveChange
		 * @param {Object} oEvent - the event that fired the function
		 */
		handleLiveChange: function (oEvent) {
			bool = false;
			this.getView().byId('idAddCont').setEnabled(false);
			var newValue;
			var oId = oEvent.getParameter("id");
			var objId = oId.split("--");
			switch (objId[1]) {
			case "HCTitle":
				newValue = oEvent.getParameter("value");
				newValue = newValue.trim();
				if (newValue === "") {
					this.getView().byId('idTitle').setText(i18n.getText("CASECText3TXT"));
					fld1 = 0;
				} else {
					this.getView().byId('idTitle').setText(newValue);
					fld1 = 1;
				}
				break;
			case "HCSubTitle":
				newValue = oEvent.getParameter("value");
				newValue = newValue.trim();
				if (newValue === "") {
					sap.ui.core.Fragment.byId(_selFrag, "CardSubTitle").setText(i18n.getText("GENCAText1TXT"));
					fld2 = 0;
				} else {
					sap.ui.core.Fragment.byId(_selFrag, "CardSubTitle").setText(newValue);
					fld2 = 1;
				}
				break;
			case "HCDesc":
				newValue = oEvent.getParameter("value");
				newValue = newValue.trim();
				if (newValue === "") {
					sap.ui.core.Fragment.byId(_selFrag, "CardDesc").setText(i18n.getText("FEEDCText1TXT"));
					fld3 = 0;
				} else {
					sap.ui.core.Fragment.byId(_selFrag, "CardDesc").setText(newValue);
					fld3 = 1;
				}
				break;
			case "HCLinkLabel":
				newValue = oEvent.getParameter("value");
				newValue = newValue.trim();
				if (newValue === "") {
					sap.ui.core.Fragment.byId(_selFrag, "CardLink").setText(i18n.getText("HEROCText1LK"));
					fld4 = 0;
				} else {
					sap.ui.core.Fragment.byId(_selFrag, "CardLink").setText(newValue);
					fld4 = 1;
				}
				break;
			case "HCURLTarget":
				newValue = oEvent.getParameters("selectedItem");
				newValue = newValue.selectedItem.getKey();
				if (newValue === "") {
					fld6 = 0;
				} else {
					fld6 = 1;
				}
				break;
			case "HCLinkTarget":
				newValue = oEvent.getParameter("value");
				newValue = newValue.trim();
				if (newValue === "") {
					fld7 = 0;
				} else {
					fld7 = 1;
				}
				break;
			case "checkBoxBackToDash":
				var check = oEvent.getSource().getSelected();
				if (check === true) {
					this.getView().getModel("local").setProperty("/backToDash", "1");
				} else {
					this.getView().getModel("local").setProperty("/backToDash", "0");
				}
				break;
			default:
				break;
			}
			this.onConfigureFragButton();
			this.onConfigureSaveButton();
		},

		/**
		 * Configures the Save Button
		 * and the add content button
		 */
		onConfigureSaveButton: function () {
			var oSaveBtn = this.getView().byId("saveButton");
			// If one of the Ignore SAP Industry or Segment or Express Care selected is select
			if (fld15_1 === 1 || fld15_2 === 1 || fld15_3 === 1) {
				fld15 = 1;
			} else {
				fld15 = 0;
			}
			var inputCtr = fld1 + fld2 + fld3 + fld4 + fld6 + fld7 + fld8 + fld9 + fld10 + fld11 + fld12 + fld13 + fld14 + fld15;
			if (inputCtr === 14) {
				oSaveBtn.setEnabled(true);
			} else {
				oSaveBtn.setEnabled(false);
			}
		},

		onConfigureFragButton: function () {
			saveCtr = fld1 + fld2 + fld3 + fld4 + fld6 + fld7;
			if (saveCtr === 6) {
				this.getView().byId('idSaveCont').setEnabled(true);
			} else {
				this.getView().byId('idSaveCont').setEnabled(false);
			}
		},

		/**
		 * Fired when the user clicks on the update/create button.
		 * Checks if the name of the card already exists, if so shows a error message, and doesn't allow the change.
		 * Asks the user to confirm he really wants to make the change.
		 * @function onConfirmation
		 */
		onConfirmation: function () {
			var existingCards = sap.ui.getCore().getModel("local").getData().ContentCardItem;
			var existingCardsLength = existingCards.length;
			var oLocalName = _TextMdl.getData().name;
			var oLocalId = _TextMdl.getData().id;
			var exist = 0;

			var sBtnLabel = this.getView().byId('saveButton').getText();
			if (sBtnLabel === "Update") {
				for (var i = 0; i < existingCardsLength; i++) {
					if (oLocalName === existingCards[i].name && existingCards[i].id !== oLocalId) {
						exist = 1;
					}
				}
				if (exist === 0) {
					var messagedialogFragment = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.messagedialog");
					messagedialogFragment.open();
					sap.ui.getCore().byId("yes").attachPress(this.onUpdateTextC);
					sap.ui.getCore().byId("idConfirm").setText(i18n.getText("EFCONText1TXT"));
				} else {
					MessageBox.error(i18n.getText("GEFCONText1TXT"));
				}
			} else {
				for (i = 0; i < existingCardsLength; i++) {
					if (oLocalName === existingCards[i].name) {
						exist = 1;
					}
				}
				if (exist === 0) {
					messagedialogFragment = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.messagedialog");
					messagedialogFragment.open();
					sap.ui.getCore().byId("yes").attachPress(this.onSaveTextC);
					sap.ui.getCore().byId("idConfirm").setText(i18n.getText("EFCONText3TXT"));
				} else {
					MessageBox.error(i18n.getText("GEFCONText1TXT"));
				}
			}
			sap.ui.getCore().byId("no").attachPress(this.onReturn);
		},

		/**
		 * Closes the error/warning message box
		 * @function onReturn
		 */
		onReturn: function () {
			var dialog = sap.ui.getCore().byId("msgdialog");
			dialog.close();
			dialog.destroy();
		},

		/**
		 * Fired when the user confirms he wants to create a new card.
		 * Retrieves the information from the form and saves the card to the database.
		 * If something goes wrong, shows a error message.
		 * If the card is successfully inserted, navs back to the main page.
		 * @function onSaveTextC
		 */
		onSaveTextC: function () {
			if (_TextMdl.getData().FragmentsToAdd === undefined || _TextMdl.getData().FragmentsToAdd.length === 0) {
			_oController.onSaveContent();
			}
			_oController.onCreateCarousel(_oController, _TextMdl, "700");
		},

		/**
		 * Fired when the user confirms he wants to update a new card.
		 * Retrieves the information from the form and updates the card in the database.
		 * If something goes wrong, shows a error message.
		 * If the card is successfully updated, navs back to the main page.
		 * @function onUpdateTextC
		 */
		onUpdateTextC: function () {
			_oController.onUpdateCarousel(_oController, _TextMdl, "700");
		}

	});
});