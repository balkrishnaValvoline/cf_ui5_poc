sap.ui.define([
	"sap/m/MessageBox",
	"valvoline/dash/DashWCMT/controller/Base.controller"
], function (MessageBox, Base) {
	"use strict";
	var _oRouter, i18n, _oController, res, action = "";

	var fld2 = 0, // call to action label (mandatory)
		fld3 = 0, // location (mandatory)
		fld4 = 1, // type is prefilled (mandatory) 
		fld6 = 0, // url target (mandatory)
		fld7 = 0, // call To Action URL (mandatory)
		fld8 = 0, // name (mandatory) 
		fld9 = 0, // status (mandatory)
		fld10 = 0, // language (mandatory) 
		fld11 = 0, // functional role  
		fld11_1 = 0,
		fld12 = 0, // priority (mandatory)
		fld5 = 0, // image url (mandatory)
		fld16 = 0; // call to action type (mandatory) 

	return Base.extend("valvoline.dash.DashWCMT.controller.StaticCardEntryForm", {
		/** @module StaticCardEntryForm */
		/**
		 * Initializes the controller.
		 * Initializes the global variables.
		 * Attaches an event to everytime someone navs to the entry form page.
		 * Initializes local models, and gets core models and associates them to the view.
		 * Configures date pickers to not allow users to choose dates from the past
		 * @function onInit
		 */
		onInit: function () {
			_oController = this;
			_oRouter = this.getRouter();
			_oRouter.getRoute("staticcardentryform").attachPatternMatched(this._onObjectMatched, this);

			// Load i18n
			i18n = _oController.getOwnerComponent().getModel("i18n").getResourceBundle();

			//initialize local model
			var oLocal = new sap.ui.model.json.JSONModel();
			this.getView().setModel(oLocal, "local");

		},

		/**
		 * Is fired everytime a user navs to the Static Card Entry Form page.
		 * Checks if the user is authorized to access this page.
		 * Checks if it is supposed to be an editing form or a creation form and makes the neccessary adjustments.
		 * @param {Object} oEvent - the event that fired the function
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function (oEvent) {
			this.checkAuthorization();

			// Check if the local model was loaded
			if (sap.ui.getCore().getModel("local") === undefined) {
				this.getRouter().navTo("cardManager");
				return;
			}
			//Get Domain model
			var domain = sap.ui.getCore().getModel("domain");
			this.getView().setModel(domain, "domain");

			var Item;
			var id = oEvent.getParameter("arguments").id;
			action = oEvent.getParameter("arguments").actionId;
			res = id;

			if (id !== "New") {
				if (action && action === "Copy") {
					this.getView().byId('saveButton').setText("Create");
				} else {
					this.getView().byId('saveButton').setText("Update");
				}
				this.getView().byId("saveButton").setEnabled(false);
				Item = sap.ui.getCore().getModel("local").getProperty("/ContentCardItem/" + res);

			} else {
				this.getView().byId('saveButton').setText("Create");
				this.getView().byId("saveButton").setEnabled(false);
				Item = false;
			}

			// Create the oLocal model
			var oLocal = new sap.ui.model.json.JSONModel();
			this.getView().setModel(oLocal, "local");
			this.getView().getModel("local").setProperty("/cardType", "900");

			if (Item) {
				Item.mediaType = parseInt(Item.mediaType, 10);
				if (action && action === "Copy") {
					var oItemCopy = {};
					for (var key in Item) {
						oItemCopy[key] = Item[key];
					}
					oItemCopy.name = "";
					oItemCopy.id = "";
					this.getView().getModel("local").setData(oItemCopy);
					fld8 = 0;
				} else {
					this.getView().getModel("local").setData(Item);
					fld8 = 1;
				}

				fld2 = 1;
				fld3 = 1;
				fld4 = 1;
				fld6 = 1;
				fld7 = 1;
				fld9 = 1;
				fld10 = 1;
				fld11 = 1;
				fld11_1 = 1;
				fld12 = 1;
				fld5 = 1;
				fld16 = 1;
			} else {
				this.getView().byId('idCallToAction').setText(i18n.getText("SCEFText5TXT"));

				fld2 = 0;
				fld3 = 0;
				fld4 = 1;
				fld6 = 0;
				fld7 = 0;
				fld8 = 0;
				fld9 = 0;
				fld10 = 0;
				fld11 = 0;
				fld11_1 = 0;
				fld12 = 0;
				fld5 = 0;
				fld16 = 0;
				this.getView().byId('idUrl').setSrc("resources/img/imageonly.png");
				this.getView().byId("idUrl").setVisible(true);
			}

			// Configures fnDisplay
			this.fnDisplay(Item, id);

			// Get Segments and set model to the view
			this.getSegments(this, id);

			this.loggedUserFunctionalRolesModel();

			// Get Account Properties
			this.getAccountProperties(this, id);

			// Get Predefined Authorization
			this.getPredefinedAuth(this);
		},

		/**
		 * Gets the Funcional Roles Model of the Logged User
		 * @function loggedUserFunctionalRolesModel
		 */

		loggedUserFunctionalRolesModel: function () {
			if (sap.ui.getCore().getModel("oModelRoles") === undefined) {
				var controller = this;
				var roleSelect = controller.getView().byId("idRoleSelect");
				roleSelect.setBusyIndicatorDelay(0).setBusy(true);
				var xsURL = "/usermgmt/user/functionalRoles";
				controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.loggedUserFunctionalRolesModelSuccess,
					controller.loggedUserFunctionalRolesModelError);
			} else {
				this.getView().setModel(sap.ui.getCore().getModel("oModelRoles"), "oModelRoles");
			}
		},

		loggedUserFunctionalRolesModelSuccess: function (controller, data) {
			var roleSelect = controller.getView().byId("idRoleSelect");
			roleSelect.setBusy(false);
			if (data.length === 0) {
				roleSelect.setEnabled(false);
			}
			var oModelRoles = new sap.ui.model.json.JSONModel();
			oModelRoles.setData(data);
			sap.ui.getCore().setModel(oModelRoles, "oModelRoles");
			controller.getView().setModel(oModelRoles, "oModelRoles");
		},
		loggedUserFunctionalRolesModelError: function () {
			var roleSelect = this.getView().byId("idRoleSelect");
			roleSelect.setBusy(false);
		},

		/**
		 * Convenience method for displaying the info of the template objects.
		 * @param {Object} item - the item to be displayed
		 * @function fnDisplay
		 */
		fnDisplay: function (item, id) {
			if (item) {
				// Titles
				this.getView().byId('idCallToAction').setText(item.callToActionLabel);
				// Image
				if (item.mediaType === 0) {
					this.getView().byId('idUrl').setSrc(item.mediaURL);
					this.getView().byId("idUrl").setVisible(true);
				}
				// Role and Location Related
				if (item.isRoleBased === "T") {
					this.byId("idRoleBased").setSelected(true);
					this.byId("idRoleSelect").setVisible(true);
					var roles = sap.ui.getCore().getModel("local").getProperty("/ContentCardFunctionalRole/" + id);
					if (roles) {
						for (var i = 0; i < roles.length; i++) {
							this.byId("idRoleSelect").addSelectedKeys(roles[i].functionalRoleValue);
						}
					}
				} else {
					this.byId("idRoleBased").setSelected(false);
					this.byId("idRoleSelect").setVisible(false);
				}

				if (item.userLevel) {
					var aUserLevels = item.userLevel.split(",");
					if (aUserLevels.length > 0) {
						for (var j = 0; j < aUserLevels.length; j++) {
							this.byId("idUserLevel").addSelectedKeys(aUserLevels[j]);
						}
					}
				}
			} else {
				this.byId("idRoleBased").setSelected(false);
				this.byId("idRoleSelect").setVisible(false);
			}
		},

		/**
		 * 1st Form
		 * Handle Live change Name
		 * @parameter (oEvent)
		 */
		handleLiveChangeName: function (oEvent) {
			var newValue = oEvent.getParameter("value");
			newValue = newValue.trim();
			if (newValue === "") {
				fld8 = 0;
			} else {
				fld8 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live the Change of the Base Role Box
		 * @(parameter) (oEvent)
		 */
		handleLiveChangeRoleBasedCheckBox: function () {
			var roleCheckBox = this.getView().byId("idRoleBased").getSelected();
			if (roleCheckBox) {
				this.byId("idRoleSelect").setVisible(true);
				this.loggedUserFunctionalRolesModel();
				fld11_1 = 1;
				fld11 = 0;
			} else {
				this.byId("idRoleSelect").setVisible(false);
				fld11_1 = 0;

				fld11 = 0;
			}
			this.byId("idRoleSelect").clearSelection();

			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Select Role
		 * @(parameter) (oEvent)
		 */
		handleLiveChangeRoleBaseSelect: function (oEvent) {
			if (oEvent.getSource().getSelectedKeys() && oEvent.getSource().getSelectedKeys().length > 0) {
				fld11 = 1;
			} else {
				fld11 = 0;
			}
			this.onConfigureSaveButton();
		},
		
		/**
		 * 1st Form
		 * Handle Live change optional field
		 * @(parameter) (oEvent)
		 */
		handleLiveChangeOptionalField: function (oEvent) {
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Action Type
		 * @(parameter) (oEvent)
		 */
		handleLiveChangeActionType: function (oEvent) {
			var newValue = oEvent.getParameters("selectedItem");
			newValue = newValue.selectedItem.getKey();
			if (newValue === "") {
				fld16 = 0;
			} else {
				fld16 = 1;
			}
			this.onConfigureSaveButton();
		},
		/**
		 * 1st Form
		 * Handle Live change Location / Category
		 * @(parameter) (oEvent)
		 */
		handleLiveChangeLocation: function (oEvent) {
			var newValue = oEvent.getParameters("selectedItem");
			newValue = newValue.selectedItem.getKey();
			if (newValue === "") {
				fld3 = 0;
			} else {
				fld3 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Status
		 * @(parameter) (oEvent)
		 */
		handleLiveChangeStatus: function (oEvent) {
			var newValue = oEvent.getParameters("selectedItem");
			newValue = newValue.selectedItem.getKey();
			if (newValue === "") {
				fld9 = 0;
			} else {
				fld9 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Priority
		 * Checks the Priority
		 * @(parameter) (oEvent)
		 */
		handleLiveChangePriority: function (oEvent) {
			var newValue = oEvent.getParameters("selectedItem");
			newValue = newValue.selectedItem.getKey();
			if (newValue === "") {
				fld12 = 0;
			} else {
				fld12 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live Change CustomerGroup5
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeCustomerGroup5: function (oEvent) {
			this.checkAccountPropertiesField(oEvent, this);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live Change SalesOffice
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeSalesOffice: function (oEvent) {
			this.checkAccountPropertiesField(oEvent, this);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live Change SalesOrg
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeSalesOrg: function (oEvent) {
			this.checkAccountPropertiesField(oEvent, this);
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * handleLiveChangeLanguage
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeLanguage: function (oEvent) {
			if (oEvent.getParameter("selectedItem").getKey()) {
				fld10 = 1;
			} else {
				fld10 = 0;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * 1st Form
		 * Handle Live change Segments
		 * Resets the main tag to 0
		 * @(parameter) oEvent
		 */
		handleLiveChangeSegments: function (oEvent) {
			this.checkSegmentField(oEvent, this);
			this.onConfigureSaveButton();
		},

		/**
		 * Handle Live Change Call to Action
		 */
		handleLiveChangeCallToActionLabel: function (oEvent) {
			var newValue = oEvent.getParameter("value");
			newValue = newValue.trim();
			if (newValue === "") {
				this.getView().byId('idCallToAction').setText(i18n.getText("SCEFText5TXT"));
				fld2 = 0;
			} else {
				this.getView().byId('idCallToAction').setText(newValue);
				fld2 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * Handle Live change URL Target
		 */
		handleLiveChangeURLTarget: function (oEvent) {
			var newValue = oEvent.getParameters("selectedItem");
			newValue = newValue.selectedItem.getKey();
			if (newValue === "") {
				fld6 = 0;
			} else {
				fld6 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * Handle Live change Call To Action
		 */
		handleLiveChangeCallToActionURL: function (oEvent) {
			var newValue = oEvent.getParameter("value");
			newValue = newValue.trim();
			if (newValue === "") {
				fld7 = 0;
			} else {
				fld7 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * Handle Live change Image
		 */
		handleLiveChangeImage: function (oEvent) {
			var newValue = oEvent.getParameter("value");
			if (newValue === "") {
				this.getView().byId('idUrl').setSrc("resources/img/imageonly.png");
				fld5 = 0;
			} else {
				this.getView().byId('idUrl').setSrc(newValue);
				this.getView().getModel("local").setProperty("/mediaType", 0);
				fld5 = 1;
			}
			this.onConfigureSaveButton();
		},

		/**
		 * Configures the Save Button
		 */
		onConfigureSaveButton: function () {
			var oSaveBtn = this.getView().byId("saveButton");
			if (fld11 === 0) {
				if (fld11_1 === 0) {
					fld11 = 1;
				} else {
					fld11 = 0;
				}
			}
			var inputCtr = fld2 + fld3 + fld4 + fld6 + fld7 + fld8 + fld9 + fld10 + fld12 + fld5 + fld16 + fld11;
			if (inputCtr === 12) {
				oSaveBtn.setEnabled(true);
			} else {
				oSaveBtn.setEnabled(false);
			}
		},

		/**
		 * Fired when the user clicks on the update/create button.
		 * Checks if the name of the card already exists, if so shows a error message, and doesn't allow the change.
		 * Asks the user to confirm he really wants to make the change.
		 * @function onConfirmation
		 */
		onConfirmation: function () {
			var existingCards = sap.ui.getCore().getModel("local").getData().ContentCardItem;
			var existingCardsLength = existingCards.length;
			var oLocalName = _oController.getView().getModel("local").getData().name;
			var oLocalId = _oController.getView().getModel("local").getData().id;
			var exist = 0;

			if (res === "New" || action === "Copy") {
				for (var i = 0; i < existingCardsLength; i++) {
					if (oLocalName === existingCards[i].name) {
						exist = 1;
					}
				}
				if (exist === 0) {
					var messagedialogFragment = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.messagedialog");
					messagedialogFragment.open();
					sap.ui.getCore().byId("yes").attachPress(this.onSaveEntryForm);
					sap.ui.getCore().byId("idConfirm").setText(i18n.getText("EFCONText3TXT"));
				} else {
					MessageBox.error(i18n.getText("EFCONText2TXT"));
				}
			} else {
				for (i = 0; i < existingCardsLength; i++) {
					if (oLocalName === existingCards[i].name && existingCards[i].id !== oLocalId) {
						exist = 1;
					}
				}
				if (exist === 0) {
					messagedialogFragment = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.messagedialog");
					messagedialogFragment.open();
					sap.ui.getCore().byId("yes").attachPress(this.onUpdateEntryForm);
					sap.ui.getCore().byId("idConfirm").setText(i18n.getText("EFCONText1TXT"));
				} else {
					MessageBox.error(i18n.getText("EFCONText2TXT"));
				}
			}
			sap.ui.getCore().byId("no").attachPress(this.onReturn);
		},

		/**
		 * Closes the error/warning message box
		 * @function onReturn
		 */
		onReturn: function () {
			var dialog = sap.ui.getCore().byId("msgdialog");
			dialog.close();
			dialog.destroy();
		},

		/**
		 * Fired when the user confirms he wants to create a new card and call generic create card function.
		 * @function onSaveEntryForm
		 */
		onSaveEntryForm: function () {
			_oController.onCreate(_oController);
		},

		/**
		 * Fired when the user confirms he wants to update a new card and call generic create card function.
		 * @function onUpdateEntryForm
		 */
		onUpdateEntryForm: function () {
			_oController.onUpdate(_oController);
		},

		/**
		 * Fired when the user press on predefined authorization.
		 * @function onSelectPredefinedAuth
		 */
		onSelectPredefinedAuth: function (oEvent) {
			var oControl = oEvent.getSource();
			if (this.dialogFragmentPredefinedAuth) {
				if (this.dialogFragmentPredefinedAuth.isOpen()) {
					this.dialogFragmentPredefinedAuth.close();
				}
				this.dialogFragmentPredefinedAuth.destroy();
			}
			this.dialogFragmentPredefinedAuth = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.PredefinedAuth",
				_oController);
			this.dialogFragmentPredefinedAuth.setModel(this.getView().getModel("predefinedAuth"));
			this.dialogFragmentPredefinedAuth.openBy(oControl);
		},

		/**
		 * Fired when the user selects predefined authorization.
		 * @function onNavToAuthDetail
		 */
		onNavToAuthDetail: function (oEvent) {
			var oCtx = oEvent.getSource().getBindingContext();
			var oModel = oEvent.getSource().getModel();
			var oData = oModel.getProperty(oCtx.getPath());
			_oController._formatAuthData(oData.authorizationDetails);
			var oNavCon = sap.ui.getCore().byId("idAuthNavCon");
			var oDetailPage = sap.ui.getCore().byId("idAuthDetail");
			oNavCon.to(oDetailPage);
			oDetailPage.bindElement(oCtx.getPath());
			_oController.dialogFragmentPredefinedAuth.focus();
		},

		/**
		 * Fired when the user nav backs predefined authorization.
		 * @function onNavBack
		 */
		onNavBack: function (oEvent) {
			var oNavCon = sap.ui.getCore().byId("idAuthNavCon");
			oNavCon.back();
			_oController.dialogFragmentPredefinedAuth.focus();
		},

		/**
		 * to flaten the dataset for authorization
		 * @function _formatAuthData
		 */
		_formatAuthData: function (data) {
			var oModel = new sap.ui.model.json.JSONModel();
			var obj, aFormattedData = [];
			if (data) {
				for (var i = 0; i < data.length; i++) {
					for (var j = 0; j < data[i].typeDetails.length; j++) {
						obj = data[i].typeDetails[j];
						obj.type = data[i].type;
						aFormattedData.push(obj);
					}
				}
			}
			oModel.setData(aFormattedData);
			_oController.dialogFragmentPredefinedAuth.setModel(oModel, "authDetails");

		},

		/**
		 * Fired when the user selects predefined authorization.
		 * @function applyPredefinedAuth
		 */
		applyPredefinedAuth: function () {
			var oSelectedModel = _oController.dialogFragmentPredefinedAuth.getModel("authDetails");
			var aSelectedData = oSelectedModel.getData();
			if (aSelectedData.length > 0) {
				_oController._clearPersonalizationRules();
			}
			_oController._applyPredefinedPersonalizationRules(aSelectedData);
		},

		/**
		 * clears the personalization rules
		 * @function _clearPersonalizationRules
		 */
		_clearPersonalizationRules: function () {
			_oController.getView().byId("idSalesOrg").clearSelection();
			_oController.getView().byId("idSegment").clearSelection();
			_oController.getView().byId("idRoleBased").setSelected(false);
			_oController.getView().byId("idRoleSelect").clearSelection();
			_oController.getView().byId("idRoleSelect").setVisible(false);
			_oController.getView().byId("idAdminChecked").setSelected(false);
			_oController.getView().byId("idUserLevel").clearSelection();
			_oController.getView().byId("idSalesOffice").clearSelection();
			_oController.getView().byId("idCustomerGroup5").clearSelection();
		},

		/**
		 * Fired when the user apply predefined authorization.
		 * @function _applyPredefinedPersonalizationRules
		 * @params aData(array)
		 */
		_applyPredefinedPersonalizationRules: function (aData) {
			var sID, oControl;
			for (var i = 0; i < aData.length; i++) {
				sID = "id" + (aData[i].type && aData[i].type.replace(/\s+/g, ""));
				oControl = _oController.getView().byId(sID);
				if (oControl) {
					if (oControl.setSelectedKeys) {
						oControl.addSelectedKeys(aData[i].id);
					} else if (oControl.setSelected) {
						oControl.setSelected(true);
					}
				}
			}
			if (_oController.getView().byId("idRoleSelect").getSelectedKeys().length > 0) {
				_oController.getView().byId("idRoleBased").setSelected(true);
				_oController.getView().byId("idRoleSelect").setVisible(true);
				fld11 = 1;
				fld11_1 = 1;
			} else {
				fld11 = 0;
				fld11_1 = 0;
			}
			_oController.dialogFragmentPredefinedAuth.close();
			this.onConfigureSaveButton();
		}
	});
});