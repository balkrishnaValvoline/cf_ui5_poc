sap.ui.define([
	"valvoline/dash/DashWCMT/controller/Base.controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/Fragment",
	"sap/m/MessageBox"
], function (Base, JSONModel, Fragment, MessageBox) {
	"use strict";
	var oThis, oCmHdr, _oRouter, oDomain;
	var i18n;
	var _deletedCardsBody, _archivedCardsBody;
	var bSessionTimeOut = false;
	var _availableCardStatus = ['A', 'I', 'P'];
	return Base.extend("valvoline.dash.DashWCMT.controller.CardManager", {
		/** @module Main */
		/**
		 * Initializes the local variables.
		 * Attaches an event to everytime someone navs to the main page.
		 * Saves global values on core models (card categories, card types, card sections, card priorities and display quantities)
		 * Attaches an event to table clicks.
		 * @function onInit
		 */
		onInit: function () {
			var sLocale = sap.ui.getCore().getConfiguration().getLanguage();
			var oi18nModel = new sap.ui.model.resource.ResourceModel({
				bundleName: "valvoline.dash.DashWCMT.i18n.i18n",
				bundleLocale: sLocale
			});
			sap.ui.getCore().setModel(oi18nModel, "i18n");

			i18n = sap.ui.getCore().getModel("i18n").getResourceBundle();
			oThis = this;
			_oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			_oRouter.getRoute("cardManager").attachPatternMatched(this.loadCards, this);

			//Added Get Domain Contents
			oDomain = new sap.ui.model.json.JSONModel();
			sap.ui.getCore().setModel(oDomain, "domain");
			sap.ui.getCore().getModel("domain").setProperty("/CardCategory", this.getByName("CardCategory"));
			sap.ui.getCore().getModel("domain").setProperty("/CardType", this.getByName("CardType"));
			sap.ui.getCore().getModel("domain").setProperty("/ContainerCard", this.getByName("Section"));
			sap.ui.getCore().getModel("domain").setProperty("/CardPriority", this.getByName("CardPriority"));
			sap.ui.getCore().getModel("domain").setProperty("/CardDisplayQuantity", this.getByName("CardDisplayQuantity"));
			sap.ui.getCore().getModel("domain").setProperty("/LanguageCode", this.getByName("LanguageCode"));
			sap.ui.getCore().getModel("domain").setProperty("/UserLevel", this.getByName("UserLevel"));
			sap.ui.getCore().getModel("domain").setProperty("/CardCallToActionType", this.getByName("CardCallToActionType"));
			//End of Get Domain

			var versionModel = new JSONModel();
			var applicationVersion = this.getOwnerComponent().getManifestEntry("sap.app").applicationVersion.version;
			versionModel.setProperty("/version", applicationVersion);
			this.getView().setModel(versionModel, "appConfigModel");
			oThis.idleTimeSetup();

			// Set the location Model
			var aSections = sap.ui.getCore().getModel("domain").getProperty("/ContainerCard");
			var locationArray = [];
			locationArray.push({
				id: 0,
				value: 0,
				description: 'All'
			});
			for (var i = 0; i < aSections.length; i++) {
				if (aSections[i].value !== "2" && aSections[i].value !== "3") {
					locationArray.push(aSections[i]);
				}
			}
			var locationModel = new sap.ui.model.json.JSONModel();
			locationModel.setData(locationArray);
			oThis.getView().setModel(locationModel, "locationModel");
		},

		/**
		 * Fired after the view is rendered.
		 * Checks if the user is authorized to access the page.
		 * @function onAfterRendering
		 */
		onAfterRendering: function () {
			$('#loading').remove();
			this.checkAuthorization();
		},

		/**
		 * Gets the card list to show on the table, deals with multi-section cards, returning only the first one
		 * @function loadCards
		 * @returns the list of cards
		 */
		loadCards: function () {
			var controller = this;
			oThis.getView().byId("floatingDeleteButton").setVisible(false);
			oThis.getView().byId("sectionSelectDrpDwn").setSelectedKey("0");
			oThis.WCMT_MainTable = controller.getView().byId("WCMT_MainTable");
			oThis.WCMT_MainTable_archived = controller.getView().byId("WCMT_MainTable_archived");
			oThis.WCMT_MainTable.setBusyIndicatorDelay(0);
			oThis.WCMT_MainTable.setBusy(true);
			oThis.WCMT_MainTable_archived.setBusyIndicatorDelay(0);
			oThis.WCMT_MainTable_archived.setBusy(true);
			oCmHdr = new sap.ui.model.json.JSONModel();
			sap.ui.getCore().setModel(oCmHdr, "local");
			var xsURL = "/core/contentCards";
			var oData = oThis.handleAjaxJSONCall(oThis, true, xsURL, "GET", oThis.loadCardsSuccess,
				oThis.loadCardsError);
			oThis.getView().byId("idIconTabBar").setSelectedItem(oThis.getView().byId("idIconTabBar").getItems()[0]);
			oThis._refreshFiltersSorters();
			return oData;
		},

		loadCardsSuccess: function (controller, data) {
			var cardsToShow = [];
			var cardsSegments = [];
			var cardGroups = [];
			var cardCustomersGroup5 = [],
				cardSalesDistrict = [],
				cardSalesGroup = [],
				cardSalesOffice = [],
				cardSalesOrg = [],
				cardShipTo = [],
				cardProductCodes = [],
				cardFunctionalRole = [];

			// Logic for showing only the first section of the carousels in the main page
			for (var i = 0; i < data.length; i++) {
				if (data[i].contentCards.cardGroup) {
					if (cardGroups.indexOf(data[i].contentCards.cardGroup) === -1) {
						cardGroups.push(data[i].contentCards.cardGroup);
						data[i].contentCards.salesOrgFormatted = data[i].cardSalesOrg ? oThis.formatSalesOrg(data[i].cardSalesOrg) : "";
						cardsToShow.push(data[i].contentCards);
						cardsSegments.push(data[i].cardSegments);
						cardCustomersGroup5.push(data[i].cardCustomersGroup5 ? data[i].cardCustomersGroup5 : []);
						cardSalesDistrict.push(data[i].cardSalesDistrict ? data[i].cardSalesDistrict : []);
						cardSalesGroup.push(data[i].cardSalesGroup ? data[i].cardSalesGroup : []);
						cardSalesOffice.push(data[i].cardSalesOffice ? data[i].cardSalesOffice : []);
						cardSalesOrg.push(data[i].cardSalesOrg ? data[i].cardSalesOrg : []);
						cardShipTo.push(data[i].cardShipTo ? data[i].cardShipTo : []);
						cardProductCodes.push(data[i].cardProductCodes ? data[i].cardProductCodes : []);
						cardFunctionalRole.push(data[i].cardFunctionalRole ? data[i].cardFunctionalRole : []);
					}
				} else {
					if (data[i].contentCards.cardCategory !== "4000" && data[i].contentCards.cardCategory !== "3000") {
						data[i].contentCards.salesOrgFormatted = data[i].cardSalesOrg ? oThis.formatSalesOrg(data[i].cardSalesOrg) : "";
						cardsToShow.push(data[i].contentCards);
						cardsSegments.push(data[i].cardSegments);
						cardCustomersGroup5.push(data[i].cardCustomersGroup5 ? data[i].cardCustomersGroup5 : []);
						cardSalesDistrict.push(data[i].cardSalesDistrict ? data[i].cardSalesDistrict : []);
						cardSalesGroup.push(data[i].cardSalesGroup ? data[i].cardSalesGroup : []);
						cardSalesOffice.push(data[i].cardSalesOffice ? data[i].cardSalesOffice : []);
						cardSalesOrg.push(data[i].cardSalesOrg ? data[i].cardSalesOrg : []);
						cardShipTo.push(data[i].cardShipTo ? data[i].cardShipTo : []);
						cardProductCodes.push(data[i].cardProductCodes ? data[i].cardProductCodes : []);
						cardFunctionalRole.push(data[i].cardFunctionalRole ? data[i].cardFunctionalRole : []);
					}
				}
			}

			// Set the original model to the core
			sap.ui.getCore().getModel("local").setProperty("/ContentCardItem", cardsToShow);
			sap.ui.getCore().getModel("local").setProperty("/ContentCardSegments", cardsSegments);
			sap.ui.getCore().getModel("local").setProperty("/ContentCardCustomersGroup5", cardCustomersGroup5);
			sap.ui.getCore().getModel("local").setProperty("/ContentCardSalesDistrict", cardSalesDistrict);
			sap.ui.getCore().getModel("local").setProperty("/ContentCardSalesGroup", cardSalesGroup);
			sap.ui.getCore().getModel("local").setProperty("/ContentCardSalesOffice", cardSalesOffice);
			sap.ui.getCore().getModel("local").setProperty("/ContentCardSalesOrg", cardSalesOrg);
			sap.ui.getCore().getModel("local").setProperty("/ContentCardShipTo", cardShipTo);
			sap.ui.getCore().getModel("local").setProperty("/ContentCardProductCodes", cardProductCodes);
			sap.ui.getCore().getModel("local").setProperty("/ContentCardFunctionalRole", cardFunctionalRole);
			sap.ui.getCore().getModel("local").setProperty("/Cards", data);

			// Set the Model in the View
			var cardsToShowModel = new sap.ui.model.json.JSONModel();
			cardsToShow = oThis.applyFormatters(cardsToShow);
			cardsToShowModel.setData(cardsToShow);
			oThis.getView().setModel(cardsToShowModel, "cardsToShow");
			oThis._filterTables();
			setTimeout(function () {
				oThis.WCMT_MainTable.setBusy(false);
				oThis.WCMT_MainTable_archived.setBusy(false);
			}, 0);

		},
		loadCardsError: function () {
			oThis.WCMT_MainTable.setBusy(false);
			oThis.WCMT_MainTable_archived.setBusy(false);
		},

		/**
		 * Functions to Apply Formatters to certain elements of the CardsToShow data
		 */
		applyFormatters: function (cardsToShow) {
			var controller = this;
			for (var i = 0; i < cardsToShow.length; i++) {
				cardsToShow[i].cardTypeFormated = controller.formatType(cardsToShow[i].cardType);
				cardsToShow[i].sectionFormated = controller.formatLocation(cardsToShow[i].section);
				cardsToShow[i].languageFormated = controller.formatLanguage(cardsToShow[i].lang);
				cardsToShow[i].statusFormated = controller.formatStatus(cardsToShow[i].status);
				cardsToShow[i].updatedOnFormated = controller.formatDate(cardsToShow[i].updatedOn);
			}
			return cardsToShow;
		},

		/**
		 * Navigates to the Card Catalog page
		 * @function toCardCatalog
		 */
		toCardCatalog: function () {
			_oRouter.navTo("cardCatalog");
		},

		/**
		 * Navigates to Feed Settings page
		 * @function toFeedSettings
		 */
		toFeedSettings: function () {
			_oRouter.navTo("feedsettings");
		},

		/**
		 * Transforms the number code of the card types into its description
		 * @function formatType
		 * @param {string} oType - the code of the type
		 * @returns The description of the type
		 */
		formatType: function (oType) {
			var aCardType = sap.ui.getCore().getModel("domain") && sap.ui.getCore().getModel("domain").getProperty("/CardType");
			if (aCardType && aCardType.length) {
				for (var obj in aCardType) {
					if (aCardType[obj].value === oType) {
						return aCardType[obj].description;
					}
				}
			}
			return "N/A";
		},

		/**
		 * Transforms the number code of the card location into its description
		 * @function formatLocation
		 * @param {string} section - the code of the location
		 * @returns The description of the location
		 */
		formatLocation: function (section) {
			var aSectionCode = sap.ui.getCore().getModel("domain") && sap.ui.getCore().getModel("domain").getProperty("/ContainerCard");
			if (aSectionCode && aSectionCode.length) {
				for (var obj in aSectionCode) {
					if (aSectionCode[obj].value === section) {
						return aSectionCode[obj].description;
					}
				}
			}
			return "N/A";
		},

		/**
		 * Fired when the user clicks on a table row. Redirects the user to clone page of that card.
		 * @function toDuplicateForm
		 * @param oEvent - the event that fired the function
		 */
		toDuplicateForm: function (oEvent) {
			var oSelItemPath = oEvent.getSource().getBindingContext("cardsToShow").getPath();
			var sPath = oSelItemPath.split("/").pop(-1);
			var oSelTypeId = oEvent.getSource().getBindingContext("cardsToShow").getObject();

			var cardTypeId = oSelTypeId.cardType;
			if (cardTypeId === "100") {
				_oRouter.navTo("entryform", {
					id: sPath,
					actionId: "Copy"
				});
			} else if (cardTypeId === "200" || cardTypeId === "300") {
				_oRouter.navTo("generalentryform", {
					id: sPath,
					actionId: "Copy"
				});
			} else if (cardTypeId === "500") {
				_oRouter.navTo("herocardform", {
					id: sPath,
					actionId: "Copy"
				});
			} else if (cardTypeId === "700") {
				_oRouter.navTo("textcarouselcardform", {
					id: sPath,
					actionId: "Copy"
				});
			} else if (cardTypeId === "800") {
				_oRouter.navTo("mediacarouselcardform", {
					id: sPath,
					actionId: "Copy"
				});
			} else if (cardTypeId === "900") {
				_oRouter.navTo("staticcardentryform", {
					id: sPath,
					actionId: "Copy"
				});
			} else {
				return;
			}
		},

		/**
		 * Fired when the user clicks on a table row. Redirects the user to edition page of that card.
		 * @function toEditForm
		 * @param oEvent - the event that fired the function
		 */
		toEditForm: function (oEvent) {
			var oSelItemPath = oEvent.getSource().getBindingContext("cardsToShow").getPath();
			var sPath = oSelItemPath.split("/").pop(-1);
			var oSelTypeId = oEvent.getSource().getBindingContext("cardsToShow").getObject();

			var cardTypeId = oSelTypeId.cardType;
			if (cardTypeId === "100") {
				_oRouter.navTo("entryform", {
					id: sPath
				});
			} else if (cardTypeId === "200" || cardTypeId === "300") {
				_oRouter.navTo("generalentryform", {
					id: sPath
				});
			} else if (cardTypeId === "500") {
				_oRouter.navTo("herocardform", {
					id: sPath
				});
			} else if (cardTypeId === "700") {
				_oRouter.navTo("textcarouselcardform", {
					id: sPath
				});
			} else if (cardTypeId === "800") {
				_oRouter.navTo("mediacarouselcardform", {
					id: sPath
				});
			} else if (cardTypeId === "900") {
				_oRouter.navTo("staticcardentryform", {
					id: sPath
				});
			} else {
				return;
			}
		},
		/**
		 * Opens Delete Card confirmation pop up.
		 * @function onDeleteCard
		 * @param {oEvent} The event of clicking on delete cards on Delete Button
		 */
		onDeleteCards: function () {
			var WCMT_MainTable_archived = this.byId("WCMT_MainTable_archived");
			var oModel_MainTable_Archived = WCMT_MainTable_archived.getModel("cardsToShow");
			var items = WCMT_MainTable_archived.getSelectedIndices();
			var sPath;
			var item;

			_deletedCardsBody = [];
			// Check all the selected items and add them to the array
			for (var i = 0; i < items.length; i++) {
				sPath = WCMT_MainTable_archived.getContextByIndex(items[i]).sPath;
				item = oModel_MainTable_Archived.getProperty(sPath);
				if (item.cardGroup) {
					//get all card ids from the group to delete
					var xsURL = "/core/contentCardsGroup/" + item.cardGroup;
					oThis.handleAjaxJSONCall(oThis, true, xsURL, "GET", oThis.onDeleteCardSuccess,
						oThis.onDeleteCardError);
				} else {
					_deletedCardsBody.push({
						"id": item.id
					});
				}
			}

			if (items.length > 0) {
				var deleteCardDialog = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.deleteCardDialog");
				deleteCardDialog.open();
				sap.ui.getCore().byId("deleteCardConfirmation").attachPress(this.deleteCardConfirmation);
				sap.ui.getCore().byId("deleteCardDecline").attachPress(this.onReturn);
				sap.ui.getCore().byId("idDeleteMessage").setText("You have selected " + items.length + " card(s). " +
					i18n.getText("DELDText2_1TXT") + " the selected card(s)?");
			}
		},

		/**
		 * Opens Archive Card confirmation pop up.
		 * @function onArchiveCard
		 * @param {oEvent} The event of clicking on archive card on Archive Button
		 */
		onArchiveCard: function (oEvent) {
			var path = oEvent.getSource().getBindingContext("cardsToShow").getPath();
			var cardId = oEvent.getSource().getBindingContext("cardsToShow").getProperty(path).id;
			var cardName = oEvent.getSource().getBindingContext("cardsToShow").getProperty(path).name;
			var cardGroup = oEvent.getSource().getBindingContext("cardsToShow").getProperty(path).cardGroup;
			_archivedCardsBody = [];
			if (cardGroup) {
				//get all card ids from the group to archive
				var xsURL = "/core/contentCardsGroup/" + cardGroup;
				oThis.handleAjaxJSONCall(oThis, false, xsURL, "GET", oThis.onArchiveCardSuccess,
					oThis.onArchiveCardError);
			} else {
				_archivedCardsBody.push({
					"id": cardId
				});
			}
			var archivedCardDialog = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.deleteCardDialog");
			archivedCardDialog.open();
			sap.ui.getCore().byId("deleteCardText").setText(i18n.getText("ARCHText1TXT"));
			sap.ui.getCore().byId("deleteCardConfirmation").attachPress(this.archiveCardConfirmation);
			sap.ui.getCore().byId("deleteCardDecline").attachPress(
				this.onReturn);
			sap.ui.getCore().byId("idDeleteMessage").setText(i18n.getText("ARCHText2_1TXT") + ' "' + cardName + '" ' + i18n.getText(
				"DELDText2_2TXT"));

		},

		/**
		 * Opens Delete Card confirmation pop up.
		 * @function onDeleteCard
		 * @param {oEvent} The event of clicking on delete card link
		 */
		onDeleteCard: function (oEvent) {
			var path = oEvent.getSource().getBindingContext("cardsToShow").getPath();
			var cardId = oEvent.getSource().getBindingContext("cardsToShow").getProperty(path).id;
			var cardName = oEvent.getSource().getBindingContext("cardsToShow").getProperty(path).name;
			var cardGroup = oEvent.getSource().getBindingContext("cardsToShow").getProperty(path).cardGroup;

			_deletedCardsBody = [];

			if (cardGroup) {
				//get all card ids from the group to delete
				var xsURL = "/core/contentCardsGroup/" + cardGroup;
				oThis.handleAjaxJSONCall(oThis, false, xsURL, "GET", oThis.onDeleteCardSuccess,
					oThis.onDeleteCardError);
			} else {
				_deletedCardsBody.push({
					"id": cardId
				});
			}
			var deleteCardDialog = new sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragment.deleteCardDialog");
			deleteCardDialog.open();
			sap.ui.getCore().byId("deleteCardConfirmation").attachPress(this.deleteCardConfirmation);
			sap.ui.getCore().byId("deleteCardDecline").attachPress(
				this.onReturn);
			sap.ui.getCore().byId("idDeleteMessage").setText(i18n.getText("DELDText2_1TXT") + ' "' + cardName + '" ' + i18n.getText(
				"DELDText2_2TXT"));

		},

		onDeleteCardSuccess: function (controller, data) {
			for (var i = 0; i < data.length; i++) {
				_deletedCardsBody.push({
					"id": data[i]
				});
			}
		},
		onDeleteCardError: function (oError) {
			var errRes = oThis.getError(oError);
			MessageBox.error(i18n.getText("REQErrorTXT") + " " + errRes);
		},

		/**
		 * call back success funtion
		 * @function onArchiveCardSuccess
		 * @param{object} controller
		 * @param{object} data - data from request
		 */
		onArchiveCardSuccess: function (controller, data) {
			for (var i = 0; i < data.length; i++) {
				_archivedCardsBody.push({
					"id": data[i]
				});
			}
		},

		/**
		 * call back error funtion
		 * @function onArchiveCardError
		 * * @param{object} error - error object fro request
		 */
		onArchiveCardError: function (oError) {
			var errRes = oThis.getError(oError);
			MessageBox.error(i18n.getText("REQErrorTXT") + " " + errRes);
		},

		/**
		 * Archive Card confirmation.
		 * @function archiveCardConfirmation
		 * @param {object} Card Id to be archived
		 */

		archiveCardConfirmation: function () {
			oThis.dialog = sap.ui.getCore().byId("deleteCardDialog");
			oThis.dialog.setBusyIndicatorDelay(0);
			oThis.dialog.setBusy(true);
			oThis._updateCount = 0;
			var xsURL;
			var statusUpdateBody = {};
			statusUpdateBody.contentCards = {};
			statusUpdateBody.contentCards.status = "C";
			for (var i = 0; i < _archivedCardsBody.length; i++) {
				xsURL = "/core/contentCards/" + _archivedCardsBody[i].id;
				oThis.handleAjaxJSONCall(oThis, true, xsURL, "PUT", oThis.archiveCardConfirmationSuccess,
					oThis.deleteCardConfirmationError, statusUpdateBody);
			}
		},

		/**
		 * success callback for Archive card confirmation.
		 * @function archiveCardConfirmationSuccess
		 */
		archiveCardConfirmationSuccess: function () {
			oThis._updateCount += 1;
			if (oThis._updateCount === _archivedCardsBody.length) {
				oThis.loadCards();
				oThis.dialog.close();
				oThis.dialog.destroy();
				oThis.dialog.setBusy(false);
			}
		},

		/**
		 * Delete Card confirmation.
		 * @function deleteCardConfirmation
		 * @param {object} Card Id to be deleted
		 */

		deleteCardConfirmation: function () {
			oThis.dialog = sap.ui.getCore().byId("deleteCardDialog");
			oThis.dialog.setBusyIndicatorDelay(0);
			oThis.dialog.setBusy(true);
			var xsURL = "/core/contentCards";
			oThis.handleAjaxJSONCall(oThis, true, xsURL, "DELETE", oThis.deleteCardConfirmationSuccess,
				oThis.deleteCardConfirmationError, _deletedCardsBody);
		},

		deleteCardConfirmationSuccess: function () {
			oThis.loadCards();
			oThis.dialog.close();
			oThis.dialog.destroy();
			oThis.dialog.setBusy(false);
		},
		deleteCardConfirmationError: function (oError) {
			oThis.dialogError.setBusy(false);
			var errRes = oThis.getError(oError);
			MessageBox.error(i18n.getText("REQErrorTXT") + " " + errRes);
		},

		/**
		 * Closes the delete card message box
		 * @function onReturn
		 */
		onReturn: function () {
			var dialog = sap.ui.getCore().byId("deleteCardDialog");
			dialog.close();
			dialog.destroy();
		},

		/**
		 * Fired when the user clicks on a table row. Redirects the user to edition page of that card.
		 * @function toEditForm
		 * @param oEvent - the event that fired the function
		 */
		toPreview: function (oEvent) {
			//stop videos on changing page
			$(".videoForStopping").each(function () {
				$(this).remove();
			});

			var oSelItemPath = oEvent.getSource().getBindingContext("cardsToShow").getPath();
			var sPath = oSelItemPath.split("/").pop(-1);
			var oSelTypeId = oEvent.getSource().getBindingContext("cardsToShow").getObject();
			var cardTypeId = oSelTypeId.cardType;
			var cardDetails = sap.ui.getCore().getModel("local").getProperty("/ContentCardItem/" + sPath);

			if (cardTypeId === "100") {
				this.feedCardPopOver(oEvent, cardDetails);
			} else if (cardTypeId === "200" || cardTypeId === "300") {
				this.generalCardPopOver(oEvent, cardDetails);
			} else if (cardTypeId === "500") {
				this.heroCardPopOver(oEvent, cardDetails);
			} else if (cardTypeId === "700") {
				this.textCarouselCardPopOver(oEvent, cardDetails);
			} else if (cardTypeId === "800") {
				this.mediaCarouselCardPopOver(oEvent, cardDetails);
			} else if (cardTypeId === "900") {
				this.staticCardPopOver(oEvent, cardDetails);
			} else {
				return;
			}
		},

		/**
		 * FeedCard Pop Over function
		 @ (oEvent, cardDetails)
		 */
		feedCardPopOver: function (oEvent, cardDetails) {
			if (this._oPopover) {
				this._oPopover.destroyContent();
			}

			this._oPopover = sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragmentPopOver.feedCardPopOver", this);
			this._oPopover.openBy(oEvent.getSource());
			// Set the Selected Card Model into Core
			var oModelCardDetails = new sap.ui.model.json.JSONModel();
			oModelCardDetails.setData(cardDetails);
			sap.ui.getCore().setModel(oModelCardDetails, "oModelCardDetails");

			// Picture
			var hboxImage = sap.ui.getCore().byId("idFrame");
			if (cardDetails.mediaType === "0") {
				var image = new sap.m.Image({
					src: this.formatterNoImage(cardDetails.mediaURL)
				}).addStyleClass("imageTilePicture");
				hboxImage.addItem(image);
			} else if (cardDetails.mediaType === "1") {
				var video = new sap.ui.core.HTML({
					content: "<iframe class='videoForStopping' webkitallowfullscreen mozallowfullscreen allowfullscreen" +
						"allowtransparency='true' scrolling='no' src='" + cardDetails.mediaURL + "?controls=0&autohide=1'></iframe>"
				}).addStyleClass("imageTilePicture");
				hboxImage.addItem(video);
			}

		},

		/**
		 * static Card Pop Over function
		 @ params (oEvent, cardDetails)
		 */
		staticCardPopOver: function (oEvent, cardDetails) {
			if (this._oPopover) {
				this._oPopover.destroyContent();
			}

			this._oPopover = sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragmentPopOver.staticCardPopOver", this);
			this._oPopover.openBy(oEvent.getSource());
			// Set the Selected Card Model into Core
			var oModelCardDetails = new sap.ui.model.json.JSONModel();
			oModelCardDetails.setData(cardDetails);
			sap.ui.getCore().setModel(oModelCardDetails, "oModelCardDetails");

			// Picture
			var hboxImage = sap.ui.getCore().byId("idFrame");
			if (cardDetails.mediaType === "0") {
				var image = new sap.m.Image({
					src: this.formatterNoImageOnly(cardDetails.mediaURL)
				}).addStyleClass("imageTilePicture");
				hboxImage.addItem(image);
			} else if (cardDetails.mediaType === "1") {
				var video = new sap.ui.core.HTML({
					content: "<iframe class='videoForStopping' webkitallowfullscreen mozallowfullscreen allowfullscreen" +
						"allowtransparency='true' scrolling='no' src='" + cardDetails.mediaURL + "?controls=0&autohide=1'></iframe>"
				}).addStyleClass("imageTilePicture");
				hboxImage.addItem(video);
			}

		},

		/**
		 * General Card PopOver
		 * @(oEvent, cardDetails)
		 */
		generalCardPopOver: function (oEvent, cardDetails) {
			if (this._oPopover) {
				this._oPopover.destroyContent();
			}

			this._oPopover = sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragmentPopOver.generalCardPopOver", this);
			this._oPopover.openBy(oEvent.getSource());
			// Set the Selected Card Model into Core
			var oModelCardDetails = new sap.ui.model.json.JSONModel();
			oModelCardDetails.setData(cardDetails);
			sap.ui.getCore().setModel(oModelCardDetails, "oModelCardDetails");

			// Picture
			var hboxImage = sap.ui.getCore().byId("idFrame");
			if (cardDetails.mediaType === "0") {
				var image = new sap.m.Image({
					src: this.formatterNoImage(cardDetails.mediaURL)
				}).addStyleClass("imageTilePicture");
				hboxImage.addItem(image);
			} else if (cardDetails.mediaType === "1") {
				var video = new sap.ui.core.HTML({
					content: "<iframe class='videoForStopping' webkitallowfullscreen mozallowfullscreen allowfullscreen" +
						"allowtransparency='true' scrolling='no' src='" + cardDetails.mediaURL + "'></iframe>"
				}).addStyleClass("imageTilePicture");
				hboxImage.addItem(video);
			}

		},

		/**
		 * Hero Card PopOver
		 * @(oEvent, cardDetails)
		 */
		heroCardPopOver: function (oEvent, cardDetails) {
			if (this._oPopover) {
				this._oPopover.destroyContent();
			}
			this._oPopover = sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragmentPopOver.heroCardPopOver", this);
			this._oPopover.openBy(oEvent.getSource());

			// Set the Selected Card Model into Core
			var oModelCardDetails = new sap.ui.model.json.JSONModel();
			oModelCardDetails.setData(cardDetails);
			sap.ui.getCore().setModel(oModelCardDetails, "oModelCardDetails");
			$('.heroCardTemplate').css({
				'background-image': 'url("' + oModelCardDetails.getProperty("/mediaURL") + '")'
			});
		},

		/**
		 * Media Card PopOver
		 * @(oEvent, cardDetails)
		 */
		mediaCarouselCardPopOver: function (oEvent, cardDetails) {
			if (this._oPopover) {
				this._oPopover.destroyContent();
			}

			this._oPopover = sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragmentPopOver.mediaCarouselCardPopOver", this);
			this._oPopover.openBy(oEvent.getSource());

			// Setting the Title of the Carousel
			sap.ui.getCore().byId("idTitle").setText(cardDetails.title);

			// Setting data for the carousel
			var allCards = sap.ui.getCore().getModel("local").getProperty("/Cards/");
			var carouselSections = [];
			if (cardDetails.cardGroup && cardDetails.cardGroup !== "") {
				for (var j = 0; j < allCards.length; j++) {
					if (allCards[j].contentCards.cardGroup === cardDetails.cardGroup) {
						carouselSections.push(allCards[j].contentCards);
					}
				}
			}

			// Carousel Component
			var carousel = sap.ui.getCore().byId("idCarousel");
			var numberOfSections = carouselSections.length;

			var nPages = numberOfSections / 4;

			var leftToRender = numberOfSections;
			var begin = 0;
			var end;
			for (var z = 0; z < nPages; z++) {
				begin = numberOfSections - leftToRender;

				if (begin + 4 > numberOfSections) {
					end = numberOfSections;
				} else {
					end = begin + 4;
				}

				var carouselPage = new sap.m.HBox({
					width: "100%",
					height: "100%",
					justifyContent: "Start"
				});
				for (var i = begin; i < end; i++) {
					leftToRender--;
					// Main Frame Card
					var vboxMainFrame = new sap.m.VBox().addStyleClass("whiteBackground normalTile mediaContentMargin");
					// Picture
					var hboxImage = new sap.m.HBox().addStyleClass("imageTilePicture");
					if (carouselSections[i].mediaType === "0") {
						var image = new sap.m.Image({
							src: this.formatterNoImage(carouselSections[i].mediaURL)
						}).addStyleClass("caroucel-image-tile-picture");
						hboxImage.addItem(image);
					} else if (carouselSections[i].mediaType === "1") {
						var video = new sap.ui.core.HTML({
							content: "<iframe class='videoForStopping' webkitallowfullscreen mozallowfullscreen allowfullscreen" +
								"allowtransparency='true' scrolling='no' src='" + carouselSections[i].mediaURL + "'></iframe>"
						}).addStyleClass("imageTilePicture");
						hboxImage.addItem(video);
					}
					// Text
					var vboxTextFrame = new sap.m.VBox().addStyleClass("feedCardContent");
					var textSubtitle = new sap.m.Text({
						text: carouselSections[i].subtitle,
						maxLines: 3
					}).addStyleClass("pageSubtitle mcTextAlignCenter mc-subtitle subtitle-max-lines");

					var vboxDescActionFrame = new sap.m.VBox({
						justifyContent: "SpaceBetween",
						height: "100%"
					});
					var textDescription = new sap.m.Text({
						text: carouselSections[i].shortText,
						maxLines: 7
					}).addStyleClass("bodyCopy sapUiSmallMarginTop mcTextAlignCenter description-max-lines");
					vboxTextFrame.addItem(textSubtitle);
					vboxTextFrame.addItem(textDescription);
					// Link
					var hboxLink = new sap.m.HBox().addStyleClass("feedCardBtnBorder");
					var link = new sap.m.Link({
						text: carouselSections[i].callToActionLabel
					}).addStyleClass("links ValvolineLink mcTextAlignCenter card-link");
					hboxLink.addItem(link);
					// Build
					vboxMainFrame.addItem(hboxImage);
					vboxDescActionFrame.addItem(vboxTextFrame);
					vboxDescActionFrame.addItem(hboxLink);
					vboxMainFrame.addItem(vboxDescActionFrame);
					carouselPage.addItem(vboxMainFrame);

				}
				carousel.addPage(carouselPage);
			}
		},

		/**
		 * Media Card PopOver generator
		 * @(oEvent, cardDetails)
		 */
		textCarouselCardPopOver: function (oEvent, cardDetails) {

			if (this._oPopover) {
				this._oPopover.destroyContent();
			}

			this._oPopover = sap.ui.xmlfragment("valvoline.dash.DashWCMT.fragmentPopOver.textCarouselCardPopOver", this);
			this._oPopover.openBy(oEvent.getSource());

			// Setting the Title of the Carousel
			sap.ui.getCore().byId("idTitle").setText(cardDetails.title);

			// Setting data for the carousel
			var allCards = sap.ui.getCore().getModel("local").getProperty("/Cards/");
			var carouselSections = [];
			if (cardDetails.cardGroup && cardDetails.cardGroup !== "") {
				for (var j = 0; j < allCards.length; j++) {
					if (allCards[j].contentCards.cardGroup === cardDetails.cardGroup) {
						carouselSections.push(allCards[j].contentCards);
					}
				}
			}

			// Carousel Component
			var carousel = sap.ui.getCore().byId("idCarousel");
			var numberOfSections = carouselSections.length;

			var nPages = numberOfSections / 4;
			/*if (numberOfSections > 3) {
				numberOfSections = 4;
			}*/
			var leftToRender = numberOfSections;
			var begin = 0;
			var end;
			for (var z = 0; z < nPages; z++) {
				var carouselPage = new sap.m.HBox({
					width: "100%",
					height: "100%",
					justifyContent: "Start"
				});
				begin = numberOfSections - leftToRender;
				if (begin + 4 > numberOfSections) {
					end = numberOfSections;
				} else {
					end = begin + 4;
				}
				for (var i = begin; i < end; i++) {
					leftToRender--;
					// Main Frame Card
					var vboxMainFrame = new sap.m.VBox({
						height: "300px"
					}).addStyleClass("whiteBackground textCarouselTile");
					// Text
					var vboxTextFrame = new sap.m.VBox().addStyleClass("subtitleMargin tcContentPadding");
					var textTitle = new sap.m.Text({
						text: carouselSections[i].subtitle,
						maxLines: 3
					}).addStyleClass("pageSubtitle tcContentTitle subtitle-max-lines");
					vboxTextFrame.addItem(textTitle);
					// Description
					var vboxDescActionFrame = new sap.m.VBox({
						justifyContent: "SpaceBetween",
						height: "100%"
					});
					var vboxTextDescFrame = new sap.m.VBox().addStyleClass("subtitleMargin tcContentPadding tcContentsubTitle");
					var textDescription = new sap.m.Text({
						text: carouselSections[i].shortText,
						maxLines: 7
					}).addStyleClass("bodyCopy description-max-lines");
					vboxTextDescFrame.addItem(textDescription);
					// Link
					var hboxLink = new sap.m.HBox({}).addStyleClass("tcContentPadding");
					var link = new sap.m.Link({
						text: carouselSections[i].callToActionLabel
					}).addStyleClass("links valvolineLink card-link");
					hboxLink.addItem(link);
					// Build
					vboxMainFrame.addItem(vboxTextFrame);
					vboxDescActionFrame.addItem(vboxTextDescFrame);
					vboxDescActionFrame.addItem(hboxLink);
					vboxMainFrame.addItem(vboxDescActionFrame);

					carouselPage.addItem(vboxMainFrame);
				}
				carousel.addPage(carouselPage);
			}
		},

		/**
		 * If there is no image returns placeholder image
		 * @parameter(mediaURL)
		 */
		formatterNoImage: function (mediaURL) {
			if (mediaURL) {
				if (mediaURL.indexOf("http") === -1) {
					return "resources/img/image-or-video.png";
				} else {
					return mediaURL;
				}
			} else {
				return "resources/img/image-or-video.png";
			}
		},

		/**
		 * If there is no image returns placeholder image
		 * @parameter(mediaURL)
		 */
		formatterNoImageOnly: function (mediaURL) {
			if (mediaURL) {
				if (mediaURL.indexOf("http") === -1) {
					return "resources/img/imageonly.png";
				} else {
					return mediaURL;
				}
			} else {
				return "resources/img/image-or-video.png";
			}
		},

		/**
		 * Transforms the one letter code of the card status into its full description
		 * @function formatStatus
		 * @param {string} oStat - the code of the status
		 * @returns the description of the status
		 */
		formatStatus: function (oStat) {
			if (oStat === "A") {
				return "Active";
			} else if (oStat === "I") {
				return "Expired";
			} else if (oStat === "P") {
				return "Pending";
			} else if (oStat === "C") {
				return "Archived";
			} else {
				return "N/A";
			}
		},

		/**
		 * Transforms the language code in full description
		 * @function formatLanguage
		 * @param {string} sLang - the code of the language
		 * @returns the description of the language
		 */
		formatLanguage: function (sLang) {
			var aLanguageCode = sap.ui.getCore().getModel("domain") && sap.ui.getCore().getModel("domain").getProperty("/LanguageCode");
			if (aLanguageCode && aLanguageCode.length) {
				for (var obj in aLanguageCode) {
					if (aLanguageCode[obj].value === sLang) {
						return aLanguageCode[obj].description;
					}
				}
			}
			return "N/A";

		},

		/**
		 * Setup for timeout validation
		 * @function idleTimeSetup
		 **/
		idleTimeSetup: function () {
			//Initialize idle time values
			window.nIdleTime = 0;
			window.nServiceIdleTime = 0;
			$(document).ready(function () {
				//Increment the idle time counter every minute.
				setInterval(oThis.timerIncrement, 60000); // 1 minute
				//Zero the idle timer on mouse click.
				$(this).click(function (e) {
					//Reset the idle time
					window.nIdleTime = 0;
				});
			});
		},

		/**
		 * Handle time increments, service pings and trigger timeout
		 * @function timerIncrement
		 **/
		timerIncrement: function () {
			//increments the user idle time
			window.nIdleTime = window.nIdleTime + 60000;
			//increments the service call idle time
			window.nServiceIdleTime = window.nServiceIdleTime + 60000;
			if (window.nIdleTime >= 3600000) { // 60+ minute
				if (!bSessionTimeOut) {
					sap.m.MessageBox.show(
						oThis.getResourceBundle().getText("STIdleMessageDialog"), {
							icon: sap.m.MessageBox.Icon.INFORMATION,
							title: "Information",
							actions: [sap.m.MessageBox.Action.CLOSE],
							onClose: function () {
								sap.m.URLHelper.redirect("/WCMT/do/logout", false);
							}
						}
					);
					bSessionTimeOut = true;
				}
			} else if (window.nServiceIdleTime >= 900000) { //15+min, time will reset when the checkSessionTimeout service is called
				oThis.checkSessionTimeout();
			} else {
				//defensive programming: no default action needed
			}
		},

		/**
		 * Return the sales Org as string
		 * @function formatSalesOrg
		 * @param {Array} sales Org Array
		 */
		formatSalesOrg: function (aSalesOrg) {
			var sSalesOrg = "";
			if (aSalesOrg && aSalesOrg.length > 0) {
				for (var i = 0; i < aSalesOrg.length; i++) {
					if (i !== 0) {
						sSalesOrg += ", ";
					}
					sSalesOrg += aSalesOrg[i].salesOrgDesc + "(" + aSalesOrg[i].salesOrgName + ")";
				}
			}
			return sSalesOrg;
		},

		/**
		 * Handles the visibility of delete button
		 * @function onSelectionChange
		 * @param {oEvent} The event of selecting the table rows
		 */
		onSelectionChange: function (oEvent) {
			if (oEvent && oEvent.getSource().getSelectedIndices() && oEvent.getSource().getSelectedIndices().length > 0) {
				oThis.getView().byId("floatingDeleteButton").setVisible(true);
			} else {
				oThis.getView().byId("floatingDeleteButton").setVisible(false);
			}
		},

		/**
		 * Handles the location change filter
		 * @function onLocationSectionChange
		 * @param {oEvent} The event of selecting location
		 */
		onLocationSectionChange: function (oEvent) {
			oThis._refreshFiltersSorters();
			oThis._filterTables();
		},

		/**
		 * Handles the filter
		 * @function _filterTables
		 * @param isColumnFilter(boolean) to identify column filtering
		 */
		_filterTables: function (isColumnFilter) {
			var aFilters = [];
			for (var j = 0; j < _availableCardStatus.length; j++) {
				aFilters.push(new sap.ui.model.Filter("status", sap.ui.model.FilterOperator.EQ, _availableCardStatus[j]));
			}
			var aArchivedFilter = [];
			aArchivedFilter.push(new sap.ui.model.Filter("status", sap.ui.model.FilterOperator.EQ, "C"));
			var sLanguageSelectedKey = oThis.getView().byId("sectionSelectDrpDwn") && oThis.getView().byId("sectionSelectDrpDwn").getSelectedKey();
			if (sLanguageSelectedKey && sLanguageSelectedKey !== "0") {
				var oFilter = new sap.ui.model.Filter("section", sap.ui.model.FilterOperator.EQ, sLanguageSelectedKey);
				aFilters.push(oFilter);
				aArchivedFilter.push(oFilter);
			}
			if (isColumnFilter) {
				var existingMainTableFilters = oThis.WCMT_MainTable.getBinding().aFilters;
				var existingArchivedTableFilters = oThis.WCMT_MainTable_archived.getBinding().aFilters;

				if (existingMainTableFilters && existingMainTableFilters.length > 0) {
					for (var k = 0; k < existingMainTableFilters.length; k++) {
						aFilters.push(existingMainTableFilters[k]);
					}
				}
				if (existingArchivedTableFilters && existingArchivedTableFilters.length > 0) {
					for (var l = 0; l < existingArchivedTableFilters.length; l++) {
						aArchivedFilter.push(existingArchivedTableFilters[l]);
					}
				}
			}
			oThis.WCMT_MainTable.getBinding().filter(aFilters);
			oThis.WCMT_MainTable_archived.getBinding().filter(aArchivedFilter);
			oThis.WCMT_MainTable.setVisibleRowCount(oThis.WCMT_MainTable.getBinding().getLength());
			oThis.WCMT_MainTable_archived.setVisibleRowCount(oThis.WCMT_MainTable_archived.getBinding().getLength());
		},

		/**
		 * Handles the column filtering
		 * @function onTableFilter
		 * @param oEvent 
		 */
		onTableFilter: function (oEvent) {
			setTimeout(function () {
				oThis._filterTables(true);
			}, 0);
		},

		/**
		 * Remove filter and Sorter from tables
		 * @function _refreshFiltersSorters
		 */
		_refreshFiltersSorters: function () {
			var mainTableTotalColumns = oThis.WCMT_MainTable.getColumns().length;
			var archivedTableTotalColumns = oThis.WCMT_MainTable_archived.getColumns().length;
			for (var i = 0; i < mainTableTotalColumns; i++) {
				oThis.WCMT_MainTable.getColumns()[i].setSorted(false);
				oThis.WCMT_MainTable.getColumns()[i].setFilterValue("");
				oThis.WCMT_MainTable.getColumns()[i].setFiltered(false);
			}
			for (var j = 0; j < archivedTableTotalColumns; j++) {
				oThis.WCMT_MainTable_archived.getColumns()[j].setSorted(false);
				oThis.WCMT_MainTable_archived.getColumns()[j].setFilterValue("");
				oThis.WCMT_MainTable_archived.getColumns()[j].setFiltered(false);
			}
		}
	});
});