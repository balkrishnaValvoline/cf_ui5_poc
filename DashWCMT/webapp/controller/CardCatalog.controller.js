sap.ui.define([
	"valvoline/dash/DashWCMT/controller/Base.controller"
], function (Base) {
	"use strict";
	var _oRouter;
	return Base.extend("valvoline.dash.DashWCMT.controller.CardCatalog", {
		/** @module CardCatalog */
		/**
		 * Initalizes the controller. Attaches an event that fires everytime the route for the cardCatalog page is matched
		 * @function onInit
		 */
		onInit: function () {
			_oRouter = this.getRouter();
			_oRouter.getRoute("cardCatalog").attachPatternMatched(this._onObjectMatched, this);
		},
		/**
		 * Is fired everytime the user navs to this page, and checks if he is authorized to access it
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {
			this.checkAuthorization();
			// Check if the local model was loaded
			if (sap.ui.getCore().getModel("local") === undefined) {
				this.getRouter().navTo("cardManager");
				return;
			} 
		},

		/**
		 * Navs to the entryform creation page
		 * @function editEntryForm
		 */
		editEntryForm: function () {
			_oRouter.navTo("entryform", {
				id: "New"
			});
		},
		
		/**
		 * Navs to the staticCardEntryForm creation page
		 * @function editStaticForm
		 */
		editStaticForm: function () {
			_oRouter.navTo("staticcardentryform", {
				id: "New"
			});
		},

		/**
		 * Navs to the products content card creation page
		 * @function toProductsContentCard
		 */
		toProductsContentCard: function () {
			_oRouter.navTo("productscontentcard", {
				id: "New"
			});
		},

		/**
		 * Navs to the general entry form creation page
		 * @function toGeneralCard
		 */
		toGeneralCard: function () {
			_oRouter.navTo("generalentryform", {
				id: "New"
			});
		},

		/**
		 * Navs to the hero card form creation page
		 * @function toHeroCardForm
		 */
		toHeroCardForm: function () {
			_oRouter.navTo("herocardform", {
				id: "New"
			});
		},

		/**
		 * Navs to the media carousel card form creation page
		 * @function toMediaCarouselCardForm
		 */
		toMediaCarouselCardForm: function () {
			_oRouter.navTo("mediacarouselcardform", {
				id: "New"
			});
		},

		/**
		 * Navs to the text carousel card form creation page
		 * @function toTextCarouselCardForm
		 */
		toTextCarouselCardForm: function () {
			_oRouter.navTo("textcarouselcardform", {
				id: "New"
			});
		}

	});
});