/**
 * In this function are all the cards showed in the Feed View.
 * Each card as a ID:
 * Card Type ('100' - Feed, '200' - Image, '300' - Video, '400' - Product, - '500' - Hero, '600' - List,  '700' - Carousel
 * @patricia.barros and @joao.alves.ferreira
 */

jQuery.sap.declare("valvoline.dash.portal.DashPortalWeb.controls.Card");
jQuery.sap.require("sap.m.CustomTile");

var _BaseController;
sap.m.CustomTile.extend("valvoline.dash.portal.DashPortalWeb.controls.Card", {
	metadata: {
		properties: {
			"CardType": {
				type: "int",
				defaultValue: 1
			},
			"CardTitle": {
				type: "string",
				defaultValue: ""
			},
			"CardShortText": {
				type: "string",
				defaultValue: ""
			},
			"CardLongText": {
				type: "string",
				defaultValue: ""
			},
			"CardLongTextItem1Label": {
				type: "string",
				defaultValue: ""
			},
			"CardLongTextItem1": {
				type: "string",
				defaultValue: ""
			},
			"CardLongTextItem2Label": {
				type: "string",
				defaultValue: ""
			},
			"CardLongTextItem2": {
				type: "string",
				defaultValue: ""
			},
			"CardLongTextItem3Label": {
				type: "string",
				defaultValue: ""
			},
			"CardLongTextItem3": {
				type: "string",
				defaultValue: ""
			},
			"CardLongTextItem4Label": {
				type: "string",
				defaultValue: ""
			},
			"CardLongTextItem4": {
				type: "string",
				defaultValue: ""
			},
			"CardLongTextItem5Label": {
				type: "string",
				defaultValue: ""
			},
			"CardLongTextItem5": {
				type: "string",
				defaultValue: ""
			},
			"CardLongTextItem6": {
				type: "string",
				defaultValue: ""
			},
			"CardUrl": {
				type: "string",
				defaultValue: ""
			},
			"CardMediaURL": {
				type: "string",
				defaultValue: ""
			},
			"CardTimeStamp": {
				type: "string",
				defaultValue: ""
			},
			"CardSection": {
				type: "string",
				defaultValue: ""
			},
			"CardButtonText": {
				type: "string",
				defaultValue: ""
			},
			"CardButtonAction": {
				type: "string",
				defaultValue: ""
			},
			"CardButtonTarget": {
				type: "string",
				defaultValue: ""
			},
			"CardId": {
				type: "string"
			},
			"CardSections": {
				type: "object[]"
			},
			"CardCategory": {
				type: "string"
			},
			"HeaderStatus": {
				type: "string"
			},
			"CardNavigationType": {
				type: "string"
			},
			"viewContext": {
				type: "object"
			}
		},
		events: {
			"press": {},
			"gtmHandling": {
				parameters: {
					callToActionURL: {
						type: "string"
					}
				}
			}
		}
	},
	/** @module Card */
	/**
	 * Renders the cards (format, data and style) in the card container
	 * @function renderer
	 * @param {Object} rm - render manager
	 * @param {Object} oControl - element to render
	 */
	renderer: function (rm, oControl) {

		var BaseController = sap.ui.require("valvoline/dash/portal/DashPortalWeb/controller/Base.controller");
		_BaseController = new BaseController();

		switch (oControl.getCardType()) {

			// FEED CARD
		case 100:
			rm.write("<div tabindex=\"0\"");
			rm.writeControlData(oControl);
			rm.addClass("reusable-card-container");
			rm.addClass("feedCardTile");
			rm.addClass("sapMCustomTile");
			rm.addClass("reusable-cursor-pointer-modifier");
			rm.writeClasses();
			if (oControl._invisible) {
				rm.addStyle("visibility", "hidden");
				rm.writeStyles();
			}

			/* WAI ARIA if in TileContainer context */
			if (oControl.getParent() instanceof sap.m.TileContainer) {
				rm.writeAccessibilityState({
					role: "option",
					posinset: oControl._getTileIndex(),
					setsize: oControl._getTilesCount()
				});
			}

			rm.write(">");

			rm.write("<div class='sapMVbox feedCard'>");
			rm.write("<div class='feedCardDiv'>");
			//Title

			var HBoxTitle = new sap.m.HBox().addStyleClass("shortTextContainer");
			var Titletext = new sap.m.Text({
				text: oControl.getCardTitle(),
				maxLines: 2
			}).addStyleClass("pageTitle info feedCardTitle reusable-cursor-pointer-modifier");
			HBoxTitle.addItem(Titletext);
			rm.renderControl(HBoxTitle);

			//TimeStamp

			//format timeStamp
			var formattedTimestamp;
			if (oControl.getCardTimeStamp() !== "") {
				formattedTimestamp = oControl.formatTimeStamp(oControl.getCardTimeStamp());
			} else {
				formattedTimestamp = "";
			}

			rm.write("<h4 class='info feedCardTimeStamp'>");
			rm.write(formattedTimestamp);
			rm.write("</h4>");
			rm.write("</div>");

			// IMAGE SECTION
			rm.write("<div class='reusable-card-image'>");
			if (oControl.getCardMediaURL()) {
				rm.write("<img class='reusable-card-image'src=");
				rm.write(oControl.getCardMediaURL());
				rm.write("></img>");
			}
			rm.write("</div>");

			// INFORMATION SECTION
			rm.write("<div class='sapMVBox feedCardContent'>");

			// SUBTITLE SECTION
			if (!oControl.getHeaderStatus()) {
				var HBoxSubt = new sap.m.HBox({
					height: "78px"
				});
				var Text = new sap.m.Text({
					text: oControl.getCardShortText(),
					tooltip: oControl.getCardShortText(),
					maxLines: 3
				}).addStyleClass("pageSubtitle card-subtitle reusable-cursor-pointer-modifier");
				HBoxSubt.addItem(Text);
				rm.renderControl(HBoxSubt);
			} else {

				var iValue = 0;
				iValue = _BaseController.orderStatusValue(oControl.getHeaderStatus());

				var oShapeContainer = new sap.m.VBox().addStyleClass("order-status-shape-container");
				var oShapeStatus = new sap.suite.ui.commons.statusindicator.StatusIndicator({
					width: "116px",
					height: "18px",
					value: iValue
				});
				oShapeContainer.addItem(oShapeStatus);
				var oShapeGroupOrderReceived = new sap.suite.ui.commons.statusindicator.ShapeGroup();
				var oCustomShapeOrderReceived = new sap.suite.ui.commons.statusindicator.CustomShape({
					definition: _BaseController.getKey("keys_svg", "keyShape_orderReceived"),
					strokeWidth: 0.149,
					strokeColor: "black",
					x: 0,
					y: 2,
					width: "18px",
					fillingType: "Linear"
				});
				// fix for mobile and tablet (iOs) - no filling
				if (sap.ui.Device.os.ios && (sap.ui.Device.system.phone || sap.ui.Device.system.tablet)) {
					if (oControl.getHeaderStatus() === "RECEIVED" || oControl.getHeaderStatus() === "IN PROCESS" || oControl.getHeaderStatus() ===
						"SHIPPED") {
						oCustomShapeOrderReceived.setFillColor("#009cd9");
						oCustomShapeOrderReceived.setFillingType("None");
					}
				}
				oShapeGroupOrderReceived.addShape(oCustomShapeOrderReceived);

				var oShapeGroupArrow1 = new sap.suite.ui.commons.statusindicator.ShapeGroup();
				var oCustomShapeArrow1 = new sap.suite.ui.commons.statusindicator.CustomShape({
					definition: _BaseController.getKey("keys_svg", "keyShape_rightArrow"),
					strokeWidth: 0.149,
					strokeColor: "black",
					x: 23,
					y: 0,
					width: "16px",
					fillingType: "Linear"
				});
				if (sap.ui.Device.os.ios && (sap.ui.Device.system.phone || sap.ui.Device.system.tablet)) {
					if (oControl.getHeaderStatus() === "IN PROCESS" || oControl.getHeaderStatus() === "SHIPPED") {
						oCustomShapeArrow1.setFillColor("#009cd9");
						oCustomShapeArrow1.setFillingType("None");
					}
				}
				oShapeGroupArrow1.addShape(oCustomShapeArrow1);

				var oShapeGroupInProcess = new sap.suite.ui.commons.statusindicator.ShapeGroup();
				var oCustomShapeInProcess = new sap.suite.ui.commons.statusindicator.CustomShape({
					definition: _BaseController.getKey("keys_svg", "keyShape_inProcess"),
					strokeWidth: 0.149,
					strokeColor: "black",
					x: 44,
					y: 0,
					width: "18px",
					fillingType: "Linear"
				});
				if (sap.ui.Device.os.ios && (sap.ui.Device.system.phone || sap.ui.Device.system.tablet)) {
					if (oControl.getHeaderStatus() === "IN PROCESS" || oControl.getHeaderStatus() === "SHIPPED") {
						oCustomShapeInProcess.setFillColor("#009cd9");
						oCustomShapeInProcess.setFillingType("None");
					}
				}
				oShapeGroupInProcess.addShape(oCustomShapeInProcess);

				var oShapeGroupArrow2 = new sap.suite.ui.commons.statusindicator.ShapeGroup();
				var oCustomShapeArrow2 = new sap.suite.ui.commons.statusindicator.CustomShape({
					definition: _BaseController.getKey("keys_svg", "keyShape_rightArrow"),
					strokeWidth: 0.149,
					strokeColor: "black",
					x: 67,
					y: 0,
					width: "16px",
					fillingType: "Linear"
				});
				if (sap.ui.Device.os.ios && (sap.ui.Device.system.phone || sap.ui.Device.system.tablet)) {
					if (oControl.getHeaderStatus() === "SHIPPED") {
						oCustomShapeArrow2.setFillColor("#009cd9");
						oCustomShapeArrow2.setFillingType("None");
					}
				}
				oShapeGroupArrow2.addShape(oCustomShapeArrow2);

				var oShapeGroupShipped = new sap.suite.ui.commons.statusindicator.ShapeGroup();
				var oCustomShapeShipped = new sap.suite.ui.commons.statusindicator.CustomShape({
					definition: _BaseController.getKey("keys_svg", "keyShape_shipped"),
					strokeWidth: 0.149,
					strokeColor: "black",
					x: 88,
					y: 0,
					width: "18px",
					fillingType: "Linear"
				});
				if (sap.ui.Device.os.ios && (sap.ui.Device.system.phone || sap.ui.Device.system.tablet)) {
					if (oControl.getHeaderStatus() === "SHIPPED") {
						oCustomShapeShipped.setFillColor("#009cd9");
						oCustomShapeShipped.setFillingType("None");
					}
				}
				oShapeGroupShipped.addShape(oCustomShapeShipped);

				oShapeStatus.addGroup(oShapeGroupOrderReceived);
				oShapeStatus.addGroup(oShapeGroupArrow1);
				oShapeStatus.addGroup(oShapeGroupInProcess);
				oShapeStatus.addGroup(oShapeGroupArrow2);
				oShapeStatus.addGroup(oShapeGroupShipped);

				if (!(sap.ui.Device.os.ios && (sap.ui.Device.system.phone || sap.ui.Device.system.tablet))) {
					var oShapePropertyThreshold1 = new sap.suite.ui.commons.statusindicator.PropertyThreshold({
						fillColor: "#009cd9",
						toValue: 100
					});
					var oShapePropertyThreshold2 = new sap.suite.ui.commons.statusindicator.PropertyThreshold({
						fillColor: "#908c8c",
						toValue: 0
					});

					oShapeStatus.addPropertyThreshold(oShapePropertyThreshold1);
					oShapeStatus.addPropertyThreshold(oShapePropertyThreshold2);
				}

				rm.write("<div class='shortTextContainer' style='height:78px'>");
				rm.write("<h2 class='sapMText pageSubtitle ordersFeedCardShorText reusable-cursor-pointer-modifier'>");
				rm.write(oControl.getCardShortText());
				rm.write("</h2>");
				rm.renderControl(oShapeContainer);
				rm.write("</div>");
			}

			// DESCRIPTION SECTION
			if (oControl.getCardLongText() !== "") {
				var HBoxDesc = new sap.m.HBox();
				var TextDesc = new sap.m.Text({
					text: oControl.getCardLongText(),
					tooltip: oControl.getCardLongText(),
					maxLines: 7
				}).addStyleClass(
					"bodyCopy feedCardLongText card-description feedCardLongTextCasesOrders reusable-cursor-pointer-modifier");
				HBoxDesc.addItem(TextDesc);
				rm.renderControl(HBoxDesc);
			} else {
				rm.write("<div class='sapMVBox sapMFlexBox sapMFlexItem feedCardLongText feedCardLongTextCasesOrders'>");
				if (oControl.getCardLongTextItem1Label() !== "" && oControl.getCardLongTextItem1() !== "") {

					// Creates a VBox that contains the information
					rm.write("<div class='sapMText reusable-cursor-pointer-modifier'>");

					// P.O NUMBER
					rm.write("<p");
					if (oControl.getCardCategory() === "Order" || oControl.getCardCategory() === "Case") {
						rm.addClass("bodyCopyBold");
						rm.addClass("feedCardCaseOrderInfoHeight");
					} else {
						rm.addClass("bodyCopyBold");
						rm.addClass("marginBottomCardLongTextItem1Label");
					}
					rm.addClass("sapMText reusable-cursor-pointer-modifier");
					rm.addClass("sapUiTinyMarginEnd");

					rm.writeClasses();
					rm.write(">");
					rm.write(oControl.getCardLongTextItem1Label());
					rm.write("</p>");

					rm.write("<p");
					if (oControl.getCardCategory() === "Order" || oControl.getCardCategory() === "Case") {
						rm.addClass("bodyCopy");
						rm.addClass("feedCardCaseOrderInfoHeight");
					} else {
						rm.addClass("bodyCopy");
						rm.addClass("marginBottomCardLongTextItem1Label paddingTopCardLongTextItem1Label");
					}
					rm.addClass("sapMText reusable-cursor-pointer-modifier");
					rm.writeClasses();
					rm.write(">");
					rm.write(oControl.getCardLongTextItem1());
					rm.write("</p>");
					rm.write("</div>");
				}

				// Valvoline Order Number or Date
				if (oControl.getCardLongTextItem5() !== null && oControl.getCardLongTextItem5() !== "" && oControl.getCardLongTextItem5() !==
					undefined) {
					if (oControl.getCardLongTextItem5Label !== "" && oControl.getCardLongTextItem5 !== "") {
						rm.write("<div class='flexDiv sapMFlexBox sapMFlexItem'>");
						rm.write("<p");
						if (oControl.getCardCategory() === "Order" || oControl.getCardCategory() === "Case") {
							rm.addClass("bodyCopyBold");
							rm.addClass("feedCardCaseOrderInfoHeight");
						} else {
							rm.addClass("bodyCopyBold");
							rm.addClass("marginTopCardLongTextItem2Label");
						}

						rm.addClass("sapMText reusable-cursor-pointer-modifier");
						rm.addClass("sapUiTinyMarginEnd");
						rm.writeClasses();
						rm.write(">");
						rm.write(oControl.getCardLongTextItem5Label());
						rm.write("</p>");
						rm.write("<p");
						if (oControl.getCardCategory() === "Order" || oControl.getCardCategory() === "Case") {
							rm.addClass("bodyCopy");
							rm.addClass("feedCardCaseOrderInfoHeight");
						} else {
							rm.addClass("bodyCopy");
							rm.addClass("marginTopCardLongTextItem2Label");
						}
						rm.addClass("sapMText reusable-cursor-pointer-modifier");
						rm.writeClasses();
						rm.write(">");
						rm.write(oControl.getCardLongTextItem5());
						rm.write("</p>");
						rm.write("</div>");
					}
				} else {
					if (oControl.getCardLongTextItem2Label() !== "" && oControl.getCardLongTextItem2() !== "") {
						rm.write("<div class='flexDiv sapMFlexBox sapMFlexItem'>");
						rm.write("<p");
						if (oControl.getCardCategory() === "Order" || oControl.getCardCategory() === "Case") {
							rm.addClass("bodyCopyBold");
							rm.addClass("feedCardCaseOrderInfoHeight");
						} else {
							rm.addClass("bodyCopyBold");
							rm.addClass("marginTopCardLongTextItem2Label");
						}
						rm.addClass("sapMText reusable-cursor-pointer-modifier");
						rm.addClass("sapUiTinyMarginEnd");
						rm.writeClasses();
						rm.write(">");
						rm.write(oControl.getCardLongTextItem2Label());
						rm.write("</p>");
						rm.write("<p");
						if (oControl.getCardCategory() === "Order" || oControl.getCardCategory() === "Case") {
							rm.addClass("bodyCopy");
							rm.addClass("feedCardCaseOrderInfoHeight");
						} else {
							rm.addClass("bodyCopy");
							rm.addClass("marginTopCardLongTextItem2Label");
						}
						rm.addClass("sapMText reusable-cursor-pointer-modifier");
						rm.writeClasses();
						rm.write(">");
						if (oControl.getCardCategory() === "Case") {
							rm.write(_BaseController.formatDateUTCtoLocal(oControl.getCardLongTextItem2()));
						} else {
							rm.write(oControl.getCardLongTextItem2());
						}
						rm.write("</p>");
						rm.write("</div>");
					}
				}

				// Est. Date delivery
				if (oControl.getCardLongTextItem3Label() !== "" && oControl.getCardLongTextItem3() !== "") {
					rm.write("<div class='flexDiv sapMFlexBox sapMFlexItem'>");
					rm.write("<p");
					if (oControl.getCardCategory() === "Order" || oControl.getCardCategory() === "Case") {
						rm.addClass("bodyCopyBold");
						rm.addClass("feedCardCaseOrderInfoHeight");
					} else {
						rm.addClass("bodyCopyBold");
					}
					rm.addClass("sapMText reusable-cursor-pointer-modifier");
					rm.addClass("sapUiTinyMarginEnd");
					rm.writeClasses();
					rm.write(">");
					rm.write(oControl.getCardLongTextItem3Label());
					rm.write("</p>");
					rm.write("<p");
					if (oControl.getCardCategory() === "Order" || oControl.getCardCategory() === "Case") {
						rm.addClass("bodyCopy");
						rm.addClass("feedCardCaseOrderInfoHeight");
					} else {
						rm.addClass("bodyCopy");
					}
					rm.addClass("sapMText reusable-cursor-pointer-modifier");
					rm.writeClasses();
					rm.write(">");
					if (oControl.getCardCategory() === "Order") {
						rm.write(_BaseController.formatDate2EstOrd(oControl.getCardLongTextItem3(), oControl.getCardLongTextItem6()));
					} else {
						rm.write(oControl.getCardLongTextItem3());
					}
					rm.write("</p>");
					rm.write("</div>");
				}
				if (oControl.getCardLongTextItem4Label() !== "" && oControl.getCardLongTextItem4() !== "") {
					rm.write("<div class='sapMHBox sapMFlexBox sapMFlexItem'>");
					rm.write("<p");
					if (oControl.getCardCategory() === "Order" || oControl.getCardCategory() === "Case") {
						rm.addClass("bodyCopyBold");
						rm.addClass("feedCardCaseOrderInfoHeight");
					} else {
						rm.addClass("bodyCopyBold");
					}
					rm.addClass("sapMText reusable-cursor-pointer-modifier");
					rm.addClass("sapUiTinyMarginEnd");
					rm.writeClasses();
					rm.write(">");
					rm.write(oControl.getCardLongTextItem4Label());
					rm.write("</p>");
					rm.write("<p");
					if (oControl.getCardCategory() === "Order" || oControl.getCardCategory() === "Case") {
						rm.addClass("bodyCopy");
						rm.addClass("feedCardCaseOrderInfoHeight");
					} else {
						rm.addClass("bodyCopy");
					}
					rm.addClass("sapMText reusable-cursor-pointer-modifier");
					rm.writeClasses();
					rm.write(">");
					rm.write(oControl.getCardLongTextItem4());
					rm.write("</p>");
					rm.write("</div>");
				}
				rm.write("</div>");
			}

			rm.write("</div>");

			//
			// attach press event to the parent container control
			//
			oControl.attachPress(function (oEvent) {
				oControl.cardBtnpress(this, oControl.getCardButtonTarget(), oControl.getCardButtonAction(), oControl.getCardCategory());

				//GTM
				_BaseController.GTMDataLayer('feedcrdbtnclck',
					'Feed Card-click',
					oControl.getCardTitle(),
					oControl.getCardShortText()
				);
			});

			rm.write("</div>");
			rm.write("</div>");
			break;

			// Image card
		case 200:

			rm.write("<div tabindex=\"0\"");
			rm.writeControlData(oControl);
			rm.addClass("reusable-card-container");
			rm.addClass("reusable-image-card-container");
			rm.addClass("sapMCustomTile");
			rm.addClass("reusable-cursor-pointer-modifier");
			rm.writeClasses();
			if (oControl._invisible) {
				rm.addStyle("visibility", "hidden");
				rm.writeStyles();
			}

			/* WAI ARIA if in TileContainer context */
			if (oControl.getParent() instanceof sap.m.TileContainer) {
				rm.writeAccessibilityState({
					role: "option",
					posinset: oControl._getTileIndex(),
					setsize: oControl._getTilesCount()
				});
			}

			rm.write(">");

			rm.write("<div class='sapMVbox imageCardImageContainer'>");
			//Image
			if (oControl.getCardMediaURL()) {
				rm.write("<img class='reusable-card-image' src=");
				rm.write(oControl.getCardMediaURL());
				rm.write("></img>");
			}

			rm.write("<div class='sapMVBox imgCardContent'>");

			// SUBTITLE SECTION
			var HBox = new sap.m.HBox({
				renderType: "Bare",
				height: "78px"
			});
			Text = new sap.m.Text({
				text: oControl.getCardShortText(),
				maxLines: 3,
				tooltip: oControl.getCardShortText()
			}).addStyleClass("pageSubtitle card-subtitle reusable-cursor-pointer-modifier");
			HBox.addItem(Text);
			rm.renderControl(HBox);

			var oDescLinkContainer = new sap.m.VBox({
				justifyContent: "SpaceBetween",
				renderType: "Bare",
				height: "auto"
			});
			// DESCRIPTION SECTION
			HBox = new sap.m.HBox({
				renderType: "Bare"
			});
			Text = new sap.m.Text({
				text: oControl.getCardLongText(),
				tooltip: oControl.getCardLongText(),
				maxLines: 7
			}).addStyleClass("bodyCopy feedCardLongText reusable-cursor-pointer-modifier");
			HBox.addItem(Text);
			oDescLinkContainer.addItem(HBox);

			//Link
			//div to wrap up link
			var oLinkHBox = new sap.m.HBox({
				renderType: "Bare",
				height: "44px"
			}).addStyleClass("imageCardLink");
			var link;
			if ((oControl.getCardButtonText() === "View All" && oControl.getCardTitle() === "LATEST NEWS") || (oControl.getCardButtonText() ===
					_BaseController.getResourceBundle().getText("RCCallToActionLabel2") && oControl.getCardTitle() === _BaseController.getResourceBundle()
					.getText("RCTitle"))) {
				link = new sap.m.Link({
					text: oControl.getCardButtonText()
				}).addStyleClass("links valvolineLink");

				//
				// attach the press event to the parent control
				//
				oControl.attachPress(function (oEvent) {
					oControl.cardBtnpress(this, oControl.getCardButtonTarget(), oControl.getCardButtonAction(), oControl.getCardCategory());

					//GTM
					if (oControl.getParent().getId().indexOf("cardContainerLearnVU") !== -1) {
						_BaseController.GTMDataLayer('learncrdslnkclick',
							'Valvoline University',
							oControl.getCardShortText() + '- ' + oControl.getCardButtonText().replace(/\s+/g, '') + ' -click',
							oControl.getCardButtonAction()
						);
					} else if (oControl.getParent().getId().indexOf("cardContainerService") !== -1) {

						_BaseController.GTMDataLayer('solutncrdslnkclick',
							'Solutions',
							oControl.getCardShortText() + '- ' + oControl.getCardButtonText() + ' -click',
							"#/" + oControl.getCardButtonAction()
						);

					} else if (oControl.getParent().getId().indexOf("cardContainerPromote") !== -1) {
						_BaseController.GTMDataLayer('promtcrdlinkclick',
							'Promote',
							oControl.getCardShortText() + '- ' + oControl.getCardButtonText().replace(/\s+/g, '') + ' -click',
							oControl.getCardButtonAction()
						);
					} else if (oControl.getParent().getId().indexOf("cardContainerPromotionsSummary") !== -1) {
						_BaseController.GTMDataLayer('promosumrycrdclck',
							'Promotions Summary',
							oControl.getCardShortText() + '- ' + oControl.getCardButtonText().replace(/\s+/g, '') + ' -click',
							oControl.getCardButtonAction()
						);
					} //END GTM
				});
			} else {
				if (oControl.getCardButtonTarget() === "New Tab" || oControl.getCardButtonTarget() === "New tab") {
					link = new sap.m.Link({
						text: oControl.getCardButtonText()
					}).addStyleClass("links valvolineLink");

					//
					// attach the press event to the parent control
					//
					oControl.attachPress(function (oEvent) {
						//
						// route via a function press instead of a link
						//
						oControl.cardBtnpress(this, oControl.getCardButtonTarget(), oControl.getCardButtonAction(), oControl.getCardCategory());

						//GTM
						if (oControl.getParent().getId().indexOf("cardContainerLearnVU") !== -1) {
							_BaseController.GTMDataLayer('learncrdslnkclick',
								'Valvoline University',
								oControl.getCardShortText() + '- ' + oControl.getCardButtonText().replace(/\s+/g, '') + ' -click',
								oControl.getCardButtonAction()
							);
						} else if (oControl.getParent().getId().indexOf("cardContainerService") !== -1) {
							_BaseController.GTMDataLayer('solutncrdslnkclick',
								'Solutions',
								oControl.getCardShortText() + '- ' + oControl.getCardButtonText() + ' -click',
								oControl.getCardButtonAction()
							);

						} else if (oControl.getParent().getId().indexOf("cardContainerPromote") !== -1) {
							_BaseController.GTMDataLayer('promtcrdlinkclick',
								'Promote',
								oControl.getCardShortText() + '- ' + oControl.getCardButtonText().replace(/\s+/g, '') + ' -click',
								oControl.getCardButtonAction()
							);
						} else if (oControl.getParent().getId().indexOf("cardContainerPromotionsSummary") !== -1) {
							_BaseController.GTMDataLayer('promosumrycrdclck',
								'Promotions Summary',
								oControl.getCardShortText() + '- ' + oControl.getCardButtonText().replace(/\s+/g, '') + ' -click',
								oControl.getCardButtonAction()
							);
						} //END GTM
					});
				} else {
					link = new sap.m.Link({
						text: oControl.getCardButtonText()
					}).addStyleClass("links valvolineLink");
					//
					// attach the press event to the parent control
					//
					oControl.attachPress(function (oEvent) {

						//GTM
						if (oControl.getParent().getId().indexOf("cardContainerLearnVU") !== -1) {
							_BaseController.GTMDataLayer('learncrdslnkclick',
								'Valvoline University',
								oControl.getCardShortText() + '- ' + oControl.getCardButtonText().replace(/\s+/g, '') + ' -click',
								oControl.getCardButtonAction()
							);
						} else if (oControl.getParent().getId().indexOf("cardContainerService") !== -1) {

							_BaseController.GTMDataLayer('solutncrdslnkclick',
								'Solutions',
								oControl.getCardShortText() + '- ' + oControl.getCardButtonText() + ' -click',
								"#/" + oControl.getCardButtonAction()
							);

						} else if (oControl.getParent().getId().indexOf("cardContainerPromote") !== -1) {
							_BaseController.GTMDataLayer('promtcrdlinkclick',
								'Promote',
								oControl.getCardShortText() + '- ' + oControl.getCardButtonText().replace(/\s+/g, '') + ' -click',
								oControl.getCardButtonAction()
							);
						} else if (oControl.getParent().getId().indexOf("cardContainerPromotionsSummary") !== -1) {
							_BaseController.GTMDataLayer('promosumrycrdclck',
								'Promotions Summary',
								oControl.getCardShortText() + '- ' + oControl.getCardButtonText().replace(/\s+/g, '') + ' -click',
								oControl.getCardButtonAction()
							);
						} //END GTM

						//
						// route via a function press instead of a link
						//
						oControl.cardBtnpress(this, oControl.getCardButtonTarget(), oControl.getCardButtonAction(), oControl.getCardCategory());
					});

				}
			}
			rm.renderControl(oDescLinkContainer);
			//close wrapping link div
			rm.write("</div>");

			rm.write("</div>");

			oLinkHBox.addItem(link);
			rm.renderControl(oLinkHBox);
			rm.write("</div>");
			break;

			// Video Card
		case 300:

			rm.write("<div tabindex=\"0\"");
			rm.writeControlData(oControl);
			rm.addClass("reusable-card-container");
			rm.addClass("reusable-image-card-container");
			rm.addClass("reusable-cursor-pointer-modifier");
			rm.addClass("sapMCustomTile");
			rm.writeClasses();
			if (oControl._invisible) {
				rm.addStyle("visibility", "hidden");
				rm.writeStyles();
			}

			/* WAI ARIA if in TileContainer context */
			if (oControl.getParent() instanceof sap.m.TileContainer) {
				rm.writeAccessibilityState({
					role: "option",
					posinset: oControl._getTileIndex(),
					setsize: oControl._getTilesCount()
				});
			}

			rm.write(">");

			rm.write("<div class='sapMVbox'>");
			//Video
			if (oControl.getCardMediaURL()) {
				rm.write(
					"<iframe class='videoForStoping' webkitallowfullscreen mozallowfullscreen allowfullscreen allowtransparency='true' scrolling='no' src="
				);
				rm.write(oControl.getCardMediaURL());
				rm.write("></iframe>");

			}

			rm.write("<div class='sapMVBox imgCardContent'>");
			//ShortText
			// SUBTITLE SECTION
			HBox = new sap.m.HBox({
				renderType: "Bare",
				height: "78px"
			});
			Text = new sap.m.Text({
				text: oControl.getCardShortText(),
				maxLines: 3,
				tooltip: oControl.getCardShortText()
			}).addStyleClass("pageSubtitle  card-subtitle reusable-cursor-pointer-modifier");
			HBox.addItem(Text);
			rm.renderControl(HBox);

			// DESCRIPTION SECTION
			HBox = new sap.m.HBox({
				renderType: "Bare"
			});
			Text = new sap.m.Text({
				text: oControl.getCardLongText(),
				tooltip: oControl.getCardLongText(),
				maxLines: 7
			}).addStyleClass("bodyCopy feedCardLongText card-description reusable-cursor-pointer-modifier");
			HBox.addItem(Text);
			rm.renderControl(HBox);

			// closing of imgCardContent by Ruther So 05/31/17
			rm.write("</div>");

			//Link
			oLinkHBox = new sap.m.HBox({
				renderType: "Bare",
				height: "44px"
			}).addStyleClass("imageCardLink");

			link = new sap.m.Link({
				text: oControl.getCardButtonText()
			}).addStyleClass("links valvolineLink");
			oLinkHBox.addItem(link);
			rm.renderControl(oLinkHBox);

			//
			// attach the press event to the parent control
			//
			oControl.attachPress(function (oEvent) {
				//
				// route via a function press instead of a link
				//
				oControl.cardBtnpress(this, oControl.getCardButtonTarget(), oControl.getCardButtonAction(), oControl.getCardCategory());

				//GTM
				if (oControl.getParent().getId().indexOf("cardContainerService") !== -1) {
					_BaseController.GTMDataLayer('solutncrdslnkclick',
						'Solutions',
						oControl.getCardShortText() + '- ' + oControl.getCardButtonText() + ' -click',
						oControl.getCardButtonAction()
					);

				}
			});

			rm.write("</div>");
			rm.write("</div>");
			break;

			// Product card
		case 400:

			rm.write("<div tabindex=\"0\"");
			rm.writeControlData(oControl);
			rm.addClass("productTile");
			rm.writeClasses();
			if (oControl._invisible) {
				rm.addStyle("visibility", "hidden");
				rm.writeStyles();
			}

			/* WAI ARIA if in TileContainer context */
			if (oControl.getParent() instanceof sap.m.TileContainer) {
				rm.writeAccessibilityState({
					role: "option",
					posinset: oControl._getTileIndex(),
					setsize: oControl._getTilesCount()
				});
			}
			rm.write(">");

			//Image
			rm.write("<div class='sapMVbox'>");
			rm.write("<center>");
			if (oControl.getCardMediaURL()) {
				rm.write("<img class='productTilePicture' src=");
				rm.write(oControl.getCardMediaURL());
				rm.write("></img>");
			}
			rm.write("<div class='sapMVBox productCardContent'>");

			//ShortText
			rm.write("<div class='fixShortTextHeight shortTextContainer'>");
			//div to wrap up card short text
			rm.write("<h2 class='sapMText pageSubtitle feedCardShorText'>");
			rm.write(oControl.getCardShortText());
			rm.write("</h2>");

			//Long Text
			rm.write("</div>");
			//div to wrap up long text
			rm.write("<div class='productTileWrapLongText'>");
			rm.write("<p class='sapMText bodyCopy'");
			rm.write(">");
			// Removed 01/06
			rm.write(oControl.getCardLongText().substring(0, 50) + " ...");
			rm.write("</p>");
			rm.write("</div>");

			rm.write("</div>");
			rm.write("</div>");
			rm.write("</center>");
			rm.write("</div>");
			break;

			// List card
		case 600:

			var sections = oControl.getCardSections();
			//draw list card only if there's sections
			if (sections !== null && sections.length !== 0) {
				rm.write("<div tabindex=\"0\"");
				rm.writeControlData(oControl);
				rm.addClass("listTile");
				rm.addClass("sapMCustomTile");
				rm.writeClasses();
				if (oControl._invisible) {
					rm.addStyle("visibility", "hidden");
					rm.writeStyles();
				}

				// WAI ARIA if in TileContainer context
				if (oControl.getParent() instanceof sap.m.TileContainer) {
					rm.writeAccessibilityState({
						role: "option",
						posinset: oControl._getTileIndex(),
						setsize: oControl._getTilesCount()
					});
				}

				rm.write(">");

				//Title
				rm.write("<div class='listCardTitle'>");
				rm.write("<h2 class='sapMText pageSubtitle'>");
				rm.write(oControl.getCardTitle());
				rm.write("</h2>");
				rm.write("</div>");

				var cardShortText, cardLongText, cardCallToActionLabel, cardCallToActionURL;

				for (var i = 0; i < sections.length > 0; i++) {

					cardShortText = sections[i].subtitle;
					cardLongText = sections[i].description;
					cardCallToActionURL = sections[i].callToActionLabelURL;
					cardCallToActionLabel = sections[i].callToActionLabel;

					//don't draw border when is the last item
					if (i === sections.length - 1) {
						rm.write("<div class='sapMVbox listCardItemNoBorder'>");
					} else {
						rm.write("<div class='sapMVbox listCardItemBorder'>");
					}

					rm.write("<div class='sapMVBox'>");
					//ShortText
					rm.write("<h3 class='sapMText info'>");
					rm.write(cardShortText);
					rm.write("</h3>");
					//Long Text
					rm.write("<p class='sapMText bodyCopy feedCardLongText'");
					rm.write(">");
					rm.write(cardLongText);
					rm.write("</p>");

					//Call to Action
					rm.write("<a href='" + cardCallToActionURL + "' class='links valvolineLink'");
					if (oControl.getCardButtonTarget() === "New Tab") {
						rm.write("target='_blank'");
					}
					rm.write(">");
					rm.write(cardCallToActionLabel);
					rm.write("</a>");
					rm.write("</div>");

					rm.write("</div>");
				}
				rm.write("</div>");
			}
			break;
			// Hero Card

		case 500:
			rm.write("<div tabindex=\"0\"");
			rm.writeControlData(oControl);
			rm.addClass("reusable-hero-card-container");
			rm.addClass("sapMCustomTile");
			rm.addClass("reusable-cursor-pointer-modifier");
			rm.writeClasses();
			if (oControl._invisible) {
				rm.addStyle("visibility", "hidden");
				rm.writeStyles();
			}

			if (oControl.getParent() instanceof sap.m.TileContainer) {
				rm.writeAccessibilityState({
					role: "option",
					posinset: oControl._getTileIndex(),
					setsize: oControl._getTilesCount()
				});
			}

			rm.write(">");
			rm.write("<div class='heroCard'");
			rm.addStyle(" background-image", "url(" + oControl.getCardMediaURL() + ")");
			rm.writeStyles();
			rm.write(">");

			//hero card content
			rm.write("<div class='sapMVBox heroCardContent'>");

			var HBoxHero = new sap.m.HBox().addStyleClass("shortTextContainer");
			var TextHero = new sap.m.Text({
				text: oControl.getCardTitle(),
				maxLines: 2
			}).addStyleClass("pageTitle byline reusable-cursor-pointer-modifier");
			HBoxHero.addItem(TextHero);
			rm.renderControl(HBoxHero);

			//SUBTITLE
			HBoxHero = new sap.m.HBox().addStyleClass("hero-subtitle");
			TextHero = new sap.m.Text({
				text: oControl.getCardShortText(),
				maxLines: 3,
				tooltip: oControl.getCardShortText()
			}).addStyleClass("pageSubtitle  card-subtitle reusable-cursor-pointer-modifier");
			HBoxHero.addItem(TextHero);
			rm.renderControl(HBoxHero);

			//DESCRIPTION
			HBoxHero = new sap.m.HBox().addStyleClass("shortTextContainer");
			TextHero = new sap.m.Text({
				text: oControl.getCardLongText(),
				maxLines: 7,
				tooltip: oControl.getCardLongText()
			}).addStyleClass("bodyCopy heroCardLongText card-description reusable-cursor-pointer-modifier");
			HBoxHero.addItem(TextHero);
			rm.renderControl(HBoxHero);
			oLinkHBox = new sap.m.HBox({
				renderType: "Bare",
				height: "44px"
			}).addStyleClass("hero-imageCardLink");
			link = new sap.m.Link({
				text: oControl.getCardButtonText()
			}).addStyleClass("links valvolineLink");
			oLinkHBox.addItem(link);
			rm.renderControl(oLinkHBox);

			//
			// attach the press event to the parent control
			//
			oControl.attachPress(function (oEvent) {
				oControl.cardBtnpress(this, oControl.getCardButtonTarget(), oControl.getCardButtonAction(), oControl.getCardCategory());
				//GTM
				if (oControl.getParent().getId().indexOf("cardContainerLearnVU") !== -1) {
					_BaseController.GTMDataLayer('vustrtlearningclick',
						'Valvoline University',
						oControl.getCardButtonText() + ' -click',
						oControl.getCardButtonAction()
					);
				} else if (oControl.getParent().getId().indexOf("cardContainerPromote") !== -1) {
					var url = window.location;
					_BaseController.GTMDataLayer('promtcrdlinkclick',
						'Promote',
						oControl.getCardShortText() + '- ' + oControl.getCardButtonText().replace(/\s+/g, '') + ' -click',
						url.hash
					);
				} //END GTM
			});

			rm.write("</div>");
			rm.write("</div>");
			rm.write("</div>");
			break;
		case 900:
			rm.write("<div tabindex=\"0\"");
			rm.writeControlData(oControl);
			rm.addClass("reusable-navigation-card-container");
			rm.addClass("reusable-cursor-pointer-modifier");
			//Exceptions!!
			var mHybrisParameters = sap.ui.getCore().getModel("HybrisParameters");
			switch (oControl.getCardButtonAction()) {
			case ("/my-account/drum-label-detail"):
				//If product label card
				if (mHybrisParameters === undefined) {
					mHybrisParameters = new sap.ui.model.json.JSONModel();
					mHybrisParameters.setData(_BaseController.getGlobalParametersByGroup("HybrisParameters"));
					sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
				}
				//Show/hide approveOrder card on mobile
				if (mHybrisParameters.getData().showPrintLabelMobile === "false") {
					rm.addClass("hidden-in-mobile");
				}
				break;
			case ("/my-account/approval-dashboard"):
				if (mHybrisParameters === undefined) {
					mHybrisParameters = new sap.ui.model.json.JSONModel();
					mHybrisParameters.setData(_BaseController.getGlobalParametersByGroup("HybrisParameters"));
					sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
				}
				//Show/hide approveOrder card on mobile
				if (mHybrisParameters.getData().showApproveOrder === "false") {
					rm.addClass("hidden-in-mobile");
				}
				break;
			case ("/my-account/deliverytickets"):
				if (mHybrisParameters === undefined) {
					mHybrisParameters = new sap.ui.model.json.JSONModel();
					mHybrisParameters.setData(_BaseController.getGlobalParametersByGroup("HybrisParameters"));
					sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
				}
				//Show/hide delivery tickets card on mobile
				if (mHybrisParameters.getData().showDeliveryTickets === "false") {
					rm.addClass("hidden-in-mobile");
				}
				break;
			case ("/my-account/invoice-history"):
				if (mHybrisParameters === undefined) {
					mHybrisParameters = new sap.ui.model.json.JSONModel();
					mHybrisParameters.setData(_BaseController.getGlobalParametersByGroup("HybrisParameters"));
					sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
				}
				//Show/hide invoices card on mobile
				if (mHybrisParameters.getData().showInvoices === "false") {
					rm.addClass("hidden-in-mobile");
				}
				break;
			case ("/my-account/order/quickorder"):
				//Orders Cards on mobile	
				if (mHybrisParameters === undefined) {
					mHybrisParameters = new sap.ui.model.json.JSONModel();
					mHybrisParameters.setData(_BaseController.getGlobalParametersByGroup("HybrisParameters"));
					sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
				}
				//Show/hide quick order card on mobile
				if (mHybrisParameters.getData().showQuickOrder === "false") {
					rm.addClass("hidden-in-mobile");
				}
				break;
			default:
				break;
			}
			rm.writeClasses();
			rm.write(">");

			var oImageVBoxCard = new sap.m.VBox({
				width: "100%"
			}).addStyleClass("reusable-navigation-text-container-compensation");
			var oImageCard = new sap.m.Image({
				width: "100%",
				densityAware: false,
				src: oControl.getCardMediaURL()
			}).addStyleClass("reusable-navigation-card-image");
			var oActionHBoxCard = new sap.m.HBox().addStyleClass("navigation-cards-link-container reusable-cursor-pointer-modifier");
			var oActionCard = new sap.m.Link({
				text: oControl.getCardButtonText()
			}).addStyleClass("valvolineLink pageSubtitle reusable-navigation-card-text");
			oImageVBoxCard.addItem(oImageCard);
			oActionHBoxCard.addItem(oActionCard);
			oControl.attachPress(
				function (oEvent) {
					oControl.card900Btnpress(this, oControl.getCardNavigationType(), oControl.getCardButtonTarget(), oControl.getCardButtonAction());
				});
			rm.renderControl(oImageVBoxCard);
			rm.renderControl(oActionHBoxCard);

			rm.write("</div>");
			break;
		default:
			break;
		}

	},

	/**
	 * Timestamp formatter
	 * @formatAMPM
	 * @param {string} date - timestamp
	 * @returns timestamp formatted as "hh:mm AM/PM"
	 */
	formatAMPM: function (date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? "PM" : "AM";
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		minutes = minutes < 10 ? "0" + minutes : minutes;
		var strTime = hours + ":" + minutes + " " + ampm;
		return strTime;
	},

	/**
	 * Formatter for timestamp card
	 * @formatTimeStamp
	 * @param {string} time - timestamp
	 * @returns timestamp formatted as "MMM dd"
	 */
	formatTimeStamp: function (time) {
		var timestamp = null;
		var timeS = time.toString();
		if (timeS.indexOf("GMT") === -1) {
			var oDate = new Date(time);
			var Localdate = new Date(oDate.valueOf() - (oDate.getTimezoneOffset()) * 60000);

			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			time = Localdate.toLocaleDateString(selectedLanguage);

			timestamp = new Date(Localdate);
		} else {
			timestamp = new Date(time);
		}
		var today = new Date();

		if (time === "") {
			return "";
		} else if (today.getFullYear() === timestamp.getFullYear() && today.getMonth() === timestamp.getMonth() && today.getDate() ===
			timestamp.getDate()) {
			return _BaseController.getResourceBundle().getText("TodayTXT");
		} else {
			//print formatted date
			var monthNames = [_BaseController.getResourceBundle().getText("Mjan"),
				_BaseController.getResourceBundle().getText("Mfeb"),
				_BaseController.getResourceBundle().getText("Mmar"),
				_BaseController.getResourceBundle().getText("Mapr"),
				_BaseController.getResourceBundle().getText("Mmay"),
				_BaseController.getResourceBundle().getText("Mjun"),
				_BaseController.getResourceBundle().getText("Mjul"),
				_BaseController.getResourceBundle().getText("Maug"),
				_BaseController.getResourceBundle().getText("Msep"),
				_BaseController.getResourceBundle().getText("Moct"),
				_BaseController.getResourceBundle().getText("Mnov"),
				_BaseController.getResourceBundle().getText("Mdec")
			];
			return monthNames[timestamp.getMonth()] + " " + timestamp.getDate();

		}
	},

	/**
	 * Opens details page depending on the card type
	 * @function cardBtnpress
	 * @param {Object} oEvent - Button pressed
	 * @param {string} target - Redirection or New Tab
	 * @param {string} url - Url data
	 * @param {string} category - Card category
	 */
	cardBtnpress: function (oEvent, target, url, category) {
		switch (target) {
		case "Route":

			switch (category) {
			case "Order":
				sap.ui.getCore().getEventBus().publish("routerNav", "ordersNav", {
					orderUUID: "0"
				});
				break;
			case "Case":
				sap.ui.getCore().getEventBus().publish("routerNav", "casesNav", {
					caseId: "0"
				});
				break;
			default:
				sap.ui.getCore().getEventBus().publish("routerNav", "otherNav", {
					url: url
				});
				break;
			}
			break;
		case "New Tab":
			window.open(url, "_blank");
			break;
		default:
			sap.ui.getCore().getEventBus().publish("routerNav", "otherNav", {
				url: url
			});
			break;
		}
	},
	/**
	 * Opens details page depending on the card type
	 * @function cardBtnpress
	 * @param {Object} oEvent - Button pressed
	 * @param {string} calltoactiontype  - Type of navigation
	 * @param {string} urlTarget - new or current tab
	 * @param {string} calltoactionURL - URL/route/fragment
	 */
	card900Btnpress: function (oEvent, callToActionType, urlTarget, callToActionURL) {
		switch (callToActionType) {
		case "1":
			//card should navigate to the calltoactionURL route (DASH Navigation)
			if (callToActionURL === "priceList") {
				//Fire the GTM tags, the price list exception handling will be triggered there
				this.fireEvent("gtmHandling", {
					callToActionURL: callToActionURL
				});
			} else if (urlTarget === "New Tab") {
				//Fire the GTM tags
				this.fireEvent("gtmHandling", {
					callToActionURL: callToActionURL
				});
				//Trigger the navigation
				_BaseController.dashNavTo(callToActionURL, true);
			} else {
				//Fire the GTM tags
				this.fireEvent("gtmHandling", {
					callToActionURL: callToActionURL
				});
				//Trigger the navigation
				_BaseController.dashNavTo(callToActionURL);
			}
			break;
		case "2":
			//card should navigate to the iframe with calltoactionURL route (Hybris Navigation)
			var mHybrisParameters = sap.ui.getCore().getModel("HybrisParameters");
			if (mHybrisParameters === undefined) {
				mHybrisParameters = new sap.ui.model.json.JSONModel();
				mHybrisParameters.setData(this.getGlobalParametersByGroup("HybrisParameters"));
				sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
			}
			//Fire the GTM tags, the price list exception handling will be triggered there
			this.fireEvent("gtmHandling", {
				callToActionURL: callToActionURL
			});
			_BaseController.nav2Hybris(mHybrisParameters.getData().Hybris + callToActionURL);
			break;
		case "3":
			//card should navigate to the calltoactionURL page (External Navigation)
			//Fire the GTM tags
			this.fireEvent("gtmHandling", {
				callToActionURL: callToActionURL
			});
			if (urlTarget === "New Tab") {
				sap.m.URLHelper.redirect(callToActionURL, true);
			} else {
				sap.m.URLHelper.redirect(callToActionURL, false);
			}
			break;
		default:
			//No action
			break;
		}
	}
});