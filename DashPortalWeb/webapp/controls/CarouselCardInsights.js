/*Control for rendering cards of type Carousel*/
jQuery.sap.declare("valvoline.dash.portal.DashPortalWeb.controls.CarouselCardInsights");

jQuery.sap.require("sap.m.Carousel");

sap.m.Carousel.extend("valvoline.dash.portal.DashPortalWeb.controls.CarouselCardInsights", {
	// Card Type ('1' - Feed, '2' - Image, '3' - Video,
	// '4' - Product, - '5' - Hero, '6' -List,  '7' - Carousel)
	metadata: {
		properties: {
			"CardType": {
				type: "int",
				defaultValue: 1
			},
			"CardTitle": {
				type: "string",
				defaultValue: ""
			},
			"CardId": {
				type: "int"
			},
			"CardSections": {
				type: "object[]"
			}
		}
	},
	/** @module CarouselCardInsights */
	/**
	 * Renders the Insight carousel cards
	 * @function renderer
	 * @param {Object} rm - render manager
	 * @param {Object} oCarousel - element to render
	 */
	renderer: function (rm, oCarousel) {

		// create title Header for Carousel
		rm.write("<div class='carouselPageContainer blackBorder' height='auto' ");
		rm.addClass("");
		rm.writeClasses();
		rm.write(">");

		rm.write("<h2 class='trends carouselTitleInsights center'>");
		rm.write(oCarousel.getCardTitle());
		rm.write("</h2>");

		var sections = oCarousel.getCardSections();

		if (sections !== undefined) {
			var size = sections.length;
			var nPages;
			var nSectionsDisplay;

			//determine how many pages the carousel will have in mobile - 1 section per page
			if (sap.ui.Device.system.phone) {
				nPages = size;
				nSectionsDisplay = 1;
			}
			//determine how many pages the carousel will have in desktop - 3 section per page
			else {
				nPages = size / 4;
				nSectionsDisplay = 4;
			}
			var missingCard = sections.length;
			var begin = 0;
			var width = 100 / nSectionsDisplay;

			//create the pages and add them to the carousel
			for (var i = 0; i < nPages; i++) {
				var newPageContainer = new sap.m.VBox({
					width: "100%",
					height: "auto"
				});
				var newPage = new sap.m.HBox({
					width: "100%",
					height: "auto"
				}).addStyleClass("carouselPage carouselPageLeftMargin");
				var pageTitle = new sap.m.HBox({
					height: "10%",
					justifyContent: "Center"
				}).addStyleClass("carouselTitleInsights sapMFlexBox");

				pageTitle.addItem(new sap.m.Text({
					text: oCarousel.getCardTitle()
				}).addStyleClass("pageSubtitleInsights"));

				newPageContainer.addItem(newPage);
				oCarousel.addPage(newPageContainer);

				var end;

				if (missingCard < nSectionsDisplay) {
					end = begin + missingCard;
				} else {
					end = begin + nSectionsDisplay;
				}

				for (var j = begin; j < end; j++) {
					var cardShortText = sections[j].subtitle;
					var cardLongText = sections[j].description;
					var cardCallToActionLabel = sections[j].callToActionLabel;
					if (sections[j].isBackToDash === "1") {
						var cardLink = sections[j].callToActionURL + "?" + window.location.href;
					} else {
						cardLink = sections[j].callToActionURL;
					}
					var cardUrlTargert = sections[j].urlTarget;
					var cardURLmedia = sections[j].mediaURL;
					var cardMediaType = sections[j].mediatype;

					var newBox = new sap.m.VBox({
						width: width + "%",
						height: "auto"
					}).addStyleClass("carouselCardSectionInsights center");

					var newBoxImage = new sap.m.VBox({
						justifyContent: "Center"
					}).addStyleClass("insightsTrendPictureContainer");

					var newBoxTitle = new sap.m.VBox({
						height: "78px"
					}).addStyleClass("alignmentCarousel alignmentCarouselTextInsights");
					var newBoxText = new sap.m.VBox({}).addStyleClass("alignmentCarouselInsightsTexT alignmentCarouselTextInsights");
					var newBoxLink = new sap.m.HBox({
						renderType: "Bare",
						height: "36px",
						width: "100%",
						justifyContent: "Center"
					}).addStyleClass("carousel-imageCardLink center");
					if (cardMediaType === "0") {
						newBoxImage.addItem(new sap.m.Image({
							src: cardURLmedia,
							width: "100%",
							height: "auto"
						}).addStyleClass("imageCardInsights"));
					} else if (cardMediaType === "1") {
						newBoxImage.addItem(new sap.ui.core.HTML({
							content: "<iframe width='100%' height='auto' justifyContent='Center' class='imageCardInsights videoForStoping' src='" +
								cardURLmedia + "'>" +
								"</iframe>"
						}));

					}
					newBoxTitle.addItem(new sap.m.Text({
						text: cardShortText,
						maxLines: 3,
						tooltip: cardShortText
					}).addStyleClass("pageSubtitleInsights"));
					newBoxText.addItem(new sap.m.Text({
						text: cardLongText,
						maxLines: 7,
						tooltip: cardLongText
					}).addStyleClass("bodyCopy center"));
					if (cardUrlTargert === "" || cardUrlTargert === null) {
						newBoxLink.addItem(new sap.m.Link({
							text: ""
						}));
					} else if (cardUrlTargert === "New Tab") {
						newBoxLink.addItem(new sap.m.Link({
							text: cardCallToActionLabel,
							href: cardLink,
							target: "_blank"
						}).addStyleClass("link valvolineLink carousel-link-insights"));
					} else {
						newBoxLink.addItem(new sap.m.Link({
							text: cardCallToActionLabel,
							href: cardLink
						}).addStyleClass("link valvolineLink carousel-link-insights"));
					}
					newBox.addItem(newBoxImage);
					newBox.addItem(newBoxTitle);
					newBox.addItem(newBoxText);
					newBox.addItem(newBoxLink);
					newPage.addItem(newBox);
				}
				missingCard = sections.length - (end - begin);
				begin = begin + nSectionsDisplay;
			}
		}

		var aPages = oCarousel.getPages(),
			iPageCount = aPages.length,
			sPageIndicatorPlacement = oCarousel.getPageIndicatorPlacement(),
			sArrowsPlacement = oCarousel.getArrowsPlacement(),
			sId = oCarousel.getId(),
			iBulletsToNumbersThreshold = sap.m.Carousel._BULLETS_TO_NUMBERS_THRESHOLD,
			iIndex = oCarousel._getPageNumber(oCarousel.getActivePage());
		this._renderOpeningDiv(rm, oCarousel);

		//visual indicator
		if (sPageIndicatorPlacement === sap.m.PlacementType.Top) {
			this._renderPageIndicatorAndArrows({
				rm: rm,
				iPageCount: iPageCount,
				sId: sId,
				iIndex: iIndex,
				iBulletsToNumbersThreshold: iBulletsToNumbersThreshold,
				sArrowsPlacement: sArrowsPlacement,
				bBottom: false,
				bShowPageIndicator: oCarousel.getShowPageIndicator()
			}, oCarousel);
		}

		this._renderInnerDiv(rm, oCarousel, aPages, sPageIndicatorPlacement);

		if (sap.ui.Device.system.desktop && iPageCount > 1 && sArrowsPlacement === sap.m.CarouselArrowsPlacement.Content) {
			this._renderHudArrows(rm, oCarousel);
		}

		//visual indicator
		if (sPageIndicatorPlacement === sap.m.PlacementType.Bottom) {
			this._renderPageIndicatorAndArrows({
				rm: rm,
				iPageCount: iPageCount,
				sId: sId,
				iIndex: iIndex,
				iBulletsToNumbersThreshold: iBulletsToNumbersThreshold,
				sArrowsPlacement: sArrowsPlacement,
				bBottom: true,
				bShowPageIndicator: oCarousel.getShowPageIndicator()
			}, oCarousel);
		}

		this._renderClosingDiv(rm);
		//page-wrap ends
		rm.write("</div>");
	}
});