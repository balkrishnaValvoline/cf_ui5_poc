/**
 * In this function are all the cards showed in the Feed View.
 * @patricia.barros
 */

jQuery.sap.declare("valvoline.dash.portal.DashPortalWeb.controls.CardContainer");

jQuery.sap.require("sap.m.ScrollContainer");
jQuery.sap.require("sap.ui.core.format.DateFormat");
jQuery.sap.require("valvoline.dash.portal.DashPortalWeb.controls.Card");
jQuery.sap.require("valvoline.dash.portal.DashPortalWeb.controls.CarouselCard");
jQuery.sap.require("valvoline.dash.portal.DashPortalWeb.controls.CarouselCardInsights");

var needsTimeStamp, _oRm, _render, _BaseController, _oControl;

sap.m.ScrollContainer.extend("valvoline.dash.portal.DashPortalWeb.controls.CardContainer", {

	metadata: {
		properties: {
			"containerCardId": {
				type: "int"
			},
			"cardCategory": {
				type: "string",
				defaultValue: "All"
			},
			"maxNumCards": {
				type: "int"
			},
			"viewContext": {
				type: "object"
			}
		},
		events: {
			scroll: {}
		}
	},

	onInit: function () {
		needsTimeStamp = false;
	},
	/** @module CardContainer */
	/**
	 * Initializes the render for the the card container.
	 * @function renderer
	 * @param rm - render manager
	 * @param oControl - element to render
	 */
	renderer: function (oRm, oControl) {
		_oRm = oRm;
		_oControl = oControl;
		_render = true;

		// get the reference and instance from BaseController
		var BaseController = sap.ui.require("valvoline/dash/portal/DashPortalWeb/controller/Base.controller");
		_BaseController = new BaseController();

		oControl.renderCardContainer(oRm, oControl, _BaseController);
	},

	/**
	 * Renders the cardContainer.
	 * @function renderCardContainer
	 * @param {Object} rm - render manager
	 * @param {Object} oControl - element to render
	 * @param {Object} controller
	 */
	renderCardContainer: function (oRm, oControl, controller) {
		oRm.write("<div");
		oRm.writeControlData(oControl);
		var width = oControl.getWidth(),
			height = oControl.getHeight();
		if (width) {
			oRm.addStyle("width", width);
		}
		if (height) {
			oRm.addStyle("height", height);
		}
		oRm.writeStyles();

		if (oControl.getVertical()) {
			if (!oControl.getHorizontal()) {
				oRm.addClass("sapMScrollContV");
			} else {
				oRm.addClass("sapMScrollContVH");
			}
		} else if (oControl.getHorizontal()) {
			oRm.addClass("sapMScrollContH");
		}
		oRm.writeClasses();

		var sTooltip = oControl.getTooltip_AsString();
		if (sTooltip) {
			oRm.writeAttributeEscaped("title", sTooltip);
		}

		if (oControl.getFocusable()) {
			oRm.writeAttributeEscaped("tabindex", "0");
		}
		oRm.write("><div id='" + oControl.getId() + "-scroll' class='sapMScrollContScroll scrollContainer'>");

		oRm.write("<flexdiv class='flexDiv reusable-cards-container-flex-div-centering'>");

		oControl.renderContent(oRm, oControl, controller);

		oRm.write("</flexdiv>");
		oRm.write("</div></div>");
	},

	/**
	 * Renders cards on the containter since the starting index.
	 * @function renderContent
	 * @param {Object} rm - render manager
	 * @param {Object} oControl - element to render
	 */
	renderContent: function (oRm, oControl) {

		// render child controls
		var aContent = oControl.getContent();
		var size = aContent.length;

		// process each card and rederer it
		for (var i = 0; i < size; i++) {
			var cardTile = aContent[i];

			// verifiy is is the Footer
			if (cardTile.getId().indexOf("footer") !== -1 || cardTile.getId().indexOf("indicator") !== -1) {
				oRm.renderControl(cardTile);
				continue;
			}

			// check if the control has data model
			if (cardTile.getModel()) {
				var idCard = aContent[i].getCardId();

				var card = null;
				var cardSections = [];
				var cards = sap.ui.getCore().getModel("cards").getData();
				if (cards) {
					for (var j = 0; j < cards.length; j++) {
						if (cards[j].UUID === idCard) {
							card = cards[j];
						}
					}
				}
				var cardCategoryId;
				var cardTypeId;
				if (card !== null) {
					cardSections = card.contentCardSections;
					cardCategoryId = card.cardCategoryId;
					cardTypeId = card.cardTypeId;
				}

				// categoryId to filter cards 	// filter by card category
				var cardCategoryFilter = oControl.getCardCategory();
				if (cardCategoryFilter !== "All" && cardCategoryFilter !== cardCategoryId) {
					continue;
				}

				if (cardCategoryId === "3000" || cardCategoryId === "4000") {
					/*"3000": // Orders
					  "4000": // Cases*/
					needsTimeStamp = true;
					_render = true;
				} else {

					/*"1000": // User Login
					  "2000": //Product Finder
					  "6000": //Insights
					  "7000": //Learn
					  "8000": //Solutions
					  "9000": //Promote
					  Others*/
					needsTimeStamp = false;
					_render = true;
				}

				switch (cardTypeId) {
					// List Card
				case "600":
					// build the sections array
					var itens = cardSections;
					var sections = [];
					for (j = 0; j < itens.length; j++) {
						var contentCardSectionItem = itens[j];
						sections[j] = contentCardSectionItem;
					}

					// init cardTile with array of sections
					cardTile.setCardSections(sections);

					oRm.renderControl(cardTile);

					break;
					// Carrousel Card
				case "700":
					cardTile = new valvoline.dash.portal.DashPortalWeb.controls.CarouselCard({
						"height": "auto",
						"loop": true
					});
					cardTile.setCardType(parseInt(cardTypeId));
					if (card) {
						cardTile.setCardTitle(card.title);
					}

					// build the sections array
					itens = cardSections;
					sections = [];
					for (j = 0; j < itens.length; j++) {
						contentCardSectionItem = itens[j];
						sections[j] = contentCardSectionItem;
					}

					// init cardTile with array of sections
					cardTile.setCardSections(sections);

					oRm.renderControl(cardTile);

					break;
					// Carrousel Card
				case "800":

					cardTile = new valvoline.dash.portal.DashPortalWeb.controls.CarouselCardInsights({
						"height": "auto",
						"loop": true
					});
					cardTile.setCardType(parseInt(cardTypeId));
					if (card) {
						cardTile.setCardTitle(card.title);
					}

					// build the sections array
					itens = cardSections;
					sections = [];
					for (j = 0; j < itens.length; j++) {
						contentCardSectionItem = itens[j];
						sections[j] = contentCardSectionItem;
					}

					// init cardTile with array of sections
					cardTile.setCardSections(sections);
					oRm.renderControl(cardTile);

					break;
				default:
					if (_render === true && cardSections.length > 0) {
						var cardSection = cardSections[0];
						switch (cardCategoryId) {
						case "4000": //cases
							cardTile.setCardType(parseInt(cardTypeId));
							cardTile.setCardButtonTarget("Route");
							if (card !== null) {
								cardTile.setCardTitle(card.title);
							} else {
								cardTile.setCardTitle("");
							}
							cardTile.setCardShortText(cardSection.subtitle);
							cardTile.setCardLongTextItem1Label(_BaseController.getResourceBundle().getText("MACCardLongTextItem1"));
							cardTile.setCardLongTextItem1(cardSection.description.replace(/^0+/, ""));
							cardTile.setCardLongTextItem2Label(_BaseController.getResourceBundle().getText("MACCardLongTextItem2"));
							cardTile.setCardLongTextItem2(cardSection.description1);
							if (cardSection.isclosed === "true") {
								cardTile.setCardLongTextItem5Label(_BaseController.getResourceBundle().getText("MACCardLongTextItem3"));
								cardTile.setCardLongTextItem5(cardSection.closeddate.split("T")[0].replace(/-/g, "/"));
							}
							cardTile.setCardButtonText(cardSection.callToActionLabel);
							cardTile.setCardMediaURL(cardSection.mediaURL);
							cardTile.setCardButtonAction("myAccount/cases/0/" + cardSection.caseId);
							cardTile.addStyleClass("caseCard caseCardID");
							cardTile.setCardCategory("Case");
							var updated = new Date(cardSection.lastmodifieddate);

							if (updated.getYear() > 0) {
								cardTile.setCardTimeStamp(cardSection.lastmodifieddate);
							} else {
								cardTile.setCardTimeStamp(cardSection.description1);
							}

							oRm.renderControl(cardTile);
							break;
						case "3000": //orders
							var orderDate = cardSection.description;
							var orderTime = cardSection.description4;
							var orderEstTime = cardSection.description5;

							if (!orderTime) {
								orderTime = "T12:00:00Z";
							} else {
								orderTime = "".concat("T", orderTime, "Z");
							}

							var dateUTCString = orderDate.concat(orderTime);
							var newdate = new Date(dateUTCString);

							cardTile = new valvoline.dash.portal.DashPortalWeb.controls.Card();
							cardTile.setCardType(parseInt(cardTypeId, 10));

							if (card !== null) {
								cardTile.setCardTitle(card.title);
							} else {
								cardTile.setCardTitle("");
							}
							cardTile.setCardShortText(cardSection.subtitle);
							if (cardSection.description2) {
								cardTile.setCardLongTextItem1Label(_BaseController.getResourceBundle().getText("MAORDCardLongTextItem3"));
								cardTile.setCardLongTextItem1(cardSection.description2);
							}
							if (cardSection.description1) {
								cardTile.setCardLongTextItem2Label(_BaseController.getResourceBundle().getText("MAORDCardLongTextItem2"));
								cardTile.setCardLongTextItem2(cardSection.description1);
							}

							if (cardSection.description3 && cardSection.description3 !== "UNCONFIRMED") {
								cardTile.setCardLongTextItem3Label(_BaseController.getResourceBundle().getText("MAORDCardLongTextItem4"));
								var orderEstDate = cardSection.description3;
								cardTile.setCardLongTextItem3(orderEstDate);
								cardTile.setCardLongTextItem6(orderEstTime);
							} else {
								cardTile.setCardLongTextItem3Label(_BaseController.getResourceBundle().getText("MAORDCardLongTextItem4"));
								cardTile.setCardLongTextItem3(_BaseController.getResourceBundle().getText("MAORDUnconfirmed"));
							}

							cardTile.setCardButtonText(cardSection.callToActionLabel);
							cardTile.setCardMediaURL(cardSection.mediaURL);
							cardTile.setCardButtonAction("orders/" + cardSection.orderUUID);
							cardTile.setCardButtonTarget("Route");
							cardTile.addStyleClass("caseCard orderCardID");
							cardTile.setCardCategory("Order");
							cardTile.setHeaderStatus(cardSection.headerStatus);

							if (newdate.getYear()) {
								cardTile.setCardTimeStamp(dateUTCString);
							}

							_oRm.renderControl(cardTile);
							break;
						default: //others
							// set the card scetion values
							cardTile.setCardShortText(cardSection.subtitle);
							cardTile.setCardLongText(cardSection.description);
							cardTile.setCardButtonText(cardSection.callToActionLabel);
							cardTile.setCardButtonAction(cardSection.callToActionURL || null);
							cardTile.setCardButtonTarget(cardSection.urlTarget || null);
							cardTile.setCardNavigationType(cardSection.callToActionType || null);
							cardTile.setViewContext(this.getViewContext() || null);
							if (this.getViewContext().gtmHandling) {
								cardTile.attachGtmHandling(this.getViewContext().gtmHandling || null);
							}
							//set timestamp only if the card is transactional
							if (needsTimeStamp) {
								//if there's an update date, set the timestamp to that, else set the timestamp to the creation date
								if (card !== null) {
									updated = new Date(card.updatedOn);
								} else {
									updated = "";
								}
								if (card !== null) {
									if (updated.getYear() > 0) {
										cardTile.setCardTimeStamp(card.updatedOn);
									} else {
										cardTile.setCardTimeStamp(card.createdOn);
									}
								} else {
									cardTile.setCardTimeStamp("");
								}
							}

							cardTile.setCardMediaURL(cardSection.mediaURL);
							oRm.renderControl(cardTile);
							break;

						}
					} // end of switch
				}
			}
		}
	}

});