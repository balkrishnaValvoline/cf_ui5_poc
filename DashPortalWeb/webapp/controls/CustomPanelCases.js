/*Control for rendering cards of type Panel*/
jQuery.sap.declare("valvoline.dash.portal.DashPortalWeb.controls.CustomPanelCases");

jQuery.sap.require("sap.m.Panel", "sap.m.Button", "sap.ui.core.Icon");

var _BaseController;

sap.m.Panel.extend("valvoline.dash.portal.DashPortalWeb.controls.CustomPanelCases", {

	metadata: {
		properties: {
			"headerQuestion": {
				type: "String",
				defaultValue: ""
			},
			"subject": {
				type: "String",
				defaultValue: ""
			},
			"headerQuestion2": {
				type: "String",
				defaultValue: ""
			},
			"type": {
				type: "String",
				defaultValue: ""
			},
			"active": {
				type: "Boolean",
				defaultValue: true
			},
			"caseNumber": {
				type: "String",
				defaultValue: ""
			},
			"createdDate": {
				type: "String",
				defaultValue: ""
			},
			"closedDate": {
				type: "String",
				defaultValue: ""
			}
		}
	},

	/** @module CustomPanelCases */
	/**
	 * Initializes the case's panel render
	 * @function renderer
	 * @param {Object} oRm - render manager
	 * @param {Object} oControl - element to render
	 */
	renderer: function (oRm, oControl) {
		var BaseController = sap.ui.require("valvoline/dash/portal/DashPortalWeb/controller/Base.controller");
		_BaseController = new BaseController();
		this.startPanel(oRm, oControl);
		oControl.renderHeader(oRm, oControl);
		this.renderContent(oRm, oControl);
		this.endPanel(oRm);
	},

	/**
	 * Renders the case's panel header.
	 * @function renderHeader
	 * @param {Object} oRm - render manager
	 * @param {Object} oControl - element to render
	 */
	renderHeader: function (oRm, oControl) {
		var bIsExpandable = oControl.getExpandable(),
			bIsExpanded = oControl.getExpanded(),
			oHeaderTBar = oControl.getHeaderToolbar(),
			sHeaderClass,
			that = this,
			screenWidth = sap.ui.Device.media.getCurrentRange("DASH_MOBILE").name;

		// we need a wrapping div around icon and header
		// otherwise the border needed for both do not exact align
		oRm.write("<header tabindex='0'");
		if (oHeaderTBar) {
			sHeaderClass = "sapMPanelWrappingDivTb";
		} else {
			sHeaderClass = "sapMPanelWrappingDiv";
		}
		oRm.addClass(sHeaderClass);
		oRm.addClass("reusable-cursor-pointer-modifier");
		if (bIsExpanded) {
			oRm.addClass(sHeaderClass + "Expanded");
		}
		oRm.writeClasses();
		oRm.writeAttribute("id", oControl.getId() + "-headerPanel");
		oRm.write(">");

		// render header
		var sHeaderText = oControl.getHeaderText();
		var sHeaderQuestion = oControl.getHeaderQuestion();
		var sSubject = oControl.getSubject();
		var sCreatedDate = oControl.getCreatedDate();
		var sClosedDate = oControl.getClosedDate();

		if (oHeaderTBar) {
			oHeaderTBar.setDesign(sap.m.ToolbarDesign.Transparent, true);
			oHeaderTBar.addStyleClass("sapMPanelHeaderTB");
			oRm.renderControl(oHeaderTBar);

		} else if (sHeaderText || bIsExpandable) {
			oRm.write("<div class='sapMVBox'>");
			oRm.write("<div class='sapMHBox sapMFlexBox sapMFlexItem sapUiTinyMarginBottom'>");

			var oIcon = oControl._getIcon();
			if (bIsExpanded) {
				oIcon.addStyleClass("sapMPanelExpandableIconExpanded");
			} else {
				oIcon.removeStyleClass("sapMPanelExpandableIconExpanded");
			}
			oRm.renderControl(oIcon);

			if (!(screenWidth === "mobile") && oControl.getType() === "cases") {
				//date
				oRm.write("<h1");
				oRm.addClass("sapMPanelHdr");
				oRm.addClass("info");
				oRm.writeClasses();
				oRm.addStyle("margin-left", "4rem");
				oRm.addStyle("min-width", "5rem");
				oRm.addStyle("max-height", "1.2rem");
				oRm.writeStyles();
				oRm.writeAttribute("id", oControl.getId() + "-header");
				oRm.write(">");
				if (oControl.getClosedDate() === "") {
					oRm.writeEscaped(this.formatTimeStamp(sCreatedDate));
				} else {
					oRm.writeEscaped(this.formatTimeStamp(sClosedDate));
				}
				oRm.write("</h1>");
			}

			if (screenWidth === "mobile") {
				//title
				oRm.write("<div class='sapMVBox sapMFlexBox sapMFlexItem sapUiTinyMarginBottom'>");

				oRm.write("<h1");
				oRm.addClass("sapMPanelHdr");
				oRm.addClass("faqsTitle2");
				if (oControl.getType() !== "cases") {
					oRm.addClass("limitAlignCases");
					oRm.writeClasses();
				} else {
					oRm.addClass("limitAlignCases2");
					oRm.writeClasses();
				}

				if (oControl.getType() !== "cases") {
					oRm.addStyle("margin-left", "4.5rem");
				}
				oRm.addStyle("max-height", "auto");
				oRm.writeStyles();
				oRm.writeAttribute("id", oControl.getId() + "-header");
				oRm.write(">");
				oRm.writeEscaped(sHeaderText);
				oRm.write("</h1>");

				//subject
				oRm.write("<h1");
				oRm.addClass("sapMPanelHdr");
				oRm.addClass("faqsTitle2");
				if (oControl.getType() !== "cases") {
					oRm.addClass("limitAlignCases");
					oRm.writeClasses();
				} else {
					oRm.addClass("limitAlignCases2");
					oRm.writeClasses();
				}

				if (oControl.getType() !== "cases") {
					oRm.addStyle("margin-left", "4.5rem");
				}
				oRm.addStyle("max-height", "auto");
				oRm.writeStyles();
				oRm.writeAttribute("id", oControl.getId() + "-header");
				oRm.write(">");
				oRm.writeEscaped(sSubject);
				oRm.write("</h1>");

				//description
				oRm.write("<h2");
				oRm.addClass("sapMPanelHdr");
				oRm.addClass("faqsTitle3");
				if (oControl.getType() !== "cases") {
					oRm.addClass("limitAlignCases");
					oRm.writeClasses();
				} else {
					oRm.addClass("limitAlignCases2");
					oRm.writeClasses();
				}

				oRm.addStyle("min-height", "auto");
				oRm.addStyle("max-width", "18rem");
				oRm.writeStyles();
				oRm.writeAttribute("id", oControl.getId() + "-header");
				oRm.write(">");
				oRm.writeEscaped(sHeaderQuestion);
				oRm.write("</h2>");

				//timestamp
				oRm.write("<h1");
				oRm.addClass("sapMPanelHdr");
				oRm.addClass("info");
				oRm.writeClasses();
				oRm.writeAttribute("id", oControl.getId() + "-header");
				oRm.write(">");
				if (oControl.getClosedDate() === "") {
					oRm.writeEscaped(this.formatTimeStamp(sCreatedDate));
					oRm.writeEscaped("  ");
				} else {
					oRm.writeEscaped(this.formatTimeStamp(sClosedDate));
				}
				oRm.write("</h1>");
				oRm.write("</div>");
				oRm.write("<div class='sapMHBox sapMFlexBox sapMFlexItem'>");

			} else {
				//title
				oRm.write("<h1");
				oRm.addClass("sapMPanelHdr");
				oRm.addClass("faqsTitle2");
				if (oControl.getType() !== "cases") {
					oRm.addClass("limitAlignCases");
					oRm.writeClasses();
				} else {
					oRm.addClass("limitAlignCases2");
					oRm.writeClasses();
				}

				if (oControl.getType() !== "cases") {
					oRm.addStyle("margin-left", "4.5rem");
				} else {
					oRm.addStyle("margin-left", "1.5rem");
				}
				oRm.addStyle("height", "18px");
				oRm.writeStyles();
				oRm.writeAttribute("id", oControl.getId() + "-header");
				oRm.write(">");
				oRm.writeEscaped(sHeaderText);
				oRm.write("</h1>");
				oRm.write("</div>");
				oRm.write("<div class='sapMHBox sapMFlexBox sapMFlexItem'>");
			}

			if ((screenWidth === "other") && oControl.getType() === "cases") {

				//timestamp
				oRm.write("<h3");
				oRm.addClass("sapMPanelHdr");
				oRm.addClass("legal");
				oRm.writeClasses();
				oRm.addStyle("margin-left", "4rem");
				oRm.addStyle("min-height", "2rem");
				oRm.addStyle("min-width", "5rem");
				oRm.writeStyles();
				oRm.writeAttribute("id", oControl.getId() + "-header");
				oRm.write(">");
				oRm.write("</h3>");
			}

			if (!(screenWidth === "mobile")) {

				//subtitle
				oRm.write("<h2");
				oRm.addClass("sapMPanelHdr");
				oRm.addClass("faqsTitle3");
				if (oControl.getType() !== "cases") {
					oRm.addClass("limitAlignCases");
					oRm.writeClasses();
				} else {
					oRm.addClass("limitAlignCases2");
					oRm.writeClasses();
				}
				if (oControl.getType() !== "cases") {
					oRm.addStyle("margin-left", "4.5rem");
				} else {
					oRm.addStyle("margin-left", "1.5rem");
				}
				oRm.addStyle("min-height", "auto");
				oRm.writeStyles();
				oRm.writeAttribute("id", oControl.getId() + "-header");
				oRm.write(">");
				oRm.writeEscaped(sHeaderQuestion);
				oRm.write("</h2>");

				oRm.write("</div>");

				oRm.write("</div>");
			}
		}

		if (bIsExpandable) {
			oRm.write("</header>");
		}

		var oInfoTBar = oControl.getInfoToolbar();

		if (oInfoTBar) {
			if (bIsExpandable) {
				// use this class as marker class to ease selection later in onAfterRendering
				oInfoTBar.addStyleClass("sapMPanelExpandablePart");
			}

			// render infoBar
			oInfoTBar.setDesign(sap.m.ToolbarDesign.Info, true);
			oInfoTBar.addStyleClass("sapMPanelInfoTB");
			oRm.renderControl(oInfoTBar);
		}

		//Assign Expanded function to Header
		if (bIsExpandable) {
			//Timeout equal to 0 to guarantee the header is created
			setTimeout(function () {
				var oHeader = $("#" + that.getId() + "-headerPanel");

				//Assign Expanded function to header and change icons based on expanded or collapse
				oHeader.click(function () {
					$(':focus').blur();
					that.setExpanded(!that.getExpanded());
					$(':focus').blur();

				});
			}, 0);
		}
	},
	/**
	 * Expands the case panel.
	 * @function setExpanded
	 * @param {string} setExpanded - Panel expandaded state
	 */
	setExpanded: function (bExpanded) {
		var myBtn = this.oIconCollapsed;
		if (bExpanded === false) {
			myBtn.setSrc('sap-icon://add');
		} else {
			myBtn.setSrc('sap-icon://less');
		}
		if (bExpanded === this.getExpanded()) {
			return this;
		}
		this.setProperty("expanded", bExpanded, true);

		if (!this.getExpandable()) {
			return this;
		}

		// ARIA
		this._getIcon().$().attr("aria-expanded", this.getExpanded());

		this._toggleExpandCollapse();
		this._toggleCssClasses();
		this.fireExpand({
			expand: bExpanded
		});

		//GTM
		var statusCases;
		if (this.getActive() === "trueActive") {
			statusCases = "Active";
		} else {
			statusCases = "Completed";
		}

		if (bExpanded !== false) {
			window.dataLayer.push({
				'event': 'caseExpandclick',
				'eventCategory': 'Support Requests',
				'eventAction': 'Detail Expansion click',
				'eventLabel': statusCases
			});
		}
		//End GTM

		return this;
	},

	/**
	 * Colapses the case's panel
	 * @function _toggleExpandCollapse
	 */
	_toggleExpandCollapse: function () {
		var oOptions = {};
		if (!this.getExpandAnimation()) {
			oOptions.duration = 0;
		}
		this.$().children(".sapMPanelExpandablePart").slideToggle(oOptions);
	},

	_toggleCssClasses: function () {
		var $this = this.$();

		// for controlling the visibility of the border
		$this.children(".sapMPanelWrappingDiv").toggleClass("sapMPanelWrappingDivExpanded");
		$this.children(".sapMPanelWrappingDivTb").toggleClass("sapMPanelWrappingDivTbExpanded");
		$this.find(".sapMPanelExpandableIcon").first().toggleClass("faqsButtonPressed");

	},
	/**
	 * Changes the case's panel icon
	 * @function _createIcon
	 */
	//Change icon
	_createIcon: function () {
		var that = this;
		var newIcon = new sap.ui.core.Icon({
			id: that.getId() + "-CollapsedImg",
			src: "sap-icon://add",
			decorative: false,
			useIconTooltip: false
		}).addStyleClass("sapMPanelExpandableIcon expandableIconLess caseButtonMarginTop zIndexMobile reusable-cursor-pointer-modifier");

		return newIcon;
	},

	/**
	 * Changes timestamp format into "MMM DD"
	 * @function formatTimeStamp
	 * @param {string} time - timestamp
	 * @returns Formatted timestamp "MMM DD"
	 */
	formatTimeStamp: function (time) {

		var oDate = new Date(time);
		var Localdate = new Date(oDate.valueOf() - (oDate.getTimezoneOffset()) * 60000);

		var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
		time = Localdate.toLocaleDateString(selectedLanguage);

		var timestamp = new Date(Localdate);
		var today = new Date();

		if (time == "") {
			return "";
		} else if (today.getFullYear() === timestamp.getFullYear() && today.getMonth() === timestamp.getMonth() && today.getDate() ===
			timestamp.getDate()) {
			return _BaseController.getResourceBundle().getText("TodayTXT");
		} else {
			//print formatted date
			var monthNames = [_BaseController.getResourceBundle().getText("MJAN"),
				_BaseController.getResourceBundle().getText("MFEB"),
				_BaseController.getResourceBundle().getText("MMAR"),
				_BaseController.getResourceBundle().getText("MAPR"),
				_BaseController.getResourceBundle().getText("MMAY"),
				_BaseController.getResourceBundle().getText("MJUN"),
				_BaseController.getResourceBundle().getText("MJUL"),
				_BaseController.getResourceBundle().getText("MAUG"),
				_BaseController.getResourceBundle().getText("MSEP"),
				_BaseController.getResourceBundle().getText("MOCT"),
				_BaseController.getResourceBundle().getText("MNOV"),
				_BaseController.getResourceBundle().getText("MDEC")
			];
			return monthNames[timestamp.getMonth()] + " " + timestamp.getDate();
		}
	}

});