jQuery.sap.declare("valvoline.dash.portal.DashPortalWeb.controls.CustomSearchField");

jQuery.sap.require("sap.m.SearchField");
jQuery.sap.require("valvoline.dash.portal.DashPortalWeb.controls.CustomSearchField");

sap.m.SearchField.extend("valvoline.dash.portal.DashPortalWeb.controls.CustomSearchField", {

	metadata: {
		aggregations: {
			suggestionItems: {
				type: "valvoline.dash.portal.DashPortalWeb.controls.CustomSuggestionItem",
				multiple: true,
				singularName: "suggestionItem"
			}
		}
	},
	
	renderer: function(oRm,oControl, sSearch, bSelected){
                sap.m.SearchFieldRenderer.render(oRm,oControl); //use supercass renderer routine
            }
});