sap.ui.define([
	"sap/ui/core/TooltipBase",
	"sap/m/Text",
	"sap/ui/core/library",
	"sap/ui/core/Control",
	"./PopupItemListRenderer" // preload renderer to avoid synchronous XHR
], function(TooltipBase, Text, sapUiCore, Control) {
	"use strict";

	return TooltipBase.extend("valvoline.dash.portal.DashPortalWeb.controls.PopupItemList", {
		metadata: {
			properties: {
				"maxWidth": {
					type: sapUiCore.CSSSize.getName(),
					group: "Dimension",
					defaultValue: "20rem"
				},
				"height": {
					type: sapUiCore.CSSSize.getName(),
					group: "Dimension",
					defaultValue: "auto"
				},
				"width": {
					type: sapUiCore.CSSSize.getName(),
					group: "Dimension",
					defaultValue: "auto"
				}
				// "text" property already provided by TooltipBase
			},
			defaultAggregation: "content",
			aggregations: {
				"content": {
					type: Control.getMetadata().getName(),
					multiple: false,
					bindable: true
				},
				"_text": {
					type: Text.getMetadata().getName(),
					multiple: false,
					hidden: true
				}
			}
		},

		init: function() {
			if (typeof TooltipBase.prototype.init === "function") {
				TooltipBase.prototype.init.apply(this, arguments);
			}
			this._validateRendering();
		},

		setText: function(text) {
			if (typeof TooltipBase.prototype.setText === "function") {
				TooltipBase.prototype.setText.call(this, text);
			}
			if (!this.getAggregation("_text")) {
				var textControl = new Text();
				this.setAggregation("_text", textControl);
			}
			this.getAggregation("_text").setText(text);
			this.setProperty("text", text, true);
			return this._validateRendering();
		},

		setContent: function(control) {
			this.setAggregation("content", control, true);
			this._validateRendering();
		},

		destroyContent: function() {
			this.destroyAggregation("content", true);
			this._validateRendering();
		},

		_validateRendering: function() {
			var contentToRender = this.getContent() || this.getText().trim();
			return this.setVisible(!!contentToRender);
			// TooltipBase disallows rending this control when it's invisible.
		}

	});
});