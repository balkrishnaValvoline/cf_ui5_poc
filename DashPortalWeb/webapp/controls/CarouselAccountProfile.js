jQuery.sap.declare("valvoline.dash.portal.DashPortalWeb.controls.CarouselAccountProfile");

jQuery.sap.require("sap.m.Carousel");

sap.m.Carousel.extend("valvoline.dash.portal.DashPortalWeb.controls.CarouselAccountProfile", {
	/** @module CarouselAccountProfile */
	/**
	 * Initializes the rendering functions for myAccountProfile carousel container.
	 * @function renderer
	 * @param {Object} rm - render manager
	 * @param {Object} oCarousel - element to render
	 */
	renderer: function(rm, oCarousel) {
		var aPages = oCarousel.getPages(),
			iPageCount = aPages.length,
			sPageIndicatorPlacement = oCarousel.getPageIndicatorPlacement(),
			sArrowsPlacement = oCarousel.getArrowsPlacement(),
			sId = oCarousel.getId(),
			iBulletsToNumbersThreshold = sap.m.Carousel._BULLETS_TO_NUMBERS_THRESHOLD,
			iIndex = oCarousel._getPageNumber(oCarousel.getActivePage());
		this._renderOpeningDiv(rm, oCarousel);

		//visual indicator
		if (sPageIndicatorPlacement === sap.m.PlacementType.Top) {
			this._renderPageIndicatorAndArrows({
				rm: rm,
				iPageCount: iPageCount,
				sId: sId,
				iIndex: iIndex,
				iBulletsToNumbersThreshold: iBulletsToNumbersThreshold,
				sArrowsPlacement: sArrowsPlacement,
				bBottom: false,
				bShowPageIndicator: oCarousel.getShowPageIndicator()
			}, oCarousel);
		}

		this._renderInnerDiv(rm, oCarousel, aPages, sPageIndicatorPlacement);

		if (sap.ui.Device.system.desktop && iPageCount > 1 && sArrowsPlacement === sap.m.CarouselArrowsPlacement.Content) {
			this._renderHudArrows(rm, oCarousel);
		}

		//visual indicator
		if (sPageIndicatorPlacement === sap.m.PlacementType.Bottom) {
			this._renderPageIndicatorAndArrows({
				rm: rm,
				iPageCount: iPageCount,
				sId: sId,
				iIndex: iIndex,
				iBulletsToNumbersThreshold: iBulletsToNumbersThreshold,
				sArrowsPlacement: sArrowsPlacement,
				bBottom: true,
				bShowPageIndicator: oCarousel.getShowPageIndicator()
			}, oCarousel);
		}

		this._renderClosingDiv(rm);
		//page-wrap ends
	},

	/**
	 * Carousel structure opening div.
	 * @function renderOpeningDiv
	 * @param {Object} rm - render manager
	 * @param {Object} oCarousel - element to render
	 */
	renderOpeningDiv: function(rm, oCarousel) {
		var sTooltip = oCarousel.getTooltip_AsString();

		//Outer carousel div
		rm.write("<div");

		// Height Auto
		rm.addStyle("height", "100%");
		rm.writeStyles();

		rm.writeControlData(oCarousel);
		// custom F6 handling
		rm.writeAttribute("data-sap-ui-customfastnavgroup", "true");

		rm.addStyle("width", oCarousel.getWidth());
		rm.addStyle("height", oCarousel.getHeight());
		rm.writeStyles();

		rm.addClass("sapMCrsl");
		//'sapMCrslFluid' is originally from mobify-carousel
		rm.addClass("sapMCrslFluid");

		// add all classes (also custom classes) to carousel tag
		rm.writeClasses();

		if (sTooltip) {
			rm.writeAttributeEscaped("title", sTooltip);
		}

		rm.writeAttributeEscaped("tabindex", "0");

		// ARIA
		rm.writeAccessibilityState(oCarousel, {
			role: "list"
		});

		rm.write(">");
	},

	/**
	 * Carousel structure inner div
	 * @funtion _renderInnerDiv
	 * @param {Object} rm - render manager
	 * @param {Object} oCarousel - element to render
	 * @param {string} aPages - total number of pages
	 * @param {string} sPageIndicatorPlacement - placement on the page
	 */
	_renderInnerDiv: function(rm, oCarousel, aPages, sPageIndicatorPlacement) {
		rm.write("<div class='sapMCrslInner");
		//do housekeeping
		oCarousel._cleanUpScrollContainer();

		if (aPages.length > 1 && (oCarousel.getShowPageIndicator() || oCarousel.getArrowsPlacement() === sap.m.CarouselArrowsPlacement.PageIndicator)) {
			if (sPageIndicatorPlacement === sap.m.PlacementType.Bottom) {
				rm.write(" sapMCrslBottomOffset");

				if (oCarousel.getArrowsPlacement() === sap.m.CarouselArrowsPlacement.PageIndicator) {
					rm.write(" sapMCrslBottomArrowsOffset");
				}
			} else {
				rm.write(" sapMCrslTopOffset");
				if (oCarousel.getArrowsPlacement() === sap.m.CarouselArrowsPlacement.PageIndicator) {
					rm.write(" sapMCrslTopArrowsOffset");
				}
			}
		}

		rm.write("'>");

		var fnRenderPage = function(oPage, iIndex, aArray) {
			//item div
			rm.write("<div class='sapMCrslItem");
			rm.write("' id='" + oCarousel.sId + "-" + oPage.sId + "-slide'");
			// Height Auto
			rm.addStyle("height", "100%");
			rm.writeStyles();
			// ARIA
			rm.writeAccessibilityState(oPage, {
				role: "listitem",
				posinset: iIndex + 1,
				setsize: aArray.length
			});

			rm.write(">");
			rm.renderControl(oCarousel._createScrollContainer(oPage, iIndex));
			rm.write("</div>");
		};

		//Render Pages
		aPages.forEach(fnRenderPage);

		rm.write("</div>");
	},

	/**
	 * Carousel structure closing div
	 * @funtion _renderClosingDiv
	 * @param  {Object} rm - render manager
	 */
	_renderClosingDiv: function(rm) {
		rm.write('</div>');
	},

	/**
	 * Carousel page indicator and next/previous page icon rendering
	 * @funtion _renderClosingDiv
	 * @param {Object} settings - carousel settings
	 * @param {Object} oCarousel - element to render
	 */
	_renderPageIndicatorAndArrows: function(settings, oCarousel) {
		var rm = settings.rm,
			iPageCount = settings.iPageCount,
			bShowIndicatorArrows = sap.ui.Device.system.desktop && settings.sArrowsPlacement === sap.m.CarouselArrowsPlacement.PageIndicator,
			bBottom = settings.bBottom,
			sId = settings.sId,
			iIndex = settings.iIndex,
			iBulletsToNumbersThreshold = settings.iBulletsToNumbersThreshold,
			bShowPageIndicator = settings.bShowPageIndicator,
			sPageIndicatorDisplayStyle = bShowPageIndicator ? "" : "opacity: 0",
			oResourceBundle = sap.ui.getCore().getLibraryResourceBundle("sap.m"),
			sOffsetCSSClass = "",
			sTextBetweenNumbers = sap.ui.getCore().getLibraryResourceBundle("sap.m").getText("CAROUSEL_PAGE_INDICATOR_TEXT");

		// If there is only one page - do not render the indicator
		if (iPageCount <= 1) {
			return;
		}
		if (!bShowPageIndicator && !bShowIndicatorArrows) {
			return;
		}
		if (bBottom) {
			sOffsetCSSClass += " sapMCrslControlsBottom";
		} else {
			sOffsetCSSClass += " sapMCrslControlsTop";
		}

		if (bShowIndicatorArrows) {
			rm.write("<div");
			rm.addClass("sapMCrslControls");
			rm.addClass(sOffsetCSSClass);

			rm.writeClasses();
			rm.write(">");
			rm.write('<div class="sapMCrslControlsContainer' + sOffsetCSSClass + '">');
		} else {
			rm.write('<div class="sapMCrslControlsNoArrows' + sOffsetCSSClass + '">');
		}
		// left arrow
		if (bShowIndicatorArrows) {
			this._renderPrevArrow(rm, oCarousel);
		}

		// page indicator
		var sPageIndicatorId = sId + "-pageIndicator";
		rm.write('<div id="' + sPageIndicatorId + '" style="' + sPageIndicatorDisplayStyle + '"');

		if (iPageCount < iBulletsToNumbersThreshold) {
			rm.write(' class="sapMCrslBulleted">');
			for (var i = 1; i <= iPageCount; i++) {
				rm.write("<span role='img' data-slide=" + i + " aria-label='" + oResourceBundle.getText("CAROUSEL_POSITION", [i, iPageCount]) + "'>" +
					i + "</span>");
			}
		} else {
			rm.write(' class="sapMCrslNumeric">');
			rm.write('<span id=' + sId + '-' + 'slide-number>' + (iIndex + 1) + ' ' + sTextBetweenNumbers + ' ' + iPageCount + '</span>');
		}
		rm.write("</div>");
		// page indicator end

		// right arrow
		if (bShowIndicatorArrows) {
			this._renderNextArrow(rm, oCarousel);
		}
		if (!bShowIndicatorArrows) {
			rm.write("</div>");
		}
		if (bShowIndicatorArrows) {
			rm.write("</div>");
			rm.write("</div>");
		}
	},

	/**
	 * Carousel page indicator icons positioning
	 * @function _renderHudArrows
	 * @param {Object} rm - render manager
	 * @param {Object} oCarousel - element to render
	 */
	_renderHudArrows: function(rm, oCarousel) {
		var arrowPositionHudClass;
		if (oCarousel.getShowPageIndicator()) {

			if (oCarousel.getPageIndicatorPlacement() === sap.m.PlacementType.Top) {
				arrowPositionHudClass = "sapMCrslHudTop";
			} else if (oCarousel.getPageIndicatorPlacement() === sap.m.PlacementType.Bottom) {
				arrowPositionHudClass = "sapMCrslHudBottom";
			}

		} else {
			arrowPositionHudClass = "sapMCrslHudMiddle";
		}
		//heads up controls for desktop browsers
		var sHudId = oCarousel.getId() + "-hud";
		rm.write('<div id="' + sHudId + '" class="sapMCrslHud ' + arrowPositionHudClass + '">');

		this._renderPrevArrow(rm, oCarousel);

		this._renderNextArrow(rm, oCarousel);

		rm.write("</div>");
	},

	/**
	 * Render previous page link icon
	 * @function _renderPrevArrow
	 * @param {Object} rm - render manager
	 * @param {Object} oCarousel - element to render
	 */
	_renderPrevArrow: function(rm, oCarousel) {
		rm.write("<a class='sapMCrslPrev' href='#' data-slide='prev' tabindex='-1'><div class='sapMCrslArrowInner'>");
		rm.renderControl(oCarousel._getNavigationArrow("left"));
		rm.write("</div></a>");
	},

	/**
	 * Render next page link icon
	 * @function _renderNextArrow
	 * @param {Object} rm - render manager
	 * @param {Object} oCarousel - element to render
	 */
	_renderNextArrow: function(rm, oCarousel) {
		rm.write("<a class='sapMCrslNext' href='#' data-slide='next' tabindex='-1'><div class='sapMCrslArrowInner'>");
		rm.renderControl(oCarousel._getNavigationArrow("right"));

		rm.write("</div></a>");
	}

});