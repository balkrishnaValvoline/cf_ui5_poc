/*Control for rendering cards of type Carousel*/
jQuery.sap.declare("valvoline.dash.portal.DashPortalWeb.controls.CarouselCard");

jQuery.sap.require("sap.m.Carousel");

sap.m.Carousel.extend("valvoline.dash.portal.DashPortalWeb.controls.CarouselCard", {

	metadata: {
		properties: {
			"CardType": {
				type: "int",
				//Card Type ('1' - Feed, '2' - Image, '3' - Video, '4' - Product, - '5' - Hero, '6' -List,  '7' - Carousel,
				defaultValue: 1
			},
			"CardTitle": {
				type: "string",
				defaultValue: ""
			},
			"CardId": {
				type: "int"
			},
			"CardSections": {
				type: "object[]"
			}
		}
	},
	/** @module CarouselCard */
	/**
	 * Renders the carousel cards
	 * @function renderer
	 * @param {Object} rm - render manager
	 * @param {Object} oCarousel - element to render
	 */
	renderer: function (rm, oCarousel) {

		//create title Header for Carousel
		rm.write("<div class='carouselPageContainer' height='auto' ");
		rm.addClass("");
		rm.writeClasses();
		rm.write(">");

		var HBoxTitle = new sap.m.HBox().addStyleClass("shortTextContainer");
		var Titletext = new sap.m.Text({
			text: oCarousel.getCardTitle(),
			maxLines: 2
		}).addStyleClass("pageSubtitle carouselTitle carouselPageLeftMargin");
		HBoxTitle.addItem(Titletext);
		rm.renderControl(HBoxTitle);

		var sections = oCarousel.getCardSections();
		if (sections !== undefined) {
			var size = sections.length;

			var nPages;
			var nSectionsDisplay;
			//determine how many pages the carousel will have in mobile - 1 section per page
			if (sap.ui.Device.resize.width < 768) {
				nPages = size;
				nSectionsDisplay = 1;
			}

			//determine how many pages the carousel will have in desktop - 3 section per page
			else {
				nPages = size / 4;
				nSectionsDisplay = 4;
			}
			var missingCard = sections.length;
			var begin = 0;

			//create the pages and add them to the carousel
			for (var i = 0; i < nPages; i++) {
				var newPageContainer = new sap.m.VBox({
					width: "100%",
					height: "auto"
				});
				var newPage = new sap.m.HBox({
					width: "100%",
					height: "auto"
				}).addStyleClass("carouselPage carouselPageLeftMargin");
				var pageTitle = new sap.m.HBox({
					height: "10%",
					justifyContent: "Start"
				}).addStyleClass("carouselTitleInsights sapMFlexBox");

				pageTitle.addItem(new sap.m.Label({
					text: oCarousel.getCardTitle()
				}).addStyleClass("pageSubtitle "));

				newPageContainer.addItem(newPage);
				oCarousel.addPage(newPageContainer);

				var width = 100 / nSectionsDisplay;

				var end;

				if (missingCard < nSectionsDisplay) {
					end = begin + missingCard;
				} else {
					end = begin + nSectionsDisplay;
				}

				for (var j = begin; j < end; j++) {
					var cardShortText = sections[j].subtitle;
					var cardLongText = sections[j].description;
					var cardCallToActionLabel = sections[j].callToActionLabel;
					var cardLink = sections[j].callToActionURL;
					var cardUrlTargert = sections[j].urlTarget;
					var newBox = new sap.m.VBox({
						width: width + "%",
						height: "auto"
					}).addStyleClass("carouselCardSection");
					var newBoxTitle = new sap.m.VBox({}).addStyleClass("learnHeightTitle alignmentCarousel");
					var newBoxText = new sap.m.VBox({}).addStyleClass("learnHeightText alignmentCarousel");
					var newBoxLink = new sap.m.HBox({}).addStyleClass("learnHeightLink alignmentCarousel carousel-CardLink");

					newBoxTitle.addItem(new sap.m.Text({
						text: cardShortText,
						maxLines: 3,
						tooltip: cardShortText
					}).addStyleClass("pageSubtitle carousel-subtitle"));
					newBoxText.addItem(new sap.m.Text({
						text: cardLongText,
						maxLines: 7,
						tooltip: cardLongText
					}).addStyleClass("bodyCopy heroCardLongText"));
					if (cardUrlTargert === "" || cardUrlTargert === null) {
						newBoxLink.addItem(new sap.m.Link({
							text: ""
						}).addStyleClass("link valvolineLink carousel-link"));
					} else if (cardUrlTargert === "New Tab") {
						newBoxLink.addItem(new sap.m.Link({
							text: cardCallToActionLabel,
							href: cardLink,
							target: "_blank"
						}).addStyleClass("link valvolineLink carousel-link"));
					} else {
						newBoxLink.addItem(new sap.m.Link({
							text: cardCallToActionLabel,
							href: cardLink
						}).addStyleClass("link valvolineLink carousel-link"));
					}
					newBox.addItem(newBoxTitle);
					newBox.addItem(newBoxText);
					newBox.addItem(newBoxLink);
					newPage.addItem(newBox);
				}
				missingCard = sections.length - (end - begin);
				begin = begin + nSectionsDisplay;
			}
		}

		var aPages = oCarousel.getPages(),
			iPageCount = aPages.length,
			sPageIndicatorPlacement = oCarousel.getPageIndicatorPlacement(),
			sArrowsPlacement = oCarousel.getArrowsPlacement(),
			sId = oCarousel.getId(),
			iBulletsToNumbersThreshold = sap.m.Carousel._BULLETS_TO_NUMBERS_THRESHOLD,
			iIndex = oCarousel._getPageNumber(oCarousel.getActivePage());
		this._renderOpeningDiv(rm, oCarousel);

		//visual indicator
		if (sPageIndicatorPlacement === sap.m.PlacementType.Top) {
			this._renderPageIndicatorAndArrows({
				rm: rm,
				iPageCount: iPageCount,
				sId: sId,
				iIndex: iIndex,
				iBulletsToNumbersThreshold: iBulletsToNumbersThreshold,
				sArrowsPlacement: sArrowsPlacement,
				bBottom: false,
				bShowPageIndicator: oCarousel.getShowPageIndicator()
			}, oCarousel);
		}

		this._renderInnerDiv(rm, oCarousel, aPages, sPageIndicatorPlacement);

		if (sap.ui.Device.system.desktop && iPageCount > 1 && sArrowsPlacement === sap.m.CarouselArrowsPlacement.Content) {
			this._renderHudArrows(rm, oCarousel);
		}

		//visual indicator
		if (sPageIndicatorPlacement === sap.m.PlacementType.Bottom) {
			this._renderPageIndicatorAndArrows({
				rm: rm,
				iPageCount: iPageCount,
				sId: sId,
				iIndex: iIndex,
				iBulletsToNumbersThreshold: iBulletsToNumbersThreshold,
				sArrowsPlacement: sArrowsPlacement,
				bBottom: true,
				bShowPageIndicator: oCarousel.getShowPageIndicator()
			}, oCarousel);
		}

		this._renderClosingDiv(rm);
		//page-wrap ends
		rm.write("</div>");
	}

});