sap.ui.define([
	"sap/ui/core/theming/Parameters",
	"sap/ui/core/library"
], function(ThemingParameters, sapCoreLib) {
	"use strict";

	var backgroundColor = ThemingParameters.get("sapUiGroupContentBackground");
	var OpenState = sapCoreLib.OpenState;

	return {
		render: function(renderManager, control) {
			if (control._getPopup().getOpenState() === OpenState.OPENING) {
				var textControl = control.getAggregation("_text");
				var alternateControl = control.getAggregation("content");
				var controlToRender = alternateControl ? alternateControl : textControl.addStyleClass("sapUiTinyMargin");
				renderManager.write("<div")
					.writeControlData(control)
					.addClass("myCustomTooltip")
					.writeClasses()
					.addStyle("max-width", control.getMaxWidth())
					.addStyle("height", control.getHeight())
					.addStyle("width", control.getWidth())
					.addStyle("background-color", backgroundColor) // theme-dependent
					.writeStyles()
					.write(">")
					.renderControl(controlToRender)
					.write("</div>");
			}
		}
	};
}, true);