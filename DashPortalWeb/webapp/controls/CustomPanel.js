/*Control for rendering cards of type Panel*/
jQuery.sap.declare("valvoline.dash.portal.DashPortalWeb.controls.CustomPanel");

jQuery.sap.require("sap.m.Panel", "sap.m.Button", "sap.ui.core.Icon");

sap.m.Panel.extend("valvoline.dash.portal.DashPortalWeb.controls.CustomPanel", {

	metadata: {
		properties: {
			"headerQuestion": {
				type: "String",
				defaultValue: ""
			},
			"headerQuestion2": {
				type: "String",
				defaultValue: ""
			},
			"type": {
				type: "String",
				defaultValue: ""
			},
			"active": {
				type: "Boolean",
				defaultValue: true
			},
			"caseNumber": {
				type: "String",
				defaultValue: ""
			}
		}
	},
	/** @module CustomPanel */
	/**
	 * Initializes the panel render
	 * @function renderer
	 * @param {Object} oRm - render manager
	 * @param {Object} oControl - element to render
	 */
	renderer: function(oRm, oControl) {
		this.startPanel(oRm, oControl);
		oControl.renderHeader(oRm, oControl);
		this.renderContent(oRm, oControl);
		this.endPanel(oRm);
	},
	/**
	 * Renders the panel header.
	 * @function renderHeader
	 * @param {Object} oRm - render manager
	 * @param {Object} oControl - element to render
	 */
	renderHeader: function(oRm, oControl) {
		var bIsExpandable = oControl.getExpandable(),
			bIsExpanded = oControl.getExpanded(),
			oHeaderTBar = oControl.getHeaderToolbar(),
			sHeaderClass,
			that = this;

		// we need a wrapping div around icon and header
		// otherwise the border needed for both do not exact align
		oRm.write("<header tabindex='0'");
		if (oHeaderTBar) {
			sHeaderClass = "sapMPanelWrappingDivTb";
		} else {
			sHeaderClass = "marginHeaderTop";
		}
		oRm.addClass(sHeaderClass);
		oRm.addClass("reusable-cursor-pointer-modifier");
		if (bIsExpanded) {
			oRm.addClass(sHeaderClass + "Expanded");
		}
		oRm.writeClasses();
		oRm.writeAttribute("id", oControl.getId() + "-headerPanel");
		oRm.write(">");

		// render header
		var sHeaderText = oControl.getHeaderText();
		var sHeaderQuestion = oControl.getHeaderQuestion();
		if (oHeaderTBar) {
			oHeaderTBar.setDesign(sap.m.ToolbarDesign.Transparent, true);
			oHeaderTBar.addStyleClass("sapMPanelHeaderTB");
			oRm.renderControl(oHeaderTBar);

		} else if (sHeaderText || bIsExpandable) {
			oRm.write("<div class='sapMVBox'>");
			oRm.write("<div class='sapMHBox sapMFlexBox sapMFlexItem marginHeaderBottom'>");

			var oIcon = oControl._getIcon();

			oRm.renderControl(oIcon);

			//title
			if (sHeaderText !== "" && oControl.getType() !== "equipment") {
				oRm.write("<h1");
				oRm.addClass("sapMPanelHdr");
				oRm.addClass("faqsTitle2");
				oRm.addClass("limitAlignCases");
				oRm.writeClasses();
				oRm.addStyle("margin-left", "4.5rem");
				oRm.addStyle("min-height", "1.2rem");
				oRm.writeStyles();
				oRm.writeAttribute("id", oControl.getId() + "-header");
				oRm.write(">");
				oRm.writeEscaped(sHeaderText);
				oRm.write("</h1>");
				oRm.write("</div>");
				oRm.write("<div class='sapMHBox sapMFlexBox sapMFlexItem marginBottomFaqTitle2'>");
			}
			//subtitle
			oRm.write("<h2");
			oRm.addClass("sapMPanelHdr");
			oRm.addClass("faqsTitle3");
			if (oControl.getType() !== "cases") {
				oRm.addClass("limitAlignCases");
				oRm.writeClasses();
			} else {
				oRm.addClass("limitAlignCases2");
				oRm.writeClasses();
			}
			if (oControl.getType() !== "cases") {
				oRm.addStyle("margin-left", "4.5rem");
			} else {
				oRm.addStyle("margin-left", "1.5rem");
			}
			oRm.addStyle("min-height", "auto");
			oRm.addStyle("padding-bottom", "2px");
			oRm.writeStyles();
			oRm.writeAttribute("id", oControl.getId() + "-header");
			oRm.write(">");
			oRm.writeEscaped(sHeaderQuestion);
			oRm.write("</h2>");

			oRm.write("</div>");

			oRm.write("</div>");
		}

		if (bIsExpandable) {
			oRm.write("</header>");
		}

		var oInfoTBar = oControl.getInfoToolbar();

		if (oInfoTBar) {
			if (bIsExpandable) {
				// use this class as marker class to ease selection later in onAfterRendering
				oInfoTBar.addStyleClass("sapMPanelExpandablePart");
			}

			// render infoBar
			oInfoTBar.setDesign(sap.m.ToolbarDesign.Info, true);
			oInfoTBar.addStyleClass("sapMPanelInfoTB");
			oRm.renderControl(oInfoTBar);
		}

		//Assign Expanded function to Header
		if (bIsExpandable) {
			//Timeout equal to 0 to guarantee the header is created
			setTimeout(function() {
				var oHeader = $("#" + that.getId() + "-headerPanel");
				//Assign Expanded function to header and change icons based on expanded or collapse
				oHeader.click(function() {
					that.setExpanded(!that.getExpanded(),"fromPress");
				});
			}, 0);
		}
	},
	/**
	 * Expands the panel.
	 * @function setExpanded
	 * @param {string} setExpanded - Panel expandaded state
	 * @param {string} sCallerOrigin - If called as result of a click it will contain the string "fromPress"
	 */
	setExpanded: function(bExpanded, sCallerOrigin) {
		if (this.getExpanded() === bExpanded) {
			return this;
		}
		if (this.oIconCollapsed !== undefined) {
			if (bExpanded === false) {
				this.oIconCollapsed.setSrc("sap-icon://add");
			} else {
				this.oIconCollapsed.setSrc("sap-icon://less");
			}
		}

		this.setProperty("expanded", bExpanded, true);

		if (!this.getExpandable()) {
			return this;
		}

		// ARIA
		this._getIcon().$().attr("aria-expanded", this.getExpanded());

		this._toggleExpandCollapse();
		this._toggleCssClasses();
		this.fireExpand({
			expand: bExpanded
		});

		//GTM
		if (bExpanded !== false) {
			//Tracks only if panel was expanded
			if (window.location.hash.indexOf("faqs") > -1) {
				var categoryFaq;
				switch (this.getParent().getParent().getItems().indexOf(this.getParent())) {
					case 1:
						categoryFaq = "Dash Faqs";
						break;

					case 3:
						categoryFaq = "Contact Us";
						break;

					case 5:
						categoryFaq = "Product";
						break;
					default:
						break;
				}
				if (categoryFaq !== undefined) {
					window.dataLayer.push({
						'event': 'faqexpand',
						'eventCategory': 'FAQs Expansions -click',
						'eventAction': categoryFaq,
						'eventLabel': this.getHeaderText() + " | " + this.getHeaderQuestion()
					});
				}
			} else if (window.location.hash.indexOf("Equipment") > -1 && sCallerOrigin === "fromPress") {
				window.dataLayer.push({
					'event': 'eqpmntExpansion',
					'eventCategory': 'Equipment',
					'eventAction': 'Equipment Expand -click',
					'eventLabel': this.getHeaderQuestion()
				});
			}
		}

		return this;
	},

	/**
	 * Colapses the panel
	 * @function _toggleExpandCollapse
	 */
	_toggleExpandCollapse: function() {
		var oOptions = {};
		if (!this.getExpandAnimation()) {
			oOptions.duration = 0;
		}
		this.$().children(".sapMPanelExpandablePart").slideToggle(oOptions);
	},

	/**
	 * Toggles CSS Clases
	 * @function _toggleCssClasses
	 */
	_toggleCssClasses: function() {
		var $this = this.$();

		// for controlling the visibility of the border
		$this.children(".sapMPanelWrappingDiv").toggleClass("sapMPanelWrappingDivExpanded");
		$this.children(".sapMPanelWrappingDivTb").toggleClass("sapMPanelWrappingDivTbExpanded");
		$this.find(".sapMPanelExpandableIcon").first().toggleClass("faqsButtonPressed");

	},
	/**
	 * Changes the panel icon
	 * @function _createIcon
	 */
	//Change icon
	_createIcon: function() {
		var that = this;
		var newIcon = new sap.ui.core.Icon({
			id: that.getId() + "-CollapsedImg",
			src: "sap-icon://add",
			decorative: false,
			useIconTooltip: false
		}).addStyleClass("sapMPanelExpandableIcon reusable-cursor-pointer-modifier");
		return newIcon;
	}

});