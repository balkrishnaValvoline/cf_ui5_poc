jQuery.sap.declare("valvoline.dash.portal.DashPortalWeb.controls.CustomSuggestionItem");

jQuery.sap.require("sap.m.SuggestionItem");

var customItem = sap.m.SuggestionItem.extend("valvoline.dash.portal.DashPortalWeb.controls.CustomSuggestionItem", {

	metadata: {
		properties: {
			"styleClass": {
				type: "String",
				defaultValue: ""
			}
		}
	}
});

customItem.prototype.render = function(oRenderManager, oItem, sSearch, bSelected) {
	var rm = oRenderManager;
	var text = oItem.getText();
	var icon = oItem.getIcon();
	var styleClass = oItem.getStyleClass();
	var parent = oItem.getParent();
	var items = parent && parent.getSuggestionItems && parent.getSuggestionItems() || [];
	var index = items.indexOf(oItem);
	sSearch = sSearch || "";

	rm.write("<li");
	rm.writeElementData(oItem);
	rm.addClass(styleClass);
	rm.addClass("sapMSuLI");
	rm.addClass("sapMSelectListItem");
	rm.addClass("sapMSelectListItemBase");
	rm.addClass("sapMSelectListItemBaseHoverable");

	rm.writeAttribute("role", "option");
	rm.writeAttribute("aria-posinset", index + 1);
	rm.writeAttribute("aria-setsize", items.length);
	if (bSelected) {
		rm.addClass("sapMSelectListItemBaseSelected");
		rm.writeAttribute("aria-selected", "true");
		if (parent) {
			parent.$("I").attr("aria-activedecendant", oItem.getId());
		}
	}
	rm.writeClasses();
	rm.write(">");

	if (text) {
		rm.write("<div>");
		if (icon) {
			rm.writeIcon(icon, "sapMSuggestionItemIcon", {});
			rm.write("<span");
			rm.addStyle("margin-left", ".5rem");
		} else {
			rm.write("<span");
		}

		rm.writeStyles();
		rm.write(">");
		var i;
		i = text.toUpperCase().indexOf(sSearch.toUpperCase());
		if (i > -1) {
			rm.writeEscaped(text.slice(0, i));
			rm.write("<b>");
			rm.writeEscaped(text.slice(i, i + sSearch.length));
			rm.write("</b>");
			text = text.substring(i + sSearch.length);
		}
		rm.writeEscaped(text);
		rm.write("</span>");

		rm.write("</div>");
	}

	rm.write("</li>");
};