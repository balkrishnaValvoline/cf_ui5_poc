function httpCall(url, callback){
    var xmlhttp;
    // compatible with IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function(){
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200){
            callback(JSON.parse(xmlhttp.response));
        } else {
        	//Back end service call failed, no access to the GTM/GA tracking ID's
        }
    };
    xmlhttp.open("GET", url, false);
    xmlhttp.send();
}
function handleSuccess(dataReturn){
	window.trackingID = {"gtm" : dataReturn.GTMtrackingID, "ga": dataReturn.GAtrackingID};
}
httpCall("/service-layer-dash/core/parametersByGroupID?groupID=GoogleAnalytics", handleSuccess);
var dataLayer = [];

(function(w, d, s, l, i) {
	w[l] = w[l] || [];
	w[l].push({
		'gtm.start': new Date().getTime(),
		event: 'gtm.js' 
	});
	var f = d.getElementsByTagName(s)[0],
		j = d.createElement(s),
		dl = l != 'dataLayer' ? '&l=' + l : '';
	j.async = true;
	j.src =
		'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
	f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', window.trackingID.gtm);

(function(i, s, o, g, r, a, m) {
	i['GoogleAnalyticsObject'] = r;
	i[r] = i[r] || function() {
		(i[r].q = i[r].q || []).push(arguments);
	}, i[r].l = 1 * new Date();
	a = s.createElement(o),
		m = s.getElementsByTagName(o)[0];
	a.async = 1;
	a.src = g;
	m.parentNode.insertBefore(a, m);
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
ga('create', window.trackingID.ga, 'auto'); //Accenture DASH team's GA
ga('set', 'page', 'index.html');
ga('send', 'pageview');