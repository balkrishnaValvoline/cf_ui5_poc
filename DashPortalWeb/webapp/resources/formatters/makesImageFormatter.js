/**
 * formatter JavaScript (V1.0):
 * This javascript contains formatter functions for the  make image icon that require the calling component context
 * @author Marcos Matos
 */
sap.ui.define(function () {
		var makesImageFormatter = {
			makesImageIconFormatter: function (image) {
				if (image === null) {
					this.getParent().addStyleClass("makeImage");
					this.getParent().addStyleClass("makePadding");
				} else if (image === "N/A") {
					//to be able to add an img element without the image
					return "data:image/png;base64,";
				} else {
					var base64Img = image && image.trim();
					if (base64Img && base64Img.substring(0, 2) !== "{}" && base64Img.substring(0, 4) !== "data") {
						return "data:image/png;base64," + base64Img;
					}
				}
				return null;
			}
		};
		return makesImageFormatter;
	},
	true);