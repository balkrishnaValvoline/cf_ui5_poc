/**
 * formatter JavaScript (V1.0):
 * This javascript contains formatter functions for the stockingSoluction that require the calling component context
 * @author Daniel Andrade
 */
sap.ui.define(function () {
	var stockingSolutionFormatter = {
		/**
		 * Returns the correct value to be presented
		 * @function setContextBinding
		 */
		setContextBinding: function (row) {
			if (row) {
				var sCellID = this.getId();
				var aAggregationCells = this.getParent().getParent().getCells();
				for (var i = 0; i < aAggregationCells.length; i++) {
					if (aAggregationCells[i].getAggregation("items")[0].getId() === sCellID) {
						break;
					}
				}
				return (row[i].reorderLabel !== undefined ? row[i].reorderLabel : row[i]);
			} else {
				return "";
			}
		},
		/**
		 * Calculates if the Cart icon should be displayed
		 * @function setAdd2CartVisibility
		 */
		setAdd2CartVisibility: function (row, sAccess) {
			var bReorderQty = false;
			if (sAccess && row) {
				var oParentBindingContext = this.getParent().getParent().getBindingContext("stockingSolution");
				if (oParentBindingContext) {
					var sCellID = this.getId();
					var aAggregationCells = this.getParent().getParent().getCells();
					//starts at 2 to ignore the viscosity and the 1st reorderPoint
					for (var i = 2; i < aAggregationCells.length; i++) {
						if (aAggregationCells[i].getAggregation("items")[1].getId() === sCellID) {
							break;
						}
					}
					if (i % 2 === 0 && row[i].reorderLabel !== "-") {
						bReorderQty = true;
					}
				}
			}
			return bReorderQty;
		},
		/**
		 * Sets the header span in for multi header cases
		 * @function setHeaderSpan
		 */
		setHeaderSpan: function (value) {
			var oParentBindingContext = this.getParent().getBindingContext("stockingSolution");
			if (oParentBindingContext) {
				var sPath = oParentBindingContext.getPath();
				var oEntries = sPath.split("/");
				var iPosition = oEntries[oEntries.length - 1];
				//Check if number is not odd
				if (iPosition % 2 === 1) {
					oEntries[oEntries.length - 1] = parseInt(iPosition) + 1;
					var sNextPath = oEntries.toString().replace(/,/g, "/");
					var oNextHeader = oParentBindingContext.getModel().getProperty(sNextPath) || undefined;
					var oCurrentHeader = oParentBindingContext.getModel().getProperty(sPath);
					if (oNextHeader && oNextHeader.main === oCurrentHeader.main) {
						var oController = this;
						setTimeout(function () {
							if (oController.getParent() !== undefined) {
								oController.getParent().setHeaderSpan(2, 1);
							}
						}, 0);
					}
				}
			}
			return value;
		}
	};
	return stockingSolutionFormatter;
}, true);