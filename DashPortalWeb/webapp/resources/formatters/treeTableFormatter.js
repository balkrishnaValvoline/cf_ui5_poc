/**
 * formatter JavaScript (V1.0):
 * This javascript contains formatter functions for the treeTable that require the calling component context
 * @author Daniel Andrade
 */
sap.ui.define(function() {
	var treeTableFormatter = {
		
		/**
		 * Validates edit permitions for Corporate based on the User's account level
		 * @function userAccountLvlValidatorForCorporate
		 */
		userAccountLvlValidatorForCorporate: function(accountLevel){
			var userAccountLevel = sap.ui.getCore().getModel("oAccounts").getData().level;
			if(accountLevel === "Level 4"){
				switch(userAccountLevel){
				case(4):
					return true;
				case(3):
					return false;
				case(2):
					return false;
				case(1):
					return false;
				default:
					return false;
				}
			}
			return false;
		},
		
		/**
		 * Validates edit permitions for Regions based on the User's account level
		 * @function userAccountLvlValidatorForDistrict
		 */
		userAccountLvlValidatorForRegion: function(userAccountLevel){
			switch(userAccountLevel){
				case(4):
					return true;
				case(3):
					return false;
				case(2):
					return false;
				case(1):
					return false;
				default:
					return false;
			}
		},
		
		/**
		 * Validates edit permitions for Districts based on the User's account level
		 * @function userAccountLvlValidatorForStore
		 */
		userAccountLvlValidatorForDistrict: function(userAccountLevel, accLevel){
			if (userAccountLevel === 4){
				return true;
			} else if (userAccountLevel === 3 && accLevel === "Level 2"){
				return true;
			}
			return false;
		},
		
		/**
		 * Validates edit permitions for Stores based on the User's account level
		 * @function userAccountLvlValidatorForStore
		 */
		userAccountLvlValidatorForStore: function(userAccountLevel, accLevel){
			if (userAccountLevel === 4){
				return true;
			} else if (userAccountLevel === 3 && (accLevel === "Level 2" || accLevel === "Level 1" || accLevel === undefined)){
				return true;
			} else if (userAccountLevel === 2 && accLevel === "Level 1" || accLevel === undefined){
				return true;
			}else if (userAccountLevel === 1 && accLevel == undefined){
				return true;
			}
			return false;
		},
		
		/**
		 * Sets the Region fields to enable based on level
		 * @function inputFormatterAccountRegion
		 */
		inputFormatterAccountRegion: function(accountLevel) {
			var editable = false;
			if (accountLevel !== null && (accountLevel === "Level 4" || accountLevel === "Level 3")) {
				var userAccountLevel = sap.ui.getCore().getModel("oAccounts").getData().level;
				editable = treeTableFormatter.userAccountLvlValidatorForRegion(userAccountLevel);
			}
			return editable;
		},

		/**
		 * Sets the District fields to enable based on level
		 * @function inputFormatterAccountDistrict
		 */
		inputFormatterAccountDistrict: function(accountLevel) {
			var editable = false;
			if (accountLevel === "Level 4" || accountLevel === "Level 3" || accountLevel === "Level 2") {
				var userAccountLevel = sap.ui.getCore().getModel("oAccounts").getData().level;
				editable = treeTableFormatter.userAccountLvlValidatorForDistrict(userAccountLevel, accountLevel);
			}
			return editable;
		},

		/**
		 * Sets the Store fields to enable based on level
		 * @function inputFormatterAccountStore
		 */
		inputFormatterAccountStore: function(accountLevel) {
			var editable = false;
			if (accountLevel === "Level 4" || accountLevel === "Level 3" || accountLevel === "Level 2" || accountLevel === "Level 1" || accountLevel === undefined) {
				var userAccountLevel = sap.ui.getCore().getModel("oAccounts").getData().level;
				editable = treeTableFormatter.userAccountLvlValidatorForStore(userAccountLevel, accountLevel);
			}
			return editable;
		}
	};
	return treeTableFormatter;
}, true);