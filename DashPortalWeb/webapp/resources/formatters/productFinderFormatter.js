/**
 * formatter JavaScript (V1.0):
 * This javascript contains formatter functions for the treeTable that require the calling component context
 * @author Daniel Andrade
 */
sap.ui.define(function() {
	var productFinderFormatter = {
		productFinderEnabledNameLinkFormatter: function(accountLevel){
			if (accountLevel) {
				this.removeStyleClass("valvolineLinkDisabled");
				this.addStyleClass("valvolineLink");
				return true;
			} else {
				this.removeStyleClass("valvolineLink");
				this.addStyleClass("valvolineLinkDisabled");
				return false;
			}
		}
	};
	return productFinderFormatter;
}, true);