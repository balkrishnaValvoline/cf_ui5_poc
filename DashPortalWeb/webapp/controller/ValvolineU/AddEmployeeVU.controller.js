sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter"
], function (Base, JSONModel, Filter) {
	"use strict";
	var _oRouter,
		_oController;

	var _firstnameInputOk = false;
	var _lastnameInputOk = false;
	var _usernameInputOk = false;
	var _emailInputOk = false;
	var _1stRender = true;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.ValvolineU.AddEmployeeVU", {

		/** @module AddEmployeeVU */
		/**
		 * This function initializes the controller.
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oController = this;
			_oRouter = this.getRouter();
			_oRouter.getRoute("addEmployeeVU").attachPatternMatched(this._onObjectMatched, this);

			//Remove side Navigation
			sap.ui.getCore().byId("__xmlview0--vbox_background_main").destroy();
			sap.ui.getCore().byId("__xmlview0--menuMobileHeader").destroy();
			//Remove Shell width Limit
			$(".sapMShellAppWidthLimited ").removeClass("sapMShellAppWidthLimited");
		},

		/**
		 * If it is the first render, calls the the function to attach the focus out Events
		 * @function _onObjectMatched
		 */
		onBeforeRendering: function () {
			if (_1stRender) {
				setTimeout(function () {
					_oController.inputFieldFocusOutFunction();
				}, 0);
				_1stRender = false;
			}
		},

		/**
		 * Loads user data information
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("SIMPLIFIEDUSER_PAGE")) {
				return;
			}
		},

		/**
		 * Validates First name
		 * @function onCheckInputFirstname
		 */
		onCheckInputFirstname: function () {
			var firstnameInput = this.getView().byId("firstnameInput");
			if (firstnameInput.getValue().length <= 0) {
				firstnameInput.setValueState("Warning");
				_firstnameInputOk = false;
			} else {
				firstnameInput.setValueState("None");
				_firstnameInputOk = true;
			}
			this.enableAddUserButton();
		},

		/**
		 * Validates Last name
		 * @function onCheckInputLastname
		 */
		onCheckInputLastname: function () {
			var lastnameInput = this.getView().byId("lastnameInput");
			if (lastnameInput.getValue().length <= 0) {
				lastnameInput.setValueState("Warning");
				_lastnameInputOk = false;
			} else {
				lastnameInput.setValueState("None");
				_lastnameInputOk = true;
			}
			this.enableAddUserButton();
		},

		/**
		 * Validates Check Email
		 * @function onCheckInputEmail
		 */
		onCheckInputEmail: function () {
			var emailInput = this.getView().byId("emailInput");
			var usernameInput = this.getView().byId("usernameInput");

			// Live Change
			usernameInput.setValue(emailInput.getValue());

			var regex =
				/^[A-Za-z0-9._%+-]*[A-Za-z0-9]+@+[A-Za-z0-9]+[A-Za-z0-9.-]*\.[A-Za-z]{2,}$/;

			if (regex.test(emailInput.getValue())) {
				emailInput.setValueState("None");
				emailInput.removeStyleClass("warningInputField");
				_emailInputOk = true;
			} else {
				emailInput.setValueStateText(_oController.getResourceBundle().getText("UMCUText2IT"));
				emailInput.setValueState("Warning");
				emailInput.addStyleClass("warningInputField");
				_emailInputOk = false;
			}

			this.enableAddUserButton();
		},

		/**
		 * Function that handles the success case of the ajax call that validates the inputed user name
		 * @function onCheckInputUserNameSuccess
		 */
		onCheckInputUserNameSuccess: function (oController, oData, oPiggyBack) {
			var usernameInput = oController.getView().byId("usernameInput");
			var validCrossUserM = oController.getView().byId("validCrossUserM");
			var declineCrossUserM = oController.getView().byId("declineCrossUserM");
			if (oData.userAlreadyExists) {
				usernameInput.setValueStateText(oController.getResourceBundle().getText("UMUserExists"));
				usernameInput.setValueState("Error");
				declineCrossUserM.setVisible(true);
				validCrossUserM.setVisible(false);
				usernameInput.addStyleClass("errorInputField");
				_usernameInputOk = false;
			} else {
				usernameInput.removeStyleClass("errorInputField");
				usernameInput.setValueState("None");
				declineCrossUserM.setVisible(false);
				validCrossUserM.setVisible(true);
				_usernameInputOk = true;
			}
			oController.enableAddUserButton();
		},

		/**
		 * Function that handles the error case of the ajax call that validates the inputed user name
		 * @function onCheckInputUserNameError
		 */
		onCheckInputUserNameError: function (oController, oError, oPiggyBack) {
			var usernameInput = oController.getView().byId("usernameInput");
			usernameInput.setValueStateText(oController.getResourceBundle().getText("UMUserExists"));
			usernameInput.setValueState("Error");
			_usernameInputOk = false;
			oController.enableAddUserButton();
		},

		/**
		 * Check Input Username validation
		 * @ parameter
		 */
		onCheckInputUsernameValidation: function () {
			var usernameInput = this.getView().byId("usernameInput");
			var validCrossUserM = this.getView().byId("validCrossUserM");
			var declineCrossUserM = this.getView().byId("declineCrossUserM");
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();

			var username = usernameInput.getValue();
			if (username.length > 0) {
				var path = "validation?username=";
				var xsURL = "/usermgmt/user/" + path + username + "&lang=" + selectedLanguage;
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onCheckInputUserNameSuccess, _oController.onCheckInputUserNameError);
			} else {
				usernameInput.removeStyleClass("errorInputField");
				usernameInput.setValueState("None");
				validCrossUserM.setVisible(false);
				declineCrossUserM.setVisible(false);
			}
		},

		/**
		 * Validates Input Username
		 * @function onCheckInputUsername
		 */
		onCheckInputUsername: function () {
			var usernameInput = _oController.getView().byId("usernameInput");
			var validCrossUserM = _oController.getView().byId("validCrossUserM");
			var declineCrossUserM = _oController.getView().byId("declineCrossUserM");
			_usernameInputOk = false;
			usernameInput.removeStyleClass("errorInputField");
			usernameInput.setValueState("None");
			validCrossUserM.setVisible(false);
			declineCrossUserM.setVisible(false);
			usernameInput.setValueStateText(_oController.getResourceBundle().getText("UMCUText1IT"));
			if (usernameInput.getValue().length <= 0 || usernameInput.getValue().indexOf(" ") !== -1) {
				usernameInput.setValueState("Warning");
				_usernameInputOk = false;
			} else {
				usernameInput.removeStyleClass("errorInputField");
				usernameInput.setValueState("None");
			}
			this.enableAddUserButton();
		},

		/**
		 * 
		 */
		enableAddUserButton: function () {
			var addUserButton = this.getView().byId("addUserButton");
			if (_firstnameInputOk && _lastnameInputOk && _usernameInputOk && _emailInputOk) {
				addUserButton.setEnabled(true);
			} else {
				addUserButton.setEnabled(false);
			}
		},

		/**
		 * 
		 */
		inputFieldFocusOutFunction: function () {
			var usernameInputField = this.getView().byId("usernameInput").getDomRef();
			var emailInput = this.getView().byId("emailInput").getDomRef();
			var controller = this;
			usernameInputField.addEventListener("focusout", function () {
				controller.onCheckInputUsernameValidation();
			});
			emailInput.addEventListener("focusout", function () {
				controller.onCheckInputUsernameValidation();
			});
		},

		/**
		 * Generate a message Strip to be displayed in the page
		 * @function onShowPageMessageStrip
		 * @param {string} text - Text to be displayed
		 * @param {string} type - Type of message
		 */
		onShowPageMessageStrip: function (text, type) {
			var messageStripContainer = this.getView().byId("messageStripContainer");
			var messageStrip = new sap.m.MessageStrip({
				text: text,
				type: type,
				width: "90%",
				showCloseButton: true
			});
			messageStripContainer.addItem(messageStrip);
		},

		/**
		 * Function that handles the success case of the ajax call to add the new User
		 * @function onAddUserSuccess
		 */
		onAddUserSuccess: function (oController, oData, oPiggyBack) {
			var addButton = oController.getView().byId("addUserButton");
			var usernameInputRoot = oController.getView().byId("usernameInput");
			//Reset initial screen		
			_usernameInputOk = false;
			_emailInputOk = false;
			_firstnameInputOk = false;
			_lastnameInputOk = false;

			usernameInputRoot.setValue("");
			oController.getView().byId("emailInput").setValue("");
			oController.getView().byId("firstnameInput").setValue("");
			oController.getView().byId("lastnameInput").setValue("");

			usernameInputRoot.setValueState("None");
			oController.getView().byId("declineCrossUserM").setVisible(false);
			oController.getView().byId("validCrossUserM").setVisible(false);

			addButton.setText(oController.getResourceBundle().getText("VUPopUpText2BT"));
			oController.getView().byId("fragmentContent").setVisible(false);
			oController.getView().byId("successText").setVisible(true);

			oController.getView().setBusy(false);
		},

		/**
		 * Function that handles the error case of the ajax call to add the new User
		 * @function onAddUserError
		 */
		onAddUserError: function (oController, oError, oPiggyBack) {
			oController.getView().setBusy(false);
			if (oError.status === 409) {
				oController.onShowPageMessageStrip(oController.getResourceBundle().getText("UMUserExists"), "Warning");
			} else {
				oController.onShowPageMessageStrip(oError.status + " " + oController.getResourceBundle().getText("UMErrorDuringOperation"),
					"Error");
			}
		},

		/**
		 * Calls the service responsible adding the the user
		 * @function onAddUser
		 */
		onAddUser: function () {
			var controller = this;
			var addButton = controller.getView().byId("addUserButton");
			if (addButton.getText() === controller.getResourceBundle().getText("VUPopUpText2BT")) {
				addButton.setText(controller.getResourceBundle().getText("VUPopUpText1BT"));
				addButton.setEnabled(false);
				controller.getView().byId("fragmentContent").setVisible(true);
				setTimeout(function () {
					_oController.inputFieldFocusOutFunction();
				}, 0);
				controller.getView().byId("successText").setVisible(false);
			} else {
				// Set Busy Indicatores
				this.getView().setBusyIndicatorDelay(0);
				this.getView().setBusy(true);

				// Values based on inputs
				var firstnameInput = this.getView().byId("firstnameInput").getValue();
				var lastnameInput = this.getView().byId("lastnameInput").getValue();
				var usernameInput = this.getView().byId("usernameInput").getValue();
				var emailInput = this.getView().byId("emailInput").getValue();

				// Send all the Parameters
				var user = {};
				user.username = usernameInput;
				user.firstname = firstnameInput;
				user.lastname = lastnameInput;
				user.email = emailInput;

				var userjsonString = JSON.stringify(user);
				var xsURL = "/usermgmt/user/simplifiedUser";
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				_oController.handleAjaxJSONCall(controller, true, xsURL, "POST", controller.onAddUserSuccess, controller.onAddUserError,
					userjsonString);
			}
		}
	});
});