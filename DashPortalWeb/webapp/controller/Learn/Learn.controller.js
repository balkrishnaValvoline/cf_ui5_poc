sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function (Base) {
	"use strict";
	jQuery.sap.require("valvoline.dash.portal.DashPortalWeb.controls.CardContainer");
	var _oController;
	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Learn.Learn", {
		/** @module Learn */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			_oController = this;
			oRouter.getRoute("learn").attachPatternMatched(this._onObjectMatched, this);
		},

		onAfterRendering: function () {
			/* Set the menu as active if the app is reloaded */
			$(".js-learn-sidenav").addClass("sidenav__item--active");

			//see if URL is available
			var sURL = this.getView().getModel("HowToVideos").getData().HowToValUniversityURL;
			if (sURL !== undefined) {
				this.getView().byId("howToVideosButton").setVisible(true);
				this.getView().byId("howToVideo").setVisible(true);
			}
		},

		/**
		 * Populate Cards
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("LEARN_PAGE")) {
				return;
			}
			this.headerIconAccess();

			//get user info and direct account
			this.getUserInformation(this);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			// Populate Cards
			_oController.byId("cardContainerLearnVU").setViewContext(_oController);
			this.populateCardContainer("Learn VU", "4", "All");

			//stop videos on changing page
			this.stopVideo();

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "learn"
			});
		}
	});
});