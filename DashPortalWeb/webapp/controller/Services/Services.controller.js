sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function (Base) {
	"use strict";
	var _oController;
	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Services.Services", {
		/** @module Services */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			var oRouter = this.getRouter();
			oRouter.getRoute("service").attachPatternMatched(this._onObjectMatched, this);
			_oController = this;
			/* 
			Check EventBus for a Recruiting Sign-up Message
			*/
			sap.ui.getCore().getEventBus().subscribe("displayRecruitingMessage", "displayRecruitingMessage", this.onDisplayRecruitingMessage,
				this);
		},

		onAfterRendering: function () {
			/* Set the menu as active if the app is reloaded */
			$(".js-services-sidenav").addClass("sidenav__item--active");
		},

		/**
		 * Verify authorization and layout access
		 * @function  _onObjectMatched
		 * @param {Object} event - Triggered when navigating to Solutions Page
		 */
		_onObjectMatched: function () {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("SOLUTIONS_PAGE")) {
				return;
			}

			//get user info and direct account
			this.getUserInformation(this);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			this.headerIconAccess();

			// Populate Cards
			this.populateCards();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);
		},

		/**
		 * Populate Cards for Service view.
		 * @function populateCards
		 */
		populateCards: function () {
			_oController.byId("cardContainerService").setViewContext(_oController);
			this.populateCardContainer("Service", "6", "All");

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "solutions"
			});
		},

		onDisplayRecruitingMessage: function (channel, event, data) {
			var recruitingConfirmation = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog");

			//Set Dialog Buttons
			sap.ui.getCore().byId("icon_Dialog_Warning").setSrc("sap-icon://customfont/success");
			sap.ui.getCore().byId("icon_Dialog_Warning").removeStyleClass("warningMessageDialogIcon");
			sap.ui.getCore().byId("icon_Dialog_Warning").addStyleClass("successMessageDialogIcon");
			sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(this.onRecruitingConfirm);
			sap.ui.getCore().byId("button_Dialog_Warning_No").setVisible(false);
			sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(this.getResourceBundle().getText("DLWokTXT"));

			//Set Dialog Message
			sap.ui.getCore().byId("text1_Dialog_Warning").setText(this.getResourceBundle().getText("RPSuccessBody"));

			if (data.isJobBoard === "T") {
				sap.ui.getCore().byId("text2_Dialog_Warning").setText(this.getResourceBundle().getText("RSUDJobBoardMsg"));
			}

			//Set Dialog Title
			sap.ui.getCore().byId("title_Dialog_Warning").setText(this.getResourceBundle().getText("RPSuccessTitle"));

			recruitingConfirmation.open();
		},

		onRecruitingConfirm: function () {
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();
			sap.ui.getCore().setModel(null, "layoutAccess");
			dialog.setBusy(false);
		}
	});
});