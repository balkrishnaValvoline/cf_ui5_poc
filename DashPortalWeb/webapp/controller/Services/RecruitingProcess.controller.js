sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function(Base) {
	"use strict";

	var _oView;
	var _oController;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Services.RecruitingProcess", {
		/** @module Insights Distributor */
		/**
		 * This function is called when the fist rendered. Set the Account models, and builds all the diferent Graphs displayed in the view.
		 * @function onInit
		 */
		onInit: function() {
			_oView = this.getView();
			_oController = this;
			Base.prototype.onInit.call(this);
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("recruiting").attachPatternMatched(this._onObjectMatched, this);
		},

		/**
		 * Set the menu as active if the app is reloaded and makes an element in the filter disabled
		 * @function onAfterRendering
		 */
		onAfterRendering: function() {
			$(".js-services-sidenav").addClass("sidenav__item--active");
		},

		/**
		 * This function is called everytime the page is loaded, and refresh the data and the page opening.
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function() {
			// Clears all error message
			this.clearServerErrorMsg(this);
			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "services"
			});
			//Page Layout Configuration
			if (!this.checkRecruitingPageAccess("MARKETING_PAGE")) {
				return;
			}
			
			//get user info and direct account
			this.getUserInformation(this); 
			//check Language and set selected Key in combo box
			this.onChangeLanguage();
			
			this.headerIconAccess();
			
			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			//Get Pricing options for subscription
			this.getPricingOptions();
			// Check if trial is availabe and display/hide trial button
			this.checkTrial();

		},

		/**
		 * Function that handles the success case of the ajax call to get pricing options
		 * @function onGetPricingOptionsSuccess
		 */
		onGetPricingOptionsSuccess: function(oController, oData, oPiggyBack) {
			var pricingTable = oController.getView().byId("PricingOptionsTable");
			if (oData) {
				var pricingOption = oData;

				var oModelPricingOptionRow = new sap.ui.model.json.JSONModel();
				var oModelPricingOptionColumns = new sap.ui.model.json.JSONModel();

				if (pricingOption.currency) {
					pricingOption.rows[0].currency = pricingOption.currency;
				}
				oModelPricingOptionRow.setData(pricingOption.rows);
				oModelPricingOptionColumns.setData(pricingOption.columns);

				_oView.setModel(oModelPricingOptionRow, "pricingOption");
				_oView.setModel(oModelPricingOptionColumns, "pricingOptionColumns");

				var oModelSelectPlan = new sap.ui.model.json.JSONModel();
				if (pricingOption.columns.length > 0 && pricingOption.rows.length > 0) {
					var selectPlan = [{
						option: pricingOption.columns[1].columnName + " - " + pricingOption.currency + oController.formatValuesGroupingDecimalSeparators(
							pricingOption.rows[0].valueColumn2),
						key: pricingOption.keys[0].keyColumn2
					}, {
						option: pricingOption.columns[2].columnName + " - " + pricingOption.currency + oController.formatValuesGroupingDecimalSeparators(
							pricingOption.rows[0].valueColumn3),
						key: pricingOption.keys[1].keyColumn3
					}, {
						option: pricingOption.columns[3].columnName + " - " + pricingOption.currency + oController.formatValuesGroupingDecimalSeparators(
							pricingOption.rows[0].valueColumn4),
						key: pricingOption.keys[2].keyColumn4
					}];
				}

				oModelSelectPlan.setData(selectPlan);
				_oView.setModel(oModelSelectPlan, "selectPlan");
			}

			pricingTable.setBusy(false);
		},

		/**
		 * Function that handles the error case of the ajax call to get pricing options
		 * @function onGetPricingOptionsError
		 */
		onGetPricingOptionsError: function(oController, oError, oPiggyBack) {
			var pricingTable = this.getView().byId("PricingOptionsTable");
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"RPerroristrialTXT"));
			pricingTable.setBusy(false);
			oController.getView().byId("signUp").setVisible(false);
		},

		/**
		 * Load pricing options for subscrisption plans
		 * @function getPricingOptions
		 */
		getPricingOptions: function() {
			var controller = this;

			var pricingTable = this.getView().byId("PricingOptionsTable");
			pricingTable.setBusyIndicatorDelay(0);
			pricingTable.setBusy(true);

			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();

			var xsURL = "/recruiting/recruitingSubscriptions?lang=" + selectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onGetPricingOptionsSuccess, controller.onGetPricingOptionsError);

		},

		/**
		 * Function that handles the success case of the ajax call that checks if the user has a free trial available
		 * @function onCheckTrialSuccess
		 */
		onCheckTrialSuccess: function(oController, oData, oPiggyBack) {
			if (oData === true) {
				oController.getView().byId("freeTrial").setVisible(false);
			} else {
				oController.getView().byId("freeTrial").setVisible(true);
			}
		},

		/**
		 * Function that handles the error case of the ajax call that checks if the user has a free trial available
		 * @function onCheckTrialError
		 */
		onCheckTrialError: function(oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"RPerroristrialTXT"));
			oController.getView().byId("freeTrial").setVisible(false);
		},

		/**
		 * Check if user has a free trial available
		 * @function checkTrial
		 */
		checkTrial: function() {
			var controller = this;
			var xsURL = "/recruiting/accountSubscriptionsTrial";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onCheckTrialSuccess, controller.onCheckTrialError);
		},

		/**
		 * Open the free trial popup
		 * @function onFreeTrial
		 */
		onFreeTrial: function(oEvent) {

			var recruitingFreeTrial = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog");

			//Set Warning Dialog Buttons
			sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(this.freeTrialSubscribe);
			sap.ui.getCore().byId("button_Dialog_Warning_No").attachPress(this.freeTrialClose);
			sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWyesTXT"));
			sap.ui.getCore().byId("button_Dialog_Warning_No").setText(_oController.getResourceBundle().getText("DLWnoTXT"));
			//Set Warning Dialog Message
			sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("RFTDText1") + ' ' +
				_oController.getResourceBundle().getText("RFTDText2"));
			sap.ui.getCore().byId("text2_Dialog_Warning").setText(_oController.getResourceBundle().getText("RFTDText3"));
			//Set Warnig Dialog Title
			sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("RFTDTitle"));

			recruitingFreeTrial.open();

			//GTM
			_oController.GTMDataLayer('bttmrcrtngbttn',
				'Recruiting',
				'Recruiting Button click',
				oEvent.getSource().getText()
			);

		},

		/**
		 * Function that handles the success case of the ajax call that subscribes to the free trial
		 * @function onFreeTrialSubscribeSuccess
		 */
		onFreeTrialSubscribeSuccess: function(oController, oData, oPiggyBack) {
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();
			sap.ui.getCore().setModel(null, "layoutAccess");
			dialog.setBusy(false);

			oController.getRouter().navTo("service");

			sap.ui.getCore().getEventBus().publish("displayRecruitingMessage", "displayRecruitingMessage", oPiggyBack);
		},

		/**
		 * Function that handles the error case of the ajax call that subscribes to the free trial
		 * @function onFreeTrialSubscribeError
		 */
		onFreeTrialSubscribeError: function(oController, oError, oPiggyBack) {
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();
			dialog.setBusy(false);

			setTimeout(function() {
				_oController.DataLossWarning =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", _oController);
				//Set Warning Buttons
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.onFreeTrialSubscribeErrorClose);
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWcloseTXT"));
				sap.ui.getCore().byId("button_Dialog_Warning_No").setVisible(false);
				//Set Warning Message
				sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWCaseSubmitedError"));
				//Set Warnig Dialog Title
				sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWCaseSubmitedErrorTitle"));
				sap.ui.getCore().byId("icon_Dialog_Warning").setSrc("sap-icon://customfont/error");
				sap.ui.getCore().byId("icon_Dialog_Warning").removeStyleClass("warningMessageDialogIcon");
				sap.ui.getCore().byId("icon_Dialog_Warning").addStyleClass("errorMessageDialogIcon");
				_oController.DataLossWarning.open();
			}, 0);
		},

		onFreeTrialSubscribeErrorClose: function() {
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();
		},

		/**
		 * Confirmation of free trial subscription and navigation to solutions page
		 * @function freeTrialSubscribe
		 */
		freeTrialSubscribe: function() {
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.setBusyIndicatorDelay(0);
			dialog.setBusy(true);

			var body = {
				isTrial: "T",
				isJobBoard: "F"
			};

			var oPiggyBack = {
				"isJobBoard": "F"
			};

			var bodyJsonString = JSON.stringify(body);
			var xsURL = "/recruiting/accountSubscriptions";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "POST", _oController.onFreeTrialSubscribeSuccess, _oController.onFreeTrialSubscribeError,
				bodyJsonString, oPiggyBack);
		},

		/**
		 * Decline of free trial subscription and close popup
		 * @function freeTrialSubscribe
		 */
		freeTrialClose: function() {
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();
		},

		/**
		 * Opens popup to select a subscription plan
		 * @function onSignUp
		 */
		onSignUp: function(oEvent) {
			if (this.recruitingSignUp) {
				this.recruitingSignUp.destroy();
			}
			this.recruitingSignUp =
				new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.Recruiting_SignUp", this);

			var i18nModel = new sap.ui.model.resource.ResourceModel({
				bundleUrl: "i18n/i18n.properties"
			});
			this.recruitingSignUp.setModel(i18nModel, "i18n");

			this.recruitingSignUp.open();

			var nPhoneNumber = sap.ui.getCore().getModel("oAccounts").getData().phone;

			sap.ui.getCore().byId("recruitingSignUpConfirm").setEnabled(false);

			sap.ui.getCore().byId("accname").setText(sap.ui.getCore().getModel("oDirectAccount").getData().accountname);
			sap.ui.getCore().byId("accountnumber").setText(parseInt(sap.ui.getCore().getModel("oDirectAccount").getData().accountnum, 10));
			sap.ui.getCore().byId("phone").setValue(nPhoneNumber);

			var oModelSelectPlan = new sap.ui.model.json.JSONModel();
			oModelSelectPlan.setData(this.getView().getModel("selectPlan").getData());
			sap.ui.getCore().byId("subscribeFor").setModel(oModelSelectPlan, "selectPlan");

			//GTM
			_oController.GTMDataLayer('bttmrcrtngbttn',
				'Recruiting',
				'Recruiting Button click',
				oEvent.getSource().getText()
			);
		},

		/**
		 * Function that handles the success case of the ajax call for the sign up confirmation 
		 * @function onSignUpConfirmationSuccess
		 */
		onSignUpConfirmationSuccess: function(oController, oData, oPiggyBack) {
			oController.recruitingSignUp.close();
			oController.recruitingSignUp.destroy();
			sap.ui.getCore().setModel(null, "layoutAccess");
			oController.recruitingSignUp.setBusy(false);
			/*
			  Navigate to Solutions Page and display message
			*/

			oController.getRouter().navTo("service");

			sap.ui.getCore().getEventBus().publish("displayRecruitingMessage", "displayRecruitingMessage", oPiggyBack);
		},

		/**
		 * Function that handles the error case of the ajax call for the sign up confirmation
		 * @function onSignUpConfirmationError
		 */
		onSignUpConfirmationError: function(oController, oError, oPiggyBack) {
			oController.recruitingSignUp.close();
			oController.recruitingSignUp.destroy();
			sap.ui.getCore().setModel(null, "layoutAccess");
			oController.recruitingSignUp.setBusy(false);

			setTimeout(function() {
				_oController.DataLossWarning =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", _oController);
				//Set Warning Buttons
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.onFreeTrialSubscribeErrorClose);
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWcloseTXT"));
				sap.ui.getCore().byId("button_Dialog_Warning_No").setVisible(false);
				//Set Warning Message
				sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWCaseSubmitedError"));
				//Set Warnig Dialog Title
				sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWCaseSubmitedErrorTitle"));
				sap.ui.getCore().byId("icon_Dialog_Warning").setSrc("sap-icon://customfont/error");
				sap.ui.getCore().byId("icon_Dialog_Warning").removeStyleClass("warningMessageDialogIcon");
				sap.ui.getCore().byId("icon_Dialog_Warning").addStyleClass("errorMessageDialogIcon");
				_oController.DataLossWarning.open();
			}, 0);

		},

		/**
		 * Confirmation of a subscription plan and navigation to solutions page
		 * @function signUpConfirmation
		 */
		signUpConfirmation: function() {

			this.recruitingSignUp.setBusyIndicatorDelay(0);
			this.recruitingSignUp.setBusy(true);

			var recruitingSubscriptionID = sap.ui.getCore().byId("subscribeFor").getSelectedKey();
			var jobBoard = sap.ui.getCore().byId("jobBoard").getSelectedKey();
			var phone = sap.ui.getCore().byId("phone").getValue();

			var body = {
				isTrial: "F",
				recruitingSubscriptionID: recruitingSubscriptionID,
				isJobBoard: jobBoard,
				phone: phone
			};

			var oPiggyBack = {
				"isJobBoard": jobBoard
			};

			var bodyJsonString = JSON.stringify(body);
			var xsURL = "/recruiting/accountSubscriptions";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "POST", _oController.onSignUpConfirmationSuccess, _oController.onSignUpConfirmationError,
				bodyJsonString, oPiggyBack);
		},

		/**
		 * Close subscription plan popup
		 * @function signUpClose
		 */
		signUpClose: function() {
			if (this.recruitingSignUp) {
				this.recruitingSignUp.close();
				this.recruitingSignUp.destroy();
			}
		},

		/**
		 * Selection of a 365 day subscription plan, enables the jobBoard 
		 * @function signUpClose
		 */
		onSelectSubscription: function() {
			var plan = sap.ui.getCore().byId("subscribeFor").getSelectedItem().getText();

			if (plan.indexOf(this.getView().getModel("i18n").getResourceBundle().getText("RSUDText2_365")) !== -1) {
				sap.ui.getCore().byId("jobBoardNo").setText(
					this.getView().getModel("i18n").getResourceBundle().getText("RSUDText2_No"));
				sap.ui.getCore().byId("jobBoard").setSelectedKey("");
				sap.ui.getCore().byId("jobBoard").setEnabled(true);

			} else {
				sap.ui.getCore().byId("jobBoardNo").setText(
					this.getView().getModel("i18n").getResourceBundle().getText("RSUDText2_Placeholder"));
				sap.ui.getCore().byId("jobBoard").setSelectedKey("F");
				sap.ui.getCore().byId("jobBoard").setEnabled(false);
			}
			_oController.validateSignUp();
		},

		/**
		 * Selection of the subscription plan and job board, enables the confirm button
		 * @function signUpClose
		 */
		validateJobBoard: function() {
			var selectedPlan = sap.ui.getCore().byId("subscribeFor").getSelectedItem();
			var plan;
			var isJobBoard = sap.ui.getCore().byId("jobBoard").getSelectedKey();

			if (selectedPlan != undefined) {
				plan = selectedPlan.getText();
			} else {
				return false;
			}

			if (plan.indexOf(this.getView().getModel("i18n").getResourceBundle().getText("RSUDText2_365")) === -1) {
				return true;
			} else if (isJobBoard !== "") {
				return true;
			} else {
				return false;
			}
		},

		onJobBoardTrial: function() {
			sap.ui.getCore().byId("freeTrialSubscribe").setEnabled(true);
		},

		/**
		 * Function that handles the success case of the ajax call to check the recruiting page access
		 * @function onCheckRecruitingPageAccessSuccess
		 */
		onCheckRecruitingPageAccessSuccess: function(oController, oData, oPiggyBack) {
			var bAccess;
			if (oData === undefined) {
				bAccess = false;
				//not authorized
				ga('set', 'page', '/NotAuthorized');
				ga('send', 'pageview');
				oController.getRouter().navTo("notAuthorized");
			} else {
				for (var i = 0; i < oData.AccessList.length; i++) {
					if (oData.AccessList[i] === oPiggyBack.elementCode) {
						bAccess = true;
						break;
					} else {
						bAccess = false;
					}
				}
			}
			return bAccess;
		},

		/**
		 * Function that handles the error case of the ajax call to check the recruiting page access
		 * @function onCheckRecruitingPageAccessError
		 */
		onCheckRecruitingPageAccessError: function(oController, oError, oPiggyBack) {
			var bAccess;
			if (oError.status === 401) {
				//not authorized
				ga('set', 'page', '/NotAuthorized');
				ga('send', 'pageview');
				oController.getRouter().navTo("notAuthorized");
				bAccess = false;
			} else {
				ga('set', 'page', '/UnexpectedError');
				ga('send', 'pageview');
				oController.getRouter().navTo("unexpectedError");
				bAccess = false;
			}
			return bAccess;
		},

		/**
		 * Check Recruiting Page Access
		 * @function checkRecruitingPageAccess
		 * @param {string} - elementCode (e.g. MARKETING_PAGE)
		 */
		checkRecruitingPageAccess: function(elementCode) {
			var controller = this;

			var oPiggyBack = {
				"elementCode": elementCode
			};
			var xsURL = "/auth/recruitingAccessList";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			var bAccess = _oController.handleAjaxJSONCall(_oController, false, xsURL, "GET", _oController.onCheckRecruitingPageAccessSuccess,
				_oController.onCheckRecruitingPageAccessError, undefined, oPiggyBack);

			if (!bAccess) {
				ga('set', 'page', '/NotAuthorized');
				ga('send', 'pageview');
				controller.getRouter().navTo("notAuthorized");
				return false;
			} else {
				return true;
			}
		},

		/**
		 * Verifies if phone input is correct
		 * @function verifyPhoneRecruiting
		 */
		verifyPhoneRecruiting: function(event) {
			var sPhoneValue = sap.ui.getCore().byId("phone").getValue(),
				bValPhone = true,
				regex = /^[+]*[(]{0,1}[0-9]{0,4}[)]{0,1}[0-9][-\s\.0-9]*$/; //Regular expression that matches the structure of a phone number

			//Validation of the fields
			if (sPhoneValue.length !== 0) {
				bValPhone = regex.test(sap.ui.getCore().byId("phone").getValue());
			}

			if (!bValPhone) {
				sap.ui.getCore().byId("phone").setValueState(sap.ui.core.ValueState.Error);
			} else {
				sap.ui.getCore().byId("phone").setValueState(sap.ui.core.ValueState.None);
			}

			return bValPhone;
		},

		/**
		 * 
		 */
		validateSignUp: function() {
			var isPlanOK = false,
				isJobBoardOK = false,
				isPhoneOK = false;

			//Validate Plan
			if (sap.ui.getCore().byId("subscribeFor").getSelectedKey() !== "") {
				isPlanOK = true;
			}
			//ValidateBoard
			isJobBoardOK = _oController.validateJobBoard();
			// Validate Phone
			isPhoneOK = _oController.verifyPhoneRecruiting();

			//Check Validations
			if (isPlanOK && isJobBoardOK && isPhoneOK) {
				sap.ui.getCore().byId("recruitingSignUpConfirm").setEnabled(true);
			} else {
				sap.ui.getCore().byId("recruitingSignUpConfirm").setEnabled(false);
			}

		}
	});
});