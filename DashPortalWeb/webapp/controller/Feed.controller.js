sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel",
	"valvoline/dash/portal/DashPortalWeb/controls/Card"
], function (Base, JSONModel, CardControl) {
	"use strict";
	var _oController, _fstSearch = true,
		_existantCards;
	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Feed", {
		/** @module Feed */
		/**
		 * This function initializes the controller and defines the card types to be loaded.
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oController = this;
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("feed").attachPatternMatched(this._onObjectMatched, this);

		},

		/**
		 * Reset the filters
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {
			_fstSearch = true;
			_oController = this;

			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page Layout Configuration
			if (!_oController.checkDASHPageAuthorization("COMMON_PAGE")) {
				return;
			}
			_oController.headerIconAccess();

			// Reset side bar menu buttons
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "feed"
			});

			//get user info and direct account
			this.getUserInformation(_oController);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			// Loads the information for the select
			this.fillSelect();

			//Shopping Cart Values
			_oController.getShoppingCartCookie(this);
			_oController.byId("cardContainerFeed").setViewContext(_oController);

			// Load Cards
			_oController.byId("cardContainerFeed").removeEventDelegate(_oController._oScrollToLastResultPositionEventDelegate);
			_oController._oScrollToLastResultPositionEventDelegate = null;
			_oController._toggleScrollToLoad(false);
			_oController.populateCardContainer("Feed", "1", "All");
			_oController._toggleScrollToLoad(true);

			//scroll up - mobile
			setTimeout(function () {
				if ($(".sapMScrollContV")[0] !== undefined) {
					$(".sapMScrollContV")[0].scrollTop = 0;
				} else {
					return;
				}
			}, 0);

			this.checkMonthlyPaymentWarn(this);

		},

		/**
		 * Function that handles the success case of the ajax call to fill the select
		 * @function onFillSelectSuccess
		 */
		onFillSelectSuccess: function (oController, oData, oPiggyBack) {
			var oModelCardsSuggestion = new JSONModel();
			var view = oController.getView();
			var selectBox = view.byId("feedFilter");
			try {
				selectBox.getPicker().getAggregation("buttons")[0].setText(oController.getResourceBundle()
					.getText("DNCcancelTXT"));
			} catch (oError) {
				//do notting its mobile
			}
			oModelCardsSuggestion.setProperty("/CardName", oData);
			view.setModel(oModelCardsSuggestion, "filter");
			selectBox.setSelectedKey("All");
		},

		/**
		 * Function that handles the error case of the ajax call to fill the select
		 * @function onFillSelectError
		 */
		onFillSelectError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRCards"));
		},

		/**
		 * Gets the model to fill the select and fills it
		 * @function fillSelect
		 */
		fillSelect: function () {
			var browserSelectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			var controller = this;
			var xsURL = "/core/feedFilterCategories" + "?lang=" + browserSelectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onFillSelectSuccess, controller.onFillSelectError);
		},

		/**
		 * Enables/Disables scroll to load functionality for the grid and visual view
		 * @function _toggleScrollToLoad
		 * @param {boolean} bToggle whether to enable or disable scroll to load
		 */
		_toggleScrollToLoad: function (bToggle) {
			var oScrollContainer = this.byId("LazyLoadingContainer");

			// hack: we side-step all the scrolling complexity and attach growing-list-style to the ScrollEnablement callback directly
			if (!oScrollContainer._oScroller) { // not initialize yet
				return;
			}
			if (bToggle) {
				if (!this._fnInjectedScrollToLoadCallback) {
					this._fnInjectedScrollToLoadCallback = function () {
						this._loadMoreCards();
						if (this._fnScrollLoadCallback) {
							this._fnScrollLoadCallback();
						}
					}.bind(this);
				}

				// store initial callback once
				if (!this._fnScrollLoadCallback && oScrollContainer._oScroller._fnScrollLoadCallback !== this._fnInjectedScrollToLoadCallback) {
					this._fnScrollLoadCallback = oScrollContainer._oScroller._fnScrollLoadCallback;
				}
				// hook in our callback
				oScrollContainer._oScroller._fnScrollLoadCallback = this._fnInjectedScrollToLoadCallback;
			} else {
				// reset initial callback
				oScrollContainer._oScroller._fnScrollLoadCallback = this._fnScrollLoadCallback;
			}
		},

		/**
		 * Filter the card according to the filter
		 * @function onSearch
		 * @param {Object} event - Triggered when there is an action in the filter drop down
		 */
		onSearch: function (event) {
			// Checks if the timeout session
			this.checkSessionTimeout();

			var categoryId = null;
			var item = event.getParameter("selectedItem");

			if (item === null || item.getKey() === "0") {
				categoryId = "All";
				if ($(".caseCard")) {
					$(".caseCard").remove();
				}
				//GTM
				_oController.GTMDataLayer('drpdwnfilter',
					'Filter By - Feed',
					'DropDown',
					categoryId
				);
			} else {
				categoryId = item.getKey();

				//GTM
				_oController.GTMDataLayer('drpdwnfilter',
					'Filter By - Feed',
					'DropDown',
					event.getParameters().selectedItem.getText()
				);

			}
			this._toggleScrollToLoad(false);

			//filter results
			if (_fstSearch) {
				_existantCards = this.getView().byId("cardContainerFeed").getModel().getData();
				_fstSearch = false;
			}
			var filteredCardsArray = [];
			for (var i = 0; i < _existantCards.length; i++) {
				if (categoryId === "All") {
					filteredCardsArray.push(_existantCards[i]);
				} else if (_existantCards[i].cardCategoryId === categoryId) {
					filteredCardsArray.push(_existantCards[i]);
				}
			}

			_oController.onRePopulateCardContainer(filteredCardsArray, categoryId);

			this._toggleScrollToLoad(true);
		},

		onRePopulateCardContainer: function (data, categoryId) {
			var cardContainer = _oController.getView().byId("cardContainerFeed");

			//remove previous content and disable busy indicator
			cardContainer.destroyContent();

			var cardTemplate;
			var model = new sap.ui.model.json.JSONModel(data);
			cardContainer.setModel(model);
			cardContainer.setCardCategory(categoryId);
			sap.ui.getCore().setModel(model, "cards");
			var size;
			var pageSize = sap.ui.getCore().getModel("pageSize").getData().pageSize;
			// If there is Data generate the Cards if no set the "NO CARD INFORMATION."
			if (data.length > 0) {
				if (data.length > pageSize) {
					size = pageSize;
				} else {
					size = data.length;
				}
				// Run all the Data
				for (var i = 0; i < size; i++) {
					_oController.byId("CardScreen").setVisible(true);
					if (_oController.byId("NoCardsScreen")) {
						_oController.byId("NoCardsScreen").setVisible(false);
					}
					// If is mobile is the HERO card will be converted in Image card
					if (_oController.onGetDashDevice() === "NARROW") {
						if (data[i].cardTypeId === "500") {
							cardTemplate = new CardControl("", {
								CardId: data[i].UUID,
								CardTitle: data[i].title,
								CardType: 200
							});
						} else {
							cardTemplate = new CardControl("", {
								CardId: data[i].UUID,
								CardTitle: data[i].title,
								CardType: parseInt(data[i].cardTypeId, 10)
							});
						}
					} else {
						cardTemplate = new CardControl("", {
							CardId: data[i].UUID,
							CardTitle: data[i].title,
							CardType: parseInt(data[i].cardTypeId, 10)
						});
					}
					cardContainer.insertContent(cardTemplate, i);
				}
			} else {
				_oController.byId("CardScreen").setVisible(false);
				if (_oController.byId("NoCardsScreen")) {
					_oController.byId("NoCardsScreen").setVisible(true);
				}
			}
			setTimeout(function () {
				if ($(".sapMScrollContV")[0] !== undefined) {
					$(".sapMScrollContV")[0].scrollTop = 0;
				} else {
					return;
				}
			}, 0);
		},

		/**
		 * Loads the remaining cards when user reaches the bottom of the Feed page.
		 * @function  _loadMoreCards
		 */
		_loadMoreCards: function () {
			// deal with async populate cards
			if (!sap.ui.getCore().getModel("cards") || !sap.ui.getCore().getModel("cards").getData()) {
				return;
			}
			this.byId("cardContainerFeed").setBusy(true);

			var pageSize = sap.ui.getCore().getModel("pageSize").getData().pageSize;
			var oBindingInfo = this.byId("cardContainerFeed").getContent();
			var iOldLength = parseInt(oBindingInfo.length, 10);
			var iTotalLength = sap.ui.getCore().getModel("cards").getData().length;
			var cards = sap.ui.getCore().getModel("cards").getData();

			// exit condition
			if (iOldLength >= iTotalLength) {
				this._toggleScrollToLoad(false);
				this.byId("cardContainerFeed").setBusy(false);
				return;
			}
			var end = null;

			if (iOldLength + pageSize > iTotalLength) {
				end = iTotalLength;
			} else {
				end = iOldLength + pageSize;
			}
			for (var i = iOldLength; i < end; i++) {
				var cardTemplate = new CardControl("", {
					CardId: cards[i].UUID,
					CardTitle: cards[i].title,
					CardType: parseInt(cards[i].cardTypeId, 10)
				});
				this.byId("cardContainerFeed").addContent(cardTemplate);
			}

			this._oScrollToLastResultPositionEventDelegate = {
				onAfterRendering: function () {
					var OldLength = this.byId("cardContainerFeed").getContent().length;
					var oLastItemFromOldLength = this.byId("cardContainerFeed").getContent()[OldLength - 1];
					if (oLastItemFromOldLength) {
						if (oLastItemFromOldLength.$().scrollIntoView) {
							oLastItemFromOldLength.$().scrollIntoView(false);
						} else {
							this.byId("LazyLoadingContainer").scrollToElement(oLastItemFromOldLength);
						}
					} else {
						return;
					}
					this.byId("cardContainerFeed").removeEventDelegate(this._oScrollToLastResultPositionEventDelegate);

				}.bind(this)
			};
			this.byId("cardContainerFeed").addEventDelegate(this._oScrollToLastResultPositionEventDelegate);
			this.byId("cardContainerFeed").setBusy(false);
		}
	});
});