sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel",
	'sap/ui/Device'
], function (Base, JSONModel, Device) {
	"use strict";
	var _oRouter, _oView, _oUser, _oController, _aLocationPagesDesktop, _begin_Active, _accTablePaginationSize, _oModelAllAccounts,
		_oModelAllAccountsFiltered, _createSelectionRange = true,
		_resultsList = [1, 5, 10, 50, 100, 200];
	var DASH_MOBILE = "DASH_MOBILE";

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.MyAccount.MyAccountProfile", {
		/** @module MyAccountProfile */
		/**
		 * This function initializes the controller and sets the Account model
		 * @function onInit
		 */

		onInit: function () {
			Base.prototype.onInit.call(this);
			_oRouter = this.getRouter();
			_oRouter.getRoute("myAccountProfile").attachPatternMatched(this._onObjectMatched, this);
			_oView = this.getView();
			_oController = this;
			_aLocationPagesDesktop = [];

			// Register an event handler to changes of the screen size
			sap.ui.Device.media.attachHandler(_oController.resizeToMobile, this, DASH_MOBILE);
		},

		onExit: function () {
			// Detach function from event Handler
			sap.ui.Device.media.detachHandler(_oController.resizeToMobile, _oController, DASH_MOBILE);
		},

		_onObjectMatched: function () {
			_accTablePaginationSize = 5;
			_begin_Active = 0;

			// Clears all error message
			this.clearServerErrorMsg(this);

			//set the navbar icon as selected
			this.resetMenuButtons();

			// Clears all error message
			this.clearServerErrorMsg(this);
			//get user info and direct account
			this.getUserInformation(this);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();
			_oView.byId("myAccountOwnerInfoSection").setVisible(false);
			_oView.byId("accountsTableContainer").setBusy(true);
			_oView.byId("accountLocResultsPerPage").setVisible(false);
			_oController.byId("cardContainerMyAccount").setViewContext(_oController);
			//Load Navigation Cards
			_oController.populateCardContainer("My Account", "9");
			// Get the User Accounts
			this.getAccounts(this, this.populateStoresTable);

			// PageLayout configuration
			this.headerIconAccess();

			// Populate Stores Carousel
			//this.populateStoresTable();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			this.checkMonthlyPaymentWarn(this);

			_oView.byId("searchFieldAccounts").setValue("");
		},

		/**
		 * Redirects the carrousel for the selected accounts page.
		 * @function  _onObjectMatched
		 */
		onCarouselPageChanged: function (oEvent) {
			var sOldPageId = oEvent.getParameter("oldActivePageId");
			var sNewPageId = oEvent.getParameter("newActivePageId");
			var iOldPageIndex, iNewPageIndex;
			var carouselDataModel = this.getView().getModel("carouselDataModel");
			iOldPageIndex = sOldPageId.split('---')[1];
			iNewPageIndex = sNewPageId.split('---')[1];
			var variation = iOldPageIndex - iNewPageIndex;
			if (variation === 1 || variation <= -2) {
				//previous
				if (carouselDataModel.getProperty("/currentPage") === 1) {
					carouselDataModel.setProperty("/prevPage", carouselDataModel.getProperty("/currentPage"));
					carouselDataModel.setProperty("/currentPage", carouselDataModel.getProperty("/totalPages"));
				} else {
					carouselDataModel.setProperty("/prevPage", carouselDataModel.getProperty("/currentPage"));
					carouselDataModel.setProperty("/currentPage", carouselDataModel.getProperty("/currentPage") - 1);
				}
			} else if (variation === -1 || variation >= 2) {
				//next
				if (carouselDataModel.getProperty("/currentPage") === carouselDataModel.getProperty("/totalPages")) {
					carouselDataModel.setProperty("/prevPage", carouselDataModel.getProperty("/currentPage"));
					carouselDataModel.setProperty("/currentPage", 1);
				} else {
					carouselDataModel.setProperty("/prevPage", carouselDataModel.getProperty("/currentPage"));
					carouselDataModel.setProperty("/currentPage", carouselDataModel.getProperty("/currentPage") + 1);
				}
			}
			carouselDataModel.updateBindings();

			if (_oController.onGetDashDevice() === "NARROW") {
				oEvent.getSource().getPages()[oEvent.getSource()._getPageNumber(oEvent.getSource().getActivePage())].removeAllItems();
				oEvent.getSource().getPages()[oEvent.getSource()._getPageNumber(oEvent.getSource().getActivePage())].addItem(
					_aLocationPagesDesktop[carouselDataModel.getProperty("/currentPage") - 1]);
			} else {
				if (oEvent.getSource().getPages().length !== carouselDataModel.getProperty("/totalPages")) {

					oEvent.getSource().getPages()[2].removeAllItems();
					oEvent.getSource().getPages()[2].addItem(_aLocationPagesDesktop[2]);
					oEvent.getSource().getPages()[2].sId = "myLocationsPage---" + 3;
					_oView.setBusyIndicatorDelay(0).setBusy(true);
					setTimeout(function () {
						for (var i = 4; i <= _aLocationPagesDesktop.length; i++) {
							_oView.byId("storesCarousel").addPage(new sap.m.VBox({
								id: "myLocationsPage---" + i,
								width: "100%",
								height: "auto"
							}).addStyleClass("carouselPageContainer carouselPageContainerProfileMobile"));
							_oView.byId("storesCarousel").getPages()[i - 1].addItem(_aLocationPagesDesktop[i - 1]);

							if (_oView.getModel("carouselDataModel").getProperty("/currentPage") === _oView.getModel("carouselDataModel").getProperty(
									"/totalPages")) {
								_oView.byId("storesCarousel").setActivePage("myLocationsPage---" + _oView.getModel("carouselDataModel").getProperty(
									"/totalPages"));
							}
						}
						_oView.setBusy(false);
					}, 0);
				}
			}
		},

		/**
		 *  Open pop-up to change password.
		 * @function changePassword
		 */
		changePassword: function (type) {
			// Checks Session time out
			_oController.checkSessionTimeout();
			// IF THE DIALOG IS CLOSED BY PRESSING "ESCAPE"
			if (_oController.dialogFragment) {
				_oController.dialogFragment.destroyContent();
			}
			_oController.dialogFragment = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.ChangePasswordPopUp",
				_oController);
			_oController.getResourceBundle();

			var i18nModel = new sap.ui.model.resource.ResourceModel({
				bundleUrl: "i18n/i18n.properties"
			});
			_oController.dialogFragment.setModel(i18nModel, "i18n");
			_oController.dialogFragment.open();

			//GTM
			_oController.GTMDataLayer('passwrdevent',
				'My Account Page',
				'Password',
				"Change Password -linkclick"
			);
		},
		/**
		 * Join the Curent Page with the total of pages
		 * @function setLocationsPages
		 * @param {string} - CurentPage
		 * @param {string} - TotalPages
		 */
		setLocationsPages: function (curentPage, totalPages) {
			return curentPage + " / " + totalPages;
		},
		/**
		 * Function that handles the success case of the ajax call to submit the new password
		 * @function onSubmitnewPasswordSuccess
		 */
		onSubmitnewPasswordSuccess: function (oController, oData, oPiggyBack) {
			var dialog = sap.ui.getCore().byId("changePassword");
			dialog.setBusy(false);
			oController.closeDialog();
			var type = "Success";
			var message = oController.getResourceBundle().getText("MApasswordChange");
			oController.onShowMessage(oController, type, null, message);

			//GTM
			_oController.GTMDataLayer('passwrdevent',
				'My Account Page',
				'Password',
				"Changed Password - Success"
			);
		},

		/**
		 * Function that handles the error case of the ajax call to submit the new password
		 * @function onSubmitnewPasswordError
		 */
		onSubmitnewPasswordError: function (oController, oError, oPiggyBack) {
			var dialog = sap.ui.getCore().byId("changePassword");
			oController.clearServerErrorMsgStrip(oController);
			var message = oError.responseJSON.fault.detail.longDescription.split(" , ");
			var i;

			if (oError.responseJSON !== undefined && oError.responseJSON.fault.detail.message !== null && oError.responseJSON.fault.detail.longDescription !==
				null) {
				for (i = 0; i < message.length; i++) {
					if (message[i].length > 4) {
						oController.onShowMessageStrip(message[i],
							"Error");
					}
				}
			} else {
				oController.onShowMessageStrip(oError.status,
					"Error");

			}
			dialog.setBusy(false);

		},

		/**
		 * Submit new password.
		 * @function submitnewPassword
		 */
		submitnewPassword: function () {
			// Checks Session time out
			this.checkSessionTimeout();
			var dialog = sap.ui.getCore().byId("changePassword");
			dialog.setBusyIndicatorDelay(0);
			dialog.setBusy(true);

			var controller = this;

			// Send all the Parameters
			var newPassword = {};
			//check what type of change request it is
			newPassword.oldPassword = sap.ui.getCore().byId("oldPassword").getValue();
			newPassword.newPassword = sap.ui.getCore().byId("newPassword").getValue();

			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();

			var pwJsonString = JSON.stringify(newPassword);
			var xsURL = "/core/changePassword" + "?lang=" + selectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "PUT", controller.onSubmitnewPasswordSuccess, controller.onSubmitnewPasswordError,
				pwJsonString);
		},

		/**
		 * Verify input fields during live change on submitnewPassword submission.
		 * @function verifyInput
		 * @param {Object} event - triggered on live change of input fields on submitnewPassword dialog
		 */
		verifySubmit: function () {

			var confirmPassword = sap.ui.getCore().byId("confirmPassword");
			sap.ui.getCore().byId("submitnewPassword").setBusy(false);

			if (!sap.ui.getCore().byId("oldPassword").getValue() || !sap.ui.getCore().byId("newPassword").getValue() || !sap.ui.getCore().byId(
					"confirmPassword").getValue()) {
				sap.ui.getCore().byId("submitnewPassword").setEnabled(false);
			} else if (sap.ui.getCore().byId("confirmPassword").getValue() !== sap.ui.getCore().byId("newPassword").getValue() || sap.ui.getCore()
				.byId("newPassword").getValue() !== sap.ui.getCore().byId("confirmPassword").getValue()) {
				sap.ui.getCore().byId("submitnewPassword").setEnabled(false);
				confirmPassword.setValueState("Error");
				confirmPassword.addStyleClass("errorInputField");
			} else {
				confirmPassword.removeStyleClass("errorInputField");
				sap.ui.getCore().byId("submitnewPassword").setEnabled(true);
				confirmPassword.setValueState("None");
			}

		},

		/**
		 * Navigates to Privacy Policy Change Password.
		 * @function privacyPolicyPassword
		 */
		privacyPolicyPassword: function () {
			window.open("https://www.valvoline.com/dash/password-policy", "_blank");

			//GTM
			_oController.GTMDataLayer('passwrdevent',
				'My Account Page',
				'Password',
				"Password Policy -click");
		},

		/**
		 * Redirects to toRequest function in base controller
		 * @function toRequestChangeAccount
		 */
		toRequestChangeAccount: function () {
			//GTM
			_oController.GTMDataLayer('reqchangeclck',
				'My Account Page',
				'Request Change-click',
				'Top'
			);

			this.toRequest("changeAccount");
		},

		/**
		 * Redirects to toRequest function in base controller
		 * @function toRequestChangeStoreList
		 */
		toRequestChangeStoreList: function () {
			//GTM
			_oController.GTMDataLayer('reqchangeclck',
				'My Account Page',
				'Request Change-click',
				'Bottom'
			);

			this.toRequest("changeStoreList");
		},

		/**
		 * Open an email window with the account email on mail to field
		 * @function mailTo
		 */
		mailTo: function (oEvent) {
			//send mail to contact
			if (oEvent.getSource().getText()) {
				window.open("mailto:" + oEvent.getSource().getText(), "_parent");
			}
		},

		/**
		 * Opens Dialog to request price list
		 * @function toPriceList
		 */
		toPriceList: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			//GTM
			_oController.GTMDataLayer('myacntcrdclck',
				'My Account Page',
				'Price List-Click',
				'#/MyAccount/PriceList'
			);

			// IF THE DIALOG IS CLOSED BY PRESSING "ESCAPE"
			if (this.dialogFragment) {
				this.dialogFragment.destroyContent();
			}
			this.dialogFragment = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.PriceList", this);
			this.dialogFragment.open();
			var shipToModel = sap.ui.getCore().getModel("oAccounts").getData().accounts;
			var model = new JSONModel(shipToModel);
			sap.ui.getCore().byId("shipToPriceList").setModel(model, "oAccounts");
			sap.ui.getCore().byId("shipToPriceList").setFilterSecondaryValues(true);
			var today = new Date();

			var priceDatePicker = sap.ui.getCore().byId("PriceDate");
			priceDatePicker.setMinDate(new Date(today.toDateString()));
			priceDatePicker.addEventDelegate({
				onAfterRendering: function () {
					var oDateInner = this.$().find('.sapMInputBaseInner');
					var oID = oDateInner[0].id;
					$('#' + oID).attr("disabled", "disabled");
				}
			}, priceDatePicker);

			this.getResourceBundle();

			var i18nModel = new sap.ui.model.resource.ResourceModel({
				bundleUrl: "i18n/i18n.properties"
			});
			this.dialogFragment.setModel(i18nModel, "i18n");
		},

		/**
		 * On Check input
		 *
		 */
		onCheckInput: function () {
			var date = sap.ui.getCore().byId("PriceDate").getValue();
			var accountNumber = sap.ui.getCore().byId("shipToPriceList").getSelectedKey();
			var address;
			if (sap.ui.getCore().byId("shipToPriceList").getSelectedItem()) {
				address = sap.ui.getCore().byId("shipToPriceList").getSelectedItem().getProperty("additionalText");
			} else {
				address = "";
			}
			sap.ui.getCore().byId("adressPriceList").setText(address);

			var dateOk;
			var accountNumberOk;
			if (date) {
				dateOk = true;
			} else {
				dateOk = false;
			}

			if (accountNumber) {
				accountNumberOk = true;
			} else {
				accountNumberOk = false;
			}

			if (dateOk && accountNumberOk) {
				sap.ui.getCore().byId("sendRequestPriceList").setEnabled(true);
			} else {
				sap.ui.getCore().byId("sendRequestPriceList").setEnabled(false);
			}

		},

		/**
		 * Function that handles the success case of the ajax call that handles the price list request
		 * @function onSendRequestPriceListSuccess
		 */
		onSendRequestPriceListSuccess: function (oController, oData, oPiggyBack) {
			var busyDialog = sap.ui.getCore().byId("priceList_Dialog");
			busyDialog.setBusy(false);
			oController.closeDialog();
			// oController.priceListMessageBox();
			var type = "Success";
			var message = oController.getResourceBundle().getText("MSGBoxPriceListTxt");
			oController.onShowMessage(oController, type, null, message);
		},

		/**
		 * Function that handles the error case of the ajax call that handles the price list request
		 * @function onSendRequestPriceListError
		 */
		onSendRequestPriceListError: function (oController, oError, oPiggyBack) {
			var busyDialog = sap.ui.getCore().byId("priceList_Dialog");
			oController.onShowMessageStrip(oController.getResourceBundle().getText("DPErrorInvalidDate"),
				"Error");
			busyDialog.setBusy(false);

		},

		sendRequestPriceList: function () {
			var controller = this;
			var userInfo = sap.ui.getCore().getModel("oAccounts").getData();
			var userEmail = userInfo.email;
			var accountNumber = sap.ui.getCore().byId("shipToPriceList").getSelectedKey();
			var date = sap.ui.getCore().byId("PriceDate").getValue();
			var isDateValid = new Date(date);

			var busyDialog = sap.ui.getCore().byId("priceList_Dialog");
			busyDialog.setBusyIndicatorDelay(0);
			busyDialog.setBusy(true);

			if (isDateValid.toString() !== "Invalid Date") {

				var body = {
					accountNumber: accountNumber,
					priceDate: date,
					email: userEmail
				};

				var bodyJsonString = JSON.stringify(body);
				var xsURL = "/erp/priceList";
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				controller.handleAjaxJSONCall(controller, true, xsURL, "POST", controller.onSendRequestPriceListSuccess, controller.onSendRequestPriceListError,
					bodyJsonString);
			} else {
				controller.onShowMessageStrip(controller.getResourceBundle().getText("DPErrorInvalidDate"),
					"Error");
			}

			//GTM
			_oController.GTMDataLayer('pricesendevnt',
				'Price List',
				'Send',
				sap.ui.getCore().getModel("oAccounts").getData().uuid
			);
		},

		/**
		 *  Close and destroy the dialog.
		 * @function closeDialog
		 */
		closeDialog: function () {
			this.dialogFragment.close();
			this.dialogFragment.destroy();
		},

		/**
		 * Populates the stores' table
		 * @function populateStoresTable
		 */
		populateStoresTable: function () {
			_oUser = sap.ui.getCore().getModel("oAccounts").getData();
			var oAccounts = _oUser.accounts;

			_oModelAllAccounts = new JSONModel(oAccounts);
			_oModelAllAccountsFiltered = new JSONModel(oAccounts);

			//Runs resizer to load page content according to the device size.
			_oController.resizeToMobile(sap.ui.Device.media.getCurrentRange(DASH_MOBILE));

			// Validate Owner Details
			_oController.validateOwnerDetails();
			//used to let the page render the locations before setting it to false
			setTimeout(function () {
				_oView.byId("accountsTableContainer").setBusy(false);
			}, 0);
		},

		/**
		 * Sets the layout of the arrows for the table and controls the date to be loaded in the table
		 * @function tablePagination
		 */
		accTablePagination: function (beginCounter) {
			var oDataTablePaginated = [];
			var oModelTablePaginated = new sap.ui.model.json.JSONModel();
			var oModelaccountsTable = null;

			oModelaccountsTable = _oModelAllAccountsFiltered;

			// Set up the arrows of table
			var oAccTable = _oView.byId("UserManagemnentTable");
			var navigationArrows = this.byId("navigationArrows");
			var rightNavigationArrow = this.byId("idRightNav");
			var leftNavigationArrow = this.byId("idLeftNav");
			var textCounterNavigation = this.byId("idTextCount");

			var iLengthActive = 0;

			if (oModelaccountsTable && oModelaccountsTable.getData()) {
				iLengthActive = oModelaccountsTable.getData().length;
			}

			var end = beginCounter + _accTablePaginationSize;

			// Set visible or not the table arrows if there is no active items to show in table
			if (iLengthActive === 0) {
				navigationArrows.setVisible(false);
				_oController.getView().byId("accountLocResultsPerPage").setVisible(false);
			} else {
				navigationArrows.setVisible(true);
				if (_oView.byId("accountLocResults1").getVisible()) {
					_oController.getView().byId("accountLocResultsPerPage").setVisible(iLengthActive > 1);
				} else if (_oView.byId("accountLocResults5").getVisible()) {
					_oController.getView().byId("accountLocResultsPerPage").setVisible(iLengthActive > 5);
				}
			}

			if (_createSelectionRange) {
				_oController.getView().byId("accountLocResultsPerPage").setVisible(iLengthActive > _accTablePaginationSize);
				_oController.getView().byId("accountLocResults50").setVisible(iLengthActive > 10);
				_oController.getView().byId("accountLocResults100").setVisible(iLengthActive > 50);
				_oController.getView().byId("accountLocResults200").setVisible(iLengthActive > 100);
			}
			_oController.removeSearchResultsStyleClass("accountLocResults", _resultsList, _oView);
			_oController.getView().byId("accountLocResults" + _accTablePaginationSize).addStyleClass("numberOfResultsLinkSelected");

			if (beginCounter < _accTablePaginationSize) {
				leftNavigationArrow.setEnabled(false);
			} else {
				leftNavigationArrow.setEnabled(true);
			}

			if (end >= iLengthActive) {
				end = iLengthActive;
				rightNavigationArrow.setEnabled(false);
			} else {
				rightNavigationArrow.setEnabled(true);
			}

			if (iLengthActive < _accTablePaginationSize) {
				end = iLengthActive;
			}

			if (_accTablePaginationSize === 1 && iLengthActive !== 0) {
				textCounterNavigation.setText(beginCounter + 1 + " " + _oController.getResourceBundle().getText("of") + " " +
					iLengthActive);
			} else if (iLengthActive !== 0) {
				textCounterNavigation.setText(beginCounter + 1 + "-" + end + " " + _oController.getResourceBundle().getText("of") + " " +
					iLengthActive);
			}

			for (var i = beginCounter; i < end; i++) {
				if (oModelaccountsTable) {
					oDataTablePaginated.push(oModelaccountsTable.getProperty("/" + i));
				}
			}
			oModelTablePaginated.setData(oDataTablePaginated);
			if (iLengthActive > 100) {
				oModelTablePaginated.setSizeLimit(200);
			}
			oAccTable.setModel(oModelTablePaginated);
			_oController.getView().setModel(oModelTablePaginated, "modelLocations");
		},

		/**
		 * Loads more items into the table
		 * @function toRight
		 */
		toRight: function (oEvent) {
			_begin_Active = _begin_Active + _accTablePaginationSize;
			this.accTablePagination(_begin_Active);
		},

		/**
		 * Loads more items into the table
		 * @function toLeft
		 */
		toLeft: function (oEvent) {
			_begin_Active = _begin_Active - _accTablePaginationSize;
			this.accTablePagination(_begin_Active);
		},

		resizeToMobile: function (mParams) {
			if (mParams.name === "mobile") {
				_begin_Active = 0;
				_accTablePaginationSize = 1;
				_createSelectionRange = true;
				_oView.byId("accountLocResults1").setVisible(true);
				_oView.byId("accountLocResults5").setVisible(false);
				this.accTablePagination(_begin_Active);
			} else {
				_begin_Active = 0;
				_accTablePaginationSize = 5;
				_createSelectionRange = true;
				_oView.byId("accountLocResults1").setVisible(false);
				_oView.byId("accountLocResults5").setVisible(true);
				this.accTablePagination(_begin_Active);
			}
		},

		sapAccountNameVisibilityFormatter: function (accountName) {
			return (accountName !== undefined && accountName !== "");
		},

		/**
		 * JSONFilter function that receives the oData and filters
		 * @ parameter (filterValue, oModel)
		 */
		JSONFilter: function (filterValue, oModel) {
			// DataJSON original and filtered
			var oDataJSON = oModel.getData();
			var oDataJSONFiltered = [];
			var sLCFilterValue = filterValue.toLowerCase();

			for (var i = 0; i < oDataJSON.length; i++) {
				// JSON Parameters to be filtered
				var accountname = "",
					sapaccountname2 = "",
					accountnum = "",
					billingstreet = "",
					billingcity = "",
					billingstate = "",
					billingpostalcode = "";

				if (oDataJSON[i].accountname) {
					accountname = oDataJSON[i].accountname.toLowerCase();
				}
				if (oDataJSON[i].sapaccountname2) {
					sapaccountname2 = oDataJSON[i].sapaccountname2.toLowerCase();
				}
				if (oDataJSON[i].accountnum) {
					accountnum = oDataJSON[i].accountnum.toLowerCase();
				}
				if (oDataJSON[i].billingstreet) {
					billingstreet = oDataJSON[i].billingstreet.toLowerCase();
				}
				if (oDataJSON[i].billingcity) {
					billingcity = oDataJSON[i].billingcity.toLowerCase();
				}
				if (oDataJSON[i].billingstate) {
					billingstate = oDataJSON[i].billingstate.toLowerCase();
				}
				if (oDataJSON[i].billingpostalcode) {
					billingpostalcode = oDataJSON[i].billingpostalcode.toLowerCase();
				}

				if (accountname.indexOf(sLCFilterValue) !== -1 ||
					sapaccountname2.indexOf(sLCFilterValue) !== -1 ||
					accountnum.indexOf(sLCFilterValue) !== -1 ||
					billingstreet.indexOf(sLCFilterValue) !== -1 ||
					billingcity.indexOf(sLCFilterValue) !== -1 ||
					billingstate.indexOf(sLCFilterValue) !== -1 ||
					billingpostalcode.indexOf(sLCFilterValue) !== -1) {
					oDataJSONFiltered.push(oDataJSON[i]);
				}
			}
			return oDataJSONFiltered;
		},

		/**
		 * Filters the table
		 * @function onLiveSearchUserManagement
		 * @param {Object} oEvent - Search field input
		 */
		onLiveSearchAccountTable: function (oEvent) {
			// Get value of search field
			var sNewValue = oEvent.getParameter("newValue");

			if (!sNewValue) {
				// Resets model
				_oModelAllAccountsFiltered.setData(_oModelAllAccounts.getData());
			} else {
				// Filtering the oModel
				_oModelAllAccountsFiltered.setData(this.JSONFilter(sNewValue, _oModelAllAccounts));
			}

			// Start all the table at page 0
			_begin_Active = 0;
			_createSelectionRange = false;
			this.accTablePagination(_begin_Active);
		},

		/**
		 * Opens the Price List message box
		 * @function
		 */
		priceListMessageBox: function () {
			sap.m.MessageBox.show(
				this.getResourceBundle().getText("MSGBoxPriceListTxt"), {
					icon: sap.m.MessageBox.Icon.INFORMATION,
					title: this.getResourceBundle().getText("MSGBoxPriceListTitle"),
					actions: [sap.m.MessageBox.Action.CLOSE]
				}
			);
		},

		toPasswordPolicy: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();
			var sURL = this.getRouter().getURL("productvps");
			sap.m.URLHelper.redirect("#" + sURL, true);
		},
		/**
		 * Validate Owner Details.
		 * @function validateOwnerDetails
		 */
		validateOwnerDetails: function () {
			var oOwnerInfoModel = new sap.ui.model.json.JSONModel();
			var bDisplayOwnerDetails = false;
			var oOwnerInfo = {};
			var oDirectAccount = sap.ui.getCore().getModel("oDirectAccount");
			if (oDirectAccount && oDirectAccount.getProperty("/ownername") && oDirectAccount.getProperty("/ownersEmail")) {
				oDirectAccount = oDirectAccount.getData();
				bDisplayOwnerDetails = true;

				var oExclusionList = _oController.getDomainsByName("TBMExclusion");
				if (oExclusionList && oExclusionList.length) {
					//check if the ownername exists in the exclusion list 
					for (var i = 0; i < oExclusionList.length; i++) {
						if (oExclusionList[i].description.toLowerCase() === oDirectAccount.ownername.toLowerCase()) {
							bDisplayOwnerDetails = false;
						}
					}
				}
			}
			if (bDisplayOwnerDetails) {
				_oView.byId("myAccountOwnerInfoSection").setVisible(true);
				oOwnerInfo.ownername = oDirectAccount.ownername;
				oOwnerInfo.ownersEmail = oDirectAccount.ownersEmail;
				if (oDirectAccount.ownersMobilePhone) {
					oOwnerInfo.ownersPhone = oDirectAccount.ownersMobilePhone;
				} else if (oDirectAccount.ownersPhone) {
					oOwnerInfo.ownersPhone = oDirectAccount.ownersPhone;
				} else {
					oOwnerInfo.ownersPhone = "";
				}
			} else {
				_oView.byId("myAccountOwnerInfoSection").setVisible(false);
			}
			oOwnerInfoModel.setData(oOwnerInfo);
			_oView.setModel(oOwnerInfoModel, "oOwnerModel");
		},

		/**
		 *  Select the number of results displayed on a page
		 *  @selectResultsPerPage
		 */
		selectResultsPerPage: function (oEvent) {
			var sValue = oEvent.getSource().getText();
			_accTablePaginationSize = parseInt(sValue, 10);
			_createSelectionRange = false;
			_begin_Active = 0;
			_oController.accTablePagination(_begin_Active);
		},

		/**
		 * Check if table have Data
		 * @function formatterNoLocationsVisiblity
		 **/
		formatterNoLocationsVisiblity: function (model) {
			if (Object.keys(model).length === 0) {
				_oController.getView().byId("UserManagemnentTable").addStyleClass("fix-border-myaccount");
				return true;
			}
			return false;
		},
		/**
		 * Triggers the GTM tags, also handles the price list exception
		 * @function gtmHandling
		 * @param {String} callToActionURL - Route/link or fragment
		 **/
		gtmHandling: function (oEvent) {
			var callToActionURL = oEvent.getParameter("callToActionURL");
			var sEventAction = _oController.getKey("keys_gtm", "keyMyAccP_" + callToActionURL.replace(/-|\//g, "") + "_eventAction");
			if (callToActionURL === "priceList") {
				_oController.toPriceList();
			} else if (sEventAction !== undefined) {
				var sRouteEndPoint = _oRouter.getRoute(callToActionURL).getPattern();
				if (callToActionURL === "myAccountCases") {
					sRouteEndPoint = sRouteEndPoint.replace("{caseId}", "0");
				}
				var sEvent = _oController.getKey("keys_gtm", "keyMyAccP_cardClick_event");
				var sEventCategory = _oController.getKey("keys_gtm", "keyMyAccP_cardClick_eventCategory");
				if (sEvent && sEventCategory && sEventAction && sRouteEndPoint) {
					//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
					_oController.GTMDataLayer(sEvent,
						sEventCategory,
						sEventAction,
						"#/" + sRouteEndPoint
					);
				}
			}
		}
	});
});