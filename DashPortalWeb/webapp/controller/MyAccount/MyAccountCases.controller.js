sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"valvoline/dash/portal/DashPortalWeb/controls/CustomPanelCases",
	"sap/ui/model/json/JSONModel"
], function (Base, CustomPanelCases, JSONModel) {
	"use strict";
	var _oView, _oController, _oRouter, _oContact, _oAccount, _gtmLastearch, _firstRender;
	var _iCaseListPaginationSize = 5;
	var _selectedActvResult = 5,
		_selectedCompResult = 5,
		_createSelectionRange = true;
	//Models Active Cases
	var _iBeginActive, _oDataActiveCases, _oDataActiveCasesFiltered, _currentActiveSort = "both";
	//Models Active Cases
	var _iBeginCompleted, _oDataCompletedCases, _oDataCompletedCasesFiltered, _currentCompletedSort = "both";
	var _resultsList = [5, 10, 50, 100, 200];

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.MyAccount.MyAccountCases", {
		/** @module MyAccountCases */
		/**
		 * This function initializes the controller and sets the selectedAccount
		 * model.
		 * 
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oRouter = this.getRouter();
			_oRouter.getRoute("myAccountCases").attachPatternMatched(this._onObjectMatched, this);
			_oController = this;
			_oView = _oController.getView();
			_firstRender = true;
			sap.ui.Device.media.attachHandler(_oController.resizeScreen, this, "DASH_MOBILE");
		},

		/**
		 * Runs gtm search configurations after first render of the page
		 * model.
		 * 
		 * @function onAfterRendering
		 */
		onAfterRendering: function () {
			if (_firstRender) {
				_oController.inputSearchCasesFunctions();
				_firstRender = false;
			}
		},

		/**
		 * Preforms the configuration of the page and reloads the models on entering the page
		 * 
		 * @function _onObjectMatched
		 * @param {Object}
		 *            event - Triggered when navigating to myAccountCases page
		 */
		_onObjectMatched: function (Event) {
			_iBeginActive = _iBeginCompleted = 0;

			_oController.resizeScreen(sap.ui.Device.media.getCurrentRange("DASH_MOBILE"));

			_oController.clearSorting();
			_oController.getView().byId("searchFieldCases").setValue("");

			var oModel = {};
			oModel.reloadInc = 0;
			_oView.setModel(new JSONModel(oModel), "accountsByDistrictRefreshTracker");

			// Clears all error message
			this.clearServerErrorMsg(this);

			// set the navbar icon as selected
			this.resetMenuButtons();

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("CASES_PAGE")) {
				return;
			}
			this.headerIconAccess();

			//get user info and direct account
			this.getUserInformation(this, this.toRunAfterGettingUserInfo);

			// check Language and set selected Key in combo box
			this.onChangeLanguage();

			// Sets the model oAccountsByDistrict
			this.getAccountsByDistrict(this, "comboBoxAccountCases", false, this.accountsLoaded);

			//Set the case containers to busy
			this.getView().byId("activeCasesContainer").setBusyIndicatorDelay(0).setBusy(true);
			this.getView().byId("completedCasesContainer").setBusyIndicatorDelay(0).setBusy(true);
			this.getView().byId("comboBoxAccountCases").setEnabled(false);
			this.getView().byId("searchFieldCases").setEnabled(false);

			// Shopping Cart Values
			this.getShoppingCartCookie(this);

			var promiseGetActiveCases, promiseGetCompletedCases;

			promiseGetActiveCases = new Promise(function (resolve, reject) {
				_oController.getActiveCases(null, null, null, resolve);
			});

			promiseGetCompletedCases = new Promise(function (resolve, reject) {
				_oController.getCompletedCases(null, null, null, resolve);
			});

			Promise.all([promiseGetActiveCases, promiseGetCompletedCases]).then(function () {
				_oController.getView().byId("comboBoxAccountCases").setEnabled(true);
				_oController.getView().byId("searchFieldCases").setEnabled(true);
			});

		},

		/**
		 * Increments view model with global vars to trigger view formatters
		 * 
		 * @function accountsLoaded
		 */
		accountsLoaded: function () {
			var oModel = _oView.getModel("accountsByDistrictRefreshTracker");
			oModel.getData().reloadInc = oModel.getData().reloadInc + 1;
			_oView.setModel(oModel, "accountsByDistrictRefreshTracker");
			_oController.reconfigAddressLabel();
		},

		/**
		 * This function handles the logic that requires the user information to be available.
		 * @function toRunAfterGettingUserInfo
		 * @param {object} controller - the page context
		 */
		toRunAfterGettingUserInfo: function (controller) {
			_oContact = sap.ui.getCore().getModel("oAccounts").getData();
			if (_oContact.accounts.length > 0) {
				var oAccount = new sap.ui.model.json.JSONModel(_oContact.accounts[0]);
				sap.ui.getCore().setModel(oAccount, "oSelectedAccount");
				_oView.setModel(oAccount, "oSelectedAccount");
			}
		},

		/**
		 * This function resets the AddressLabel when you navigate to the page.
		 * @function reconfigAddressLabel
		 */
		reconfigAddressLabel: function () {
			_oController.getView().byId("comboBoxAccountCases").setSelectedKey(0);
			_oAccount = _oController.findAccountByDistrict("0", _oController);
			sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oController.getView().byId("accountInfoContainerDesktop").setVisible(false);
		},

		/**
		 * On location change, redefine the cases to loaded.
		 * 
		 * @function onChangeAccountComboBox
		 * @param {object} oEvent - Event that trigered the function
		 */
		onChangeAccountComboBox: function (oEvent) {
			// Checks Session time out
			_oController.checkSessionTimeout();

			_oController.getView().byId("comboBoxAccountCases").setEnabled(false);
			_oController.getView().byId("searchFieldCases").setEnabled(false);
			_oController.getView().byId("activeCasesList").setNoDataText(_oController.getResourceBundle().getText("MACactiveCaseNoDataText"));
			_oController.getView().byId("completedCasesList").setNoDataText(_oController.getResourceBundle().getText("MACclosedCaseNoDataText"));
			_oController.clearSorting();

			_oController.getView().byId("searchFieldCases").setValue("");
			if (oEvent !== "" && oEvent.getParameter("newValue") === "") {
				_oView.byId("comboBoxAccountCases").setSelectedKey("0");
			}
			var selectedKey = _oController.byId("comboBoxAccountCases").getSelectedKey();
			var districtFlag;
			var accountID;
			if (selectedKey.indexOf("district") === -1) {
				_oAccount = _oController.findAccountByDistrict(selectedKey, _oController);
				if (_oAccount === undefined) {
					_oView.byId("comboBoxAccountCases").setSelectedKey("0");
					_oAccount = _oController.findAccountByDistrict("0", _oController);
				}
				if (_oAccount.id === "0") {
					_oView.byId("comboBoxAccountCases").setSelectedKey("0");
					_oView.byId("accountAddress1Mobile").setVisible(false);
				} else {
					_oView.byId("accountAddress1Mobile").setVisible(true);
				}
				sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
				_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
				accountID = sap.ui.getCore().getModel("oSelectedAccount").getData().id;
				districtFlag = "0";
			} else if (selectedKey.indexOf("district") !== -1) {
				_oAccount = [];
				_oAccount.id = "district-" + oEvent.getParameters().value;
				_oAccount.billingcity = _oController.getResourceBundle().getText("MAresultsTXT");
				sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
				_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
				accountID = "0";
				districtFlag = oEvent.getParameters().value;
			} else {
				return;
			}
			if (_oController.getView().byId("comboBoxAccountCases").getSelectedKey() === "0" || _oController.getView().byId(
					"comboBoxAccountCases").getSelectedKey()
				.indexOf("district") !== -1) {
				_oController.getView().byId("accountInfoContainerDesktop").setVisible(false);
			} else {
				_oController.getView().byId("accountInfoContainerDesktop").setVisible(true);
			}

			var promiseGetActiveCases, promiseGetCompletedCases;

			//Setup active cases			
			_selectedActvResult = 5;
			promiseGetActiveCases = new Promise(function (resolve, reject) {
				_oController.getActiveCases(accountID, districtFlag, null, resolve);
			});

			//Setup completed cases
			_selectedCompResult = 5;
			promiseGetCompletedCases = new Promise(function (resolve, reject) {
				_oController.getCompletedCases(accountID, districtFlag, null, resolve);
			});

			Promise.all([promiseGetActiveCases, promiseGetCompletedCases]).then(function () {
				_oController.getView().byId("comboBoxAccountCases").setEnabled(true);
				_oController.getView().byId("searchFieldCases").setEnabled(true);
			});

			if (oEvent && oEvent.getParameters() && oEvent.getParameters().newValue) {
				//GTM
				if (_oAccount.billingstreet && _oAccount.billingcity && _oAccount.billingstate && _oAccount.billingpostalcode) {
					_oController.GTMDataLayer('drpdwnfilter',
						'Cases',
						'DropDown',
						oEvent.getParameters().newValue.replace(/\s/g, '') +
						" - " + _oAccount.billingstreet + " " + _oAccount.billingcity + ", " + _oAccount.billingstate + " " + _oAccount.billingpostalcode
					);
				} else {
					_oController.GTMDataLayer('drpdwnfilter',
						'Cases',
						'DropDown',
						oEvent.getParameters().newValue.replace(/^\s+/g, '')
					);
				}
			}

		},

		/**
		 * Activates or deactivates busy of contact info based on the parameter.
		 * @function invalidateCaseAccountInfo
		 */
		invalidateCaseAccountInfo: function (bIsBusy) {
			var aFieldList = $(".contactInfo").control();
			var iLength = aFieldList.length;
			var i;

			for (i = 0; i < iLength; i++) {
				aFieldList[i].setBusy(bIsBusy);
			}
		},

		/**
		 * Get active cases for the logged account
		 * 
		 * @function getActiveCases
		 * @param {string}
		 *            idAccount user's account ID or district flag
		 */
		getActiveCases: function (sIdAccount, SDistrictFlag, bIsReload, promiseResolve) {
			var xsURL;
			if (sIdAccount !== "0" && SDistrictFlag === "0") {
				xsURL = "/salesforce/activeCases?accountId=" + sIdAccount;
			} else if (sIdAccount === "0" && SDistrictFlag !== "0") {
				xsURL = "/salesforce/activeCases?customerreportinggroup=" + SDistrictFlag;
			} else {
				xsURL = "/salesforce/activeCases";
			}
			this.getView().byId("activeCasesContainer").setBusyIndicatorDelay(0).setBusy(true);
			this.getView().byId("actvCasesResultsPerPage").setVisible(false);

			var controller = this;
			_createSelectionRange = true;
			_selectedActvResult = 5;

			var oPiggyBack = {};
			oPiggyBack.isReload = bIsReload;
			oPiggyBack.promiseResolve = promiseResolve;

			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onGetActiveCasesSuccess, controller.onGetActiveCasesError,
				undefined, oPiggyBack);
		},

		/**
		 * Function that handles the success case of the ajax call that gets the active cases
		 * @function ongGetActiveCasesSuccess
		 */
		onGetActiveCasesSuccess: function (oController, oData, oPiggyBack) {
			if (oData === undefined) {
				_oDataActiveCases = [];
				_oDataActiveCasesFiltered = [];
			} else {
				_oDataActiveCases = oData;
				_oDataActiveCasesFiltered = oData;
			}

			_iBeginActive = 0;

			oController.casesListPagination(true);

			setTimeout(function () {
				oController.getView().byId("activeCasesContainer").setBusy(false);
			}, 0);

			if (oPiggyBack.isReload) {
				var sValue = oController.getView().byId("searchFieldCases").getValue();
				var mParameters = {};
				mParameters.newValue = sValue;
				mParameters.isReload = true;
				_oController.getView().byId("searchFieldCases").fireLiveChange(mParameters);
			}

			/**
			 * When loading both Active and Completed Cases we need to resolve the promise.
			 * When loading only Active Cases we can enable these fields immediately.
			 */
			if (oPiggyBack.promiseResolve) {
				oPiggyBack.promiseResolve();
			} else {
				oController.getView().byId("comboBoxAccountCases").setEnabled(true);
				oController.getView().byId("searchFieldCases").setEnabled(true);
			}
		},

		/**
		 * Function that handles the error case of the ajax call that gets the active cases
		 * @function onGetActiveCasesError
		 */
		onGetActiveCasesError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRActiveCases"));
			oController.getView().byId("activeCasesContainer").setBusy(false);
			oController.getView().byId("comboBoxAccountCases").setEnabled(true);
			oController.getView().byId("searchFieldCases").setEnabled(true);

			_oDataActiveCases = [];
			_oDataActiveCasesFiltered = [];

			_iBeginActive = 0;

			oController.casesListPagination(true);
			_oController.getView().byId("activeNavigatorLabel").setBusy(false);
		},

		/**
		 * Get active cases for the logged account
		 * 
		 * @function getCompletedCases
		 * @param {string}
		 *            idContact - user's contact ID
		 * @param {string}
		 *            idAccount user's account ID
		 */
		getCompletedCases: function (idAccount, districtFlag, bIsReload, promiseResolve) {
			var xsURL;

			this.getView().byId("completedCasesContainer").setBusyIndicatorDelay(0).setBusy(true);
			this.getView().byId("compCasesResultsPerPage").setVisible(false);

			if (idAccount !== "0" && districtFlag === "0") {
				xsURL = "/salesforce/completedCases?accountId=" + idAccount;
			} else if (idAccount === "0" && districtFlag !== "0") {
				xsURL = "/salesforce/completedCases?customerreportinggroup=" + districtFlag;
			} else {
				xsURL = "/salesforce/completedCases";
			}

			var oPiggyBack = {};
			oPiggyBack.isReload = bIsReload;
			oPiggyBack.promiseResolve = promiseResolve;

			var controller = this;
			_createSelectionRange = true;
			_selectedCompResult = 5;

			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(_oController, true, xsURL, "GET", controller.onGetCompletedCasesSuccess, controller.onGetCompletedCasesError,
				undefined, oPiggyBack);
		},

		/**
		 * Function that handles the success case of the ajax call to get the completed Cases
		 * @function onGetCompletedCasesSuccess
		 */
		onGetCompletedCasesSuccess: function (oController, oData, oPiggyBack) {
			if (oData === undefined) {
				_oDataCompletedCases = [];
				_oDataCompletedCasesFiltered = [];
			} else {
				_oDataCompletedCases = oData;
				_oDataCompletedCasesFiltered = oData;
			}

			_iBeginCompleted = 0;

			oController.casesListPagination(false);

			oController.getView().byId("completedCasesContainer").setBusy(false);

			if (oPiggyBack.isReload) {
				var sValue = oController.getView().byId("searchFieldCases").getValue();
				var mParameters = {};
				mParameters.newValue = sValue;
				mParameters.isReload = true;
				_oController.getView().byId("searchFieldCases").fireLiveChange(mParameters);
			}

			/**
			 * When loading both Active and Completed Cases we need to resolve the promise.
			 * When loading only Completed Cases we can enable these fields immediately.
			 */
			if (oPiggyBack.promiseResolve) {
				oPiggyBack.promiseResolve();
			} else {
				oController.getView().byId("comboBoxAccountCases").setEnabled(true);
				oController.getView().byId("searchFieldCases").setEnabled(true);
			}
		},

		/**
		 * Function that handles the error case of the ajax call to get the completed Cases
		 * @function onGetCompletedCasesError
		 */
		onGetCompletedCasesError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRCompletedCases"));
			oController.getView().byId("completedCasesContainer").setBusy(false);
			oController.getView().byId("comboBoxAccountCases").setEnabled(true);
			oController.getView().byId("searchFieldCases").setEnabled(true);

			_oDataCompletedCases = [];
			_oDataCompletedCasesFiltered = [];

			_iBeginCompleted = 0;

			oController.casesListPagination(_iBeginCompleted, false);
			_oController.getView().byId("completedNavigatorLabel").setBusy(false);

		},

		/**
		 * Sets the layout of the arrows for the table and controls the date to be loaded in the List
		 * @function casesListPagination
		 */
		casesListPagination: function (bIsActiveCases) {
			var oDataListPaginated = [];
			var oModelListPaginated = new sap.ui.model.json.JSONModel();
			var oDataCasesList = null;
			var navigationArrows;
			var rightNavigationArrow;
			var leftNavigationArrow;
			var textCounterNavigation;
			var iLengthActive = 0;
			var iBeginCounter, end;
			var resultsPagination, resultlinkSelected, resultlink50, resultlink100, resultlink200;

			if (bIsActiveCases) {
				iBeginCounter = _iBeginActive;
				if (_selectedActvResult > 100) {
					oModelListPaginated.setSizeLimit(_selectedActvResult);
				}
				_oView.setModel(oModelListPaginated, "oActiveCasesToShow");
			} else {
				iBeginCounter = _iBeginCompleted;
				if (_selectedCompResult > 100) {
					oModelListPaginated.setSizeLimit(_selectedCompResult);
				}
				_oView.setModel(oModelListPaginated, "oCompletedCasesToShow");
			}

			//Setup vars based on target list
			if (bIsActiveCases) {
				oDataCasesList = _oDataActiveCasesFiltered;
				navigationArrows = _oView.byId("paginationNavigatorActive");
				rightNavigationArrow = _oView.byId("toRightActive");
				leftNavigationArrow = _oView.byId("toLeftActive");
				textCounterNavigation = _oView.byId("activeNavigatorLabel");
				resultsPagination = _oView.byId("actvCasesResultsPerPage");
				resultlink50 = _oView.byId("actvCasesResults50");
				resultlink100 = _oView.byId("actvCasesResults100");
				resultlink200 = _oView.byId("actvCasesResults200");
				end = iBeginCounter + _selectedActvResult;
				_iCaseListPaginationSize = _selectedActvResult;
				resultlinkSelected = _oView.byId("actvCasesResults" + _iCaseListPaginationSize);
				_oController.removeSearchResultsStyleClass("actvCasesResults", _resultsList, _oView);
			} else {
				oDataCasesList = _oDataCompletedCasesFiltered;
				navigationArrows = _oView.byId("paginationNavigatorCompleted");
				rightNavigationArrow = _oView.byId("toRightCompleted");
				leftNavigationArrow = _oView.byId("toLeftCompleted");
				textCounterNavigation = _oView.byId("completedNavigatorLabel");
				resultsPagination = _oView.byId("compCasesResultsPerPage");
				resultlink50 = _oView.byId("compCasesResults50");
				resultlink100 = _oView.byId("compCasesResults100");
				resultlink200 = _oView.byId("compCasesResults200");
				end = iBeginCounter + _selectedCompResult;
				_iCaseListPaginationSize = _selectedCompResult;
				resultlinkSelected = _oView.byId("compCasesResults" + _iCaseListPaginationSize);
				_oController.removeSearchResultsStyleClass("compCasesResults", _resultsList, _oView);
			}

			//Setup length of the List
			if (oDataCasesList) {
				iLengthActive = oDataCasesList.length;
			}

			// Set visible or not the table arrows if there is no active items to show in table
			if (iLengthActive === 0) {
				navigationArrows.setVisible(false);
			} else {
				navigationArrows.setVisible(true);
			}
			resultsPagination.setVisible(iLengthActive > 5);
			if (_createSelectionRange) {
				resultlink50.setVisible(iLengthActive > 10);
				resultlink100.setVisible(iLengthActive > 50);
				resultlink200.setVisible(iLengthActive > 100);
			}

			resultlinkSelected.addStyleClass("numberOfResultsLinkSelected");

			if (iBeginCounter < _iCaseListPaginationSize) {
				leftNavigationArrow.setEnabled(false);
			} else {
				leftNavigationArrow.setEnabled(true);
			}

			if (end >= iLengthActive) {
				end = iLengthActive;
				rightNavigationArrow.setEnabled(false);
			} else {
				rightNavigationArrow.setEnabled(true);
			}

			if (iLengthActive < _iCaseListPaginationSize) {
				end = iLengthActive;
			}

			if (_iCaseListPaginationSize === 1 && iLengthActive !== 0) {
				textCounterNavigation.setText(iBeginCounter + 1 + " " + _oController.getResourceBundle().getText("MAofTXT") + " " +
					iLengthActive);
			} else if (iLengthActive !== 0) {
				textCounterNavigation.setText(iBeginCounter + 1 + "-" + end + " " + _oController.getResourceBundle().getText("MAofTXT") + " " +
					iLengthActive);
			}

			textCounterNavigation.setBusy(false);

			for (var i = iBeginCounter; i < end; i++) {
				if (oDataCasesList) {
					oDataListPaginated.push(oDataCasesList[i]);
				}
			}
			oModelListPaginated.setData(oDataListPaginated);
			if (bIsActiveCases) {
				_oView.getModel("oActiveCasesToShow").setData(oModelListPaginated.getData());
			} else {
				_oView.getModel("oCompletedCasesToShow").setData(oModelListPaginated.getData());
			}

		},

		/**
		 * Sets the search result messages after page reload
		 * @function setSearchResultMessage
		 */

		setSearchResultMessage: function () {
			if (_oController.getView().byId("searchFieldCases").getValue().length > 0) {
				if (_oDataActiveCases.length > 0 && _oDataActiveCasesFiltered.length === 0) {
					_oView.byId("activeCasesList").setNoDataText(_oController.getResourceBundle().getText("MACsearchNoDataText"));
				} else {
					_oView.byId("activeCasesList").setNoDataText(_oController.getResourceBundle().getText("MACactiveCaseNoDataText"));
				}
				if (_oDataCompletedCases.length > 0 && _oDataCompletedCasesFiltered.length === 0) {
					_oView.byId("completedCasesList").setNoDataText(_oController.getResourceBundle().getText("MACsearchNoDataText"));
				} else {
					_oView.byId("completedCasesList").setNoDataText(_oController.getResourceBundle().getText("MACclosedCaseNoDataText"));
				}
			}
		},

		/**
		 * Loads new page into the table when clik on Active cases List
		 * @function toRight
		 */
		onActiveChangePage: function (oEvent) {
			var sDirection = oEvent.getSource().data("direction");
			var sPage;
			_iCaseListPaginationSize = _selectedActvResult;
			if (sDirection === "left") {
				_iBeginActive = _iBeginActive - _iCaseListPaginationSize;
			} else {
				_iBeginActive = _iBeginActive + _iCaseListPaginationSize;
			}
			this.casesListPagination(true);
			sPage = (_iBeginActive + 1) + "-" + (_iBeginActive + _iCaseListPaginationSize);

			//GTM
			_oController.GTMDataLayer('paginationclick',
				'Support Requests',
				'Cases Pagination click - ' + 'ACTIVE',
				sPage
			);

			var elmnt;
			elmnt = _oController.getView().byId("activeCases");
			if (sap.ui.Device.system.phone === true) {
				setTimeout(function () {
					var b = $("#" + _oView.byId("createNewCaseButton").getId());
					b[0].scrollIntoView({
						behavior: "smooth", // or "auto" or "instant"
						block: "start"
					});
				}, 0);
			} else if (sap.ui.Device.browser.internet_explorer === true || sap.ui.Device.system.tablet === true) {
				setTimeout(function () {
					var b = $("#" + _oView.byId("activeCases").getId());
					b[0].scrollIntoView({
						behavior: "smooth", // or "auto" or "instant"
						block: "start"
					});
				}, 0);
			} else {
				_oController.getScrollElement().element.scrollToElement(elmnt);
			}

		},

		/**
		 * Loads new page into the table when clik on Completed cases List
		 * @function toRight
		 */
		onCompletedChangePage: function (oEvent) {
			var sDirection = oEvent.getSource().data("direction");
			var sPage;
			_iCaseListPaginationSize = _selectedCompResult;
			if (sDirection === "left") {
				_iBeginCompleted = _iBeginCompleted - _iCaseListPaginationSize;
			} else {
				_iBeginCompleted = _iBeginCompleted + _iCaseListPaginationSize;
			}
			this.casesListPagination(false);
			sPage = (_iBeginCompleted + 1) + "-" + (_iBeginCompleted + _iCaseListPaginationSize);

			//GTM
			_oController.GTMDataLayer('paginationclick',
				'Support Requests',
				'Cases Pagination click - ' + 'Completed',
				sPage
			);

			var elmnt;
			elmnt = _oController.getView().byId("completedCases");
			if (sap.ui.Device.system.phone === true) {
				setTimeout(function () {
					var b = $("#" + _oView.byId("actvOrdersResults200").getId());
					b[0].scrollIntoView({
						behavior: "smooth", // or "auto" or "instant"
						block: "start"
					});
				}, 0);
			} else if (sap.ui.Device.browser.internet_explorer === true || sap.ui.Device.system.tablet === true) {
				setTimeout(function () {
					var b = $("#" + _oView.byId("completedCases").getId());
					b[0].scrollIntoView({
						behavior: "smooth", // or "auto" or "instant"
						block: "start"
					});
				}, 0);
			} else {
				_oController.getScrollElement().element.scrollToElement(elmnt);
			}

		},

		/*********************************************************************************************/
		/*                                   Email Functions                                        */
		/*********************************************************************************************/

		/**
		 * Opens new email window with "mail to:" field filled with the selected
		 * account's email on click of ACTIVE cases
		 * 
		 * @function onEmailAccountActiveCasesPress
		 */
		onEmailAccountActiveCasesPress: function (oEvent) {
			var email = oEvent.getSource().getText();
			if (email !== "" && email !== null && email !== undefined) {
				window.open("mailto:" + email);
			}
			//GTM
			_oController.GTMDataLayer('caseEmaillnkclick',
				'Support Requests',
				'Email Contact',
				'Active ' + 'email link -click'
			);
		},

		/**
		 * Opens new email window with "mail to:" field filled with the selected
		 * account's email on click of COMPLETED cases
		 * 
		 * @function onEmailAccountCompletedCasesPress
		 */
		onEmailAccountCompletedCasesPress: function (oEvent) {
			var email = oEvent.getSource().getText();
			if (email !== "" && email !== null && email !== undefined) {
				window.open("mailto:" + email);
			}
			//GTM
			_oController.GTMDataLayer('caseEmaillnkclick',
				'Support Requests',
				'Email Contact',
				'Completed ' + 'email link -click'
			);
		},

		/*********************************************************************************************/
		/*                                   Search Functions                                        */
		/*********************************************************************************************/
		/**
		 * Filters the case models with the value of the provided event
		 * 
		 * @function emailAccountCases
		 */
		onLiveSearchCases: function (oEvent) {
			// Get value of search field
			var sNewValue = oEvent.getParameter("newValue");
			var bIsReload = oEvent.getParameter("isReload");
			var oController = this,
				oView = this.getView();

			if (!sNewValue) {
				// Resets model
				_oDataActiveCasesFiltered = _oDataActiveCases;
				_oDataCompletedCasesFiltered = _oDataCompletedCases;
			} else {
				// Filtering the oModel
				_oDataActiveCasesFiltered = this.dataFilter(sNewValue, _oDataActiveCases);
				_oDataCompletedCasesFiltered = this.dataFilter(sNewValue, _oDataCompletedCases);
				if (_oDataActiveCases.length > 0 && _oDataActiveCasesFiltered.length === 0) {
					oView.byId("activeCasesList").setNoDataText(oController.getResourceBundle().getText("MACsearchNoDataText"));
				} else {
					oView.byId("activeCasesList").setNoDataText(oController.getResourceBundle().getText("MACactiveCaseNoDataText"));
				}
				if (_oDataCompletedCases.length > 0 && _oDataCompletedCasesFiltered.length === 0) {
					oView.byId("completedCasesList").setNoDataText(oController.getResourceBundle().getText("MACsearchNoDataText"));
				} else {
					oView.byId("completedCasesList").setNoDataText(oController.getResourceBundle().getText("MACclosedCaseNoDataText"));
				}
				//GTM Var
				_gtmLastearch = sNewValue;
			}

			//Sorting the model
			_oDataActiveCasesFiltered = this.sortCasesData(_oDataActiveCasesFiltered, "active", _currentActiveSort);
			_oDataCompletedCasesFiltered = this.sortCasesData(_oDataCompletedCasesFiltered, "completed", _currentCompletedSort);

			if (!bIsReload) {
				// Start lists in page 1
				_iBeginActive = 0;
				_iBeginCompleted = 0;
			}
			_createSelectionRange = false;

			this.casesListPagination(true);
			this.casesListPagination(false);
		},

		/**
		 * JSONFilter function that receives the oData and filters
		 * 
		 * @param {string} filterValue - Value that will be used to filter the model
		 * @param {object} oModel - Model containing the values to be filtered
		 * @return {object} Model to each the filter was already applied
		 */
		dataFilter: function (filterValue, oModel) {
			// DataJSON original and filtered
			var oData = oModel;
			var oDataFiltered = [];
			var sCaseNumber, sDescription, sSubject, sSuppliedname, oAccountName, sAccountName;
			var i;
			for (i = 0; i < oData.length; i++) {
				// JSON Parameters to be filtered
				sCaseNumber = parseInt(oData[i].casenumber, 10).toString();
				sDescription = oData[i].description && oData[i].description.toLowerCase();
				sSubject = oData[i].subject && oData[i].subject.toLowerCase();
				sSuppliedname = oData[i].suppliedname && oData[i].suppliedname.toLowerCase();

				if (sCaseNumber.indexOf(filterValue.toLowerCase()) !== -1) {
					oDataFiltered.push(oData[i]);
				} else if (sDescription && (sDescription.indexOf(filterValue.toLowerCase()) !== -1)) {
					oDataFiltered.push(oData[i]);
				} else if (sSubject && (sSubject.indexOf(filterValue.toLowerCase()) !== -1)) {
					oDataFiltered.push(oData[i]);
				} else if (sSuppliedname && (sSuppliedname.indexOf(filterValue.toLowerCase()) !== -1)) {
					oDataFiltered.push(oData[i]);
				} else {
					oAccountName = this.findAccountByDistrict(oData[i].accountid);
					sAccountName = oAccountName && oAccountName.accountname && oAccountName.accountname.toLowerCase();
					if (sAccountName && (sAccountName.indexOf(filterValue.toLowerCase()) !== -1)) {
						oDataFiltered.push(oData[i]);
					}

				}
			}
			return oDataFiltered;
		},

		/**
		 * Sets an event listener for the focusout event on the search bar to handle the gtm tag when
		 * the user clicks outside of the search bar
		 * 
		 * @function inputSearchCasesFunctions
		 */
		inputSearchCasesFunctions: function () {
			var inputSearchCases = this.getView().byId("searchFieldCases").getDomRef();
			inputSearchCases.addEventListener("focusout", function () {
				_oController.gtmInputSearchCasesFunctions("handleLiveSearch");
			});
		},

		/**
		 * Triggers the gtm tag when the user clears the search bar
		 * 
		 * @function handleSearch
		 */
		handleSearch: function (oEvent) {
			if (oEvent.getParameters().clearButtonPressed === true) {
				_oController.gtmInputSearchCasesFunctions("handleSearchClear");
			}
		},

		/**
		 * Triggers the gtm tag based on the handler that called this function
		 * 
		 * @function gtmInputSearchCasesFunctions
		 */
		gtmInputSearchCasesFunctions: function (sCaller) {
			if (sCaller !== "handleSearchClear") {
				var sSearch = _oController.getView().byId("searchFieldCases").getValue();

				if (sSearch === "" || sSearch === null || sSearch === undefined) {
					return;
				}
			} else {
				sSearch = _gtmLastearch;
			}

			//GTM return search Keyword
			_oController.GTMDataLayer('casesrch',
				'Support Requests',
				'Search Requests',
				sSearch
			);

		},

		/*********************************************************************************************/
		/*                                   Sort Functions                                          */
		/*********************************************************************************************/
		/**
		 * Calls the sorting function for the models and uptdates pagination
		 * 
		 * @function handleSearch
		 */
		onSingleSort: function (oEvent) {
			var bIsActive = (oEvent.getSource().data("active") === "true");
			var sOrder;
			var sType;

			if (oEvent.getSource().getSrc().indexOf("sort_desc.png") !== -1) {
				sOrder = "asc";
			} else {
				sOrder = "desc";
			}

			if (bIsActive && _oDataActiveCasesFiltered.length > 0) {
				_iBeginActive = 0;
				_currentActiveSort = sOrder;
				_oDataActiveCasesFiltered = this.sortCasesData(_oDataActiveCasesFiltered, "active", _currentActiveSort);
				_oController.getView().byId("sortButton_ActiveCases").setSrc("resources/icon/sort_" + sOrder + ".png");
				_oController.casesListPagination(true);
				sType = "Active";
			} else if (!bIsActive && _oDataCompletedCasesFiltered.length > 0) {
				_iBeginCompleted = 0;
				_currentCompletedSort = sOrder;
				_oDataCompletedCasesFiltered = this.sortCasesData(_oDataCompletedCasesFiltered, "completed", _currentCompletedSort);
				_oController.getView().byId("sortButton_CompletedCases").setSrc("resources/icon/sort_" + sOrder + ".png");
				_oController.casesListPagination(false);
				sType = "Completed";
			}

			if (sType) {
				//GTM sort by Descending Cases Active
				var sOrderFullName;
				if (sOrder === "asc") {
					sOrderFullName = "Descending";
				} else {
					sOrderFullName = "Ascending";
				}

				_oController.GTMDataLayer('casesort',
					'Cases',
					'Case Sorting | ' + sType,
					sOrderFullName
				);
			}
		},

		/**
		 * Sorts the provided search model
		 * 
		 * @function sortCasesData
		 */
		sortCasesData: function (oModel, sType, sOrder) {
			var objectName;

			if (sType === "active") {
				objectName = "createddate";
			} else {
				objectName = "closeddate";
			}

			oModel.sort(function (a, b) {
				a = new Date(a.valueOf()[objectName]);
				b = new Date(b.valueOf()[objectName]);
				switch (sOrder) {
				case "asc":
					if (isNaN(a.getTime()) && !isNaN(b.getTime())) {
						return 1;
					} else if (!isNaN(a.getTime()) && isNaN(b.getTime())) {
						return -1;
					}
					if (a < b) {
						return -1;
					} else if (a > b) {
						return 1;
					} else {
						return 0;
					}
				case "desc":
					if (isNaN(a.getTime()) && !isNaN(b.getTime())) {
						return 1;
					} else if (!isNaN(a.getTime()) && isNaN(b.getTime())) {
						return -1;
					}
					if (a < b) {
						return 1;
					} else if (a > b) {
						return -1;
					} else {
						return 0;
					}
				default:
					return 0;
				}
			});
			return oModel;
		},

		/**
		 * Clears the sorting arrows and vars
		 * 
		 * @function clearSorting
		 */
		clearSorting: function () {
			_oController.clearSortingActive();
			_oController.clearSortingCompleted();
		},

		/**
		 * Clears the sorting arrows and vars
		 * 
		 * @function clearSorting
		 */
		clearSortingActive: function () {
			_currentActiveSort = "desc";
			_oController.getView().byId("sortButton_ActiveCases").setSrc("resources/icon/sort_desc.png");
		},

		/**
		 * Clears the sorting arrows and vars
		 * 
		 * @function clearSorting
		 */
		clearSortingCompleted: function () {
			_currentCompletedSort = "desc";
			_oController.getView().byId("sortButton_CompletedCases").setSrc("resources/icon/sort_desc.png");
		},

		/*********************************************************************************************/
		/*                                   Refreshers                                              */
		/*********************************************************************************************/

		onRefreshActive: function () {
			_oController.getView().byId("activeCasesContainer").setBusy(true);
			_oController.getView().byId("activeNavigatorLabel").setBusy(true);
			setTimeout(function () {
				_oController.onRefreshCasesList(true);
			}, 0);

			//GTMDataLayer(event, eventCategory, eventAction, eventLabel)
			_oController.GTMDataLayer('caserfrsh',
				'Cases',
				'Refresh -linkclick',
				'Active Cases'
			);
		},

		onRefreshCompleted: function () {
			this.getView().byId("completedCasesContainer").setBusy(true);
			setTimeout(function () {
				_oController.onRefreshCasesList(false);
			}, 0);

			//GTMDataLayer(event, eventCategory, eventAction, eventLabel)
			_oController.GTMDataLayer('caserfrsh',
				'Cases',
				'Refresh -linkclick',
				'Completed Cases'
			);

		},

		onRefreshCasesList: function (bIsActive) {
			// Checks Session time out
			_oController.checkSessionTimeout();

			_oController.getView().byId("comboBoxAccountCases").setEnabled(false);
			_oController.getView().byId("searchFieldCases").setEnabled(false);

			var selectedKey = _oController.byId("comboBoxAccountCases").getSelectedKey();
			var districtFlag;
			var accountID;

			if (_oAccount === undefined) {
				if (bIsActive) {
					_oController.getActiveCases();
				} else {
					_oController.getCompletedCases();
				}
				return;
			}

			if (selectedKey.indexOf("district") === -1) {
				if (_oAccount.id === "0") {
					accountID = "0";
				} else {
					accountID = _oAccount.id;
				}
				districtFlag = "0";
			} else if (selectedKey.indexOf("district") !== -1) {
				accountID = "0";
				districtFlag = _oView.byId("comboBoxAccountCases").getProperty("value");
			}
			if (bIsActive) {
				//Setup active cases
				_oController.clearSortingActive();
				_oController.getActiveCases(accountID, districtFlag, true);
			} else {
				//Setup completed cases
				_oController.clearSortingCompleted();
				_oController.getCompletedCases(accountID, districtFlag, true);
			}
		},

		/*********************************************************************************************/
		/*                                   Resizers                                                */
		/*********************************************************************************************/
		/**
		 * Updates global vars model on screen change with the new screen range
		 * 
		 * @function resizeScreen
		 */
		resizeScreen: function (mParams) {
			var oModel = _oView.getModel("pageGlobalVars");
			var oData;

			if (!oModel) {
				oData = {};
			} else {
				oData = oModel.getData();
			}

			oData.screenSize = mParams.name;
			_oView.setModel(new JSONModel(oData), "pageGlobalVars");
		},

		/*********************************************************************************************/
		/*                                   Formatters                                              */
		/*********************************************************************************************/

		/**
		 * Concatenates the case number with the subject
		 * 
		 * @function formatterActive
		 */
		formatterHeaderTitle: function (subject, casenumber, sScreenSizeName, sCaseHeader) {
			if (subject && casenumber && sScreenSizeName !== "mobile") {
				return ("(" + sCaseHeader + ": " + casenumber.replace(/^0+/, "") + ") " + subject);
			} else {
				if (casenumber) {
					return (sCaseHeader + ": " + casenumber.replace(/^0+/, ""));
				} else {
					return "";
				}
			}
		},

		/**
		 * Gets the account number of the given ID
		 * 
		 * @function formatterAccountNumber
		 */
		formatterAccountNumber: function (sId, oAccounts) {
			if (sId) {
				var oAccount = this.findAccountByDistrict(sId);
				if (oAccount && oAccount.accountnumber) {
					return parseInt(oAccount.accountnumber, 10);
				} else {
					return "";
				}
			} else {
				return "";
			}
		},

		/**
		 * Gets the account name of the given ID
		 * 
		 * @function formatterAccountName
		 */
		formatterAccountName: function (sId, oAccounts) {
			if (sId) {
				var oAccount = this.findAccountByDistrict(sId);
				if (oAccount && oAccount.accountname) {
					return oAccount.accountname;
				} else {
					return "";
				}
			} else {
				return "";
			}
		},

		/**
		 * Collapses the panel when any of the provided variables are changed.
		 * 
		 * @function formatterExpanded
		 **/
		formatterExpanded: function (var1, var2, var3) {
			return false;
		},

		/**
		 * Formatter to hide contact name when there is no contact name available. Contact name field is not hidden if
		 * the user doesn't have any other field. This is done to maintain an empty space in the field
		 * 
		 * @function formatterContactNameVisibility
		 **/
		formatterContactNameVisibility: function (sName, sPhone, sEmail) {
			var bVisible = true;

			if (sName) {
				return true;
			}
			if (sPhone) {
				bVisible = false;
			}
			if (sEmail) {
				bVisible = false;
			}
			return bVisible;
		},

		/**
		 * formater to hide contact info field if it is not available
		 * 
		 * @function formatterContactInformationVisibility
		 **/
		formatterContactInformationVisibility: function (sInfo) {
			if (sInfo) {
				return true;
			} else {
				return false;
			}
		},

		/**
		 * Formater to set an element busy if no armument is provided
		 * 
		 * @function accountInfoBusyState
		 **/
		accountInfoBusyState: function (oAccounts) {
			if (oAccounts) {
				return false;
			} else {
				return true;
			}
		},
		/**
		 *  Select the number of results displayed on a page
		 *  @selectActvResultsPerPage
		 */
		selectActvResultsPerPage: function (oEvent) {
			var sValue = oEvent.getSource().getText();
			_selectedActvResult = parseInt(sValue, 10);
			_iBeginActive = 0;
			_oController.casesListPagination(true);
		},

		/**
		 *  Select the number of results displayed on a page
		 *  @selectCompResultsPerPage
		 */
		selectCompResultsPerPage: function (oEvent) {
			var sValue = oEvent.getSource().getText();
			_selectedCompResult = parseInt(sValue, 10);
			_iBeginCompleted = 0;
			_oController.casesListPagination(false);
		}
	});
});