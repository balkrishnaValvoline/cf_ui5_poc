sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/Filter",
	"sap/ui/core/util/Export",
	"sap/ui/core/util/ExportTypeCSV"
], function (Base, JSONModel, FilterOperator, Filter, Export, ExportTypeCSV) {
	"use strict";
	var _oRouter, _oView, _oController, _oContact, _oAccount, _visitSiteURL, _bIsFirstLoad = true,
		_bHasSevData = false,
		_bHasFAData = false,
		_bAccountsLoaded = false;

	// Account Select Table Model
	var _oModelAccountSelectTable = new sap.ui.model.json.JSONModel();
	var _oModelAccountSelectSortedData = new sap.ui.model.json.JSONModel();
	var _beginAccountSelect = 0;
	var DASH_MOBILE = "DASH_MOBILE";
	var _iAccountSelectPageSize, _SelectedAccountNum, _TempSelectedAccountNum, _TempSelectedAccountNumber,
		_TempSelectedCostumerName;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.MyAccount.MyAccountOilAnalysis", {
		/** @module MyAccountOilAnalysis */
		/**
		 * This function initializes the controller and sets the selectedAccount
		 * model.
		 * 
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oRouter = this.getRouter();
			_oRouter.getRoute("myAccountOilAnalysis").attachPatternMatched(this._onObjectMatched, this);
			_oController = this;
			_oView = _oController.getView();
		},

		/**
		 * This function is called everytime the page is loaded, and refresh the data and the page opening.
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {
			// Clears all error message
			_oController.clearServerErrorMsg(_oController);
			// set the navbar icon as selected
			_oController.resetMenuButtons();

			// Page Layout Configuration
			if (!_oController.checkDASHPageAuthorization("OILANL_PAGE")) {
				return;
			}
			//get user info and direct account
			this.getUserInformation(this, this.toRunAfterGettingUserInfo);

			// Check Language and set selected Key in combo box
			_oController.onChangeLanguage();

			_oController.getAccountsByDistrict(_oController, "comboBoxAccountOilAnalysis", false, this.reconfigAddressLabel);

			_oController.headerIconAccess();

			// Shopping Cart Values
			_oController.getShoppingCartCookie(_oController);

			_oController.rebuildCharts();

			//Load Page Data
			_oController.getVisitPageURL();

			// Get the User Accounts
			var onGetAccountsSuccess = function () {
				if (sap.ui.getCore().byId("accountedit") && sap.ui.getCore().byId("accountedit").getBusy()) {
					sap.ui.getCore().byId("accountedit").setBusy(false);
				}
				_bAccountsLoaded = true;
			};
			_oController.getAccounts(_oController, onGetAccountsSuccess);
		},

		/**
		 * Set the menu as active if the app is reloaded and makes an element in the filter disabled
		 * @function onAfterRendering
		 */
		onAfterRendering: function () {
			//Remove the focus of the input
			$(':focus').blur();
		},

		/**
		 * This function handles the logic that requires the user information to be available.
		 * @function toRunAfterGettingUserInfo
		 * @param {object} controller - the page context
		 */
		toRunAfterGettingUserInfo: function (controller) {
			_oContact = sap.ui.getCore().getModel("oAccounts").getData();
			if (_oContact.accounts.length > 0) {
				var oAccount = new sap.ui.model.json.JSONModel(_oContact.accounts[0]);
				sap.ui.getCore().setModel(oAccount, "oSelectedAccount");
				_oView.setModel(oAccount, "oSelectedAccount");
			}
		},

		/**
		 * This function resets the AddressLabel when you navigate to the page.
		 * @function reconfigAddressLabel
		 */
		reconfigAddressLabel: function () {
			_oController.getView().byId("comboBoxAccountOilAnalysis").setSelectedKey(0);
			_oAccount = _oController.findAccountByDistrict("0", _oController);
			sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oController.getView().byId("accountInfoContainerDesktop").setVisible(false);
			_oController.onChangeAccountComboBox("");
		},

		/**
		 * This function gets the account id and loads all the graphs.
		 * @function onChangeAccountComboBox
		 */
		onChangeAccountComboBox: function (oEvent) {
			// Checks Session time out
			_oController.checkSessionTimeout();

			if (oEvent !== "" && oEvent.getParameter("newValue") === "") {
				_oView.byId("comboBoxAccountOilAnalysis").setSelectedKey("0");
			}
			var selectedKey = _oController.byId("comboBoxAccountOilAnalysis").getSelectedKey();
			var districtFlag;
			var accountNum;
			if (selectedKey.indexOf("district") === -1) {
				_oAccount = this.findAccountByDistrict(selectedKey, _oController);
				if (_oAccount === undefined) {
					_oView.byId("comboBoxAccountOilAnalysis").setSelectedKey("0");
					_oAccount = this.findAccountByDistrict("0", _oController);
				}
				if (_oAccount.id === "0") {
					_oView.byId("comboBoxAccountOilAnalysis").setSelectedKey("0");
					_oView.byId("accountAddress1Mobile").setVisible(false);
					accountNum = "0";
				} else {
					_oView.byId("accountAddress1Mobile").setVisible(true);
					accountNum = _oAccount.accountnum;
				}
				sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
				_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
				districtFlag = "0";
			} else if (selectedKey.indexOf("district") !== -1) {
				_oAccount = [];
				_oAccount.id = "district-" + oEvent.getParameters().value;
				_oAccount.billingcity = _oController.getResourceBundle().getText("MAresultsTXT");
				sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
				_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
				accountNum = "0";
				districtFlag = oEvent.getParameters().value;
			} else {
				return;
			}
			if (_oView.byId("comboBoxAccountOilAnalysis").getSelectedKey() === "0" || _oView.byId("comboBoxAccountOilAnalysis").getSelectedKey()
				.indexOf(
					"district") !== -1) {
				_oView.byId("accountInfoContainerDesktop").setVisible(false);
			} else {
				_oView.byId("accountInfoContainerDesktop").setVisible(true);
			}

			if (_bIsFirstLoad) {
				_bIsFirstLoad = false;
			} else {
				//Setup severity
				_oController.getSeverityData(accountNum, districtFlag);
				//Setup Fluid Age
				_oController.getFluidAgeData(accountNum, districtFlag);
			}
			//If The browser is Safari the download export file does not ocour if the model is not preloaded, so a pre-download of the model is needed
			if (sap.ui.Device.browser.safari) {
				_oController.prepareExport();
			} else {
				_oView.setModel(new JSONModel({}), "exportDataModel");
			}

			if (oEvent && oEvent.getParameters().newValue) {
				//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
				_oController.GTMDataLayer('drpDwnFltroilAnalys',
					'Oil Analysis',
					'Selecting Location -Dropdown',
					oEvent.getParameters().newValue.trim()
				);
			}
		},

		/**
		 * Destroys and rebuilds all charts
		 * 
		 * @function rebuildCharts
		 */
		rebuildCharts: function () {
			var oPie12 = this.getView().byId("OilAnalysisPieChart12");
			var oPie3 = this.getView().byId("OilAnalysisPieChart3");
			var oHistoMiles = this.getView().byId("oilAnalysisChartMiles");
			var oHistoHour = this.getView().byId("oilAnalysisChartHour");
			var sFluidAgeByHour = _oView.getModel("i18n").getResourceBundle().getText("MAOAFAHoursCLM");
			var sFluidAgeByMiles = _oView.getModel("i18n").getResourceBundle().getText("MAOAFAMilesCLM");

			oPie12.removeAllFeeds();
			oPie12.destroyDataset();

			oPie3.removeAllFeeds();
			oPie3.destroyDataset();

			oHistoMiles.removeAllFeeds();
			oHistoMiles.destroyDataset();

			oHistoHour.removeAllFeeds();
			oHistoHour.destroyDataset();

			_oController.preparePieChart("OilAnalysisPieChart12");
			_oController.preparePieChart("OilAnalysisPieChart3");

			_oController.prepareColumnChart("oilAnalysisChartHour", sFluidAgeByHour);
			_oController.prepareColumnChart("oilAnalysisChartMiles", sFluidAgeByMiles);

			var selectedKey = _oController.byId("comboBoxAccountOilAnalysis").getSelectedKey();
			var districtFlag;
			var accountNum;

			if (_oAccount === undefined) {
				//Setup severity
				_oController.getSeverityData();
				//Setup Fluid Age
				_oController.getFluidAgeData();

				return;
			}

			if (selectedKey.indexOf("district") === -1) {
				if (_oAccount.id === "0") {
					accountNum = "0";
				} else {
					accountNum = _oAccount.accountnum;
				}
				districtFlag = "0";
			} else if (selectedKey.indexOf("district") !== -1) {
				accountNum = "0";
				districtFlag = _oView.byId("comboBoxAccountOilAnalysis").getProperty("value");
			}

			//Setup severity
			_oController.getSeverityData(accountNum, districtFlag);
			//Setup Fluid Age
			_oController.getFluidAgeData(accountNum, districtFlag);
		},

		/************ Data Loaders ************/
		/**
		 * Calls service to retrieve Severity data
		 * 
		 * @function getSeverityData
		 */
		getSeverityData: function (accountNum, districtFlag) {
			_oView.byId("OilAnalysisPieChart12").setBusyIndicatorDelay(0).setBusy(true);
			_oView.byId("OilAnalysisPieChart3").setBusyIndicatorDelay(0).setBusy(true);
			var selectedLanguage, selectedLocation;
			if (sap.ui.getCore().getModel("selectedLanguage") === undefined) {
				selectedLanguage = this.getSelectedLanguageForLangCode();
			} else {
				selectedLanguage = sap.ui.getCore().getModel("selectedLanguage").getData().selectedLanguage;
			}
			if (accountNum !== "0" && districtFlag === "0") {
				selectedLocation = "&accountnum=" + accountNum;
			} else if (accountNum === "0" && districtFlag !== "0") {
				selectedLocation = "&customerreportinggroup=" + districtFlag;
			} else {
				selectedLocation = "";
			}

			_bHasSevData = false;

			var xsURL = "/oilanalysis/severity?lang=" + selectedLanguage + selectedLocation;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.getSeverityDataSuccess, _oController.getSeverityDataError);
		},

		/**
		 * Runs on Severity service call success. Prepares Severity Pie charts with retrieved data
		 * and prepares model for the severity tables.
		 * 
		 * @function getSeverityDataSuccess
		 */
		getSeverityDataSuccess: function (oController, oData, oPiggyBack) {
			var i, isEmpty;
			if (oData !== undefined && oData !== null && oData.severity12Months !== undefined && oData.severity12Months.length !== 0) {
				isEmpty = true;
				for (i = 0; i < oData.severity12Months.length; i++) {
					if (oData.severity12Months[i].count !== 0) {
						isEmpty = false;
					}
				}
				if (isEmpty) {
					oController.hideOilAnalysisChart("OilAnalysisPieChart12");
				} else {
					oController.showOilAnalysisChart("OilAnalysisPieChart12", new JSONModel(oData.severity12Months));
					_bHasSevData = true;
					_oView.setModel(new JSONModel(oData.pieChart12Months), "severity12Months");
				}
			} else {
				oController.hideOilAnalysisChart("OilAnalysisPieChart12");
			}

			if (oData !== undefined && oData !== null && oData.severity3Months !== undefined && oData.severity3Months.length !== 0) {
				isEmpty = true;
				for (i = 0; i < oData.severity3Months.length; i++) {
					if (oData.severity3Months[i].count !== 0) {
						isEmpty = false;
					}
				}
				if (isEmpty) {
					oController.hideOilAnalysisChart("OilAnalysisPieChart3");
				} else {
					oController.showOilAnalysisChart("OilAnalysisPieChart3", new JSONModel(oData.severity3Months));
					_bHasSevData = true;
					_oView.setModel(new JSONModel(oData.pieChart3Months), "severity3Months");
				}
			} else {
				oController.hideOilAnalysisChart("OilAnalysisPieChart3");
			}

			oController.validateExportButton();
		},

		/**
		 *  Runs on Severity service call Error. Hides Pie charts and displays error message.
		 * 
		 * @function getSeverityDataError
		 */
		getSeverityDataError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRParameterByName"));
			oController.validateExportButton();
			oController.preparePieChart("OilAnalysisPieChart12");
			oController.preparePieChart("OilAnalysisPieChart3");
			_oView.byId("OilAnalysisPieChart12").setBusy(false);
			_oView.byId("OilAnalysisPieChart3").setBusy(false);
			_oView.byId("OilAnalysisPieChart12").setVisible(false);
			_oView.byId("OilAnalysisPieChart3").setVisible(false);
			_oView.byId("OilAnalysisPieChart12-noData").setVisible(true);
			_oView.byId("OilAnalysisPieChart3-noData").setVisible(true);

		},

		/**
		 * Loads parameter data and Shows Pie Chart width the provided ID
		 * 
		 * @function showOilAnalysisChart
		 */
		showOilAnalysisChart: function (chartID, oData) {
			_oView.byId(chartID).setModel(oData);
			_oView.byId(chartID).setBusy(false);
			_oView.byId(chartID).setVisible(true);
			_oView.byId(chartID + "-noData").setVisible(false);
		},

		/**
		 * Hides PieChart with the provided ID.
		 * 
		 * @function hideOilAnalysisChart
		 */
		hideOilAnalysisChart: function (chartID) {
			_oView.byId(chartID).setBusy(false);
			_oView.byId(chartID).setVisible(false);
			_oView.byId(chartID + "-noData").setVisible(true);
		},

		/**
		 * Shows Fluid age chart with with provided id and loads it with provided data.
		 * 
		 * @function showOilAnalysisFAChart
		 */
		showOilAnalysisFAChart: function (chartID, oData) {
			_oView.byId(chartID).setModel(oData);
			_oView.byId(chartID).setBusy(false);
			_oView.byId(chartID).setVisible(true);
		},

		/**
		 * Hides Fluid age chart with with provided id
		 * 
		 * @function hideOilAnalysisFACharts
		 */
		hideOilAnalysisFACharts: function (chartID) {
			_oView.byId(chartID).setBusy(false);
			_oView.byId(chartID).setVisible(false);
		},

		/**
		 * Calls service to retrieve Fluid Age data
		 * 
		 * @function getFluidAgeData
		 */
		getFluidAgeData: function (accountNum, districtFlag) {
			_oView.byId("oilAnalysisChartHour").setBusyIndicatorDelay(0).setBusy(true);
			_oView.byId("oilAnalysisChartMiles").setBusyIndicatorDelay(0).setBusy(true);
			var selectedLanguage, selectedLocation;
			if (sap.ui.getCore().getModel("selectedLanguage") === undefined) {
				selectedLanguage = this.getSelectedLanguageForLangCode();
			} else {
				selectedLanguage = sap.ui.getCore().getModel("selectedLanguage").getData().selectedLanguage;
			}
			if (accountNum !== "0" && districtFlag === "0") {
				selectedLocation = "&accountnum=" + accountNum;
			} else if (accountNum === "0" && districtFlag !== "0") {
				selectedLocation = "&customerreportinggroup=" + districtFlag;
			} else {
				selectedLocation = "";
			}

			_bHasFAData = false;

			var xsURL = "/oilanalysis/fluidAge?lang=" + selectedLanguage + selectedLocation;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.getFluidAgeDataSuccess, _oController.getFluidAgeDataError);
		},

		/**
		 * Runs on Fluid Age service call success. Prepares Fluid Age charts with retrieved data.
		 * 
		 * @function getFluidAgeDataSuccess
		 */
		getFluidAgeDataSuccess: function (oController, oData, oPiggyBack) {

			if (oData !== undefined && oData !== null && oData.byHours !== undefined && oData.byHours.length !== 0) {
				oController.showOilAnalysisFAChart("oilAnalysisChartHour", new JSONModel(oData.byHours));
				_bHasFAData = true;
			} else {
				oController.hideOilAnalysisFACharts("oilAnalysisChartHour");
			}

			if (oData !== undefined && oData !== null && oData.byMiles !== undefined && oData.byMiles.length !== 0) {
				oController.showOilAnalysisFAChart("oilAnalysisChartMiles", new JSONModel(oData.byMiles));
				_bHasFAData = true;
			} else {
				oController.hideOilAnalysisFACharts("oilAnalysisChartMiles");
			}

			oController.validateExportButton();

			_oView.byId("oilAnalysisChart-noData").setVisible(!_bHasFAData);
		},

		/**
		 * Runs on Fluid Age service call Error. Hides Charts and displays error message.
		 * 
		 * @function getFluidAgeDataError
		 */
		getFluidAgeDataError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRParameterByName"));
			oController.validateExportButton();
			oController.preparePieChart("oilAnalysisChartHour");
			oController.preparePieChart("oilAnalysisChartMiles");
			_oView.byId("oilAnalysisChartHour").setBusy(false);
			_oView.byId("oilAnalysisChartMiles").setBusy(false);
			_oView.byId("oilAnalysisChartHour").setVisible(false);
			_oView.byId("oilAnalysisChartMiles").setVisible(false);
			_oView.byId("oilAnalysisChart-noData").setVisible(true);
		},

		/**
		 * Calls service to retrieve Export Data.
		 * 
		 * @function getExportData
		 */
		getExportData: function (accountNum, districtFlag, resolve) {
			var selectedLanguage, selectedLocation, oPiggyBack;

			if (sap.ui.getCore().getModel("selectedLanguage") === undefined) {
				selectedLanguage = this.getSelectedLanguageForLangCode();
			} else {
				selectedLanguage = sap.ui.getCore().getModel("selectedLanguage").getData().selectedLanguage;
			}

			if (accountNum !== "0" && districtFlag === "0") {
				selectedLocation = "&accountnum=" + accountNum;
			} else if (accountNum === "0" && districtFlag !== "0") {
				selectedLocation = "&customerreportinggroup=" + districtFlag;
			} else {
				selectedLocation = "";
			}

			if (resolve !== undefined) {
				oPiggyBack = {
					"resolve": resolve
				};
			}

			var xsURL = "/oilanalysis/exportSamples?lang=" + selectedLanguage + selectedLocation;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.getExportDataSuccess, _oController.getExportDataError,
				undefined, oPiggyBack);
		},

		/**
		 * Runs on Export service call success. Runs function to initiate the download of the file.
		 * 
		 * @function getExportDataSuccess
		 */
		getExportDataSuccess: function (oController, oData, oPiggyBack) {
			var oModel = {};
			oModel.data = oData;
			_oView.setModel(new JSONModel(oModel), "exportDataModel");
			// _oController.onDataExport();
			_oView.byId("oilAnalysisExportButton").setBusy(false);
			if (oPiggyBack.resolve !== undefined) {
				oPiggyBack.resolve();
			}
		},

		/**
		 * Runs on Export service call error. Displays Error message
		 * 
		 * @function getExportDataError
		 */
		getExportDataError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRParameterByName"));
			_oView.byId("oilAnalysisExportButton").setBusy(false);
		},

		/**
		 * Enables or disables Button Based on data being available
		 * 
		 * @function validateExportButton
		 */
		validateExportButton: function () {
			var bEnableButton = (_bHasSevData || _bHasFAData);
			_oView.byId("oilAnalysisExportButton").setEnabled(bEnableButton);
		},

		/**
		 * Transforms Export data in a CSS files and initiates the download.
		 * 
		 * @function onDataExport
		 */
		onDataExport: sap.m.Table.prototype.exportData || function (oEvent) {
			var resourceBundle = _oView.getModel("i18n").getResourceBundle();
			var oModel = _oView.getModel("exportDataModel");
			var oExport;

			if (oModel !== undefined && oModel.getData().data !== undefined) {
				oExport = new Export({

					exportType: new ExportTypeCSV({
						fileExtension: "csv",
						separatorChar: ","
					}),

					models: oModel,

					rows: {
						path: "/data"
					},
					columns: [{
						name: resourceBundle.getText("MAOAUIDTXT"),
						template: {
							content: "{unitID}"
						}
					}, {
						name: resourceBundle.getText("MAOAUMakeTXT"),
						template: {
							content: "{unitMake}"
						}
					}, {
						name: resourceBundle.getText("MAOAUModelTXT"),
						template: {
							content: "{unitModel}"
						}
					}, {
						name: resourceBundle.getText("MAOAUYManTXT"),
						template: {
							content: "{unitYearOfManufacture}"
						}
					}, {
						name: resourceBundle.getText("MAOACompNameTXT"),
						template: {
							content: "{compartmentName}"
						}
					}, {
						name: resourceBundle.getText("MAOACompMakeTXT"),
						template: {
							content: "{compartmentMake}"
						}
					}, {
						name: resourceBundle.getText("MAOACompModelTXT"),
						template: {
							content: "{compartmentModel}"
						}
					}, {
						name: resourceBundle.getText("MAOAUSiteTXT"),
						template: {
							content: "{unitSite}"
						}
					}, {
						name: resourceBundle.getText("MAOACompAgeTXT"),
						template: {
							content: "{compartmentAgeUOM}"
						}
					}, {
						name: resourceBundle.getText("MAOACompDefFluidTXT"),
						template: {
							content: "{compartmentDefaultFluid}"
						}
					}, {
						name: resourceBundle.getText("MAOACompFluidGradeTXT"),
						template: {
							content: "{compartmentFluidGrade}"
						}
					}, {
						name: resourceBundle.getText("MAOACompFluidManTXT"),
						template: {
							content: "{sampleFluidManufacturer}"
						}
					}, {
						name: resourceBundle.getText("MAOASampleFluidAgeTXT"),
						template: {
							content: "{sampleFluidAge}"
						}
					}, {
						name: resourceBundle.getText("MAOAProbCodeTXT"),
						template: {
							content: "{problemCode}"
						}
					}, {
						name: resourceBundle.getText("MAOALastUpdDateTXT"),
						template: {
							content: "{lastUpdateDate}"
						}
					}, {
						name: resourceBundle.getText("MAOASampleFluidTXT"),
						template: {
							content: "{sampleFluid}"
						}
					}, {
						name: resourceBundle.getText("MAOASampleFGradeTXT"),
						template: {
							content: "{sampleFluidGrade}"
						}
					}, {
						name: resourceBundle.getText("MAOASampleFManTXT"),
						template: {
							content: "{sampleFluidManufacturer}"
						}
					}, {
						name: resourceBundle.getText("MAOADiagTXT"),
						template: {
							content: "{diagnosis}"
						}
					}]
				});

				var today = new Date();
				var month = _oController.pad(today.getMonth() + 1, 2, "0");
				var fileName = "OilAnalysis_" + today.getFullYear() + today.getDate() + month + today.getHours() + today.getMinutes() +
					today.getSeconds();

				oExport.saveFile(fileName).catch(function (oError) {

				}).then(function () {
					oExport.destroy();
				});
				//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
				_oController.GTMDataLayer('chartbttn',
					'Oil Analysis',
					oEvent.getSource().getText() + ' -ButtonClick',
					'Export Data'
				);
			} else {
				var selectedKey = _oController.byId("comboBoxAccountOilAnalysis").getSelectedKey();
				var districtFlag;
				var accountNum;

				_oView.byId("oilAnalysisExportButton").setBusyIndicatorDelay(0).setBusy(true);

				if (selectedKey.indexOf("district") === -1) {
					if (_oAccount.id === "0") {
						accountNum = "0";
					} else {
						accountNum = _oAccount.accountnum;
					}
					districtFlag = "0";
				} else if (selectedKey.indexOf("district") !== -1) {
					accountNum = "0";
					districtFlag = _oView.byId("comboBoxAccountOilAnalysis").getProperty("value");
				}
				var promiseExportData = new Promise(function (resolve, reject) {
					_oController.getExportData(accountNum, districtFlag, resolve);
				});
				promiseExportData.then(function () {
					_oView.byId("oilAnalysisExportButton").firePress();
				});
			}
		},

		/**
		 * Retrieves Visit page url
		 * 
		 * @function getVisitPageURL
		 */
		getVisitPageURL: function () {
			var nameID = "OilAnalysisURL";
			var groupID = "OilAnalysisParameters";

			_oController.getGlobalParameter(nameID, groupID, _oController.getVisitPageURLSuccess, _oController.getVisitPageURLError);
		},

		/**
		 * Runs on Visit page url service call success. Applies URL to global variable
		 * 
		 * @function getVisitPageURLSuccess
		 */
		getVisitPageURLSuccess: function (oController, oData, oPiggyBack) {
			_visitSiteURL = oData[0].value;
		},

		/**
		 *  Runs on Visit page url service call error. Displays error message.
		 * 
		 * @function getVisitPageURLError
		 */
		getVisitPageURLError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRParameterByName"));
		},

		/**
		 * Called when Visit site button is pressed. Opens URL on a new tab
		 * 
		 * @function onPressToSite
		 */
		onPressToSite: function (oEvent) {
			window.open(_visitSiteURL);

			//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
			_oController.GTMDataLayer('chartbttn',
				'Oil Analysis',
				oEvent.getSource().getText() + ' -ButtonClick',
				_visitSiteURL
			);
		},

		/**
		 * Called when Order Kits Button is pressed
		 * 
		 * @function onPressOrderKits
		 */
		onPressOrderKits: function (oEvent) {
			oEvent.getSource().setBusyIndicatorDelay(0);
			oEvent.getSource().setBusy(true);
			setTimeout(function () {
				// Checks Session time out
				_oController.checkSessionTimeout();

				//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
				_oController.GTMDataLayer('chartbttn',
					'Oil Analysis',
					'Order Your Oil Kit -ButtonClick',
					'Order Your Oil Kit'
				);
				if (_oController.checkDASHLayoutAccess("SHOP_ICON")) {
					// go to hybris
					var oHybrisoilKitPageParameter = _oController.getGlobalParameter("HybrisOilKitPage", "HybrisParameters");
					_oController.getView().byId("oilAnalysisOrderKitsButton").setBusy(false);
					if (oHybrisoilKitPageParameter && oHybrisoilKitPageParameter.length > 0) {
						_oController.nav2Hybris(oHybrisoilKitPageParameter[0].value);
					}
				} else {
					//User doesn´t have the PLCORD role so the order will be placed as a case
					var i18nModel = new sap.ui.model.resource.ResourceModel({
						bundleUrl: "i18n/i18n.properties"
					});
					var oUserInformation = _oView.getModel("oAccounts").getData();
					var oSelectedAccount = sap.ui.getCore().getModel("oSelectedAccount").getData();
					var sCostumerName, sAccountNumber;

					// If the fragement is already created, destroys all the content
					if (_oController.dialogFragmentOilKitOrder) {
						_oController.dialogFragmentOilKitOrder.destroyContent();
						_oController.dialogFragmentOilKitOrder.destroy();
					}

					_oController.dialogFragmentOilKitOrder = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.OrderOilKit",
						_oController);

					_oController.dialogFragmentOilKitOrder.setModel(i18nModel, "i18n");

					if (oSelectedAccount.id === "0" || oSelectedAccount.id.indexOf("district") !== -1) {
						//If all location or a district is selected set the direct account as default selection
						sCostumerName = oUserInformation.accounts[0].accountname;
						sAccountNumber = oUserInformation.accounts[0].accountnumber;
						_SelectedAccountNum = oUserInformation.accounts[0].accountnum;
					} else {
						//Set current selected account
						sCostumerName = oSelectedAccount.accountname;
						sAccountNumber = oSelectedAccount.accountnumber;
						_SelectedAccountNum = oSelectedAccount.accountnum;

					}
					//get the oil kit quantity options from the domains
					var oOilKitQuantityModel =
						new sap.ui.model.json.JSONModel(_oController.getDomainsByName("OilKitQuantity"));
					//set up the base model for case data
					var oOilKitOrderModel =
						new sap.ui.model.json.JSONModel({
							"oilKitQuantity": "",
							"costumerName": sCostumerName,
							"accountnum": _SelectedAccountNum,
							"accountnumber": sAccountNumber,
							"contactName": oUserInformation.firstname + " " + oUserInformation.lastname,
							"contactPhoneNumber": "",
							"email": oUserInformation.email
						});
					var aQuantity = oOilKitQuantityModel.getData();
					if (aQuantity && aQuantity.length > 0) {
						var oDefaultTxt = {
							id: 0,
							lang: aQuantity[0].lang,
							value: _oController.getResourceBundle().getText("MAOAOrderOilKitDefaultTxt"),
							description: aQuantity[0].description,
							name: aQuantity[0].name
						};
						aQuantity.unshift(oDefaultTxt);
					}

					_oController.dialogFragmentOilKitOrder.setModel(oOilKitQuantityModel, "orderKitQtyItems");
					_oController.dialogFragmentOilKitOrder.setModel(oOilKitOrderModel, "oilKitOrderModel");

					_oController.dialogFragmentOilKitOrder.open();
					_oController.getView().byId("oilAnalysisOrderKitsButton").setBusy(false);
					if (!_bAccountsLoaded) {
						sap.ui.getCore().byId("accountedit").setBusyIndicatorDelay(0);
						sap.ui.getCore().byId("accountedit").setBusy(true);
					}
					_oController.dialogFragmentOilKitOrder.focus();
				}
			}, 0);
		},
		/**
		 * Formatter to define if the sapAccountName Field should be visible
		 * 
		 * @function sapAccountNameVisibilityFormatter
		 */
		sapAccountNameVisibilityFormatter: function (accountName) {
			return (accountName !== undefined && accountName !== "");
		},

		/**
		 * Closes the Oil kit order dialog and destroys the fragment
		 * @function closeDialogOilKitOrder
		 */
		closeDialogOilKitOrder: function (oEvent) {
			if (this.dialogFragmentOilKitOrder) {
				this.dialogFragmentOilKitOrder.close();
				this.dialogFragmentOilKitOrder.destroyContent();
				this.dialogFragmentOilKitOrder.destroy();
			}
		},

		/**
		 * Closes the succes dialog for the order oil kil case
		 * 
		 * @function doneDialogOrderYourOilKitsSuccess
		 */
		onOYOKSuccessConfirm: function () {
			var dialog = sap.ui.getCore().byId("warningDialog");

			dialog.setBusy(false);
			dialog.close();
			dialog.destroy();
		},

		/**
		 * Runs on Severity service call success. Prepares Severity Pie charts with retrieved data
		 * and prepares model for the severity tables.
		 * 
		 * @function doneDialogOrderYourOilKitsSuccess
		 */
		doneDialogOrderYourOilKitsSuccess: function (oController, oData, oPiggyBack) {

			var oyokCaseConfirmation = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog");

			//Set Dialog Buttons
			sap.ui.getCore().byId("icon_Dialog_Warning").setSrc("sap-icon://customfont/success");
			sap.ui.getCore().byId("icon_Dialog_Warning").removeStyleClass("warningMessageDialogIcon");
			sap.ui.getCore().byId("icon_Dialog_Warning").addStyleClass("successMessageDialogIcon");
			sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(oController.onOYOKSuccessConfirm);
			sap.ui.getCore().byId("button_Dialog_Warning_No").setVisible(false);
			sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(oController.getResourceBundle().getText("DLWokTXT"));

			//Set Dialog Message
			sap.ui.getCore().byId("text1_Dialog_Warning").setText(oController.getResourceBundle().getText("MAOAOrderOilKitSuccessMSG1"));
			sap.ui.getCore().byId("text1_Dialog_Warning").addStyleClass("oyok-success-msg1-padding");
			sap.ui.getCore().byId("text2_Dialog_Warning").setText(oController.getResourceBundle().getText("MAOAOrderOilKitSuccessMSG2"));
			sap.ui.getCore().byId("text2_Dialog_Warning").addStyleClass("oyok-success-msg2-padding");

			//Set Dialog Title
			sap.ui.getCore().byId("title_Dialog_Warning").setText(oController.getResourceBundle().getText("MAOAOrderOilKitSuccessTitle"));

			oyokCaseConfirmation.open();
			oController._removeDialogResize(oyokCaseConfirmation);
			if (oController.dialogFragmentOilKitOrder) {
				oController.dialogFragmentOilKitOrder.close();
				oController.dialogFragmentOilKitOrder.destroyContent();
				oController.dialogFragmentOilKitOrder.destroy();
			}
		},

		/**
		 *  Runs on Severity service call Error. Hides Pie charts and displays error message.
		 * 
		 * @function doneDialogOrderYourOilKitsError
		 */
		doneDialogOrderYourOilKitsError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERROYOKCaseCreation"));

			if (oController.dialogFragmentOilKitOrder) {
				oController.dialogFragmentOilKitOrder.close();
				oController.dialogFragmentOilKitOrder.destroyContent();
				oController.dialogFragmentOilKitOrder.destroy();
			}
		},

		/**
		 * Sends the case information to open it in salesforce
		 * 
		 * @function doneDialogOrderYourOilKits
		 */
		doneDialogOrderYourOilKits: function () {
			this.dialogFragmentOilKitOrder.setBusyIndicatorDelay(0);
			this.dialogFragmentOilKitOrder.setBusy(true);

			var sSelectedLanguage, sQuantity, sAccountNumber, sPhone, xsURL;

			if (sap.ui.getCore().getModel("selectedLanguage") === undefined) {
				sSelectedLanguage = this.getSelectedLanguageForLangCode();
			} else {
				sSelectedLanguage = sap.ui.getCore().getModel("selectedLanguage").getData().selectedLanguage;
			}
			sQuantity = this.dialogFragmentOilKitOrder.getModel("oilKitOrderModel").getProperty("/oilKitQuantity");
			sAccountNumber = this.dialogFragmentOilKitOrder.getModel("oilKitOrderModel").getProperty("/accountnum");
			sPhone = this.dialogFragmentOilKitOrder.getModel("oilKitOrderModel").getProperty("/contactPhoneNumber");

			xsURL = "/oilanalysis/orderOilKit?quantity=" + sQuantity + "&accountNumber=" + sAccountNumber;
			if (sPhone) {
				xsURL = xsURL + "&phone=" + sPhone;
			}
			xsURL = xsURL + "&lang=" + sSelectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "POST", _oController.doneDialogOrderYourOilKitsSuccess, _oController.doneDialogOrderYourOilKitsError);
		},

		/**
		 *  Handles the press event for changing the selected account.
		 * 
		 * @function doneDialogOrderYourOilKitsError
		 */
		onPressSelectAccount: function (oEvent) {

			// Checks Session time out
			this.checkSessionTimeout();

			// If the fragement is already created, destroys all the content
			if (this.dialogFragmentAccountSelect) {
				this.dialogFragmentAccountSelect.destroyContent();
				this.dialogFragmentAccountSelect.destroy();
			}

			this.dialogFragmentAccountSelect = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.OrderOilAccount", this);
			this.dialogFragmentAccountSelect.open();

			//binds the i18n model to the dialog
			var i18nModel = new sap.ui.model.resource.ResourceModel({
				bundleUrl: "i18n/i18n.properties"
			});
			this.dialogFragmentAccountSelect.setModel(i18nModel, "i18n");

			//Sets up the account for the selection
			this.onAccountSelectConfiguration();
			//Sets the number of lines that should be visible on the account table pagination
			this.resizeToMobile(sap.ui.Device.media.getCurrentRange(DASH_MOBILE));

			this.dialogFragmentAccountSelect.focus();
		},

		/**
		 *  Sets the number of visible rows based on the screen size.
		 * 
		 * @function resizeToMobile
		 */
		resizeToMobile: function (mParams) {
			if (sap.ui.getCore().byId("AccountSelectFragment") !== undefined && sap.ui.getCore().byId("AccountSelectFragment").getVisible()) {
				if (mParams.name === "mobile") {
					_iAccountSelectPageSize = 1;
				} else {
					_iAccountSelectPageSize = 5;
				}
				_beginAccountSelect = 0;
				this.tablePaginationAccountSelect();
			}
		},

		/**
		 * When the quantity is selected sets it on the model and enables the order button
		 * 
		 * @function onSetOilKitQuantity
		 */
		onSetOilKitQuantity: function (oEvent) {
			if (oEvent.getParameter("selectedItem").getKey() === "0") {
				this.dialogFragmentOilKitOrder.getModel("oilKitOrderModel").setProperty("/oilKitQuantity", "");
				if (sap.ui.getCore().byId("phone").getValueState() === "None") {
					sap.ui.getCore().byId("oyokOrderButton").setEnabled(false);
				}
			} else {
				this.dialogFragmentOilKitOrder.getModel("oilKitOrderModel").setProperty("/oilKitQuantity", oEvent.getParameter("selectedItem").getText());
				if (sap.ui.getCore().byId("phone").getValueState() === "None") {
					sap.ui.getCore().byId("oyokOrderButton").setEnabled(true);
				}
			}

		},

		/**
		 * Closes the AccountSelection Fragment
		 * 
		 * @function cancelDialogAccountSelect
		 */
		cancelDialogAccountSelect: function () {
			this.dialogFragmentAccountSelect.close();
			this.dialogFragmentAccountSelect.destroyContent();
			this.dialogFragmentAccountSelect.destroy();

		},

		/**
		 * Sets the selected account updating the required models and closes/destroys the fragment
		 * 
		 * @function cancelDialogAccountSelect
		 */
		doneDialogAccountSelect: function () {
			this.dialogFragmentOilKitOrder.getModel("oilKitOrderModel").setProperty("/accountnum", _TempSelectedAccountNum);
			this.dialogFragmentOilKitOrder.getModel("oilKitOrderModel").setProperty("/accountnumber", _TempSelectedAccountNumber);
			this.dialogFragmentOilKitOrder.getModel("oilKitOrderModel").setProperty("/costumerName", _TempSelectedCostumerName);
			_SelectedAccountNum = _TempSelectedAccountNum;

			this.dialogFragmentAccountSelect.close();
			this.dialogFragmentAccountSelect.destroyContent();
			this.dialogFragmentAccountSelect.destroy();

		},

		/**
		 * Loads the Accounts Information to populate the table adding the selection property
		 * 
		 * @function onAccountSelectConfiguration
		 */
		onAccountSelectConfiguration: function () {
			var oModelAccounts = new sap.ui.model.json.JSONModel();
			var i;

			oModelAccounts.setData(sap.ui.getCore().getModel("oAccounts").getData().accounts);
			for (i = 0; i < oModelAccounts.getData().length; i++) {
				if (oModelAccounts.getData()[i].accountnum === _SelectedAccountNum) {
					oModelAccounts.setProperty("/" + i + "/isSelected", true);
					_TempSelectedAccountNum = oModelAccounts.getProperty("/" + i + "/accountnum");
					_TempSelectedCostumerName = oModelAccounts.getProperty("/" + i + "/accountname");
					_TempSelectedAccountNumber = oModelAccounts.getProperty("/" + i + "/accountnumber");
				} else {
					oModelAccounts.setProperty("/" + i + "/isSelected", false);
				}
			}

			// Sets the right data to the table
			_oModelAccountSelectTable = oModelAccounts;
			_oModelAccountSelectSortedData = oModelAccounts;
		},

		/**
		 * Validates if the phone syntax is correct
		 * @function verifyPhoneInput
		 * @param {Object} oEvent - Phone input data event
		 */
		verifyPhoneInput: function (oEvent) {
			var sPhoneValue = sap.ui.getCore().byId("phone").getValue(),
				bValPhone = true,
				regex = /^[+]*[(]{0,1}[0-9]{0,4}[)]{0,1}[0-9][-\s\.0-9]*$/; //Regular expression that matches the structure of a phone number
			//Validation of the fields
			if (sPhoneValue.length !== 0) {
				bValPhone = regex.test(sap.ui.getCore().byId("phone").getValue());
			}

			if (bValPhone) {
				oEvent.getSource().setValueState(sap.ui.core.ValueState.None);
				if (sap.ui.getCore().byId("orderKitQty").getSelectedIndex() !== 0) {
					sap.ui.getCore().byId("oyokOrderButton").setEnabled(true);
				}
			} else {
				oEvent.getSource().setValueState(sap.ui.core.ValueState.Error);
				sap.ui.getCore().byId("oyokOrderButton").setEnabled(false);
			}
		},

		/**
		 * Filters the table
		 * @function onLiveSearchAccountSelect
		 * @param {Object} oEvent - Account Select input data
		 */
		onLiveSearchAccountSelect: function (oEvent) {
			var newValue = oEvent.getParameter("newValue");
			var oDataAccountSelectTableFiltered = [];
			var oModelAccountSelectTableFiltered = new sap.ui.model.json.JSONModel();

			if (!newValue) {
				_beginAccountSelect = 0; // Sets the arrows to 0
				_oModelAccountSelectTable = _oModelAccountSelectSortedData;
				this.tablePaginationAccountSelect();
			} else {
				var aFilter = [
					new sap.ui.model.Filter("accountname", sap.ui.model.FilterOperator.Contains, newValue),
					new sap.ui.model.Filter("sapaccountname2", sap.ui.model.FilterOperator.Contains, newValue),
					new sap.ui.model.Filter("accountnum", sap.ui.model.FilterOperator.Contains, newValue),
					new sap.ui.model.Filter("billingstreet", sap.ui.model.FilterOperator.Contains, newValue),
					new sap.ui.model.Filter("billingcity", sap.ui.model.FilterOperator.Contains, newValue),
					new sap.ui.model.Filter("billingstate", sap.ui.model.FilterOperator.Contains, newValue),
					new sap.ui.model.Filter("billingpostalcode", sap.ui.model.FilterOperator.Contains, newValue)
				];

				var filter = new sap.ui.model.Filter(aFilter, false); //False means it will apply an OR logic, if you want AND pass true
				sap.ui.getCore().byId("AccountSelectTable").setModel(_oModelAccountSelectSortedData, "AccountSelect");
				sap.ui.getCore().byId("AccountSelectTable").getBinding("items").filter(filter);

				var itemsIndices = sap.ui.getCore().byId("AccountSelectTable").getBinding("items").aIndices;
				for (var i = 0; i < itemsIndices.length; i++) {
					oDataAccountSelectTableFiltered.push(_oModelAccountSelectSortedData.getProperty("/" + itemsIndices[i]));
				}

				_beginAccountSelect = 0; // Sets the arrows to 0
				oModelAccountSelectTableFiltered.setData(oDataAccountSelectTableFiltered);
				_oModelAccountSelectTable = oModelAccountSelectTableFiltered;
				this.tablePaginationAccountSelect();
			}
		},

		/**
		 * Set the new selected account
		 * @function onSelectFlag
		 * @param {Object} oEvent - The selected selected combo box
		 */
		onSetSelectedAccount: function (oEvent) {
			var oSelectedObject = oEvent.getSource().getBindingContext("AccountSelect").getObject();
			var i;
			if (oEvent.getParameter("selected")) {
				//updates the original model and stores the selected account information on variables to avoid crossing the model when confirming the change
				for (i = 0; i < _oModelAccountSelectSortedData.getData().length; i++) {
					if (_oModelAccountSelectSortedData.getProperty("/" + i + "/accountnum") === oSelectedObject.accountnum) {
						_TempSelectedAccountNum = _oModelAccountSelectSortedData.getProperty("/" + i + "/accountnum");
						_TempSelectedCostumerName = _oModelAccountSelectSortedData.getProperty("/" + i + "/accountname");
						_TempSelectedAccountNumber = _oModelAccountSelectSortedData.getProperty("/" + i + "/accountnumber");
						_oModelAccountSelectSortedData.setProperty("/" + i + "/isSelected", true);
					} else {
						_oModelAccountSelectSortedData.setProperty("/" + i + "/isSelected", false);
					}
				}
			}

		},

		/**
		 * Sets the layout of the arrows for the table and controls the data to be loaded in the table
		 * 
		 * @function tablePaginationAccountAccess
		 */
		tablePaginationAccountSelect: function () {
			var _iLengthActive = _oModelAccountSelectTable.getData().length;
			var end = _beginAccountSelect + _iAccountSelectPageSize;
			var oDataTablePaginated = [];
			var oModelTablePaginated = new sap.ui.model.json.JSONModel();

			if (_iLengthActive === 0) {
				sap.ui.getCore().byId("navigationArrowsAccountSelect").setVisible(false);
			} else {
				sap.ui.getCore().byId("navigationArrowsAccountSelect").setVisible(true);
			}

			// Arrows and Number indicator layout
			if (_beginAccountSelect < _iAccountSelectPageSize) {
				sap.ui.getCore().byId("idLeftNavAccountSelect").setEnabled(false);
			} else {
				sap.ui.getCore().byId("idLeftNavAccountSelect").setEnabled(true);
			}
			if (end >= _iLengthActive) {
				end = _iLengthActive;
				sap.ui.getCore().byId("idRightNavAccountSelect").setEnabled(false);
			} else {
				sap.ui.getCore().byId("idRightNavAccountSelect").setEnabled(true);
			}
			if (_iLengthActive < _iAccountSelectPageSize) {
				end = _iLengthActive;
			}
			if (_iAccountSelectPageSize === 1 && _iLengthActive !== 0) {
				sap.ui.getCore().byId("idTextCountAccountSelect").setText(_beginAccountSelect + 1 + " " + _oController.getResourceBundle().getText(
						"of") + " " +
					_iLengthActive);
			} else {
				sap.ui.getCore().byId("idTextCountAccountSelect").setText(_beginAccountSelect + 1 + "-" + end + " " + _oController.getResourceBundle()
					.getText("of") + " " + _iLengthActive);
			}

			// Paginated Model
			for (var i = _beginAccountSelect; i < end; i++) {
				oDataTablePaginated.push(_oModelAccountSelectTable.getProperty("/" + i));
			}
			oModelTablePaginated.setData(oDataTablePaginated);
			sap.ui.getCore().byId("AccountSelectTable").setModel(oModelTablePaginated, "AccountSelect");
			sap.ui.getCore().byId("noDataText").setModel(oModelTablePaginated, "modelAccountAccess");
		},

		/**
		 * Loads more items into the table
		 * @function toRightAccountSelect
		 */
		toRightAccountSelect: function () {
			_beginAccountSelect = _beginAccountSelect + _iAccountSelectPageSize;
			this.tablePaginationAccountSelect();
		},

		/**
		 * Loads more items into the table
		 * @function toLeftAccountSelect
		 */
		toLeftAccountSelect: function () {
			_beginAccountSelect = _beginAccountSelect - _iAccountSelectPageSize;
			this.tablePaginationAccountSelect();
		},

		/**
		 * Called when Visit site button is pressed. Calles the service to retrieve export data.
		 * 
		 * @function onPressExport
		 */
		prepareExport: function () {
			var selectedKey = _oController.byId("comboBoxAccountOilAnalysis").getSelectedKey();
			var districtFlag;
			var accountNum;

			_oView.byId("oilAnalysisExportButton").setBusyIndicatorDelay(0).setBusy(true);
			$(':focus').blur();

			if (selectedKey.indexOf("district") === -1) {
				if (_oAccount.id === "0") {
					accountNum = "0";
				} else {
					accountNum = _oAccount.accountnum;
				}
				districtFlag = "0";
			} else if (selectedKey.indexOf("district") !== -1) {
				accountNum = "0";
				districtFlag = _oView.byId("comboBoxAccountOilAnalysis").getProperty("value");
			}
			_oController.getExportData(accountNum, districtFlag);
		},

		/**
		 * Opens Severity tables when pie chart is pressed.
		 * 
		 * @function handleSeverityChartPress
		 */
		handleSeverityChartPress: function (oEvent) {
			// Checks Session time out
			_oController.checkSessionTimeout();

			if (this._oDialog) {
				this._oDialog.destroyContent();
			}

			var sSeverityLabel = _oView.getModel("i18n").getResourceBundle().getText("MAOASeverityTXT");
			var sSeverity = oEvent.getParameters().data[0].data.severity;
			var sChartID = oEvent.getParameters().id;

			this._oDialog = sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.OilAnalysisSeverity", this);

			if (sChartID.indexOf("OilAnalysisPieChart12") !== -1) {
				this._oDialog.setModel(_oView.getModel("severity12Months"));
			} else {
				this._oDialog.setModel(_oView.getModel("severity3Months"));
			}

			$(':focus').blur();

			this.getView().addDependent(this._oDialog);
			this._oDialog.open();

			var oSeverityColumn = sap.ui.getCore().byId("severityTable").getColumns()[5];
			sap.ui.getCore().byId("oilAnalysisDialogHeaderText").setText(sSeverityLabel + ": " + sSeverity);
			sap.ui.getCore().byId("severityTable").filter(oSeverityColumn, sSeverity);
		},

		/**
		 * Called when Severity Dialog is closed. 
		 * 
		 * @function closeSeverityDialog
		 */
		closeSeverityDialog: function () {
			this._oDialog.close();
			this._oDialog.destroy();

			// This change forces the chart to update, deselecting all data slices from both pie charts.
			_oView.byId("OilAnalysisPieChart12").setVizProperties({
				plotArea: {
					dataLabel: {
						visible: "True"
					}
				}
			});
			_oView.byId("OilAnalysisPieChart3").setVizProperties({
				plotArea: {
					dataLabel: {
						visible: "True"
					}
				}
			});
		},

		/**
		 * Prepares Pie chart with required setiings
		 * 
		 * @function preparePieChart
		 */
		preparePieChart: function (OAPieChart) {
			var oVizFrame = this.getView().byId(OAPieChart);
			var severity = "severity";
			var count = "count";

			oVizFrame.setVizProperties({
				dataLabel: {
					visible: true
				},
				plotArea: {
					colorPalette: ['#008000', '#FFFF00', "#FF6600", "#FF0000"]
				},
				categoryAxis: {
					title: {
						visible: true
					},
					label: {
						angle: 30
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						fontSize: 20,
						position: 'top',
						maxWidth: 100,
						height: 50,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				},
				interaction: {
					selectability: {
						mode: "single"
					}
				}
			});
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: severity,
					value: "{severity}"
				}],

				measures: [{
					name: count,
					value: "{count}"
				}],

				data: {
					path: "/"
				}
			});

			oVizFrame.setDataset(oDataset);

			var feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
				'uid': "color",
				'type': "Dimension",
				'values': [severity]
			});
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
				'uid': "size",
				'type': "Measure",
				'values': [count]
			});

			oVizFrame.addFeed(feedValueAxis);
			oVizFrame.addFeed(feedCategoryAxis);
		},

		/**
		 * Prepares Columns Charts with Required settings
		 * 
		 * @function prepareColumnChart
		 */
		prepareColumnChart: function (sOAChart, sChartName) {
			var oVizFrame = this.getView().byId(sOAChart);
			var sFrequency = _oView.getModel("i18n").getResourceBundle().getText("MAOAFreqCLM");
			oVizFrame.setVizProperties({
				plotArea: {
					colorPalette: ['#009cd9', '#e1261c'],
					dataPointSize: {
						min: 60,
						max: 150
					}
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: true,
						style: {
							fontSize: "14px",
							fontWeight: "bold",
							fontFamily: "Roboto"
						}
					},
					label: {
						angle: 30
					}
				},
				title: {
					visible: false
				},
				tooltip: {
					visible: true
				},
				legendGroup: {
					layout: {
						fontSize: 20,
						position: 'top',
						maxWidth: 100,
						height: 50,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				},
				interaction: {
					selectability: {
						mode: "none"
					}
				}
			});
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: sChartName,
					value: "{sampleFluidAge}"
				}],

				measures: [{
					name: sFrequency,
					value: "{count}"
				}],

				data: {
					path: "/"
				}
			});

			oVizFrame.setDataset(oDataset);

			var feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
				'uid': "categoryAxis",
				'type': "Dimension",
				'values': [sChartName]
			});
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
				'uid': "valueAxis",
				'type': "Measure",
				'values': [sFrequency]
			});

			oVizFrame.addFeed(feedValueAxis);
			oVizFrame.addFeed(feedCategoryAxis);
		},
		/**
		 * Formatter for no Accounts order oil kits Account Access.
		 * @function formatterNoAccountsAcessVisiblity
		 * @param {Object} model - Account Acess model.
		 * @returns {Boolean} - True or False if model have values
		 */
		formatterNoAccountsAccessVisiblity: function (model) {
			if (Object.keys(model).length === 0) {
				sap.ui.getCore().byId("AccountSelectTable").addStyleClass("fix-border-accountaccess");
				return true;
			}
			return false;
		}
	});
});