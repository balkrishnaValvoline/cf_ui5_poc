sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel",
	'sap/ui/core/util/Export',
	'sap/ui/core/util/ExportTypeCSV',
	"sap/m/MessageToast",
	"sap/ui/core/mvc/Controller",
	"jquery.sap.global"
], function (Base, JSONModel, Export, ExportTypeCSV, MessageToast, Controller, jQuery) {
	"use strict";
	var _oController;
	var _oView;
	var _oRouter;
	var _oModelChart = new JSONModel();
	var _oModelChartIntact = new JSONModel();
	var _oColumnsChart = new JSONModel();
	var _sSortedLabel;
	var _sSortedOrder;
	var _isSubmitted = false;
	var _accountIdSelected;
	var _sPeriodSelected;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.MyAccount.MyAccountSellinData", {
		/** @module MyAccountSellinData */
		/**
		 * This function initializes the controller 
		 * 
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oController = this;
			_oView = _oController.getView();
			_oRouter = this.getRouter();
			_oRouter.getRoute("myAccountSellinData").attachPatternMatched(this._onObjectMatched, this);
			this.searchFieldEnabled();

		},

		_onObjectMatched: function (oEvent) {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page Layout Configuration
			if (!this.checkDASHPageAuthorization("MILLER_PRO_PAGE")) {
				return;
			}

			// Set "ENTRY FORM" button invisible if authorization "MILLER_PRO_FORM" is missing
			if (this.checkDASHLayoutAccess("MILLER_PRO_FORM")) {
				_oView.byId("SellinDataSubmit").setVisible(true);
			} else {
				_oView.byId("SellinDataSubmit").setVisible(false);
			}

			_sSortedLabel = "dealerCode";
			_sSortedOrder = "ascending";
			this.getView().byId("sortButton_dealerCode").setSrc("resources/icon/sort_asc.png");

			//reset other sort icons
			this.getView().byId("sortButton_dealerName").setSrc("resources/icon/sort_both.png");

			this.getView().byId("sortButton_PrevMonthPrevYear_TotalCPRO").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthPrevYear_TotalVPS").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthPrevYear_Maintenance").setSrc("resources/icon/sort_both.png");

			this.getView().byId("sortButton_PrevMonthCurrentYear_TotalCPRO").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthCurrentYear_TotalVPS").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthCurrentYear_Maintenance").setSrc("resources/icon/sort_both.png");

			this.getView().byId("sortButton_Variation").setSrc("resources/icon/sort_both.png");

			this.getView().byId("sortButton_CurrentMonthCurrentYear_TotalCPRO").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_CurrentMonthCurrentYear_TotalVPS").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_CurrentMonthCurrentYear_Maintenance").setSrc("resources/icon/sort_both.png");

			//get user info and direct account
			this.getUserInformation(this);

			// Check Language and set selected Key in combo box
			this.onChangeLanguage();

			this.headerIconAccess();

			// Shopping Cart Values
			this.getShoppingCartCookie(this);

			var chart_table = _oController.getView().byId("chart_MainTable");
			chart_table.setBusyIndicatorDelay(0);
			chart_table.setBusy(true);

			this.getView().byId("SellinDataSubmit").setEnabled(false);
			_oController.getView().byId("searchFieldSellinData").setBusyIndicatorDelay(0);
			_oController.getView().byId("searchFieldSellinData").setBusy(true);

			// Load Sell-in Data Chart
			this.loadChart();
		},

		/**
		 * Loads the Sell-in Data chart
		 * @function loadChart
		 */
		loadChart: function () {

			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			var oPiggyBack = {};

			var xsURL = "/sellindata/monthlydata" + "?lang=" + selectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onLoadChartSuccess, _oController.onLoadChartError,
				null, oPiggyBack);
		},

		/**
		 * Function that handles the success case of the ajax call that loads the Sell-in Data chart
		 * @function onLoadChartSuccess
		 */
		onLoadChartSuccess: function (oController, oData, oPiggyBack) {
			var oChart = oData;
			_sSortedLabel = "dealerCode";
			_sSortedOrder = "ascending";

			_oModelChartIntact.setData(oData);
			_oController.getView().setModel(_oModelChartIntact, "oChartIntact");

			if (oData.length) {
				//adds the Total row
				oData.push(oController.computeTotal(oChart));
				//set the month/year headers data
				oPiggyBack.prevYearPrevMonth = oData[0].prevYearPrevMonth.month + "-" + oData[0].prevYearPrevMonth.year;
				oPiggyBack.currentYearPrevMonth = oData[0].prevYearPrevMonth.month + "-" + oData[0].currentYearPrevMonth.year;
				oPiggyBack.prevMonth = oData[0].prevYearPrevMonth.month;
				oPiggyBack.prevYear = oData[0].prevYearPrevMonth.year;
				oPiggyBack.currentYear = oData[0].currentYearPrevMonth.year;
				oPiggyBack.currentYearCurrentMonth = oData[0].currentYearCurrentMonth.month + "-" + oData[0].currentYearCurrentMonth
					.year;
			}
			//model for the header columns
			_oColumnsChart.setData(oPiggyBack);
			_oController.getView().setModel(_oColumnsChart, "oColumnsChart");
			_oModelChart.setData(oData);
			oController.getView().setModel(_oModelChart, "oChart");

			//sorting the model
			oController.onFilteroModel(_oModelChart.getData(), _sSortedOrder, _sSortedLabel);
			_oView.getModel("oChart").updateBindings();

			_oController.getView().byId("chart_MainTable").setBusy(false);
			_oController.getView().byId("SellinDataSubmit").setEnabled(true);
			_oController.getView().byId("searchFieldSellinData").setBusy(false);
			_oController.getView().byId("searchFieldSellinData").setValue("");

		},

		/**
		 * Function that handles the error case of the ajax call that loads the Sell-in Data chart
		 * @function onLoadChartError
		 */
		onLoadChartError: function (oController, oError, oPiggyBack) {
			_oColumnsChart.setData(oPiggyBack);
			_oController.getView().setModel(_oColumnsChart, "oColumnsChart");
			oController.getView().byId("chart_MainTable").setBusy(false);
			oController.getView().byId("SellinDataSubmit").setEnabled(true);
			oController.getView().byId("searchFieldSellinData").setBusy(false);

			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"MASDLoadChartErrorTXT"), null);
		},

		/**
		 * Select fragment sellinDataLocation (Entry Form 1st dialog)
		 * @function onPressEntryForm
		 * @param {Object} oEvent - Entry Form button
		 */
		onPressEntryForm: function (oEvent) {
			// Checks the session timeout
			this.checkSessionTimeout();

			// If the fragment is already created, destroys all the content
			if (this.dialogFragmentSellinDataLocation) {
				if (_oController.getView().getModel("oPeriod") !== undefined) {
					_oController.getView().getModel("oPeriod").setData([]);
				}
				this.dialogFragmentSellinDataLocation.destroyContent();

			}

			this.dialogFragmentSellinDataLocation =
				new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.EntryFormSellinDataLocation", this);
			this.getView().addDependent(this.dialogFragmentSellinDataLocation);

			this.dialogFragmentSellinDataLocation.open();

			sap.ui.getCore().byId("SellinDataLocationList").setBusyIndicatorDelay(0);
			sap.ui.getCore().byId("SellinDataLocationList").setBusy(true);

			// Get the User Accounts
			this.getAccounts(this, this.setupAllAccounts);

			//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
			//Tracking for onclick - Entry Form button

			_oController.GTMDataLayer('entryFrmbttn',
				'Miller Pro',
				'Button click',
				oEvent.getSource().getText()
			);

		},

		/**
		 *  Close and destroy the entry form (1st dialog).
		 * @function closeDialog
		 */
		closeDialog: function () {
			this.dialogFragmentSellinDataLocation.close();
			this.dialogFragmentSellinDataLocation.destroy();
		},

		closeEntryFormDialog: function () {
			this.dialogFragmentSellinDataEntryForm.close();
			this.dialogFragmentSellinDataEntryForm.destroy();
		},

		/**
		 * On Select location - calls the entry form service
		 *
		 */
		onLoadEntryForm: function () {
			_accountIdSelected = sap.ui.getCore().byId("SellinDataLocationList").getSelectedKey();

			if (sap.ui.getCore().byId("SellinDataLocationList").getSelectedKey() === "") {
				if (_oController.getView().getModel("oPeriod") !== undefined) {
					_oController.getView().getModel("oPeriod").setData([]);
				}
			}

			if (_accountIdSelected !== null && _accountIdSelected !== undefined && _accountIdSelected !== "") {
				var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();

				sap.ui.getCore().byId("SellinDataPeriod").setBusyIndicatorDelay(0);
				sap.ui.getCore().byId("SellinDataPeriod").setBusy(true);
				var xsURL = "/sellindata/entryform" + "?accountId=" + _accountIdSelected + "&lang=" + selectedLanguage;

				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onLoadEntryFormSuccess, _oController.onLoadEntryFormError);
			} else {
				sap.ui.getCore().byId("SellinDataPeriod").setEnabled(false);
				sap.ui.getCore().byId("SellinDataNext").setEnabled(false);
				if (_oController.getView().getModel("oPeriod") !== undefined) {
					_oController.getView().getModel("oPeriod").setData([]);
				}
			}

		},

		/**
		 * Function that handles the success case of the ajax call that loads the entry form
		 * @function onLoadEntryFormSuccess
		 */
		onLoadEntryFormSuccess: function (oController, oData, oPiggyBack) {

			var aEntryForm = [];
			var aPeriodToDisplay = [];
			var sStartDate, sEndDate, oElemPeriod;
			//model with the time periods
			var oPeriod = new JSONModel();
			//model with the content of the entry form
			var oEntryForm = new JSONModel();
			var oElemPeriodPLCHLDR = {
				period: oController.getResourceBundle().getText(
					"MASDPLCHLDRPeriod"),
				key: "0"
			};

			if (oData.length) {
				for (var j = 0; j < oData.length; j++) {
					// if it is the current period or it wasn't submitted before
					if (oData[j].isCurrentWeekData === "T" || oData[j].totalRepairOrder === undefined) {
						aEntryForm.push(oData[j]);
						sStartDate = oController.formatDate(oData[j].startDate);
						sEndDate = oController.formatDate(oData[j].endDate);
						oElemPeriod = {};
						oElemPeriod.period = sStartDate + " - " + sEndDate;
						oElemPeriod.key = oData[j].year + "-" + oData[j].month + "-" + oData[j].week;
						aPeriodToDisplay.push(oElemPeriod);
					}
				}
			}
			aPeriodToDisplay.unshift(oElemPeriodPLCHLDR);

			oPeriod.setData(aPeriodToDisplay);
			oEntryForm.setData(aEntryForm);
			oController.getView().setModel(oPeriod, "oPeriod");
			oController.getView().setModel(oEntryForm, "oEntryForm");

			sap.ui.getCore().byId("SellinDataPeriod").setBusy(false);
			sap.ui.getCore().byId("SellinDataPeriod").setEnabled(true);
			sap.ui.getCore().byId("SellinDataPeriod").setSelectedItem(null);
			sap.ui.getCore().byId("SellinDataNext").setEnabled(false);

		},

		/**
		 * Function that handles the error case of the ajax call that loads the entry form
		 * @function onLoadEntryFormError
		 */
		onLoadEntryFormError: function (oController, oError, oPiggyBack) {
			sap.ui.getCore().byId("SellinDataPeriod").setBusy(false);
			sap.ui.getCore().byId("SellinDataPeriod").setEnabled(false);
			sap.ui.getCore().byId("SellinDataNext").setEnabled(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"MASDEntryFormErrorTXT"), null);
		},

		/**
		 * Selects the time period
		 * @function onSelectPeriod
		 * @param {Object} oEvent
		 */
		onSelectPeriod: function () {
			_sPeriodSelected = sap.ui.getCore().byId("SellinDataPeriod").getSelectedItem().getText();
			if (sap.ui.getCore().byId("SellinDataPeriod").getSelectedKey() !== "0" && sap.ui.getCore().byId("SellinDataLocationList").getSelectedKey() !==
				"") {
				sap.ui.getCore().byId("SellinDataNext").setEnabled(true);
			} else {
				sap.ui.getCore().byId("SellinDataNext").setEnabled(false);
			}

		},

		/**
		 * Select fragment onPressFillinEntryForm (Entry Form 2nd dialog)
		 * @function onPressFillinEntryForm
		 *
		 */
		onPressFillinEntryForm: function () {

			// Checks the session timeout
			this.checkSessionTimeout();

			// If the fragment is already created, destroys all the content
			if (this.dialogFragmentSellinDataEntryForm) {
				this.dialogFragmentSellinDataEntryForm.destroyContent();
			}

			var sKeyPeriod = sap.ui.getCore().byId("SellinDataPeriod").getSelectedKey();
			var sWeekDate = sKeyPeriod.split("-").join("");
			var aEntryFormData = _oController.getView().getModel("oEntryForm").getData();
			var oEntryFormSelected = new JSONModel();
			var oSelectedEntryForm;
			_isSubmitted = false;

			for (var i = 0; i < aEntryFormData.length; i++) {
				if (sWeekDate === aEntryFormData[i].weekDate) {
					oSelectedEntryForm = JSON.parse(JSON.stringify(aEntryFormData[i]));
					if (aEntryFormData[i].isCurrentWeekData === "T" && aEntryFormData[i].totalRepairOrder !== undefined) {
						_isSubmitted = true;
					}
					break;
				}
			}

			oEntryFormSelected.setData(oSelectedEntryForm);
			_oController.getView().setModel(oEntryFormSelected, "oEntryFormSelected");

			this.dialogFragmentSellinDataEntryForm =
				new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.EntryFormSellinData", this);
			this.getView().addDependent(this.dialogFragmentSellinDataEntryForm);

			this.dialogFragmentSellinDataEntryForm.open();

		},

		/**
		 * Submits the Entry Form
		 * @function SubmitEntryForm
		 */
		SubmitEntryForm: function () {

			// Checks the session timeout
			this.checkSessionTimeout();

			var entryForm = _oController.getView().getModel("oEntryFormSelected").getData();

			entryForm.accountID = _accountIdSelected;
			entryForm.totalRepairOrder = sap.ui.getCore().byId("entryform_field0").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field0").getValue();
			entryForm.odorEliminator = sap.ui.getCore().byId("entryform_field1").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field1").getValue();
			entryForm.oilAdditive = sap.ui.getCore().byId("entryform_field2").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field2").getValue();
			entryForm.batteryKit = sap.ui.getCore().byId("entryform_field3").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field3").getValue();
			entryForm.brakeStopSqueal = sap.ui.getCore().byId("entryform_field4").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field4").getValue();
			entryForm.fuelPopPourProducts = sap.ui.getCore().byId("entryform_field5").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field5").getValue();
			entryForm.vpsbrakeFluid = sap.ui.getCore().byId("entryform_field6").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field6").getValue();
			entryForm.vpsCoolant = sap.ui.getCore().byId("entryform_field7").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field7").getValue();
			entryForm.vpsFrontDifferential = sap.ui.getCore().byId("entryform_field8").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field8").getValue();
			entryForm.vpsRearDifferential = sap.ui.getCore().byId("entryform_field9").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field9").getValue();
			entryForm.vpsDifferentialAdditive = sap.ui.getCore().byId("entryform_field10").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field10").getValue();
			entryForm.vpsEngineCleaning = sap.ui.getCore().byId("entryform_field11").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field11").getValue();
			entryForm.vpsFuelRail = sap.ui.getCore().byId("entryform_field12").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field12").getValue();
			entryForm.vpsManualTransmission = sap.ui.getCore().byId("entryform_field13").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field13").getValue();
			entryForm.vpsPowerSteering = sap.ui.getCore().byId("entryform_field14").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field14").getValue();
			entryForm.vpsTransmisson = sap.ui.getCore().byId("entryform_field15").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field15").getValue();
			entryForm.vps15kRestorationKit = sap.ui.getCore().byId("entryform_field16").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field16").getValue();
			entryForm.throttleBody = sap.ui.getCore().byId("entryform_field17").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field17").getValue();
			entryForm.vps4x4 = sap.ui.getCore().byId("entryform_field18").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field18").getValue();
			entryForm.other = sap.ui.getCore().byId("entryform_field19").getValue() === "" ? 0 : sap.ui.getCore().byId(
				"entryform_field19").getValue();

			if (_isSubmitted === false) {

				//GTM
				_oController.GTMDataLayer('entryFrmbttn',
					'Miller Pro',
					'Button click',
					'Entry Form Submitted'
				);
				_oController.sendEntryForm();

			} else {

				setTimeout(function () {
					var reSubmitEntryFormDialog = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog");
					//Set Warning Dialog Buttons
					sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.reSubmitEntryForm);
					sap.ui.getCore().byId("button_Dialog_Warning_No").attachPress(_oController.onReturn);
					sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWyesTXT"));
					sap.ui.getCore().byId("button_Dialog_Warning_No").setText(_oController.getResourceBundle().getText("DLWnoTXT"));
					//Set Warning Dialog Message
					sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText(
						"MASDEntryFormAlreadySubmittedMsg1TXT"));
					sap.ui.getCore().byId("text2_Dialog_Warning").setText(_oController.getResourceBundle().getText(
						"MASDEntryFormAlreadySubmittedMsg2TXT", _sPeriodSelected));
					//Set Warning Dialog Title
					sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("MASDEntryFormAlreadySubmitted"));
					reSubmitEntryFormDialog.open();
					_oController._removeDialogResize(reSubmitEntryFormDialog);
				}, 0);

			}

		},

		/**
		 * Re-Submits the Entry Form
		 * @function reSubmitEntryForm
		 */
		reSubmitEntryForm: function () {
			//GTM
			_oController.GTMDataLayer('entryFrmbttn',
				'Miller Pro',
				'Button click',
				'Entry Form Re-Submitted'
			);

			_oController.sendEntryForm();
		},

		/**
		 * Closes the reSubmit Entry Form warning and the Entry Form dialog
		 * @function onReturn
		 */
		onReturn: function () {
			var warningDialog = sap.ui.getCore().byId("warningDialog");
			//var entryFormDialog = sap.ui.getCore().byId("dialogEntryForm");
			warningDialog.close();
			warningDialog.destroy();
			_oController.closeEntryFormDialog();

		},

		/**
		 * Submits the entry form
		 * @function sendEntryForm
		 */
		sendEntryForm: function () {
			var dialog = sap.ui.getCore().byId("warningDialog");
			if (dialog) {
				dialog.close();
				dialog.destroy();
			}

			var dialogEntryForm = sap.ui.getCore().byId("dialogEntryForm");
			dialogEntryForm.setBusyIndicatorDelay(0);
			dialogEntryForm.setBusy(true);

			var entryForm = _oController.getView().getModel("oEntryFormSelected").getData();
			var entryFormJsonString = JSON.stringify(entryForm);

			var xsURL = "/sellindata/entryform";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "POST", _oController.onSendEntryFormSuccess, _oController.onSendEntryFormError,
				entryFormJsonString);
		},

		/**
		 * Function that handles the success case of the ajax call that creates a entry form and submits the same
		 * @function onSendEntryFormSuccess
		 */
		onSendEntryFormSuccess: function (oController, oData, oPiggyBack) {

			setTimeout(function () {
				_oController.DataLossWarning =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", _oController);
				//Set Warning Buttons
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.onSubmiEntryFormDialogSuccessClose);
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWcloseTXT"));
				sap.ui.getCore().byId("button_Dialog_Warning_No").setVisible(false);
				//Set Warning Message
				sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText(
					"MASDEntryFormSubmittedSuccessfully"));
				//Set Warning Dialog Title
				sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText(
					"MASDEntryFormSubmittedSuccessfullyTitle"));
				sap.ui.getCore().byId("icon_Dialog_Warning").setSrc("sap-icon://customfont/success");
				sap.ui.getCore().byId("icon_Dialog_Warning").removeStyleClass("warningMessageDialogIcon");
				sap.ui.getCore().byId("icon_Dialog_Warning").addStyleClass("successMessageDialogIcon");
				_oController.DataLossWarning.open();
				_oController._removeDialogResize(_oController.DataLossWarning);
			}, 0);

		},

		/**
		 * Function that handles the error case of the ajax call that creates a entry form and submits the same
		 * @function onSendEntryFormError
		 */
		onSendEntryFormError: function (oController, oError, oPiggyBack) {
			var dialogEntryForm = sap.ui.getCore().byId("dialogEntryForm");
			dialogEntryForm.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"MASDSubmitErrorTXT"), null);
		},

		onSubmiEntryFormDialogSuccessClose: function () {
			var dialogEntryForm = sap.ui.getCore().byId("dialogEntryForm");
			dialogEntryForm.setBusy(false);
			_oController.closeEntryFormDialog();
			_oController.closeDialog();

			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();

			_oController.getView().byId("sortButton_dealerCode").setSrc("resources/icon/sort_asc.png");
			//reset other sort icons
			_oController.getView().byId("sortButton_dealerName").setSrc("resources/icon/sort_both.png");

			_oController.getView().byId("sortButton_PrevMonthPrevYear_TotalCPRO").setSrc("resources/icon/sort_both.png");
			_oController.getView().byId("sortButton_PrevMonthPrevYear_TotalVPS").setSrc("resources/icon/sort_both.png");
			_oController.getView().byId("sortButton_PrevMonthPrevYear_Maintenance").setSrc("resources/icon/sort_both.png");

			_oController.getView().byId("sortButton_PrevMonthCurrentYear_TotalCPRO").setSrc("resources/icon/sort_both.png");
			_oController.getView().byId("sortButton_PrevMonthCurrentYear_TotalVPS").setSrc("resources/icon/sort_both.png");
			_oController.getView().byId("sortButton_PrevMonthCurrentYear_Maintenance").setSrc("resources/icon/sort_both.png");

			_oController.getView().byId("sortButton_Variation").setSrc("resources/icon/sort_both.png");

			_oController.getView().byId("sortButton_CurrentMonthCurrentYear_TotalCPRO").setSrc("resources/icon/sort_both.png");
			_oController.getView().byId("sortButton_CurrentMonthCurrentYear_TotalVPS").setSrc("resources/icon/sort_both.png");
			_oController.getView().byId("sortButton_CurrentMonthCurrentYear_Maintenance").setSrc("resources/icon/sort_both.png");

			var chart_table = _oController.getView().byId("chart_MainTable");
			chart_table.setBusyIndicatorDelay(0);
			chart_table.setBusy(true);
			_oController.getView().byId("SellinDataSubmit").setEnabled(false);
			_oController.getView().byId("searchFieldSellinData").setBusyIndicatorDelay(0);
			_oController.getView().byId("searchFieldSellinData").setBusy(true);

			_oController.loadChart();
		},

		/**
		 * Verify input fields during live change on entry form submission.
		 * @function verifyInput
		 * @param {Object} event - triggered on live change of input fields on entry form dialog
		 */
		verifyInput: function (oEvent) {
			//sets field to normal state
			oEvent.getSource().setValueState(sap.ui.core.ValueState.None);
			var bEnableSubmit = true;

			var sFieldValue = oEvent.getSource().getValue() === "" ? 0 : oEvent.getSource().getValue();
			var sTotalRepairOrderValue = sap.ui.getCore().byId(
				"entryform_field0").getValue() === "" ? 0 : sap.ui.getCore().byId("entryform_field0").getValue();

			// check if it's the total repair order (field0) or the other services (field19) field
			if (oEvent.getSource().getId().indexOf(sap.ui.getCore().byId(
					"entryform_field0").getId()) !== -1 || oEvent.getSource().getId().indexOf(sap.ui.getCore().byId(
					"entryform_field19").getId()) !== -1) {
				if (parseInt(sFieldValue, 10) < 0 || (sFieldValue.indexOf(".") > -1) || (sFieldValue.indexOf("-") > -1) || isNaN(sFieldValue) || (
						sFieldValue.length > 1 &&
						sFieldValue.charAt(0) == 0)) {
					oEvent.getSource().setValueState(sap.ui.core.ValueState.Error);
					sap.ui.getCore().byId("submitEntryForm").setEnabled(false);
					return;
				}

			} else { // the other fields need to be less than the value of total repair order
				if (parseInt(sFieldValue, 10) < 0 || (sFieldValue.indexOf(".") > -1) || (sFieldValue.indexOf("-") > -1) || isNaN(sFieldValue) ||
					parseInt(sFieldValue, 10) > parseInt(
						sTotalRepairOrderValue, 10) || (
						sFieldValue.length > 1 && sFieldValue.charAt(
							0) == 0)) {
					oEvent.getSource().setValueState(sap.ui.core.ValueState.Error);
					sap.ui.getCore().byId("submitEntryForm").setEnabled(false);
					return;
				}
			}

			var aFields = ["entryform_field0", "entryform_field1", "entryform_field2", "entryform_field3", "entryform_field4",
				"entryform_field5", "entryform_field6",
				"entryform_field7", "entryform_field8", "entryform_field9",
				"entryform_field10", "entryform_field11", "entryform_field12", "entryform_field13", "entryform_field14", "entryform_field15",
				"entryform_field16", "entryform_field17", "entryform_field18", "entryform_field19"
			];
			// as there are some fields dependencies need to reavaluate if still valid
			for (var i = 0; i < aFields.length; i++) {
				sFieldValue = sap.ui.getCore().byId(aFields[i]).getValue();
				if (aFields[i] !== "entryform_field19") {
					if (parseInt(sFieldValue, 10) < 0 || (sFieldValue.indexOf(".") > -1) || (sFieldValue.indexOf("-") > -1) || isNaN(sFieldValue) ||
						parseInt(sFieldValue, 10) >
						parseInt(
							sTotalRepairOrderValue, 10) || (
							sFieldValue.length > 1 && sFieldValue.charAt(0) == 0)) {
						sap.ui.getCore().byId(aFields[0]).setValueState(sap.ui.core.ValueState.Error);
						bEnableSubmit = false;
					} else {
						sap.ui.getCore().byId(aFields[i]).setValueState(sap.ui.core.ValueState.None);
					}
				} else if (aFields[i] === "entryform_field19") {
					if (parseInt(sFieldValue, 10) < 0 || (sFieldValue.indexOf(".") > -1) || (sFieldValue.indexOf("-") > -1) || isNaN(sFieldValue) ||
						(sFieldValue.length > 1 &&
							sFieldValue.charAt(0) == 0)) {
						bEnableSubmit = false;
					} else {
						sap.ui.getCore().byId(aFields[i]).setValueState(sap.ui.core.ValueState.None);
					}
				}
			}

			if (bEnableSubmit) {
				sap.ui.getCore().byId("submitEntryForm").setEnabled(true);
			} else {
				sap.ui.getCore().byId("submitEntryForm").setEnabled(false);
			}

		},

		/**
		 * This function is called when all the accounts are available
		 * @function setupAllAccounts
		 */
		setupAllAccounts: function () {
			var oAccounts = new JSONModel(_oView.getModel("oAccounts").getData().accounts);
			oAccounts.setSizeLimit(_oView.getModel("oAccounts").getData().accounts.length);
			_oView.setModel(oAccounts, "ShipTo");
			sap.ui.getCore().byId("SellinDataLocationList").setBusy(false);

		},

		/**
		 * Function that computes the Total row
		 * @function computeTotal
		 * */
		computeTotal: function (oData) {
			var oTotal = {
				dealerCode: "",
				dealerName: "TOTAL",
				prevYearPrevMonth: {},
				currentYearPrevMonth: {},
				variation: {},
				currentYearCurrentMonth: {}

			};

			var oFormatOptions = {
				minFractionDigits: 2,
				maxFractionDigits: 2
			};
			var oFloatFormat = sap.ui.core.format.NumberFormat.getFloatInstance(oFormatOptions);

			var bprevYearPrevMonthTotalCPROFound = false,
				bprevYearPrevMonthTotalVPSFound = false,
				bprevYearPrevMonthMaintenanceFound = false,
				bcurrentYearPrevMonthTotalCPROFound = false,
				bcurrentYearPrevMonthTotalVPSFound = false,
				bcurrentYearPrevMonthMaintenanceFound = false,
				bcurrentYearCurrentMonthTotalCPROFound = false,
				bcurrentYearCurrentMonthTotalVPSFound = false;

			oTotal.prevYearPrevMonth.totalCPRO = 0;
			oTotal.prevYearPrevMonth.totalVPS = 0;
			oTotal.currentYearPrevMonth.totalCPRO = 0;
			oTotal.currentYearPrevMonth.totalVPS = 0;
			oTotal.currentYearCurrentMonth.totalCPRO = 0;
			oTotal.currentYearCurrentMonth.totalVPS = 0;

			for (var i = 0; i < oData.length; i++) {
				//prevYearPrevMonth
				if (oData[i].prevYearPrevMonth.totalCPRO !== undefined) {
					bprevYearPrevMonthTotalCPROFound = true;
					oTotal.prevYearPrevMonth.totalCPRO += parseInt(oData[i].prevYearPrevMonth.totalCPRO, 10);
				}

				if (oData[i].prevYearPrevMonth.totalVPS !== undefined) {
					bprevYearPrevMonthTotalVPSFound = true;
					oTotal.prevYearPrevMonth.totalVPS += parseInt(oData[i].prevYearPrevMonth.totalVPS, 10);
				}

				//currentYearPrevMonth
				if (oData[i].currentYearPrevMonth.totalCPRO !== undefined) {
					bcurrentYearPrevMonthTotalCPROFound = true;
					oTotal.currentYearPrevMonth.totalCPRO += parseInt(oData[i].currentYearPrevMonth.totalCPRO, 10);
				}

				if (oData[i].currentYearPrevMonth.totalVPS !== undefined) {
					bcurrentYearPrevMonthTotalVPSFound = true;
					oTotal.currentYearPrevMonth.totalVPS += parseInt(oData[i].currentYearPrevMonth.totalVPS, 10);
				}

				//currentYearCurrentMonth
				if (oData[i].currentYearCurrentMonth.totalCPRO !== undefined) {
					bcurrentYearCurrentMonthTotalCPROFound = true;
					oTotal.currentYearCurrentMonth.totalCPRO += parseInt(oData[i].currentYearCurrentMonth.totalCPRO, 10);
				}

				if (oData[i].currentYearCurrentMonth.totalVPS !== undefined) {
					bcurrentYearCurrentMonthTotalVPSFound = true;
					oTotal.currentYearCurrentMonth.totalVPS += parseInt(oData[i].currentYearCurrentMonth.totalVPS, 10);
				}
			}

			// prevYearPrevMonth maintenance
			if (bprevYearPrevMonthTotalCPROFound === false) {
				oTotal.prevYearPrevMonth.totalCPRO = undefined;
			}

			if (bprevYearPrevMonthTotalVPSFound === false) {
				oTotal.prevYearPrevMonth.totalVPS = undefined;
			}

			if (bprevYearPrevMonthTotalCPROFound === false || bprevYearPrevMonthTotalVPSFound === false) {
				oTotal.prevYearPrevMonth.maintenance = undefined;
			} else {
				if (oTotal.prevYearPrevMonth.totalCPRO == 0) {
					oTotal.prevYearPrevMonth.maintenance = 0;
				} else {
					oTotal.prevYearPrevMonth.maintenance = (oTotal.prevYearPrevMonth.totalVPS / oTotal.prevYearPrevMonth.totalCPRO) * 100;
					bprevYearPrevMonthMaintenanceFound = true;
				}
			}

			// currentYearPrevMonth maintenance
			if (bcurrentYearPrevMonthTotalCPROFound === false) {
				oTotal.currentYearPrevMonth.totalCPRO = undefined;
			}

			if (bcurrentYearPrevMonthTotalVPSFound === false) {
				oTotal.currentYearPrevMonth.totalVPS = undefined;
			}

			if (bcurrentYearPrevMonthTotalCPROFound === false || bcurrentYearPrevMonthTotalVPSFound === false) {
				oTotal.currentYearPrevMonth.maintenance = undefined;
			} else {
				if (oTotal.currentYearPrevMonth.totalCPRO == 0) {
					oTotal.currentYearPrevMonth.maintenance = 0;
				} else {
					oTotal.currentYearPrevMonth.maintenance = (oTotal.currentYearPrevMonth.totalVPS / oTotal.currentYearPrevMonth.totalCPRO) * 100;
					bcurrentYearPrevMonthMaintenanceFound = true;
				}
			}

			// variation
			if (bprevYearPrevMonthMaintenanceFound === false || bcurrentYearPrevMonthMaintenanceFound === false) {
				oTotal.variation.difference = undefined;
			} else {
				oTotal.variation.difference = _oController.deCommaFyFloats(oFloatFormat.format(oTotal.currentYearPrevMonth.maintenance)) -
					_oController.deCommaFyFloats(
						oFloatFormat.format(oTotal.prevYearPrevMonth
							.maintenance));
			}

			//currentYearCurrentMonth
			if (bcurrentYearCurrentMonthTotalCPROFound === false) {
				oTotal.currentYearCurrentMonth.totalCPRO = undefined;
			}

			if (bcurrentYearCurrentMonthTotalVPSFound === false) {
				oTotal.currentYearCurrentMonth.totalVPS = undefined;
			}

			if (bcurrentYearCurrentMonthTotalCPROFound === false || bcurrentYearCurrentMonthTotalVPSFound === false) {
				oTotal.currentYearCurrentMonth.maintenance = undefined;
			} else {
				if (oTotal.currentYearCurrentMonth.totalCPRO == 0) {
					oTotal.currentYearCurrentMonth.maintenance = 0;
				} else {
					oTotal.currentYearCurrentMonth.maintenance = (oTotal.currentYearCurrentMonth.totalVPS / oTotal.currentYearCurrentMonth.totalCPRO) *
						100;
				}
			}

			return oTotal;
		},

		/**
		 * Function to enabled the "touch" properties of the searchfield
		 * @function searchFieldEnabled
		 */
		searchFieldEnabled: function () {
			this.getView().addEventDelegate({
				onAfterShow: function () {
					var inputID = _oController.getView().createId("searchFieldSellinData");
					$("#" + inputID + "-I").attr("readonly", false);
				}
			});
		},

		/**
		 * Filters the table
		 * @function onLiveSearchSellinData
		 * @param {Object} oEvent - Search field input
		 */
		onLiveSearchSellinData: function (oEvent) {
			// Get value of search field
			var newValue = oEvent.getParameter("newValue");
			var oChart = _oView.getModel("oChart");
			var aSplitInnerColumn;
			var obj = oChart.getData();

			if (obj === null || obj === undefined || obj === "") {
				return;
			} else {
				obj.pop();
			}

			if (!newValue) {
				var oDataIntact = _oModelChartIntact.getData();
				if (_sSortedLabel === "dealerName" || _sSortedLabel === "dealerCode") {
					this.onFilteroModel(oDataIntact, _sSortedOrder, _sSortedLabel);
				} else {
					aSplitInnerColumn = _sSortedLabel.split(".");
					if (aSplitInnerColumn[1] === "difference" || aSplitInnerColumn[1] === "maintenance") {
						this.onFilteroModelMultiFloat(oDataIntact, _sSortedOrder, aSplitInnerColumn[0], aSplitInnerColumn[1]);
					} else {
						this.onFilteroModelMulti(oDataIntact, _sSortedOrder, aSplitInnerColumn[0], aSplitInnerColumn[1]);
					}
				}

				oDataIntact.push(this.computeTotal(oDataIntact));
				oChart.setData(oDataIntact);
			} else {
				// Filtering the oModel
				var oDataFiltered = this.JSONFilter(newValue, _oModelChartIntact);
				if (_sSortedLabel === "dealerName" || _sSortedLabel === "dealerCode") {
					this.onFilteroModel(oDataFiltered, _sSortedOrder, _sSortedLabel);
				} else {
					aSplitInnerColumn = _sSortedLabel.split(".");
					if (aSplitInnerColumn[1] === "difference" || aSplitInnerColumn[1] === "maintenance") {
						this.onFilteroModelMultiFloat(oDataFiltered, _sSortedOrder, aSplitInnerColumn[0], aSplitInnerColumn[1]);
					} else {
						this.onFilteroModelMulti(oDataFiltered, _sSortedOrder, aSplitInnerColumn[0], aSplitInnerColumn[1]);
					}
				}
				if (oDataFiltered.length) {
					oDataFiltered.push(this.computeTotal(oDataFiltered));
				}
				oChart.setData(oDataFiltered);
			}
		},

		/**
		 * JSONFilter function that receives the oData and filters
		 * @ parameter (filterValue, oModel)
		 */
		JSONFilter: function (filterValue, oModel) {
			// DataJSON original and filtered
			var oDataJSON = oModel.getData();
			var oDataJSONFiltered = [];
			for (var i = 0; i < oDataJSON.length; i++) {
				// JSON Parameters to be filtered
				var dealerName = "";
				var dealerCode = "";

				if (oDataJSON[i].dealerName) {
					dealerName = oDataJSON[i].dealerName.toLowerCase();
				}
				if (oDataJSON[i].dealerCode) {
					dealerCode = oDataJSON[i].dealerCode.toLowerCase();
				}

				if (dealerName.indexOf(filterValue.toLowerCase()) !== -1 || dealerCode.indexOf(filterValue.toLowerCase()) !== -1) {
					oDataJSONFiltered.push(oDataJSON[i]);
				}
			}
			return oDataJSONFiltered;
		},

		/**
		 * Ordering Algorithm for Dealer Code and Dealer Name
		 @parameter (oData,orderMode,objectName)
		 */
		onFilteroModel: function (oData, sOrderMode, sObjectName) {
			oData.sort(function (a, b) {
				switch (sOrderMode) {
				case "ascending":
					if ((a.valueOf()[sObjectName] === undefined || a.valueOf()[sObjectName] === null || a.valueOf()[sObjectName] === "") && (b.valueOf()[
							sObjectName] !== undefined && b.valueOf()[sObjectName] !== null && b.valueOf()[sObjectName] !== "")) {
						return 1;
					} else if ((a.valueOf()[sObjectName] !== undefined && a.valueOf()[sObjectName] !== null && a.valueOf()[sObjectName] !== "") &&
						(b
							.valueOf()[sObjectName] === undefined || b.valueOf()[sObjectName] === null || b.valueOf()[sObjectName] === "")) {
						return -1;
					} else if (a.valueOf()[sObjectName] === undefined && b.valueOf()[sObjectName] === "") {
						return -1;
					} else if (a.valueOf()[sObjectName] === "" && b.valueOf()[sObjectName] === undefined) {
						return 1;
					} else if (a.valueOf()[sObjectName] === undefined && b.valueOf()[sObjectName] === undefined) {
						return 0;
					} else if (a.valueOf()[sObjectName].toLowerCase() < b.valueOf()[sObjectName].toLowerCase()) {
						return -1;
					} else if (a.valueOf()[sObjectName].toLowerCase() > b.valueOf()[sObjectName].toLowerCase()) {
						return 1;
					} else {
						return 0;
					}
					break;
				case "descending":
					if ((a.valueOf()[sObjectName] === undefined || a.valueOf()[sObjectName] === null || a.valueOf()[sObjectName] === "") && (b.valueOf()[
							sObjectName] !== undefined && b.valueOf()[sObjectName] !== null && b.valueOf()[sObjectName] !== "")) {
						return 1;
					} else if ((a.valueOf()[sObjectName] !== undefined && a.valueOf()[sObjectName] !== null && a.valueOf()[sObjectName] !== "") &&
						(b
							.valueOf()[sObjectName] === undefined || b.valueOf()[sObjectName] === null || b.valueOf()[sObjectName] === "")) {
						return -1;
					} else if (a.valueOf()[sObjectName] === undefined && b.valueOf()[sObjectName] === "") {
						return -1;
					} else if (a.valueOf()[sObjectName] === "" && b.valueOf()[sObjectName] === undefined) {
						return 1;
					} else if (a.valueOf()[sObjectName] === undefined && b.valueOf()[sObjectName] === undefined) {
						return 0;
					} else if (a.valueOf()[sObjectName].toLowerCase() < b.valueOf()[sObjectName].toLowerCase()) {
						return 1;
					} else if (a.valueOf()[sObjectName].toLowerCase() > b.valueOf()[sObjectName].toLowerCase()) {
						return -1;
					} else {
						return 0;
					}
					break;
				default:
					break;
				}
			});
		},

		/**
		 * Ordering Algorithm for MultiHeader Columns (Float Columns)
		 @parameter (oData,orderMode,sObjectName, sInnerObjetName)
		 */
		onFilteroModelMultiFloat: function (oData, sOrderMode, sObjectName, sInnerObjectName) {
			oData.sort(function (a, b) {
				switch (sOrderMode) {
				case "ascending":
					if ((a.valueOf()[sObjectName][sInnerObjectName] === undefined || a.valueOf()[sObjectName][sInnerObjectName] === null || a.valueOf()[
							sObjectName][sInnerObjectName] === "") && (b.valueOf()[
							sObjectName][sInnerObjectName] !== undefined || b.valueOf()[sObjectName][sInnerObjectName] !== null || b.valueOf()[
							sObjectName][sInnerObjectName] !== "")) {
						return 1;
					} else if ((a.valueOf()[sObjectName][sInnerObjectName] !== undefined || a.valueOf()[sObjectName][sInnerObjectName] !== null ||
							a.valueOf()[sObjectName][sInnerObjectName] !== "") &&
						(b
							.valueOf()[sObjectName][sInnerObjectName] === undefined || b.valueOf()[sObjectName][sInnerObjectName] === null || b.valueOf()[
								sObjectName][sInnerObjectName] === "")) {
						return -1;
					} else if (parseFloat(a.valueOf()[sObjectName][sInnerObjectName]) < parseFloat(b.valueOf()[sObjectName][sInnerObjectName])) {
						return -1;
					} else if (parseFloat(a.valueOf()[sObjectName][sInnerObjectName]) > parseFloat(b.valueOf()[sObjectName][sInnerObjectName])) {
						return 1;
					} else {
						return 0;
					}
					break;
				case "descending":
					if ((a.valueOf()[sObjectName][sInnerObjectName] === undefined || a.valueOf()[sObjectName][sInnerObjectName] === null || a.valueOf()[
							sObjectName][sInnerObjectName] === "") && (b.valueOf()[
							sObjectName][sInnerObjectName] !== undefined || b.valueOf()[sObjectName][sInnerObjectName] !== null || b.valueOf()[
							sObjectName][sInnerObjectName] !== "")) {
						return 1;
					} else if ((a.valueOf()[sObjectName][sInnerObjectName] !== undefined || a.valueOf()[sObjectName][sInnerObjectName] !== null ||
							a.valueOf()[sObjectName][sInnerObjectName] !== "") &&
						(b
							.valueOf()[sObjectName][sInnerObjectName] === undefined || b.valueOf()[sObjectName][sInnerObjectName] === null || b.valueOf()[
								sObjectName][sInnerObjectName] === "")) {
						return -1;
					} else if (parseFloat(a.valueOf()[sObjectName][sInnerObjectName]) < parseFloat(b.valueOf()[sObjectName][sInnerObjectName])) {
						return 1;
					} else if (parseFloat(a.valueOf()[sObjectName][sInnerObjectName]) > parseFloat(b.valueOf()[sObjectName][sInnerObjectName])) {
						return -1;
					} else {
						return 0;
					}
					break;
				default:
					break;
				}
			});
		},

		/**
		 * Ordering Algorithm for MultiHeader Columns (Integer Columns)
		 @parameter (oData,orderMode,sObjectName, sInnerObjetName)
		 */
		onFilteroModelMulti: function (oData, sOrderMode, sObjectName, sInnerObjectName) {
			oData.sort(function (a, b) {
				switch (sOrderMode) {
				case "ascending":
					if ((a.valueOf()[sObjectName][sInnerObjectName] === undefined || a.valueOf()[sObjectName][sInnerObjectName] === null || a.valueOf()[
							sObjectName][sInnerObjectName] === "") && (b.valueOf()[
							sObjectName][sInnerObjectName] !== undefined || b.valueOf()[sObjectName][sInnerObjectName] !== null || b.valueOf()[
							sObjectName][sInnerObjectName] !== "")) {
						return 1;
					} else if ((a.valueOf()[sObjectName][sInnerObjectName] !== undefined || a.valueOf()[sObjectName][sInnerObjectName] !== null ||
							a.valueOf()[sObjectName][sInnerObjectName] !== "") &&
						(b
							.valueOf()[sObjectName][sInnerObjectName] === undefined || b.valueOf()[sObjectName][sInnerObjectName] === null || b.valueOf()[
								sObjectName][sInnerObjectName] === "")) {
						return -1;
					} else if (parseInt(a.valueOf()[sObjectName][sInnerObjectName], 10) < parseInt(b.valueOf()[sObjectName][sInnerObjectName],
							10)) {
						return -1;
					} else if (parseInt(a.valueOf()[sObjectName][sInnerObjectName], 10) > parseInt(b.valueOf()[sObjectName][sInnerObjectName],
							10)) {
						return 1;
					} else {
						return 0;
					}
					break;
				case "descending":
					if ((a.valueOf()[sObjectName][sInnerObjectName] === undefined || a.valueOf()[sObjectName][sInnerObjectName] === null || a.valueOf()[
							sObjectName][sInnerObjectName] === "") && (b.valueOf()[
							sObjectName][sInnerObjectName] !== undefined || b.valueOf()[sObjectName][sInnerObjectName] !== null || b.valueOf()[
							sObjectName][sInnerObjectName] !== "")) {
						return 1;
					} else if ((a.valueOf()[sObjectName][sInnerObjectName] !== undefined || a.valueOf()[sObjectName][sInnerObjectName] !== null ||
							a.valueOf()[sObjectName][sInnerObjectName] !== "") &&
						(b
							.valueOf()[sObjectName][sInnerObjectName] === undefined || b.valueOf()[sObjectName][sInnerObjectName] === null || b.valueOf()[
								sObjectName][sInnerObjectName] === "")) {
						return -1;
					} else if (parseInt(a.valueOf()[sObjectName][sInnerObjectName], 10) < parseInt(b.valueOf()[sObjectName][sInnerObjectName],
							10)) {
						return 1;
					} else if (parseInt(a.valueOf()[sObjectName][sInnerObjectName], 10) > parseInt(b.valueOf()[sObjectName][sInnerObjectName],
							10)) {
						return -1;
					} else {
						return 0;
					}
					break;
				default:
					break;
				}
			});
		},

		/**
		 * Sorting Table Column - No Multi Header (Dealer Name / Dealer Code)
		 * @ (oEvent)
		 */
		onSortingColumnNoMultiHeader: function (oEvent) {
			var icon = oEvent.getSource(),
				iconName = icon.getSrc().split("sort_")[1],
				orderState = iconName.replace('.png', ''),
				oFilterVariable = oEvent.getSource().data("sortlbl");
			_sSortedLabel = oEvent.getSource().data("sortlbl");

			// other icons to sort_both
			this.getView().byId("sortButton_dealerCode").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_dealerName").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthPrevYear_TotalCPRO").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthPrevYear_TotalVPS").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthPrevYear_Maintenance").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthCurrentYear_TotalCPRO").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthCurrentYear_TotalVPS").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthCurrentYear_Maintenance").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_Variation").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_CurrentMonthCurrentYear_TotalCPRO").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_CurrentMonthCurrentYear_TotalVPS").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_CurrentMonthCurrentYear_Maintenance").setSrc("resources/icon/sort_both.png");

			var obj = _oView.getModel("oChart").getData();
			if (obj.length) {
				obj.pop();
			}
			if (orderState === "asc" || orderState === "both") {
				orderState = "desc";
				_sSortedOrder = "descending";
				this.onFilteroModel(_oModelChart.getData(), "descending", oFilterVariable);

			} else if (orderState === "desc") {
				orderState = "asc";
				_sSortedOrder = "ascending";
				this.onFilteroModel(_oModelChart.getData(), "ascending", oFilterVariable);

			} else {
				return;
			}

			if (obj.length) {
				obj.push(this.computeTotal(obj));
			}
			_oModelChart.setData(obj);
			_oView.getModel("oChart").updateBindings();
			icon.setSrc("resources/icon/sort_" + orderState + ".png");
		},

		/**
		 * Sorting Table Column Multi Header
		 * @ (oEvent)
		 */
		onSortingColumnMultiHeader: function (oEvent) {
			var icon = oEvent.getSource(),
				iconName = icon.getSrc().split("sort_")[1],
				orderState = iconName.replace('.png', '');
			_sSortedLabel = oEvent.getSource().data("sortlbl");
			//example of label: PrevYearPrevMonth.totalCPRO
			var oFilterVariable = _sSortedLabel.split(".")[0],
				oInnerVariable = _sSortedLabel.split(".")[1];

			// other icons to sort_both
			this.getView().byId("sortButton_dealerCode").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_dealerName").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthPrevYear_TotalCPRO").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthPrevYear_TotalVPS").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthPrevYear_Maintenance").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthCurrentYear_TotalCPRO").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthCurrentYear_TotalVPS").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_PrevMonthCurrentYear_Maintenance").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_Variation").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_CurrentMonthCurrentYear_TotalCPRO").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_CurrentMonthCurrentYear_TotalVPS").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_CurrentMonthCurrentYear_Maintenance").setSrc("resources/icon/sort_both.png");

			var obj = _oView.getModel("oChart").getData();
			if (obj.length) {
				obj.pop();
			}
			if (orderState === "asc" || orderState === "both") {
				orderState = "desc";
				_sSortedOrder = "descending";
				if (oInnerVariable === "maintenance" || oInnerVariable === "difference") {
					this.onFilteroModelMultiFloat(_oModelChart.getData(), "descending", oFilterVariable, oInnerVariable);
				} else {
					this.onFilteroModelMulti(_oModelChart.getData(), "descending", oFilterVariable, oInnerVariable);
				}

			} else if (orderState === "desc") {
				orderState = "asc";
				_sSortedOrder = "ascending";
				if (oInnerVariable === "maintenance" || oInnerVariable === "difference") {
					this.onFilteroModelMultiFloat(_oModelChart.getData(), "ascending", oFilterVariable, oInnerVariable);
				} else {
					this.onFilteroModelMulti(_oModelChart.getData(), "ascending", oFilterVariable, oInnerVariable);
				}
			} else {
				return;
			}

			if (obj.length) {
				obj.push(this.computeTotal(obj));
			}
			_oModelChart.setData(obj);
			_oView.getModel("oChart").updateBindings();
			icon.setSrc("resources/icon/sort_" + orderState + ".png");
		},

		/**
		 * Formats Column Multi Header
		 * @ (sHeader)
		 */
		formatHeaderMulti: function (sHeader) {
			if (sHeader !== undefined) {
				var iMonth = sHeader.split("-")[0];
				var iYear = sHeader.split("-")[1];
				return _oController.monthName(iMonth) + " " + iYear;
			} else {
				return this.getResourceBundle().getText("UMNA");
			}
		},

		/**
		 * Formats Column Variation
		 * @ (iMonth)
		 */
		formatHeaderVariation: function (iMonth) {
			if (iMonth !== undefined) {
				return _oController.monthName(iMonth);
			} else {
				return this.getResourceBundle().getText("UMNA");
			}
		},

		/**
		 * Formats the percentage
		 * @ (sValue)
		 */
		formatPercentage: function (sValue) {
			if (sValue === undefined || sValue === null) {
				return this.getResourceBundle().getText("UMNA");
			} else {
				var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
				var oFormatOptions = {
					minFractionDigits: 2,
					maxFractionDigits: 2
				};
				var oFloatFormat = sap.ui.core.format.NumberFormat.getFloatInstance(oFormatOptions, oLocale);
				return oFloatFormat.format(sValue) + "%";
			}
		},

		/**
		 * Formats the Location
		 * @ (sDealerCode, sDealerName)
		 */
		formatLocation: function (sDealerCode, sDealerName) {
			if (sDealerCode === undefined || sDealerCode === null) {
				return sDealerName;
			} else {
				return sDealerCode + " - " + sDealerName;
			}
		},

		/**
		 * Formats the Dealer Name
		 * @ (sDealerName)
		 */

		formatDealerName: function (sDealerName) {
			var oTable, oTableCells, inputTableId;
			if (sDealerName === "TOTAL") {
				oTable = _oController.getView().byId("chart_MainTable");
				//logic implemented due to a firefox render issue, which was rendering after aplying the bodyTotalSellinData class, resulting in the loss of the class applied.
				var totalClassApply = function () {
					oTableCells = oTable.getRows()[oTable.getRows().length - 1].getAggregation("cells");
					inputTableId = oTableCells[1].getId();

					if (oTableCells[1].getText() === _oController.getResourceBundle().getText("MASDTotal")) {
						if (!$("#" + inputTableId).hasClass("bodyTotalSellinData")) {

							$("#" + inputTableId).removeClass("bodyCopy2SellinData");
							$("#" + inputTableId).addClass("bodyTotalSellinData");
							setTimeout(totalClassApply, 0);
						}
					}
				};

				setTimeout(function () {
					oTableCells = oTable.getRows()[oTable.getRows().length - 1].getAggregation("cells");
					if (oTableCells[1].getText() === _oController.getResourceBundle().getText("MASDTotal")) {
						inputTableId = oTableCells[1].getId();
					}
					setTimeout(totalClassApply, 0);
				}, 0);

				return this.getResourceBundle().getText("MASDTotal");
			} else {

				setTimeout(function () {
					oTable = _oController.getView().byId("chart_MainTable");
					oTableCells = oTable.getRows()[oTable.getRows().length - 1].getAggregation("cells");
					if (oTableCells[1].getText() !== _oController.getResourceBundle().getText("MASDTotal")) {
						inputTableId = oTableCells[1].getId();
						$("#" + inputTableId).addClass("bodyCopy2SellinData");
						$("#" + inputTableId).removeClass("bodyTotalSellinData");
					}
				}, 0);
				return sDealerName;
			}

		},

		/** Formats the color of the maintenance percentage number.
		 * Green = Success
		 * Yellow = Warning
		 * Red = Error
		 * 
		 * @function formatPercentageStatusColor
		 */
		formatMaintenancePercentageStatusColor: function (sValue) {
			if (sValue === undefined || sValue === null) {
				return "None";
			} else {
				if (sValue >= 22.0) {
					return "Success";
				} else if (sValue >= 10.0 && sValue < 22.0) {
					return "Warning";
				} else {
					return "Error";
				}
			}
		},

		/** Formats the color of the variation percentage number.
		 * Green = Success
		 * Yellow = Warning
		 * Red = Error
		 * 
		 * @function formatVariationPercentageStatusColor
		 */
		formatVariationPercentageStatusColor: function (sValue) {
			if (sValue === undefined || sValue === null) {
				return "None";
			} else {
				if (sValue >= 5.0) {
					return "Success";
				} else if (sValue >= 0.0 && sValue < 5.0) {
					return "Warning";
				} else {
					return "Error";
				}
			}
		},

		/** Formats the variation header
		 * @function formatVariation
		 */
		formatVariation: function (sPrevYear, sCurrentYear) {
			if (sPrevYear === undefined && sCurrentYear === undefined) {
				return this.getResourceBundle().getText("MASDTemplateCol7");
			} else {
				return this.getResourceBundle().getText("MASDTemplateCol6", [sPrevYear, sCurrentYear]);
			}
		},

		/**
		 * Checks if the value of the column is undefined, if so returns N/A, otherwise returns the value formatted
		 * 
		 * @function formatChartNumber
		 **/
		formatChartNumber: function (sValue) {
			if (sValue === undefined || sValue === null) {
				return this.getResourceBundle().getText("UMNA");
			} else {
				return this.formatValuesGroupingSeparator(sValue);
			}
		},

		/**
		 * Formats the number of rows of the table.
		 * 
		 * @function formatTableRowNumber
		 */

		formatTableRowNumber: function (chart) {
			if (chart === undefined || chart.length === 0) {
				return 1;
			}
			if (chart.length >= 9) {
				return 9;
			} else {
				return chart.length;
			}
		},

		/**
		 * Returns date in the local format
		 * @function formatDate
		 * @param {string} [sDate] - A string with a date info
		 * @returns Formatted Date
		 */
		formatDate: function (sDate) {
			if (!sDate) {
				return "";
			} else {
				var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
				var slocaleID = ((oLocale.sLocaleId === "en") ? "en-US" : oLocale.sLocaleId);

				var day = parseInt(sDate.split("-")[2], 10);
				var month = parseInt(sDate.split("-")[1], 10) - 1;
				var year = sDate.split("-")[0];

				var timestamp = new Date(year, month, day);
				var datestring = timestamp.toLocaleDateString(slocaleID);

				return datestring;
			}
		},

		/**
		 * Returns the month name given a number.
		 * 
		 * @function monthName
		 * 
		 */
		monthName: function (iMonth) {
			return [this.getResourceBundle().getText("INGTSText12LK"), this.getResourceBundle().getText("INGTSText11LK"), this.getResourceBundle()
				.getText("INGTSText10LK"), this.getResourceBundle().getText("INGTSText9LK"), this.getResourceBundle().getText("INGTSText8LK"),
				this.getResourceBundle().getText("INGTSText7LK"), this.getResourceBundle().getText("INGTSText6LK"), this.getResourceBundle().getText(
					"INGTSText5LK"), this.getResourceBundle().getText("INGTSText4LK"), this.getResourceBundle().getText("INGTSText3LK"), this.getResourceBundle()
				.getText("INGTSText2LK"), this.getResourceBundle().getText("INGTSText1LK")
			][iMonth - 1];
		}

	});
});