sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter"
], function(Base, JSONModel, Filter) {
	"use strict";
	var _oRouter,
		_oController,
		_oView,
		_oGoalSettings,
		_oGoalSettingsChanges;
	var _hasChanged = false;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.MyAccount.MyAccountGoalSetting", {

		/** @module MyAccountGoalSetting */
		/**
		 * This function initializes the controller and sets the selectedAccount model.
		 * @function onInit
		 */
		onInit: function() {
			Base.prototype.onInit.call(this);
			_oController = this;
			_oView = this.getView();
			_oRouter = this.getRouter();
			_oRouter.getRoute("myAccountGoalSetting").attachPatternMatched(this._onObjectMatched, this);

			this.onDataLossPreventingBrowserAlert();

			this.getView().byId("multiheaderGallons").setHeaderSpan([3, 1]);
			this.getView().byId("multiheaderPercentage").setHeaderSpan([3, 1]);
			this.getView().byId("multiheaderVPSUnits").setHeaderSpan([3, 1]);
		},

		/**
		 * Loads user data information
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function() {
			// Clears all error message
			this.clearServerErrorMsg(this);

			//set the navbar icon as selected
			this.resetMenuButtons();

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("GOAL_PAGE")) {
				return;
			}
			
			//get user info and direct account
			this.getUserInformation(this);

			this.headerIconAccess(); 
			
			//check Language and set selected Key in combo box
			this.onChangeLanguage();
			
			//Shopping Cart Values
			this.getShoppingCartCookie(this);
			
			
			//Data Loss prevention
			if (_hasChanged) {
				setTimeout(function() {
					_oController.DataLossWarning =
						new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", _oController);
					//Set Warning Buttons
					sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.onRefreshDialog_Refresh);
					sap.ui.getCore().byId("button_Dialog_Warning_No").attachPress(_oController.onRefreshDialog_Close);
					sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWreloadTXT"));
					sap.ui.getCore().byId("button_Dialog_Warning_No").setText(_oController.getResourceBundle().getText("DLWeditTXT"));
					//Set Warning Message
					sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWSubmitTitle"));
					sap.ui.getCore().byId("text2_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWSubmtitDescription"));
					//Set Warnig Dialog Title
					sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWMainTitle"));
					_oController.DataLossWarning.open();
					_oController._removeDialogResize(_oController.DataLossWarning);

				}, 0);
			} else {
				this.loadGoalSettingData();
			}
		},

		/**
		 * On Close Dialog Warning Messages
		 */
		onRefreshDialog_Close: function() {
			if (_oController.DataLossWarning) {
				_oController.DataLossWarning.close();
				_oController.DataLossWarning.destroyContent();
				_oController.DataLossWarning.destroy();
			}
			_oController.getView().getModel("oGoalSettings").updateBindings(true);
			var table = _oController.getView().byId("TreeTableGoalSetting");
			if (table.getBinding() !== undefined && table.getBinding().getLength() < 10) {
				table.setVisibleRowCount(table.getBinding().getLength());
			} else {
				table.setVisibleRowCount(10);
			}
		},

		/**
		 * On Refresh Data on Dialog Warning Messages
		 */
		onRefreshDialog_Refresh: function() {
			if (_oController.DataLossWarning) {
				_oController.DataLossWarning.close();
				_oController.DataLossWarning.destroyContent();
				_oController.DataLossWarning.destroy();
			}
			// Clear Edited Input Values and reload Data
			_oController.loadGoalSettingData();
			_hasChanged = false;
		},

		/**
		 * onDataLossPreventingBrowserAlert
		 * Sets a browser alert that prevents closing if there is values to submit
		 */
		onDataLossPreventingBrowserAlert: function() {
			// Runs when the user tries to close the "goalSettings" page
			$(window).bind('beforeunload', function() {
				var route = sap.ui.core.routing.HashChanger.getInstance().getHash();
				if (_hasChanged === true && route === "myAccount/goalSetting") {
					return true;
				}
			});
		},

		/**
		 * Function that handles the success case of the ajax call to submit Claims
		 * @function onSaveSubmitClaimSuccess
		 */
		onLoadGoalSettingDataSuccess: function(oController, oData, oPiggyBack) {
			var goalSettingTable = oController.getView().byId("TreeTableGoalSetting");
			// Save Model
			_oGoalSettings = new sap.ui.model.json.JSONModel({
				" ": "",
				"corporateGoals": {
					"set": {
						"corporateTotal": "",
						"corporatePrcnt": "",
						"corporateTotalVPS": ""
					}
				},
				"globalSettings": {
					"descendants": {}
				}
			});
			_oGoalSettingsChanges = new sap.ui.model.json.JSONModel({
				" ": "",
				"corporateGoals": {
					"set": {}
				},
				"globalSettings": {
					"descendants": {}
				}
			});
			if (oData[0].corporateTotal !== undefined) {
				_oGoalSettings.getData().corporateGoals.set.corporateTotal = oData[0].corporateTotal;
			}
			if (oData[0].corporatePrcnt !== undefined) {
				_oGoalSettings.getData().corporateGoals.set.corporatePrcnt = oData[0].corporatePrcnt;
			}
			if (oData[0].corporateTotalVPS !== undefined) {
				_oGoalSettings.getData().corporateGoals.set.corporateTotalVPS = oData[0].corporateTotalVPS;
			}
			if (_oView.getModel("oAccounts").getData().level !== undefined) {
				_oGoalSettings.getData().lvl = _oView.getModel("oAccounts").getData().level;
			}

			_oGoalSettings.setProperty("/globalSettings/descendants", JSON.parse(JSON.stringify(oData)));
			oData = oController.cleanGoalChangesModel(oData);
			_oGoalSettingsChanges.setProperty("/globalSettings/descendants", JSON.parse(JSON.stringify(oData)));
			oController.getView().setModel(_oGoalSettingsChanges, "oGoalSettingsChanges");
			oController.getView().setModel(_oGoalSettings, "oGoalSettings");
			setTimeout(function() {
				goalSettingTable.setBusy(false);
				oController.getView().byId("corporateGoalTable").setBusy(false);
			}, 0);
		},

		/**
		 * Function that handles the error case of the ajax call to submit Claims
		 * @function onSaveSubmitClaimError
		 */
		onLoadGoalSettingDataError: function(oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText);
			oController.getView().byId("corporateGoalTable").setBusy(false);
			oController.getView().byId("TreeTableGoalSetting").setBusy(false);
		},

		/**
		 * Load Accounts and goal settings data
		 * @function loadGoalSettingData
		 */
		loadGoalSettingData: function() {
			var goalSettingTable = this.getView().byId("TreeTableGoalSetting");
			goalSettingTable.setBusyIndicatorDelay(0);
			goalSettingTable.setBusy(true);
			_oController.getView().byId("corporateGoalTableContainer").setVisible(false);
			_oController.getView().byId("corporateGoalTable").setBusyIndicatorDelay(0);
			_oController.getView().byId("corporateGoalTable").setBusy(true);
			//Get last year Values
			var currYear = new Date().getFullYear();
			var xsURL = "/goalsetting/accountyear?year=" + currYear;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onLoadGoalSettingDataSuccess, _oController.onLoadGoalSettingDataError);
		},

		/**
		 * Sets the max shown rows, based on treeBinding
		 * @function onCheckInputFirstname
		 */
		setVisibleRows: function(oEvent) {
			if (oEvent.getSource().getBinding().getLength() !== undefined && oEvent.getSource().getBinding().getLength() < 10) {
				oEvent.getSource().setVisibleRowCount(oEvent.getSource().getBinding().getLength());
			} else {
				oEvent.getSource().setVisibleRowCount(10);
			}
		},

		/**
		 * Fired by inputs on the table values for the % column
		 * @function onLiveChangePrcnt
		 */
		onLiveChangePrcnt: function(oEvt) {
			var input = oEvt.getParameter("newValue").toString();
			var lastValue = oEvt.getSource()._lastValue;
			var row = oEvt.getSource().getBinding("value").getContext().getPath();
			var column = oEvt.getSource().getBinding("value").getPath();

			if (input !== "") {
				input = input.replace(/[^0-9]/g, "");
			}
			if (input > 100) {
				if (parseInt(input.toString().substring(0, 3)) === 100) {
					input = parseInt(input.toString().substring(0, 3));
				} else {
					input = parseInt(input.toString().substring(0, 2));
				}
			}
			//Model to send to the database, without formatting
			_oController.getView().getModel("oGoalSettingsChanges").setProperty(row + "/" + column, input);
			input = _oController.commaFy(input);
			_oController.getView().getModel("oGoalSettings").setProperty(row + "/" + column, input);
			oEvt.getSource().setValue(input);
			if (lastValue !== input) {
				_hasChanged = true;
			}
			_oController.checkSubmit();
		},

		/**
		 * Fired by inputs on the table values
		 * @function onLiveChange
		 */
		onLiveChange: function(oEvt) {
			var input = oEvt.getParameter("newValue").toString();
			var lastValue = oEvt.getSource()._lastValue;
			var row = oEvt.getSource().getBinding("value").getContext().getPath();
			var column = oEvt.getSource().getBinding("value").getPath();

			if (input !== "") {
				input = input.replace(/[^0-9]/g, "");
			}
			//Model to send to the database, without formatting
			_oController.getView().getModel("oGoalSettingsChanges").setProperty(row + "/" + column, input);

			input = _oController.commaFy(input);
			_oController.getView().getModel("oGoalSettings").setProperty(row + "/" + column, input);
			oEvt.getSource().setValue(input);
			if (lastValue !== input) {
				_hasChanged = true;
			}
			_oController.checkSubmit();
		},

		/**
		 * Removes goal Values from the GoalSettings Structure
		 * @function cleanGoalChangesModel
		 */
		cleanGoalChangesModel: function(descendants) {
			if (descendants !== undefined && descendants.length > 0) {
				for (var i = 0; i < descendants.length; i++) {
					if (descendants[i].storeTotal !== undefined) {
						delete descendants[i].storeTotal;
					}
					if (descendants[i].storeTotalVPS !== undefined) {
						delete descendants[i].storeTotalVPS;
					}
					if (descendants[i].storePrcnt !== undefined) {
						delete descendants[i].storePrcnt;
					}
					if (descendants[i].districtTotal !== undefined) {
						delete descendants[i].districtTotal;
					}
					if (descendants[i].districtTotalVPS !== undefined) {
						delete descendants[i].districtTotalVPS;
					}
					if (descendants[i].districtPrcnt !== undefined) {
						delete descendants[i].districtPrcnt;
					}
					if (descendants[i].regionTotal !== undefined) {
						delete descendants[i].regionTotal;
					}
					if (descendants[i].regionTotalVPS !== undefined) {
						delete descendants[i].regionTotalVPS;
					}
					if (descendants[i].regionPrcnt !== undefined) {
						delete descendants[i].regionPrcnt;
					}
					if (descendants[i].corporateTotal !== undefined) {
						delete descendants[i].corporateTotal;
					}
					if (descendants[i].corporateTotalVPS !== undefined) {
						delete descendants[i].corporateTotalVPS;
					}
					if (descendants[i].corporatePrcnt !== undefined) {
						delete descendants[i].corporatePrcnt;
					}
					if (descendants[i].descendants !== undefined) {
						descendants[i].descendants = _oController.cleanGoalChangesModel(descendants[i].descendants);
					}
				}
			}
			return descendants;
		},

		/**
		 * Account Name to present
		 * @function formatterAccountName
		 */
		formatterAccountName: function(accName, accDistrict) {
			var formtText;
			if (accDistrict !== undefined && accDistrict !== "") {
				formtText = accName + " - " + accDistrict;
			} else {
				formtText = accName;
			}
			return formtText;
		},

		/**
		 * Sets the max shown rows, based on treeBinding
		 * @function formatTableRowNumber
		 */
		formatTableRowNumber: function() {
			var table = this.getView().byId("TreeTableGoalSetting");
			if (table.getBinding() !== undefined && table.getBinding().getLength() < 10) {
				return table.getBinding().getLength();
			} else {
				var maxRowsToShow = 10;
				return maxRowsToShow;
			}
		},

		/**
		 * Function that handles the success case of the ajax call that submits the data
		 * @function onSubmitSuccess
		 */
		onSubmitSuccess: function(oController, oData, oPiggyBack) {
			// Success message
			var message = oController.getResourceBundle().getText("GSSubmitedSuccTXT");
			var type = "Success";
			oController.onShowMessage(_oController, type, null, message);
			_oView.byId("goalSettingSubmit").setBusy(false);
			_oView.byId("goalSettingSubmit").setEnabled(false);
			_hasChanged = false;

		},

		/**
		 * Function that handles the error case of the ajax call that submits the data
		 * @function onSubmitError
		 */
		onSubmitError: function(oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText);
			_oView.byId("goalSettingSubmit").setBusy(false);
			_oView.byId("goalSettingSubmit").setEnabled(false);
		},

		/**
		 * Actions to take to submit the data
		 * @function onSubmit
		 */
		onSubmit: function(oEvt) {
			var data = _oView.getModel("oGoalSettingsChanges").getData();
			if (data.globalSettings.descendants[0].accountLevel === "Level 4" && sap.ui.getCore().getModel("oAccounts").getData().level === 4) {
				if (data.corporateGoals.set.corporateTotal !== undefined) {
					data.globalSettings.descendants[0].corporateTotal = data.corporateGoals.set.corporateTotal;
				}
				if (data.corporateGoals.set.corporatePrcnt !== undefined) {
					data.globalSettings.descendants[0].corporatePrcnt = data.corporateGoals.set.corporatePrcnt;
				}
				if (data.corporateGoals.set.corporateTotalVPS !== undefined) {
					data.globalSettings.descendants[0].corporateTotalVPS = data.corporateGoals.set.corporateTotalVPS;
				}
			}
			oEvt.getSource().setBusyIndicatorDelay(0);
			oEvt.getSource().setBusy(true);
			data = data.globalSettings.descendants;
			//Get last year Values
			var currYear = new Date().getFullYear();
			var xsURL = "/goalsetting/accountyear?year=" + currYear;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "POST", _oController.onSubmitSuccess, _oController.onSubmitError,
				JSON.stringify(data));
			//GTM
			_oController.GTMDataLayer('goalsbmt',
				'Goal Setting',
				'Submission Form',
				'Submit -Buttonclick'
			);
		},

		/**
		 * formatter to comaFy the input Value
		 * @function formatterInputValue
		 */
		formatterInputValue: function(value) {
			if (value !== undefined && value !== null && value !== "") {
				value = _oController.commaFy(_oController.deCommaFy(value));
			}
			return value;
		},

		/**
		 * formatter to enable/disable the submit button
		 * @function checkSubmit
		 */
		checkSubmit: function() {
			if (_hasChanged) {
				_oView.byId("goalSettingSubmit").setEnabled(true);
			} else {
				_oView.byId("goalSettingSubmit").setEnabled(false);
			}
		},

		/**
		 * Function that checks if the descendants entries follow the submit rule of all or no inputs filled
		 * @function checkSubmitDescendants
		 */
		checkSubmitDescendants: function(aDescendants) {
			var enableButton = true;
			for (var i = 0; i < aDescendants.length; i++) {
				switch (aDescendants[i].accountLevel) {
					case ("Level 4"):
					case ("Level 3"):
						if ((!aDescendants[i].regionTotal && !aDescendants[i].regionPrcnt && !aDescendants[i].regionTotalVPS && !aDescendants[i].districtTotal &&
								!aDescendants[i].districtPrcnt && !aDescendants[i].districtTotalVPS &&
								!aDescendants[i].storeTotal && !aDescendants[i].storePrcnt && !aDescendants[i].storeTotalVPS) || (aDescendants[i].regionTotal >=
								0 &&
								aDescendants[i].regionPrcnt >= 0 && aDescendants[i].regionTotalVPS >= 0 &&
								aDescendants[i].districtTotal >= 0 && aDescendants[i].districtPrcnt >= 0 && aDescendants[i].districtTotalVPS >= 0 &&
								aDescendants[i].storeTotal >= 0 &&
								aDescendants[i].storePrcnt >= 0 && aDescendants[i].storeTotalVPS >= 0)) {
							if (aDescendants[i].descendants !== undefined && aDescendants[i].descendants.length !== 0) {
								enableButton = enableButton && _oController.checkSubmitDescendants(aDescendants[i].descendants);
							}
						} else if ((aDescendants[i].regionTotal >= 0 && aDescendants[i].regionPrcnt >= 0 && aDescendants[i].regionTotalVPS >= 0 &&
								aDescendants[i].districtTotal >= 0 &&
								aDescendants[i].districtPrcnt >= 0 && aDescendants[i].districtTotalVPS >= 0) && !aDescendants[i].storeTotal && !aDescendants[i]
							.storePrcnt &&
							!aDescendants[i].storeTotalVPS) {
							if (aDescendants[i].descendants !== undefined && aDescendants[i].descendants.length !== 0) {
								enableButton = enableButton && _oController.checkSubmitDescendants(aDescendants[i].descendants);
							}
						} else if ((aDescendants[i].regionTotal >= 0 && aDescendants[i].regionPrcnt >= 0 && aDescendants[i].regionTotalVPS >= 0) && (!
								aDescendants[i].districtTotal &&
								!aDescendants[i].districtPrcnt && !aDescendants[i].districtTotalVPS && !aDescendants[i].storeTotal && !aDescendants[i].storePrcnt &&
								!aDescendants[i].storeTotalVPS)) {
							if (aDescendants[i].descendants !== undefined && aDescendants[i].descendants.length !== 0) {
								enableButton = enableButton && _oController.checkSubmitDescendants(aDescendants[i].descendants);
							}
						} else {
							enableButton = false;
						}
						break;
					case ("Level 2"):
						if ((!aDescendants[i].districtTotal && !aDescendants[i].districtPrcnt && !aDescendants[i].districtTotalVPS && !aDescendants[i].storeTotal &&
								!aDescendants[i].storePrcnt && !aDescendants[i].storeTotalVPS) || (aDescendants[i].districtTotal >= 0 && aDescendants[i].districtPrcnt >=
								0 &&
								aDescendants[i].districtTotalVPS >= 0 && aDescendants[i].storeTotal >= 0 &&
								aDescendants[i].storePrcnt >= 0 && aDescendants[i].storeTotalVPS >= 0)) {
							if (aDescendants[i].descendants !== undefined && aDescendants[i].descendants.length !== 0) {
								enableButton = enableButton && _oController.checkSubmitDescendants(aDescendants[i].descendants);
							}
						} else if ((aDescendants[i].districtTotal >= 0 && aDescendants[i].districtPrcnt >= 0 && aDescendants[i].districtTotalVPS >= 0) &&
							(!
								aDescendants[i].storeTotal && !aDescendants[i].storePrcnt &&
								!aDescendants[i].storeTotalVPS)) {
							if (aDescendants[i].descendants !== undefined && aDescendants[i].descendants.length !== 0) {
								enableButton = enableButton && _oController.checkSubmitDescendants(aDescendants[i].descendants);
							}
						} else {
							enableButton = false;
						}
						break;
					case ("Level 1"):
						if ((aDescendants[i].storeTotal >= 0 && aDescendants[i].storePrcnt >= 0 && aDescendants[i].storeTotalVPS >= 0) || (!aDescendants[
									i].storeTotal &&
								!aDescendants[i].storePrcnt && !aDescendants[i].storeTotalVPS)) {
							break;
						} else {
							enableButton = false;
						}
						break;
				}
			}
			return enableButton;
		}

	});
});
