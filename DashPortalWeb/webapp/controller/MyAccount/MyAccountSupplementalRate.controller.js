sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	'sap/ui/core/util/Export',
	'sap/ui/core/util/ExportTypeCSV',
	'sap/ui/model/json/JSONModel'
], function (Base, Export, ExportTypeCSV, JSONModel) {
	"use strict";

	var _begin_Active = null,
		// Table pagination size
		_tablePaginationSize = 25,
		_selectedActvResult = 25,
		_oModelSupplementalRate_Active = new sap.ui.model.json.JSONModel(),
		_oModelSupplementalRateTable_Active = new sap.ui.model.json.JSONModel(),
		_oModelSupplementalRate_Future = new sap.ui.model.json.JSONModel(),
		_sSortedLabel = "accname",
		_sSortedOrder = "ascending",
		_enableClaim = true,
		_createSelectionRange = true,
		_oController,
		_oView,
		_resultsList = [25, 50, 100, 200],
		_oRouter;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.MyAccount.MyAccountSupplementalRate", {
		/** @module SupplementalRate */
		/**
		 * This function initializes the controller and sets the selectedAccount model.
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);

			_oRouter = this.getRouter();
			_oRouter.getRoute("myAccountSupplementalRate").attachPatternMatched(this._onObjectMatched, this);
			this.searchFieldEnabled();
			_oController = this;
			_oView = this.getView();

			//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
			this.getView().byId("fileuploadclaims").attachBrowserEvent("click", function () {
				_oController.GTMDataLayer('srbuttonclick',
					'Supplemental Rate',
					'Upload Claims -click',
					'UPLOAD CLAIMS'
				);
			});
		},

		/**
		 * Loads user data information
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {
			// Clears all error message
			this.clearServerErrorMsg(this);

			//set the navbar icon as selected
			this.resetMenuButtons();

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("SUPPLEMENT_RATE_PAGE")) {
				return;
			}

			//get user info and direct account
			_oController.getUserInformation(_oController);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			this.headerIconAccess();

			this.displayLayoutAccess();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			_enableClaim = true;
			// Load Supplemental Rate
			this.loadModelSupplementalRates();

			this.checkMonthlyPaymentWarn(this);

			//Reset all sorting items
			this.getView().byId("sortButton_1_SupplementalRateTable_Active").setSrc("resources/icon/sort_asc.png");
			this.getView().byId("sortButton_2_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_4_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");
			this.getView().byId("mobile_sortButton_1_SupplementalRateTable_Active").setSrc("resources/icon/sort_asc.png");
			this.getView().byId("mobile_sortButton_2_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");
			this.getView().byId("mobile_sortButton_4_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");

		},

		/**
		 * Function to enabled the "touch" properties of the searchfield
		 * @function searchFieldEnabled
		 */
		searchFieldEnabled: function () {
			this.getView().addEventDelegate({
				onAfterShow: function () {
					var inputID = _oController.getView().createId("searchFieldSupplementalRate");
					$("#" + inputID + "-I").attr("readonly", false);
				}
			});
		},

		/**
		 * Filters the table
		 * @function onLiveSearchUserManagement
		 * @param {Object} oEvent - Search field input
		 */
		onLiveSearchSupplementRates: function (oEvent) {
			// Get value of search field
			var newValue = oEvent.getParameter("newValue");

			// Setup all needed models
			var oModelUserManagementTableFiltered_Active = new sap.ui.model.json.JSONModel();
			_createSelectionRange = false;
			if (!newValue) {
				// Equals to the Pagination Model
				_oModelSupplementalRateTable_Active = _oModelSupplementalRate_Active;

				_begin_Active = 0;

				this.onFilteroModel(_oModelSupplementalRate_Active, _sSortedOrder, _sSortedLabel);
				this.tablePagination("SupplementalRateTable_Active", _begin_Active);

			} else {
				// Filtering the oModel
				oModelUserManagementTableFiltered_Active.setData(this.JSONFilter(newValue, _oModelSupplementalRate_Active));

				// Equals to the Pagination Model
				_oModelSupplementalRateTable_Active = oModelUserManagementTableFiltered_Active;

				// Start all the table at page 0
				_begin_Active = 0;

				this.onFilteroModel(_oModelSupplementalRate_Active, _sSortedOrder, _sSortedLabel);
				this.tablePagination("SupplementalRateTable_Active", _begin_Active);

			}
		},

		/**
		 * JSONFilter function that receives the oData and filters
		 * @ parameter (filterValue, oModel)
		 */
		JSONFilter: function (filterValue, oModel) {
			// DataJSON original and filtered
			var oDataJSON = oModel.getData();
			var oDataJSONFiltered = [];
			for (var i = 0; i < oDataJSON.length; i++) {
				// JSON Parameters to be filtered
				var accname = "";
				var materialNumber = "",
					accountnumber = "";
				if (oDataJSON[i].accname) {
					accname = oDataJSON[i].accname.toLowerCase();
				}
				if (oDataJSON[i].materialNumber) {
					materialNumber = oDataJSON[i].materialNumber.toLowerCase();
				}
				if (oDataJSON[i].accountnumber) {
					accountnumber = oDataJSON[i].accountnumber.toLowerCase();
				}
				if (accname.indexOf(filterValue.toLowerCase()) !== -1 || materialNumber.indexOf(filterValue.toLowerCase()) !== -1 || accountnumber
					.indexOf(filterValue.toLowerCase()) !== -1) {
					oDataJSONFiltered.push(oDataJSON[i]);
				}
			}
			return oDataJSONFiltered;
		},

		/**
		 * Sets the layout of the arrows for the table and controls the date to be loaded in the table
		 * @function tablePagination
		 */
		tablePagination: function (tableID, beginCounter) {
			var oDataTablePaginated = [];
			var oModelTablePaginated = new sap.ui.model.json.JSONModel();
			_tablePaginationSize = _selectedActvResult;

			var _oModelSupplementalRateTable = null;

			if (tableID === "SupplementalRateTable_Active") {
				_oModelSupplementalRateTable = _oModelSupplementalRateTable_Active;
			}

			// Set up the arrows of table
			var SupplementalRateTable = this.getView().byId(tableID);
			var navigationArrows = this.byId("navigationArrows_" + tableID);
			var rightNavigationArrow = this.byId("idRightNav_" + tableID);
			var leftNavigationArrow = this.byId("idLeftNav_" + tableID);
			var textCounterNavigation = this.byId("idTextCount_" + tableID);

			var _iLengthActive = 0;

			if (_oModelSupplementalRateTable && _oModelSupplementalRateTable.getData()) {
				_iLengthActive = _oModelSupplementalRateTable.getData().length;
			}

			var end = beginCounter + _tablePaginationSize;

			// Set visible or not the table arrows if there is no active items to show in table
			if (_iLengthActive === 0) {
				navigationArrows.setVisible(false);
			} else {
				navigationArrows.setVisible(true);
			}
			_oController.getView().byId("supplRateResultsPerPage").setVisible(_iLengthActive > 25);
			if (_createSelectionRange) {
				_oController.getView().byId("supplRateResults100").setVisible(_iLengthActive > 50);
				_oController.getView().byId("supplRateResults200").setVisible(_iLengthActive > 100);
			}
			_oController.removeSearchResultsStyleClass("supplRateResults", _resultsList, _oView);
			_oController.getView().byId("supplRateResults" + _selectedActvResult).addStyleClass("numberOfResultsLinkSelected");

			if (beginCounter < _tablePaginationSize) {
				leftNavigationArrow.setEnabled(false);
			} else {
				leftNavigationArrow.setEnabled(true);
			}

			if (end >= _iLengthActive) {
				end = _iLengthActive;
				rightNavigationArrow.setEnabled(false);
			} else {
				rightNavigationArrow.setEnabled(true);
			}

			if (_iLengthActive < _tablePaginationSize) {
				end = _iLengthActive;
			}

			if (_iLengthActive !== 0) {
				textCounterNavigation.setText(beginCounter + 1 + "-" + end + " " + _oController.getResourceBundle().getText("of") + " " +
					_iLengthActive);
			}

			for (var i = beginCounter; i < end; i++) {
				if (_oModelSupplementalRateTable) {
					oDataTablePaginated.push(_oModelSupplementalRateTable.getProperty("/" + i));
				}
			}
			oModelTablePaginated.setData(oDataTablePaginated);
			if (_iLengthActive > 100) {
				oModelTablePaginated.setSizeLimit(200);
			}

			SupplementalRateTable.setModel(oModelTablePaginated);
		},

		/**
		 * Loads more items into the table
		 * @function toRight
		 */
		toRight: function (oEvent) {
			var buttonId = oEvent.getSource().getId();
			var TableId = buttonId.split("idRightNav_")[1];

			if (TableId === "SupplementalRateTable_Active") {
				var start = _begin_Active + 1 + _tablePaginationSize;
				var _oModelSupplementalRateTable = null;
				_oModelSupplementalRateTable = _oModelSupplementalRateTable_Active;
				var iLengthActive = _oModelSupplementalRateTable.getData().length;
				_begin_Active = _begin_Active + _tablePaginationSize;
				var end = _begin_Active + _tablePaginationSize;
				if (end > iLengthActive) {
					end = iLengthActive;
				}
				this.tablePagination(TableId, _begin_Active);

				//GTM 
				_oController.GTMDataLayer('srpaginationclick_t1',
					'Supplemental Rate',
					'Pagination-click',
					start + '-' + end
				);
			}
		},

		/**
		 * Loads more items into the table
		 * @function toLeft
		 */
		toLeft: function (oEvent) {
			var buttonId = oEvent.getSource().getId();
			var TableId = buttonId.split("idLeftNav_")[1];

			if (TableId === "SupplementalRateTable_Active") {
				var start = _begin_Active + 1 - _tablePaginationSize;
				var _oModelSupplementalRateTable = null;
				_oModelSupplementalRateTable = _oModelSupplementalRateTable_Active;
				var iLengthActive = _oModelSupplementalRateTable.getData().length;
				_begin_Active = _begin_Active - _tablePaginationSize;
				var end = _begin_Active + _tablePaginationSize;
				if (end > iLengthActive) {
					end = iLengthActive;
				}
				this.tablePagination(TableId, _begin_Active);

				//GTM 
				_oController.GTMDataLayer('srpaginationclick_t1',
					'Supplemental Rate',
					'Pagination-click',
					start + '-' + end
				);
			}
		},

		/**
		 * Configure all the models and set the "ascending" order for the 3 Tables
		 @ parameter (data) Receives data from the Ajax call
		 */
		oDataConfiguration: function (data) {
			_oModelSupplementalRate_Active.setData(data.active);
			_oModelSupplementalRate_Future.setData(data.future);

			_oModelSupplementalRateTable_Active = _oModelSupplementalRate_Active;

			this.getView().setModel(_oModelSupplementalRateTable_Active, "oModelInfoActive");

			var orderState = "ascending";
			var oFilterVariable = "accname";

			this.onFilteroModel(_oModelSupplementalRateTable_Active, orderState, oFilterVariable);

			_begin_Active = 0;

			this.tablePagination("SupplementalRateTable_Active", _begin_Active);

			// Set the CurrencyIsoCode
			var oModelCurrencyIsoCode = new sap.ui.model.json.JSONModel();

			if (data.active !== undefined && data.active.length > 0) {
				oModelCurrencyIsoCode.setData({
					"currencyIsoCode": "(" + data.active[0].currencyIsoCode + ")"
				});
			} else
			if (data.expired !== undefined && data.expired.length > 0) {
				oModelCurrencyIsoCode.setData({
					"currencyIsoCode": "(" + data.expired[0].currencyIsoCode + ")"
				});
			} else
			if (data.future !== undefined && data.future.length > 0) {
				oModelCurrencyIsoCode.setData({
					"currencyIsoCode": "(" + data.future[0].currencyIsoCode + ")"
				});
			} else {
				oModelCurrencyIsoCode.setData({
					"currencyIsoCode": ""
				});
			}
			this.getView().setModel(oModelCurrencyIsoCode, "oModelInfoCurrencyIsoCode");
		},

		/**
		 * Function that handles the success case of the ajax call to submit Claims
		 * @function onLoadModelSupplementalRatesSuccess
		 */
		onLoadModelSupplementalRatesSuccess: function (oController, oData, oPiggyBack) {
			var SupplementalRateTable_Container = oController.getView().byId("vbox_container");
			var searchFieldUserManagement = oController.byId("searchFieldSupplementalRate");
			SupplementalRateTable_Container.setBusy(false);
			searchFieldUserManagement.setValue("");
			searchFieldUserManagement.setEnabled(true);
			oController.oDataConfiguration(oData);
		},

		/**
		 * Function that handles the error case of the ajax call to submit Claims
		 * @function onLoadModelSupplementalRatesError
		 */
		onLoadModelSupplementalRatesError: function (oController, oError, oPiggyBack) {
			var SupplementalRateTable_Container = oController.getView().byId("vbox_container");
			var searchFieldUserManagement = oController.byId("searchFieldSupplementalRate");
			SupplementalRateTable_Container.setVisible(false);
			SupplementalRateTable_Container.setBusy(false);
			searchFieldUserManagement.setEnabled(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"UMLoadUserManagement"), null, null);
			searchFieldUserManagement.setValue("");

		},

		/**
		 * Set the filter for the selected account and get cases for selected accounts.
		 * @function loadModelSupplementalRates
		 */
		loadModelSupplementalRates: function () {
			var SupplementalRateTable_Container = this.getView().byId("vbox_container");
			SupplementalRateTable_Container.setBusyIndicatorDelay(0);
			SupplementalRateTable_Container.setBusy(true);
			_oController.getView().byId("supplRateResultsPerPage").setVisible(false);
			_createSelectionRange = true;
			_selectedActvResult = 25;
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			var xsURL = "/salesforce/supplementalRates?lang=" + selectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onLoadModelSupplementalRatesSuccess, _oController.onLoadModelSupplementalRatesError);
		},

		/**
		 * Sorting Table Column 1
		 * @ (oEvent)
		 */
		onSortingColumn_1: function (oEvent) {
			var icon = oEvent.getSource(),
				iconName = icon.getSrc().split("sort_")[1],
				orderState = iconName.replace('.png', ''),
				buttonId = oEvent.getSource().getId(),
				TableId = buttonId.split("sortButton_1_")[1],
				oFilterVariable = "accname";
			_sSortedLabel = "accname";

			this.getView().byId("sortButton_2_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_4_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");
			this.getView().byId("mobile_sortButton_2_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");
			this.getView().byId("mobile_sortButton_4_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");

			if (orderState === "asc" || orderState === "both") {
				orderState = "desc";
				_sSortedOrder = "descending";
				this.onFilteroModel(_oModelSupplementalRateTable_Active, "descending", oFilterVariable);
				this.tablePagination(TableId, _begin_Active);

				//GTM 
				_oController.GTMDataLayer('srsortingclick_t1',
					'Supplemental Rate',
					'Column Sort-click',
					_oController.getResourceBundle().getText("MASPRAccountTBL") + '(' + orderState + ')'
				);

			} else if (orderState === "desc") {
				orderState = "asc";
				_sSortedOrder = "ascending";
				this.onFilteroModel(_oModelSupplementalRateTable_Active, "ascending", oFilterVariable);
				this.tablePagination(TableId, _begin_Active);

				//GTM 
				_oController.GTMDataLayer('srsortingclick_t1',
					'Supplemental Rate',
					'Column Sort-click',
					_oController.getResourceBundle().getText("MASPRAccountTBL") + '(' + orderState + ')'
				);

			} else {
				return;
			}

			this.getView().byId("mobile_sortButton_1_SupplementalRateTable_Active").setSrc("resources/icon/sort_" + orderState + ".png");
			this.getView().byId("sortButton_1_SupplementalRateTable_Active").setSrc("resources/icon/sort_" + orderState + ".png");
		},

		/**
		 * Sorting Table Column 1
		 * @ (oEvent)
		 */
		onSortingColumn_2: function (oEvent) {
			var icon = oEvent.getSource(),
				iconName = icon.getSrc().split("sort_")[1],
				orderState = iconName.replace('.png', ''),
				buttonId = oEvent.getSource().getId(),
				TableId = buttonId.split("sortButton_2_")[1],
				oFilterVariable = "accountnumber";
			_sSortedLabel = "accountnumber";

			this.getView().byId("sortButton_1_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_4_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");
			this.getView().byId("mobile_sortButton_1_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");
			this.getView().byId("mobile_sortButton_4_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");

			if (orderState === "asc" || orderState === "both") {
				orderState = "desc";
				_sSortedOrder = "descending";
				this.onFilteroModel(_oModelSupplementalRateTable_Active, "descending", oFilterVariable);
				this.tablePagination(TableId, _begin_Active);

				//GTM 
				_oController.GTMDataLayer('srsortingclick_t1',
					'Supplemental Rate',
					'Column Sort-click',
					_oController.getResourceBundle().getText("MASPCAccountNumTBL") + '(' + orderState + ')'
				);

			} else if (orderState === "desc") {
				orderState = "asc";
				_sSortedOrder = "ascending";
				this.onFilteroModel(_oModelSupplementalRateTable_Active, "ascending", oFilterVariable);
				this.tablePagination(TableId, _begin_Active);

				//GTM 
				_oController.GTMDataLayer('srsortingclick_t1',
					'Supplemental Rate',
					'Column Sort-click',
					_oController.getResourceBundle().getText("MASPCAccountNumTBL") + '(' + orderState + ')'
				);

			} else {
				return;
			}

			this.getView().byId("mobile_sortButton_2_SupplementalRateTable_Active").setSrc("resources/icon/sort_" + orderState + ".png");
			this.getView().byId("sortButton_2_SupplementalRateTable_Active").setSrc("resources/icon/sort_" + orderState + ".png");
		},

		/**
		 * Sorting Table Column 2
		 * @ (oEvent)
		 */
		onSortingColumn_4: function (oEvent) {
			var icon = oEvent.getSource(),
				iconName = icon.getSrc().split("sort_")[1],
				orderState = iconName.replace('.png', ''),
				buttonId = oEvent.getSource().getId(),
				TableId = buttonId.split("sortButton_4_")[1],
				oFilterVariable = "materialNumber";
			_sSortedLabel = "materialNumber";

			this.getView().byId("sortButton_1_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");
			this.getView().byId("sortButton_2_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");
			this.getView().byId("mobile_sortButton_1_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");
			this.getView().byId("mobile_sortButton_2_SupplementalRateTable_Active").setSrc("resources/icon/sort_both.png");

			if (orderState === "asc" || orderState === "both") {
				orderState = "desc";
				_sSortedOrder = "descending";
				this.onFilteroModel(_oModelSupplementalRateTable_Active, "descending", oFilterVariable);
				this.tablePagination(TableId, _begin_Active);

				//GTM 
				_oController.GTMDataLayer('srsortingclick_t1',
					'Supplemental Rate',
					'Column Sort-click',
					_oController.getResourceBundle().getText("MASPRMaterialNumberTBL") + '(' + orderState + ')'
				);

			} else if (orderState === "desc") {
				orderState = "asc";
				_sSortedOrder = "ascending";
				this.onFilteroModel(_oModelSupplementalRateTable_Active, "ascending", oFilterVariable);
				this.tablePagination(TableId, _begin_Active);

				//GTM 
				_oController.GTMDataLayer('srsortingclick_t1',
					'Supplemental Rate',
					'Column Sort-click',
					_oController.getResourceBundle().getText("MASPRMaterialNumberTBL") + '(' + orderState + ')'
				);

			} else {
				return;
			}

			this.getView().byId("mobile_sortButton_4_SupplementalRateTable_Active").setSrc("resources/icon/sort_" + orderState + ".png");
			this.getView().byId("sortButton_4_SupplementalRateTable_Active").setSrc("resources/icon/sort_" + orderState + ".png");
		},

		onClickingViewClaims: function (oEvent) {
			// Checks Session time out
			this.checkSessionTimeout();

			this.getRouter().navTo("MyAccountSupplementalClaims");

			//GTM
			_oController.GTMDataLayer('srbuttonclick',
				'Supplemental Rate',
				'Button -click',
				oEvent.getSource().getText()
			);

		},

		/**
		 * Export CSV file
		 * @ (oEvent)
		 */
		onDataExport: function (format) {
			var oApprovedDays = this.getGlobalParameter("approvedDays", "RatesParameters");
			var oModelData = JSON.parse(JSON.stringify(_oController.getView().getModel("oModelInfoActive").getData()));

			//add future rates to the export
			var future = _oModelSupplementalRate_Future.getData();

			if (future !== undefined && future !== null) {
				for (var i = 0; i < future.length; i++) {
					oModelData.push(future[i]);
				}
			}

			var sDays = oApprovedDays[0].value;

			oModelData.sort(function (a, b) {
				return new Date(b.beginDate) - new Date(a.beginDate);
			});

			var today = new Date();
			var month = _oController.pad(today.getMonth() + 1, 2, "0");
			var fileName = "SupplementalRates_" + today.getFullYear() + today.getDate() + month + today.getHours() + today.getMinutes() +
				today.getSeconds();

			if (format === "csv") {

				var oExport = new Export({

					exportType: new ExportTypeCSV({
						fileExtension: "csv",
						separatorChar: ","
					}),

					models: new JSONModel(oModelData),

					rows: {
						path: "/"
					},
					columns: [{
						name: _oController.getResourceBundle().getText("MASPRExportAcountNameCL"),
						template: {
							content: "{accname}"
						}
					}, {
						name: _oController.getResourceBundle().getText("MASPRExportAcountNumCL"),
						template: {
							content: "{accountnumber}"
						}
					}, {
						name: _oController.getResourceBundle().getText("MASPRExportMatNumCL"),
						template: {
							content: "{materialNumber}"
						}
					}, {
						name: _oController.getResourceBundle().getText("MASPRExportMatDescCL"),
						template: {
							content: "{materialNumberDesc}"
						}
					}, {
						name: _oController.getResourceBundle().getText("MASPRExportApprovedPriceCL"),
						template: {
							content: "{approvedSellingPrice}"
						}
					}, {
						name: _oController.getResourceBundle().getText("MASPRExportRateCL"),
						template: {
							content: "{supplementalPrice}"
						}
					}, {
						name: _oController.getResourceBundle().getText("MASPRExportBeginDateCL"),
						template: {
							content: {
								path: "beginDate",
								formatter: function (oValue1) {
									if (oValue1) {
										return oValue1.split('T')[0];
									}
								}
							}
						}
					}, {
						name: _oController.getResourceBundle().getText("MASPRExportEndDateCL"),
						template: {
							content: {
								path: "endDate",
								formatter: function (oValue1) {
									if (oValue1) {
										return oValue1.split('T')[0];
									}
								}
							}
						}
					}, {
						name: _oController.getResourceBundle().getText("MASPRExportNewCL", sDays),
						template: {
							content: {
								path: "newRate",
								formatter: function (oValue1) {
									return oValue1 ? oValue1 : "";
								}
							}
						}
					}]
				});

				oExport.saveFile(fileName).catch(function (oError) {

				}).then(function () {
					oExport.destroy();
				});
			} else {
				var aXLSXFormatData = _oController._createExcelFormatData(oModelData, sDays);
				var wb = _oController._jsonToWorkbook(aXLSXFormatData, "SupplementalRates_" + today.getFullYear());
				_oController._saveFileToExcel(wb, fileName);
			}

			//GTM
			_oController.GTMDataLayer('srbuttonclick',
				'Supplemental Rate',
				'Button -click',
				_oController.getView().byId("SupplementalsClaimsExport").getText()
			);
		},

		/**
		 * Configure page items display/hide
		 * List of Page Components to control
		 * @function displayLayoutAccess
		 */
		displayLayoutAccess: function () {
			var components = [{
				"Description": "SUPPLEMENTAL_CLAIM_PAGE",
				"Id": "SupplementalsClaimsView"
			}, {
				"Description": "SUPPLEMENTAL_CLAIM_SUBMISSION",
				"Id": "SupplementalsClaimsValidate"
			}, {
				"Description": "SUPPLEMENTAL_CLAIM_SUBMISSION",
				"Id": "claimButon"
			}, {
				"Description": "SUPPLEMENTAL_CLAIM_SUBMISSION",
				"Id": "fileuploadclaims"
			}];

			for (var i = 0; i < components.length; i++) {
				this.getView().byId(components[i].Id).setVisible(this.checkDASHLayoutAccess(components[i].Description));
			}
		},
		onPressValidateClaims: function (oEvent) {

			//GTM
			_oController.GTMDataLayer('srbuttonclick',
				'Supplemental Rate',
				'Button -click',
				oEvent.getSource().getText()
			);

			_oRouter.navTo("MyAccountSupplementalValidationClaims", {
				upload: false
			});

		},

		/**
		 * Function that handles the success case of the ajax call to create a claim
		 * @function onCreateClaimSuccess
		 */
		onCreateClaimSuccess: function (oController, oData, oPiggyBack) {
			_oRouter.navTo("MyAccountSupplementalValidationClaims", {
				upload: false
			});

		},

		/**
		 * Function that handles the error case of the ajax call to create a claim
		 * @function onCreateClaimError
		 */
		onCreateClaimError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"MASCInsertClaimsErrorTXT"), null, null);
		},

		/**
		 * Fires when the user clicks on the claim button.
		 * @function onCreateClaim
		 */
		onCreateClaim: function (oEvent) {
			if (_enableClaim) {
				_enableClaim = false;
				setTimeout(function () {
					_enableClaim = true;
				}, 3000);
				var path = oEvent.getSource().getBindingContext().getPath();
				var rate = this.getView().byId("SupplementalRateTable_Active").getModel().getProperty(path);

				var claim = [{
					"accname": rate.accname,
					"accountnumber": rate.accountnumber,
					"materialNumber": rate.materialNumber,
					"materialNumberDesc": rate.materialNumberDesc,
					"supplementalPrice": rate.supplementalPrice,
					"sellingPrice": rate.approvedSellingPrice,
					"currencyIsoCode": rate.currencyIsoCode,
					"isManual": "F",
					"distributorID": rate.distributorid,
					"accountid": rate.accountid,
					"supplementalRateID": rate.id
				}];
				var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();

				var xsURL = "/salesforce/insertClaims" + "?lang=" + selectedLanguage;
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				_oController.handleAjaxJSONCall(_oController, true, xsURL, "POST", _oController.onCreateClaimSuccess, _oController.onCreateClaimError,
					JSON.stringify(claim));
				_oController.GTMDataLayer('srbuttonclick',
					'Supplemental Rate',
					'Button -click',
					oEvent.getSource().getText()
				);

			}
		},

		/**
		 * Returne N/A on null value
		 @ parameter (value)
		 */
		valueNull: function (value) {
			if (value === null || value === "" || value === undefined) {
				return _oController.getResourceBundle().getText("UMNA");
			} else {
				return value;
			}
		},

		/**
		 * Ordering Algorithm
		 @ parameter (oModeo,orderMode,objectName)
		 */
		onFilteroModel: function (oModel, orderMode, objectName) {
			oModel.getData().sort(function (a, b) {
				switch (orderMode) {
				case "ascending":
					if ((a.valueOf()[objectName] === undefined || a.valueOf()[objectName] === null || a.valueOf()[objectName] === "") && (b.valueOf()[
							objectName] !== undefined || b.valueOf()[objectName] !== null || b.valueOf()[objectName] !== "")) {
						return 1;
					} else if ((a.valueOf()[objectName] !== undefined || a.valueOf()[objectName] !== null || a.valueOf()[objectName] !== "") && (b
							.valueOf()[objectName] === undefined || b.valueOf()[objectName] === null || b.valueOf()[objectName] === "")) {
						return -1;
					} else if (a.valueOf()[objectName].toLowerCase() < b.valueOf()[objectName].toLowerCase()) {
						return -1;
					} else if (a.valueOf()[objectName].toLowerCase() > b.valueOf()[objectName].toLowerCase()) {
						return 1;
					} else {
						return 0;
					}
					break;
				case "descending":
					if ((a.valueOf()[objectName] === undefined || a.valueOf()[objectName] === null || a.valueOf()[objectName] === "") && (b.valueOf()[
							objectName] !== undefined || b.valueOf()[objectName] !== null || b.valueOf()[objectName] !== "")) {
						return 1;
					} else if ((a.valueOf()[objectName] !== undefined || a.valueOf()[objectName] !== null || a.valueOf()[objectName] !== "") && (b
							.valueOf()[objectName] === undefined || b.valueOf()[objectName] === null || b.valueOf()[objectName] === "")) {
						return -1;
					} else if (a.valueOf()[objectName].toLowerCase() < b.valueOf()[objectName].toLowerCase()) {
						return 1;
					} else if (a.valueOf()[objectName].toLowerCase() > b.valueOf()[objectName].toLowerCase()) {
						return -1;
					} else {
						return 0;
					}
					break;
				default:
					break;
				}
			});
		},

		onUploadClaims: function (oEvent) {
			var oFile = oEvent.getParameter("files")[0];

			_oRouter.navTo("MyAccountSupplementalValidationClaims", {
				upload: true
			});

			//GTM
			_oController.GTMDataLayer('srbuttonclick',
				'Supplemental Rate',
				'Upload Claims -success',
				'UPLOAD CLAIMS'
			);

			//send inserted claim data to claims
			sap.ui.getCore().getEventBus().publish("uploadClaims", "uploadClaims", {
				data: oFile
			});
		},

		/**
		 *  Select the number of results displayed on a page
		 *  @selectResultsPerPage
		 */
		selectResultsPerPage: function (oEvent) {
			var sValue = oEvent.getSource().getText();
			_selectedActvResult = parseInt(sValue, 10);
			_createSelectionRange = false;
			_begin_Active = 0;
			_oController.tablePagination("SupplementalRateTable_Active", _begin_Active);

		},

		/**
		 * open Exports dialog
		 * 
		 * @function onDataExportBtn
		 */
		onDataExportBtn: function () {
			//Ipad resquesting desktop sites is identified as macOS
			if ((sap.ui.Device.os.ios || sap.ui.Device.os.macintosh) && (sap.ui.Device.system.phone || sap.ui.Device.system.tablet)) {
				_oController.onDataExport("csv");
			} else {
				// IF THE DIALOG IS CLOSED BY PRESSING "ESCAPE"
				if (_oController.oExportDialog) {
					_oController.oExportDialog.destroy();
				}
				_oController.oExportDialog =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.ExportDialog", this);
				_oController._createExportTypeModel(_oController.oExportDialog);
				_oController.oExportDialog.setModel(_oController.getView().getModel("i18n"), "i18n");
				// Set Header
				sap.ui.getCore().byId("exportDialogHeader").setText(_oController.getResourceBundle().getText("MASCExportRateHeaderText"));
				// Attach functions
				sap.ui.getCore().byId("export_ExportDialog").attachPress(_oController.onExportClaimBtn);
				sap.ui.getCore().byId("close_ExportDialog").attachPress(_oController.onCloseExportDialog);
				_oController.oExportDialog.open();
			}
		},

		/**
		 * close Exports dialog
		 * 
		 * @function onCloseExportDialog
		 */
		onCloseExportDialog: function () {
			_oController.oExportDialog.destroy();
		},

		/**
		 * press of export on Exports dialog
		 * 
		 * @function onExportClaimBtn
		 */
		onExportClaimBtn: function () {
			var sSelectedFormat = sap.ui.getCore().byId("exportType").getSelectedKey();
			switch (sSelectedFormat) {
			case "1":
				_oController.onDataExport("csv");
				break;
			case "2":
				_oController.onDataExport("xlxs");
				break;
			default:
				break;
			}
			_oController.oExportDialog.destroy();
		},

		/**
		 * change of export type on Exports dialog
		 * 
		 * @function onExportClaimBtn
		 */
		onChangeExportType: function (oEvent) {
			if (oEvent.getSource().getSelectedKey()) {
				_oController.oExportDialog.getModel("oExportTypes").setProperty("/0/enabledProperty", false);
				sap.ui.getCore().byId("export_ExportDialog").setEnabled(true);
			} else {
				sap.ui.getCore().byId("export_ExportDialog").setEnabled(false);
			}
		},

		/**
		 * method to format the Excel Data
		 * @function _createExcelFormatData
		 * @param {data} Raw Data
		 * @returns Formatted Data
		 */
		_createExcelFormatData: function (data, sDays) {
			var aXLSXFormatData = [];
			var dateFormatExcel = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "MM/dd/YYYY"
			});
			var oXLSXFormatHeader = {
				"accname": _oController.getResourceBundle().getText("MASPRExportAcountNameCL"),
				"accountnumber": _oController.getResourceBundle().getText("MASPRExportAcountNumCL"),
				"materialNumber": _oController.getResourceBundle().getText("MASPRExportMatNumCL"),
				"materialNumberDesc": _oController.getResourceBundle().getText("MASPRExportMatDescCL"),
				"approvedSellingPrice": _oController.getResourceBundle().getText("MASPRExportApprovedPriceCL"),
				"supplementalPrice": _oController.getResourceBundle().getText("MASPRExportRateCL"),
				"beginDate": _oController.getResourceBundle().getText("MASPRExportBeginDateCL"),
				"endDate": _oController.getResourceBundle().getText("MASPRExportEndDateCL"),
				"newRate": _oController.getResourceBundle().getText("MASPRExportNewCL", sDays)
			};
			for (var i = 0; i < data.length; i++) {
				var aKeys = Object.keys(oXLSXFormatHeader);
				var oRow = {};
				for (var j = 0; j < aKeys.length; j++) {
					if (aKeys[j] === "beginDate" || aKeys[j] === "endDate") {
						if (data[i][aKeys[j]]) {
							data[i][aKeys[j]] = dateFormatExcel.format(new Date(data[i][aKeys[j]]));
						}
					}
					if (aKeys[j] === "newRate") {
						data[i][aKeys[j]] = data[i][aKeys[j]] ? data[i][aKeys[j]] : "";
					}
					oRow[oXLSXFormatHeader[aKeys[j]]] = data[i][aKeys[j]];
				}
				aXLSXFormatData.push(oRow);
			}
			return aXLSXFormatData;
		}
	});

});