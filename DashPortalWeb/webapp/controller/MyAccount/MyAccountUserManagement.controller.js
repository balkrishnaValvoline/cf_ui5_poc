sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function (Base) {
	"use strict";
	var _oRouter,
		_oController,
		_oView,
		_endPage;

	var _uuid,
		_begin = 0,
		_beginAccountAccess = 0,
		_dialogLayoutStyle = null,
		_PrimaryAccountId = null,
		_AccountAccessIdList = [],
		_FunctionalRoleSelected = false,
		_tableSize = null,
		_iAccountAccessPageSize,
		_selectedActvResult = 10,
		_createSelectionRange = true;

	// Full Data Model
	var _oModelUserManagementFullData = new sap.ui.model.json.JSONModel();
	// User Management Table Model
	var _oModelUserManagementTable = new sap.ui.model.json.JSONModel();
	var _oModelUserManagementSortedData = new sap.ui.model.json.JSONModel();
	// Account Access Table Model
	var _oModelAccountAccessTable = new sap.ui.model.json.JSONModel();
	var _oModelAccountAccessSortedData = new sap.ui.model.json.JSONModel();
	var _oModelAccountAccessSortedDataSaveOnDone = new sap.ui.model.json.JSONModel();

	var _firstnameInputOk = false;
	var _lastnameInputOk = false;
	var _usernameInputOk = false;
	var _emailInputOk = false;
	var _accountAccessInputOK = false;
	var _functionalRolesOk = false;
	var _accountAccessHasChanged = false;
	var _isFirst = true;
	var _lastSearchValue;
	var _1stRender = true;
	var _validateUserNameExistant = false;
	var _bWasPopOverCreated;

	var DASH_MOBILE = "DASH_MOBILE";

	var _resultsList = [10, 25, 50, 100, 200];

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.MyAccount.MyAccountUserManagement", {
		/** @module MyAccountUserManagement */
		/**
		 * This function initializes the controller and sets the selectedAccount model.
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oController = this;
			_oView = this.getView();
			_oRouter = this.getRouter();
			_oRouter.getRoute("myAccountUserManagement").attachPatternMatched(this._onObjectMatched, this);
			this.searchFieldEnabled();
			// Set Table Size 10
			_tableSize = 10;
			_isFirst = true;
			_bWasPopOverCreated = false;

			// Register an event handler to changes of the screen size
			sap.ui.Device.media.attachHandler(_oController.resizeToMobile, this, DASH_MOBILE);
		},

		onExit: function () {
			// Detach function from event Handler
			sap.ui.Device.media.detachHandler(_oController.resizeToMobile, _oController, DASH_MOBILE);
		},

		/**
		 * If it is the first render, calls the the function to attach the focus out Events
		 * @function onBeforeRendering
		 */
		onBeforeRendering: function () {
			if (_1stRender) {
				setTimeout(function () {
					_oController.inputSearchOrderFunctions();
				}, 0);
				_1stRender = false;
			}
		},

		/**
		 * Loads user data information
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {
			_isFirst = true;

			// Clears all error message
			this.clearServerErrorMsg(this);

			//set the navbar icon as selected
			this.resetMenuButtons();

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("USER_MGMT_PAGE")) {
				return;
			}
			this.headerIconAccess();
			//get user info and direct account
			this.getUserInformation(this);
			_oView.byId("vbox_container").setBusy(true);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			// Get the User Accounts
			var promisseGetAccounts = new Promise(function (resolve, reject) {
				_oController.resolvePromisseGetAccounts = resolve;
				_oController.getAccounts(_oController, _oController.userInformationModel);
			});
			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			// Loading the Admin Data
			var promisseloadModelUserManagement = new Promise(function (resolve, reject) {
				_oController.resolvePromisseloadModelUserManagement = resolve;
				_oController.loadModelUserManagement();
			});

			_begin = 0;
			_beginAccountAccess = 0;
			_PrimaryAccountId = null;
			_AccountAccessIdList = null;
			Promise.all([promisseGetAccounts, promisseloadModelUserManagement]).then(function () {
				_oView.byId("vbox_container").setBusy(false);
			});
			// Set the flag to open the Manage dialof after the update or clone dialog
			_oController.bManageUserDialog = false;
			if (!_bWasPopOverCreated) {
				_bWasPopOverCreated = true;

				// Create Popover Message
				this._oPopover = new sap.m.Popover({
					showHeader: false,
					placement: "Left",
					horizontalScrolling: false,
					verticalScrolling: false
				});
				var oDisplayText = new sap.m.Text({
					text: _oController.getResourceBundle()
						.getText(
							"UMHelpText")
				}).addStyleClass("UserManagementHelpPopover");
				this._oPopover.addContent(oDisplayText);
				// Attach mouse hover event on help icon
				_oController.attachPopoverOnHelp(_oController.getView().byId("ManageUserHelp-Icon"), _oController._oPopover);
				_oController.attachPopoverOnHelp(_oController.getView().byId("ManageUserHelp-IconTop"), _oController._oPopover);
			}
		},

		/**
		 * Function to enabled the "touch" properties of the searchfield
		 * @function searchFieldEnabled
		 */
		searchFieldEnabled: function () {
			_oView.addEventDelegate({
				onAfterShow: function () {
					var inputID = _oView.createId("searchFieldUserManagement");
					$("#" + inputID + "-I").attr("readonly", false);
				}
			});
		},

		resizeToMobile: function (mParams) {
			if (sap.ui.getCore().byId("accountAccessFragment") !== undefined && sap.ui.getCore().byId("accountAccessFragment").getVisible()) {
				if (mParams.name === "mobile") {
					_iAccountAccessPageSize = 1;
				} else {
					_iAccountAccessPageSize = 5;
				}
				_beginAccountAccess = 0;
				this.tablePaginationAccountAccess();
			}
		},

		/**
		 * Gets the Role Permission Model of the Admin
		 * @function userInformationModel
		 */
		userInformationModel: function () {
			var userInformationModel = new sap.ui.model.json.JSONModel();
			var accountsInfo = sap.ui.getCore().getModel("oAccounts").getData();
			_uuid = sap.ui.getCore().getModel("oAccounts").getData().uuid;
			var accounts = [];

			for (var i = 0; i < sap.ui.getCore().getModel("oAccounts").getData().accounts.length; i++) {
				accounts.push(sap.ui.getCore().getModel("oAccounts").getData().accounts[i]);
			}

			var user = {
				"id": accountsInfo.id,
				"username": accountsInfo.username,
				"firstname": accountsInfo.firstname,
				"lastname": accountsInfo.lastname,
				"email": accountsInfo.email,
				"phone": accountsInfo.phone,
				"admin": accountsInfo.admin,
				"uuid": accountsInfo.uuid,
				"ownerUuid": accountsInfo.ownerUuid,
				"ownerUserName": accountsInfo.ownerUserName,
				"status": accountsInfo.status,
				"directAccount": accountsInfo.directAccount,
				"lastupdaterelation": accountsInfo.lastupdatedrelation,
				"level": accountsInfo.level,
				"mailingStreet": accountsInfo.mailingStreet,
				"mailingCity": accountsInfo.mailingCity,
				"mailingState": accountsInfo.mailingState,
				"mailingPostalCode": accountsInfo.mailingPostalCode
			};

			userInformationModel.setData(user);
			userInformationModel.setProperty("/accounts", accounts);

			sap.ui.getCore().setModel(userInformationModel, "UserInformation");
			_oController.userInformationFunctionalRolesModel();
			_oController.resolvePromisseGetAccounts();
		},

		/**
		 * Function that handles the success case of the ajax call to get the user's functional roles data
		 * @function onUserInformationFunctionalRolesModelSuccess
		 */
		onUserInformationFunctionalRolesModelSuccess: function (oController, oData, oPiggyBack) {
			var vbox_container = oController.getView().byId("vbox_container");
			vbox_container.setBusy(false);
			if (oData.length > 0) {
				var createUserButton = oController.getView().byId("createUserButton");
				createUserButton.setEnabled(true);
			}
			sap.ui.getCore().getModel("UserInformation").setProperty("/functionalrolesAssociated", oData);
		},

		/**
		 * Function that handles the error case of the ajax call to get the user's functional roles data
		 * @function onUserInformationFunctionalRolesModelError
		 */
		onUserInformationFunctionalRolesModelError: function (oController, oError, oPiggyBack) {
			var vbox_container = oController.getView().byId("vbox_container");
			var createUserButton = oController.getView().byId("createUserButton");
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"UMLoadUserManagement"));
			vbox_container.setBusy(false);
			createUserButton.setEnabled(false);
		},

		/**
		 * Gets the Funcional Roles Model of the Admin
		 * @function userFunctionalRolesModel
		 */
		userInformationFunctionalRolesModel: function () {
			var controller = this;
			var vbox_container = _oView.byId("vbox_container");
			var createUserButton = _oView.byId("createUserButton");
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			createUserButton.setEnabled(false);
			vbox_container.setBusyIndicatorDelay(0);
			vbox_container.setBusy(true);

			var xsURL = "/usermgmt/user/froles?lang=" + selectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onUserInformationFunctionalRolesModelSuccess, controller.onUserInformationFunctionalRolesModelError);
		},

		/**
		 * Function that handles the success case of the ajax call that loads the user management model data
		 * @function onLoadModelUserManagementSuccess
		 */
		onLoadModelUserManagementSuccess: function (oController, oData, oPiggyBack) {
			var UserManagemnentTableContainer = oController.getView().byId("UserManagemnentTableContainer");
			var searchFieldUserManagement = oController.byId("searchFieldUserManagement");
			var navigationArrows = oController.byId("navigationArrows");

			if (sap.ui.getCore().getModel("oAccounts") && sap.ui.getCore().getModel("oAccounts").getData().uuid) {
				_uuid = sap.ui.getCore().getModel("oAccounts").getData().uuid;
			}

			// Starts the table in the first page
			_begin = 0;
			UserManagemnentTableContainer.setBusy(false);
			navigationArrows.setBusy(false);
			if (oData.length === 0) {
				searchFieldUserManagement.setEnabled(false);
				navigationArrows.setVisible(false);
			} else {
				searchFieldUserManagement.clear();
				searchFieldUserManagement.setEnabled(true);
				navigationArrows.setVisible(true);
				_oModelUserManagementFullData.setData(oData);
				_oModelUserManagementSortedData = _oModelUserManagementFullData;
				_oModelUserManagementTable = _oModelUserManagementSortedData;
				oController.tablePagination();
				oController.byId("radioButtonSegmentedButton").setSelectedIndex(0);
			}
			_oController.resolvePromisseloadModelUserManagement();
		},

		/**
		 * Function that handles the error case of the ajax call that loads the user management model data
		 * @function onLoadModelUserManagementError
		 */
		onLoadModelUserManagementError: function (oController, oError, oPiggyBack) {
			var UserManagemnentTableContainer = oController.getView().byId("UserManagemnentTableContainer");
			var searchFieldUserManagement = oController.byId("searchFieldUserManagement");
			var navigationArrows = oController.byId("navigationArrows");
			navigationArrows.setVisible(false);
			searchFieldUserManagement.setEnabled(false);
			UserManagemnentTableContainer.setBusy(false);
			navigationArrows.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"UMLoadUserManagement"));
			searchFieldUserManagement.clear();
		},

		/**
		 * Set the filter for the selected account and get cases for selected accounts.
		 * @function loadModelUserManagement
		 */
		loadModelUserManagement: function () {
			var controller = this;
			var UserManagemnentTableContainer = _oView.byId("UserManagemnentTableContainer");
			UserManagemnentTableContainer.setBusyIndicatorDelay(0);
			UserManagemnentTableContainer.setBusy(true);

			var navigationArrows = controller.byId("navigationArrows");
			navigationArrows.setBusyIndicatorDelay(0);
			navigationArrows.setBusy(true);
			controller.byId("userMgmResultsPerPage").setVisible(false);
			_createSelectionRange = true;
			_selectedActvResult = 10;

			var xsURL = "/usermgmt/user/" + "shipToUsers/";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadModelUserManagementSuccess, controller.onLoadModelUserManagementError);
		},

		sapAccountNameVisibilityFormatter: function (accountName) {
			return (accountName !== undefined && accountName !== "");
		},

		/**
		 * Filters the table
		 * @function onLiveSearchUserManagement
		 * @param {Object} oEvent - Search field input
		 */
		onLiveSearchUserManagement: function (oEvent) {
			if (oEvent.getParameter("newValue") !== "" && oEvent.getParameter("newValue") !== null) {
				_lastSearchValue = oEvent.getParameter("newValue");
			} else {
				_lastSearchValue = undefined;
			}

			var oModelUserManagementTableFiltered = new sap.ui.model.json.JSONModel();
			_oModelUserManagementSortedData = this.onRadioButtonSelection();
			_createSelectionRange = false;
			if (!_lastSearchValue) {
				_oModelUserManagementTable = _oModelUserManagementSortedData;
				_begin = 0;
				this.tablePagination();
			} else {
				oModelUserManagementTableFiltered.setData(this.JSONFilter(_lastSearchValue, _oModelUserManagementSortedData));
				_oModelUserManagementTable = oModelUserManagementTableFiltered;
				_begin = 0;
				this.tablePagination();
			}

		},

		inputSearchOrderFunctions: function () {
			var inputSearch = _oView.byId("searchFieldUserManagement").getDomRef();
			inputSearch.addEventListener("focusout", function () {
				_oController.gtmSearch("handleLiveSearch");
			});
		},

		gtmSearch: function (oEvent) {
			//GTM
			if (_lastSearchValue !== "" && _lastSearchValue !== null && _lastSearchValue !== undefined) {
				_oController.GTMDataLayer('usersearchevnt',
					'User Management',
					'User Search',
					_lastSearchValue
				);
				_lastSearchValue = "";
			}
		},

		/**
		 * 
		 */
		handleSearch: function (oEvent) {
			if (oEvent.getParameters().clearButtonPressed === true) {
				_oController.gtmSearch();
			}
		},

		/**
		 * Radio Button
		 * @function onSegmentedButtonPress
		 */
		onRadioButtonSelection: function (oEvent) {
			// If the function is called by the component clears the search filter
			if (oEvent) {
				this.byId("searchFieldUserManagement").clear();
			}
			var oModelUserManagementTableFiltered = new sap.ui.model.json.JSONModel();
			var selectedIndex = this.byId("radioButtonSegmentedButton").getSelectedIndex();
			_createSelectionRange = false;
			switch (selectedIndex) {
			case 0:
				oModelUserManagementTableFiltered = _oModelUserManagementFullData;
				_oModelUserManagementTable = oModelUserManagementTableFiltered;
				_begin = 0;
				this.tablePagination();
				break;
			case 1:
				oModelUserManagementTableFiltered.setData(this.JSONFilter_OwnerUuid(_uuid, _oModelUserManagementFullData));
				_oModelUserManagementTable = oModelUserManagementTableFiltered;
				_begin = 0;
				this.tablePagination();
				break;
			default:
				break;
			}
			return oModelUserManagementTableFiltered;
		},

		/**
		 * 
		 */
		rbUserManagementAllU: function (oEvent) {
			if (oEvent.getParameter("selected")) {
				//GTM
				_oController.GTMDataLayer('radiobttnevent',
					'User Management',
					'Radio Button Selection',
					'All Users'
				);
			}
		},

		/**
		 * 
		 */
		rbUserManagementOwnerU: function (oEvent) {
			if (oEvent.getParameter("selected")) {
				//GTM
				_oController.GTMDataLayer('radiobttnevent',
					'User Management',
					'Radio Button Selection',
					'My Users'
				);
			}
		},

		/**
		 * JSONFilter function that receives the oData and filters
		 * @ parameter (filterValue, oModel)
		 */
		JSONFilter: function (filterValue, oModel) {
			var oDataJSON = oModel.getData();
			var oDataJSONFiltered = [];
			var sLCFilterValue = filterValue.toLowerCase();

			for (var i = 0; i < oDataJSON.length; i++) {
				var fullName = oDataJSON[i].firstname.toLowerCase() + " " + oDataJSON[i].lastname.toLowerCase();
				var email = oDataJSON[i].email.toLowerCase();
				var username = oDataJSON[i].username.toLowerCase();
				if (email.indexOf(sLCFilterValue) !== -1 ||
					username.indexOf(sLCFilterValue) !== -1 ||
					fullName.indexOf(sLCFilterValue) !== -1) {
					oDataJSONFiltered.push(oDataJSON[i]);
				}
			}
			return oDataJSONFiltered;
		},

		/**
		 * Filter the users created by the logged user
		 * @ parameter(filtervalue, oModel)
		 * return oDataJSONFiltered
		 */
		JSONFilter_OwnerUuid: function (filterValue, oModel) {
			var oDataJSON = oModel.getData();
			var oDataJSONFiltered = [];
			var sLCFilterValue = filterValue.toLowerCase();

			for (var i = 0; i < oDataJSON.length; i++) {
				if (oDataJSON[i].ownerUuid) {
					var ownerUuid = oDataJSON[i].ownerUuid.toLowerCase();
					if (ownerUuid.indexOf(sLCFilterValue) !== -1) {
						oDataJSONFiltered.push(oDataJSON[i]);
					}
				}
			}
			return oDataJSONFiltered;
		},

		/**
		 * Sets the layout of the arrows for the table and controls the date to be loaded in the table
		 * @function tablePagination
		 */
		tablePagination: function () {
			var _iLengthActive = _oModelUserManagementTable.getData().length || 0;
			_tableSize = _selectedActvResult;
			var end = _begin + _tableSize;
			_endPage = end;
			var oDataTablePaginated = [];
			var oModelTablePaginated = new sap.ui.model.json.JSONModel();

			if (_iLengthActive === 0) {
				this.byId("navigationArrows").setVisible(false);
				this.byId("userMgmResultsPerPage").setVisible(false);
			} else {
				this.byId("navigationArrows").setVisible(true);
			}
			this.byId("userMgmResultsPerPage").setVisible(_iLengthActive > 10);
			if (_createSelectionRange) {
				this.byId("userMgmResults50").setVisible(_iLengthActive > 25);
				this.byId("userMgmResults100").setVisible(_iLengthActive > 50);
				this.byId("userMgmResults200").setVisible(_iLengthActive > 100);
			}
			this.removeSearchResultsStyleClass("userMgmResults", _resultsList, _oView);
			this.byId("userMgmResults" + _selectedActvResult).addStyleClass("numberOfResultsLinkSelected");

			if (_begin < _tableSize) {
				this.byId("idLeftNav").setEnabled(false);
			} else {
				this.byId("idLeftNav").setEnabled(true);
			}
			if (end >= _iLengthActive) {
				end = _iLengthActive;
				this.byId("idRightNav").setEnabled(false);
			} else {
				this.byId("idRightNav").setEnabled(true);
			}

			if (_iLengthActive < _tableSize) {
				end = _iLengthActive;
			}

			if (_iLengthActive !== 0) {
				this.byId("idTextCount").setText(_begin + 1 + "-" + end + " " + _oController.getResourceBundle()
					.getText("of") + " " + _iLengthActive);
			}

			for (var i = _begin; i < end; i++) {
				oDataTablePaginated.push(_oModelUserManagementTable.getProperty("/" + i));
			}

			oModelTablePaginated.setData(oDataTablePaginated);
			if (_selectedActvResult > 100) {
				oModelTablePaginated.setSizeLimit(_selectedActvResult);
			}
			_oView.byId("UserManagemnentTable").setModel(oModelTablePaginated, "UserManagement");
			_oView.setModel(oModelTablePaginated, "modelUserManagement");
		},

		/**
		 * Loads more items into the table
		 * @function toRight
		 */
		toRight: function () {
			_begin = _begin + _tableSize;
			this.tablePagination();

			//GTM
			_oController.GTMDataLayer('paginationclick',
				'User Management',
				'Pagination click',
				_begin + 1 + '-' + _endPage
			);
		},

		/**
		 * Loads more items into the table
		 * @function toLeft
		 */
		toLeft: function () {
			_begin = _begin - _tableSize;
			this.tablePagination();

			//GTM
			_oController.GTMDataLayer('paginationclick',
				'User Management',
				'Pagination click',
				_begin + 1 + '-' + _endPage
			);
		},

		/**
		 * Opens the Create User Fragment
		 * @function onPressCreateUser
		 */
		onPressCreateUser: function () {
			_isFirst = true;
			// Checks the session timeout
			this.checkSessionTimeout();

			_oController.bManageUserDialog = false;
			//GTM
			_oController.GTMDataLayer('crtuserbttnclick',
				'User Management',
				'Create User-ButtonClick',
				sap.ui.getCore().getModel("oAccounts").getData().uuid
			);

			// If the fragement is already created, destroys all the content
			if (this.dialogFragmentCreateEditViewUser) {
				this.dialogFragmentCreateEditViewUser.destroyContent();
			}

			this.dialogFragmentCreateEditViewUser =
				new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.CreateUser", this);
			this.dialogFragmentCreateEditViewUser.open();
			_dialogLayoutStyle = "Create";

			// Reset variables
			_PrimaryAccountId = null;
			_AccountAccessIdList = null;
			_firstnameInputOk = false;
			_lastnameInputOk = false;
			_usernameInputOk = false;
			_emailInputOk = false;
			_accountAccessInputOK = false;
			_functionalRolesOk = false;

			this.onFunctionalRolesConfiguration();
			this.onAccountAccessConfiguration();

			var i18nModel = new sap.ui.model.resource.ResourceModel({
				bundleUrl: "i18n/i18n.properties"
			});
			this.dialogFragmentCreateEditViewUser.setModel(i18nModel, "i18n");
			this.inputFieldFocusOutFunction();

			var accounts = sap.ui.getCore().getModel("UserInformation").getData().accounts;
			if (accounts.length === 1) {
				_accountAccessInputOK = true;
				sap.ui.getCore().byId("primaryaccountLabel").setText(this.formatterContactAddress(accounts[0].billingstreet, accounts[0].billingcity,
					accounts[0].billingstate, accounts[0].billingpostalcode));
			}
		},
		/**
		 *
		 */
		inputFieldFocusOutFunction: function () {
			var usernameInputField = $("#usernameInput")[0];
			var emailInput = $("#emailInput")[0];
			var controller = this;
			usernameInputField.addEventListener("focusout", function () {
				controller.onCheckInputUsernameValidation();
			});
			emailInput.addEventListener("focusout", function () {
				controller.onCheckInputUsernameValidation();
			});
		},

		/**
		 * Select fragment view or manage
		 * @function onPressViewManageUser
		 * @param {Object} oEvent - View/Manage button
		 */
		onPressViewManageUser: function (oEvent) {
			_isFirst = true;
			// Checks the session timeout
			this.checkSessionTimeout();

			// set a flag to open the manage dialog on close press after Edit or Clone dialog
			_oController.bManageUserDialog = false;
			var tablePath;
			if (_oController.bCloseAfterManage) {
				// Set the context of the user
				tablePath = _oController.oManageUserContext;
				_oController.bCloseAfterManage = false;
			} else {
				tablePath = oEvent.getSource().getBindingContext("UserManagement").getPath();
				_oController.oManageUserContext = tablePath;

				//GTM
				_oController.GTMDataLayer('listactivitiesevent',
					'User Management',
					'List Activites -click',
					oEvent.getSource().getText() + ' -linkclick'
				);
			}
			var selectedUserUuid = this.getView().byId("UserManagemnentTable").getModel("UserManagement").getProperty(tablePath).uuid;
			var ownerUuid = this.getView().byId("UserManagemnentTable").getModel("UserManagement").getProperty(tablePath).ownerUuid;

			// If the fragement is already created, destroys all the content
			if (this.dialogFragmentCreateEditViewUser) {
				this.dialogFragmentCreateEditViewUser.destroyContent();
			}
			_dialogLayoutStyle = null;
			if (_uuid === ownerUuid) {
				this.dialogFragmentCreateEditViewUser =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.ManageUser", this);
				this.dialogFragmentCreateEditViewUser.open();

				_dialogLayoutStyle = "View";
				_firstnameInputOk = true;
				_lastnameInputOk = true;
				_usernameInputOk = true;
				_emailInputOk = true;
				_accountAccessInputOK = true;
				_functionalRolesOk = true;
				this.getSelectedUser(selectedUserUuid); // Get the selectedUserInformation
			} else {
				this.dialogFragmentCreateEditViewUser =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.ViewUser", this);
				this.dialogFragmentCreateEditViewUser.open();
				_dialogLayoutStyle = "View";
				this.getSelectedUser(selectedUserUuid); // Get the selectedUserInformation
			}
			var i18nModel = new sap.ui.model.resource.ResourceModel({
				bundleUrl: "i18n/i18n.properties"
			});
			this.dialogFragmentCreateEditViewUser.setModel(i18nModel, "i18n");
		},

		/**
		 * Select fragment edit user
		 * @function onPressEditUser
		 * @param {Object} oEvent - Edit User button
		 */
		onPressEditUser: function (oEvent) {
			_isFirst = true;
			// Checks the session timeout
			this.checkSessionTimeout();

			_oController.bManageUserDialog = true;
			// If the fragement is already created, destroys all the content
			if (this.dialogFragmentCreateEditViewUser) {
				this.dialogFragmentCreateEditViewUser.close();
				this.dialogFragmentCreateEditViewUser.destroyContent();
			}
			_dialogLayoutStyle = null;

			this.dialogFragmentCreateEditViewUser =
				new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.EditUser", this);
			this.dialogFragmentCreateEditViewUser.open();
			_dialogLayoutStyle = "Edit";
			_firstnameInputOk = true;
			_lastnameInputOk = true;
			_usernameInputOk = true;
			_emailInputOk = true;
			_accountAccessInputOK = true;
			_functionalRolesOk = true;

			// Calls the Models Configuration functions
			_oController.onFunctionalRolesConfiguration();
			_oController.onAccountAccessConfiguration();

			var i18nModel = new sap.ui.model.resource.ResourceModel({
				bundleUrl: "i18n/i18n.properties"
			});
			this.dialogFragmentCreateEditViewUser.setModel(i18nModel, "i18n");

			//GTM
			_oController.GTMDataLayer('listactivitiesevent',
				'User Management',
				'List Activites -click',
				oEvent.getSource().getText() + ' -Buttonclick'
			);
		},

		/**
		 * Select fragment clone user
		 * @function onPressCloneUser
		 * @param {Object} oEvent - Clone User button
		 */
		onPressCloneUser: function (oEvent) {
			_isFirst = true;
			// Checks the session timeout
			this.checkSessionTimeout();

			_oController.bManageUserDialog = true;
			var sPrimaryAccount = sap.ui.getCore().byId("primaryaccountLabel").getText();

			// If the fragement is already created, destroys all the content
			if (this.dialogFragmentCreateEditViewUser) {
				this.dialogFragmentCreateEditViewUser.close();
				this.dialogFragmentCreateEditViewUser.destroyContent();
			}
			_dialogLayoutStyle = "Clone";
			this.dialogFragmentCreateEditViewUser =
				new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.CreateUser", this);
			this.dialogFragmentCreateEditViewUser.open();

			_firstnameInputOk = false;
			_lastnameInputOk = false;
			_usernameInputOk = false;
			_emailInputOk = false;
			_accountAccessInputOK = true;
			_functionalRolesOk = true;

			// Calls the Models Configuration functions
			_oController.onFunctionalRolesConfiguration();
			_oController.onAccountAccessConfiguration();

			var i18nModel = new sap.ui.model.resource.ResourceModel({
				bundleUrl: "i18n/i18n.properties"
			});
			this.dialogFragmentCreateEditViewUser.setModel(i18nModel, "i18n");
			this.inputFieldFocusOutFunction();

			// Set the Primary Account Detail
			sap.ui.getCore().byId("primaryaccountLabel").setText(sPrimaryAccount);

			//GTM
			_oController.GTMDataLayer('listactivitiesevent',
				'User Management',
				'List Activites -click',
				oEvent.getSource().getText() + ' -Buttonclick'
			);
		},

		/**
		 * Function that handles the success case of the ajax call that gets the selected user
		 * @function onGetSelectedUserSuccess
		 */
		onGetSelectedUserSuccess: function (oController, oData, oPiggyBack) {
			var viewEditFragment = oController.dialogFragmentCreateEditViewUser;
			var oModelSelectedUser = new sap.ui.model.json.JSONModel();
			sap.ui.getCore().setModel(oModelSelectedUser, "oModelSelectedUser");

			viewEditFragment.setBusy(false);
			oModelSelectedUser.setData(oData);
			sap.ui.getCore().setModel(oModelSelectedUser, "oModelSelectedUser");

			// Calls the Models Configuration functions
			oController.onFunctionalRolesConfiguration();
			oController.onAccountAccessConfiguration();
		},

		/**
		 * Function that handles the error case of the ajax call that gets the selected user
		 * @function onGetSelectedUserError
		 */
		onGetSelectedUserError: function (oController, oError, oPiggyBack) {
			var viewEditFragment = oController.dialogFragmentCreateEditViewUser;
			viewEditFragment.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRDomainByValue"));
			_oController.bManageUserDialog = false;
			oController.closeDialogCreateUser();
		},

		/**
		 * Gets the  user information through uuui and saves it to a model
		 * @function getSelectedUser
		 * @param {string} uuid - Forgerock ID
		 */
		getSelectedUser: function (uuid) {
			var controller = this;
			var viewEditFragment = this.dialogFragmentCreateEditViewUser;

			var oModelSelectedUser = new sap.ui.model.json.JSONModel();
			sap.ui.getCore().setModel(oModelSelectedUser, "oModelSelectedUser");
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();

			viewEditFragment.setBusyIndicatorDelay(0);
			viewEditFragment.setBusy(true);

			var xsURL = "/usermgmt/user/" + uuid + "?lang=" + selectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onGetSelectedUserSuccess, controller.onGetSelectedUserError);
		},

		/**
		 * Configure the User Role Model to the be read by the functional
		 * @function onFunctionalRolesConfiguration
		 */
		onFunctionalRolesConfiguration: function () {
			var oModelLoggedUserInformation = sap.ui.getCore().getModel("UserInformation");
			var oModelAdminFunctionalRoles = new sap.ui.model.json.JSONModel();
			oModelAdminFunctionalRoles.setData(oModelLoggedUserInformation.getProperty("/functionalrolesAssociated"));

			var oModelSelectedUser;
			var oModelSelectUserFunctionalRoles = new sap.ui.model.json.JSONModel();

			var adminRolePermissionId;
			var selectedUserRolePermissionId;
			var i, j, x, y;

			switch (_dialogLayoutStyle) {
			case "Create":
				for (i = 0; i < oModelAdminFunctionalRoles.getData().length; i++) {
					oModelAdminFunctionalRoles.setProperty("/" + i + "/isEnabled", true);
					for (j = 0; j < oModelAdminFunctionalRoles.getProperty("/" + i + "/functionalRoles").length; j++) {
						oModelAdminFunctionalRoles.setProperty("/" + i + "/functionalRoles/" + j + "/isEnabled", true);
						oModelAdminFunctionalRoles.setProperty("/" + i + "/functionalRoles/" + j + "/isSelected", false);
					}
				}
				// Set the Status Select Box
				sap.ui.getCore().setModel(oModelAdminFunctionalRoles, "oModelFunctionalRolesDisplayed");
				this.generatePermissionsPanel();
				this.onDefaultRoles();
				_oController.onAllItemsSelectedVerification();
				break;

			case "View":
				oModelSelectedUser = sap.ui.getCore().getModel("oModelSelectedUser");
				oModelSelectUserFunctionalRoles.setData(oModelSelectedUser.getProperty("/functionalrolesAssociated"));
				for (i = 0; i < oModelSelectUserFunctionalRoles.getData().length; i++) {
					oModelSelectUserFunctionalRoles.setProperty("/" + i + "/isEnabled", false);
					for (j = 0; j < oModelSelectUserFunctionalRoles.getProperty("/" + i + "/functionalRoles").length; j++) {
						oModelSelectUserFunctionalRoles.setProperty("/" + i + "/functionalRoles/" + j + "/isEnabled", false);
						oModelSelectUserFunctionalRoles.setProperty("/" + i + "/functionalRoles/" + j + "/isSelected", true);
					}
				}
				// Set the Status Select Box
				sap.ui.getCore().byId("statusInput").setSelectedKey(oModelSelectedUser.getProperty("/status"));
				sap.ui.getCore().setModel(oModelSelectUserFunctionalRoles, "oModelFunctionalRolesDisplayed");
				this.generatePermissionsPanel();
				_oController.onAllItemsSelectedVerification();
				break;

			case "Edit":
				oModelSelectedUser = sap.ui.getCore().getModel("oModelSelectedUser");
				oModelSelectUserFunctionalRoles.setData(oModelSelectedUser.getProperty("/functionalrolesAssociated"));
				for (i = 0; i < oModelAdminFunctionalRoles.getData().length; i++) {
					oModelAdminFunctionalRoles.setProperty("/" + i + "/isEnabled", true);
					for (j = 0; j < oModelAdminFunctionalRoles.getProperty("/" + i + "/functionalRoles").length; j++) {
						adminRolePermissionId = oModelAdminFunctionalRoles.getProperty("/" + i + "/functionalRoles/" + j + "/id");
						oModelAdminFunctionalRoles.setProperty("/" + i + "/functionalRoles/" + j + "/isSelected", false);
						oModelAdminFunctionalRoles.setProperty("/" + i + "/functionalRoles/" + j + "/isEnabled", true);

						for (x = 0; x < oModelSelectUserFunctionalRoles.getData().length; x++) {
							for (y = 0; y < oModelSelectUserFunctionalRoles.getProperty("/" + x + "/functionalRoles").length; y++) {
								selectedUserRolePermissionId = oModelSelectUserFunctionalRoles.getProperty("/" + x + "/functionalRoles/" + y + "/id");
								if (adminRolePermissionId === selectedUserRolePermissionId) {
									oModelAdminFunctionalRoles.setProperty("/" + i + "/functionalRoles/" + j + "/isEnabled", true);
									oModelAdminFunctionalRoles.setProperty("/" + i + "/functionalRoles/" + j + "/isSelected", true);
								}
							}
						}
					}
				}
				// Set the Status Select Box
				sap.ui.getCore().byId("statusInput").setSelectedKey(oModelSelectedUser.getProperty("/status"));
				sap.ui.getCore().setModel(oModelAdminFunctionalRoles, "oModelFunctionalRolesDisplayed");
				this.generatePermissionsPanel();
				_oController.onFunctionalRoleSelectedVerification();
				_oController.onAllItemsSelectedVerification();
				break;

			case "Clone":
				oModelSelectedUser = sap.ui.getCore().getModel("oModelSelectedUser");
				oModelSelectUserFunctionalRoles.setData(oModelSelectedUser.getProperty("/functionalrolesAssociated"));
				for (i = 0; i < oModelAdminFunctionalRoles.getData().length; i++) {
					oModelAdminFunctionalRoles.setProperty("/" + i + "/isEnabled", true);
					for (j = 0; j < oModelAdminFunctionalRoles.getProperty("/" + i + "/functionalRoles").length; j++) {
						adminRolePermissionId = oModelAdminFunctionalRoles.getProperty("/" + i + "/functionalRoles/" + j + "/id");
						oModelAdminFunctionalRoles.setProperty("/" + i + "/functionalRoles/" + j + "/isSelected", false);
						oModelAdminFunctionalRoles.setProperty("/" + i + "/functionalRoles/" + j + "/isEnabled", true);

						for (x = 0; x < oModelSelectUserFunctionalRoles.getData().length; x++) {
							for (y = 0; y < oModelSelectUserFunctionalRoles.getProperty("/" + x + "/functionalRoles").length; y++) {
								selectedUserRolePermissionId = oModelSelectUserFunctionalRoles.getProperty("/" + x + "/functionalRoles/" + y + "/id");
								if (adminRolePermissionId === selectedUserRolePermissionId) {
									oModelAdminFunctionalRoles.setProperty("/" + i + "/functionalRoles/" + j + "/isEnabled", true);
									oModelAdminFunctionalRoles.setProperty("/" + i + "/functionalRoles/" + j + "/isSelected", true);
								}
							}
						}
					}
				}
				// Set the Status Select Box
				sap.ui.getCore().byId("statusInput").setSelectedKey(oModelSelectedUser.getProperty("/status"));
				sap.ui.getCore().setModel(oModelAdminFunctionalRoles, "oModelFunctionalRolesDisplayed");
				this.generatePermissionsPanel();
				_oController.onFunctionalRoleSelectedVerification();
				_oController.onAllItemsSelectedVerification();
				break;

			default:
				break;
			}
		},

		/**
		 * This function creates the Permission Panel
		 * @function generatePermissionsPanel
		 */
		generatePermissionsPanel: function () {
			// Gets the current Functional Model displayed
			var oModelFunctionalRolesDisplayed = sap.ui.getCore().getModel("oModelFunctionalRolesDisplayed");

			// Shows sets the fragment configuration for the no Functional Roles
			this.noPermissionMessageAction(oModelFunctionalRolesDisplayed);

			for (var i = 0; i < oModelFunctionalRolesDisplayed.getData().length; i++) {
				// Creates the Panel Component
				var permissionsPanel = new sap.m.Panel({
					expandable: true,
					expanded: true,
					width: "auto",
					height: "auto"
				}).addStyleClass("permissionSelectBox changeBorderSelectBoxUserManagement createUserRolesPanelBorderBottom");

				// Creates the HBox for the ToolBar
				var toolBarHbox = new sap.m.HBox({
					alignItems: "Center"
				});
				var headerToolBar = new sap.m.Toolbar({});

				// Creates the Main CheckBox
				var permissionsParentCheckBox = new sap.m.CheckBox({
					id: "checkBoxId_" + i,
					enabled: oModelFunctionalRolesDisplayed.getProperty("/" + i + "/isEnabled")
				}).addStyleClass("changeBorderSelectBoxUserManagement");
				permissionsParentCheckBox.attachSelect(this.onMainCheckBoxSelect);

				// Creates the Main Text for the Main CheckBox
				var permissionsParentText = new sap.m.Text({
					text: oModelFunctionalRolesDisplayed.getProperty("/" + i).categoryDescription
				}).addStyleClass("bodyCopy2 permissionSelectBox");

				toolBarHbox.addItem(permissionsParentCheckBox);
				toolBarHbox.addItem(permissionsParentText);
				headerToolBar.addContent(toolBarHbox);
				permissionsPanel.setHeaderToolbar(headerToolBar);

				// Creates the Content HBox for the information Inside the Panel
				var contentHbox = new sap.m.HBox({
					width: "100%"
				}).addStyleClass("panelContentWrap permissionPanelContentPosition");

				for (var j = 0; j < oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles").length; j++) {
					// Creates the 2nd HBox
					var roleHbox = new sap.m.HBox({
						alignItems: "Center"
					}).addStyleClass("panelMargin");

					// Creates the 2nd CheckBox
					var roleCheckBox = new sap.m.CheckBox({
						id: "checkBoxId_" + i + "_" + j,
						enabled: oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles/" + j + "/isEnabled"),
						selected: oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles/" + j + "/isSelected")
					}).attachSelect(this.onSecondCheckBoxSelect);
					roleCheckBox.addStyleClass("changeBorderSelectBoxUserManagement");

					// Creates the 2nd Title for the 2nd CheckBox
					var roleText = new sap.m.Text({
						text: oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles/" + j + "/description")
					}).addStyleClass("bodyCopy2");

					roleHbox.addItem(roleCheckBox);
					roleHbox.addItem(roleText);
					contentHbox.addItem(roleHbox);
				}
				permissionsPanel.addContent(contentHbox);
				var permissionsPanelContainer = sap.ui.getCore().byId("permissionsPanel");
				permissionsPanelContainer.addItem(permissionsPanel);
			}
		},

		/**
		 * Select All Permissions
		 * @function onSelectAllCheckBoxSelect
		 * @param {Object} oEvent - check box "Select All"
		 */
		onSelectAllCheckBoxSelect: function (oEvent) {
			var oModelFunctionalRolesDisplayed = sap.ui.getCore().getModel("oModelFunctionalRolesDisplayed");
			var selectAllCheckBoxStatus = oEvent.getParameter("selected");
			for (var i = 0; i < oModelFunctionalRolesDisplayed.getData().length; i++) {
				if (oModelFunctionalRolesDisplayed.getProperty("/" + i + "/isEnabled")) {
					sap.ui.getCore().byId("checkBoxId_" + i).setSelected(selectAllCheckBoxStatus);
				}
				for (var j = 0; j < oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles").length; j++) {
					if (oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles/" + j + "/isEnabled")) {
						sap.ui.getCore().byId("checkBoxId_" + i + "_" + j).setSelected(selectAllCheckBoxStatus);
					}
				}
			}
			_oController.onFunctionalRoleSelectedVerification();
		},

		/**
		 * Select All the Items CheckBoxes
		 * @function onMainCheckBoxSelect
		 * @param {Object} oEvent - Checkbox selected
		 */
		onMainCheckBoxSelect: function (oEvent) {
			var permissionsParentCheckBoxStatus = oEvent.getParameter("selected");
			var rolesList = oEvent.getSource().getParent().getParent().getParent().getContent()[0].getAggregation("items");

			for (var i = 0; i < rolesList.length; i++) { // All the items contained in that Role Secton
				if (rolesList[i].getAggregation("items")[0].getEnabled()) {
					rolesList[i].getAggregation("items")[0].setSelected(permissionsParentCheckBoxStatus);
				}
			}
			oEvent.getSource().getParent().getParent().getParent().setExpanded(true);
			_oController.onRoleCombinationsGroup(oEvent);
			_oController.onAllItemsSelectedVerification();
			_oController.onFunctionalRoleSelectedVerification();
		},

		/**
		 * Check all checkboxes for the select All state checkbox
		 * @function onSecondCheckBoxSelect
		 * @param {Object} oEvent - Check box selected
		 */
		onSecondCheckBoxSelect: function (oEvent) {
			_oController.onRoleCombinationsIndividual(oEvent);
			_oController.onAllItemsSelectedVerification();
			_oController.onFunctionalRoleSelectedVerification();
		},

		/**
		 * Verifies the State of all secondary checkboxes
		 * @function onAllItemsSelectedVerification
		 */
		onAllItemsSelectedVerification: function () {
			var oModelFunctionalRolesDisplayed = sap.ui.getCore().getModel("oModelFunctionalRolesDisplayed");
			var allSelected = true; // For the SelectAll CheckBox
			var allItemsSelected = true; // For the Parent SelectAll CheckBox

			for (var i = 0; i < oModelFunctionalRolesDisplayed.getData().length; i++) { // Cycle all the Itmes of each Role Panel
				allItemsSelected = true;
				for (var j = 0; j < oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles").length; j++) {
					if (!sap.ui.getCore().byId("checkBoxId_" + i + "_" + j).getSelected()) {
						allItemsSelected = false;
						allSelected = false;
					}
				}

				if (allItemsSelected) { // Set each parent Role checoBox
					sap.ui.getCore().byId("checkBoxId_" + i).setSelected(true);
				} else {
					sap.ui.getCore().byId("checkBoxId_" + i).setSelected(false);
				}
			}

			if (allSelected) { // Set all Role Checbox
				if (sap.ui.getCore().byId("selectAllCheckBox")) {
					sap.ui.getCore().byId("selectAllCheckBox").setSelected(true);
				}
			} else {
				if (sap.ui.getCore().byId("selectAllCheckBox")) {
					sap.ui.getCore().byId("selectAllCheckBox").setSelected(false);
				}
			}
		},

		/**
		 * Select Default Roles
		 * @function onDefaultRoles
		 */
		onDefaultRoles: function () {
			var oModelFunctionalRolesDisplayed = sap.ui.getCore().getModel("oModelFunctionalRolesDisplayed");
			var defaultRoles = ["PRDFND", "VLVUUR"];

			for (var x = 0; x < oModelFunctionalRolesDisplayed.getData().length; x++) {
				for (var y = 0; y < oModelFunctionalRolesDisplayed.getProperty("/" + x + "/functionalRoles").length; y++) {
					for (var r = 0; r < defaultRoles.length; r++) {
						if (defaultRoles[r] === oModelFunctionalRolesDisplayed.getProperty("/" + x + "/functionalRoles/" + y + "/value")) {
							sap.ui.getCore().byId("checkBoxId_" + x + "_" + y).setSelected(true);
						}
					}
				}
			}
			_oController.onFunctionalRoleSelectedVerification();
		},

		/**
		 * Select Main Permissions
		 * @function onRoleCombinationsGroup
		 * @param {Object} oEvent - Checkbox selected
		 */
		onRoleCombinationsGroup: function (oEvent) {
			var oModelFunctionalRolesDisplayed = sap.ui.getCore().getModel("oModelFunctionalRolesDisplayed");
			var rolesCombination = [];
			var selectedFunctionalRole;
			var selectedCheckBoxId = oEvent.getSource().getId();
			var i = selectedCheckBoxId.split("checkBoxId_")[1];
			var state = oEvent.getSource().getProperty("selected");

			// Loads the associatedRoles for the selected funtional role
			for (var j = 0; j < oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles").length; j++) {
				if (sap.ui.getCore().byId("checkBoxId_" + i + "_" + j).getSelected()) {
					selectedFunctionalRole = oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles/" + j + "/value");

					// For each functional role
					if (selectedFunctionalRole === oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles/" + j + "/value")) {
						rolesCombination = oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles/" + j + "/associatedRoles");

						// Apply the logic for the associated functional roles
						for (var x = 0; x < oModelFunctionalRolesDisplayed.getData().length; x++) {
							for (var y = 0; y < oModelFunctionalRolesDisplayed.getProperty("/" + x + "/functionalRoles").length; y++) {
								var functionalRoleid = oModelFunctionalRolesDisplayed.getProperty("/" + x + "/functionalRoles/" + y + "/id");
								for (var r = 0; r < rolesCombination.length; r++) {
									if (rolesCombination[r].id === functionalRoleid) {
										sap.ui.getCore().byId("checkBoxId_" + x + "_" + y).setSelected(state); // Set selected
									}
								}
							}
						}
					}
				}
			}
			_oController.onAllItemsSelectedVerification();
		},

		/**
		 * Select Main Permissions
		 * @function onRoleCombinationsIndividual
		 * @param {Object} oEvent - Checkbox selected
		 */
		onRoleCombinationsIndividual: function (oEvent) {
			var oModelFunctionalRolesDisplayed = sap.ui.getCore().getModel("oModelFunctionalRolesDisplayed");
			var rolesCombination = [];

			var selectedCheckBoxId = oEvent.getSource().getId();
			var i_j = selectedCheckBoxId.split("checkBoxId_")[1];
			var i = i_j.split("_")[0];
			var j = i_j.split("_")[1];
			var selectedFunctionalRole = oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles/" + j + "/value");
			var state = oEvent.getSource().getProperty("selected");

			// Loads the associatedRoles for the selected funtional role
			for (var a = 0; a < oModelFunctionalRolesDisplayed.getData().length; a++) {
				for (var b = 0; b < oModelFunctionalRolesDisplayed.getProperty("/" + a + "/functionalRoles").length; b++) {

					// For the selected functional role
					if (selectedFunctionalRole === oModelFunctionalRolesDisplayed.getProperty("/" + a + "/functionalRoles/" + b + "/value")) {
						rolesCombination = oModelFunctionalRolesDisplayed.getProperty("/" + a + "/functionalRoles/" + b + "/associatedRoles");
						var x, y;
						// Applies the necessary logic for each selectBox
						for (x = 0; x < oModelFunctionalRolesDisplayed.getData().length; x++) {
							for (y = 0; y < oModelFunctionalRolesDisplayed.getProperty("/" + x + "/functionalRoles").length; y++) {
								var functionalRoleid = oModelFunctionalRolesDisplayed.getProperty("/" + x + "/functionalRoles/" + y + "/id");
								for (var r = 0; r < rolesCombination.length; r++) {
									if (rolesCombination[r].id === functionalRoleid) {
										sap.ui.getCore().byId("checkBoxId_" + x + "_" + y).setSelected(state); // Set selected
									}
								}
							}
						}
					}
				}
			}
		},

		/**
		 * Select Main Permissions
		 * @function onGetCheckSelected
		 */
		onGetCheckSelected: function () {
			var i, j;
			var oModelFunctionalRolesDisplayed = sap.ui.getCore().getModel("oModelFunctionalRolesDisplayed");
			var updatedFunctionalRolesList = [];
			var updatedFunctionalRolesListValue = [];

			for (i = 0; i < oModelFunctionalRolesDisplayed.getData().length; i++) {
				for (j = 0; j < oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles").length; j++) {
					if (sap.ui.getCore().byId("checkBoxId_" + i + "_" + j).getSelected()) {
						updatedFunctionalRolesList.push(
							oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles/" + j + "/value")
						);
						updatedFunctionalRolesListValue.push({
							"value": oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles/" + j + "/value")
						});
					}
				}
			}

			switch (_dialogLayoutStyle) {
			case "Create":
				return updatedFunctionalRolesListValue;

			case "Clone":
				return updatedFunctionalRolesListValue;

			case "Edit":
				var isEqual = true;
				var oModelSelectedUser = sap.ui.getCore().getModel("oModelSelectedUser").getData();
				var previousfunctionalRolesList = [];
				for (i = 0; i < oModelSelectedUser.functionalrolesAssociated.length; i++) {
					for (j = 0; j < oModelSelectedUser.functionalrolesAssociated[i].functionalRoles.length; j++) {
						previousfunctionalRolesList.push(
							oModelSelectedUser.functionalrolesAssociated[i].functionalRoles[j].value
						);
					}
				}

				// Comparation between arrays
				if (previousfunctionalRolesList.length !== updatedFunctionalRolesList.length) {
					isEqual = false;
				} else {
					for (i = 0; i < updatedFunctionalRolesList.length; i++) {
						if (previousfunctionalRolesList.indexOf(updatedFunctionalRolesList[i]) === -1) {
							isEqual = false;
						}
					}
				}

				if (isEqual) {
					return null;
				} else {
					return updatedFunctionalRolesListValue;
				}
				break;
			default:
				break;

			}
		},

		/**
		 * Validates First name
		 * @function onCheckInputFirstname
		 */
		onCheckInputFirstname: function () {
			var firstnameInput = sap.ui.getCore().byId("firstnameInput");
			if (firstnameInput.getValue().length <= 0) {
				firstnameInput.setValueState("Warning");
				_firstnameInputOk = false;
			} else {
				firstnameInput.setValueState("None");
				_firstnameInputOk = true;
			}
			this.enableUserCreateEditButton();
		},

		/**
		 * Validates Last name
		 * @function onCheckInputLastname
		 */
		onCheckInputLastname: function () {
			var lastnameInput = sap.ui.getCore().byId("lastnameInput");
			if (lastnameInput.getValue().length <= 0) {
				lastnameInput.setValueState("Warning");
				_lastnameInputOk = false;
			} else {
				lastnameInput.setValueState("None");
				_lastnameInputOk = true;
			}
			this.enableUserCreateEditButton();
		},

		/**
		 * Validates Check Email
		 * @function onCheckInputEmail
		 */
		onCheckInputEmail: function () {
			var emailInput = sap.ui.getCore().byId("emailInput");
			var usernameInput = sap.ui.getCore().byId("usernameInput");
			// Live Change
			if (_dialogLayoutStyle === "Create" || _dialogLayoutStyle === "Clone") {
				usernameInput.setValue(emailInput.getValue());
				this.onCheckInputUsername();
			}

			var regex =
				/^[A-Za-z0-9._%+-]*[A-Za-z0-9]+@+[A-Za-z0-9]+[A-Za-z0-9.-]*\.[A-Za-z]{2,}$/;

			if (regex.test(emailInput.getValue())) {
				emailInput.setValueState("None");
				emailInput.removeStyleClass("warningInputField");
				_emailInputOk = true;
			} else {
				emailInput.setValueStateText(_oController.getResourceBundle().getText("UMCUText2IT"));
				emailInput.setValueState("Warning");
				emailInput.addStyleClass("warningInputField");
				_emailInputOk = false;
			}

			this.enableUserCreateEditButton();
		},

		/**
		 * Function that handles the success case of the ajax call that validates the inputed username 
		 * @function onCheckInputUsernameValidationSuccess
		 */
		onCheckInputUsernameValidationSuccess: function (oController, oData, oPiggyBack) {
			var usernameInput = sap.ui.getCore().byId("usernameInput");
			var validCrossUserM = sap.ui.getCore().byId("validCrossUserM");
			var declineCrossUserM = sap.ui.getCore().byId("declineCrossUserM");
			if (oData.userAlreadyExists) {
				usernameInput.setValueStateText(oController.getResourceBundle().getText("UMUserExists"));
				usernameInput.setValueState("Error");
				declineCrossUserM.setVisible(true);
				validCrossUserM.setVisible(false);
				usernameInput.addStyleClass("errorInputField");
				_usernameInputOk = false;
			} else {
				usernameInput.removeStyleClass("errorInputField");
				usernameInput.setValueState("None");
				declineCrossUserM.setVisible(false);
				validCrossUserM.setVisible(true);
				_usernameInputOk = true;
			}
			oController.enableUserCreateEditButton();
		},

		/**
		 * Function that handles the error case of the ajax call that validates the inputed username
		 * @function onCheckInputUsernameValidationError
		 */
		onCheckInputUsernameValidationError: function (oController, oError, oPiggyBack) {
			var usernameInput = sap.ui.getCore().byId("usernameInput");
			usernameInput.setValueStateText(oController.getResourceBundle().getText("UMUserExists"));
			usernameInput.setValueState("Error");
			_usernameInputOk = false;
			oController.enableUserCreateEditButton();
		},

		/**
		 * Check Input Username validation
		 * @ parameter
		 */
		onCheckInputUsernameValidation: function () {
			_oController.setUserCreateEditButtonBusy(true);
			setTimeout(function () {
				var path = "validation?username=";
				var usernameInput = sap.ui.getCore().byId("usernameInput");
				var validCrossUserM = sap.ui.getCore().byId("validCrossUserM");
				var declineCrossUserM = sap.ui.getCore().byId("declineCrossUserM");
				var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();

				var username = usernameInput.getValue();
				var xsURL = "/usermgmt/user/" + path + username + "&lang=" + selectedLanguage;
				if (_validateUserNameExistant) {
					//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
					_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onCheckInputUsernameValidationSuccess,
						_oController.onCheckInputUsernameValidationError);
				} else {
					validCrossUserM.setVisible(false);
					declineCrossUserM.setVisible(false);
					sap.ui.getCore().byId("createUserButton").setBusy(false);
				}
			}, 0);

		},

		/**
		 * Validates Input Username
		 * @function onCheckInputUsername
		 */
		onCheckInputUsername: function () {
			var usernameInput = sap.ui.getCore().byId("usernameInput");
			var validCrossUserM = sap.ui.getCore().byId("validCrossUserM");
			var declineCrossUserM = sap.ui.getCore().byId("declineCrossUserM");
			_usernameInputOk = false;
			usernameInput.removeStyleClass("errorInputField");
			usernameInput.setValueState("None");
			validCrossUserM.setVisible(false);
			declineCrossUserM.setVisible(false);

			var regex = /.*[*()&! ]{1}.*/;

			if (_dialogLayoutStyle === "Create" || _dialogLayoutStyle === "Clone") {
				if (usernameInput.getValue().length <= 0) {
					usernameInput.setValueStateText(_oController.getResourceBundle().getText("UMCUText1IT"));
					usernameInput.setValueState("Warning");
					_usernameInputOk = false;
					_validateUserNameExistant = false;

				} else if (regex.test(usernameInput.getValue())) {
					usernameInput.setValueStateText(_oController.getResourceBundle().getText("UMCUText3IT"));
					usernameInput.setValueState("Warning");
					_usernameInputOk = false;
					_validateUserNameExistant = false;
				} else {
					usernameInput.removeStyleClass("errorInputField");
					usernameInput.setValueState("None");
					_validateUserNameExistant = true;
				}
			} else {
				_usernameInputOk = true;
			}
			this.enableUserCreateEditButton();
		},

		/**
		 * Validates Account Access
		 * @function onCheckAccountAccess
		 */
		onCheckAccountAccess: function () {
			var accountAccessInput = sap.ui.getCore().byId("accountAccessVerification"); // Account Access

			if (_dialogLayoutStyle === "Create" || _dialogLayoutStyle === "Clone") {
				if (!_PrimaryAccountId || !_AccountAccessIdList) {
					_accountAccessInputOK = false;
				} else {
					_accountAccessInputOK = true;
					accountAccessInput.setVisible(false);
				}
			} else {
				_accountAccessInputOK = true;
			}
			this.enableUserCreateEditButton();
		},

		/**
		 * Functional Roles verification the State of all secondary checkboxes
		 * @function onFunctionalRoleSelectedVerification
		 */
		onFunctionalRoleSelectedVerification: function () {
			var oModelFunctionalRolesDisplayed = sap.ui.getCore().getModel("oModelFunctionalRolesDisplayed");
			_FunctionalRoleSelected = false;
			for (var i = 0; i < oModelFunctionalRolesDisplayed.getData().length; i++) { // Cycle all the Items of each Role Panel
				for (var j = 0; j < oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles").length; j++) {
					if (sap.ui.getCore().byId("checkBoxId_" + i + "_" + j).getSelected()) {
						_FunctionalRoleSelected = true;
						_functionalRolesOk = true;
					}
				}
			}
			if (_dialogLayoutStyle === "Create" || _dialogLayoutStyle === "Edit" || _dialogLayoutStyle === "Clone") {
				var updatedFunctionalRolesListValue = [];
				for (i = 0; i < oModelFunctionalRolesDisplayed.getData().length; i++) {
					for (j = 0; j < oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles").length; j++) {
						if (sap.ui.getCore().byId("checkBoxId_" + i + "_" + j).getSelected()) {
							updatedFunctionalRolesListValue.push({
								"value": oModelFunctionalRolesDisplayed.getProperty("/" + i + "/functionalRoles/" + j + "/value")
							});
						}
					}
				}
				var functionalRolesList = updatedFunctionalRolesListValue;
				if (functionalRolesList.length === 1 && functionalRolesList[0].value === "VIWPRC") {
					_functionalRolesOk = false;
					sap.ui.getCore().byId("aditionalRole").setVisible(true);
					sap.ui.getCore().byId("roleVerification").setVisible(true);
				} else {
					sap.ui.getCore().byId("aditionalRole").setVisible(false);
					sap.ui.getCore().byId("roleVerification").setVisible(false);
					if (_FunctionalRoleSelected) {
						_functionalRolesOk = true;
					} else {
						_functionalRolesOk = false;
					}
				}
			}
			this.enableUserCreateEditButton();
		},

		/**
		 *
		 */
		enableUserCreateEditButton: function () {
			var createUserButton = sap.ui.getCore().byId("createUserButton");
			if (_firstnameInputOk && _lastnameInputOk && _usernameInputOk && _emailInputOk && _accountAccessInputOK && _functionalRolesOk) {
				createUserButton.setEnabled(true);
			} else {
				createUserButton.setEnabled(false);
			}
			createUserButton.setBusy(false);
		},
		/**
		 * Enables busy indicator on UserCreate Edit Button
		 * 
		 */
		setUserCreateEditButtonBusy: function (state) {
			var createUserButton = sap.ui.getCore().byId("createUserButton");
			if (state === undefined) {
				state = true;
			}
			createUserButton.setBusyIndicatorDelay(0);
			createUserButton.setBusy(state);
		},

		/**
		 * Function that handles the success case of the ajax call that creates a user
		 * @function onCreateUserSuccess
		 */
		onCreateUserSuccess: function (oController, oData, oPiggyBack) {
			oController.dialogFragmentCreateEditViewUser.setBusy(false);
			_oController.bManageUserDialog = false;
			oController.closeDialogCreateUser({
				"mParameters": {
					"id": "create"
				}
			});
			oController.loadModelUserManagement();
			oController.onShowMessage(oController, "Success", "", oController.getResourceBundle().getText("UMUserCreateSuccess"));
		},

		/**
		 * Function that handles the error case of the ajax call that creates a user
		 * @function onCreateUserError
		 */
		onCreateUserError: function (oController, oError, oPiggyBack) {
			oController.dialogFragmentCreateEditViewUser.setBusy(false);
			if (oError.status === 409) {
				oController.onShowMessageStrip(oController.getResourceBundle().getText("UMUserExists"), "Warning");
			} else {
				oController.onShowMessageStrip(oError.status + " " + oController.getResourceBundle().getText("UMErrorDuringOperation"),
					"Error");
			}
		},

		/**
		 * Calls the service responsible for creating a user
		 * @function onCreateUser
		 */
		onCreateUser: function () {
			//GTM
			_oController.GTMDataLayer('creatuserform',
				'User Management',
				'Create User-click',
				sap.ui.getCore().getModel("oAccounts").getData().uuid
			);

			var userInformationModel = sap.ui.getCore().getModel("UserInformation");

			// Set Busy Indicatores
			this.dialogFragmentCreateEditViewUser.setBusyIndicatorDelay(0);
			this.dialogFragmentCreateEditViewUser.setBusy(true);

			// Values based on inputs
			var firstnameInput = sap.ui.getCore().byId("firstnameInput").getValue();
			var lastnameInput = sap.ui.getCore().byId("lastnameInput").getValue();
			var usernameInput = sap.ui.getCore().byId("usernameInput").getValue();
			var emailInput = sap.ui.getCore().byId("emailInput").getValue();
			var status = sap.ui.getCore().byId("statusInput").getSelectedKey();
			var functionalRolesList = this.onGetCheckSelected();

			// Send all the Parameters
			var user = {};
			user.username = usernameInput;
			user.firstname = firstnameInput;
			user.lastname = lastnameInput;
			user.email = emailInput;

			//acounts
			var accounts = sap.ui.getCore().getModel("UserInformation").getData().accounts;
			if (accounts.length === 1) {
				user.directAccount = accounts[0].id;
				user.accounts = accounts;
			} else {
				user.directAccount = _PrimaryAccountId;
				user.accounts = _AccountAccessIdList;
			}
			user.functionalroles = functionalRolesList;
			user.status = status;
			user.admin = "F";
			user.ownerUuid = userInformationModel.getProperty("/uuid");
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();

			var userjsonString = JSON.stringify(user);
			var xsURL = "/usermgmt/user/" + "?lang=" + selectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "POST", _oController.onCreateUserSuccess, _oController.onCreateUserError,
				userjsonString);
		},

		/**
		 * Function that handles the success case of the ajax call to update the user
		 * @function onUpdateUserSuccess
		 */
		onUpdateUserSuccess: function (oController, oData, oPiggyBack) {

			oController.dialogFragmentCreateEditViewUser.setBusy(false);
			_oController.bManageUserDialog = false;
			oController.closeDialogCreateUser({
				"mParameters": {
					"id": "update"
				}
			});
			oController.loadModelUserManagement();

			if (oPiggyBack.xhr.status === 207) {
				oController.onShowMessage(oController, "Warning", "", oController.getResourceBundle().getText("UMUserUpdateRestrictions"));
			} else {
				oController.onShowMessage(oController, "Success", "", oController.getResourceBundle().getText("UMUserUpdateSuccess"));
			}

		},

		/**
		 * Function that handles the error case of the ajax call to update the user
		 * @function onUpdateUserError
		 */
		onUpdateUserError: function (oController, oError, oPiggyBack) {
			oController.dialogFragmentCreateEditViewUser.setBusy(false);
			if (oError.status === 409) {
				oController.onShowMessageStrip(oController.getResourceBundle().getText("UMUserExists"), "Warning");
			} else {
				oController.onShowMessageStrip(oError.status + " " + oController.getResourceBundle().getText("UMErrorDuringOperation"),
					"Error");
			}
		},

		/**
		 * Calls the service responsible for updating a user info
		 * @function onUpdateUser
		 */
		onUpdateUser: function () {
			var userInformationModel = sap.ui.getCore().getModel("UserInformation");
			var oModelSelectedUser = sap.ui.getCore().getModel("oModelSelectedUser").getData();

			// Set Busy Indicatores
			this.dialogFragmentCreateEditViewUser.setBusyIndicatorDelay(0);
			this.dialogFragmentCreateEditViewUser.setBusy(true);

			// Data to be updated
			var selectedUserUuid = oModelSelectedUser.uuid;
			var firstnameInput = sap.ui.getCore().byId("firstnameInput").getValue(); // Values based on inputs
			var lastnameInput = sap.ui.getCore().byId("lastnameInput").getValue();
			var emailInput = sap.ui.getCore().byId("emailInput").getValue();
			var status = sap.ui.getCore().byId("statusInput").getSelectedKey();
			var functionalRolesList = this.onGetCheckSelected();
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();

			// Save the Object
			var user = {};
			user.firstname = firstnameInput;
			user.lastname = lastnameInput;
			user.email = emailInput;

			//acounts
			var accounts = sap.ui.getCore().getModel("UserInformation").getData().accounts;
			if (accounts.length === 1) {
				user.directAccount = accounts[0].id;
				user.accounts = accounts;
			} else {
				user.directAccount = _PrimaryAccountId;
				user.accounts = _AccountAccessIdList;
			}

			user.status = status;
			user.admin = "F";
			user.ownerUuid = userInformationModel.getProperty("/uuid");
			user.functionalroles = functionalRolesList;

			var userjsonString = JSON.stringify(user);
			var xsURL = "/usermgmt/user/" + selectedUserUuid + "?lang=" + selectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "PUT", _oController.onUpdateUserSuccess, _oController.onUpdateUserError,
				userjsonString);

			//GTM
			_oController.GTMDataLayer('listactivitiesevent',
				'User Management',
				'List Activites -click',
				'Update'
			);
		},

		/**
		 * Closes the CreateUser Fragment
		 * @function closeDialogCreateUser
		 */
		closeDialogCreateUser: function (oEvent) {
			//GTM
			if (oEvent.mParameters.id === "closeUserButton") {
				_oController.GTMDataLayer('creatuserform',
					'User Management',
					'Cancel-click',
					sap.ui.getCore().getModel("oAccounts").getData().uuid
				);
			}

			this.dialogFragmentCreateEditViewUser.close();
			this.dialogFragmentCreateEditViewUser.destroy();
			// Open the Manage User Dialog
			if (_oController.bManageUserDialog && _oController.oManageUserContext) {
				_oController.bCloseAfterManage = true;
				_oController.onPressViewManageUser();
				return;
			}

			_PrimaryAccountId = null;
			_AccountAccessIdList = null;
		},
		/* End of Screen 2 "Create/Edit/View User"   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
		/* Start of Screen 3 "Account Access"  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
		/**
		 * Opens the AccountAccess Fragment
		 * @function toAccountAccess
		 */
		toAccountAccess: function () {
			if (this.dialogFragmentAccountAccess) {
				this.dialogFragmentAccountAccess.destroyContent();
			}
			this.dialogFragmentAccountAccess = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.AccountAccess",
				_oController);
			this.dialogFragmentAccountAccess.open();
			this._removeDialogResize(this.dialogFragmentAccountAccess);
			$("#accountAccessSearchField-I").attr("readonly", false);

			this.resizeToMobile(sap.ui.Device.media.getCurrentRange(DASH_MOBILE));

			switch (_dialogLayoutStyle) {
				//GTM
			case "Create":
				_oController.GTMDataLayer('creatuserform',
					'User Management',
					'Account Access-click',
					sap.ui.getCore().getModel("oAccounts").getData().uuid
				);
				break;
			case "Edit":
				_oController.GTMDataLayer('listactivitiesevent',
					'User Management',
					'List Activites-click',
					'Account Access-Edit'
				);
				break;
			case "View":
				_oController.GTMDataLayer('listactivitiesevent',
					'User Management',
					'List Activites-click',
					'Account Access-View'
				);
				break;
			default:
				break;
			}

			var i18nModel = new sap.ui.model.resource.ResourceModel({
				bundleUrl: "i18n/i18n.properties"
			});
			this.dialogFragmentAccountAccess.setModel(i18nModel, "i18n");
			this.showHideAsterisk();
			this.configureAccountAccessButtons();
			setTimeout(function () {
				$(":focus").blur();
			}, 0);
		},

		/**
		 * Closes the AccountAcess Fragment
		 * @function toAccountAccess
		 */
		cancelDialogAccountAccess: function () {
			var modelData = [];
			for (var i = 0; i < _oModelAccountAccessSortedDataSaveOnDone.getData().length; i++) {
				var account = {
					accountname: _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i + "/accountname"),
					accountnum: _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i + "/accountnum"),
					accountnumber: _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i + "/accountnumber"),
					billingcity: _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i + "/billingcity"),
					billingcountry: _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i + "/billingcountry"),
					billingpostalcode: _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i + "/billingpostalcode"),
					billingstate: _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i + "/billingstate"),
					billingstreet: _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i + "/billingstreet"),
					id: _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i + "/id"),
					isEnabled: _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i + "/isEnabled"),
					isPrimaryAccount: _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i + "/isPrimaryAccount"),
					isSelected: _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i + "/isSelected"),
					shippingstreet: _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i + "/shippingstreet"),
					isRadioEnabled: _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i + "/isRadioEnabled")
				};
				if (account.isPrimaryAccount) {
					_PrimaryAccountId = account.id;
				}
				modelData.push(account);
			}

			_oModelAccountAccessSortedData.setData(modelData);
			_oController.onGetAccountAccess();
			if (_accountAccessHasChanged) {
				for (i = 0; i < _oModelAccountAccessSortedData.getData().length; i++) {
					_oModelAccountAccessSortedData.setProperty("/" + i + "/isPrimaryAccount", _oModelAccountAccessSortedDataSaveOnDone.getProperty(
						"/" + i + "/isPrimaryAccount"));
					_oModelAccountAccessSortedData.setProperty("/" + i + "/isSelected", _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i +
						"/isSelected"));
					_oModelAccountAccessSortedData.setProperty("/" + i + "/isEnabled", _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" + i +
						"/isEnabled"));
					_oModelAccountAccessSortedData.setProperty("/" + i + "/isRadioEnabled", _oModelAccountAccessSortedDataSaveOnDone.getProperty("/" +
						i +
						"/isRadioEnabled"));

				}
			}

			//reset table model
			_beginAccountAccess = 0; // Sets the arrows to 0
			_oModelAccountAccessTable = _oModelAccountAccessSortedData;

			this.dialogFragmentAccountAccess.close();
			this.dialogFragmentAccountAccess.destroy();

			switch (_dialogLayoutStyle) {
			case "Create":
				_oController.GTMDataLayer('accountAccessPopup',
					'User Management',
					'Account Access Popup -Opened-Create',
					"Cancel"
				);
				break;
			case "Edit":
				_oController.GTMDataLayer('accountAccessPopup',
					'User Management',
					'Account Access Popup -Opened-Edit',
					"Cancel"
				);
				break;
			case "View":
				_oController.GTMDataLayer('accountAccessPopup',
					'User Management',
					'Account Access Popup -Opened-View',
					"Cancel"
				);
				break;
			default:
				break;
			}

		},

		doneDialogAccountAccess: function () {
			var modelData = [];
			for (var i = 0; i < _oModelAccountAccessSortedData.getData().length; i++) {
				var account = {
					accountname: _oModelAccountAccessSortedData.getProperty("/" + i + "/accountname"),
					accountnum: _oModelAccountAccessSortedData.getProperty("/" + i + "/accountnum"),
					accountnumber: _oModelAccountAccessSortedData.getProperty("/" + i + "/accountnumber"),
					billingcity: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingcity"),
					billingcountry: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingcountry"),
					billingpostalcode: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingpostalcode"),
					billingstate: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingstate"),
					billingstreet: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingstreet"),
					id: _oModelAccountAccessSortedData.getProperty("/" + i + "/id"),
					isEnabled: _oModelAccountAccessSortedData.getProperty("/" + i + "/isEnabled"),
					isPrimaryAccount: _oModelAccountAccessSortedData.getProperty("/" + i + "/isPrimaryAccount"),
					isSelected: _oModelAccountAccessSortedData.getProperty("/" + i + "/isSelected"),
					shippingstreet: _oModelAccountAccessSortedData.getProperty("/" + i + "/shippingstreet"),
					isRadioEnabled: _oModelAccountAccessSortedData.getProperty("/" + i + "/isRadioEnabled")
				};

				modelData.push(account);
			}

			_oModelAccountAccessSortedDataSaveOnDone.setData(modelData);

			//reset table model
			_beginAccountAccess = 0; // Sets the arrows to 0
			_oModelAccountAccessTable = _oModelAccountAccessSortedData;

			this.dialogFragmentAccountAccess.close();
			this.dialogFragmentAccountAccess.destroy();

			switch (_dialogLayoutStyle) {
			case "Create":
				this.onGetAccountAccess();
				this.onCheckAccountAccess();
				_accountAccessHasChanged = true;
				_oController.GTMDataLayer('accountAccessPopup',
					'User Management',
					'Account Access Popup -Opened-Create',
					"Done"
				);
				break;
			case "Edit":
				this.onGetAccountAccess();
				_accountAccessHasChanged = true;
				_oController.GTMDataLayer('accountAccessPopup',
					'User Management',
					'Account Access Popup -Opened-Edit',
					"Done"
				);
				break;
			default:
				break;
			}
		},

		/**
		 * Loads the Accounts Information to populate the table
		 * @function loadModelAccountAccessInfo
		 */
		onAccountAccessConfiguration: function () {
			var oModelLoggedUserInformation = sap.ui.getCore().getModel("UserInformation");
			var oModelSelectedUser = sap.ui.getCore().getModel("oModelSelectedUser");
			var oModelAdminAccounts = new sap.ui.model.json.JSONModel();
			var oModelSelectedUserAccounts = new sap.ui.model.json.JSONModel();
			var adminAccountId, selectedUserAccountId, selectedUserPrimaryAccount;
			var i, j;

			oModelAdminAccounts.setData(oModelLoggedUserInformation.getProperty("/accounts"));

			switch (_dialogLayoutStyle) {
			case "Create":
				for (i = 0; i < oModelAdminAccounts.getData().length; i++) {
					oModelAdminAccounts.setProperty("/" + i + "/isEnabled", true);
					oModelAdminAccounts.setProperty("/" + i + "/isSelected", false);
					oModelAdminAccounts.setProperty("/" + i + "/isPrimaryAccount", false);
					oModelAdminAccounts.setProperty("/" + i + "/isRadioEnabled", true);
				}
				// Sets the right data to the table
				_oModelAccountAccessTable = oModelAdminAccounts;
				_oModelAccountAccessSortedData = oModelAdminAccounts;
				break;
			case "View":

				selectedUserPrimaryAccount = oModelSelectedUser.getProperty("/directAccount");
				oModelSelectedUserAccounts.setData(oModelSelectedUser.getProperty("/accounts"));

				for (i = 0; i < oModelSelectedUserAccounts.getData().length; i++) {
					oModelSelectedUserAccounts.setProperty("/" + i + "/isEnabled", false);
					oModelSelectedUserAccounts.setProperty("/" + i + "/isSelected", true);
					if (oModelSelectedUserAccounts.getProperty("/" + i + "/id") === selectedUserPrimaryAccount) {
						oModelSelectedUserAccounts.setProperty("/" + i + "/isPrimaryAccount", true);
					}
					oModelSelectedUserAccounts.setProperty("/" + i + "/isRadioEnabled", false);
				}

				// Sets the right data to the table
				_oModelAccountAccessTable = oModelSelectedUserAccounts;
				_oModelAccountAccessSortedData = oModelSelectedUserAccounts;
				break;
			case "Edit":

				selectedUserPrimaryAccount = oModelSelectedUser.getProperty("/directAccount");
				oModelSelectedUserAccounts.setData(oModelSelectedUser.getProperty("/accounts"));

				for (i = 0; i < oModelAdminAccounts.getData().length; i++) {
					adminAccountId = oModelAdminAccounts.getProperty("/" + i + "/id");
					oModelAdminAccounts.setProperty("/" + i + "/isEnabled", true);
					oModelAdminAccounts.setProperty("/" + i + "/isSelected", false);
					oModelAdminAccounts.setProperty("/" + i + "/isPrimaryAccount", false);
					oModelAdminAccounts.setProperty("/" + i + "/isRadioEnabled", true);
					for (j = 0; j < oModelAdminAccounts.getData().length; j++) {
						selectedUserAccountId = oModelSelectedUserAccounts.getProperty("/" + j + "/id");
						if (adminAccountId === selectedUserAccountId) {
							oModelAdminAccounts.setProperty("/" + i + "/isEnabled", true);
							oModelAdminAccounts.setProperty("/" + i + "/isSelected", true);
						}
						if (oModelAdminAccounts.getProperty("/" + i + "/id") === selectedUserPrimaryAccount) {
							oModelAdminAccounts.setProperty("/" + i + "/isPrimaryAccount", true);
							oModelAdminAccounts.setProperty("/" + i + "/isEnabled", false);
						}
					}
				}

				// Sets the right data to the table
				_oModelAccountAccessTable = oModelAdminAccounts;
				_oModelAccountAccessSortedData = oModelAdminAccounts;

				// Gets the Account Access
				this.onGetAccountAccess();
				break;

			case "Clone":

				selectedUserPrimaryAccount = oModelSelectedUser.getProperty("/directAccount");
				oModelSelectedUserAccounts.setData(oModelSelectedUser.getProperty("/accounts"));

				for (i = 0; i < oModelAdminAccounts.getData().length; i++) {
					adminAccountId = oModelAdminAccounts.getProperty("/" + i + "/id");
					oModelAdminAccounts.setProperty("/" + i + "/isEnabled", true);
					oModelAdminAccounts.setProperty("/" + i + "/isSelected", false);
					oModelAdminAccounts.setProperty("/" + i + "/isPrimaryAccount", false);
					oModelAdminAccounts.setProperty("/" + i + "/isRadioEnabled", true);
					for (j = 0; j < oModelAdminAccounts.getData().length; j++) {
						selectedUserAccountId = oModelSelectedUserAccounts.getProperty("/" + j + "/id");
						if (adminAccountId === selectedUserAccountId) {
							oModelAdminAccounts.setProperty("/" + i + "/isEnabled", true);
							oModelAdminAccounts.setProperty("/" + i + "/isSelected", true);
						}
						if (oModelAdminAccounts.getProperty("/" + i + "/id") === selectedUserPrimaryAccount) {
							oModelAdminAccounts.setProperty("/" + i + "/isPrimaryAccount", true);
							oModelAdminAccounts.setProperty("/" + i + "/isEnabled", false);
						}
					}
				}

				// Sets the right data to the table
				_oModelAccountAccessTable = oModelAdminAccounts;
				_oModelAccountAccessSortedData = oModelAdminAccounts;

				// Gets the Account Access
				this.onGetAccountAccess();
				break;
			default:
				break;
			}
		},

		/**
		 * Filters the table
		 * @function onLiveSearchAccountAccess
		 * @param {Object} oEvent - Account Access input data
		 */
		onLiveSearchAccountAccess: function (oEvent) {
			var newValue = oEvent.getParameter("newValue");
			var oDataAccountAccessTableFiltered = [];
			var oModelAccountAccessTableFiltered = new sap.ui.model.json.JSONModel();

			if (!newValue) {
				_beginAccountAccess = 0; // Sets the arrows to 0
				_oModelAccountAccessTable = _oModelAccountAccessSortedData;
				this.tablePaginationAccountAccess();
			} else {
				var aFilter = [
					new sap.ui.model.Filter("accountname", sap.ui.model.FilterOperator.Contains, newValue),
					new sap.ui.model.Filter("sapaccountname2", sap.ui.model.FilterOperator.Contains, newValue),
					new sap.ui.model.Filter("accountnum", sap.ui.model.FilterOperator.Contains, newValue),
					new sap.ui.model.Filter("billingstreet", sap.ui.model.FilterOperator.Contains, newValue),
					new sap.ui.model.Filter("billingcity", sap.ui.model.FilterOperator.Contains, newValue),
					new sap.ui.model.Filter("billingstate", sap.ui.model.FilterOperator.Contains, newValue),
					new sap.ui.model.Filter("billingpostalcode", sap.ui.model.FilterOperator.Contains, newValue)
				];

				var filter = new sap.ui.model.Filter(aFilter, false); //False means it will apply an OR logic, if you want AND pass true
				sap.ui.getCore().byId("AccountAccessTable").setModel(_oModelAccountAccessSortedData, "AccountAccess");
				sap.ui.getCore().byId("AccountAccessTable").getBinding("items").filter(filter);

				var itemsIndices = sap.ui.getCore().byId("AccountAccessTable").getBinding("items").aIndices;
				for (var i = 0; i < itemsIndices.length; i++) {
					oDataAccountAccessTableFiltered.push(_oModelAccountAccessSortedData.getProperty("/" + itemsIndices[i]));
				}

				_beginAccountAccess = 0; // Sets the arrows to 0
				oModelAccountAccessTableFiltered.setData(oDataAccountAccessTableFiltered);
				_oModelAccountAccessTable = oModelAccountAccessTableFiltered;
				this.tablePaginationAccountAccess();
			}
		},

		/**
		 * Change the visual of the Account Access flag icon
		 * @function onSelectFlag
		 * @param {Object} oEvent - The flag icon selected
		 */
		onSelectFlag: function (oEvent) {
			// Runs only if its the Edit or Create dialog
			if (_dialogLayoutStyle !== "View") {
				var tablePath = oEvent.getSource().getBindingContext("AccountAccess").getPath();
				var tableModel = oEvent.getSource().getBindingContext("AccountAccess").getModel();
				var i;

				// Starts by enabling all the flags When pressing a flag
				for (i = 0; i < tableModel.getData().length; i++) {
					tableModel.setProperty("/" + i + "/isEnabled", true);
				}

				for (i = 0; i < _oModelAccountAccessSortedData.getData().length; i++) {
					_oModelAccountAccessSortedData.setProperty("/" + i + "/isPrimaryAccount", false);
					_oModelAccountAccessSortedData.setProperty("/" + i + "/isEnabled", true);
				}

				for (i = 0; i < tableModel.getData().length; i++) {
					tableModel.setProperty("/" + i + "/isPrimaryAccount", false);
				}

				tableModel.setProperty(tablePath + "/isPrimaryAccount", true);
				tableModel.setProperty(tablePath + "/isSelected", true);
				tableModel.setProperty(tablePath + "/isEnabled", false);
			}

			_oController.showHideAsterisk();

		},

		/**
		 * Get Account Access
		 * @function onGetAccountAccess
		 */
		onGetAccountAccess: function () {
			var PrimaryAccountAddress, bPrimaryAccountSel = false;
			_AccountAccessIdList = [];

			var modelData = [];
			for (var i = 0; i < _oModelAccountAccessSortedData.getData().length; i++) {
				if (_isFirst) {
					//if it's the first time, replicate model
					var account = {
						accountname: _oModelAccountAccessSortedData.getProperty("/" + i + "/accountname"),
						accountnum: _oModelAccountAccessSortedData.getProperty("/" + i + "/accountnum"),
						accountnumber: _oModelAccountAccessSortedData.getProperty("/" + i + "/accountnumber"),
						billingcity: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingcity"),
						billingcountry: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingcountry"),
						billingpostalcode: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingpostalcode"),
						billingstate: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingstate"),
						billingstreet: _oModelAccountAccessSortedData.getProperty("/" + i + "/billingstreet"),
						id: _oModelAccountAccessSortedData.getProperty("/" + i + "/id"),
						isEnabled: _oModelAccountAccessSortedData.getProperty("/" + i + "/isEnabled"),
						isPrimaryAccount: _oModelAccountAccessSortedData.getProperty("/" + i + "/isPrimaryAccount"),
						isSelected: _oModelAccountAccessSortedData.getProperty("/" + i + "/isSelected"),
						shippingstreet: _oModelAccountAccessSortedData.getProperty("/" + i + "/shippingstreet"),
						isRadioEnabled: _oModelAccountAccessSortedData.getProperty("/" + i + "/isRadioEnabled")
					};
					modelData.push(account);
				}

				if (_oModelAccountAccessSortedData.getProperty("/" + i + "/isSelected") === true) {
					_AccountAccessIdList.push({
						"id": _oModelAccountAccessSortedData.getProperty("/" + i + "/id")
					});
				}
				if (_oModelAccountAccessSortedData.getProperty("/" + i + "/isPrimaryAccount")) {
					bPrimaryAccountSel = true;
					PrimaryAccountAddress = this.formatterContactAddress(_oModelAccountAccessSortedData.getProperty("/" + i + "/billingstreet"),
						_oModelAccountAccessSortedData.getProperty("/" + i + "/billingcity"), _oModelAccountAccessSortedData.getProperty("/" +
							i +
							"/billingstate"), _oModelAccountAccessSortedData.getProperty("/" + i + "/billingpostalcode"));

					_PrimaryAccountId = _oModelAccountAccessSortedData.getProperty("/" + i + "/id");
					sap.ui.getCore().byId("primaryaccountLabel").setText(PrimaryAccountAddress);
				}
			}
			if (!bPrimaryAccountSel) {
				_PrimaryAccountId = null;
				sap.ui.getCore().byId("primaryaccountLabel").setText(_oController.getResourceBundle().getText("UMCUText6LB"));
			}

			if (_isFirst) {
				_oModelAccountAccessSortedDataSaveOnDone.setData(modelData);
			}
			_isFirst = false;
		},

		/**
		 * Sets the layout of the arrows for the table and controls the date to be loaded in the table
		 * @function tablePaginationAccountAccess
		 */
		tablePaginationAccountAccess: function () {
			var _iLengthActive = _oModelAccountAccessTable.getData().length;
			var end = _beginAccountAccess + _iAccountAccessPageSize;
			var oDataTablePaginated = [];
			var oModelTablePaginated = new sap.ui.model.json.JSONModel();

			if (_iLengthActive === 0) {
				sap.ui.getCore().byId("navigationArrowsAccountAccess").setVisible(false);
			} else {
				sap.ui.getCore().byId("navigationArrowsAccountAccess").setVisible(true);
			}

			// Arrows and Number indicator layout
			if (_beginAccountAccess < _iAccountAccessPageSize) {
				sap.ui.getCore().byId("idLeftNavAccountAccess").setEnabled(false);
			} else {
				sap.ui.getCore().byId("idLeftNavAccountAccess").setEnabled(true);
			}
			if (end >= _iLengthActive) {
				end = _iLengthActive;
				sap.ui.getCore().byId("idRightNavAccountAccess").setEnabled(false);
			} else {
				sap.ui.getCore().byId("idRightNavAccountAccess").setEnabled(true);
			}
			if (_iLengthActive < _iAccountAccessPageSize) {
				end = _iLengthActive;
			}
			if (_iAccountAccessPageSize === 1 && _iLengthActive !== 0) {
				sap.ui.getCore().byId("idTextCountAccountAccess").setText(_beginAccountAccess + 1 + " " + _oController.getResourceBundle().getText(
						"of") + " " +
					_iLengthActive);
			} else {
				sap.ui.getCore().byId("idTextCountAccountAccess").setText(_beginAccountAccess + 1 + "-" + end + " " + _oController.getResourceBundle()
					.getText("of") + " " + _iLengthActive);
			}

			// Paginated Model
			for (var i = _beginAccountAccess; i < end; i++) {
				oDataTablePaginated.push(_oModelAccountAccessTable.getProperty("/" + i));
			}
			oModelTablePaginated.setData(oDataTablePaginated);
			sap.ui.getCore().byId("AccountAccessTable").setModel(oModelTablePaginated, "AccountAccess");
			sap.ui.getCore().byId("noDataText").setModel(oModelTablePaginated, "modelAccountAccess");
		},

		/**
		 * Loads more items into the table
		 * @function toRightAccountAccess
		 */
		toRightAccountAccess: function () {
			_beginAccountAccess = _beginAccountAccess + _iAccountAccessPageSize;
			this.tablePaginationAccountAccess();
		},

		/**
		 * Loads more items into the table
		 * @function toLeftAccountAccess
		 */
		toLeftAccountAccess: function () {
			_beginAccountAccess = _beginAccountAccess - _iAccountAccessPageSize;
			this.tablePaginationAccountAccess();
		},

		/* End of Screen 3 "Account Access"  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
		/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

		/**
		 * Generates a "No functional Roles Message" for each type of Dialog
		 * @function noPermissionMessageAction
		 * @param {Object} oModel - Functional Roles Model
		 */
		noPermissionMessageAction: function (oModel) {
			var fragmentContent;
			var noPermissionMessage;
			switch (_dialogLayoutStyle) {
			case "Create":
			case "Clone":
			case "Edit":
				if (oModel.getData().length === 0) {
					fragmentContent = sap.ui.getCore().byId("fragmentContent");
					noPermissionMessage = sap.ui.getCore().byId("noPermissionMessage");
					fragmentContent.setVisible(false);
					noPermissionMessage.setVisible(true);
				}
				break;
			case "View":
				if (oModel.getData().length === 0) {
					noPermissionMessage = sap.ui.getCore().byId("noPermissionMessage");
					noPermissionMessage.setVisible(true);
				}
				break;
			default:
				break;
			}
		},

		/**
		 * Put active or inactive status
		 * @function UMStatus
		 * @param {string} status - Status state (Active/Inactive)
		 */
		UMStatus: function (status) {
			var statusText;
			if (status === "A") {
				statusText = _oController.getResourceBundle().getText("UMActive");
			} else if (status === "I") {
				statusText = _oController.getResourceBundle().getText("UMInactive");
			}
			return statusText;
		},

		/**
		 * Put edit or view depending on username
		 * @function UMEditView
		 * @param {string} ownerUuid - Owner's ForgeRock ID
		 */
		UMEditView: function (ownerUuid) {
			var text;
			// If the owneruuid is the same of the user
			if (_uuid && _uuid === ownerUuid) {
				text = _oController.getResourceBundle().getText("UMManage");
			} else {
				text = _oController.getResourceBundle().getText("UMView");
			}
			return text;
		},

		/**
		 * PrimaryAccount Flag formatter
		 * @function PrimaryAccountFlag
		 * @param {string} isPrimaryFlag - Primary Flag state
		 */
		PrimaryAccountFlag: function (isPrimaryFlag) {
			switch (isPrimaryFlag) {
			case true:
				return "red";
			case false:
				return "lightgray";
			case undefined:
				return "lightgray";
			default:
				break;
			}
		},

		/**
		 * Determines if the Account Access Required Aestrix should be visible, according to the Clone or Create button press
		 * @function accountAccessRequiredVisible
		 * @param {array} accounts - the list of accounts of the user
		 */
		accountAccessRequiredVisible: function (accounts) {
			return _dialogLayoutStyle === "Clone" ? false : !(accounts !== undefined && accounts.length === 1);
		},

		/**
		 * Determines if the Account Access button should be visible, according to the accounts array size
		 * @function accountAccessVisible
		 * @param {array} accounts - the list of accounts of the user
		 */
		accountAccessVisible: function (accounts) {
			return !(accounts !== undefined && accounts.length === 1);
		},
		/**
		 * Checks if we should show the required asterisks or not
		 * @function showHideAsterisk
		 **/
		showHideAsterisk: function () {
			_oController.onGetAccountAccess();

			var accountAst = sap.ui.getCore().byId("accountVerification");
			var directAccountAst = sap.ui.getCore().byId("primaryFlagVerification");

			if (!_PrimaryAccountId) {
				directAccountAst.removeStyleClass("accountAccessHeaderHide");
				accountAst.setVisible(false);
			} else {
				directAccountAst.addStyleClass("accountAccessHeaderHide");
				accountAst.setVisible(true);
				sap.ui.getCore().byId("accountAccessDoneButton").setEnabled(true);
			}
		},
		/**
		 * Configure the footer buttons on the account access dialog 
		 * @function configureAccountAccessButtons
		 **/
		configureAccountAccessButtons: function () {

			var doneButton = sap.ui.getCore().byId("accountAccessDoneButton");
			var cancelButton = sap.ui.getCore().byId("cancelAccessButton");

			if (_dialogLayoutStyle === "View") {
				doneButton.setVisible(false);
				cancelButton.setText(_oController.getResourceBundle()
					.getText(
						"DNCcloseTXT"));
			}
		},

		/**
		 * Attach Popover on Mouse hover 
		 * @function attachPopoverOnHelp
		 **/
		attachPopoverOnHelp: function (oTargetControl, oPopover) {
			oTargetControl.addEventDelegate({
				onmouseover: function (oEvent) {
					// Remove focus
					$(':focus').blur();
					oPopover.openBy(oTargetControl);
				},
				onmouseout: function (oEvent) {
					// Remove focus
					$(':focus').blur();
					oPopover.close();
				}
			});
		},

		/**
		 * Attach Press Event on Help icon 
		 * @function onPressHelpIcon
		 **/
		onPressHelpIcon: function (oEvent) {
			var oTargetControl = oEvent.getSource();
			$(':focus').blur();
			_oController._oPopover.openBy(oTargetControl);

		},
		/**
		 *  Select the number of results displayed on a page
		 *  @selectResultsPerPage
		 */
		selectResultsPerPage: function (oEvent) {
			var sValue = oEvent.getSource().getText();
			_selectedActvResult = parseInt(sValue, 10);
			_begin = 0;
			_oController.tablePagination();
		},

		/**
		 * Formatter for no Accounts User Management.
		 * @function formatterNoAccountsAcessVisiblity
		 * @param {Object} model - Account Acess model.
		 * @returns {Boolean} - True or False if model have values
		 */
		formatterNoAccountsVisibility: function (model) {
			if (Object.keys(model).length === 0) {
				_oController.getView().byId("UserManagemnentTable").addStyleClass("fix-border-usermanagement");
				return true;
			}
			return false;
		},

		/**
		 * Formatter for no Accounts User Management Account Acess.
		 * @function formatterNoAccountsAcessVisiblity
		 * @param {Object} model - Account Acess model.
		 * @returns {Boolean} - True or False if model have values
		 */
		formatterNoAccountsAccessVisiblity: function (model) {
			if (Object.keys(model).length === 0) {
				sap.ui.getCore().byId("AccountAccessTable").addStyleClass("fix-border-accountaccess");
				return true;
			}
			return false;
		}
	});
});