sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel",
	"valvoline/dash/portal/DashPortalWeb/controls/Card",
	'sap/ui/core/util/Export',
	'sap/ui/core/util/ExportTypeCSV',
	"sap/m/MessageToast",
	"sap/ui/core/mvc/Controller",
	"jquery.sap.global"
], function (Base, JSONModel, CardControl, Export, ExportTypeCSV, MessageToast, Controller, jQuery) {
	"use strict";
	var _oController;
	var _oRouter;
	var _deleteClaimsBody;
	var _insertClaimsBody;
	var _saveClaimsBody;
	var _prevSubmited = 0;
	var _hasChanged = false;
	var _loadedClaims = false;
	var _claimStatus;
	var _reader;
	var _fileType;

	/*
		A new property is defined in Date Object to the Date String - YYYYMMDDHHMMSS
	*/
	Object.defineProperty(Date.prototype, "YYYYMMDDHHMMSS", {
		value: function () {
			function pad(n) {
				return (n < 10 ? '0' : '') + n;
			}
			return this.getFullYear() +
				pad(this.getMonth() + 1) +
				pad(this.getDate()) +
				pad(this.getHours()) +
				pad(this.getMinutes()) +
				pad(this.getSeconds());
		}
	});
	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.MyAccount.MyAccountSupplementalValidationClaims", {
		/** @module Feed */
		/**
		 * This function initializes the controller and defines the card types
		 * to be loaded.
		 * 
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oController = this;
			_oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			_oRouter.getRoute("MyAccountSupplementalValidationClaims").attachPatternMatched(this._onObjectMatched, this);

			this.onDataLossPreventingBrowserAlert();

			sap.ui.getCore().getEventBus().subscribe("uploadClaims", "uploadClaims", this.onUploadClaimsRates, this);

			_reader = new FileReader();
			_reader.addEventListener("load", function (evt) {
				var data;
				if (_reader.readAsBinaryString || _fileType === "csv") {
					data = evt.target.result;
				} else {
					var binary = "";
					var bytes = new Uint8Array(_reader.result);
					var length = bytes.byteLength;
					for (var i = 0; i < length; i++) {
						binary += String.fromCharCode(bytes[i]);
					}
					data = binary;
				}
				_oController.csvJSON(data);
			});
			_reader.addEventListener("error", function (evt) {
				_oController.onShowMessage(_oController, "Error", evt.target.error.name, "Unable to load the file");
			});
		},

		_onObjectMatched: function (oEvent) {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page Layout Configuration
			if (!this.checkDASHPageAuthorization("SUPPLEMENTAL_CLAIM_SUBMISSION")) {
				return;
			}

			//get user info and direct account
			_oController.getUserInformation(_oController);

			// Check Language and set selected Key in combo box
			this.onChangeLanguage();

			this.headerIconAccess();

			// Shopping Cart Values
			this.getShoppingCartCookie(this);

			// Remove the focus of the input
			$(':focus').blur();

			_claimStatus = this.getDomainsByName("ClaimsStatus");

			//Model to manage export button and status filters
			var statModel = new JSONModel({
				"exportBtn": false,
				"statusFilter": [],
				"selectedStat": "-1"
			});
			_oController.setModel(statModel, "claimStatModel");
			// Set Claim Status values to model
			_oController.claimStatusFilterValues(true);

			var upload = oEvent.getParameter("arguments").upload;
			if (_loadedClaims && _hasChanged) {
				_oController.getView().getModel("oClaims").updateBindings(true);
				return;
			} else if (this.getView().getParent().getPreviousPage() === undefined || upload === "false") {
				this.loadModelValidationClaims();
			}

		},

		/**
		 * Function to set Status drop-down values
		 * 
		 * @function claimStatusFilterValues
		 */
		claimStatusFilterValues: function (bResetSelection) {
			var claimStatus = this.getDomainsByName("ClaimsStatus");
			var i18nResourceBundle = this.getView().getModel("i18n");
			var statusArr = [];
			var statObj = {
				"statText": i18nResourceBundle.getProperty("MASCVStatusAllTXT"),
				"statKey": "-1"
			};
			statusArr.push(statObj);
			if (claimStatus.length) {
				for (var i = 0; i < claimStatus.length; i++) {
					if (claimStatus[i].value !== "2") {
						statObj = {
							"statText": claimStatus[i].description,
							"statKey": claimStatus[i].value
						};
						statusArr.push(statObj);
					}
				}
			}
			_oController.getModel("claimStatModel").getData().statusFilter = statusArr;
			if (bResetSelection) {
				_oController.getModel("claimStatModel").getData().selectedStat = "-1";
			} else {
				var oFilteredContexts = _oController.getView().byId("ClaimsVal_MainTable").getBinding("rows").getContexts();
				var iVisRowCount = _oController.formatTableRowNumber(oFilteredContexts);
				var oClaimsTable = _oController.getView().byId("ClaimsVal_MainTable");
				if (iVisRowCount) {
					oClaimsTable.setVisibleRowCount(iVisRowCount);
				} else {
					oClaimsTable.setVisibleRowCount(1);
				}
			}
			_oController.getModel("claimStatModel").refresh();
		},

		/**
		 * Event is fired when Status drop-down is changed
		 * Filters the table content on the input applied
		 * @function onStatusChange
		 */
		onStatusChange: function (oEvt) {
			if (oEvt) {
				var selectedKey = oEvt.getSource().getSelectedKey();
				var claimsTable = _oController.getView().byId("ClaimsVal_MainTable");
				var filter = null;
				if (selectedKey !== "-1") {
					if (selectedKey === "3") {
						filter = [new sap.ui.model.Filter("status", sap.ui.model.FilterOperator.EQ, selectedKey),
							new sap.ui.model.Filter("status", sap.ui.model.FilterOperator.EQ, "2")
						];
					} else {
						filter = new sap.ui.model.Filter("status", sap.ui.model.FilterOperator.EQ, selectedKey);
					}
				}
				claimsTable.getBinding("rows").filter(filter);
				var filteredContexts = _oController.getView().byId("ClaimsVal_MainTable").getBinding("rows").getContexts();
				var visRowCount = _oController.formatTableRowNumber(filteredContexts);
				if (visRowCount) {
					claimsTable.setVisibleRowCount(visRowCount);
					claimsTable.getBinding("rows").refresh();
				} else {
					claimsTable.setVisibleRowCount(1);
					claimsTable.getBinding("rows").refresh();
				}
				var exportBtnEnable = _oController.exportBtnEnableControl(filteredContexts);
				_oController.getView().byId("SupplementalsClaimsExportBtn").setEnabled(exportBtnEnable);
				// GTM
				_oController.GTMDataLayer('cvstatusfilter',
					'Validate Claims',
					'Status Filter -dropdown',
					oEvt.getSource().getSelectedItem().getText()
				);

			}
		},
		/**
		 * opens Exports dialog
		 * 
		 * @function onPressExportClaimsBtn
		 */
		onPressExportClaimsBtn: function () {
			//Ipad resquesting desktop sites is identified as macOS
			if ((sap.ui.Device.os.ios || sap.ui.Device.os.macintosh) && (sap.ui.Device.system.phone || sap.ui.Device.system.tablet)) {
				_oController.onPressExportClaims("csv");
			} else {
				// IF THE DIALOG IS CLOSED BY PRESSING "ESCAPE"
				if (_oController.oExportDialog) {
					_oController.oExportDialog.destroy();
				}
				_oController.oExportDialog =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.ExportDialog", this);
				_oController._createExportTypeModel(_oController.oExportDialog);
				_oController.oExportDialog.setModel(_oController.getView().getModel("i18n"), "i18n");
				// Set Header
				sap.ui.getCore().byId("exportDialogHeader").setText(_oController.getResourceBundle().getText("MASCExportClaimHeaderText"));
				// Attach functions
				sap.ui.getCore().byId("export_ExportDialog").attachPress(_oController.onExportClaimBtn);
				sap.ui.getCore().byId("close_ExportDialog").attachPress(_oController.onCloseExportDialog);
				_oController.oExportDialog.open();
			}
		},

		/**
		 * HDFI - 6813
		 * Exports the table content to CSV with filters applied
		 * 
		 * @function onPressExportClaims
		 */

		onPressExportClaims: function (format) {
			// Checks Session time out
			this.checkSessionTimeout();

			var statusFilters = _oController.getModel("claimStatModel").getData().statusFilter;
			var statusText = {
				"2": "Error"
			};
			for (var i = 0; i < statusFilters.length; i++) {
				statusText[statusFilters[i]["statKey"]] = statusFilters[i].statText;
			}
			var filteredContexts = _oController.getView().byId("ClaimsVal_MainTable").getBinding("rows").getContexts();
			var oModel = _oController.getModel("oClaims");
			var regex = /;/gi;
			var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "MM-dd-YYYY"
			});
			var dateFormatExcel = sap.ui.core.format.DateFormat.getDateInstance({
				pattern: "MM/dd/YYYY"
			});
			var d = new Date();
			var claims = [];
			for (var j = 0; j < filteredContexts.length; j++) {
				var data = {};
				data = JSON.parse(JSON.stringify(oModel.getProperty(filteredContexts[j].getPath())));
				if (data.status === "3") {
					var errorMsg = data.validationError;
					data.validationError = errorMsg.replace(regex, '.');
				}
				if (data.invoiceDate) {
					var invoiceDate = data.invoiceDate;
					data.invoiceDate = (format === "csv") ? dateFormat.format(new Date(invoiceDate)) : dateFormatExcel.format(new Date(invoiceDate));
				}
				data.status = statusText[data.status];
				claims.push(data);
			}
			if (format === "csv") {
				var exportModel = new JSONModel(claims);
				var i18nResourceBundle = this.getView().getModel("i18n");
				var oExport = new Export({
					exportType: new ExportTypeCSV({
						fileExtension: "csv",
						separatorChar: ","
					}),
					models: exportModel,
					rows: {
						path: "/"
					},
					columns: [{
						name: i18nResourceBundle.getProperty("MNText1BT"),
						template: {
							content: "{accname}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MNText2BT"),
						template: {
							content: "{accountnumber}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MNText3BT"),
						template: {
							content: "{materialNumber}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MASPRSellingTBL"),
						template: {
							content: "{sellingPrice}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MNText5BT"),
						template: {
							content: "{supplementalPrice}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MNText6BT"),
						template: {
							content: "{gallonsSold}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MNText7BT"),
						template: {
							content: "{invoiceNumber}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MASCVTemplateCol7"),
						template: {
							content: "{invoiceDate}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MASCVTemplateCol10"),
						template: {
							content: "{actualSellingPrice}"
						}

					}, {
						name: i18nResourceBundle.getProperty("MNText10BT"),
						template: {
							content: "{status}"
						}

					}, {
						name: i18nResourceBundle.getProperty("SCVPErrorMessageCSVTXT"),
						template: {
							content: "{validationError}"
						}

					}]
				});
				oExport.saveFile("ValidateSupplementalClaims_" + d.YYYYMMDDHHMMSS()).catch(function () {

				}).then(function () {
					oExport.destroy();
				});
			} else {
				var aXLSXFormatData = _oController._createExcelFormatData(claims);
				var wb = _oController._jsonToWorkbook(aXLSXFormatData, "ValidateSupplementalClaims_" + d.getFullYear());
				_oController._saveFileToExcel(wb, "ValidateSupplementalClaims_" + d.YYYYMMDDHHMMSS());
			}

			// GTM
			_oController.GTMDataLayer('cvbuttonclick',
				'Validate Claims',
				'Button -click',
				_oController.getView().byId("SupplementalsClaimsExportBtn").getText()
			);
		},

		exportBtnEnableControl: function (claims) {
			if (claims.length) {
				return true;
			} else {
				return false;
			}
		},

		/**
		 * Formats the numeric inputs of the table (actual selling price,
		 * gallons sold, and supplemental price)
		 * 
		 * @function formatInputs
		 */
		formatInputs: function () {

			var rows = _oController.getView().getModel("oClaims").getData();

			// Check all the selected items and add them to the array
			for (var i = 0; i < rows.length; i++) {
				var valueSellingPriceFormatted = _oController.formatFloat(rows[i].actualSellingPrice);
				var valueGallonsSoldFormatted = _oController.formatFloat(rows[i].gallonsSold);
				var valueRateFormatted = _oController.formatFloat(rows[i].supplementalPrice);

				rows[i].actualSellingPrice = valueSellingPriceFormatted;
				rows[i].gallonsSold = valueGallonsSoldFormatted;
				rows[i].supplementalPrice = valueRateFormatted;
			}

			if (rows.length) {
				_oController.getView().getModel("claimStatModel").getData().exportBtn = true;
				_oController.getView().getModel("claimStatModel").refresh();
			}
			_oController.getView().getModel("oClaims").refresh();
			_oController.getView().getModel("oClaims").updateBindings(true);
		},

		/**
		 * Triggered when the user types in the numeric fields, formats the
		 * inputs
		 * 
		 * @function valueChangeFloat
		 */
		valueChangeFloat: function (oEvent) {
			_hasChanged = true;
			var value = oEvent.getParameter("newValue");

			var newVal = _oController.formatFloat(value);

			// Adding 0.00001 is a scaling technique due to JS crazy floats, otherwise values like 1.005 would round to 1.00 instead of 1.01 
			//var newVal = Math.round((parseFloat(value) + 0.00001) * 100) / 100;

			var row = oEvent.getSource().getBinding("value").getContext().getPath();
			var column = oEvent.getSource().getBinding("value").getPath();

			_oController.getView().getModel("oClaims").setProperty(row + "/" + column, newVal);

		},

		/**
		 * Triggered when the user types in the numeric integer fields
		 * 
		 * @function valueChangeInteger
		 */
		valueChangeInteger: function (oEvent) {
			_hasChanged = true;
			var value = oEvent.getParameter("newValue");

			var newVal = _oController.formatInteger(value);

			var row = oEvent.getSource().getBinding("value").getContext().getPath();
			var column = oEvent.getSource().getBinding("value").getPath();

			_oController.getView().getModel("oClaims").setProperty(row + "/" + column, newVal);

		},

		/**
		 * Formats a integer
		 * 
		 * @function formatInteger
		 */
		formatInteger: function (value) {
			if (value !== undefined && value !== null) {
				value = value.split(",").join("");

				// Removal of Leading ZEROS ("01" -> "1")
				if (!isNaN(value) && value !== "") {
					value = parseFloat(value) + "";
					return _oController.valueChangeFormatterInteger(value).toString();
				}
			}
			return value;
		},

		/**
		 * Formatts the value with 0 decimal digits
		 * 
		 * @(parameter) (SValue) return (sValueFormatted)
		 */
		valueChangeFormatterFloat: function (sValue) {
			var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
			var oFormatOptions = {
				style: "standard",
				decimals: 2,
				groupingEnabled: "true"
			};
			var oFloatFormat = sap.ui.core.format.NumberFormat.getFloatInstance(oFormatOptions, oLocale);
			var sValueFormatted = oFloatFormat.format(sValue);
			return sValueFormatted;
		},

		/**
		 * Formatts the value with 0 decimal digits
		 * 
		 * @(parameter) (SValue) return (sValueFormatted)
		 */
		valueChangeFormatterInteger: function (sValue) {
			var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
			var oFormatOptions = {
				maxFractionDigits: 2,
				decimals: 0
			};
			var oIntegerFormat = sap.ui.core.format.NumberFormat.getFloatInstance(oFormatOptions, oLocale);
			var sValueFormatted = oIntegerFormat.format(sValue);
			return sValueFormatted;
		},

		onUploadClaimsRates: function (channel, event, data) {
			var dataLoaded = false;
			var oFile = data.data;
			var aFileName = oFile && oFile.name && oFile.name.split(".");
			_fileType = aFileName && aFileName[aFileName.length - 1];
			if (oFile && window.FileReader) {
				if (_fileType && _fileType === "csv") {
					_reader.readAsText(oFile);
				} else {
					if (_reader.readAsBinaryString) {
						_reader.readAsBinaryString(oFile);
					} else {
						_reader.readAsArrayBuffer(oFile);
					}
				}
				dataLoaded = true;
			}
			if (!dataLoaded) {
				this.loadModelValidationClaims();
			}
		},

		/**
		 * onDataLossPreventingBrowserAlert Sets a browser alert that prevents
		 * closing if there is values to submit
		 */
		onDataLossPreventingBrowserAlert: function () {
			// Runs when the user tries to close the "distributorInsights" page
			$(window).bind('beforeunload', function () {
				var route = sap.ui.core.routing.HashChanger.getInstance().getHash();
				if (_hasChanged === true && route === "myAccount/supplementalValidationClaims") {
					return true;
				}
			});
		},

		/**
		 * Function that handles the success case of the ajax call that loads the Claims
		 * @function onLoadValidationClaimsSuccess
		 */
		onLoadValidationClaimsSuccess: function (oController, oData, oPiggyBack) {
			var SupplementalValidatinClaims_Container = oController.getView().byId("vbox_card_container_contacts");
			_oController.clearTableFilterSort();
			_loadedClaims = true;
			_hasChanged = false;
			SupplementalValidatinClaims_Container.setBusy(false);
			oController.oDataConfiguration(oData);
		},
		/**
		 * Function that handles the error case of the ajax call that loads the Claims
		 * @function onLoadValidationClaimsError
		 */
		onLoadValidationClaimsError: function (oController, oError, oPiggyBack) {
			var SupplementalValidatinClaims_Container = oController.getView().byId("vbox_card_container_contacts");
			SupplementalValidatinClaims_Container.setVisible(false);
			SupplementalValidatinClaims_Container.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"MASCLoadClaimsErrorTXT"), null);
		},

		/**
		 * Loads the staged claims and sets the data on the table.
		 * 
		 * @function loadModelValidationClaims
		 */
		loadModelValidationClaims: function () {
			var controller = this;
			var SupplementalValidatinClaims_Container = this.getView().byId("vbox_card_container_contacts");
			SupplementalValidatinClaims_Container.setBusyIndicatorDelay(0);
			SupplementalValidatinClaims_Container.setBusy(true);
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();

			var xsURL = "/salesforce/stagedClaims" + "?lang=" + selectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend
			_oController.handleAjaxJSONCall(controller, true, xsURL, "GET", _oController.onLoadValidationClaimsSuccess, _oController.onLoadValidationClaimsError);
		},

		/**
		 * Sets the data to the table.
		 * 
		 * @function oDataConfiguration
		 */
		oDataConfiguration: function (data) {
			var _oModelSupplementalValidatinClaims = new sap.ui.model.json.JSONModel();
			_oModelSupplementalValidatinClaims.setData(data);
			_oController.getView().setModel(_oModelSupplementalValidatinClaims, "oClaims");
			sap.ui.getCore().setModel(_oModelSupplementalValidatinClaims, "oClaims");
			_hasChanged = false;

			_oController.formatInputs();

		},

		onPressInsertClaims: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			_oController.getView().byId("statusSelectDrpDwn").setSelectedKey("-1");
			// If there are changed Input values
			if (_hasChanged) {
				this.DataLossWarning =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", this);
				// Set Warning Dialog Buttons
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(this.onInsertClaims);
				sap.ui.getCore().byId("button_Dialog_Warning_No").attachPress(this.onDialogWarnigRefresh_No);
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWyesTXT"));
				sap.ui.getCore().byId("button_Dialog_Warning_No").setText(_oController.getResourceBundle().getText("DLWnoTXT"));
				// Set Warning Dialog Message
				sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("WarningMessageDelete"));
				sap.ui.getCore().byId("text2_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWDuplicateDescription"));
				// Set Warnig Dialog Title
				sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWMainTitle"));
				this.DataLossWarning.open();
				_oController._removeDialogResize(_oController.DataLossWarning);

			} else {
				_oController.onInsertClaims();
			}
		},
		/**
		 * Fires when the user clicks on the duplicate claims icon.
		 * 
		 * @function onDuplicateClaims
		 */
		onInsertClaims: function () {
			var mainTable = _oController.byId("ClaimsVal_MainTable");
			var oModel_MainTable = mainTable.getModel("oClaims");
			var dialog = sap.ui.getCore().byId("warningDialog");

			var selectedRows = _oController.getSelectedIndexes();

			_insertClaimsBody = [];
			// Check all the selected items and add them to the array
			for (var i = 0; i < selectedRows.length; i++) {
				var item = oModel_MainTable.getProperty("/" + selectedRows[i]);
				_insertClaimsBody.push({
					"id": item.id,
					"accountnumber": item.accountnumber,
					"accname": item.accname,
					"distributorNumber": item.distributorNumber,
					"gallonsSold": _oController.deCommaFyFloats(item.gallonsSold),
					"invoiceDate": item.invoiceDate,
					"invoiceNumber": item.invoiceNumber,
					"materialNumber": item.materialNumber,
					"supplementalPrice": _oController.deCommaFyFloats(item.supplementalPrice),
					"sellingPrice": item.sellingPrice,
					"actualSellingPrice": _oController.deCommaFyFloats(item.actualSellingPrice),
					"isManual": item.isManual,
					"distributorID": item.distributorID,
					"accountid": item.accountid
				});
				// GTM
				_oController.GTMDataLayer('cvformlink',
					'Claim Validation',
					'Action click',
					_oController.getResourceBundle().getText("MNTextDupl")
				);
			}
			if (selectedRows.length > 0) {
				_oController.insertClaims();
			}

			if (dialog) {
				dialog.close();
				dialog.destroy();
				dialog.setBusy(false);
			}
		},

		/**
		 * Executes when the user agrees to delete claims.
		 * @function onDeleteClaims
		 **/
		onDeleteClaims: function () {
			var mainTable = this.byId("ClaimsVal_MainTable");
			var oModel_MainTable = mainTable.getModel("oClaims");

			var selectedRows = this.getSelectedIndexes();

			if (selectedRows === null || selectedRows === undefined || selectedRows.length === 0) {
				return;
			}

			var pendingSynchError = 0;
			_deleteClaimsBody = [];
			// Check all the selected items and add them to the array
			for (var x = 0; x < selectedRows.length; x++) {
				var item = oModel_MainTable.getProperty("/" + selectedRows[x]);
				if (item.status === "0") {
					pendingSynchError = pendingSynchError + 1;
				} else {
					_deleteClaimsBody.push({
						"id": item.id
					});
				}
			}

			var deleteClaimDialog = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog");
			if ((selectedRows.length - pendingSynchError) > 0) {
				if (pendingSynchError === 0) {
					// Set Warning Buttons
					sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(this.deleteClaimConfirmation);
					sap.ui.getCore().byId("button_Dialog_Warning_No").attachPress(this.onReturn);
					sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWyesTXT"));
					sap.ui.getCore().byId("button_Dialog_Warning_No").setText(_oController.getResourceBundle().getText("DLWnoTXT"));
					// Set Warning Message
					sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("MASCVDeleteC1TXT") + " " + (
							selectedRows.length - pendingSynchError) +
						" " + _oController.getResourceBundle().getText("MASCVDeleteC2TXT"));
					// Set Warnig Dialog Title
					sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWMainTitle"));

					//if there's unsaved data, warn the user
					if (_hasChanged) {
						// Set Warning Message
						sap.ui.getCore().byId("text1_Dialog_Warning").setText(sap.ui.getCore().byId("text1_Dialog_Warning").getText() + " " +
							_oController.getResourceBundle().getText("WarningMessageDelete"));
					}
					sap.ui.getCore().byId("text2_Dialog_Warning").setText(_oController.getResourceBundle().getText("MASCVDeleteC3TXT"));
					deleteClaimDialog.open();
				} else {
					// Set Warning Buttons
					sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(this.deleteClaimConfirmation);
					sap.ui.getCore().byId("button_Dialog_Warning_No").attachPress(this.onReturn);
					sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWyesTXT"));
					sap.ui.getCore().byId("button_Dialog_Warning_No").setText(_oController.getResourceBundle().getText("DLWnoTXT"));
					// Set Warning Message
					sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("MASCVDeleteC1TXT") + " " + (
							selectedRows.length - pendingSynchError) +
						" " + _oController.getResourceBundle().getText("MASCVDeleteC2TXT"));

					//if there's  unsaved data, warn the user
					if (_hasChanged) {
						// Set Warning Message
						sap.ui.getCore().byId("text1_Dialog_Warning").setText(sap.ui.getCore().byId("text1_Dialog_Warning").getText() + " " +
							_oController.getResourceBundle().getText("WarningMessageDelete"));
					}

					sap.ui.getCore().byId("text2_Dialog_Warning").setText(_oController.getResourceBundle().getText("MASCVDeleteC3TXT") + " " +
						_oController.getResourceBundle().getText("ErrorMessageNotePendingSynch"));

					// Set Warnig Dialog Title
					sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWMainTitle"));
					deleteClaimDialog.open();
				}
			} else {
				// Set Warning Buttons
				sap.ui.getCore().byId("button_Dialog_Warning_No").attachPress(this.onReturn);
				sap.ui.getCore().byId("button_Dialog_Warning_No").setText(_oController.getResourceBundle().getText("ErrorOkButtonPendingSynch"));
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").setVisible(false);
				// Set Warning Message
				sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("ErrorMessagePendingSynch"));
				// Set Warnig Dialog Title
				sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWMainTitle"));
				deleteClaimDialog.open();
			}

			_oController._removeDialogResize(deleteClaimDialog);

			// GTM
			_oController.GTMDataLayer('cvformlink',
				'Claim Validation',
				'Action click',
				_oController.getResourceBundle().getText("MNTextDel")
			);
		},

		/**
		 * Closes the delete claim message box
		 * 
		 * @function onReturn
		 */
		onReturn: function () {
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();
		},

		/**
		 * Function that handles the success case of the ajax call that deletes Claims
		 * @function ondeleteClaimSuccess
		 */
		ondeleteClaimSuccess: function (oController, oData, oPiggyBack) {
			var dialog = sap.ui.getCore().byId("warningDialog");
			_oController.clearTableFilterSort();
			dialog.close();
			dialog.destroy();
			dialog.setBusy(false);
			oController.loadModelValidationClaims();
		},

		/**
		 * Function that handles the error case of the ajax call that delete Claims
		 * @function onLoadValidationClaimsError
		 */
		ondeleteClaimError: function (oController, oError, oPiggyBack) {
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();
			dialog.setBusy(false);
			_oController.onShowMessage(_oController, "Error", oError.status, oError.statusText, _oController.getResourceBundle().getText(
				"MASCDeleteClaimsErrorTXT"), null);
		},

		/**
		 * Calls service to delete selected claims.
		 * 
		 * @function deleteClaimConfirmation
		 */
		deleteClaimConfirmation: function () {
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.setBusyIndicatorDelay(0);
			dialog.setBusy(true);
			var xsURL = "/salesforce/deleteClaims";

			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "POST", _oController.ondeleteClaimSuccess, _oController.ondeleteClaimError,
				JSON.stringify(
					_deleteClaimsBody));
		},
		/**
		 * Function that handles the success case of the ajax call to insert Claims
		 * @function onSaveSubmitClaimSuccess
		 */
		onInsertClaimsSuccess: function (oController, oData, oPiggyBack) {
			var SupplementalValidatinClaims_Container = _oController.getView().byId("vbox_card_container_contacts");
			_oController.clearTableFilterSort();
			// Clears all error message
			oController.clearServerErrorMsg(oController);
			oController.oDataConfiguration(oData);
			SupplementalValidatinClaims_Container.setBusy(false);
		},

		/**
		 * Function that handles the error case of the ajax call to insert Claims
		 * @function onSaveSubmitClaimError
		 */
		onInsertError: function (oController, oError, oPiggyBack) {
			var SupplementalValidatinClaims_Container = _oController.getView().byId("vbox_card_container_contacts");
			SupplementalValidatinClaims_Container.setBusy(false);
			_oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"MASCInsertClaimsErrorTXT"), null);
		},

		/**
		 * Calls service to insert all claims from the table.
		 * 
		 * @function insertClaims
		 */
		insertClaims: function () {
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			var SupplementalValidatinClaims_Container = this.getView().byId("vbox_card_container_contacts");
			SupplementalValidatinClaims_Container.setBusyIndicatorDelay(0);
			SupplementalValidatinClaims_Container.setBusy(true);
			var xsURL = "/salesforce/insertClaims" + "?lang=" + selectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "POST", _oController.onInsertClaimsSuccess, _oController.onInsertError,
				JSON.stringify(_insertClaimsBody));
		},

		/**
		 * Fires when the user presses the save button.
		 * 
		 * @function onPressSaveClaims
		 */
		onPressSaveClaims: function (oEvent) {
			var mainTable = this.byId("ClaimsVal_MainTable");
			var items = mainTable.getBinding("rows").getModel().getData();

			_saveClaimsBody = [];
			_prevSubmited = 0;
			// Check all the selected items and add them to the array
			for (var i = 0; i < items.length; i++) {
				if (items[i].status === "0") {
					_prevSubmited++;
				}

				_saveClaimsBody.push({
					"id": items[i].id,
					"accname": items[i].accname,
					"accountnumber": items[i].accountnumber,
					"distributorNumber": items[i].distributorNumber,
					"gallonsSold": _oController.deCommaFyFloats(items[i].gallonsSold),
					"invoiceDate": items[i].invoiceDate,
					"invoiceNumber": items[i].invoiceNumber,
					"materialNumber": items[i].materialNumber,
					"supplementalPrice": _oController.deCommaFyFloats(items[i].supplementalPrice),
					"sellingPrice": items[i].sellingPrice,
					"actualSellingPrice": _oController.deCommaFyFloats(items[i].actualSellingPrice),
					"isManual": items[i].isManual,
					"accountid": items[i].accountid
				});
			}

			if (items.length > 0) {
				_oController.saveSubmitClaims("F");
			}

			// GTM
			_oController.GTMDataLayer('cvbuttonclick',
				'Claim Validation',
				'Button -click',
				oEvent.getSource().getText()
			);
		},

		/**
		 * Fires when the user presses the submit button.
		 * 
		 * @function onPressSubmitClaims
		 */
		onPressSubmitClaims: function (oEvent) {
			var mainTable = this.byId("ClaimsVal_MainTable");
			var items = mainTable.getBinding("rows").getModel().getData();

			_saveClaimsBody = [];
			_prevSubmited = 0;
			// Check all the selected items and add them to the array
			for (var i = 0; i < items.length; i++) {
				if (items[i].status === "0") {
					_prevSubmited++;
				}
				_saveClaimsBody.push({
					"id": items[i].id,
					"accname": items[i].accname,
					"accountnumber": items[i].accountnumber,
					"distributorNumber": items[i].distributorNumber,
					"gallonsSold": _oController.deCommaFyFloats(items[i].gallonsSold),
					"invoiceDate": items[i].invoiceDate,
					"invoiceNumber": items[i].invoiceNumber,
					"materialNumber": items[i].materialNumber,
					"supplementalPrice": _oController.deCommaFyFloats(items[i].supplementalPrice),
					"sellingPrice": items[i].sellingPrice,
					"actualSellingPrice": _oController.deCommaFyFloats(items[i].actualSellingPrice),
					"isManual": items[i].isManual,
					"accountid": items[i].accountid
				});
			}

			if (items.length > 0) {
				_oController.saveSubmitClaims("T");
			}

			// GTM
			_oController.GTMDataLayer('cvbuttonclick',
				'Claim Validation',
				'Button -click',
				oEvent.getSource().getText()
			);
		},

		/**
		 * Function that handles the success case of the ajax call to submit Claims
		 * @function onSaveSubmitClaimSuccess
		 */
		onSaveSubmitClaimSuccess: function (oController, oData, oPiggyBack) {
			var SupplementalValidatinClaims_Container = oController.getView().byId("vbox_card_container_contacts");
			_hasChanged = false;
			// Clears all error message
			oController.clearServerErrorMsg(oController);
			oController.oDataConfiguration(oData);
			SupplementalValidatinClaims_Container.setBusy(false);

			_oController.clearTableFilterSort();
			_oController.getView().byId("statusSelectDrpDwn").setSelectedKey("-1");
			var message = "";
			var type = "";
			if (oPiggyBack.submit === "T") {
				var submited = 0;
				for (var i = 0; i < oData.length; i++) {
					if (oData[i].status === "0") {
						submited++;
					}
				}
				type = "Success";
				message = submited - _prevSubmited + " " + oController.getResourceBundle().getText("MASCVSubmitedClaimsSuccTXT");
				oController.onShowMessage(oController, type, null, message, null, null);

			} else {
				var invalidClaims = false;
				for (i = 0; i < oData.length; i++) {
					if (oData[i].status === "Invalid") {
						invalidClaims = true;
					}
				}
				if (invalidClaims) {
					type = "Warning";
					message = oController.getResourceBundle().getText("MASCVInvalidClaimsTXT");
					oController.onShowMessage(oController, type, null, message, null, null);
				}
			}
		},

		/**
		 * Function that handles the error case of the ajax call to submit Claims
		 * @function onSaveSubmitClaimError
		 */
		onSaveSubmitClaimError: function (oController, oError, oPiggyBack) {
			var SupplementalValidatinClaims_Container = oController.getView().byId("vbox_card_container_contacts");
			SupplementalValidatinClaims_Container.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"MASCValidateClaimsErrorTXT"), null);
		},

		/**
		 * Call service to save or submit claims.
		 * 
		 * @function saveSubmitClaims
		 */
		saveSubmitClaims: function (submit) {
			var SupplementalValidatinClaims_Container = this.getView().byId("vbox_card_container_contacts");
			SupplementalValidatinClaims_Container.setBusyIndicatorDelay(0);
			SupplementalValidatinClaims_Container.setBusy(true);
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();

			var oPiggyBack = {
				"submit": submit
			};
			var xsURL = "/salesforce/validateClaims" + "?submit=" + submit + "&lang=" + selectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "POST", _oController.onSaveSubmitClaimSuccess, _oController.onSaveSubmitClaimError,
				JSON.stringify(_saveClaimsBody), oPiggyBack);
		},
		/**
		 * Upload claims.
		 * 
		 * @function saveSubmitClaims
		 */
		handleUploadComplete: function (oEvent) {
			var oFile = oEvent.getParameter("files")[0];
			var aFileName = oFile && oFile.name && oFile.name.split(".");
			_fileType = aFileName && aFileName[aFileName.length - 1];
			if (oFile && window.FileReader) {
				if (_fileType && _fileType === "csv") {
					_reader.readAsText(oFile);
				} else {
					if (_reader.readAsBinaryString) {
						_reader.readAsBinaryString(oFile);
					} else {
						_reader.readAsArrayBuffer(oFile);
					}
				}
			}

			// GTM
			_oController.GTMDataLayer('cvbuttonclick',
				'Claim Validation',
				'Button -click',
				oEvent.getSource().getAggregation("tooltip")
			);

		},

		csvJSON: function (csv) {
			var lines;
			var isEmpty = false;
			if (_fileType && _fileType === "csv") {
				lines = csv.split("\n");
			} else {
				lines = _oController._excelDataToArray(csv);
				if (lines && lines.length && lines.length === 1) {
					// Check for the empty file
					isEmpty = _oController._checkForEmptyExcel(lines[0]);
				}
			}
			var i18nResourceBundle = this.getView().getModel("i18n");
			var jsonResponseText = '{"fault":{"faultstring": "' + i18nResourceBundle.getProperty("ErrorMessageInput") +
				'","detail":{"errorcode": "' + i18nResourceBundle.getProperty("ErrorCodeInput") + '"}}}';
			if (lines.length <= 4002) {
				var result = [];
				var headers = (_fileType && _fileType === "csv") ? lines[0].split(",") : _oController._createExcelFormatTemplate("header");
				var obj = {};
				var currentline;
				var collumChecker = 0;
				for (var j = 0; j < headers.length; j++) {
					switch (j) {
					case 0:
						headers[j] = "distributorNumber";
						collumChecker++;
						break;
					case 1:
						headers[j] = "distributorName";
						collumChecker++;
						break;
					case 2:
						headers[j] = "accountnumber";
						collumChecker++;
						break;
					case 3:
						headers[j] = "accname";
						collumChecker++;
						break;
					case 4:
						headers[j] = "materialNumber";
						collumChecker++;
						break;
					case 5:
						headers[j] = "distributedPartNumber";
						collumChecker++;
						break;
					case 6:
						headers[j] = "invoiceDate";
						collumChecker++;
						break;
					case 7:
						headers[j] = "invoiceNumber";
						collumChecker++;
						break;
					case 8:
						headers[j] = "gallonsSold";
						collumChecker++;
						break;
					case 9:
						headers[j] = "actualSellingPrice";
						collumChecker++;
						break;
					case 10:
						headers[j] = "supplementalPrice";
						collumChecker++;
						break;
					default:
						break;
					}
				}

				if (collumChecker === 11) {
					var invalidLines = false;
					var dateFormatExcel = sap.ui.core.format.DateFormat.getDateInstance({
						pattern: "MM/dd/YYYY"
					});
					for (var i = ((_fileType && _fileType === "csv") || (isEmpty)) ? 1 : 0; i < lines.length; i++) {
						obj = {};
						currentline = (_fileType && _fileType === "csv") ? lines[i].split(",") : _oController._objToArray(lines[i]);
						if (currentline.length >= 11) {
							var sizeErrorMsgs = [];
							for (j = 0; j < collumChecker; j++) {
								if (j === 9 || j === 10) {
									obj[headers[j]] = currentline[j].trim().replace(/(^\$|\r|\t|\n)/gm, "");
								} else if (j === 6) {
									if (currentline[j]) {
										var sDate = dateFormatExcel.format(new Date(currentline[j].trim().replace(/(\r|\t|\n)/gm, "")));
										if (isNaN(sDate.valueOf())) {
											obj[headers[j]] = sDate;
										} else {
											obj[headers[j]] = currentline[j].trim().replace(/(\r|\t|\n)/gm, "");
										}
									} else {
										obj[headers[j]] = "";
									}
								} else {
									obj[headers[j]] = currentline[j].trim().replace(/(\r|\t|\n)/gm, "");
								}
								if (j >= 2) {
									var sizeErrorMsg = _oController.verifyFieldLenght(j, obj[headers[j]]);
									if (sizeErrorMsg) {
										sizeErrorMsgs.push(sizeErrorMsg);
									}
								}
							}
							obj["isManual"] = "T";
							result.push(obj);
						} else if (currentline.length > 1) {
							invalidLines = true;
						}
						if (invalidLines) {
							_oController.clearServerErrorMsg(_oController);
							this.onShowMessage(this, "Error", i18nResourceBundle.getProperty("ErrorCodeInput"),
								i18nResourceBundle.getProperty("ErrorDescriptionInput"), jsonResponseText, null);
							return false;
						} else if (sizeErrorMsgs && sizeErrorMsgs.length >= 1) {
							_oController.clearServerErrorMsg(_oController);
							for (var x = 0; x < sizeErrorMsgs.length; x++) {
								this.onShowMessage(this, "Error", i18nResourceBundle.getProperty("ErrorCodeInput"),
									i18nResourceBundle.getProperty("MASCVLenghtError1") + " " + sizeErrorMsgs[x].maxLenght + " " + i18nResourceBundle.getProperty(
										"MASCVLenghtError2") + " " + sizeErrorMsgs[x].fieldName + " " + i18nResourceBundle.getProperty("MASCVLenghtError3"),
									jsonResponseText,
									null);
							}
							return false;
						}
					}
					_insertClaimsBody = result;
					this.insertClaims();
					_oController.getView().byId("statusSelectDrpDwn").setSelectedKey("-1");
					return true;
				}

			} else {
				i18nResourceBundle = this.getView().getModel("i18n");
				jsonResponseText = '{"fault":{"faultstring": "' + i18nResourceBundle.getProperty("ErrorMessageLines") +
					'","detail":{"errorcode": "' + i18nResourceBundle.getProperty("ErrorCodeLines") + '"}}}';
				this.onShowMessage(this, "Error", i18nResourceBundle.getProperty("ErrorCodeLines"),
					i18nResourceBundle.getProperty("ErrorDescriptionLines"), jsonResponseText, null);
			}
		},

		/**
		 * Verifies the length of the fields. Returns true if the field does not exceed maximun length, false otherwise.
		 * @function verifyFieldLenght
		 **/
		verifyFieldLenght: function (index, field) {
			var maxLength, fieldName;
			var i18nResourceBundle = this.getView().getModel("i18n");
			//the index param is for determining what is the max size for the columns
			switch (index) {
			case 2:
				fieldName = i18nResourceBundle.getProperty("MASCVTemplateCol3");
				maxLength = 10;
				break;
			case 3:
				fieldName = i18nResourceBundle.getProperty("MASCVTemplateCol4");
				maxLength = 150;
				break;
			case 4:
				fieldName = i18nResourceBundle.getProperty("MASCVTemplateCol5");
				maxLength = 20;
				break;
			case 5:
				fieldName = i18nResourceBundle.getProperty("MASCVTemplateCol6");
				maxLength = 30;
				break;
			case 6:
				fieldName = i18nResourceBundle.getProperty("MASCVTemplateCol7");
				maxLength = 20;
				break;
			case 7:
				fieldName = i18nResourceBundle.getProperty("MASCVTemplateCol8");
				maxLength = 20;
				break;
			case 8:
				fieldName = i18nResourceBundle.getProperty("MASCVTemplateCol9");
				maxLength = 11;
				break;
			case 9:
				fieldName = i18nResourceBundle.getProperty("MASCVTemplateCol10");
				maxLength = 11;
				break;
			case 10:
				fieldName = i18nResourceBundle.getProperty("MASCVTemplateCol11");
				maxLength = 11;
				break;
			default:
				break;
			}

			if (field.length > maxLength) {
				var errorObj = {
					"maxLenght": maxLength,
					"fieldName": fieldName
				};
				return errorObj;
			} else {
				return null;
			}
		},

		handleTypeMissmatch: function (oEvent) {
			var aFileTypes = oEvent.getSource().getFileType();
			var i18nResourceBundle = this.getView().getModel("i18n");
			jQuery.each(aFileTypes, function (key, value) {
				aFileTypes[key] = "*." + value;
			});
			var sSupportedFileTypes = aFileTypes.join(", ");
			var jsonResponseText = '{"fault":{"faultstring": "' + i18nResourceBundle.getProperty("WarningMessageFileType") +
				'","detail":{"errorcode": "' + i18nResourceBundle.getProperty("WarningCodeFileType") + '"}}}';
			this.onShowMessage(this, "Error", i18nResourceBundle.getProperty("WarningCodeFileType"),
				i18nResourceBundle.getProperty("WarningDescriptionFileType1") + oEvent.getParameter("fileType") + " " +
				i18nResourceBundle.getProperty("WarningDescriptionFileType2") + sSupportedFileTypes, jsonResponseText, null);
		},

		/**
		 * Given a claim status, returns the icon to show.
		 * 
		 * @function formatStatusIcon
		 */
		formatStatusIcon: function (status) {
			switch (status) {
			case "4":
				return "sap-icon://document";
			case "1":
				return "sap-icon://message-success";
			case "0":
				return "sap-icon://future";
			case "3":
			case "2":
				return "sap-icon://error";
			default:
				return null;

			}
		},

		/**
		 * Given a claim status, returns the color of the icon.
		 * 
		 * @function formatStatusIconColor
		 */
		formatStatusIconColor: function (status) {
			switch (status) {
			case "4":
				return "#009cd9";
			case "1":
				return "#26A65B";
			case "0":
				return "#F5D76E";
			case "3":
				return "#D64541";
			case "2":
				return "#D64541";
			default:
				return null;

			}
		},

		/**
		 * Checks if an input should be editable, taking status and isManual
		 * into account.
		 * 
		 * @function isEditableM
		 */
		isEditableM: function (status, isManual) {
			if (status === "0") {
				return false;
			} else {
				return (isManual === "T");
			}
		},

		/**
		 * Checks if an input should be editable, taking status into account.
		 * 
		 * @function isEditableA
		 */
		isEditableA: function (status) {
			return !(status === "0");
		},

		/**
		 * Fired when a user clicks the error icon.
		 * 
		 * @function showErrorStatus
		 */
		showErrorStatus: function (oEvent) {
			var path = oEvent.getSource().getParent().getBindingContext("oClaims").getPath();
			var claim = sap.ui.getCore().getModel("oClaims").getProperty(path);

			if (claim.status !== "2" && claim.status !== "3") {
				return;
			}

			this.errorStatusPopover(oEvent, claim);
		},

		/**
		 * errorStatusPopover Pop Over function @ (oEvent)
		 * @fucntion errorStatusPopover
		 */
		errorStatusPopover: function (oEvent, claim) {
			if (this._oPopover) {
				this._oPopover.destroyContent();
			}

			this._oPopover = sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.ClaimsErrorPopOver", this);
			this._oPopover.openBy(oEvent.getSource());

			// get error messages
			var errorMessagesUnparse = claim.validationError;
			if (errorMessagesUnparse) {
				var errorMessagesParsed = errorMessagesUnparse.split(";");

				for (var message in errorMessagesParsed) {
					if (errorMessagesParsed[message] !== "") {
						var newHBox = new sap.m.HBox({
							alignItems: "Center"
						});
						newHBox.addStyleClass("sapUiTinyMarginBottom sapUiTinyMarginTop sapUiSmallMarginBegin");
						var icon = new sap.ui.core.Icon({
							src: "sap-icon://error",
							color: "#D64541"
						}).addStyleClass("sapUiSmallMarginEnd claimStatusIcon");
						newHBox.addItem(icon);
						var text = new sap.m.Text({
							text: errorMessagesParsed[message]
						}).addStyleClass("bodyCopy");
						newHBox.addItem(text);

						this._oPopover.getContent()[0].addItem(newHBox);
					}
				}
			}
		},
		/**
		 * Checks if the save button should be enabled.
		 * 
		 * @function enableSave
		 */
		enableSave: function (claims) {
			if (claims !== undefined && claims !== null) {
				return (claims.length > 0);
			} else {
				return false;
			}
		},

		/**
		 * Checks if the submit button should be enabled.
		 * 
		 * @function enableSubmit
		 */
		enableSubmit: function (claims) {
			var validClaim = false;
			if (claims !== undefined && claims !== null) {
				for (var i = 0; i < claims.length; i++) {
					if (claims[i].status === "1") {
						validClaim = true;
					}
				}
			}
			return validClaim;
		},

		/**
		 * Formats the number of rows of the table.
		 * 
		 * @function formatTableRowNumber
		 */

		formatTableRowNumber: function (claims) {
			if (claims.length >= 10) {
				return 10;
			} else {
				return claims.length;
			}
		},

		/**
		 * Checks if a row should be selectable or not.
		 * 
		 * @function enableSelection
		 */
		enableSelection: function (status) {
			return !(status === "0");
		},
		/**
		 * Selects or deselects all claims.
		 * 
		 * @function onSelectAll
		 */
		onSelectAll: function (oEvent) {
			var table = _oController.getView().byId("ClaimsVal_MainTable");
			var rows = table.getRows();

			for (var i = 0; i < rows.length; i++) {
				var checkbox = rows[i].getCells()[0];
				if (checkbox.getEnabled()) {
					checkbox.setSelected(oEvent.getSource().getSelected());
				}
			}

		},

		/**
		 * gets the indexes of the selected table rows
		 * 
		 * @function getSelectedIndexes
		 */
		getSelectedIndexes: function () {
			var mainTable = this.byId("ClaimsVal_MainTable");

			var selectedRows = mainTable.getSelectedIndices();

			return selectedRows;
		},

		/**
		 * Refresh Icon pressed to refresh data @ onRefreshTable
		 */
		onRefreshTable: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			// If there are changed Input values
			_oController.getView().byId("statusSelectDrpDwn").setSelectedKey("-1");
			if (_hasChanged) {
				this.DataLossWarning =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", this);
				// Set Warning Dialog Buttons
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(this.onDialogWarnigRefresh_Yes);
				sap.ui.getCore().byId("button_Dialog_Warning_No").attachPress(this.onDialogWarnigRefresh_No);
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWyesTXT"));
				sap.ui.getCore().byId("button_Dialog_Warning_No").setText(_oController.getResourceBundle().getText("DLWnoTXT"));
				// Set Warning Dialog Message
				sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWRefreshTitle"));
				sap.ui.getCore().byId("text2_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWRefreshDescription"));
				// Set Warnig Dialog Title
				sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWMainTitle"));
				this.DataLossWarning.open();
				_oController._removeDialogResize(_oController.DataLossWarning);

			} else {
				_oController.loadModelValidationClaims();
			}

			// GTM
			_oController.GTMDataLayer('cvbuttonclick',
				'Claim Validation',
				'Button -click',
				_oController.getResourceBundle().getText("MASCRefreshValSucTXT")
			);

		},

		/**
		 * On Close Dialog Warning Messages
		 */
		onDialogWarnigRefresh_No: function () {
			_oController.DataLossWarning.destroy();

		},
		/**
		 * On Refresh Data on Dialog Warning Messages
		 */
		onDialogWarnigRefresh_Yes: function () {

			_oController.DataLossWarning.destroy();

			_oController.loadModelValidationClaims();

		},

		/**
		 * On Delete Data on Dialog Warning Messages
		 */
		onDialogWarnigDelete_Yes: function () {

			_oController.DataLossWarning.destroy();

			_oController.onDeleteClaims();

		},

		/**
		 * Unselect all rows
		 * 
		 * @function unselectRows
		 */
		unselectRows: function () {
			var mainTable = this.byId("ClaimsVal_MainTable");

			var rows = mainTable.getRows();
			for (var i = 0; i < rows.length; i++) {
				var checkBox = rows[i].getCells()[0];
				checkBox.setSelected(false);
			}
		},
		/**
		 * Press of export claim
		 * 
		 * @function onPressExportTemplateBtn
		 */
		onPressExportTemplateBtn: function () {
			//Ipad resquesting desktop sites is identified as macOS
			if ((sap.ui.Device.os.ios || sap.ui.Device.os.macintosh) && (sap.ui.Device.system.phone || sap.ui.Device.system.tablet)) {
				_oController.onPressExportTemplate("csv");
			} else {
				// IF THE DIALOG IS CLOSED BY PRESSING "ESCAPE"
				if (_oController.oExportDialog) {
					_oController.oExportDialog.destroy();
				}
				_oController.oExportDialog =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.ExportDialog", this);
				_oController._createExportTypeModel(_oController.oExportDialog);
				_oController.oExportDialog.setModel(_oController.getView().getModel("i18n"), "i18n");
				// Set Header
				sap.ui.getCore().byId("exportDialogHeader").setText(_oController.getResourceBundle().getText("MASCExportTemplateHeaderText"));
				// Attach functions
				sap.ui.getCore().byId("export_ExportDialog").attachPress(_oController.onExportTemplateBtn);
				sap.ui.getCore().byId("close_ExportDialog").attachPress(_oController.onCloseExportDialog);
				_oController.oExportDialog.open();
			}
		},

		/**
		 * Exports claim template
		 * 
		 * @function onPressExportTemplate
		 */
		onPressExportTemplate: function (format) {
			// Checks Session time out
			this.checkSessionTimeout();

			var oModel = new sap.ui.model.json.JSONModel();
			var i18nResourceBundle = this.getView().getModel("i18n");
			if (format === "csv") {

				var oExport = new Export({

					exportType: new ExportTypeCSV({
						fileExtension: "csv",
						separatorChar: ","
					}),

					models: oModel,

					rows: {
						path: "/"
					},
					columns: [{
						name: i18nResourceBundle.getProperty("MASCVTemplateCol1"),
						template: {
							content: ""
						}
					}, {
						name: i18nResourceBundle.getProperty("MASCVTemplateCol2"),
						template: {
							content: ""
						}
					}, {
						name: i18nResourceBundle.getProperty("MASCVTemplateCol3"),
						template: {
							content: ""
						}
					}, {
						name: i18nResourceBundle.getProperty("MASCVTemplateCol4"),
						template: {
							content: ""
						}
					}, {
						name: i18nResourceBundle.getProperty("MASCVTemplateCol5"),
						template: {
							content: ""
						}
					}, {
						name: i18nResourceBundle.getProperty("MASCVTemplateCol6"),
						template: {
							content: ""
						}
					}, {
						name: i18nResourceBundle.getProperty("MASCVTemplateCol7"),
						template: {
							content: ""
						}
					}, {
						name: i18nResourceBundle.getProperty("MASCVTemplateCol8"),
						template: {
							content: ""
						}
					}, {
						name: i18nResourceBundle.getProperty("MASCVTemplateCol9"),
						template: {
							content: ""
						}

					}, {
						name: i18nResourceBundle.getProperty("MASCVTemplateCol10"),
						template: {
							content: ""
						}

					}, {
						name: i18nResourceBundle.getProperty("MASCVTemplateCol11"),
						template: {
							content: ""
						}

					}]
				});
				oExport.saveFile("supplementalClaimsTemplate").catch(function () {

				}).then(function () {
					oExport.destroy();
				});
			} else {
				var aXLSXFormatTemplate = _oController._createExcelFormatTemplate();
				var wb = _oController._jsonToWorkbook(aXLSXFormatTemplate, "supplementalClaimsTemplate");
				_oController._saveFileToExcel(wb, "supplementalClaimsTemplate");
			}
			// GTM
			_oController.GTMDataLayer('cvbuttonclick',
				'Claim Validation',
				'Button -click',
				_oController.getView().byId("SupplementalsClaimsExportTemplate").getText()
			);
		},

		/**
		 * This function saves the input values into a array @ function
		 * valueChangeInputTable @ parameter (oEvent)
		 */
		valueChangeInput: function () {
			_hasChanged = true;
		},

		/**
		 * Navs back to Supplemental Rates page
		 * 
		 * @function backToRates
		 */
		backToRates: function () {

			// GTM
			_oController.GTMDataLayer('cvbuttonclick',
				'Validate Claims',
				'Button -click',
				_oController.getResourceBundle().getText("MASCVTextBRate")
			);

			_oRouter.navTo("myAccountSupplementalRate");
		},

		/**
		 * Gets the description of a statusCode
		 * 
		 * @function getStatusDescription
		 */
		getStatusDescription: function (statusCode) {
			for (var i = 0; i < _claimStatus.length; i++) {
				if (_claimStatus[i].value === statusCode) {
					return _claimStatus[i].description;
				}
			}
		},

		/**
		 * Clears all sorts and filters on the table
		 * 
		 * @function clearTableFilterSort
		 */

		clearTableFilterSort: function () {
			var table = this.getView().byId("ClaimsVal_MainTable");
			var iColCounter = 0;
			var iTotalCols = table.getColumns().length;
			var oListBinding = table.getBinding();
			_oController.getView().byId("statusSelectDrpDwn").setSelectedKey("-1");
			if (oListBinding) {
				oListBinding.aSorters = null;
				oListBinding.aFilters = null;
			}

			for (iColCounter; iColCounter < iTotalCols; iColCounter++) {
				table.getColumns()[iColCounter].setSorted(false);
				table.getColumns()[iColCounter].setFilterValue("");
				table.getColumns()[iColCounter].setFiltered(false);
			}
		},

		onTableSort: function (oEvent) {
			// GTM
			_oController.GTMDataLayer('cvfilter',
				'Validate Claims',
				oEvent.getParameter("sortOrder"),
				sap.ui.getCore().byId(oEvent.getParameter("column").getId()).getLabel().getText()
			);
		},

		capitalizeFirstLetter: function (string) {
			return string.charAt(0).toUpperCase() + string.slice(1);
		},

		onFilterSort: function (oEvent) {
			// GTM
			_oController.GTMDataLayer('cvfilter',
				'Validate Claims',
				_oController.capitalizeFirstLetter(oEvent.getId()) + ": " + oEvent.getParameter("value"),
				sap.ui.getCore().byId(oEvent.getParameter("column").getId()).getLabel().getText()
			);
		},

		/**
		 * close of export dialog
		 * 
		 * @function onCloseExportDialog
		 */
		onCloseExportDialog: function () {
			_oController.oExportDialog.destroy();
		},

		/**
		 * change of export type
		 * 
		 * @function onChangeExportType
		 */
		onChangeExportType: function (oEvent) {
			if (oEvent.getSource().getSelectedKey()) {
				_oController.oExportDialog.getModel("oExportTypes").setProperty("/0/enabledProperty", false);
				sap.ui.getCore().byId("export_ExportDialog").setEnabled(true);
			} else {
				sap.ui.getCore().byId("export_ExportDialog").setEnabled(false);
			}
		},

		/**
		 * click of export btn
		 * 
		 * @function onExportClaimBtn
		 */
		onExportClaimBtn: function () {
			var sSelectedFormat = sap.ui.getCore().byId("exportType").getSelectedKey();
			switch (sSelectedFormat) {
			case "1":
				_oController.onPressExportClaims("csv");
				break;
			case "2":
				_oController.onPressExportClaims("xlxs");
				break;
			default:
				break;
			}
			_oController.oExportDialog.destroy();
		},

		/**
		 * click of export btn
		 * 
		 * @function onExportTemplateBtn
		 */
		onExportTemplateBtn: function () {
			var sSelectedFormat = sap.ui.getCore().byId("exportType").getSelectedKey();
			switch (sSelectedFormat) {
			case "1":
				_oController.onPressExportTemplate("csv");
				break;
			case "2":
				_oController.onPressExportTemplate("xlxs");
				break;
			default:
				break;
			}
			_oController.oExportDialog.destroy();
		},

		/**
		 * method to format the Excel Data
		 * @function _createExcelFormatTemplate
		 * @param {isHeader} Header
		 * @returns Header Data
		 */
		_createExcelFormatTemplate: function (isHeader) {
			var aXLSXFormatTemplate = [];
			var oXLSXFormatHeader = {};
			var aXLSXFormatHeader = [
				_oController.getResourceBundle().getText("MASCVTemplateCol1"),
				_oController.getResourceBundle().getText("MASCVTemplateCol2"),
				_oController.getResourceBundle().getText("MASCVTemplateCol3"),
				_oController.getResourceBundle().getText("MASCVTemplateCol4"),
				_oController.getResourceBundle().getText("MASCVTemplateCol5"),
				_oController.getResourceBundle().getText("MASCVTemplateCol6"),
				_oController.getResourceBundle().getText("MASCVTemplateCol7"),
				_oController.getResourceBundle().getText("MASCVTemplateCol8"),
				_oController.getResourceBundle().getText("MASCVTemplateCol9"),
				_oController.getResourceBundle().getText("MASCVTemplateCol10"),
				_oController.getResourceBundle().getText("MASCVTemplateCol11")
			];
			if (isHeader === "header") {
				return aXLSXFormatHeader;
			}
			for (var i = 0; i < aXLSXFormatHeader.length; i++) {
				oXLSXFormatHeader[aXLSXFormatHeader[i]] = "";
			}
			aXLSXFormatTemplate.push(oXLSXFormatHeader);
			return aXLSXFormatTemplate;
		},

		/**
		 * method to format the Excel Data
		 * @function _createExcelFormatData
		 * @param {data} Raw Data
		 * @returns Formatted Data
		 */
		_createExcelFormatData: function (data) {
			var aXLSXFormatData = [];
			var oXLSXFormatHeader = {
				"accname": _oController.getResourceBundle().getText("MNText1BT"),
				"accountnumber": _oController.getResourceBundle().getText("MNText2BT"),
				"materialNumber": _oController.getResourceBundle().getText("MNText3BT"),
				"sellingPrice": _oController.getResourceBundle().getText("MASPRSellingTBL"),
				"supplementalPrice": _oController.getResourceBundle().getText("MNText5BT"),
				"gallonsSold": _oController.getResourceBundle().getText("MNText6BT"),
				"invoiceNumber": _oController.getResourceBundle().getText("MNText7BT"),
				"invoiceDate": _oController.getResourceBundle().getText("MASCVTemplateCol7"),
				"actualSellingPrice": _oController.getResourceBundle().getText("MASCVTemplateCol10"),
				"status": _oController.getResourceBundle().getText("MNText10BT"),
				"validationError": _oController.getResourceBundle().getText("SCVPErrorMessageCSVTXT")
			};
			var aKeys = Object.keys(oXLSXFormatHeader);
			for (var i = 0; i < data.length; i++) {
				var oRow = {};
				for (var j = 0; j < aKeys.length; j++) {
					oRow[oXLSXFormatHeader[aKeys[j]]] = data[i][aKeys[j]];
				}
				aXLSXFormatData.push(oRow);
			}
			return aXLSXFormatData;
		},

		/**
		 * convert excel to array
		 * 
		 * @function _excelDataToArray
		 */
		_excelDataToArray: function (data) {
			var wb = XLSX.read(data, {
				type: 'binary'
			});
			var sheetName = wb.SheetNames[0];
			var aData = XLSX.utils.sheet_to_json(wb.Sheets[sheetName], {
				raw: false
			});
			return aData;
		},

		/**
		 * convert the object to array
		 * 
		 * @function _objToArray
		 */
		_objToArray: function (data) {
			var aData = [];
			var isValid = false;
			var aHeader = _oController._createExcelFormatTemplate("header");
			for (var i = 0; i < aHeader.length; i++) {
				aData.push(data[aHeader[i]] ? (data[aHeader[i]] + "") : "");
				if (data[aHeader[i]] !== undefined) {
					isValid = true;
				}
			}
			if (isValid) {
				return aData;
			} else {
				aData.pop();
				return aData;
			}
		},

		/**
		 * check for empty array
		 * 
		 * @function _checkForEmptyExcel
		 */
		_checkForEmptyExcel: function (data) {
			var aHeader = Object.keys(data);
			for (var i = 0; i < aHeader.length; i++) {
				if (data[aHeader[i]]) {
					return false;
				}
			}
			return true;
		}
	});
});