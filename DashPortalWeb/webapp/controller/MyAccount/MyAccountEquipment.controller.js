sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"valvoline/dash/portal/DashPortalWeb/controls/CustomPanelCases",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter"
], function (Base, CustomPanelCases, JSONModel) {
	"use strict";
	var _oRouter, _oView, _oController, _oAccount, _1stRender, _sSearch, _sCallerFocusOut, _sCallerSearchClear, _sCallerLiveSearch,
		_selectedActvResult = 10;
	var _resultsList = [10, 25, 50, 100, 200];
	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.MyAccount.MyAccountEquipment", {
		/** @module MyAccountCases */
		/**
		 * This function initializes the controller and sets the selectedAccount
		 * model.
		 * 
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oRouter = this.getRouter();
			_oRouter.getRoute("myAccountEquipment").attachPatternMatched(this._onObjectMatched, this);
			_oController = this;
			_oView = _oController.getView();
			_1stRender = true;
			_sCallerFocusOut = 'FocusOut';
			_sCallerSearchClear = 'SearchClear';
			_sCallerLiveSearch = 'LiveSearch';
		},
		/**
		 * If it is the first render, calls the the function to attach the focus out Events
		 * @function _onObjectMatched
		 **/
		onBeforeRendering: function () {
			if (_1stRender) {
				setTimeout(function () {
					_oController.attachSearchFocusOut();
				}, 0);
				_1stRender = false;
			}
		},

		/**
		 * Set the filter for the selected account and get cases for selected
		 * accounts.
		 * 
		 * @function _onObjectMatched
		 * @param {Object}
		 *            event - Triggered when navigating to myAccountCases page
		 */
		_onObjectMatched: function (Event) {
			// Clears all error message
			_oController.clearServerErrorMsg(_oController);

			// set the navbar icon as selected
			_oController.resetMenuButtons();

			// Page authorization and layout check
			if (!_oController.checkDASHPageAuthorization("EQUIPMENT_PAGE")) {
				return;
			}

			//get user info and direct account
			this.getUserInformation(this);

			// check Language and set selected Key in combo box
			_oController.onChangeLanguage();

			_oController.headerIconAccess();

			if (sap.ui.getCore().getModel("oSelectedAccount") === undefined) {
				//set selected account to a fake account
				var oAccount;
				var fakeAccount = {
					id: "0",
					billingcity: _oController.getResourceBundle().getText("MAresultsTXT")
				};
				oAccount = new sap.ui.model.json.JSONModel(fakeAccount);
				sap.ui.getCore().setModel(oAccount, "oSelectedAccount");
				_oView.setModel(oAccount, "oSelectedAccount");
			} else {
				_oView.setModel(sap.ui.getCore().getModel("oSelectedAccount"), "oSelectedAccount");
			}

			_oController.getView().byId("accountInfoContainerDesktop").setVisible(false);
			_oController.getAccountsByDistrict(_oController, "comboBoxAccountEquipment", false, this.reconfigAddressLabel);

			//Clear the equipment search field and set the combobox to the 
			_oController.getView().byId("searchFieldEquipment").setValue("");

			// Shopping Cart Values
			_oController.getShoppingCartCookie(_oController);

			// Get all Equipment data	
			_oController.getEquipmentData("0", "0");
		},

		/**
		 * Attach the focusout event to the Search field 
		 * @function _onObjectMatched
		 */
		attachSearchFocusOut: function () {
			var inputSearchOrderDesktop = this.getView().byId("searchFieldEquipment").getDomRef();
			inputSearchOrderDesktop.addEventListener("focusout", function () {
				_oController.gtmFocusOut(_sCallerFocusOut);
			});
		},

		/**
		 * This function handles the focus out of the search bar setting the GTM tags
		 * @function gtmFocusOut
		 */
		gtmFocusOut: function (sCaller) {
			var sSearch;
			switch (sCaller) {
			case _sCallerSearchClear:
				if (_sSearch === "") {
					return;
				}
				break;
			case _sCallerLiveSearch:
				sSearch = _oController.getView().byId("searchFieldEquipment").getValue();
				if (sSearch !== "" && sSearch !== null) {
					_sSearch = sSearch;
				}
				return;
				break;
			case _sCallerFocusOut:
				sSearch = _oController.getView().byId("searchFieldEquipment").getValue();
				if (_sSearch === undefined || _sSearch === null || _sSearch === "" || sSearch === "") {
					return;
				}
				break;
			default:
				break;
			}
			//GTM Tag
			//GTMDataLayer(event, eventCategory, eventAction, eventLabel)
			_oController.GTMDataLayer(
				'equpmntSearch',
				'Equipment',
				'Equipment Keyword Search',
				_sSearch
			);
			if (sCaller === _sCallerSearchClear) {
				_sSearch = "";
			}
		},

		/**
		 * This function is responsible for checking if the clearbutton was pressed, if so trigger the GTM logic.
		 * @function onSearch
		 */
		onSearch: function (oEvent) {
			if (oEvent.getParameters().clearButtonPressed === true) {
				_oController.gtmFocusOut(_sCallerSearchClear);
			}
		},

		/**
		 * This function scrolls to the top of the page.
		 * @function onScrollUp
		 */
		onScrollUp: function () {
			var oScrollContainer = this.getView().getContent()[0];
			oScrollContainer.scrollTo(0, 700);
		},

		/**
		 * If the parameter is empty or undefined returns false
		 * @function isVisibleSerialNumber
		 */
		isVisibleSerialNumber: function (sText) {
			return (sText !== undefined && sText !== "");
		},
		/**
		 * Sets the Navigations elements visible, depending if there are Equipment or not
		 * @function isVisibleNavigation
		 */
		isVisibleNavigation: function (aEquipments) {
			return (aEquipments !== undefined && aEquipments !== null && aEquipments.length > 0);
		},

		/**
		 * Function that handles the success case of the ajax call that gets the active cases
		 * @function ongGetActiveCasesSuccess
		 * 
		 * @param {object} oController - Desired context 
		 * @param {object} oData - Response object from the service
		 * @param {object} oPiggyBack - Data that may be transfered from the original call
		 */
		onGetEquipmentDataSuccess: function (oController, oData, oPiggyBack) {
			var iFrom = 1,
				iTo = Math.min(10, oData.length),
				iOf = oData.length;
			var mEquipment = new sap.ui.model.json.JSONModel({
				"equipments": [],
				"pagination": {
					"from": iFrom,
					"to": iTo,
					"of": iOf
				}
			});
			var mEquipmentFullList = new sap.ui.model.json.JSONModel(oData);
			var i;
			for (i = 0; i < iTo; i++) {
				mEquipment.getData().equipments.push(oData[i]);
			}
			_oView.byId("equipmentResultsPerPage").setVisible(iOf > 10);
			_oView.byId("equipmentResults50").setVisible(iOf > 25);
			_oView.byId("equipmentResults100").setVisible(iOf > 50);
			_oView.byId("equipmentResults200").setVisible(iOf > 100);
			_oController.removeSearchResultsStyleClass("equipmentResults", _resultsList, _oView);
			_oView.byId("equipmentResults" + _selectedActvResult).addStyleClass("numberOfResultsLinkSelected");

			if (iOf > 100) {
				mEquipment.setSizeLimit(200);
			}
			_oView.setModel(mEquipment, "mEquipment");
			_oView.setModel(mEquipmentFullList, "mEquipmentFullList");
			_oView.setModel(new JSONModel(JSON.parse(JSON.stringify(oData))), "mEquipmentFullListIntact");
			oController.getView().byId("equipmentListContainer").setBusy(false);
		},

		/**
		 * Function that handles the error case of the ajax call that gets the active cases
		 * @function onGetActiveCasesError
		 * 
		 * @param {object} oController - Desired context 
		 * @param {object} oError - Error object from the service
		 * @param {object} oPiggyBack - Data that may be transfered from the original call
		 */
		onGetEquipmentDataError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERREquipmentData"));
			oController.getView().byId("equipmentListContainer").setBusy(false);
		},

		/**
		 * Function responsible for returning the equipment data
		 * 
		 * @function getEquipmentData
		 * @param {String} idAccount - Value 
		 * @param {String} districtFlag - Equipment Name
		 */
		getEquipmentData: function (idAccount, districtFlag) {
			var xsURL;
			if (idAccount !== "0" && districtFlag === "0") {
				xsURL = "/salesforce/assets?accountId=" + idAccount;
			} else if (idAccount === "0" && districtFlag !== "0") {
				xsURL = "/salesforce/assets?customerreportinggroup=" + districtFlag;
			} else {
				xsURL = "/salesforce/assets";
			}
			_selectedActvResult = 10;
			_oController.getView().byId("equipmentListContainer").setBusyIndicatorDelay(0);
			_oController.getView().byId("equipmentListContainer").setBusy(true);
			_oController.getView().byId("activeNavigatorLabel").setBusyIndicatorDelay(0);
			_oController.getView().byId("activeNavigatorLabel").setBusy(true);
			_oController.getView().byId("toLeftActive").setEnabled(false);
			_oController.getView().byId("toRightActive").setEnabled(false);
			_oController.getView().byId("equipmentResultsPerPage").setVisible(false);

			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onGetEquipmentDataSuccess, _oController.onGetEquipmentDataError);
		},

		/**
		 * This function resets the AddressLabel when you navigate to the page.
		 * @function reconfigAddressLabel
		 */
		reconfigAddressLabel: function () {
			_oController.getView().byId("comboBoxAccountEquipment").setSelectedKey(0);
			_oAccount = _oController.findAccountByDistrict("0", _oController);
			sap.ui.getCore().setModel(new JSONModel(_oAccount), "oSelectedAccount");
			_oView.setModel(new JSONModel(_oAccount), "oSelectedAccount");
			_oController.getView().byId("accountInfoContainerDesktop").setVisible(false);
		},

		/**
		 * Function that colapses the panel when the name binding is changed
		 * 
		 * @function isPanelExpanded
		 * @param {bolean} sExpanded - Boolean setting the panel as expanded
		 * @param {String} sName - Equipment Name
		 * @return {boolean}
		 */
		isPanelExpanded: function (bExpanded, sName) {
			return (bExpanded === true);
		},

		/**
		 * Responsible for hadling the navigation click so as the elements to be presented on the main model.
		 * 
		 * @function changeNavigationPage
		 * @param {object} page - Event that trigerend the function
		 */
		changeNavigationPage: function (page) {
			var iTreshold = _selectedActvResult;
			var sNavDirection = page.getSource().data("navDirection");
			var oEquipmentsToPresent = _oView.getModel("mEquipment");
			var i,
				iFrom = oEquipmentsToPresent.getData().pagination.from,
				iTo = oEquipmentsToPresent.getData().pagination.to,
				iOf = oEquipmentsToPresent.getData().pagination.of;
			var mEquipmentFullList = _oView.getModel("mEquipmentFullList");
			_oController.getView().byId("equipmentListContainer").setBusy(true);
			_oController.getView().byId("activeNavigatorLabel").setBusy(true);
			_oController.getView().byId("toLeftActive").setEnabled(false);
			_oController.getView().byId("toRightActive").setEnabled(false);
			oEquipmentsToPresent.getData().equipments = [];
			if (sNavDirection === "Right") {
				iFrom = parseInt(iFrom, 10) + iTreshold;
				oEquipmentsToPresent.getData().pagination.from = iFrom;
				iTo = parseInt(iTo, 10) + iTreshold;
				oEquipmentsToPresent.getData().pagination.to = iTo;

				if (iTo > iOf) {
					iTo = iOf;
				}
				for (i = iFrom - 1; i < iTo; i++) {
					oEquipmentsToPresent.getData().equipments.push(mEquipmentFullList.getData()[i]);
				}
			} else {
				iFrom = parseInt(iFrom, 10) - iTreshold;
				oEquipmentsToPresent.getData().pagination.from = iFrom;
				iTo = parseInt(iTo, 10) - iTreshold;
				oEquipmentsToPresent.getData().pagination.to = iTo;

				if (iFrom < 1) {
					iFrom = 1;
				}
				for (i = iFrom - 1; i < iTo; i++) {
					oEquipmentsToPresent.getData().equipments.push(mEquipmentFullList.getData()[i]);
				}
			}
			oEquipmentsToPresent.updateBindings();
			_oController.getView().byId("equipmentListContainer").setBusy(false);

			var elmnt = _oView.byId("searchFieldEquipment");
			if (sap.ui.Device.system.phone === true) {
				setTimeout(function () {
					var b = $("#" + _oView.byId("comboBoxAccountEquipment").getId());
					b[0].scrollIntoView({
						behavior: "smooth", // or "auto" or "instant"
						block: "start"
					});
				}, 0);
			} else if (sap.ui.Device.browser.internet_explorer === true || sap.ui.Device.system.tablet === true) {
				setTimeout(function () {
					var b = $("#" + _oView.byId("searchFieldEquipment").getId());
					b[0].scrollIntoView({
						behavior: "smooth", // or "auto" or "instant"
						block: "start"
					});
				}, 0);
			} else {
				_oController.getScrollElement().element.scrollToElement(elmnt);
			}

		},
		/**
		 * Sets the pagination text so as the state of the arrows(enabled/disabled).
		 * 
		 * @function setPaginationText
		 * @param {number} iFrom - Value that defines the start of the pagination
		 * @param {number} iTo - Value that defines the end of the pagination
		 * @param {number} iOf - Value that defines the total number of entries
		 * @return {String} Value to be presented on the pagination state
		 */
		setPaginationText: function (iFrom, iTo, iOf) {
			if (iTo >= iOf) {
				iTo = iOf;
				_oController.getView().byId("toRightActive").setEnabled(false);
			} else {
				_oController.getView().byId("toRightActive").setEnabled(true);
			}
			if (iFrom === 1) {
				if (iFrom > iOf) {
					iFrom = iOf;
				}
				_oController.getView().byId("toLeftActive").setEnabled(false);
			} else {
				_oController.getView().byId("toLeftActive").setEnabled(true);
			}
			_oController.getView().byId("activeNavigatorLabel").setBusy(false);
			return iFrom + "-" + iTo + " " + _oController.getResourceBundle().getText("MAEQOfTXT") + " " + iOf;
		},
		/**
		 * JSONFilter function that receives the oData and filters
		 * 
		 * @param {string} filterValue - Value that will be used to filter the model
		 * @param {object} oModel - Model containing the values to be filtered
		 * @return {object} Model to each the filter was already applied
		 */
		JSONFilter: function (filterValue, oModel) {
			// DataJSON original and filtered
			var oDataJSON = oModel;
			var oDataJSONFiltered = [];
			var sEquipmentName, sSerialNumber, sAssetNumber;
			var oPartialEquipment;
			var i, j;
			for (i = 0; i < oDataJSON.length; i++) {
				// JSON Parameters to be filtered
				sEquipmentName = oDataJSON[i].equipmentName.toLowerCase();
				if (sEquipmentName.indexOf(filterValue.toLowerCase()) !== -1) {
					oDataJSONFiltered.push(oDataJSON[i]);
				} else {
					oPartialEquipment = {
						"equipmentName": oDataJSON[i].equipmentName,
						"assets": [],
						"expanded": true
					};
					for (j = 0; j < oDataJSON[i].assets.length; j++) {
						if (oDataJSON[i].assets[j].serialNumber !== undefined) {
							sSerialNumber = oDataJSON[i].assets[j].serialNumber;
						} else {
							sSerialNumber = "";
						}
						sAssetNumber = oDataJSON[i].assets[j].assetNumber;
						if (sSerialNumber.toLowerCase().indexOf(filterValue.toLowerCase()) !== -1 || sAssetNumber.toLowerCase()
							.indexOf(filterValue.toLowerCase()) !== -1) {
							oPartialEquipment.assets.push({
								"assetNumber": sAssetNumber,
								"serialNumber": sSerialNumber
							});
						}
					}
					if (oPartialEquipment.assets.length > 0) {
						oDataJSONFiltered.push(oPartialEquipment);
					}
				}
			}
			return oDataJSONFiltered;
		},
		/**
		 * Filters the table
		 * @function onLiveSearchEquipment
		 * @param {object} oEvent - Means to retrieve the search field input
		 */
		onLiveSearchEquipment: function (oEvent) {
			// Get value of search field
			var newValue = oEvent.getParameter("newValue");

			//Set up GTM values
			_oController.gtmFocusOut(_sCallerLiveSearch);

			// Setup all needed modelss
			var oEquipmentsToPresent = _oView.getModel("mEquipment");
			var mEquipmentFullList = _oView.getModel("mEquipmentFullList");
			var mEquipmentFullListIntact = JSON.parse(JSON.stringify(_oView.getModel("mEquipmentFullListIntact").getData()));
			var i, iFrom, iTo, iOf;

			if (!newValue) {
				// Equals to the Pagination Model
				mEquipmentFullList.setData(mEquipmentFullListIntact);
				iFrom = 1;
				iTo = Math.min(_selectedActvResult, mEquipmentFullList.getData().length);
				iOf = mEquipmentFullList.getData().length;
				oEquipmentsToPresent.getData().pagination.from = iFrom;
				oEquipmentsToPresent.getData().pagination.to = iTo;
				oEquipmentsToPresent.getData().pagination.of = iOf;
				oEquipmentsToPresent.getData().equipments = [];
				for (i = 0; i < iTo; i++) {
					oEquipmentsToPresent.getData().equipments.push(mEquipmentFullList.getData()[i]);
				}
			} else {
				// Filtering the oModel
				mEquipmentFullList.setData(this.JSONFilter(newValue, mEquipmentFullListIntact));

				// Equals to the Pagination Model
				iFrom = 1;
				iTo = Math.min(_selectedActvResult, mEquipmentFullList.getData().length);
				iOf = mEquipmentFullList.getData().length;
				oEquipmentsToPresent.getData().pagination.from = iFrom;
				oEquipmentsToPresent.getData().pagination.to = iTo;
				oEquipmentsToPresent.getData().pagination.of = iOf;
				oEquipmentsToPresent.getData().equipments = [];
				for (i = 0; i < iTo; i++) {
					oEquipmentsToPresent.getData().equipments.push(mEquipmentFullList.getData()[i]);
				}
			}
			_oView.byId("equipmentResultsPerPage").setVisible(iOf > 10);
			oEquipmentsToPresent.updateBindings();
		},
		/**
		 * On location change, redefine the cases to loaded.
		 * 
		 * @function onChangeAccountComboBox
		 * @param {object} oEvent - Event that trigered the function
		 */
		onChangeAccountComboBox: function (oEvent) {
			// Checks Session time out
			_oController.checkSessionTimeout();

			_oController.getView().byId("searchFieldEquipment").setValue("");
			if (oEvent.getParameter("newValue") === "") {
				_oView.byId("comboBoxAccountEquipment").setSelectedKey("0");
			}
			var selectedKey = _oController.byId("comboBoxAccountEquipment").getSelectedKey();
			var districtFlag;
			var accountID;
			if (selectedKey.indexOf("district") === -1) {
				_oAccount = this.findAccountByDistrict(selectedKey, _oController);
				if (_oAccount === undefined) {
					_oView.byId("comboBoxAccountEquipment").setSelectedKey("0");
					_oAccount = this.findAccountByDistrict("0", _oController);
				}
				if (_oAccount.id === "0") {
					_oView.byId("comboBoxAccountEquipment").setSelectedKey("0");
					_oView.byId("accountAddress1Mobile").setVisible(false);
				} else {
					_oView.byId("accountAddress1Mobile").setVisible(true);
				}
				sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
				_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
				accountID = sap.ui.getCore().getModel("oSelectedAccount").getData().id;
				districtFlag = "0";
			} else if (selectedKey.indexOf("district") !== -1) {
				_oAccount = [];
				_oAccount.id = "district-" + oEvent.getParameters().value;
				_oAccount.billingcity = _oController.getResourceBundle().getText("MAresultsTXT");
				sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
				_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
				accountID = "0";
				districtFlag = oEvent.getParameters().value;
			} else {
				return;
			}
			if (this.getView().byId("comboBoxAccountEquipment").getSelectedKey() === "0" || this.getView().byId("comboBoxAccountEquipment").getSelectedKey()
				.indexOf("district") !== -1) {
				this.getView().byId("accountInfoContainerDesktop").setVisible(false);
			} else {
				this.getView().byId("accountInfoContainerDesktop").setVisible(true);
			}
			_oController.getEquipmentData(accountID, districtFlag);
		},

		/**
		 *  Select the number of results displayed on a page
		 *  @selectResultsPerPage
		 */
		selectResultsPerPage: function (oEvent) {
			var oEquipmentsToPresent = _oView.getModel("mEquipment");
			var i,
				iFrom,
				iTo,
				iOf = oEquipmentsToPresent.getData().pagination.of;
			var sValue = oEvent.getSource().getText();
			_selectedActvResult = parseInt(sValue, 10);
			var oSelectedLink = _oView.byId("equipmentResults" + _selectedActvResult);
			_oController.removeSearchResultsStyleClass("equipmentResults", _resultsList, _oView);
			oSelectedLink.addStyleClass("numberOfResultsLinkSelected");
			var mEquipmentFullList = _oView.getModel("mEquipmentFullList");
			iFrom = 1;
			iTo = _selectedActvResult;
			_oController.getView().byId("toLeftActive").setEnabled(false);
			if (iTo >= iOf) {
				iTo = iOf;
				_oController.getView().byId("toRightActive").setEnabled(false);
			} else {
				_oController.getView().byId("toRightActive").setEnabled(true);
			}
			oEquipmentsToPresent.getData().equipments = [];
			for (i = iFrom - 1; i < iTo; i++) {
				oEquipmentsToPresent.getData().equipments.push(mEquipmentFullList.getData()[i]);
			}
			oEquipmentsToPresent.getData().pagination.from = iFrom;
			oEquipmentsToPresent.getData().pagination.to = iTo;
			oEquipmentsToPresent.updateBindings();
		},

		/**
		 * Check if table have Data
		 * @function formatterNoEquipmentsVisiblity
		 **/
		formatterNoEquipmentsVisiblity: function (model) {
			if (Object.keys(model).length === 0) {
				return true;
			}
			return false;
		}
	});
});