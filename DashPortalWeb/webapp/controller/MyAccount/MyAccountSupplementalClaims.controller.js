sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	'sap/ui/model/Sorter',
	'sap/ui/core/util/Export',
	'sap/ui/core/util/ExportTypeCSV'
], function (Base, Sorter, Export, ExportTypeCSV) {
	"use strict";

	var _begin_Active = null;

	// Table pagination size
	var _tablePaginationSize = 10,
		_selectedActvResult = 10,
		_createSelectionRange = true,
		_resultsList = [10, 25, 50, 100, 200];
	var _oModelSupplementalClaims_Active = new sap.ui.model.json.JSONModel();
	var _oController, _oView;
	var _oRouter;

	// Sorting data

	var _oSortOptions = new sap.ui.model.json.JSONModel([]),
		_oSortedColumns = new sap.ui.model.json.JSONModel([]),
		_oSortedColumnsTemp = new sap.ui.model.json.JSONModel([]),
		_oNumericColumns = [2, 3, 5],
		_oDateColumns = 7,
		_aSorters = [],
		_aSortedModel,
		_isAdvancedSortActive = false,
		_oSingleSortData,
		_filterStatusList;

	//Filter Data
	var _aFinaldModel;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.MyAccount.MyAccountSupplementalClaims", {
		/** @module SupplementalRate */
		/**
		 * This function initializes the controller and sets the selectedAccount model.
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oRouter = this.getRouter();
			_oController = this;
			_oView = this.getView();
			_oRouter.getRoute("MyAccountSupplementalClaims").attachPatternMatched(this._onObjectMatched, this);
			this.searchFieldEnabled();
		},

		/**
		 * Loads user data information
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {
			// Clears all error message
			this.clearServerErrorMsg(this);

			//set the navbar icon as selected
			this.resetMenuButtons();

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("SUPPLEMENTAL_CLAIM_PAGE")) {
				return;
			}

			//get user info and direct account
			_oController.getUserInformation(_oController);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			this.headerIconAccess();

			this.headerIconAccess();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			// Load Supplemental Rate
			this.loadModelSupplementalRates();

			this.submitMonthlyPaymentAble();

			this.displayLayoutAccess();
		},

		/**
		 * Function to enabled the "touch" properties of the searchfield
		 * @function searchFieldEnabled
		 */
		searchFieldEnabled: function () {
			var controller = this;
			this.getView().addEventDelegate({
				onAfterShow: function () {
					var inputID = controller.getView().createId("searchFieldSupplementalClaims");
					$("#" + inputID + "-I").attr("readonly", false);
				}
			});
		},

		/**
		 * 
		 * 
		 */
		onFilterStatus: function () {
			this.onLiveSearchSupplementClaims();

			// Fetching the changed status
			var statusSelect = _oController.getView().byId("statusFilterSelect");

			if (statusSelect.getSelectedItem() && statusSelect.getSelectedItem().getText()) {
				//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
				_oController.GTMDataLayer('scstatusfilter',
					'Supplemental Claims',
					'Status Filter  -Dropdown',
					statusSelect.getSelectedItem().getText()
				);
			}
		},

		/**
		 * Filters the table
		 * @function onLiveSearchUserManagement
		 * @param {Object} oEvent - Search field input
		 */
		onLiveSearchSupplementClaims: function (oEvent) {
			// Get value of search field
			var newValue = _oController.getView().byId("searchFieldSupplementalClaims").getValue();
			var statusSelect = _oController.getView().byId("statusFilterSelect");

			var status = (statusSelect.getSelectedItem() !== null && statusSelect.getSelectedKey() !== "0") ?
				statusSelect.getSelectedItem().getText() : "";

			// Setup all needed models
			var oModelUserManagementTableFiltered_Active = new sap.ui.model.json.JSONModel();

			// Filtering the oModel
			oModelUserManagementTableFiltered_Active.setData(this.JSONFilter(newValue, _aSortedModel, status));

			// Equals to the Pagination Model
			_aFinaldModel = oModelUserManagementTableFiltered_Active;

			_begin_Active = 0;
			_createSelectionRange = false;

			this.tablePagination("SupplementalClaimsTable_Active", _begin_Active);
		},

		/**
		 * JSONFilter function that receives the oData and filters
		 * @ parameter (filterValue, oModel)
		 */
		JSONFilter: function (filterValue, oModel, statusValue) {
			// DataJSON original and filtered
			var oDataJSON = oModel.getData();
			var oDataJSONFiltered = [];
			if (filterValue === undefined) {
				filterValue = "";
			}
			for (var i = 0; i < oDataJSON.length; i++) {
				// JSON Parameters to be filtered
				var accname = oDataJSON[i].accName.toLowerCase();
				var accountnumber = oDataJSON[i].accountnumber.toLowerCase();
				var materialNumber = oDataJSON[i].materialNumber.toLowerCase();
				var status = oDataJSON[i].status.toLowerCase();
				if ((accname.indexOf(filterValue.toLowerCase()) !== -1 || materialNumber.indexOf(filterValue.toLowerCase()) !== -1 ||
						accountnumber
						.indexOf(filterValue.toLowerCase()) !== -1) && status.indexOf(statusValue.toLowerCase()) !== -1) {
					oDataJSONFiltered.push(oDataJSON[i]);
				}
			}
			return oDataJSONFiltered;
		},

		/**
		 * Sets the layout of the arrows for the table and controls the date to be loaded in the table
		 * @function tablePagination
		 */
		tablePagination: function (tableID, beginCounter) {

			var oDataTablePaginated = [];
			var oModelTablePaginated = new sap.ui.model.json.JSONModel();
			_tablePaginationSize = _selectedActvResult;

			// Set up the arrows of table
			var SupplementalRateTable = this.getView().byId(tableID);
			var navigationArrows = this.byId("navigationArrows_" + tableID);
			var rightNavigationArrow = this.byId("idRightNav_" + tableID);
			var leftNavigationArrow = this.byId("idLeftNav_" + tableID);
			var textCounterNavigation = this.byId("idTextCount_" + tableID);

			var _iLengthActive = 0;

			if (_aFinaldModel && _aFinaldModel.getData()) {
				_iLengthActive = _aFinaldModel.getData().length;
			}
			var end = beginCounter + _tablePaginationSize;

			// Set visible or not the table arrows if there is no active items to show in table
			if (_iLengthActive === 0) {
				navigationArrows.setVisible(false);
			} else {
				navigationArrows.setVisible(true);
			}

			_oController.getView().byId("supplClaimResultsPerPage").setVisible(_iLengthActive > 10);
			if (_createSelectionRange) {
				_oController.getView().byId("supplClaimResults50").setVisible(_iLengthActive > 25);
				_oController.getView().byId("supplClaimResults100").setVisible(_iLengthActive > 50);
				_oController.getView().byId("supplClaimResults200").setVisible(_iLengthActive > 100);
			}
			_oController.removeSearchResultsStyleClass("supplClaimResults", _resultsList, _oView);
			_oController.getView().byId("supplClaimResults" + _selectedActvResult).addStyleClass("numberOfResultsLinkSelected");

			if (beginCounter < _tablePaginationSize) {
				leftNavigationArrow.setEnabled(false);
			} else {
				leftNavigationArrow.setEnabled(true);
			}

			if (end >= _iLengthActive) {
				end = _iLengthActive;
				rightNavigationArrow.setEnabled(false);
			} else {
				rightNavigationArrow.setEnabled(true);
			}

			if (_iLengthActive < _tablePaginationSize) {
				end = _iLengthActive;
			}

			if (_iLengthActive !== 0) {
				textCounterNavigation.setText(beginCounter + 1 + "-" + end + " " + _oController.getResourceBundle().getText("of") + " " +
					_iLengthActive);
			}

			for (var i = beginCounter; i < end; i++) {
				if (_aFinaldModel) {
					oDataTablePaginated.push(_aFinaldModel.getProperty("/" + i));
				}
			}
			oModelTablePaginated.setData(oDataTablePaginated);
			if (_iLengthActive > 100) {
				oModelTablePaginated.setSizeLimit(200);
			}
			SupplementalRateTable.setModel(oModelTablePaginated);
		},

		/**
		 * Creates a sorted model based on the data loaded to the table.
		 * @function createSortedModel
		 */
		createSortedModel: function () {
			var i = 0,
				j = 0,
				z = 0,
				bMergedColumn,
				oTable = this.getView().byId("SupplementalClaimsTable_Active"),
				aItems = oTable.getItems(),
				iNColumns = aItems[i].getCells().length,
				iNRows = aItems.length,
				oSortedModel = [];
			var Text, columnName, iMergedColumn;

			for (i = 0; i < iNRows; i++) {
				var entry = {};
				iMergedColumn = 0;
				for (j = 0; j < iNColumns; j++) {
					bMergedColumn = false;
					try {
						Text = aItems[i].getCells()[j].getText();
					} catch (oEvt) {
						Text = "";
					} finally {
						if (!Text) {
							bMergedColumn = true;
							iMergedColumn = aItems[i].getCells()[j].getItems && aItems[i].getCells()[j].getItems().length;
							for (z = 0; z < iMergedColumn; z++) {
								Text = aItems[i].getCells()[j].getItems()[z].getText();
								if (Text !== "N/A") {
									columnName = aItems[i].getCells()[j].getItems()[z].getBinding("text").getPath();
									entry[columnName] = Text;
								}
							}

						}
					}

					if (!bMergedColumn) {
						if (Text !== "N/A") {
							columnName = aItems[i].getCells()[j].getBinding("text").getPath();
							entry[columnName] = Text;
						}
					}
				}
				oSortedModel.push(entry);
			}
			_aSortedModel.setData(oSortedModel);
		},

		/**
		 * Applys the selected sorters to the table
		 * @function applySorting
		 */
		applySorting: function () {
			var oTable = _oController.getView().byId("SupplementalClaimsTable_Active");
			var oBinding, sColumnID;

			_aSorters = [];

			for (var i = 0; i < _oSortedColumns.getData().length; i++) {
				var iKey = parseFloat(_oSortedColumns.getData()[i].Key, 10);
				var bIsDescending = _oSortedColumns.getData()[i].Order === "desc";
				_oSortOptions.getData().forEach(function (obj) {
					if (obj.Key === iKey.toString()) {
						sColumnID = obj.ColumnID;
					}
				});
				var oSorter = new Sorter(sColumnID, bIsDescending);

				if (_oDateColumns === iKey) {
					oSorter.fnCompare = _oController.comparatorDate;
				} else if (_oNumericColumns.indexOf(iKey) === -1) {
					oSorter.fnCompare = _oController.comparator;
				} else {
					oSorter.fnCompare = _oController.comparatorNumeric;
				}
				_aSorters.push(oSorter);
			}

			_aSortedModel.setData(JSON.parse(JSON.stringify(_oModelSupplementalClaims_Active.getData())));
			oTable.setModel(_aSortedModel);
			oBinding = oTable.getBinding("items");

			// apply the selected sort and group settings
			oBinding.sort(_aSorters);
			this.createSortedModel();
			_begin_Active = 0;
			//this.tablePagination("SupplementalClaimsTable_Active", _begin_Active);
			_oController.onLiveSearchSupplementClaims();
			_oController.getView().getModel("Sorters").updateBindings();
		},

		/**
		 * Loads to next data page
		 * @function toRight
		 */
		toRight: function (oEvent) {
			var buttonId = oEvent.getSource().getId();
			var TableId = buttonId.split("idRightNav_")[1];

			if (TableId === "SupplementalClaimsTable_Active") {
				var start = _begin_Active + 1 + _tablePaginationSize;
				var _oModelSupplementalRateTable = null;
				_oModelSupplementalRateTable = _aFinaldModel;
				var iLengthActive = _oModelSupplementalRateTable.getData().length;
				_begin_Active = _begin_Active + _tablePaginationSize;
				var end = _begin_Active + _tablePaginationSize;
				if (end > iLengthActive) {
					end = iLengthActive;
				}
				this.tablePagination(TableId, _begin_Active);

				//GTM 
				_oController.GTMDataLayer('scpaginationclick',
					'Supplemental Claims',
					'Pagination-click',
					start + '-' + end
				);
			}
		},

		/**
		 * Loads previous data page 
		 * @function toLeft
		 */
		toLeft: function (oEvent) {
			var buttonId = oEvent.getSource().getId();
			var TableId = buttonId.split("idLeftNav_")[1];

			if (TableId === "SupplementalClaimsTable_Active") {
				var start = _begin_Active + 1 - _tablePaginationSize;
				var _oModelSupplementalRateTable = null;
				_oModelSupplementalRateTable = _aFinaldModel;
				var iLengthActive = _oModelSupplementalRateTable.getData().length;
				_begin_Active = _begin_Active - _tablePaginationSize;
				var end = _begin_Active + _tablePaginationSize;
				if (end > iLengthActive) {
					end = iLengthActive;
				}
				this.tablePagination(TableId, _begin_Active);
				//GTM 
				_oController.GTMDataLayer('scpaginationclick',
					'Supplemental Claims',
					'Pagination-click',
					start + '-' + end
				);
			}
		},

		/**
		 * Configure all the models and set the "ascending" order for the 3 Tables
		 @ parameter (data) Receives data from the Ajax call
		 */
		oDataConfiguration: function (data) {
			var advancedSortButton = _oController.byId("advanced_sort");
			var exportClaimsButton = _oController.byId("exportsClaimsBtn");
			var statusSelect = _oController.getView().byId("statusFilterSelect");

			_oController.createStatusFilterModel(data);

			_isAdvancedSortActive = false;

			_oModelSupplementalClaims_Active.setData(data);
			_oModelSupplementalClaims_Active.setSizeLimit(data.length);

			_aSortedModel = _oModelSupplementalClaims_Active;
			_aFinaldModel = _aSortedModel;

			this.getView().setModel(_aFinaldModel, "oModelInfoActive");

			_begin_Active = 0;

			this.tablePagination("SupplementalClaimsTable_Active", _begin_Active);
			if (data.length === 0) {
				advancedSortButton.setVisible(false);
				exportClaimsButton.setEnabled(false);
				statusSelect.setEnabled(false);
				_oController.clearHeaderColors();
				_oController.resetSortIcons();
			} else {
				advancedSortButton.setVisible(true);
				exportClaimsButton.setEnabled(true);
				statusSelect.setEnabled(true);
				_oController.prepareSortValues();
				_oController.applySorting();
			}
		},

		/**
		 * Function that searches model for all available status and creates a model with the the results.
		 * @function createStatusFilterModel
		 * 
		 */
		createStatusFilterModel: function (oData) {
			var oStatus = [];
			_filterStatusList = [];

			_filterStatusList.push({
				"status": "All",
				"id": 0
			});

			for (var i = 0; i < oData.length; i++) {
				var status = oData[i].status;
				if (oStatus.indexOf(status) < 0) {
					oStatus.push(status);
				}
			}

			oStatus.reverse();

			for (var j = 0; j < oStatus.length; j++) {
				_filterStatusList.push({
					"status": oStatus[j],
					"id": (j + 1)
				});
			}

			this.getView().setModel(new sap.ui.model.json.JSONModel(_filterStatusList), "filterStatusList");
		},

		/**
		 * Function that handles the success case of the ajax call that loads the suplemental rates
		 * @function onLoadModelSupplementalRatesSuccess
		 */
		onLoadModelSupplementalRatesSuccess: function (oController, oData, oPiggyBack) {
			var SupplementalRateTable_Container = oController.getView().byId("SupplementalClaimsTable_Active");
			var searchFieldUserManagement = oController.byId("searchFieldSupplementalClaims");

			SupplementalRateTable_Container.setBusy(false);
			searchFieldUserManagement.setValue("");
			searchFieldUserManagement.setEnabled(true);

			var validatedClaimsAmount = {
				"validatedAmount": oData.validatedAmount
			};

			oController.getView().setModel(new sap.ui.model.json.JSONModel(validatedClaimsAmount), "validatedAmount");
			oController.getView().byId("validateAmountText").updateBindings();

			oController.oDataConfiguration(oData.claims);
		},

		/**
		 * Function that handles the error case of the ajax call that loads the suplemental rates
		 * @function onLoadModelSupplementalRatesError
		 */
		onLoadModelSupplementalRatesError: function (oController, oError, oPiggyBack) {
			var SupplementalRateTable_Container = oController.getView().byId("SupplementalClaimsTable_Active");
			var searchFieldUserManagement = oController.byId("searchFieldSupplementalClaims");
			var statusSelect = _oController.getView().byId("statusFilterSelect");

			statusSelect.setEnabled(false);
			SupplementalRateTable_Container.setVisible(false);
			SupplementalRateTable_Container.setBusy(false);
			searchFieldUserManagement.setEnabled(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"MASCLoadClaimsErrorTXT"), null, null);
			searchFieldUserManagement.setValue("");

		},

		/**
		 * Set the filter for the selected account and get cases for selected accounts.
		 * @function loadModelSupplementalClaims
		 */
		loadModelSupplementalRates: function () {
			var controller = this;
			var SupplementalRateTable_Container = this.getView().byId("SupplementalClaimsTable_Active");
			var statusSelect = _oController.getView().byId("statusFilterSelect");

			SupplementalRateTable_Container.setBusyIndicatorDelay(0);
			SupplementalRateTable_Container.setBusy(true);
			_oController.getView().byId("supplClaimResultsPerPage").setVisible(false);
			statusSelect.setSelectedKey(0);
			_createSelectionRange = true;
			_selectedActvResult = 10;

			var xsURL = "/salesforce/claims";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(_oController, true, xsURL, "GET", controller.onLoadModelSupplementalRatesSuccess, controller.onLoadModelSupplementalRatesError);
		},

		/**
		 * Comparator for date values
		 */
		comparatorDate: function (valueA, valueB) {
			var reg = /^\d*[/]\d*[/]\d*$/;
			var dateFormat;
			switch (sap.ui.getCore().getConfiguration().getLanguage()) {
			case ("en"):
				dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
					pattern: "MM/dd/YYYY"
				});
				break;
			default:
				dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
					pattern: "dd/MM/YYYY"
				});

			}

			var i, order;

			for (i = 0; i < _oSortedColumns.getData().length; i++) {
				if (parseInt(_oSortedColumns.getData()[i].Key, 10) === _oDateColumns) {
					order = _oSortedColumns.getData()[i].Order;
				}
			}

			if (valueA === undefined) {
				if (order === "asc") {
					return 1;
				} else {
					return -1;
				}
			}

			if (valueB === undefined) {
				if (order === "asc") {
					return -1;
				} else {
					return 1;
				}
			}

			var date1;
			if (valueA.match(reg)) {
				date1 = dateFormat.parse(valueA, false, false);
			} else {
				date1 = new Date(valueA);
			}
			var date2;
			if (valueB.match(reg)) {
				date2 = dateFormat.parse(valueB, false, false);
			} else {
				date2 = new Date(valueB);
			}

			if (date1 < date2) {
				return -1;
			} else if (date1 > date2) {
				return 1;
			} else {
				return 0;
			}
		},

		/**
		 * Comparator for numeric values
		 */
		comparatorNumeric: function (a, b) {
			var valueA,
				valueB;

			valueA = parseFloat(_oController.deCommaFyFloats(a));
			valueB = parseFloat(_oController.deCommaFyFloats(b));

			if (valueA < valueB) {
				return -1;
			} else if (valueA > valueB) {
				return 1;
			} else {
				return 0;
			}
		},

		/**
		 * Comparator for Text values
		 */
		comparator: function (valueA, valueB) {

			if (valueA < valueB) {
				return -1;
			} else if (valueA > valueB) {
				return 1;
			} else {
				return 0;
			}
		},

		/**
		 * Submits the request for montlhy Payment 
		 * @function onPressSubmitMonthlyPayment
		 **/
		onPressSubmitMonthlyPayment: function (oEvent) {
			var deleteClaimDialog = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog");
			//Set Warning Dialog Buttons
			sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(this.monthlyPaymentConfirmation);
			sap.ui.getCore().byId("button_Dialog_Warning_No").attachPress(this.onReturn);
			sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWyesTXT"));
			sap.ui.getCore().byId("button_Dialog_Warning_No").setText(_oController.getResourceBundle().getText("DLWnoTXT"));
			//Set Warning Dialog Message
			sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("MASCSMonthlyPayMess1TXT"));
			sap.ui.getCore().byId("text2_Dialog_Warning").setText(_oController.getResourceBundle().getText("MASCSMonthlyPayMess2TXT"));
			//Set Warnig Dialog Title
			sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("MASCSMonthlyPayBTN"));
			deleteClaimDialog.open();
			_oController._removeDialogResize(deleteClaimDialog);

			//GTM 
			_oController.GTMDataLayer('scbuttonclick',
				'Supplemental Claims',
				'Button-click',
				oEvent.getSource().getText()
			);
		},

		/**
		 * Closes the delete claim message box
		 * @function onReturn
		 */
		onReturn: function () {
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();

			//GTM 
			_oController.GTMDataLayer('popuppymntchoice',
				'Supplemental Claims',
				'Button -click',
				'Submit for Monthly Payment_popupClick - No'
			);
		},

		/**
		 * Formater to select sorting image
		 * @function formatterOrderButtonAS
		 */
		formatterOrderButtonAS: function (order) {
			if (order !== null) {
				return "resources/icon/sort_" + order + ".png";
			} else {
				return "resources/icon/sort_both.png";
			}
		},

		formatterSubmitAmountText: function (value) {
			var i18n = this.getView().getModel("i18n").getResourceBundle();
			var formatedValue = _oController.formatValuesGroupingDecimalSeparators(value);
			return i18n.getText("MASCValidatedAmountTXT") + " " + formatedValue + i18n.getText("MASPCCurrencyTBL");
		},

		// Format Keys in Advance Sorting
		formatAdvanceSortKeys: function (value) {
			if (value) {
				return value;
			}
		},

		columnNamePopupFormatter: function (oData) {
			if (oData === undefined) {
				oData = 0;
			}
			var ret;
			_oSortOptions.getData().forEach(function (obj) {
				if (obj.Key === oData) {
					ret = obj.Column;
				}
			});

			return ret;
		},

		/**
		 * Function that handles the success and error case of the ajax call to get the monthly Payment Confirmation
		 * @function onMonthlyPaymentConfirmationSuccess
		 */
		onMonthlyPaymentConfirmationSuccessError: function (oController, oData, oPiggyBack) {
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();
			dialog.setBusy(false);
			if (oData && oData.status !== undefined) {
				setTimeout(function () {
					_oController.DataLossWarning =
						new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", _oController);
					//Set Warning Buttons
					sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.onSubmitWeb2CaseDialogClose);
					sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWcloseTXT"));
					sap.ui.getCore().byId("button_Dialog_Warning_No").setVisible(false);
					//Set Warning Message
					sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWCaseSubmitedError"));
					//Set Warnig Dialog Title
					sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWCaseSubmitedErrorTitle"));
					sap.ui.getCore().byId("icon_Dialog_Warning").setSrc("sap-icon://customfont/error");
					sap.ui.getCore().byId("icon_Dialog_Warning").removeStyleClass("warningMessageDialogIcon");
					sap.ui.getCore().byId("icon_Dialog_Warning").addStyleClass("errorMessageDialogIcon");
					_oController.DataLossWarning.open();
					_oController._removeDialogResize(_oController.DataLossWarning);
				}, 0);
			} else {
				setTimeout(function () {
					_oController.DataLossWarning =
						new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", _oController);
					//Set Warning Buttons
					sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.onSubmitWeb2CaseDialogClose);
					sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWcloseTXT"));
					sap.ui.getCore().byId("button_Dialog_Warning_No").setVisible(false);
					//Set Warning Message
					sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("MASCMonthPaySucc"));
					//Set Warnig Dialog Title
					sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText(
						"DLWCaseSubmitedSuccessfullyTitle"));
					sap.ui.getCore().byId("icon_Dialog_Warning").setSrc("sap-icon://customfont/success");
					sap.ui.getCore().byId("icon_Dialog_Warning").removeStyleClass("warningMessageDialogIcon");
					sap.ui.getCore().byId("icon_Dialog_Warning").addStyleClass("successMessageDialogIcon");
					_oController.DataLossWarning.open();
					_oController._removeDialogResize(_oController.DataLossWarning);
				}, 0);
				oController.getView().byId("submitMonthlyPaymentBtn").setEnabled(false);
				_oController.loadModelSupplementalRates();
			}
		},

		onSubmitWeb2CaseDialogClose: function () {
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();
		},

		/**
		 * Submits monthly payment.
		 */
		monthlyPaymentConfirmation: function () {
			var xsURL = "/salesforce/submitClaimsPayment";
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.setBusyIndicatorDelay(0);
			dialog.setBusy(true);

			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onMonthlyPaymentConfirmationSuccessError,
				_oController.onMonthlyPaymentConfirmationSuccessError);

			//GTM 
			_oController.GTMDataLayer('popuppymntchoice',
				'Supplemental Claims',
				'Button -click',
				'Submit for Monthly Payment_popupClick - Yes'
			);
		},

		/**
		 * Function that handles the success case of the ajax call to submit Claims
		 * @function onSaveSubmitClaimSuccess
		 */
		onsubmitMonthlyPaymentAbleSuccess: function (oController, oData, oPiggyBack) {
			var enabled = false;
			if (oData === "true") {
				enabled = true;
			}
			oController.getView().byId("submitMonthlyPaymentBtn").setEnabled(enabled);
		},

		/**
		 * Function that handles the error case of the ajax call to submit Claims
		 * @function onSaveSubmitClaimError
		 */
		onsubmitMonthlyPaymentAbleError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"MASCLoadClaimsSubStatErrorTXT"), null, null);
		},

		/**
		 * Checks if user is allowed to submit monthly payment.
		 * @function submitMonthlyPaymentAble
		 */
		submitMonthlyPaymentAble: function () {
			var xsURL = "/salesforce/submitClaimsPaymentAble";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onsubmitMonthlyPaymentAbleSuccess, _oController.onsubmitMonthlyPaymentAbleError);
		},

		/**
		 * Configure page items display/hide
		 * List of Page Components to control
		 * @function displayLayoutAccess
		 */
		formatMemoNumber: function (value) {
			if (value === undefined || value === "" || value === null) {
				value = _oController.getResourceBundle().getText("UMNA");
			}
			return value;
		},

		/**
		 * Configure page items display/hide
		 * List of Page Components to control
		 * @function displayLayoutAccess
		 */
		displayLayoutAccess: function () {
			var components = [{
				"Description": "SUPPLEMENTAL_CLAIM_SUBMISSION",
				"Id": "submitMonthlyPaymentBtn"
			}];

			for (var i = 0; i < components.length; i++) {
				this.getView().byId(components[i].Id).setVisible(this.checkDASHLayoutAccess(components[i].Description));
			}
		},

		/**
		 * Refreshes data from the supplemental claims table
		 */
		onRefreshTable: function () {
			_oController.loadModelSupplementalRates();
		},

		/**
		 * Prepares sort Variables used in the advanced sort dialog
		 * @function prepareSortValues
		 */
		prepareSortValues: function () {
			var sColumn, bMergedColumn, iMergedCloumns, iKey, sColumnID;

			_oSortOptions.setData([]);
			_oSortedColumns.setData([]);

			_aSorters = [];
			_oController.isSortedActive(false);

			_oController.clearHeaderColors();
			_oController.resetSortIcons();
			iKey = 0;
			for (var i = 0; i < _oController.getView().byId("SupplementalClaimsTable_Active").getColumns().length; i++) {
				iMergedCloumns = 0;
				bMergedColumn = false;
				try {
					sColumn = _oController.getView().byId("SupplementalClaimsTable_Active").getColumns()[i].getHeader().getItems()[0].getText();
				} catch (oEvt) {
					sColumn = _oController.getView().byId("SupplementalClaimsTable_Active").getColumns()[i].getHeader().getText ? _oController.getView()
						.byId("SupplementalClaimsTable_Active").getColumns()[i].getHeader().getText() : "";
				} finally {
					if (!sColumn) {
						bMergedColumn = true;
						iMergedCloumns = _oController.getView().byId("SupplementalClaimsTable_Active").getColumns()[i].getHeader().getItems() &&
							_oController.getView().byId("SupplementalClaimsTable_Active").getColumns()[i].getHeader().getItems().length;
						if (iMergedCloumns && iMergedCloumns > 0) {
							for (var j = 0; j < iMergedCloumns; j++) {
								sColumn = _oController.getView().byId("SupplementalClaimsTable_Active").getColumns()[i].getHeader().getItems()[j].getItems()[
										0]
									.getText();
								sColumnID = _oController.getView().byId("SupplementalClaimsTable_Active").getItems()[0].getCells()[i].getItems()[j].getBinding(
									"text").getPath();
								iKey = i + j / 10;
								_oSortOptions.getData().push({
									"Key": iKey + "",
									"Column": sColumn,
									"ColumnID": sColumnID,
									"Enabled": true
								});
								if (iKey === 0 && i === 0) {
									_oSortedColumns.getData().push({
										"Key": i + "",
										"Column": sColumn,
										"Order": "asc"
									});
									_oSortOptions.getData()[i].Enabled = false;

									var oSorter = new Sorter(sColumnID, false);
									oSorter.fnCompare = _oController.comparator;
									_aSorters.push(oSorter);
								}
							}
						}
					}
				}
				if (!bMergedColumn) {
					sColumnID = _oController.getView().byId("SupplementalClaimsTable_Active").getItems()[0].getCells()[i].getBinding("text").getPath();

					_oSortOptions.getData().push({
						"Key": i + "",
						"Column": sColumn,
						"ColumnID": sColumnID,
						"Enabled": true
					});
				}

			}
			_oController.getView().byId("sort_icon_column0").setSrc("resources/icon/sort_asc.png");
			_oController.getView().setModel(_oSortedColumns, "Sorters");
			_oSortedColumnsTemp.setData(JSON.parse(JSON.stringify(_oSortedColumns.getData())));
		},

		/**
		 * Prepares and opens the advanced sort dialog
		 * @function onAdvancedSorting
		 */
		onAdvancedSorting: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();
			// IF THE DIALOG IS CLOSED BY PRESSING "ESCAPE"
			if (_oController.dialogFragment) {
				_oController.dialogFragment.destroyContent();
			}

			_oSortedColumnsTemp.setData(JSON.parse(JSON.stringify(_oSortedColumns.getData())));
			_oController.dialogFragment = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.AdvancedSorting",
				_oController);
			_oController.getView().byId("SupplementalClaimsTable_Active");
			_oController.dialogFragment.setModel(_oSortOptions, "columns");
			_oController.dialogFragment.setModel(_oSortedColumnsTemp, "sorts");
			_oController.dialogFragment.setModel(_oController.getView().getModel("i18n"), "i18n");
			_oController.dialogFragment.templateShareable = true;
			_oController.resetEnabledColumnSorts();
			_oController.dialogFragment.open();
		},

		/**
		 * Adds new row to the sorting list
		 * 
		 * @function onAddSortEntryTable
		 */
		onAddSortEntryTable: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();
			var l = _oSortOptions.getData().length;

			for (var i = 0; i < l; i++) {
				if (_oSortOptions.getData()[i].Enabled === true) {
					var sColumn = _oSortOptions.getData()[i].Column;
					var sKey = _oSortOptions.getData()[i].Key;
					_oSortOptions.getData()[i].Enabled = false;
					_oSortedColumnsTemp.getData().push({
						"Key": sKey,
						"Column": sColumn,
						"Order": "asc"
					});
					i = l;
				}
			}
			_oController.dialogFragment.getModel("sorts").updateBindings();
			_oController.dialogFragment.getModel("columns").updateBindings();
		},

		/**
		 *Removes selected rows from the advanced sorting table
		 * 
		 * @function onRemoveSortEntryTable
		 */
		onRemoveSortEntryTable: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();
			var aIndices = sap.ui.getCore().byId("tableAdvancedSorting").getSelectedIndices();

			for (var i = aIndices.length - 1; i >= 0; i--) {
				var position = aIndices[i];
				var iKey = _oSortedColumnsTemp.getData()[position].Key;
				_oSortedColumnsTemp.getData().splice(position, 1);
				_oSortOptions.getData().forEach(function (obj) {
					if (obj.Key === iKey) {
						obj.Enabled = true;
					}
				});
			}

			sap.ui.getCore().byId("tableAdvancedSorting").clearSelection();

			if (_oSortedColumnsTemp.getData().length === 0) {
				_oController.onAddSortEntryTable();
			}

			_oController.dialogFragment.getModel("sorts").updateBindings();
			_oController.dialogFragment.getModel("columns").updateBindings();
		},

		/**
		 * Moves up by one position selected rows on the advanced sorting table
		 * 
		 * @function onMoveUpSortEntryTable
		 */
		onMoveUpSortEntryTable: function () {

			// Checks Session time out
			_oController.checkSessionTimeout();

			var oTable = sap.ui.getCore().byId("tableAdvancedSorting");
			var aIndices = sap.ui.getCore().byId("tableAdvancedSorting").getSelectedIndices();
			var limitReached = false;

			oTable.clearSelection();
			for (var i = 0; i < aIndices.length; i++) {
				var position = aIndices[i];

				if (position === 0) {
					//If row is the first position it will not move
					//Activates limit Reached to avoid being switch by the next selected rows
					limitReached = true;
					oTable.addSelectionInterval(position, position);
				} else {
					if (limitReached && position === aIndices[i - 1] + 1) {
						//If first row is selected and the next row is selected too, the second row doesn't move
						oTable.addSelectionInterval(position, position);
					} else {
						//Else swaps row with the one in the above position
						limitReached = false;
						_oController.swapArrayElements(position, position - 1, _oSortedColumnsTemp.getData(), oTable);
					}
				}
			}

			_oController.dialogFragment.getModel("sorts").updateBindings();
			_oController.dialogFragment.getModel("columns").updateBindings();
		},

		/**
		 * Moves down by one position selected rows on the advanced sorting table
		 * 
		 * @function onAddSortEntryTable
		 */
		onMoveDownSortEntryTable: function () {

			// Checks Session time out
			_oController.checkSessionTimeout();

			var oTable = sap.ui.getCore().byId("tableAdvancedSorting");
			var aIndices = sap.ui.getCore().byId("tableAdvancedSorting").getSelectedIndices();
			var limitReached = false;

			oTable.clearSelection();
			for (var i = aIndices.length - 1; i >= 0; i--) {
				var position = aIndices[i];

				if (position === _oSortedColumnsTemp.getData().length - 1) {
					//If row is the last position it will not move
					//Activates limit Reached to avoid being switch by the next selected rows
					limitReached = true;
					oTable.addSelectionInterval(position, position);
				} else {
					if (limitReached && position === aIndices[i + 1] - 1) {
						//If last row is selected and the next row is selected too, the second row doesn't move
						oTable.addSelectionInterval(position, position);
					} else {
						//Else swaps row with the one in the above position
						limitReached = false;
						_oController.swapArrayElements(position, position + 1, _oSortedColumnsTemp.getData(), oTable);
					}
				}
			}

			_oController.dialogFragment.getModel("sorts").updateBindings();
			_oController.dialogFragment.getModel("columns").updateBindings();
		},

		/**
		 * Swaps te position of 2 rows provided array and apdates the selection interval of de provided table
		 * @function swapArrayElements
		 */
		swapArrayElements: function (iSelected, iTarget, aData, oTable) {
			//Switch elements
			if (iTarget < 0) {
				oTable.addSelectionInterval(iSelected, iSelected);
				return;
			}
			var oTemp = aData[iSelected];
			aData[iSelected] = aData[iTarget];
			aData[iTarget] = oTemp;
			//Switch selections
			oTable.addSelectionInterval(iTarget, iTarget);
		},

		/**
		 * Resets enabled columns when the value on the selected column in the advanced sort selection table changes
		 * @function onColumnSortChange
		 */
		onColumnSortChange: function (oEvent) {
			_oController.resetEnabledColumnSorts();
		},

		/**
		 * On sort icon press in the advanced sort table, switches the sort direction and updates model
		 * @function onSortOrderButtonPress
		 */
		onSortOrderButtonPress: function (oEvent) {

			var sModelPath = oEvent.getSource().getBinding("src").getContext().getPath();
			var currentValue = oEvent.getSource().getBinding("src").getValue();
			if (currentValue === "asc") {
				_oController.dialogFragment.getModel("sorts").setProperty(sModelPath + "/" + "Order", "desc");
			} else {
				_oController.dialogFragment.getModel("sorts").setProperty(sModelPath + "/" + "Order", "asc");
			}
			_oController.dialogFragment.getModel("sorts").updateBindings();
		},

		/**
		 * Reset enabled columns to reflect selected
		 * @function resetEnabledColumnSorts
		 */
		resetEnabledColumnSorts: function () {
			var oTable = sap.ui.getCore().byId("tableAdvancedSorting");

			oTable.clearSelection();

			_oSortOptions.getData().forEach(function (element) {
				element.Enabled = true;
			});

			_oSortedColumnsTemp.getData().forEach(function (element) {
				_oSortOptions.getData().forEach(function (obj) {
					if (obj.Key === element.Key) {
						obj.Enabled = false;
					}
				});
			});

			_oController.dialogFragment.getModel("sorts").updateBindings();
			_oController.dialogFragment.getModel("columns").updateBindings();
		},

		/**
		 * Press the Cancel button on the advanced sorting dialog
		 * Discards sort changes
		 * 
		 * @function cancelDialogAdvancedSorting
		 */
		cancelDialogAdvancedSorting: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();

			_oController.dialogFragment.close();
			_oController.dialogFragment.destroyContent();
			_oController.dialogFragment.destroy();
		},

		/**
		 * Applies the sortings when the done button is pressed on the advanced sorting dialog
		 * 
		 * @function doneDialogAdvancedSorting
		 */
		doneDialogAdvancedSorting: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();
			_oSortedColumns.setData(JSON.parse(JSON.stringify(_oSortedColumnsTemp.getData())));

			if (_oSortedColumns.getData().length === 1 && parseFloat(_oSortedColumns.getData()[0].Key, 10) === 0 && _oSortedColumns.getData()[0]
				.Order ===
				"asc") {
				_oController.isSortedActive(false);
				_oController.clearHeaderColors();
				_isAdvancedSortActive = false;
			} else {
				_oController.isSortedActive(true);
				_oController.applyHeaderColors();
				_isAdvancedSortActive = true;
			}

			_oController.resetSortIcons();
			_oController.applySorting();

			_oController.dialogFragment.close();
			_oController.dialogFragment.destroyContent();
			_oController.dialogFragment.destroy();
		},

		/**
		 * Applies or removes styling from from the advanced sorting buttons acording to 
		 * the status of the advanced sort.
		 * @function isSortedActive
		 */
		isSortedActive: function (bIsActive) {
			var advancedSortButton = this.getView().byId("advanced_sort");

			if (bIsActive) {
				advancedSortButton.addStyleClass("supplementalClaimsButtonsActive");
			} else {
				advancedSortButton.removeStyleClass("supplementalClaimsButtonsActive");
			}
		},

		onSingleSort: function (oEvent) {
			var oTable = this.getView().byId("SupplementalClaimsTable_Active"),
				aItems = oTable.getItems();
			if (aItems.length > 0) {
				var sID = oEvent.getParameter("id").match("column.*$")[0];

				var iColumnNumber = sID.substring(6),
					sOrder;

				if (oEvent.getSource().getSrc().indexOf("sort_desc.png") !== -1) {
					sOrder = "asc";
				} else {
					sOrder = "desc";
				}

				_oSingleSortData = {
					order: sOrder,
					columnN: iColumnNumber
				};

				if (_isAdvancedSortActive) {
					_oController.clearSortWarning();
				} else {
					_oController.singleSort();
				}
			}
		},

		singleSort: function () {

			var sOrder = _oSingleSortData.order,
				iColumnNumber = _oSingleSortData.columnN;
			var sColumnName;
			_oSortedColumns.setData([]);
			_isAdvancedSortActive = false;
			_oController.isSortedActive(false);
			_oController.resetSortIcons();
			_oController.getView().byId("sort_icon_column" + iColumnNumber).setSrc("resources/icon/sort_" + sOrder + ".png");

			if (sOrder === "both") {
				_oController.resetAdvancedSort();
			} else {
				_oSortOptions.getData().forEach(function (obj) {
					if (obj.Key === iColumnNumber) {
						sColumnName = obj.Column;
					}
				});
				_oSortedColumns.getData().push({
					"Key": iColumnNumber + "",
					"Column": sColumnName,
					"Order": sOrder
				});

				_oController.clearHeaderColors();
				_oController.applySorting();
			}
		},

		resetAdvancedSort: function () {
			_oController.isSortedActive(false);
			_oController.prepareSortValues();
			_oController.applySorting();
		},

		resetSortIcons: function () {
			_oController.getView().byId("sort_icon_column0").setSrc("resources/icon/sort_both.png");
			_oController.getView().byId("sort_icon_column0.1").setSrc("resources/icon/sort_both.png");
			_oController.getView().byId("sort_icon_column1").setSrc("resources/icon/sort_both.png");
			_oController.getView().byId("sort_icon_column1.1").setSrc("resources/icon/sort_both.png");
			_oController.getView().byId("sort_icon_column8").setSrc("resources/icon/sort_both.png");

		},

		applyHeaderColors: function () {
			var oTable = _oController.getView().byId("SupplementalClaimsTable_Active");
			_oController.clearHeaderColors();

			for (var i = 0; i < _oSortedColumns.getData().length; i++) {
				var iKey = parseFloat(_oSortedColumns.getData()[i].Key, 10);
				if ((iKey.toString().split(".").length > 1) || iKey === 0 || iKey === 1 || iKey === 4 || iKey === 6) {
					var j = parseInt(iKey.toString().split(".")[0]),
						k = parseInt(iKey.toString().split(".")[1]) ? parseInt(iKey.toString().split(".")[1]) : 0;
					oTable.getColumns()[j].getHeader().getItems()[k].addStyleClass("supplementalclaimsSortedHeaderColor");
				} else {
					oTable.getColumns()[iKey].getHeader().addStyleClass("supplementalclaimsSortedHeaderColor");

				}

			}
		},

		clearHeaderColors: function () {
			var oTable = _oController.getView().byId("SupplementalClaimsTable_Active"),
				iCollumns = oTable.getColumns().length,
				sColumn;

			for (var i = 0; i < iCollumns; i++) {

				sColumn = oTable.getColumns()[i].getHeader();
				if (sColumn.getItems && sColumn.getItems() && sColumn.getItems().length > 1) {
					if (sColumn.getItems()[0].hasStyleClass("supplementalclaimsSortedHeaderColor")) {
						sColumn.getItems()[0].removeStyleClass("supplementalclaimsSortedHeaderColor");
					}
					if (sColumn.getItems()[1].hasStyleClass("supplementalclaimsSortedHeaderColor")) {
						sColumn.getItems()[1].removeStyleClass("supplementalclaimsSortedHeaderColor");
					}
				}
				if (sColumn.hasStyleClass("supplementalclaimsSortedHeaderColor")) {
					sColumn.removeStyleClass("supplementalclaimsSortedHeaderColor");
				}

			}
		},

		clearSortWarning: function () {
			setTimeout(function (sOrderPar, iColumnNumberPar) {
				_oController.DataLossWarning =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", _oController);
				//Set Warning Buttons
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.onSingleSortDialog_Accept);
				sap.ui.getCore().byId("button_Dialog_Warning_No").attachPress(_oController.onSingleSortDialog_Close);
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWyesTXT"));
				sap.ui.getCore().byId("button_Dialog_Warning_No").setText(_oController.getResourceBundle().getText("DLWnoTXT"));
				//Set Warning Message
				sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWAdvancedSortTitle"));
				sap.ui.getCore().byId("text2_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWAdvancedSortDescription"));
				//Set Warnig Dialog Title
				sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWMainTitle"));
				_oController.DataLossWarning.open();
				_oController._removeDialogResize(_oController.DataLossWarning);
			}, 0);
		},

		/**
		 * On Close Dialog Warning Messages
		 */
		onSingleSortDialog_Accept: function () {
			if (_oController.DataLossWarning) {
				_oController.DataLossWarning.close();
				_oController.DataLossWarning.destroyContent();
				_oController.DataLossWarning.destroy();
			}

			_oController.singleSort();

		},

		/**
		 * On Close Dialog Warning Messages
		 */
		onSingleSortDialog_Close: function () {
			if (_oController.DataLossWarning) {
				_oController.DataLossWarning.close();
				_oController.DataLossWarning.destroyContent();
				_oController.DataLossWarning.destroy();
			}
		},

		backToRates: function () {
			//GTM 
			_oController.GTMDataLayer('scbuttonclick',
				'Supplemental Claims',
				'Button-click',
				_oController.getResourceBundle().getText("MASCVTextBRate")
			);

			_oRouter.navTo("myAccountSupplementalRate");
		},

		/**
		 * Press the Cancel button on the advanced sorting dialog
		 * Discards sort changes
		 * 
		 * @function cancelDialogAdvancedSorting
		 */
		onPressClaimsExportBtn: function () {
			//Ipad resquesting desktop sites is identified as macOS
			if ((sap.ui.Device.os.ios || sap.ui.Device.os.macintosh) && (sap.ui.Device.system.phone || sap.ui.Device.system.tablet)) {
				_oController.onClaimsExport("csv");
			} else {
				// IF THE DIALOG IS CLOSED BY PRESSING "ESCAPE"
				if (_oController.oExportDialog) {
					_oController.oExportDialog.destroy();
				}
				_oController.oExportDialog =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.ExportDialog", this);
				_oController._createExportTypeModel(_oController.oExportDialog);
				_oController.oExportDialog.setModel(_oController.getView().getModel("i18n"), "i18n");
				// Set Header
				sap.ui.getCore().byId("exportDialogHeader").setText(_oController.getResourceBundle().getText("MASCExportClaimHeaderText"));
				// Attach functions
				sap.ui.getCore().byId("export_ExportDialog").attachPress(_oController.onExportClaimBtn);
				sap.ui.getCore().byId("close_ExportDialog").attachPress(_oController.onCloseExportDialog);
				_oController.oExportDialog.open();
			}
		},

		/**
		 * Export CSV file
		 * @ (oEvent)
		 */
		onClaimsExport: function (format) {
			// Checks Session time out
			this.checkSessionTimeout();

			var today = new Date();
			var month = _oController.pad(today.getMonth() + 1, 2, "0");
			var fileName = "SupplementalClaims_" + today.getFullYear() + today.getDate() + month + today.getHours() + today.getMinutes() +
				today.getSeconds();
			var oModel = _aFinaldModel;

			if (format === "csv") {
				var i18nResourceBundle = this.getView().getModel("i18n");
				var oExport = new Export({
					exportType: new ExportTypeCSV({
						fileExtension: "csv",
						separatorChar: ","
					}),

					models: oModel,

					rows: {
						path: "/"
					},
					columns: [{
						name: i18nResourceBundle.getProperty("MASPCAccountTBL"),
						template: {
							content: "{accName}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MASPCAccountNumTBL"),
						template: {
							content: "{accountnumber}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MASPCMaterialNumberTBL"),
						template: {
							content: "{materialNumber}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MASPCeMaterialDescriptionTBL"),
						template: {
							content: "{materialNumberDesc}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MASPCSellingTBL"),
						template: {
							content: "{sellingPrice}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MASPCRateTBL"),
						template: {
							content: "{supplementalPrice}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MASPCInvoiceTBL"),
						template: {
							content: "{invoiceNumber}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MASPCInvoiceDateExport"),
						template: {
							content: "{invoiceDate}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MASPCVolumeTBL"),
						template: {
							content: "{gallonsSold}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MASPCMemoTBL"),
						template: {
							content: "{memoNumber}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MASPCPaidTBL"),
						template: {
							content: "{memoDate}"
						}
					}, {
						name: i18nResourceBundle.getProperty("MASPCStatusTBL"),
						template: {
							content: "{status}"
						}
					}]
				});

				oExport.saveFile(fileName).catch(function (oError) {

				}).then(function () {
					oExport.destroy();
				});
			} else {
				var claims = oModel.getData();
				var aXLSXFormatData = _oController._createExcelFormatData(claims);
				var wb = _oController._jsonToWorkbook(aXLSXFormatData, "SupplementalClaims_" + today.getFullYear());
				_oController._saveFileToExcel(wb, fileName);
			}

			//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
			_oController.GTMDataLayer('scbuttonclick',
				'Supplemental Claims',
				'Button -click',
				_oController.getView().byId("exportsClaimsBtn").getText()
			);

		},

		/**
		 *  Select the number of results displayed on a page
		 *  @selectResultsPerPage
		 */
		selectResultsPerPage: function (oEvent) {
			var sValue = oEvent.getSource().getText();
			_selectedActvResult = parseInt(sValue, 10);
			_createSelectionRange = false;
			_begin_Active = 0;
			_oController.tablePagination("SupplementalClaimsTable_Active", _begin_Active);

		},

		/**
		 * click of export btn
		 * 
		 * @function onExportClaimBtn
		 */
		onExportClaimBtn: function () {
			var sSelectedFormat = sap.ui.getCore().byId("exportType").getSelectedKey();
			switch (sSelectedFormat) {
			case "1":
				_oController.onClaimsExport("csv");
				break;
			case "2":
				_oController.onClaimsExport("xlxs");
				break;
			default:
				break;
			}
			_oController.oExportDialog.destroy();
		},

		/**
		 * close of export dialog
		 * 
		 * @function onCloseExportDialog
		 */
		onCloseExportDialog: function () {
			_oController.oExportDialog.destroy();
		},

		/**
		 * change of export type
		 * 
		 * @function onChangeExportType
		 */
		onChangeExportType: function (oEvent) {
			if (oEvent.getSource().getSelectedKey()) {
				_oController.oExportDialog.getModel("oExportTypes").setProperty("/0/enabledProperty", false);
				sap.ui.getCore().byId("export_ExportDialog").setEnabled(true);
			} else {
				sap.ui.getCore().byId("export_ExportDialog").setEnabled(false);
			}
		},

		/**
		 * method to format the Excel Data
		 * @function _createExcelFormatData
		 * @param {data} Raw Data
		 * @returns Formatted Data
		 */
		_createExcelFormatData: function (data) {
			var aXLSXFormatData = [];
			var oXLSXFormatHeader = {
				"accName": _oController.getResourceBundle().getText("MASPCAccountTBL"),
				"accountnumber": _oController.getResourceBundle().getText("MASPCAccountNumTBL"),
				"materialNumber": _oController.getResourceBundle().getText("MASPCMaterialNumberTBL"),
				"materialNumberDesc": _oController.getResourceBundle().getText("MASPCeMaterialDescriptionTBL"),
				"sellingPrice": _oController.getResourceBundle().getText("MASPCSellingTBL"),
				"supplementalPrice": _oController.getResourceBundle().getText("MASPCRateTBL"),
				"invoiceNumber": _oController.getResourceBundle().getText("MASPCInvoiceTBL"),
				"invoiceDate": _oController.getResourceBundle().getText("MASPCInvoiceDateExport"),
				"gallonsSold": _oController.getResourceBundle().getText("MASPCVolumeTBL"),
				"memoNumber": _oController.getResourceBundle().getText("MASPCMemoTBL"),
				"memoDate": _oController.getResourceBundle().getText("MASPCPaidTBL"),
				"status": _oController.getResourceBundle().getText("MASPCStatusTBL")
			};
			var aKeys = Object.keys(oXLSXFormatHeader);
			for (var i = 0; i < data.length; i++) {
				var oRow = {};
				for (var j = 0; j < aKeys.length; j++) {
					oRow[oXLSXFormatHeader[aKeys[j]]] = data[i][aKeys[j]];
				}
				aXLSXFormatData.push(oRow);
			}
			return aXLSXFormatData;
		}
	});
});