sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function (Base) {
	"use strict";

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Error.UnexpectedError", {
		/** @module UnexpectedError */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			this.getRouter().getRoute("unexpectedError").attachPatternMatched(this._onObjectMatched, this);
		},

		/**
		 *  Navigates to the Feed page
		 * @function onLinkPressed
		 *
		 */
		onLinkPressed: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			ga('set', 'page', '/Feed');
			ga('send', 'pageview');
			this.getRouter().navTo("feed");
		},

		_onObjectMatched: function () {
			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "other"
			});
		}
	});
});