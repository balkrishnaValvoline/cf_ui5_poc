sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function(Base) {
	"use strict";

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Error.DirectAccountInactivated", {
		/** @module UnexpectedError */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function() {
			Base.prototype.onInit.call(this);
			this.getRouter().getRoute("directAccountDeactivated").attachPatternMatched(this._onObjectMatched, this);
		},

		_onObjectMatched: function() {
			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "other"
			});

		},

		/**
		 * Hide all the menu icons
		 */
		onAfterRendering: function() {
			//disable all sidebar buttons
			$(".js-shop-sidenav").addClass("hidden");
			$(".js-productsEcom-sidenav").addClass("hidden");
			$(".js-orders-sidenav").addClass("hidden");
			$(".js-insights-sidenav").addClass("hidden");
			$(".js-learn-sidenav").addClass("hidden");
			$(".js-services-sidenav").addClass("hidden");
			$(".js-promote-sidenav").addClass("hidden");
		}
	});
});