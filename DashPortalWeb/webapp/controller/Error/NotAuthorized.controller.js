sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function(Base) {
	"use strict";

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Error.NotAuthorized", {
		/** @module NotAuthorized */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function() {
			Base.prototype.onInit.call(this);
			this.getRouter().getRoute("notAuthorized").attachPatternMatched(this._onObjectMatched, this);
		},
		_onObjectMatched: function() {
			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "other"
			});
		},

		/**
		 * Runs after the page render
		 */
		onAfterRendering: function() {
			$('#loading').remove();
		}
	});

});