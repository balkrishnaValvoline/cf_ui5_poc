sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function (Base) {
	"use strict";

	var _oRouter;
	var _oController;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Error.UnableToImpersonate", {
		/** @module UnableToImpersonate */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			_oRouter.getRoute("unableToImpersonate").attachPatternMatched(this._onObjectMatched, this);
			_oController = this;
		},

		_onObjectMatched: function () {
			var errorModel = sap.ui.getCore().getModel("oModelImpersonateError");
			if (errorModel !== null) {
				_oController.getView().setModel(errorModel, "oError");
			}
			var oMainPage = sap.ui.getCore().byId("__xmlview0--vbox_background_main");
			oMainPage.setVisible(false);
			var oMainPageMobile = sap.ui.getCore().byId("__xmlview0--menuMobileHeader");
			oMainPageMobile.setVisible(false);
		}
	});

});