sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function (Base) {
	"use strict";

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Error.NotFound", {
		/** @module NotFound */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			this.getView().setModel(sap.ui.getCore().getModel("oModelImpersonate"), "impersonateModel");
		},

		/**
		 * Sets the logOut Button Visible or not depending on the impersonation status.
		 * @function checkImpersonate
		 * @param {string} impersonateStatus - The status of impersonation
		 */
		checkImpersonateStatus: function (impersonateStatus) {
			if (impersonateStatus) {
				return false;
			}
			return true;
		},
		/**
		 * Navigates to the Feed page
		 * @function onLinkPressed
		 *
		 */
		onLinkPressed: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			ga('set', 'page', '/Feed');
			ga('send', 'pageview');
			this.getRouter().navTo("feed");
		}
	});

});