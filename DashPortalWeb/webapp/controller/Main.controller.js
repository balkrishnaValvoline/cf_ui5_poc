sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/Icon",
	"sap/ui/core/format/DateFormat"
], function (Base, JSONModel, Icon, DateFormat) {
	"use strict";
	var _oEventBus,
		_oRouter,
		_oController,
		_sidebarMenuButtonsID = [],
		_isSessionTimeOut = false;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Main", {
		/**
		 * This function initializes the controller and configues the sidebar menu
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oEventBus = sap.ui.getCore().getEventBus();
			_oRouter = this.getRouter();
			_oController = this;
			_oController.idleTimeSetup();
			sap.ui.Device.resize.attachHandler(function (oEvent) {
				//Sets the visibility for the button when the user resizes the screen (Checks if the scroll is present)
				var oScrollElemnt = $("#" + _oController.byId("vbox_background_main").getId());
				var oSideNavButton = _oController.getView().byId("sideNavScrollButton");
				if (oScrollElemnt && oScrollElemnt.length) {
					oSideNavButton.setVisible(!(oScrollElemnt.height() === oScrollElemnt[0].scrollHeight) || false);
				}
			});
			_oController.getView().addEventDelegate({
				onAfterRendering: function () {
					//Sets the visibility for the button when the page is loaded (Checks if the scroll is present)
					var oScrollElemnt = $("#" + _oController.byId("vbox_background_main").getId());
					var oSideNavButton = _oController.getView().byId("sideNavScrollButton");
					oSideNavButton.setVisible((oScrollElemnt[0] && !(oScrollElemnt.height() === oScrollElemnt[0].scrollHeight)) || false);
					//Scroll to the top/bottom of the side navigation bar logic
					$("#" + _oController.byId("vbox_background_main").getId()).scroll(function () {
						//Sets the visibility for the button when the user scrolls(Checks if the scroll is present)
						oSideNavButton.setVisible(!(oScrollElemnt.height() === oScrollElemnt[0].scrollHeight) || false);
						if (oScrollElemnt.scrollTop() + oScrollElemnt.height() - oScrollElemnt[0].scrollHeight > -1) {
							_oController.getView().byId("sideNavScrollButton").setSrc("sap-icon://slim-arrow-up");
						} else {
							_oController.getView().byId("sideNavScrollButton").setSrc("sap-icon://slim-arrow-down");
						}
					});
				}
			}, _oController);

			// First parameter is font name, second parameter is collection name, third parameter is font-family and the last parameter is the code point in Unicode
			sap.ui.core.IconPool.addIcon("user", "customfont", "valvoline-icons", "e911");
			sap.ui.core.IconPool.addIcon("help-line", "customfont", "valvoline-icons", "e927");
			sap.ui.core.IconPool.addIcon("logout", "customfont", "valvoline-icons", "e910");
			sap.ui.core.IconPool.addIcon("cart", "customfont", "valvoline-icons", "e912");
			sap.ui.core.IconPool.addIcon("my-account", "customfont", "valvoline-icons", "e902");
			sap.ui.core.IconPool.addIcon("learn", "customfont", "valvoline-icons", "e929");
			sap.ui.core.IconPool.addIcon("insights", "customfont", "valvoline-icons", "e900");
			sap.ui.core.IconPool.addIcon("promote", "customfont", "valvoline-icons", "e907");
			sap.ui.core.IconPool.addIcon("service", "customfont", "valvoline-icons", "e908");
			sap.ui.core.IconPool.addIcon("oil-can", "customfont", "valvoline-icons", "e903");
			sap.ui.core.IconPool.addIcon("orders", "customfont", "valvoline-icons", "e926");

			sap.ui.core.IconPool.addIcon("light-truck", "customfont", "valvoline-icons", "e939");
			sap.ui.core.IconPool.addIcon("passenger-car", "customfont", "valvoline-icons", "e940");
			sap.ui.core.IconPool.addIcon("motorcycles", "customfont", "valvoline-icons", "e941");
			sap.ui.core.IconPool.addIcon("leisure-marine", "customfont", "valvoline-icons", "e942");
			sap.ui.core.IconPool.addIcon("tractor", "customfont", "valvoline-icons", "e943");
			sap.ui.core.IconPool.addIcon("heavy-truck", "customfont", "valvoline-icons", "e944");
			sap.ui.core.IconPool.addIcon("classic-car", "customfont", "valvoline-icons", "e945");
			sap.ui.core.IconPool.addIcon("agriculture", "customfont", "valvoline-icons", "e946");
			sap.ui.core.IconPool.addIcon("arrow-up", "customfont", "valvoline-icons", "e948");
			sap.ui.core.IconPool.addIcon("arrow-down", "customfont", "valvoline-icons", "e947");
			sap.ui.core.IconPool.addIcon("success", "customfont", "valvoline-icons", "e950");
			sap.ui.core.IconPool.addIcon("error", "customfont", "valvoline-icons", "e961");
			sap.ui.core.IconPool.addIcon("alert", "customfont", "valvoline-icons", "e934");
			sap.ui.core.IconPool.addIcon("close", "customfont", "valvoline-icons", "e914");
			sap.ui.core.IconPool.addIcon("ecomQuickOrderIcon", "customfont", "valvoline-icons", "e953");

			sap.ui.core.IconPool.addIcon("checked-outline", "customfont", "valvoline-icons", "e960");
			sap.ui.core.IconPool.addIcon("chrono", "customfont", "valvoline-icons", "e959");
			sap.ui.core.IconPool.addIcon("delivery-truck", "customfont", "valvoline-icons", "e958");
			sap.ui.core.IconPool.addIcon("back-to-top", "customfont", "valvoline-icons", "e928");
			sap.ui.core.IconPool.addIcon("edit", "customfont", "valvoline-icons", "e925");
			sap.ui.core.IconPool.addIcon("shopping-cart-add-2", "customfont", "valvoline-icons", "e962");

			// CLose mobile menu when after clicking a link
			$('body').on('click', '.js-close-mobile-menu', function () {
				$('.menuMobile').addClass('hidden');
			});

			_oEventBus.subscribe("routerNav", "casesNav", this.routerFeedNav, this);
			_oEventBus.subscribe("routerNav", "ordersNav", this.routerFeedNav, this);
			_oEventBus.subscribe("routerNav", "otherNav", this.routerFeedNav, this);
			_oEventBus.subscribe("routeMatched", "routeMatched", this.sidebarSelect, this);

			// add the application version and Date for show in footer
			var applicationVersion = this.getOwnerComponent().getManifestEntry("sap.app").applicationVersion.version;
			var dateFormat = DateFormat.getDateInstance({
				pattern: "yyyy"
			});
			var currentYear = dateFormat.format(new Date());

			var versionModel = new JSONModel();
			versionModel.setProperty("/version", applicationVersion);
			versionModel.setProperty("/year",
				currentYear);
			this.getView().setModel(versionModel, "appConfigModel");

			//allows both sidebar menu button's label to be highlighted on mouseover, also
			_sidebarMenuButtonsID = ["productsEcom", "products", "shop", "insights", "learn", "services", "promote", "orders"];

			// Runs cookies
			this.getCookies();
			this.onChangeLanguage();
		},

		/**
		 * Runs after the page render
		 */
		onAfterRendering: function () {
			this.displayLayoutAccess();

			// Stop Loading screen
			//GTMDataLayer - event, eventCategory, eventAction, eventLabel
			_oController.GTMDataLayer("3dotspgelodng", "3DotsPage Loading time", "End", window.document.URL);
			var secBetweenStartEnd = Math.floor((new Date().getTime() - window.loadingTracking.startTime) / 1000);
			_oController.GTMDataLayer("3dotstimecalc", "3DotsPage Loading time", secBetweenStartEnd + " - Seconds", window.loadingTracking.startURL +
				" | " + window.document.URL);
			$('#loading').remove();

			// Make sidenav elements clickable
			$("#" + this.createId("shop_button")).on("click", this.toShop);
			$("#" + this.createId("products_button")).on("click", this.toProducts);
			$("#" + this.createId("orders_button")).on("click", this.toOrders);
			$("#" + this.createId("insights_button")).on("click", this.toInsights);
			$("#" + this.createId("learn_button")).on("click", this.toLearn);
			$("#" + this.createId("services_button")).on("click", this.toServices);
			$("#" + this.createId("promote_button")).on("click", this.toPromote);

			//set the cart cookie 
			this.getShoppingCartCookie(this);
		},

		/**
		 * Configure sidebar icons to display/hide
		 * @function displayLayoutAccess
		 */
		displayLayoutAccess: function () {
			this.hideAllIcons();
			if (window.localStorage.getItem("impersonateError") === "true") {
				return;
			}
			if (!this.checkDASHPageAuthorization("MAIN_MENU_PAGE")) {
				return;
			}

			var mHybrisParameters;
			mHybrisParameters = sap.ui.getCore().getModel("HybrisParameters");
			if (mHybrisParameters === undefined) {
				mHybrisParameters = new sap.ui.model.json.JSONModel();
				mHybrisParameters.setData(this.getGlobalParametersByGroup("HybrisParameters"));
				sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
			}
			// SHOP ICON
			var viewDivId;
			if (!this.checkDASHLayoutAccess("SHOP_ICON")) {
				this.getView().byId("mobileShop").setVisible(false);
				if (this.getView().byId("mobileShopTopBarContainer")) {
					this.getView().byId("mobileShopTopBarContainer").setVisible(false);
				}
			} else {
				//Added Validation to check if hybris should be displayed on mobile
				//If Mobile on hybris is disabled Shop icon/navigation should not be visible
				this.getView().byId("mobileShop").setVisible((mHybrisParameters.getData().showShop === 'true'));
				viewDivId = this.getView().createId("vbox_shop_main");
				$("#" + viewDivId).removeClass("hidden");
				if (this.getView().byId("mobileShopTopBarContainer")) {
					this.getView().byId("mobileShopTopBarContainer").setVisible((mHybrisParameters.getData().showShop === 'true'));
				}
			}
			// PRODUCTS ICON
			if (this.checkDASHLayoutAccess("PRODUCTS_ICON")) {
				viewDivId = this.getView().createId("vbox_products_main");
				$("#" + viewDivId).removeClass("hidden");
				this.getView().byId("mobileProducts").setVisible(true);
			} else {
				this.getView().byId("mobileProducts").setVisible(false);
			}
			// ORDERS ICON
			if (this.checkDASHLayoutAccess("ORDERS_ICON")) {
				viewDivId = this.getView().createId("vbox_orders_main");
				$("#" + viewDivId).removeClass("hidden");
				this.getView().byId("mobileOrders").setVisible(true);
			} else {
				this.getView().byId("mobileOrders").setVisible(false);
			}
			// INSIGHTS ICON
			if (this.checkDASHLayoutAccess("INSIGHTS_ICON")) {
				viewDivId = this.getView().createId("vbox_insights_main");
				$("#" + viewDivId).removeClass("hidden");
				this.getView().byId("mobileInsights").setVisible(true);
			} else {
				this.getView().byId("mobileInsights").setVisible(false);
			}
			// LEARN ICON
			if (this.checkDASHLayoutAccess("LEARN_ICON")) {
				viewDivId = this.getView().createId("vbox_learn_main");
				$("#" + viewDivId).removeClass("hidden");
				this.getView().byId("mobileLearn").setVisible(true);
			} else {
				this.getView().byId("mobileLearn").setVisible(false);
			}
			// SOLUTIONS ICON
			if (this.checkDASHLayoutAccess("SOLUTIONS_ICON")) {
				viewDivId = this.getView().createId("vbox_services_main");
				$("#" + viewDivId).removeClass("hidden");
				this.getView().byId("mobileServices").setVisible(true);
			} else {
				this.getView().byId("mobileServices").setVisible(false);
			}
			// PROMOTE ICON
			if (this.checkDASHLayoutAccess("PROMOTE_ICON")) {
				viewDivId = this.getView().createId("vbox_promote_main");
				$("#" + viewDivId).removeClass("hidden");
				this.getView().byId("mobilePromote").setVisible(true);
			} else {
				this.getView().byId("mobilePromote").setVisible(false);
			}
		},

		/**
		 * Removes the active class modifier on the sidenav active item.
		 * @function resetButtons
		 */
		resetButtons: function () {
			var viewDivId;
			for (var i = 0; i < _sidebarMenuButtonsID.length; i++) {
				viewDivId = this.getView().createId("vbox_" + _sidebarMenuButtonsID[i] + "_main");
				$("#" + viewDivId).removeClass("sidenav__item--active");
			}
		},

		/**
		 *  Navigates to Feed page
		 * @function toFeed
		 */
		toFeed: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();

			if (window.localStorage.getItem("impersonateError") === "true") {
				return;
			}

			//GTM
			_oController.GTMDataLayer('logoclick',
				'Logo',
				'Header',
				"#/" + _oRouter.getRoute("feed").getURL()
			);

			//stop videos on changing page
			_oController.stopVideo();

			ga('set', 'page', '/Feed');
			ga('send', 'pageview');

			// Reset All buttons
			this.resetButtons();

			// If the user press the feed button and if its already in the page, it reloads the page
			var lastPage = this.getRouter()._oRouter._prevRoutes[0].route._pattern;
			if (lastPage === "feed") {
				_oRouter.navTo("home");
				_oRouter.navTo("feed");
			}

			_oRouter.navTo("feed");
			_oEventBus.publish("resetSelect", "resetSelect");
		},

		/**
		 * Navigates to Shop page
		 * @function toShop
		 */
		toShop: function () {
			var hybrisLinks = _oController.getGlobalParametersByGroup("HybrisParameters");
			//stop videos on changing page
			_oController.stopVideo();

			//Force the closing of the mobile menu when navigating to hybris
			var oMobileBar = _oController.getView().byId("menuMobileBar");
			if (oMobileBar.getVisible()) {
				_oController.showMobileMenu();
			}

			//GTM
			_oController.GTMDataLayer('leftnavclick',
				'Left Navigation',
				'Shop-click',
				hybrisLinks["Hybris"]
			);

			ga('set', 'page', '/HybrisHome');
			ga('send', 'pageview');

			_oController.nav2Hybris(hybrisLinks.Hybris);
		},

		/**
		 * Navigates to Products page.
		 * @function toProducts
		 */
		toProducts: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();

			//GMT
			_oController.GTMDataLayer('leftnavclick',
				'Left Navigation',
				'Products-click',
				"#/" + _oRouter.getRoute("products").getURL()
			);

			//stop videos on changing page
			_oController.stopVideo();

			ga('set', 'page', '/Products');
			ga('send', 'pageview');

			_oRouter.navTo("products");
		},

		/**
		 * Navigates to Insights page.
		 * @function toInsinghts
		 */
		toInsights: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();

			//stop videos on changing page
			_oController.stopVideo();

			ga('set', 'page', '/Insights');
			ga('send', 'pageview');

			_oRouter.navTo("mainInsights");
		},

		/**
		 * Navigates to Learn page.
		 * @function toLearn
		 */
		toLearn: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();

			//GTM
			_oController.GTMDataLayer('leftnavclick',
				'Left Navigation',
				'Valvoline University-click',
				"#/" + _oRouter.getRoute("learn").getURL()
			);

			//stop videos on changing page
			_oController.stopVideo();

			ga('set', 'page', '/Learn');
			ga('send', 'pageview');

			_oRouter.navTo("learn");
		},

		/**
		 * Navigates to Services page.
		 * @function toServices
		 */
		toServices: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();

			//GTM
			_oController.GTMDataLayer('leftnavclick',
				'Left Navigation',
				'Solutions-click',
				"#/" + _oRouter.getRoute("service").getURL()
			);

			//stop videos on changing page
			_oController.stopVideo();

			ga('set', 'page', '/Solutions');
			ga('send', 'pageview');

			_oRouter.navTo("service");
		},

		/**
		 * Navigates to Promote page.
		 * @function toPromote
		 */
		toPromote: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();

			//GTM
			_oController.GTMDataLayer('leftnavclick',
				'Left Navigation',
				'Promote-click',
				"#/" + _oRouter.getRoute("promote").getURL()
			);

			//stop videos on changing page
			_oController.stopVideo();

			ga('set', 'page', '/Promote');
			ga('send', 'pageview');

			_oRouter.navTo("promote");
		},

		/**
		 * Navigation to myAccountProfile page.
		 * @function toMessages
		 */
		toMessages: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			ga('set', 'page', '/MyAccountProfile');
			ga('send', 'pageview');

			_oRouter.navTo("myAccount");
		},

		/**
		 * Navigates to myAccountCases page.
		 * @function toCases
		 */
		toCases: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			//stop videos on changing page
			_oController.stopVideo();

			_oEventBus.publish("resetSelect", "resetSelect");

			ga('set', 'page', '/MyAccountCases');
			ga('send', 'pageview');

			_oRouter.navTo("myAccountCases", {
				caseId: "0"
			});
		},

		/**
		 * Navigates to Orders page.
		 * @function toOrders
		 */
		toOrders: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();

			//stop videos on changing page
			_oController.stopVideo();

			_oEventBus.publish("resetSelect", "resetSelect");

			if (_oController.onGetDashDevice() === "NARROW") {
				//GTM
				_oController.GTMDataLayer('leftnavclick',
					'Left Navigation',
					'OrdersMobile-click',
					"#/" + _oRouter.getRoute("orderStatus").getURL()
				);

				ga('set', 'page', '/MyAccountOrderStatus');
				ga('send', 'pageview');
				_oRouter.navTo("orderStatus");
			} else {
				//GTM
				_oController.GTMDataLayer('leftnavclick',
					'Left Navigation',
					'Orders-click',
					"#/" + _oRouter.getRoute("orders").getURL()
				);

				ga('set', 'page', '/MyAccountOrders');
				ga('send', 'pageview');
				_oRouter.navTo("orders");
			}

		},

		/**
		 * Router navigation.
		 * @function routerFeedNav
		 * @param {string} event - type of navigation ("casesNav", "ordersNav" or "otherNav")
		 * @param {array}  data - data to be passed on the url of the navigation
		 */
		routerFeedNav: function (channel, event, data) {
			// Checks Session time out
			this.checkSessionTimeout();

			switch (event) {
			case "casesNav":
				ga('set', 'page', '/MyAccountCases');
				ga('send', 'pageview');
				this.getRouter().navTo("myAccountCases", {
					caseId: data.caseId
				});
				break;
			case "ordersNav":
				// Google Analytics
				ga('set', 'page', '/MyAccountOrdersDetails');
				ga('send', 'pageview');

				//
				// if orderUUID is zero, then route order status page
				//
				if (data.orderUUID === "0") {
					this.getRouter().navTo("orderStatus");
				} else {
					//Complete the needed URL using getURL function of Router, and align this to the Routing that is initialized on manifest.json
					var sUrl = this.getRouter().getURL("orderDetails", {
						orderUUID: data.orderUUID,
						language: sap.ui.getCore().getConfiguration().getLanguage()
					});

					//Execute a window.open to create a new window, Add a # before the URL to use the Mother Url
					window.open("#" + sUrl);
				}
				break;
			case "otherNav":
				if (this.getRouter().getRoute(data.url)) {
					this.getRouter().navTo(data.url);
				} else {
					window.open(data.url, "_self");
				}
				break;
			default:
				break;
			}
		},

		/**
		 * Adds the correct style to the selected button
		 * @function sidebarSelect
		 * @param {array}  data - data to be passed on the url of the navigation
		 */
		sidebarSelect: function (channel, event, data) {
			var navigatedPage = data.page;
			switch (navigatedPage) {
			case "feed":
				this.resetButtons();
				this.getView().byId("menuMobileBar").setVisible(false);
				this.getView().byId("menuMobileButton").setIcon("sap-icon://menu2");
				// Not active
				break;
			case "shop":
				this.resetButtons();
				// Not active
				break;
			case "products":
				this.resetButtons();
				$(".js-products-sidenav").addClass("sidenav__item--active");
				break;
			case "orders":
				this.resetButtons();
				$(".js-orders-sidenav").addClass("sidenav__item--active");
				break;
			case "insights":
				this.resetButtons();
				$(".js-insights-sidenav").addClass("sidenav__item--active");
				break;
			case "learn":
				this.resetButtons();
				$(".js-learn-sidenav").addClass("sidenav__item--active");
				break;
			case "solutions":
				this.resetButtons();
				$(".js-services-sidenav").addClass("sidenav__item--active");
				break;
			case "promote":
				this.resetButtons();
				$(".js-promote-sidenav").addClass("sidenav__item--active");
				break;
			default:
				// Reset All buttons
				this.resetButtons();
			}
		},

		/**
		 * This function scrolls to the top/bottom of the sideNav based on the current scroll position.
		 * @function onSideNavScrollUp
		 */
		onSideNavScrollUp: function () {
			var oScrollElemnt = $("#" + _oController.byId("vbox_background_main").getId());
			if (oScrollElemnt.scrollTop() + oScrollElemnt.height() - oScrollElemnt[0].scrollHeight > -1) {
				oScrollElemnt.animate({
					scrollTop: 0
				}, 1000);
			} else {
				oScrollElemnt.animate({
					scrollTop: oScrollElemnt[0].scrollHeight
				}, 1000);
			}
		},

		/**
		 * Handle time increments, service pings and trigger timeout
		 * @function timerIncrement
		 **/
		timerIncrement: function () {
			//increments the user idle time
			window.nIdleTime = window.nIdleTime + 60000;
			//increments the service call idle time
			window.nServiceIdleTime = window.nServiceIdleTime + 60000;
			if (window.nIdleTime >= 3600000) { // 60+ minute
				if (!_isSessionTimeOut) {
					sap.m.MessageBox.show(
						_oController.getResourceBundle().getText("STIdleMessageDialog"), {
							icon: sap.m.MessageBox.Icon.INFORMATION,
							title: "Information",
							actions: [sap.m.MessageBox.Action.CLOSE],
							onClose: function () {
								sap.m.URLHelper.redirect("/Portal/do/logout", false);
							}
						}
					);
					_isSessionTimeOut = true;
				}
			} else if (window.nServiceIdleTime >= 900000) { //15+min, time will reset when the checkSessionTimeout service is called
				_oController.checkSessionTimeout();
			} else {
				//defensive programming: no default action needed
			}
		},
		/**
		 * Setup for timeout validation
		 * @function idleTimeSetup
		 **/
		idleTimeSetup: function () {
			//Initialize idle time values
			window.nIdleTime = 0;
			window.nServiceIdleTime = 0;
			$(document).ready(function () {
				//Increment the idle time counter every minute.
				setInterval(_oController.timerIncrement, 60000); // 1 minute
				//Zero the idle timer on mouse click.
				$(this).click(function (e) {
					//Reset the idle time
					window.nIdleTime = 0;
					//Check if time should be synchronized with Hybris
					var mHybrisParameters = sap.ui.getCore().getModel("HybrisParameters");
					if (mHybrisParameters === undefined) {
						mHybrisParameters = new sap.ui.model.json.JSONModel();
						mHybrisParameters.setData(this.getGlobalParametersByGroup("HybrisParameters"));
						sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
					}
					var oHybrisParameters = mHybrisParameters.getData();
					if (oHybrisParameters.syncIdleTime === "true") {
						_oController.sendIdleTime();
					}
				});
			});
		},
		/**
		 * Sync the usr idle time with hybris
		 * @function sendIdleTime
		 **/
		sendIdleTime: function () {
			var iFrame = $('#Hybris');
			if (iFrame.is(":visible")) {
				var oMessage = {
					"idleTime": window.nIdleTime
				};
				var oHybrisParameters = sap.ui.getCore().getModel("HybrisParameters").getData();
				iFrame.get(0).contentWindow.postMessage(oMessage, oHybrisParameters.Hybris);
			}
		}
	});
});