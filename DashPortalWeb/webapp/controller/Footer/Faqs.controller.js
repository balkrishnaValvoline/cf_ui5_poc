sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel"
], function (Base, JSONModel) {
	"use strict";
	var _oRouter;
	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Footer.Faqs", {
		/** @module Faqs */
		/**
		 * This function initializes the controller and load the FAQ's model.
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oRouter = this.getRouter();

			var oModelFaqs = new JSONModel();
			oModelFaqs.loadData("model/faqsContent.json");
			this.getView().setModel(oModelFaqs);

			_oRouter.getRoute("faqs").attachPatternMatched(this._onObjectMatched, this);

			//get user info and direct account
			this.getUserInformation(this);

			this.getView().byId("FAQFooterD").addStyleClass("underline");
		},

		/**
		 * Its called everytime you enter the page
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Reset Side Nave menu buttons
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "other"
			});

			// Page Layout configuration
			if (!this.checkDASHPageAuthorization("COMMON_PAGE")) {
				return;
			}
			this.headerIconAccess();

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			// Get the User Accounts
			if (!this.getAccounts(this)) {
				return;
			}

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			$(".panelsToClose").each(function () {
				if (sap.ui.getCore().byId(this.id).getExpanded() === true) {
					sap.ui.getCore().byId(this.id).setExpanded(false);
				}
			});
		},
		/**
		 * Returns the text defined by the parameter.
		 * @function i18nFormatter
		 * @param {string} i18nFormatter - i18n variable
		 * @returns i18n Text
		 */
		i18nFormatter: function (sParam) {
			if (sParam) {
				var i18n = this.getResourceBundle();
				return i18n.getText(sParam);
			} else {
				return "";
			}
		},
		/**
		 * Navigates to 'Contact Us' page.
		 * @function toLinkContactAnswer2
		 */
		toLinkContactAnswer2: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			ga('set', 'page', '/Contacts');
			ga('send', 'pageview');
			this.getRouter().navTo("contacts");
		},

		/**
		 * Navigates to 'Other Sites' page.
		 * @function toLinkContactAnswer3
		 */
		toLinkContactAnswer3: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			ga('set', 'page', '/OtherSites');
			ga('send', 'pageview');
			this.getRouter().navTo("otherSites");
		},
		/**
		 * Navigates to 'About Valvoline Dash' page.
		 * @function toLinkContactAnswer4
		 */
		toLinkContactAnswer4: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			ga('set', 'page', '/AboutValvolineDash');
			ga('send', 'pageview');
			this.getRouter().navTo("aboutValvolineDash");
		},
		/**
		 * Navigates to 'www.ValvolineInstaller.com' page.
		 * @function toLinkContactAnswer5
		 */
		toLinkContactAnswer5: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			ga('set', 'page', '/AboutValvolineDash');
			ga('send', 'pageview');
			window.open("https://valvolinecommunity.force.com/installers/VISiteHome3", "_blank");
		}
	});
});