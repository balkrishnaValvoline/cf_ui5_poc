sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function(Base) {
	"use strict";

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Footer.PrivacyPolicy", {
		/** @module PrivacyPolicy */
		/**
		 * This function initializes the controller and underlines the Privacy Policy link
		 */
		onInit: function() {
			Base.prototype.onInit.call(this);

			/**
			 * Convenience method for getting the view model by name.
			 * Convenience method for getting for items from the view .
			 * Method {addStyleClass} to add a class
			 */
			this.getView().byId("PPFooter").addStyleClass("underline");
			this.headerIconAccess();
			this.getRouter().getRoute("privacyPolicy").attachPatternMatched(this._onObjectMatched, this);
			jQuery.sap.addUrlWhitelist("https" ,"adssettings.google.com");
			jQuery.sap.addUrlWhitelist("https" ,"tools.google.com");
			jQuery.sap.addUrlWhitelist("http" ,"www.networkadvertising.org");
			jQuery.sap.addUrlWhitelist("http" ,"dtmc.vioc.com");

		},
		_onObjectMatched: function() {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page Authorization and layout
			if (!this.checkDASHPageAuthorization("COMMON_PAGE")) {
				return;
			}

			//get user info and direct account
			this.getUserInformation(this);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "other"
			});
		},

		/**
		 * Returns the formatted text for the informations gathered by valvoline section.
		 * @function formatterPrivacyInformation
		 */
		formatterPrivacyInformation: function(text1, text2) {
			var formattedHTML;
			formattedHTML = '<span class="textJustify bodyCopy otherMarginText underline">' + text1 +
				'</span><span class="textJustify bodyCopy otherMarginText">' + ' ' + text2 + '</span>';
			return formattedHTML;
		},

		/**
		 * Returns the formatted text for the informations gathered by valvoline section.
		 * @function formatterPrivacyInformation
		 */
		formatterPrivacyInformationChoices: function(text1, link1Text, text2, text3, link2Text, text4, text5, link3Text) {
			var formattedHTML;
			var link1 =
				"https://adssettings.google.com/anonymous?hl=en&sig=ACi0TCihNZ5L-d7hxEVF44eCtQX6yappafrappdLui68hBjk72mI0XuHYXM7ezvRoYLz0-aq_65jp4QoRE9fyxBTW0q8V-TFppJy5u9q8zdpyhkAOWAFg7A";
			var link11 = "https://tools.google.com/dlpage/gaoptout";
			var link2 = "http://www.networkadvertising.org/choices";
			var link3 =
				"http://dtmc.vioc.com/adinfo/choice/prod/?type=fpc&amp;&amp;cid=18070&amp;&amp;cname=VIOC&amp;&amp;cmagic=cda79c&amp;&amp;clogo=18070.jpg&amp;&amp;loc=us&amp;&amp;lang=en-us";
			formattedHTML = '<span style="text-align: justify" class="textJustify bodyCopy">' + text1 +
				'</span> <span class="textJustify"><a class="links valvolineLink" target="_blank" href=' + ' ' + link1 + '>' +
				link1Text + ' ' + '</a></span> <span class="textJustify bodyCopy">' + text2 +
				'</span><span class="textJustify"><a class="links valvolineLink" target="_blank" href=' + ' ' + link11 + '>' +
				link1Text + '</a>' + ' ' + '</span><span style="text-align: justify" class="textJustify bodyCopy">' + text3 +
				'</span><span class="textJustify"><a class="links valvolineLink" target="_blank" href=' + ' ' + link2 + '>' +
				link2Text + ' ' + '</a></span></span><span style="text-align: justify" class="textJustify bodyCopy">' + text4 +
				'</span><span style="text-align: justify" class="textJustify bodyCopy">' + ' ' + text5 +
				'</span><span class="textJustify"><a target="_blank" class="links valvolineLink" href=' + ' ' + link3 + ' ' + '>' +
				link3Text + '</a></span>';
			return formattedHTML;
		}
	});
});