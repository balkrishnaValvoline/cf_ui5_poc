sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function(Base) {
	"use strict";

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Footer.TermsOfUse", {
		/** @module TermsOfUse */
		/**
		 * @function onInit - This function initializes the controller
		 */
		onInit: function() {
			Base.prototype.onInit.call(this);

			/**
			 * Convenience method for getting the view model by name.
			 * Convenience method for getting for items from the view .
			 * Method {addStyleClass} to add a class
			 */
			this.getView().byId("ToUFooter").addStyleClass("underline");
			this.getRouter().getRoute("termsOfUse").attachPatternMatched(this._onObjectMatched, this);
			jQuery.sap.addUrlWhitelist("mailto" ,"valvoline.com");
		},

		_onObjectMatched: function() {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("COMMON_PAGE")) {
				return;
			}
			this.headerIconAccess();

			//get user info and direct account
			this.getUserInformation(this);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "other"
			});
		},

		/**
		 * Returns the formatted text for the questions section.
		 * @function formatterQuestions
		 */
		formatterQuestions: function(text1, linkText, text2) {
			var formattedHTML;
			formattedHTML = '<span class="bodyCopy">' + text1 + ' ' +
				'<a href="mailto:feedback@valvoline.com" class="links valvolineLink underline">' + linkText + ' ' + '</a>' + text2 + '</span>';
			return formattedHTML;
		},

		/**
		 * Navigates to Valvoline's website.
		 * @function termsLinkValvoline
		 */
		termsLinkValvoline: function() {
			window.open("http://www.valvoline.com/", "_blank");
		},
		/**
		 * Opens a email window, mailto: feedback@valvoline.com
		 * @function toEmail
		 */
		toEmail: function() {
			window.open("mailto:feedback@valvoline.com", "email");
		}
	});
});