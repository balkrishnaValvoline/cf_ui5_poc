sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel"
], function (Base, JSONModel) {
	"use strict";
	var _oRouter;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Footer.AboutValvolineDash", {
		/** @module AboutValvolineDash */
		/**
		 * This function initializes the controller and adds styles to the page
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oRouter = this.getRouter();
			var oModelAbout = new JSONModel();
			oModelAbout.loadData("model/aboutValvoline.json");
			this.getView().setModel(oModelAbout);
			this.getView().byId("aboutdash_firstparagraph").addStyleClass("otherMarginText bodyCopy textJustify");
			_oRouter.getRoute("aboutValvolineDash").attachPatternMatched(this._onObjectMatched, this);
		},

		/**
		 * This function the i18n value of the given element
		 * @function i18nFormatter
		 * @param {string} sParam - i18 element
		 * @returns i18 value
		 *
		 */
		i18nFormatter: function (sParam) {
			var i18n = this.getResourceBundle();
			return i18n.getText(sParam);
		},

		_onObjectMatched: function () {
			// Clears all error message
			this.clearServerErrorMsg(this);

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "other"
			});

			//get user info and direct account
			this.getUserInformation(this);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			this.getView().byId("aboutVLVDashFooterD").addStyleClass("underline");

			//Shopping Cart Values
			this.getShoppingCartCookie(this);
		},
		/**
		 * Navigates to "Contact Us" page
		 * @function toLinkAbout
		 */
		toLinkAbout: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			ga('set', 'page', '/Contacts');
			ga('send', 'pageview');

			this.getRouter().navTo("contacts");
		}
	});
});