sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function(Base) {
	"use strict";
	var sLiveUrl = "";

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Footer.Contacts", {
		/** @module Contacts */
		/**
		 * This function initializes the controller and adds styles to the page
		 * @function onInit
		 */
		onInit: function() {
			Base.prototype.onInit.call(this);
			this.getRouter().getRoute("contacts").attachPatternMatched(this._onObjectMatched, this);
			this.headerIconAccess();
		},
		_onObjectMatched: function() {
			// Clears all error message
			this.clearServerErrorMsg(this);

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "other"
			});


			//get user info and direct account
			this.getUserInformation(this);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			this.getView().byId("ContactUSFooterD").addStyleClass("underline");

			//Shopping Cart Values
			this.getShoppingCartCookie(this);
			
			// Live Chat show/hide
			var oChatUrl = this.getGlobalParameter("liveChatURL", "LiveChat");
			if(oChatUrl[0] && oChatUrl[0].status === "A"){
				this.getView().byId("chatSupportButton").setVisible(true);
				sLiveUrl = oChatUrl[0].value;
			}else{
				this.getView().byId("chatSupportButton").setVisible(false);
				sLiveUrl = "";
			}
			
		},

		/**
		 * Opens pre-defined email client with destination: support@valvoline.com
		 * @function mailTo
		 */
		mailTo: function() {
			//send mail to valvoline support
			window.open("mailto: support@valvoline.com");
		},

		/**
		 * Opens Valvoline Live Chat Support
		 * @function onChatSupport
		 */
		onChatSupport: function() {

			var chatContainer = $(".js-chat-container");

			if (chatContainer[0]) {
				var placeToAddId = chatContainer[0].id;
				var placeToAdd = sap.ui.getCore().byId(placeToAddId);

				var VBox = new sap.m.VBox({
					width: "100%"
				}).addStyleClass("chatVbox");

				var button = new sap.m.Button({
					icon: "sap-icon://decline",
					press: this.closeChatSupport
				}).addStyleClass("chatCloseButton");
				VBox.addItem(button);

				var iframe = new sap.ui.core.HTML({
					content: "<iframe id='chatFrame' class='chatFrame' src=" + sLiveUrl + ">"
				});

				placeToAdd.addItem(VBox);
				placeToAdd.addItem(iframe);
			}

			this.initializeCountTimeChatLive();
			this.openChatLive();
		},

		/**
		 * Send GTM Tracking when Opens Valvoline Live Chat Support
		 * @function openChatLive
		 */
		openChatLive: function() {
			//GTM
			this.GTMDataLayer('livchatbttn',
				'Live Chat Support',
				'Live Chat Button -click',
				sap.ui.getCore().getModel("oAccounts").getData().uuid
			);

		}
	});
});