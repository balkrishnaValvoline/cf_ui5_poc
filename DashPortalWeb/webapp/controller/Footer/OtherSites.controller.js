sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function(Base) {
	"use strict";
	var _oView;
	var _oController;
	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Footer.OtherSites", {
		/** @module OtherSites */
		/**
		 * This function initializes the controller and ataches events to the link icons
		 * @function onInit
		 */
		onInit: function() {
			Base.prototype.onInit.call(this);
			_oView = this.getView();
			_oController = this;

			// For desktop, attachBrowserEvent function to the links
			if (this.onGetDashDevice() === "WIDE") {
				var OS = ["1", "3", "5", "7", "8", "9", "10", "11", "12", "13", "15"];
				for (var i = 0; i < OS.length; i++) {
					_oView.byId("OtherSitesLink_" + OS[i]).attachBrowserEvent("mouseover", this.mouseOverBox);
					_oView.byId("OtherSitesLink_" + OS[i]).attachBrowserEvent("mouseout", this.mouseOutBox);
					_oView.byId("OtherSitesIcon_" + OS[i]).attachBrowserEvent("mouseover", this.mouseOverBox);
					_oView.byId("OtherSitesIcon_" + OS[i]).attachBrowserEvent("mouseout", this.mouseOutBox);
				}
			}
			this.headerIconAccess();
			this.getRouter().getRoute("otherSites").attachPatternMatched(this._onObjectMatched, this);
		},

		/**
		 * Styles the links on mouseover.
		 * @function mouseOverBox
		 * @param {Object} oEvent - trigged by the mouseover on link or icon
		 */
		mouseOverBox: function(oEvent) {
			var OSid = oEvent.target.id.split("_")[3];
			_oView.byId("OtherSitesLink_" + OSid).addStyleClass("iconOtherSites");
			_oView.byId("OtherSitesIcon_" + OSid).addStyleClass("iconOtherSites");
		},

		/**
		 * Styles the links on mouseout
		 * @function mouseOverBox
		 * @param {Object} oEvent - trigged by the mouseout on link or icon
		 */
		mouseOutBox: function(oEvent) {
			var OSid = oEvent.target.id.split("_")[3];
			_oView.byId("OtherSitesLink_" + OSid).removeStyleClass("iconOtherSites");
			_oView.byId("OtherSitesIcon_" + OSid).removeStyleClass("iconOtherSites");
		},

		_onObjectMatched: function() {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page Authorization and layout
			if (!this.checkDASHPageAuthorization("COMMON_PAGE")) {
				return;
			}

			//get user info and direct account
			this.getUserInformation(this);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			this.getView().byId("otherSitesFooterD").addStyleClass("underline");

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "other"
			});
		},

		/**
		 * Opens valvolineSite in a new tab
		 * @function valvolineSiteLink
		 */
		valvolineSiteLink: function() {
			window.open("http://www.valvoline.com", "_blank");

			//GTM
			_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIcorporateText1LK")
			);
		},

		/**
		 * Opens valvolineHDSite in a new tab
		 * @function valvolineHDSiteLink
		 */
		valvolineHDSiteLink: function() {
			window.open("http://hd.valvoline.com/", "_blank");

			//GTM
			/*	_oController.GTMDataLayer('outboundlinks',
					'Outbound Links',
					'Link click',
					_oController.getResourceBundle().getText("OTHSIcorporateText1LK")
				);*/
		},

		/**
		 * Opens valvolineTeamSite in a new tab.
		 * @function valvolineTeamSiteLink
		 */
		valvolineTeamSiteLink: function() {
			window.open("http://team.valvoline.com/", "_blank");

			//GTM
			_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIcorporateText3LK")
			);
		},
		/**
		 * Opens voicTeamSite in a new tab.
		 * @function valvolineTeamSiteLink
		 */
		voicSiteLink: function() {
			window.open("https://www.vioc.com/", "_blank");

			//GTM
			/*_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIcorporateText3LK")
			);*/
		},

		/**
		 * Opens expresscareTeamSite in a new tab.
		 * @function valvolineTeamSiteLink
		 */
		expresscareSiteLink: function() {
			window.open("http://www.expresscare.com/", "_blank");

			//GTM
			_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIcorporateText5LK")
			);
		},

		/**
		 * Opens valvolinecommunitySite in a new tab.
		 * @function valvolinecommunitySiteLink
		 */
		valvolinecommunitySiteLink: function() {
			window.open("http://Valvolineserviceplus.com", "_blank");

			//GTM
			_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIcorporateText5LK")
			);
		},

		/**
		 * Opens learnValvoline in a new tab.
		 * @function learnValvolineLink
		 */
		learnValvolineLink: function() {
			window.open("http://learnvalvoline.com", "_blank");

			//GTM
			_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIorderingText2LK")
			);
		},

		/**
		 * Opens valvolinebannersSite in a new tab.
		 * @function valvolinebannersSiteLink
		 */
		valvolinebannersSiteLink: function() {
			window.open("http://www.valvolinebanners.com/", "_blank");

			//GTM
			/*_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIorderingText2LK")
			);*/
		},

		/**
		 * Opens valvolinestoreSite in a new tab.
		 * @function valvolinestoreSiteLink
		 */
		valvolinestoreSiteLink: function() {
			window.open("http://www.valvolinestore.com/", "_blank");

			//GTM
			_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIorderingText4LK")
			);
		},

		/**
		 * Opens posValvolineSite in a new tab.
		 * @function posValvolineSiteLink
		 */
		posValvolineSiteLink: function() {
			window.open("http://pos.valvoline.com/", "_blank");

			//GTM
			_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIwarrantyText1LK")
			);
		},

		/**
		 * Opens myValvolineSite in a new tab.
		 * @function  myValvolineSiteLink
		 */
		myValvolineSiteLink: function() {
			window.open("http://www.myvalvoline.com/", "_blank");

			//GTM
			_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIproductsText1LK")
			);
		},

		/**
		 * Opens valvolinefiltersSite in a new tab.
		 * @function valvolinefiltersSiteLink
		 */
		valvolinefiltersSiteLink: function() {
			window.open("http://valvolinefilters.com/", "_blank");

			//GTM
			_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIproductsText2LK")
			);
		},

		/**
		 * Opens sdsValvolineSite in a new tab.
		 * @function sdsValvolineSiteLink
		 */
		sdsValvolineSiteLink: function() {
			window.open("https://sds.valvoline.com/", "_blank");
			
			//GTM
		/*	_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIproductsText2LK")
			);*/
		},

		/**
		 * Opens valvolineInstaller in a new tab.
		 * @function installerValvolineSiteLink
		 */
		installerValvolineSiteLink: function() {
			window.open("http://valvolineInstaller.com/", "_blank");
			
			//GTM
			_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIcostumerText2LK")
			);
		},

		/**
		 * Opens valvolineUniversity in a new tab.
		 * @function universityValvolineSiteLink
		 */
		universityValvolineSiteLink: function() {
			window.open("http://university.valvoline.com/", "_blank");
			
			//GTM
			_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIcostumerText1LK")
			);
		},

		/**
		 * Opens learnValvoline in a new tab.
		 *  @function learnValvolineSiteLink
		 */
		learnValvolineSiteLink: function() {
			window.open("http://learnvalvoline.com/", "_blank");
			
			//GTM
			_oController.GTMDataLayer('outboundlinks',
				'Outbound Links',
				'Link click',
				_oController.getResourceBundle().getText("OTHSIcostumerText3LK")
			);
		}
	});
});