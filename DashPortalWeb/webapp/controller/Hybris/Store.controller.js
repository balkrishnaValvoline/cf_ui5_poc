sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/core/mvc/Controller"
], function (Base, Controller) {
	"use strict";

	var _oController;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Hybris.Store", {
		/**
		 * This function is called when the fist rendered.
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oController = this;
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("store").attachPatternMatched(this._onObjectMatched, this);
		},

		/**
		 * This function is called everytime the page is loaded, and refresh the data and the page opening.
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function (oEvent) {
			var sHybrisHash = oEvent.getParameter("arguments")["hybrisHash*"] || "";
			var mHybrisParameters = sap.ui.getCore().getModel("HybrisParameters");
			if (mHybrisParameters === undefined) {
				mHybrisParameters = new sap.ui.model.json.JSONModel();
				mHybrisParameters.setData(this.getGlobalParametersByGroup("HybrisParameters"));
				sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
			}
			var oHybrisParameters = mHybrisParameters.getData();
			if (sHybrisHash.length > 1) {
				if (oHybrisParameters.showHybrisFrame && oHybrisParameters.showHybrisFrame === "true") {
					_oController.setHybrisInIframe(oHybrisParameters.Hybris + sHybrisHash);
				} else {
					window.open(oHybrisParameters.Hybris + sHybrisHash, "_self");
				}
			} else {
				if (oHybrisParameters.showHybrisFrame && oHybrisParameters.showHybrisFrame === "true") {
					_oController.setHybrisInIframe(oHybrisParameters.Hybris);
				} else {
					window.open(oHybrisParameters.Hybris, "_self");
				}
			}
		}
	});
});