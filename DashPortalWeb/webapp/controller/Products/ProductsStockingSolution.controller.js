sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel"
], function (Base, JSONModel) {
	"use strict";
	var _oView, _oController, _oRouter;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Products.ProductsStockingSolution", {
		/** @module Stocking Solution */
		/**
		 * This function initializes the controller
		 * model.
		 * 
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oRouter = this.getRouter();
			_oController = this;
			_oView = _oController.getView();
			_oRouter.getRoute("stockingSolution").attachPatternMatched(this._onObjectMatched, this);
		},

		onAfterRendering: function () {
			/* Set the products item on the side menu as active if the app is reloaded */
			$(".js-products-sidenav").addClass("sidenav__item--active");
			//Remove the focus of the input
			$(':focus').blur();
		},

		/**
		 * Set the filter for the direct account and get the stocking solution data for the selection
		 * accounts.
		 * 
		 * @function _onObjectMatched
		 * @param {Object}
		 *            event - Triggered when navigating to stockingsolution page
		 */
		_onObjectMatched: function (Event) {
			// Clears all error message
			_oController.clearServerErrorMsg(_oController);

			// set the navbar icon as selected
			_oController.resetMenuButtons();

			// Page authorization
			if (!_oController.checkDASHPageAuthorization("STOCKINGSOLUTION_PAGE")) {
				return;
			}

			var oElementAccess = {
				"add2CartAccess": false
			};
			_oView.setModel(new JSONModel(oElementAccess), "elementAccess");
			//Set Busy Indicator on the tabBar
			_oView.byId("IconTabBar").setBusyIndicatorDelay(0).setBusy(true);
			//Set Busy Indicator on the Account combobox
			_oView.byId("comboBoxStockingSolution").setBusyIndicatorDelay(0).setBusy(true);

			//get user info and direct account
			_oController.getUserInformation(_oController, _oController.setupDirectAccount);

			// check Language and set selected Key in combo box
			_oController.onChangeLanguage();

			//Check DASH Header Layout Access
			_oController.headerIconAccess();

			// Get the User Accounts
			this.getAccounts(this, this.setupAllAccounts);

			// Shopping Cart Values
			_oController.getShoppingCartCookie(_oController);
			setTimeout(function () {
				//Case where the elements are already rendered and the afterrendering does not fire
				$(".js-products-sidenav").addClass("sidenav__item--active");
				//removes the focus from the combobox input field(iOS)
				$(':focus').blur();
			}, 0);
		},

		/**
		 * This function is called when the basic user information is available, so that the Socking data can be called
		 * @function setupDirectAccount
		 */
		setupDirectAccount: function () {
			var oAccounts = _oView.getModel("oAccounts").getData().accounts;
			_oView.setModel(new JSONModel(oAccounts), "ShipTo");
			_oView.byId("comboBoxStockingSolution").setSelectedKey(oAccounts[0].id);
			_oView.setModel(new JSONModel(oAccounts[0]), "oSelectedShipTo");
			_oController.getStockingSolution(oAccounts[0].accountnum);
		},

		/**
		 * This function is called when all the accounts are available
		 * @function setupAllAccounts
		 */
		setupAllAccounts: function () {
			var oAccounts = new JSONModel(_oView.getModel("oAccounts").getData().accounts);
			oAccounts.setSizeLimit(_oView.getModel("oAccounts").getData().accounts.length);
			_oView.setModel(oAccounts, "ShipTo");
			_oView.byId("comboBoxStockingSolution").setBusy(false);

			if (_oController.checkDASHLayoutAccess("ADD2CART_ICON") && !sap.ui.getCore().getModel("oModelImpersonate").getProperty(
					"/impersonateStatus")) {
				_oView.getModel("elementAccess").setProperty("/add2CartAccess", true);
			}

		},

		/**
		 * This function is called when a Shipto is selected
		 * @function onChangeShipToComboBox
		 */
		onChangeShipToComboBox: function (oEvent) {
			var oSelectedAccount;
			var sIndex = "/0";
			//Set Busy Indicator on the tabBar
			_oView.byId("IconTabBar").setBusy(true);
			//call the service, the callback needs to change the Selectedaccount

			if (oEvent.getSource().getSelectedItem()) {
				sIndex = oEvent.getSource().getSelectedItem().getBindingContextPath();
				oSelectedAccount = _oView.getModel("oAccounts").getProperty("/accounts" + sIndex);
			} else {
				oSelectedAccount = _oView.getModel("oAccounts").getProperty("/accounts" + sIndex);
				_oView.byId("comboBoxStockingSolution").setSelectedKey(oSelectedAccount.id);
			}
			_oView.setModel(new JSONModel(oSelectedAccount), "oSelectedShipTo");
			_oController.getStockingSolution(oSelectedAccount.accountnum);
		},

		/**
		 * Function that handles the success case of the ajax call that handles the Stocking Solution request
		 * @function getStockingSolutionSuccess
		 */
		getStockingSolutionSuccess: function (oController, oData, oPiggyBack) {
			if (oData === undefined || oData === [] || oData === {} || oData === null) {
				_oView.byId("noDataAvailable").setVisible(true);
				_oView.byId("IconTabBar").setVisible(false);
			} else {
				_oView.byId("noDataAvailable").setVisible(false);
				_oView.byId("IconTabBar").setVisible(true);
			}
			var oIconTabBar = _oView.byId("IconTabBar");
			_oView.setModel(
				new JSONModel(oData), "stockingSolution");
			oIconTabBar.setSelectedKey(oIconTabBar.getSelectedKey().replace(/[^-]*$/, "0"));
			oIconTabBar.setBusy(false);
		},

		/**
		 * Function that handles the error case of the ajax call that handles the Stocking Solution request
		 * @function getStockingSolutionError
		 */
		getStockingSolutionError: function (oController, oError, oPiggyBack) {
			var oIconTabBar = _oView.byId("IconTabBar");
			oController.onShowMessage(oController, "Error", oError.status, oController.getResourceBundle()
				.getText(
					"ERRStockingSolution"));
			oIconTabBar.setBusy(false);
			_oView.byId("noDataAvailable").setVisible(true);
			_oView.byId("IconTabBar").setVisible(false);

		},

		/**
		 * This function is called to call the service in order to retrieve the stocking solution data for a shipTo
		 * @function getStockingSolution
		 * @param {iShipTo}
		 */
		getStockingSolution: function (iAccountNum) {
			var sSelectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			var xsURL = "/products/stockingSolution" + "?shipTo=" + iAccountNum + "&lang=" + sSelectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "Get", _oController.getStockingSolutionSuccess, _oController.getStockingSolutionError);
		},

		/**
		 * Sets the number of rows to show in the initial VRI table
		 * @function formatVRITableRowNum
		 **/
		formatVRITableRowNum: function (rows) {
			if (rows !== undefined) {
				return rows.length;
			}
		},

		/**
		 * Handles the add2cart press, calling the service
		 * @function onAdd2Cart
		 **/
		onAdd2Cart: function (oEvent) {
			var oPiggyBack = oEvent.getSource();
			oPiggyBack.setBusyIndicatorDelay(0).setBusyIndicatorSize("Small").setBusy(true);
			var oModel = _oController.getView().getModel("stockingSolution");
			var oRow = oEvent.getSource().getBindingContext("stockingSolution").getPath();
			var sCellID = oEvent.getSource().getId();
			var aAggregationCells = oEvent.getSource().getParent().getParent().getCells();
			var oCurrentEntry, oRequestObject;
			//starts at 2 to ignore the viscosity and the 1st reorderPoint
			for (var i = 2; i < aAggregationCells.length; i++) {
				if (aAggregationCells[i].getAggregation("items")[1].getId() === sCellID) {
					break;
				}
			}
			oCurrentEntry = oModel.getProperty(oRow + "/" + i);

			oRequestObject = {
				"user": sap.ui.getCore().getModel("oAccounts").getProperty("/uuid"),
				"productCode": oCurrentEntry.productCode,
				"shipTo": _oView.getModel("oSelectedShipTo").getProperty("/accountnum"),
				"orderingUom": oCurrentEntry.orderingUom,
				"quantity": oCurrentEntry.quantity
			};
			_oController._selectedProductCode = oCurrentEntry.productCode;
			var sSelectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			var xsURL = "/products/add2Cart" + "?lang=" + sSelectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "POST", _oController.onAdd2CartSuccess, _oController.onAdd2CartError,
				JSON.stringify(oRequestObject), oPiggyBack);
		},

		/**
		 * Handles the success callback of the add2Cart call
		 * @function onAdd2CartSuccess
		 **/
		onAdd2CartSuccess: function (oController, oData, oPiggyBack) {
			if (oData !== undefined && oData !== null && oData.totalItems && oData.totalPrice && oData.totalPrice.formattedValue) {
				//Succesfully added to the cart, update the cart cookie
				_oController.setShoppingCartCookie(_oController, oData.totalItems, oData.totalPrice.formattedValue);
				oController.onShowMessage(oController, "SuccessSmall", undefined, oController.getResourceBundle()
					.getText(
						"PSSAdd2CartSucessMsg"));

				//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
				_oController.GTMDataLayer('reorderMssg',
					'Stocking Solution',
					'Item Add-to-Cart -Status',
					oController.getResourceBundle()
					.getText(
						"PSSAdd2CartSucessMsg")
				);

				var sSelectedId = _oController.getView().byId("IconTabBar") && _oController.getView().byId("IconTabBar").getSelectedKey();
				if (sSelectedId) {
					var sCategoryType = sap.ui.getCore().byId(sSelectedId) && sap.ui.getCore().byId(sSelectedId).getText();
					if (sCategoryType) {

						//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
						_oController.GTMDataLayer('stcksolunAdd2cart',
							'Stocking Solution',
							sCategoryType + ' -Add-to-Cart',
							_oController._selectedProductCode
						);
					}
				}
			}
			oPiggyBack.setBusy(false);
		},

		/**
		 * Handles the error callback of the add2Cart call
		 * @function onAdd2CartError
		 **/
		onAdd2CartError: function (oController, oError, oPiggyBack) {
			var sErrorMsg, sErrorType;
			if (oError && oError.responseJSON && oError.responseJSON.errorCode) {
				sErrorMsg = oError.responseJSON.errorCode.substr(oError.responseJSON.errorCode.indexOf(':') + 1) || _oController.getResourceBundle()
					.getText(
						"PSSAdd2CartErrorfullContactUsMsg");
				sErrorType = oError.responseJSON.errorCode.substr(0, oError.responseJSON.errorCode.indexOf(':')) || "";
			} else {
				sErrorMsg = _oController.getResourceBundle().getText(
					"PSSAdd2CartErrorfullContactUsMsg");
				sErrorType = "";
			}
			switch (sErrorType) {
			case ("WARN"):
				oController.onShowMessage(oController, "WarningFade", undefined, sErrorMsg);
				break;
			case ("DIALOG"):
				setTimeout(function () {
					_oController.DataLossWarning =
						new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", _oController);
					//Set Warning Buttons
					var oYesButton = sap.ui.getCore().byId("button_Dialog_Warning_Yes");
					var oNoButton = sap.ui.getCore().byId("button_Dialog_Warning_No");
					oYesButton.attachPress(_oController.cartCheckout);
					oYesButton.setText(_oController.getResourceBundle().getText("PSSAdd2CartCheckout"));
					oNoButton.attachPress(_oController.cartCheckoutClose);
					oNoButton.setText(_oController.getResourceBundle().getText("PSSAdd2CartCancel"));
					//Set Warning Message
					sap.ui.getCore().byId("text1_Dialog_Warning").setText(sErrorMsg);
					//Set Warnig Dialog Title
					sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText(
						"PSSWarningDialogTitle"));
					_oController.DataLossWarning.open();
					_oController._removeDialogResize(_oController.DataLossWarning);
				}, 0);
				break;
			case ("ERROR"):
			default:
				oController.onShowMessage(oController, "ErrorContactUs", undefined, sErrorMsg);
			}
			oPiggyBack.setBusy(false);
			//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
			_oController.GTMDataLayer('reorderMssg',
				'Stocking Solution',
				'Item Add-to-Cart -Status',
				sErrorMsg
			);
		},
		/**
		 * Cancel the cart checkout and close popup
		 * @function cartCheckoutClose
		 */
		cartCheckoutClose: function () {
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();
		},
		/**
		 * Close popup and redirect to cart page
		 * @function cartCheckout
		 */
		cartCheckout: function () {
			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();
			var hybrisLinks = _oController.getGlobalParametersByGroup("HybrisParameters");
			_oController.nav2Hybris(hybrisLinks.Hybris + hybrisLinks.HybrisCart);
		},

		onStockingSolutionTabSelect: function (oEvent) {
			var sSelectedId = oEvent.getSource().getSelectedKey();
			if (sSelectedId) {
				var sText = sap.ui.getCore().byId(sSelectedId) && sap.ui.getCore().byId(sSelectedId).getText();
				if (sText) {
					//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
					_oController.GTMDataLayer('stcksolunTab',
						'Stocking Solution',
						'Header Category -click',
						sText
					);
				}
			}
		}
	});
});