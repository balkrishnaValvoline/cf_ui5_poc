sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"jquery.sap.global",
	"sap/ui/model/json/JSONModel"
], function (Base, jQuery, JSONModel) {
	"use strict";

	var _oModelVehicleYear,
		_oModelVehicleMake,
		_oModelVehicleModel,
		_oModelVehicleType,
		_oModelProductsPageVehicleInfo,
		_oController,
		_oView,
		_vehicleTypeIDOEM,
		_categoryID,
		_noImgUrl,
		_isFromSearch,
		_fstTime = true;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Products.ProductsCategory", {
		/** @module ProductsCategory */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oController = this;
			_oView = this.getView();
			var oRouter = this.getRouter();
			oRouter.getRoute("productsCategory").attachPatternMatched(this._onObjectMatched, this);

			// Attach Click event on the HBox for the NavBack Button
			_oView.byId("navBackProductDetails").attachBrowserEvent("click", function () {
				_oController.onNavBack();
			});

			// Makes the text clickable
			_oController.byId("promotionBanner").attachBrowserEvent("click", this.onPressPromotions);
			_oController.byId("promotionBanner").addStyleClass("cursorPointer");

			_oController.byId("promotionBannerFilterInfo").attachBrowserEvent("click", this.onPressFilterInfo);
			_oController.byId("promotionBannerFilterInfo").addStyleClass("cursorPointer");
		},

		/**
		 * Set the menu as active if the app is reloaded
		 */
		onAfterRendering: function () {
			$(".js-products-sidenav").addClass("sidenav__item--active");
		},

		/**
		 * This function is called always when the page is open and verifies the user authorization and
		 * populate all the Products for the respective vehicle ID (if the navigation was from the SearchField), OR
		 * shows to the vehicle manual Selection page if the navigation was from the vehicle category selection.
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {
			if (this.getView().getParent().getCurrentPage().getViewName() !==
				"valvoline.dash.portal.DashPortalWeb.view.Products/ProductDetails") {
				_fstTime = true;
			} else {
				_fstTime = false;
			}
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("PRODUCTFINDER_PAGE")) {
				return;
			}

			if (this.getView().getModel("initialProductsData") === undefined) {
				if (this.getView().getParent().getPreviousPage() === undefined || this.getView().getParent().getCurrentPage().getViewName() !==
					"valvoline.dash.portal.DashPortalWeb.view.Products/ProductDetails") {
					var placeHolder = {
						"Components": [{
							name: this.getResourceBundle().getText("PRDTSDTproductCategoryTXT"),
							code: null,
							number: "0",
							capacities: [],
							uses: []
						}]
					};

					_oView.setModel(new JSONModel(placeHolder), "productList");
				}
			}
			// Get the model set in Core from Product Page
			this.onGetCoreModelSelectBox();

			if (_oModelProductsPageVehicleInfo === undefined) {
				ga('set', 'page', '/Products');
				ga('send', 'pageview');
				this.getRouter().navTo("productFinder", {}, true);
				return;
			}

			//get user info and direct account
			_oController.getUserInformation(_oController);

			this.headerIconAccess();

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			this.onConfigurePageLayout();

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "products"
			});

			// Set the flag for other makes option pressed
			this.bOtherMakesPressed = sap.ui.getCore().getModel("productsPageVehicleInfo").getData().otherMakesPressed;
		},

		/**
		 * This function gets models from the core and sets them on the view.
		 * @function onGetCoreModelSelectBox
		 */
		onGetCoreModelSelectBox: function () {

			// Get the model set in Core from Product Page
			_oModelVehicleYear = sap.ui.getCore().getModel("vehicleYear");
			_oModelVehicleMake = sap.ui.getCore().getModel("vehicleMake");
			_oModelVehicleModel = sap.ui.getCore().getModel("vehicleModel");
			_oModelVehicleType = sap.ui.getCore().getModel("vehicleType");
			_oModelProductsPageVehicleInfo = sap.ui.getCore().getModel("productsPageVehicleInfo");

			// Set the Models in the this View
			this.getView().setModel(_oModelVehicleYear, "vehicleYear");
			this.getView().setModel(_oModelVehicleMake, "vehicleMake");
			this.getView().setModel(_oModelVehicleModel, "vehicleModel");
			this.getView().setModel(_oModelVehicleType, "vehicleType");
		},

		/**
		 * This function sets the language to the selectbox labels
		 * @function onPlaceHolderSelectBox
		 */
		onPlaceHolderSelectBox: function () {
			//set model for Year
			var oData = sap.ui.getCore().getModel("vehicleYear").getData();
			var oModel = new sap.ui.model.json.JSONModel(oData);
			oData.Years.shift();
			oData.Years.unshift({
				id: "0",
				result: _oController.getResourceBundle().getText("PRDTSDTyearTXT")
			});
			oModel.setSizeLimit(200);
			_oController.getView().setModel(oModel, "vehicleYear");
			sap.ui.getCore().setModel(oModel, "vehicleYear");
			_oController.onPlaceHolderSelectBoxforMake();
			_oController.onPlaceHolderSelectBoxforModel();
			_oController.onPlaceHolderSelectBoxforType();
			_oController.onPlaceHolderSelectBoxforSelCategory();

		},

		onPlaceHolderSelectBoxforMake: function () {
			//set model for Make
			var oData = sap.ui.getCore().getModel("vehicleMake").getData();
			var oModel = new sap.ui.model.json.JSONModel(oData);
			oData.Makes.shift();
			oData.Makes.unshift({
				id: "0",
				result: _oController.getResourceBundle().getText("PRDTSDTmakeTXT"),
				image: null
			});
			for (var i = 0; i <= oData.Makes.length; i++) {
				if (oData.Makes[i].id === "1") {
					oData.Makes[i].result = _oController.getResourceBundle().getText("PRDTSDTnonusamakeTXT");
					break;
				}
			}
			oModel.setSizeLimit(1000);
			_oController.getView().setModel(oModel, "vehicleMake");
			sap.ui.getCore().setModel(oModel, "vehicleMake");
		},

		onPlaceHolderSelectBoxforModel: function () {
			//set model for Model
			var oData = sap.ui.getCore().getModel("vehicleModel").getData();
			var oModel = new sap.ui.model.json.JSONModel(oData);
			oData.Models.shift();
			oData.Models.unshift({
				id: "0",
				result: _oController.getResourceBundle().getText("PRDTSDTmodelTXT")
			});
			oModel.setSizeLimit(999999);
			_oController.getView().setModel(oModel, "vehicleModel");
			sap.ui.getCore().setModel(oModel, "vehicleModel");

		},

		onPlaceHolderSelectBoxforType: function () {
			//set model for Type
			var oData = sap.ui.getCore().getModel("vehicleType").getData();
			var oModel = new sap.ui.model.json.JSONModel(oData);
			oData.Types.shift();
			oData.Types.unshift({
				id: "0",
				result: _oController.getResourceBundle().getText("PRDTSDTtypeTXT")
			});
			oModel.setSizeLimit(999999);
			_oController.getView().setModel(oModel, "vehicleType");
			sap.ui.getCore().setModel(oModel, "vehicleType");

		},

		onPlaceHolderSelectBoxforSelCategory: function () {
			//set model for Category
			var oData = _oController.getView().getModel("productCategory").getData();
			var oModel = new sap.ui.model.json.JSONModel(oData);
			oData.Components.shift(0);
			oData.Components.unshift({
				name: _oController.getResourceBundle().getText("PRDTSDTproductSelectCategoryTXT"),
				code: null,
				number: "0"
			});

			_oController.getView().setModel(oModel, "productCategory");
			sap.ui.getCore().setModel(oModel, "productCategory");
		},

		/**
		 * Function that handles the success case of the ajax call that retrieves a product by it's ID
		 * @function onProductByIDSuccess
		 */
		onProductByIDSuccess: function (oController, oData, oPiggyBack) {
			var oModelAux = new sap.ui.model.json.JSONModel();
			var oModel3 = new sap.ui.model.json.JSONModel();
			var oVehicleProperties = oData.vehicleProperties;

			// Set Car Image
			_noImgUrl = oVehicleProperties.noImageUrl;
			oController.decodeBase64ToImg(oVehicleProperties.image);

			// Set the values for Search Results
			if (_isFromSearch) {
				_oView.byId("textField1").setValue(oVehicleProperties.make ? oVehicleProperties.make : "");
				_oView.byId("textField2").setValue(oVehicleProperties.model ? oVehicleProperties.model : "");
				_oView.byId("textField3").setValue(oVehicleProperties.type + " (" + oVehicleProperties.yearrange + ")");

				_oView.byId("textField1").setBusy(false);
				_oView.byId("textField2").setBusy(false);
				_oView.byId("textField3").setBusy(false);
			}

			oController.byId(oPiggyBack.sSearchField).setBusy(false);
			oModelAux.setData({
				modelData: oData
			});
			oModelAux.getData().modelData.Components.unshift({
				name: oController.getResourceBundle().getText("PRDTSDTproductSelectCategoryTXT"),
				code: null,
				number: "0",
				capacities: [],
				uses: []
			});
			oModel3.setData(oModelAux.getData().modelData);
			oController.getView().setModel(oModel3, 'productCategory');
		},

		/**
		 * Function that handles the error case of the ajax call that retrieves a product by it's ID
		 * @function onProductByIDError
		 */
		onProductByIDError: function (oController, oError, oPiggyBack) {
			oController.decodeBase64ToImg();
			oController.byId(oPiggyBack.sSearchField).setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText,
				oError.responseText, oController.getResourceBundle().getText("ERRComponents"));
		},

		/**
		 * This function  configures the page layout for the product cards.
		 * @function onConfigurePageLayout
		 */
		onConfigurePageLayout: function () {
			// Checks Session time out
			this.checkSessionTimeout();
			var controller = this;
			var language = sap.ui.getCore().getConfiguration().getLanguage();
			var xsURL;
			var oPiggyBack;

			// If the model is undefined nav back to products, else verifies if the navigation was from Selection or from Search
			if (_oModelProductsPageVehicleInfo === undefined) {
				ga('set', 'page', '/Products');
				ga('send', 'pageview');
				this.getRouter().navTo("productFinder", {}, true);
			} else {
				switch (_oModelProductsPageVehicleInfo.getProperty("/navigationMode")) {
				case "fromSelection":
					// PRODUCT PAGE FROM SEARCH CATEGORY SELECTION
					var vehicleTypeIDSelected = _oModelProductsPageVehicleInfo.getProperty("/searchTypeSelecedID");
					_vehicleTypeIDOEM = vehicleTypeIDSelected;

					var categoryIDSelected = _oModelProductsPageVehicleInfo.getProperty("/searchCategoryID");
					_categoryID = categoryIDSelected;

					// Activate the SelectBoxes
					_isFromSearch = false;
					this.getView().byId("selectBoxes").setVisible(true);
					this.getView().byId("selectBoxesSearch").setVisible(false);

					// Config Select Box
					this.getView().byId("searchYear").setEnabled(true);
					this.getView().byId("searchField1").setEnabled(true);
					this.getView().byId("searchField2").setEnabled(true);
					this.getView().byId("searchField3").setEnabled(true);
					this.getView().byId("searchField4").setVisible(true);
					this.getView().byId("oemInfoButtonSelect").setEnabled(true);

					// Set the SelectBoxes with the previous page selected vehicles
					this.getView().byId("searchYear").setSelectedKey(_oModelProductsPageVehicleInfo.getProperty("/searchYearSelecedID"));
					this.getView().byId("searchField1").setSelectedKey(_oModelProductsPageVehicleInfo.getProperty("/searchMakeSelecedID"));
					this.getView().byId("searchField2").setSelectedKey(_oModelProductsPageVehicleInfo.getProperty("/searchModelSelecedID"));
					this.getView().byId("searchField3").setSelectedKey(_oModelProductsPageVehicleInfo.getProperty("/searchTypeSelecedID"));
					this.getView().byId("searchField4").setSelectedItemId(0);

					// Set Select Box with first value
					this.byId("searchField4").setBusyIndicatorDelay(0);
					this.byId("searchField4").setBusy(true);

					// Service Call
					xsURL = "/products/productfinder/components/" + "?typeID=" + vehicleTypeIDSelected + "&lang=" + language;
					oPiggyBack = {
						"sSearchField": "searchField4"
					};

					//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
					controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onProductByIDSuccess, controller.onProductByIDError,
						undefined, oPiggyBack);

					// Set the car image container busy
					this.getView().byId("carImageVBox").setBusyIndicatorDelay(0);
					this.getView().byId("carImageVBox").setBusy(true);
					this.getView().byId("carImageProductFinder").setVisible(false);
					// Set Visible the products Card container
					this.getView().byId("productsContainer2").setVisible(true);

					// Generate the products
					controller.populateProductsCard(vehicleTypeIDSelected, "1000", "");
					break;

				case "fromSearch":
					// PRODUCT PAGE FROM SEARCH FILTER
					var vehicleTypeID = _oModelProductsPageVehicleInfo.getProperty("/ID");
					_vehicleTypeIDOEM = vehicleTypeID;

					// Activate the SelectBoxes
					_isFromSearch = true;
					this.getView().byId("selectBoxes").setVisible(false);
					this.getView().byId("selectBoxesSearch").setVisible(true);

					this.getView().byId("oemInfoButtonSelect").setEnabled(true);
					this.getView().byId("searchField6").setSelectedItemId(0);

					// Set Select Box with first value
					this.byId("textField1").setBusyIndicatorDelay(0);
					this.byId("textField1").setBusy(true);
					this.byId("textField2").setBusyIndicatorDelay(0);
					this.byId("textField2").setBusy(true);
					this.byId("textField3").setBusyIndicatorDelay(0);
					this.byId("textField3").setBusy(true);
					this.byId("searchField6").setBusyIndicatorDelay(0);
					this.byId("searchField6").setBusy(true);

					// Service Call
					xsURL = "/products/productfinder/components/" + "?typeID=" + vehicleTypeID + "&lang=" + language;
					oPiggyBack = {
						"sSearchField": "searchField6"
					};

					//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
					controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onProductByIDSuccess, controller.onProductByIDError,
						undefined, oPiggyBack);

					// Set the car image container busy
					this.getView().byId("carImageVBox").setBusyIndicatorDelay(0);
					this.getView().byId("carImageVBox").setBusy(true);
					this.getView().byId("carImageProductFinder").setVisible(false);
					// Set Visible the products Card container
					this.getView().byId("productsContainer2").setVisible(true);

					// Generate the products
					controller.populateProductsCard(vehicleTypeID, "1000", "");
					break;
				default:
					break;
				}
			}
		},

		/**
		 * Function that handles the success case of the ajax call to get the Year
		 * @function onSearchYearSuccess
		 */
		onSearchYearSuccess: function (oController, oData, oPiggyBack) {
			if (oPiggyBack.year === oController.getView().byId("searchYear").getSelectedKey()) {
				var oModelAux = new sap.ui.model.json.JSONModel();
				var oModel = new sap.ui.model.json.JSONModel();
				oController.byId("searchField1").setBusy(false);
				oController.getView().byId("searchField1").setEnabled(true);

				oModelAux.setData({
					modelData: oData
				});

				oModelAux.getData().modelData.usaMakes.unshift({
					id: "0",
					result: oController.getResourceBundle().getText("PRDTSDTmakeTXT"),
					image: null
				});
				if (oData.nonUsaMakes && oData.nonUsaMakes.length > 0) {
					oModelAux.getData().modelData.usaMakes.push({
						id: "1",
						result: oController.getResourceBundle().getText("PRDTSDTnonusamakeTXT"),
						image: null
					});
				}
				oModelAux.getData().modelData.Makes = oModelAux.getData().modelData.usaMakes;
				oModel.setData(oModelAux.getData().modelData);
				oModel.setSizeLimit(1000);
				sap.ui.getCore().setModel(oModel, "vehicleMake");
				oController.getView().setModel(oModel, "vehicleMake");

				/* Disable Makes selection if no makes were returned */
				if (!oModel.getData().Makes[1]) {
					oController.getView().byId("searchField1").setEnabled(false);
				}
			}
		},

		/**
		 * Function that handles the error case of the ajax call to get the Year
		 * @function onSearchYearError
		 */
		onSearchYearError: function (oController, oError, oPiggyBack) {
			oController.byId("searchYear").setBusy(false);
			oController.byId("searchField1").setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRMakes"));
		},

		/**
		 * This function calls the service to load the information in the select boxes.
		 * @function onSearchYear
		 */
		onSearchYear: function () {
			// Checks Session time out
			this.checkSessionTimeout();
			var controller = this;
			var vehicleYear = this.byId("searchYear").getSelectedKey();
			var categoryID = _categoryID;
			var xsURL;

			// Piggyback
			var oPiggyBack = {
				"year": vehicleYear
			};

			// Set the flag for other makes
			this.bOtherMakesPressed = false;

			// Car Image Busy indicator
			_oView.byId("carImageProductFinder").setVisible(false);
			_oView.byId("carImageVBox").setBusy(true);

			if (vehicleYear !== "0") {
				// Service Call
				this.byId("searchField1").setBusyIndicatorDelay(0);
				this.byId("searchField1").setBusy(true);
				xsURL = "/products/productfinder/makes/" + "?categoryID=" + categoryID + "&year=" + vehicleYear + "&getImage=small";
				controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onSearchYearSuccess, controller.onSearchYearError,
					undefined, oPiggyBack);

				// Set first item on the SelectBoxes
				controller.getView().byId("searchField1").setSelectedItemId(0);
				controller.getView().byId("searchField2").setSelectedItemId(0);
				controller.getView().byId("searchField3").setSelectedItemId(0);
				controller.getView().byId("searchField4").setSelectedItemId(0);

				// Set No Car Image
				this.decodeBase64ToImg();

				this.getView().byId("searchField1").setEnabled(false);
				this.getView().byId("searchField2").setEnabled(false);
				this.getView().byId("searchField3").setEnabled(false);
				this.getView().byId("searchField4").setEnabled(false);
				this.getView().byId("oemInfoButtonSelect").setEnabled(false);

				// Configure View Content
				this.getView().byId("productsContainer2").setVisible(false);

			} else {
				this.byId("searchField1").setBusyIndicatorDelay(0);
				this.byId("searchField1").setBusy(true);
				xsURL = "/products/productfinder/makes/" + "?categoryID=" + categoryID + "&getImage=small";
				controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onSearchYearSuccess, controller.onSearchYearError,
					undefined, oPiggyBack);

				// Enable SelectBox
				this.getView().byId("searchField1").setSelectedItemId(0);
				this.getView().byId("searchField2").setSelectedItemId(0);
				this.getView().byId("searchField3").setSelectedItemId(0);
				this.getView().byId("searchField4").setSelectedItemId(0);

				// Set No Car Image
				this.decodeBase64ToImg();

				this.getView().byId("searchField1").setEnabled(false);
				this.getView().byId("searchField2").setEnabled(false);
				this.getView().byId("searchField3").setEnabled(false);
				this.getView().byId("searchField4").setEnabled(false);
				this.getView().byId("oemInfoButtonSelect").setEnabled(false);

				// Configure View Content
				this.getView().byId("productsContainer2").setVisible(false);
			}
		},

		/**
		 * Function that handles the success case of the ajax call to get from make Search
		 * @function onSearchMakeSuccess
		 */
		onSearchMakeSuccess: function (oController, oData, oPiggyBack) {
			_fstTime = true;
			var oModelAux = new sap.ui.model.json.JSONModel();
			var oModel1 = new sap.ui.model.json.JSONModel();
			oModelAux.setData({
				modelData: oData
			});
			oModelAux.getData().modelData.Models.unshift({
				id: "0",
				result: oController.getResourceBundle().getText("PRDTSDTmodelTXT")
			});
			oModel1.setData(oModelAux.getData().modelData);
			oController.getView().setModel(oModel1, "vehicleModel");
			oController.getView().byId("searchField2").setEnabled(true);
			oController.byId("searchField2").setBusy(false);
		},

		/**
		 * Function that handles the error case of the ajax call to get from make Search
		 * @function onSearchMakeError
		 */
		onSearchMakeError: function (oController, oError, oPiggyBack) {
			oController.byId("searchField2").setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRModels"));
		},

		/**
		 * This function is called when the user selects the "MAKE" selectBox. Than call a service that fills the "MODEL" selectBox.
		 * @function onSearchMake
		 */
		onSearchMake: function (oEvent) {
			// Checks Session time out
			this.checkSessionTimeout();
			var controller = this;
			var vehicleMakeID = this.byId("searchField1").getSelectedKey();
			var vehicleYear = this.byId("searchYear").getSelectedKey();
			var oSelectedItem = this.getView().byId("searchField1").getSelectedItem();
			var xsURL;

			// Other Makes Case
			if (vehicleMakeID === "1") {
				var oMakeData = sap.ui.getCore().getModel("vehicleMake").getData();
				// Add or remove Non USA make vehicles to the list
				var aMakes = [];
				if (!this.bOtherMakesPressed) {
					aMakes.push.apply(aMakes, oMakeData.usaMakes);
					aMakes.push.apply(aMakes, oMakeData.nonUsaMakes);
					this.bOtherMakesPressed = true;
				} else {
					aMakes.push.apply(aMakes, oMakeData.usaMakes);
					this.bOtherMakesPressed = false;
				}
				oMakeData.Makes = aMakes;
				sap.ui.getCore().getModel("vehicleMake").refresh();
				this.bChangeEventFired = false;
				this.getView().byId("searchField1")._checkSelectionChange = function () {
					var oItem = this.getSelectedItem();
					var sKey = this.getSelectedKey();
					if (sKey === "1" && controller.bChangeEventFired) {
						this.fireChange({
							selectedItem: oItem
						});
						controller.bChangeEventFired = false;
					}
					controller.bChangeEventFired = true;
					if (sKey === "1") {
						this.getItems()[0].setText(_oController.getResourceBundle().getText("PRDTSDTmakeTXT"));
						this.setSelectedKey("0");
					}
				};
				var fnAfterClose = function () {
					if (vehicleMakeID === "1") {
						this.getView().byId("searchField1").open();
					}
					this.getView().byId("searchField1").getPicker().detachAfterClose(fnAfterClose, this);
				};
				var fnAfterOpen = function () {
					if (vehicleMakeID === "1") {
						jQuery.sap.delayedCall(0, null, function () {
							_oView.byId("searchField1").scrollToItem(oSelectedItem);
						});
					}
					this.getView().byId("searchField1").getPicker().detachAfterOpen(fnAfterOpen, this);
				};
				this.getView().byId("searchField1").getPicker().attachAfterClose(fnAfterClose, this);
				this.getView().byId("searchField1").getPicker().attachAfterOpen(fnAfterOpen, this);

				// Enable SelectBox
				this.getView().byId("searchField2").setSelectedItemId(0);
				this.getView().byId("searchField3").setSelectedItemId(0);
				this.getView().byId("searchField4").setSelectedItemId(0);

				this.getView().byId("searchField2").setEnabled(false);
				this.getView().byId("searchField3").setEnabled(false);
				this.getView().byId("searchField4").setEnabled(false);
				this.getView().byId("oemInfoButtonSelect").setEnabled(false);
				// Configure View Content
				this.getView().byId("productsContainer2").setVisible(false);

				return;
			}
			if (vehicleMakeID !== "0") {

				// Service Call
				this.byId("searchField2").setBusyIndicatorDelay(0);
				this.byId("searchField2").setBusy(true);

				// Car Image Busy indicator
				_oView.byId("carImageProductFinder").setVisible(false);
				_oView.byId("carImageVBox").setBusy(true);

				if (vehicleYear === "0") {
					xsURL = "/products/productfinder/models/" + "?makeID=" + vehicleMakeID;
				} else {
					xsURL = "/products/productfinder/models/" + "?makeID=" + vehicleMakeID + "&year=" + vehicleYear;
				}

				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onSearchMakeSuccess, controller.onSearchMakeError);

				// Set first item on the SelectBoxes
				this.getView().byId("searchField2").setSelectedItemId(0);
				this.getView().byId("searchField3").setSelectedItemId(0);
				this.getView().byId("searchField4").setSelectedItemId(0);

				// Set No Car Image
				this.decodeBase64ToImg();

				// Configure SelectBox
				this.getView().byId("searchField2").setEnabled(false);
				this.getView().byId("searchField3").setEnabled(false);
				this.getView().byId("searchField4").setEnabled(false);
				this.getView().byId("searchField4").setEnabled(false);
				this.getView().byId("oemInfoButtonSelect").setEnabled(false);

				// Configure View Content
				this.getView().byId("productsContainer2").setVisible(false);
			} else {

				// Car Image Busy indicator
				_oView.byId("carImageProductFinder").setVisible(false);
				_oView.byId("carImageVBox").setBusy(true);

				// Enable SelectBox
				this.getView().byId("searchField2").setSelectedItemId(0);
				this.getView().byId("searchField3").setSelectedItemId(0);
				this.getView().byId("searchField4").setSelectedItemId(0);

				// Set No Car Image
				this.decodeBase64ToImg();

				// Configure SelectBox
				this.getView().byId("searchField2").setEnabled(false);
				this.getView().byId("searchField3").setEnabled(false);
				this.getView().byId("searchField4").setEnabled(false);
				this.getView().byId("searchField4").setEnabled(false);
				this.getView().byId("oemInfoButtonSelect").setEnabled(false);

				// Configure View Content
				this.getView().byId("productsContainer2").setVisible(false);
			}
		},

		/**
		 * Function that handles the success case of the ajax call to get data from model Search
		 * @function onSearchMakeSuccess
		 */
		onSearchModelSuccess: function (oController, oData, oPiggyBack) {
			_fstTime = true;
			var oModelAux = new sap.ui.model.json.JSONModel();
			var oModel2 = new sap.ui.model.json.JSONModel();
			oModelAux.setData({
				modelData: oData
			});
			oModelAux.getData().modelData.Types.unshift({
				id: "0",
				result: oController.getResourceBundle().getText("PRDTSDTtypeTXT")
			});

			oModel2.setData(oModelAux.getData().modelData);
			oController.getView().setModel(oModel2, "vehicleType");

			oController.getView().byId("searchField3").setEnabled(true);
			oController.byId("searchField3").setBusy(false);
		},

		/**
		 * Function that handles the error case of the ajax call to get data from model Search
		 * @function onSearchMakeError
		 */
		onSearchModelError: function (oController, oError, oPiggyBack) {
			oController.byId("searchField3").setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRTypes"));
		},

		/**
		 * This function is called when the user selects the "MODEL" selectBox. Than call a service that fills the "TYPE" selectBox.
		 * @function onSearchModel
		 */
		onSearchModel: function () {
			// Checks Session time out
			this.checkSessionTimeout();
			var controller = this;
			var vehicleModelID = this.byId("searchField2").getSelectedKey();
			var vehicleYear = this.byId("searchYear").getSelectedKey();
			var xsURL;
			// Car Image Busy indicator
			_oView.byId("carImageProductFinder").setVisible(false);
			_oView.byId("carImageVBox").setBusy(true);

			if (vehicleModelID !== "0") {
				// Service Call
				this.byId("searchField3").setBusyIndicatorDelay(0);
				this.byId("searchField3").setBusy(true);

				if (vehicleYear === "0") {
					xsURL = "/products/productfinder/types/" + "?modelID=" + vehicleModelID;
				} else {
					xsURL = "/products/productfinder/types/" + "?modelID=" + vehicleModelID + "&year=" + vehicleYear;
				}
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onSearchModelSuccess, controller.onSearchModelError);

				// Set No Car Image
				this.decodeBase64ToImg();

				// Configure SelectBoxes
				this.getView().byId("searchField4").setEnabled(false);
				this.getView().byId("searchField3").setEnabled(false);
				this.getView().byId("oemInfoButtonSelect").setEnabled(false);
				this.getView().byId("searchField3").setSelectedItemId(0);
				this.getView().byId("searchField4").setSelectedItemId(0);

				// Configure View Content
				this.getView().byId("productsContainer2").setVisible(false);
			} else {
				// Configure SelectBoxes
				this.getView().byId("searchField3").setSelectedItemId(0);
				this.getView().byId("searchField4").setSelectedItemId(0);

				// Set No Car Image
				this.decodeBase64ToImg();

				// Enable SelectBox
				this.getView().byId("oemInfoButtonSelect").setEnabled(false);
				this.getView().byId("searchField3").setEnabled(false);
				this.getView().byId("searchField4").setEnabled(false);

				// Configure View Content
				this.getView().byId("productsContainer2").setVisible(false);
			}
		},

		/**
		 * Function that handles the success case of the ajax call to get data from model Search
		 * @function onSaveSubmitClaimSuccess
		 */
		onSearchTypeSuccess: function (oController, oData, oPiggyBack) {
			_fstTime = true;
			var vehicleType = oController.byId("searchField3").getSelectedKey();
			var oModelAux = new sap.ui.model.json.JSONModel();
			var oModel3 = new sap.ui.model.json.JSONModel();

			// Set Car Image
			_noImgUrl = oData.vehicleProperties.noImageUrl;
			oController.decodeBase64ToImg(oData.vehicleProperties.image);

			oModelAux.setData({
				modelData: oData
			});
			oModelAux.getData().modelData.Components.unshift({
				name: oController.getResourceBundle().getText("PRDTSDTproductCategoryTXT"),
				code: null,
				number: "0",
				capacities: [],
				uses: []
			});
			oModel3.setData(oModelAux.getData().modelData);
			oController.getView().setModel(oModel3, "productCategory");
			oController.byId("searchField4").setBusy(false);
			oController.populateProductsCard(vehicleType, "1000", "");
			oController.getView().byId("oemInfoButtonSelect").setEnabled(true);

		},

		/**
		 * Function that handles the error case of the ajax call to get data from model Search
		 * @function onSaveSubmitClaimError
		 */
		onSearchTypeError: function (oController, oError, oPiggyBack) {
			// Set No Car Image
			oController.decodeBase64ToImg();
			oController.byId("searchField4").setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRComponents"));
		},

		/**
		 * This function is called when the user selects the "TYPE" selectBox. Than call a service that fills the "PRODUCT CATEGORY" selectBox.
		 * @function onSearchType
		 */
		onSearchType: function () {
			// Checks Session time out
			this.checkSessionTimeout();
			var vehicleType = this.byId("searchField3").getSelectedKey();
			var language = sap.ui.getCore().getConfiguration().getLanguage();

			// Car Image Busy indicator
			_oView.byId("carImageProductFinder").setVisible(false);
			_oView.byId("carImageVBox").setBusy(true);

			if (vehicleType !== "0") {
				this.byId("searchField4").setBusyIndicatorDelay(0);
				this.byId("searchField4").setBusy(true);

				// Car Image Busy indicator
				_oView.byId("carImageProductFinder").setVisible(false);
				_oView.byId("carImageVBox").setBusy(true);

				// Update VehicleTypeOEM ID
				_vehicleTypeIDOEM = this.byId("searchField3").getSelectedKey();

				// Add the searched item to the search history
				var productsPageVehicleModel = sap.ui.getCore().getModel("productsPageVehicleInfo");
				var searchMakeSelectedName = this.byId("searchField1").getSelectedItem().getText();
				var searchModelSelectedName = this.byId("searchField2").getSelectedItem().getText();
				var searchTypeSelectedName = this.byId("searchField3").getSelectedItem().getText();
				var selectedvehicleType = productsPageVehicleModel.getData().selectedvehicleType;

				var resultText = selectedvehicleType + " - " + searchMakeSelectedName + " - " + searchModelSelectedName + " - " +
					searchTypeSelectedName;
				var o2AddToHistory = {
					id: _vehicleTypeIDOEM,
					result: resultText,
					lastHistory: true
				};

				// Add the searched item to the search history
				_oController.addToSearchHistory(o2AddToHistory);

				// Service Call
				var xsURL = "/products/productfinder/components/" + "?typeID=" + vehicleType + "&lang=" + language;

				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onSearchTypeSuccess, _oController.onSearchTypeError);

				this.getView().byId("searchField4").setEnabled(true);
				this.getView().byId("searchField4").setSelectedItemId(0);
				this.getView().byId("oemInfoButtonSelect").setEnabled(true);
			} else {
				// Set No Car Image
				this.decodeBase64ToImg();

				this.getView().byId("searchField4").setSelectedItemId(0);
				this.getView().byId("searchField4").setEnabled(false);
				this.getView().byId("productsContainer2").setVisible(false);
				this.getView().byId("oemInfoButtonSelect").setEnabled(false);
			}
		},

		/**
		 * This function is called when the user changes the "PRODUCT CATEGORY" selectBox. Than call a service populates Products Card for the respetive category.
		 * @function onSearchCategory
		 *
		 */
		onSearchCategory: function () {
			// Checks Session time out
			this.checkSessionTimeout();
			_oController.byId("productsContainer2").setBusy(true);
			var categoryID = null;

			if (_oModelProductsPageVehicleInfo.getProperty("/ID")) {
				categoryID = this.byId("searchField6").getSelectedKey();

			} else {
				categoryID = this.byId("searchField4").getSelectedKey();
			}

			var prevProducts = _oView.getModel("initialProductsData").getData();
			var filteredProds = {
				"components": []
			};
			if (categoryID === 0 || categoryID === "0") {
				filteredProds = prevProducts;
			} else {
				for (var i = 0; i < prevProducts.components.length; i++) {
					if (prevProducts.components[i].number === categoryID) {
						filteredProds.components.push(prevProducts.components[i]);
					}
				}
			}

			// Set the Card generated into the Core
			var ProductCard = {
				"productDetailInfo": []
			};
			var oModelProductCard = new sap.ui.model.json.JSONModel(ProductCard);
			var oPiggyBack = {
				"oModelProductCard": oModelProductCard
			};

			// clear the contents
			var scrollContainerProducts = _oController.getView().byId("productList");
			scrollContainerProducts.destroyItems();

			//repopulate
			_oController.onPopulateProductsCardSuccess(_oController, filteredProds, oPiggyBack);
		},

		/**
		 * Function that handles the success case of the ajax call to submit Claims
		 * @function onPopulateProductsCardSuccess
		 */
		onPopulateProductsCardSuccess: function (oController, oData, oPiggyBack) {
			var productCount = 0;
			for (var i = 0; i < oData.components.length; i++) {
				for (var j = 0; j < oData.components[i].uses.length; j++) {
					if (!oData.components[i].uses[j].products || oData.components[i].uses[j].products.length === 0) {
						var sMake, sModel;
						if (sap.ui.getCore().getModel("productsPageVehicleInfo").getProperty("/navigationMode") === "fromSearch") {
							var aVehicle = sap.ui.getCore().getModel("productsPageVehicleInfo").getProperty("/result").split(" - ");
							sMake = aVehicle[1];
							sModel = aVehicle[2];
						} else if (_oController.byId("searchField1")._oSelectionOnFocus && _oController.byId("searchField2")._oSelectionOnFocus) {
							sMake = _oController.byId("searchField1")._oSelectionOnFocus.getText();
							sModel = _oController.byId("searchField2")._oSelectionOnFocus.getText();
						}
						//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
						_oController.GTMDataLayer('noproductsfound',
							'No Products Found',
							sMake + '-Make | ' + sModel + '-Model',
							oData.components[i].name + ' -Not Found Category'
						);
					}
					for (var k = 0; k < oData.components[i].uses[j].products.length; k++) {
						oData.components[i].uses[j].products[k].productCount = productCount;
						productCount = productCount + 1;
					}
				}
			}

			// First cycle to the different Product Categories
			if (_fstTime) {
				var initialModel = new JSONModel(oData);
				_oView.setModel(initialModel, "initialProductsData");
				_fstTime = false;
			}

			_oView.setModel(new JSONModel(oData.components), "productList");
			oController.byId("productsContainer2").setBusy(false);
			oController.byId("productList").setVisible(true);
			oController.byId("searchField4").setEnabled(true);
		},

		/**
		 * Function that handles the error case of the ajax call to submit Claims
		 * @function onPopulateProductsCardError
		 */
		onPopulateProductsCardError: function (oController, oError, oPiggyBack) {
			oController.byId("productsContainer2").setBusy(false);
			oController.byId("productList").setVisible(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRProductList"));
		},

		/**
		 * This function is called when to generate the Product Cards.
		 * @function populateProductsCard
		 * @param {string} typeID - receives the vehicle ID
		 * @param {string} salesOrg - receives the SalesOrg number
		 * @param {string} componentNumber - receives the ComponentNumber ID
		 */
		populateProductsCard: function (typeID, salesOrg, componentNumber) {
			var controller = this;
			var language = sap.ui.getCore().getConfiguration().getLanguage();

			// Set the Busy Indicator on the Card Container
			this.byId("productsContainer2").setBusyIndicatorDelay(0);
			_oController.byId("productsContainer2").setBusy(true);
			_oController.byId("productList").setVisible(false);
			controller.byId("productsContainer2").setVisible(true);
			this.byId("searchField4").setEnabled(false);

			// Set the Card generated into the Core
			var ProductCard = {
				"productDetailInfo": []
			};
			var oModelProductCard = new sap.ui.model.json.JSONModel(ProductCard);
			var oPiggyBack = {
				"oModelProductCard": oModelProductCard
			};

			// Service Call
			var xsURL = "/products/productfinder/list/" + "?typeID=" + typeID + "&salesOrg=" + salesOrg + "&componentNumber=" + componentNumber +
				"&lang=" + language;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onPopulateProductsCardSuccess, controller.onPopulateProductsCardError,
				undefined, oPiggyBack);
		},

		productDetails: function (oEvent) {
			// Checks Session time out
			_oController.checkSessionTimeout();

			var product = oEvent.getSource().data("product");

			// save the last page navigated
			var oModelVehicleName = sap.ui.getCore().getModel("productsPageVehicleInfo");
			oModelVehicleName.setProperty("/navigationMode", "fromProductCategory");
			var productCardIDDetails = oEvent.getSource().data("product").productCount;

			//GTM
			_oController.GTMDataLayer('searchrsltclick',
				'Product Finder -Result click',
				oEvent.getSource().data("product").name,
				"#/" + _oController.getRouter().getRoute("productDetails").getPattern().replace(
					"{productID}", productCardIDDetails)
			);

			ga('set', 'page', '/ProductDetails');
			ga('send', 'pageview');
			_oController.getRouter().navTo("productDetails", {
				productID: productCardIDDetails
			});

			sap.ui.getCore().getEventBus().publish("productFinder", "productDetails", product);
		},

		/**
		 * Get the user back to the Product Finder page
		 * @function onNavBack
		 */
		onNavBack: function () {
			// Checks Session time out
			this.checkSessionTimeout();
			ga('set', 'page', '/ProductFinder');
			ga('send', 'pageview');
			this.getRouter().navTo("productFinder", {});
		},

		/**
		 * This function open a new Tab Page for the promotions valvoline page in Products Section.
		 * @function onPressPromotions
		 */
		onPressPromotions: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();
			var promotionsURL = "http://www.valvolinecatalog.com/";
			sap.m.URLHelper.redirect(promotionsURL, true);

			//GTM
			var url = window.location;
			_oController.GTMDataLayer('promobnnerclck',
				'BannerClick',
				url.hash,
				promotionsURL
			);

		},

		/**
		 * This function open a new Tab Page for the filter and wiper information valvoline page in Products Section.
		 * @function onPressFilterInfo
		 */
		onPressFilterInfo: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();

			var promotionsURL = _oController.getRouter().getURL("productFilterWiperInfo");
			sap.m.URLHelper.redirect("#" + promotionsURL, true);

			//GTM
			var url = window.location;
			_oController.GTMDataLayer('promobnnerclck',
				'BannerClick',
				url.hash,
				promotionsURL
			);

		},

		/**
		 * Open a new OEM Recommendation page
		 */
		onOEMRecommendation: function (oEvent) {
			// Checks Session time out
			_oController.checkSessionTimeout();
			var sUrl = this.getRouter().getURL("productOEM", {
				vehicleID: _vehicleTypeIDOEM,
				language: sap.ui.getCore().getConfiguration().getLanguage()
			});
			sap.m.URLHelper.redirect("#" + sUrl, true);

			//GTM
			_oController.GTMDataLayer('searchrsltclick',
				'Product Finder -Result click',
				oEvent.getSource().getText(),
				"#/" + sUrl.replace('/' + sap.ui.getCore().getConfiguration().getLanguage(), '')
			);
		},
		onPressVPSGuarantee: function (oEvent) {
			// Checks Session time out
			_oController.checkSessionTimeout();

			var sURL = this.getRouter().getURL("productvps");
			sap.m.URLHelper.redirect("#" + sURL, true);

			//GTM
			_oController.GTMDataLayer('searchrsltclick',
				'Product Finder -Result click',
				oEvent.getSource().getText(),
				"#/" + sURL
			);
		},

		/**
		 * This function navigates the view to Hybris Shop Now Page.
		 * @function navToShopNow
		 */
		navToShopNow: function (oEvent) {
			// Checks Session time out
			_oController.checkSessionTimeout();
			var product = oEvent.getSource().data("product");
			// save the last page navigated
			var sProductUrl = product.shopUrl;
			if (sProductUrl) {
				_oController.nav2Hybris(sProductUrl);
			}

			//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
			_oController.GTMDataLayer('shopNowEvent',
				'Shopping',
				'Product Recommendation -ShopNow -click',
				product.name
			);
		},

		/** Formatters **/
		formatterCapacityFormated: function (sCapacity) {
			if (sCapacity === "") {
				return _oController.getResourceBundle().getText("PRDTSDTcapacityNotAvailableTXT");
			} else {
				return _oController.getResourceBundle().getText("PRDTSDTcapacityDetailsTXT") + " " + sCapacity;
			}
		},

		formatterMediaUrl: function (sMediaURL) {
			if (sMediaURL) {
				return sMediaURL;
			} else {
				return "resources/img/NoImageAvailable.png";
			}
		},

		formatterVisibility: function (sValue) {
			return Boolean(sValue);
		},

		formatterNoProductsinCategoryVisibility: function (aUses) {
			var bHasProducts = false;

			if (!aUses || aUses.length === 0) {
				return true;
			}

			for (var i = 0; i < aUses.length; i++) {
				if (aUses[i].products && aUses[i].products.length !== 0) {
					bHasProducts = true;
				}
			}

			return !bHasProducts;
		},

		formatterdisableHoverLink: function (sValue) {
			if (sValue) {
				return "pageSubtitle valvolineLink";
			} else {
				return "pageSubtitle valvolineLink noHoverFooter";
			}
		},

		formatterProductsListVisibility: function (aProducts) {
			return Boolean(aProducts && aProducts.length > 0);
		},

		formatterShopNowVisibility: function (aValue) {
			return Boolean(_oController.checkDASHLayoutAccess("SHOP_BUTTON"));
		},

		/**
		 * This function decode base64 to img.
		 * @function decodeBase64ToImg
		 */
		decodeBase64ToImg: function (sValue) {
			var base64Img = sValue && sValue.trim();
			if (base64Img && base64Img.substring(0, 2) !== "{}" && base64Img.substring(0, 4) !== "data") {
				_oView.byId("carImageProductFinder").setSrc("data:image/png;base64," + base64Img);
			} else if (_noImgUrl) {
				_oView.byId("carImageProductFinder").setSrc(_noImgUrl);
			}
			_oView.byId("carImageProductFinder").setVisible(true);
			_oView.byId("carImageVBox").setBusy(false);
		}
	});
});