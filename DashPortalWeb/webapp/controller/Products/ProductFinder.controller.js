sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function (Base) {
	"use strict";
	var _oController, _oView;
	var buttons;
	var _applyButtonState;
	var _categoryID;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Products.ProductFinder", {
		/** @module Products Finder */
		/**
		 * This function initializes the controller and configures the search field and the promotion box.
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oController = this;
			_oView = this.getView();

			var oRouter = this.getRouter();
			oRouter.getRoute("productFinder").attachPatternMatched(this._onObjectMatched, this);

			// Makes the text clickable
			_oController.byId("promotionBanner").attachBrowserEvent("click", this.onPressPromotions);
			_oController.byId("promotionBanner").addStyleClass("cursorPointer");

			_oController.byId("promotionBannerFilterInfo").attachBrowserEvent("click", this.onPressFilterInfo);
			_oController.byId("promotionBannerFilterInfo").addStyleClass("cursorPointer");

			//Initialize Storage
			jQuery.sap.require('jquery.sap.storage');

			// Configure the vehicle category buttons Category Buttons Model
			buttons = [{
				id: "PassengerCars",
				categoryId: 1
			}, {
				id: "HeavyDutyTrucksAndBuses",
				categoryId: 3
			}, {
				id: "Agriculture",
				categoryId: 5
			}, {
				id: "ConstructionAndMining",
				categoryId: 10
			}, {
				id: "MotorcyclesATVsAndSnowmobiles",
				categoryId: 4
			}, {
				id: "ClassicCars",
				categoryId: 32
			}, {
				id: "LeisureMarine",
				categoryId: 31
			}];
			this.onConfigVehicleCategoryButton();
		},

		onAfterRendering: function () {
			/* Set the menu as active if the app is reloaded */
			$(".js-products-sidenav").addClass("sidenav__item--active");
			//Hide the suggestion list
			_oController.byId("searchField").suggest(false);
			//Remove the focus of the input
			$(':focus').blur();
		},

		/**
		 * Function runs everytime the page is opened
		 * @function _onObjectMatchedThis
		 */
		_onObjectMatched: function () {
			_oController = this;
			//Added to fix iOS issue when using back to product finder after chosing an item of the search field
			_oController.byId("searchField").setValue("");

			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page authorization and check layout
			if (!this.checkDASHPageAuthorization("PRODUCTFINDER_PAGE")) {
				return;
			}

			//get user info and direct account
			_oController.getUserInformation(_oController);

			this.headerIconAccess();

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "productFinder"
			});

			/* Set the menu as active if the app is reloaded */
			$(".js-products-sidenav").addClass("sidenav__item--active");

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			// Configure the Layout
			this.onClearInputFields();
			this.onPlaceHolderSelectBox();
			this.onConfigVehicleCategoryButtonUnpress();

			//Set Data for the suggest based on the search history
			var historyAux = this.retrieveSearchHistory();
			var history = [];
			// Filter history by searchText
			if (historyAux !== undefined && historyAux !== null && historyAux.length > 0) {
				for (var i = historyAux.length - 1; i >= 0; i--) {
					history.push(historyAux[i]);
				}
			}

			if (history.length === 0) {
				_oController.byId("searchField").suggest(false);
				_oController.byId("searchField").destroySuggestionItems();
			} else {
				var oModel = new sap.ui.model.json.JSONModel();
				oModel.setData(history);
				this.getView().setModel(oModel, "vehicleBrand");
				// Forces the update in SuggestionItems
				_oController.byId("searchField").addSuggestionItem();

			}
			//Hide the suggestion list
			_oController.byId("searchField").suggest(false);
			// Set the flag for other makes option pressed
			this.bOtherMakesPressed = false;
			//Remove the focus of the input
			$(':focus').blur();
		},

		/**
		 * Logic to set the CSS class to the Search suggestions 
		 * @function storeSearchHistory
		 */
		formatItem: function (last) {
			if (last) {
				return "searchHistoryFromStorage";
			} else {
				return null;
			}
		},

		/**
		 * Logic to set the History icon to the Search suggestions 
		 * @function formatItemIcon
		 */
		formatItemIcon: function (history) {
			if (history) {
				return "sap-icon://history";
			} else {
				return null;
			}
		},

		/**
		 * This function is called when always when the page is open and verifies the user authorization, and destroys and disable the searchfield suggestion items.
		 * @function onClearInputFields
		 */
		onClearInputFields: function () {
			// Resets the inputSearchField
			this.getView().byId("searchField").clear();
			if (this.onGetDashDevice() === "NARROW") {
				this.byId("searchField").destroySuggestionItems();
			} else {
				this.getView().byId("searchField").suggest(false);
				this.byId("searchField").destroySuggestionItems();
			}

			// Config Selects Boxes
			this.getView().byId("searchYear").setEnabled(false);
			this.getView().byId("searchField1").setEnabled(false);
			this.getView().byId("searchField2").setEnabled(false);
			this.getView().byId("searchField3").setEnabled(false);
			this.getView().byId("applyButton").setEnabled(false);
		},

		/**
		 * This function set the selection Menu IDs, Imagens and Names for the Selection Menu.
		 * @function onConfigVehicleCategoryButton
		 */
		onConfigVehicleCategoryButton: function () {
			var controller = this;
			for (var i = 0; i < buttons.length; i++) {
				this.getView().byId(buttons[i].id).attachBrowserEvent("click", controller.onCategoryButtonPress);
			}
		},

		/**
		 * This function remove all Vehicle Category Buttons pressed classes.
		 * @function onConfigVehicleCategoryButtonUnpress
		 */
		onCategoryButtonPress: function (oEvent) {
			// Button Id Pressed		
			var buttonPressedId = oEvent.currentTarget.id;
			buttonPressedId = buttonPressedId.split("--")[1];
			// Set the flag for other makes press
			this.bOtherMakesPressed = false;
			// Unpress all buttons
			_oController.onConfigVehicleCategoryButtonUnpress();

			// Press the selected button
			_oController.byId(buttonPressedId).addStyleClass("productCategoryButtonPressed");
			_oController.byId(buttonPressedId + "-Icon").addStyleClass("iconVehicleProductCategoryPressed");
			_oController.byId(buttonPressedId + "-Text").addStyleClass("productCategoryTextPressed");

			// Sets the correct Category Id for the Pressed Button
			for (var i = 0; i < buttons.length; i++) {
				if (buttons[i].id === buttonPressedId) {
					_oController.onCategorySelect(buttons[i].categoryId);
				}
			}

			//GTM
			_oController.GTMDataLayer('vehiclecatgclick',
				'Product Finder- Category',
				oEvent.currentTarget.id.split("--")[1],
				sap.ui.getCore().getModel("oAccounts").getData().uuid
			);

		},

		/**
		 * This function remove all Vehicle Category Buttons pressed classes.
		 * @function onConfigVehicleCategoryButtonUnpress
		 */
		onConfigVehicleCategoryButtonUnpress: function () {
			for (var i = 0; i < buttons.length; i++) {
				this.byId(buttons[i].id).removeStyleClass("productCategoryButtonPressed");
				this.byId(buttons[i].id + "-Icon").removeStyleClass("iconVehicleProductCategoryPressed");
				this.byId(buttons[i].id + "-Text").removeStyleClass("productCategoryTextPressed");
			}
		},

		/**
		 * This function set a dummy model in all selectBoxes.
		 * @function onPlaceHolderSelectBox
		 */
		onPlaceHolderSelectBox: function () {
			// Set Select Box Year
			var oModelDummy = new sap.ui.model.json.JSONModel();
			var placeHolder = {
				"Years": [{
					id: "0",
					result: this.getResourceBundle().getText("PRDTSDTyearTXT")
				}]
			};
			oModelDummy.setData(placeHolder);
			this.getView().setModel(oModelDummy, "vehicleYear");

			// Set Select Box Model
			oModelDummy = new sap.ui.model.json.JSONModel();
			placeHolder = {
				"Makes": [{
					id: "0",
					result: this.getResourceBundle().getText("PRDTSDTmakeTXT")
				}]
			};
			oModelDummy.setData(placeHolder);
			this.getView().setModel(oModelDummy, "vehicleMake");

			// Set Select Box Make
			oModelDummy = new sap.ui.model.json.JSONModel();
			placeHolder = {
				"Models": [{
					id: "0",
					result: this.getResourceBundle().getText("PRDTSDTmodelTXT")
				}]
			};
			oModelDummy.setData(placeHolder);
			this.getView().setModel(oModelDummy, "vehicleModel");

			// Set Select Box Model
			oModelDummy = new sap.ui.model.json.JSONModel();
			placeHolder = {
				"Types": [{
					id: "0",
					result: this.getResourceBundle().getText("PRDTSDTtypeTXT")
				}]
			};
			oModelDummy.setData(placeHolder);
			this.getView().setModel(oModelDummy, "vehicleType");
		},

		/**
		 * Function that handles the success case of the ajax call that loads the data based on the free search field
		 * @function onProductFreeSearchSuccess
		 */
		onProductFreeSearchSuccess: function (oController, oData, oPiggyBack) {
			var i;
			var comboInput = oController.getView().byId("searchField").getValue();
			var historyAux = oController.retrieveSearchHistory();
			var history = [];
			var oModel = new sap.ui.model.json.JSONModel();

			// Filter history by searchText
			if (historyAux !== undefined && historyAux !== null) {
				for (i = historyAux.length - 1; i >= 0; i--) {
					if (historyAux[i].result.toLocaleUpperCase().indexOf(oPiggyBack.comboInputAux.toLocaleUpperCase()) !== -1) {
						history.push(historyAux[i]);
					}
				}
			}

			if (oData.SearchTypes[0].id !== "noProducts" && history.length > 0) {
				//Concat the History array with the data retrieved from the service
				var rawToPresent = history.concat(oData.SearchTypes);

				// Remove duplicate elements
				var seen = {};
				var toPresent = [];
				var j = 0;
				for (i = 0; i < rawToPresent.length; i++) {
					var item = rawToPresent[i];
					if (seen[item.id] !== 1) {
						seen[item.id] = 1;
						toPresent[j++] = item;
					}
				}
			} else if (history.length > 0 && oData.SearchTypes[0].id === "noProducts") {
				toPresent = history;
			} else {
				toPresent = oData.SearchTypes;
			}

			// Checks if the user input is greater than 3 chars && userInput is equal to what was searched && dataReceive is bigger than 0
			if (comboInput.length >= 3 && comboInput === oPiggyBack.comboInputAux && toPresent.length !== 0) {
				oModel.setData(toPresent);
				oController.getView().setModel(oModel, "vehicleBrand");
				oController.byId("busyLoading").setBusy(false);
				// Forces the update in SuggestionItems
				oController.byId("searchField").addSuggestionItem();
				oController.getView().byId("searchField").suggest(true);
			}

			if (toPresent.length === 0) {
				oController.byId("busyLoading").setBusy(false);
			}
		},

		/**
		 * Function that handles the error case of the ajax call that loads the data based on the free search field
		 * @function onProductFreeSearchError
		 */
		onProductFreeSearchError: function (oController, oError, oPiggyBack) {
			oController.byId("busyLoading").setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText,
				oError.responseText, oController.getResourceBundle().getText("ERRProductFinderNoData"));
		},

		/**
		 * This function is called when always when the user types in the searchfield. The written string is send to the java service and returns the results.
		 * @function onLiveChange
		 * @param {Object} event - Input text
		 */
		onLiveChange: function (event) {
			var controller = this;
			var clearButtonPressed = event.getParameter("clearButtonPressed");
			var searchField = controller.getView().byId("searchField");
			var comboInputAux;
			var comboInput = searchField.getValue();
			var language = sap.ui.getCore().getConfiguration().getLanguage();
			var comboInputLength;
			var xsURL;

			// If the clear button is pressed, doesnt search
			if (!clearButtonPressed) {
				// verification of the comboInput Length
				if (comboInput) {
					comboInputLength = comboInput.length;
				} else {
					comboInputLength = 0;
				}
				if (comboInputLength < 3) {
					var historyAux = controller.retrieveSearchHistory();
					var history = [];

					// Filter history by searchText
					if (historyAux !== undefined && historyAux !== null) {
						for (var i = historyAux.length - 1; i >= 0; i--) {
							if (historyAux[i].result.toLocaleUpperCase().indexOf(comboInput.toLocaleUpperCase()) !== -1) {
								history.push(historyAux[i]);
							}
						}
					}

					if (history.length === 0) {
						this.byId("searchField").suggest(false);
						this.byId("searchField").destroySuggestionItems();
					} else {
						controller.byId("busyLoading").setBusyIndicatorDelay(0);
						controller.byId("busyLoading").setBusy(true);
						var oModel = new sap.ui.model.json.JSONModel();
						oModel.setData(history);
						controller.getView().setModel(oModel, "vehicleBrand");
						// Forces the update in SuggestionItems
						controller.byId("searchField").addSuggestionItem();
						controller.getView().byId("searchField").suggest(true);
					}
					this.byId("busyLoading").setBusy(false);
				} else {
					jQuery.sap.delayedCall(150, this, function () {
						comboInputAux = searchField.getValue();
						if (comboInput === comboInputAux) {
							controller.byId("busyLoading").setBusyIndicatorDelay(0);
							controller.byId("busyLoading").setBusy(true);
							var oPiggyBack = {
								"comboInputAux": comboInputAux
							};
							xsURL = "/products/productfinder/searchTypes/" + "?searchText=" + comboInputAux + "&lang=" + language;
							//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
							controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onProductFreeSearchSuccess, controller.onProductFreeSearchError,
								undefined, oPiggyBack);
						}
					});
				}
			}
		},

		/**
		 * Sets the suggestion to true when the user click in the searchfield
		 */
		onSuggest: function () {
			this.getView().byId("searchField").suggest(true);
		},

		/**
		 * This function is called when the user selects on an suggested Item, and navigates to the Product Category if there is any valid result. The navigation type, the vehicle ID, the vehicle Name, and the Category ID is saved in the Core.
		 * @function onSearch
		 * @param {string} event - Item selected
		 */
		onSearch: function (event) {
			// Checks Session time out
			this.checkSessionTimeout();
			var clearButtonPressed = event.getParameter("clearButtonPressed");
			var comboInput = event.getParameter("query");
			var controller = this;

			if (!clearButtonPressed) {
				if (comboInput !== undefined && event.getParameter("suggestionItem") !== undefined && event.getParameter("suggestionItem").getKey() !==
					"noProducts") {
					// Get the Vehicle Name and ID
					var vehicleSelectedID = event.getParameter("suggestionItem").getKey();
					var vehicleSelectedName = event.getParameter("suggestionItem").getText();

					var o2AddToHistory = {
						id: vehicleSelectedID,
						result: vehicleSelectedName,
						lastHistory: true
					};

					// Add the searched item to the search history
					_oController.addToSearchHistory(o2AddToHistory);

					var oModelvehicleSelectedInfo = new sap.ui.model.json.JSONModel();
					var myJSON = {
						"result": vehicleSelectedName,
						"ID": vehicleSelectedID,
						"navigationMode": "fromSearch",
						"searchMakeSelecedID": null,
						"searchModelSelecedID": null,
						"searchTypeSelecedID": null
					};

					//GTM
					_oController.GTMDataLayer('prdctsearch',
						'Product -Search',
						comboInput,
						sap.ui.getCore().getModel("oAccounts").getData().uuid
					);

					oModelvehicleSelectedInfo.setData(myJSON);
					sap.ui.getCore().setModel(oModelvehicleSelectedInfo, "productsPageVehicleInfo");
					ga('set', 'page', '/ProductsCategory');
					ga('send', 'pageview');
					//Hide the suggestion list
					_oController.byId("searchField").suggest(false);
					this.getRouter().navTo("productsCategory", {});
				} else {
					// If there is no products and the user clicks on the no products field
					if (event.getParameter("suggestionItem")) {
						if (event.getParameter("suggestionItem").getKey() === "noProducts") {
							this.getView().byId("searchField").setValue("");
							controller.byId("searchField").destroySuggestionItems();
						}
					}
				}
			}
		},

		/**
		 * This function is called when the user select the "APPLY" Button. Than call a service populates all the Products Card.
		 * @function onApply
		 */
		onApply: function () {
			// Checks Session time out
			this.checkSessionTimeout();
			var oModelvehicleSelectedInfo = new sap.ui.model.json.JSONModel();
			var searchTypeSelectedID = this.byId("searchField3").getSelectedKey();
			var searchMakeSelectedName = this.byId("searchField1").getSelectedItem().getText();
			var searchModelSelectedName = this.byId("searchField2").getSelectedItem().getText();
			var searchTypeSelectedName = this.byId("searchField3").getSelectedItem().getText();
			var selectedvehicleType = sap.ui.getCore().byId($('.productCategoryButtonPressed')[0].id).getItems()[1].getText();
			var myJSON = {
				"result": null,
				"ID": null,
				"navigationMode": "fromSelection",
				"searchCategoryID": _categoryID,
				"searchYearSelecedID": this.byId("searchYear").getSelectedKey(),
				"searchMakeSelecedID": this.byId("searchField1").getSelectedKey(),
				"searchModelSelecedID": this.byId("searchField2").getSelectedKey(),
				"searchTypeSelecedID": searchTypeSelectedID,
				"searchMakeSelecedName": searchMakeSelectedName,
				"searchModelSelecedName": searchModelSelectedName,
				"searchTypeSelecedName": searchTypeSelectedName,
				"selectedvehicleType": selectedvehicleType,
				"otherMakesPressed": this.bOtherMakesPressed
			};
			// Add the searched item to the search history
			var resultText = selectedvehicleType + " - " + searchMakeSelectedName + " - " + searchModelSelectedName + " - " +
				searchTypeSelectedName;
			var o2AddToHistory = {
				id: searchTypeSelectedID,
				result: resultText,
				lastHistory: true
			};

			// Add the searched item to the search history
			_oController.addToSearchHistory(o2AddToHistory);

			oModelvehicleSelectedInfo.setData(myJSON);
			sap.ui.getCore().setModel(oModelvehicleSelectedInfo, "productsPageVehicleInfo");
			//Hide the suggestion list
			_oController.byId("searchField").suggest(false);
			this.getRouter().navTo("productsCategory", {});

			//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
			if (_oController.byId("searchYear")._oSelectionOnFocus && _oController.byId("searchYear")._oSelectionOnFocus.getText() !== "Year") {
				_oController.GTMDataLayer('prdctfinder',
					'Product Finder-Year',
					_oController.byId("searchYear")._oSelectionOnFocus.getText() + '-Year',
					_oController.byId("searchField1")._oSelectionOnFocus.getText() + '-Make'
				);
			}

			//GTM
			_oController.GTMDataLayer('prdctfinder',
				'Product Finder-Make',
				_oController.byId("searchField1")._oSelectionOnFocus.getText() + '-Make',
				_oController.byId("searchField2")._oSelectionOnFocus.getText() + '-Model'
			);
			_oController.GTMDataLayer('prdctfinder',
				'Product Finder-Model',
				_oController.byId("searchField2")._oSelectionOnFocus.getText() + '-Model',
				_oController.byId("searchField1")._oSelectionOnFocus.getText() + '-Make'
			);
			_oController.GTMDataLayer('prdctfinder',
				'Product Finder-Type',
				_oController.byId("searchField3")._oSelectionOnFocus.getText() + '-Type',
				_oController.byId("searchField1")._oSelectionOnFocus.getText() + '-Make'
			);
			// _oController.GTMDataLayer('noproductsfound',
			// 	'No Products Found',
			// 	_oController.byId("searchField1")._oSelectionOnFocus.getText() + '-Make | ' + _oController.byId(
			// 		"searchField2")._oSelectionOnFocus.getText() + '-Model',
			// 	sap.ui.getCore().getModel("oAccounts").getData().uuid
			// );

			ga('set', 'page', '/ProductsCategory');
			ga('send', 'pageview');
		},

		setYearsbyCategorySuccess: function (oController, oData, oPiggyBack) {
			var oModel = new sap.ui.model.json.JSONModel();
			var endYear = oData[0].value.substr(0, 4);
			var beginYear = oData[0].value.substr(5, 4);
			var Years = {};
			var range = [];

			if (!beginYear) {
				beginYear = new Date().getFullYear();
			}

			range.push({
				id: "0",
				result: oController.getResourceBundle().getText("PRDTSDTyearTXT")
			});

			for (var i = beginYear; i >= endYear; i--) {
				range.push({
					id: i,
					result: i
				});
			}

			Years.Years = range;
			oModel.setData(Years);
			oModel.setSizeLimit(200);
			sap.ui.getCore().setModel(oModel, "vehicleYear");
			oController.getView().setModel(oModel, "vehicleYear");
		},

		setYearsbyCategoryError: function (oController, oError, oPiggyBack) {
			oController.byId("searchYear").setBusy(false);
			oController.byId("searchField1").setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRYears"));
		},

		/**
		 * This function is called to set the Years selection after the user selects a Category.
		 * @function setYearsbyCategory
		 */
		setYearsbyCategory: function (categoryID) {
			var nameID = categoryID;
			var groupID = "ProdFinderYearSelection";

			_oController.getGlobalParameter(nameID, groupID, _oController.setYearsbyCategorySuccess, _oController.setYearsbyCategoryError);
		},

		/**
		 * Function that handles the success case of the ajax call to get the Category
		 * @function onCategorySelectSuccess
		 */
		onCategorySelectSuccess: function (oController, oData, oPiggyBack) {
			if (oPiggyBack.categoryID === _categoryID) {
				var oModelAux = new sap.ui.model.json.JSONModel();
				var oModel = new sap.ui.model.json.JSONModel();

				oController.byId("searchYear").setBusy(false);
				oController.getView().byId("searchYear").setEnabled(true);
				oController.byId("searchField1").setBusy(false);
				oController.getView().byId("searchField1").setEnabled(true);

				oModelAux.setData({
					modelData: oData
				});

				oModelAux.getData().modelData.usaMakes.unshift({
					id: "0",
					result: oController.getResourceBundle().getText("PRDTSDTmakeTXT"),
					image: null
				});
				if (oData.nonUsaMakes && oData.nonUsaMakes.length > 0) {
					oModelAux.getData().modelData.usaMakes.push({
						id: "1",
						result: oController.getResourceBundle().getText("PRDTSDTnonusamakeTXT"),
						image: null
					});
				}
				oModelAux.getData().modelData.Makes = oModelAux.getData().modelData.usaMakes;
				oModel.setData(oModelAux.getData().modelData);
				oModel.setSizeLimit(1000);
				sap.ui.getCore().setModel(oModel, "vehicleMake");
				oController.getView().setModel(oModel, "vehicleMake");
			}
		},

		/**
		 * Function that handles the error case of the ajax call to get the Category
		 * @function onCategorySelectError
		 */
		onCategorySelectError: function (oController, oError, oPiggyBack) {
			oController.byId("searchYear").setBusy(false);
			oController.byId("searchField1").setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRMakes"));
		},

		/**
		 * This function calls the service to load the information in the select boxes.
		 * @function onCategorySelect
		 * @param {string} categoryID - The category selected by the user
		 */
		onCategorySelect: function (categoryID) {
			var controller = this;
			_categoryID = categoryID;

			// Config
			this.getView().byId("searchYear").setEnabled(true);
			this.getView().byId("searchField1").setEnabled(true);
			this.getView().byId("searchField2").setEnabled(false);
			this.getView().byId("searchField3").setEnabled(false);
			this.getView().byId("applyButton").setEnabled(false);

			// Set the flag for other makes
			this.bOtherMakesPressed = false;

			// Enable SelectBox
			this.getView().byId("searchYear").setSelectedItemId(0);
			this.getView().byId("searchField1").setSelectedItemId(0);
			this.getView().byId("searchField2").setSelectedItemId(0);
			this.getView().byId("searchField3").setSelectedItemId(0);

			// Show Busy Indicator in the SearchField
			this.byId("searchYear").setBusyIndicatorDelay(0);
			this.byId("searchYear").setBusy(true);
			this.byId("searchField1").setBusyIndicatorDelay(0);
			this.byId("searchField1").setBusy(true);

			// Set Category Years from Global Parameters
			controller.setYearsbyCategory(categoryID);

			//Piggyback
			var oPiggyBack = {
				"categoryID": categoryID
			};

			// Service Call
			var xsURL = "/products/productfinder/makes/" + "?categoryID=" + categoryID + "&getImage=small";

			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onCategorySelectSuccess, controller.onCategorySelectError,
				undefined, oPiggyBack);
		},

		/**
		 * Function that handles the success case of the ajax call to get the Year
		 * @function onSearchYearSuccess
		 */
		onSearchYearSuccess: function (oController, oData, oPiggyBack) {
			if (oPiggyBack.year === oController.getView().byId("searchYear").getSelectedKey()) {
				var oModelAux = new sap.ui.model.json.JSONModel();
				var oModel = new sap.ui.model.json.JSONModel();
				oController.byId("searchField1").setBusy(false);
				oController.getView().byId("searchField1").setEnabled(true);

				oModelAux.setData({
					modelData: oData
				});

				oModelAux.getData().modelData.usaMakes.unshift({
					id: "0",
					result: oController.getResourceBundle().getText("PRDTSDTmakeTXT"),
					image: null
				});
				if (oData.nonUsaMakes && oData.nonUsaMakes.length > 0) {
					oModelAux.getData().modelData.usaMakes.push({
						id: "1",
						result: oController.getResourceBundle().getText("PRDTSDTnonusamakeTXT"),
						image: null
					});
				}
				oModelAux.getData().modelData.Makes = oModelAux.getData().modelData.usaMakes;
				oModel.setData(oModelAux.getData().modelData);
				oModel.setSizeLimit(1000);
				sap.ui.getCore().setModel(oModel, "vehicleMake");
				oController.getView().setModel(oModel, "vehicleMake");

				/* Disable Makes selection if no makes were returned */
				if (!oModel.getData().Makes[1]) {
					oController.getView().byId("searchField1").setEnabled(false);
				}
			}
		},

		/**
		 * Function that handles the error case of the ajax call to get the Year
		 * @function onSearchYearError
		 */
		onSearchYearError: function (oController, oError, oPiggyBack) {
			oController.byId("searchYear").setBusy(false);
			oController.byId("searchField1").setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRMakes"));
		},

		/**
		 * This function calls the service to load the information in the select boxes.
		 * @function onSearchYear
		 */
		onSearchYear: function () {
			// Checks Session time out
			this.checkSessionTimeout();
			var controller = this;
			var vehicleYear = this.byId("searchYear").getSelectedKey();
			var categoryID = _categoryID;
			var language = sap.ui.getCore().getConfiguration().getLanguage();
			var xsURL;

			//Piggyback
			var oPiggyBack = {
				"year": vehicleYear
			};
			// Set the flag for other makes
			this.bOtherMakesPressed = false;
			if (vehicleYear !== "0") {
				// Service Call
				this.byId("searchField1").setBusyIndicatorDelay(0);
				this.byId("searchField1").setBusy(true);
				xsURL = "/products/productfinder/makes/" + "?categoryID=" + categoryID + "&year=" + vehicleYear + "&getImage=small" + "&lang=" +
					language;
				controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onSearchYearSuccess, controller.onSearchYearError,
					undefined, oPiggyBack);
				// Set first item on the SelectBoxes
				controller.getView().byId("searchField1").setSelectedItemId(0);
				controller.getView().byId("searchField2").setSelectedItemId(0);
				controller.getView().byId("searchField3").setSelectedItemId(0);

				// Enable SelectBox
				controller.getView().byId("searchField2").setEnabled(false);
				controller.getView().byId("searchField3").setEnabled(false);

				// Configure View Content
				controller.getView().byId("applyButton").setVisible(true);
				controller.getView().byId("applyButton").setEnabled(false);
			} else {
				this.byId("searchField1").setBusyIndicatorDelay(0);
				this.byId("searchField1").setBusy(true);
				xsURL = "/products/productfinder/makes/" + "?categoryID=" + categoryID + "&getImage=small";
				controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onSearchYearSuccess, controller.onSearchYearError,
					undefined, oPiggyBack);
				// Configure ApplyButton
				this.getView().byId("applyButton").setVisible(true);
				this.getView().byId("applyButton").setEnabled(false);

				// Enable SelectBox
				this.getView().byId("searchField1").setSelectedItemId(0);
				this.getView().byId("searchField2").setSelectedItemId(0);
				this.getView().byId("searchField3").setSelectedItemId(0);

				this.getView().byId("searchField2").setEnabled(false);
				this.getView().byId("searchField3").setEnabled(false);
			}
		},
		/**
		 * Function that handles the success case of the ajax call to get the Make
		 * @function onSearchMakeSuccess
		 */
		onSearchMakeSuccess: function (oController, oData, oPiggyBack) {
			var oModelAux = new sap.ui.model.json.JSONModel();
			var oModel1 = new sap.ui.model.json.JSONModel();
			oController.getView().byId("searchField2").setEnabled(true);
			oController.byId("searchField2").setBusy(false);

			oModelAux.setData({
				modelData: oData
			});

			oModelAux.getData().modelData.Models.unshift({
				id: "0",
				result: oController.getResourceBundle().getText("PRDTSDTmodelTXT")
			});
			oModel1.setData(oModelAux.getData().modelData);
			oModel1.setSizeLimit(999999);
			sap.ui.getCore().setModel(oModel1, "vehicleModel");
			oController.getView().setModel(oModel1, "vehicleModel");
		},

		/**
		 * Function that handles the error case of the ajax call to get the Make
		 * @function onSearchMakeError
		 */
		onSearchMakeError: function (oController, oError, oPiggyBack) {
			oController.byId("searchField2").setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRModels"));
		},

		/**
		 * This function is called when the user selects the "MAKE" selectBox. Than call a service that fills the "MODEL" selectBox.
		 * @function onSearchMake
		 */
		onSearchMake: function (oEvent) {
			// Checks Session time out
			this.checkSessionTimeout();
			var controller = this;
			var vehicleMakeID = this.byId("searchField1").getSelectedKey();
			var vehicleYear = this.byId("searchYear").getSelectedKey();
			var oSelectedItem = this.getView().byId("searchField1").getSelectedItem();
			var language = sap.ui.getCore().getConfiguration().getLanguage();
			var xsURL;
			// Other Makes Case
			if (vehicleMakeID === "1") {
				var oMakeData = sap.ui.getCore().getModel("vehicleMake").getData();
				// Add or remove Non USA make vehicles to the list
				var aMakes = [];
				if (!this.bOtherMakesPressed) {
					aMakes.push.apply(aMakes, oMakeData.usaMakes);
					aMakes.push.apply(aMakes, oMakeData.nonUsaMakes);
					this.bOtherMakesPressed = true;
				} else {
					aMakes.push.apply(aMakes, oMakeData.usaMakes);
					this.bOtherMakesPressed = false;
				}
				oMakeData.Makes = aMakes;
				sap.ui.getCore().getModel("vehicleMake").refresh();
				this.bChangeEventFired = false;
				this.getView().byId("searchField1")._checkSelectionChange = function () {
					var oItem = this.getSelectedItem();
					var sKey = this.getSelectedKey();
					if (sKey === "1" && controller.bChangeEventFired) {
						this.fireChange({
							selectedItem: oItem
						});
						controller.bChangeEventFired = false;
					}
					controller.bChangeEventFired = true;
					if (sKey === "1") {
						this.setSelectedKey("0");
					}
				};
				var fnAfterClose = function () {
					if (vehicleMakeID === "1") {
						this.getView().byId("searchField1").open();
					}
					this.getView().byId("searchField1").getPicker().detachAfterClose(fnAfterClose, this);
				};
				var fnAfterOpen = function () {
					if (vehicleMakeID === "1") {
						jQuery.sap.delayedCall(0, null, function () {
							_oView.byId("searchField1").scrollToItem(oSelectedItem);
						});
					}
					this.getView().byId("searchField1").getPicker().detachAfterOpen(fnAfterOpen, this);
				};
				this.getView().byId("searchField1").getPicker().attachAfterClose(fnAfterClose, this);
				this.getView().byId("searchField1").getPicker().attachAfterOpen(fnAfterOpen, this);

				this.getView().byId("applyButton").setVisible(true);
				this.getView().byId("applyButton").setEnabled(false);

				// Enable SelectBox
				this.getView().byId("searchField2").setSelectedItemId(0);
				this.getView().byId("searchField3").setSelectedItemId(0);

				this.getView().byId("searchField2").setEnabled(false);
				this.getView().byId("searchField3").setEnabled(false);
				return;
			}
			if (vehicleMakeID !== "0") {
				// Service Call
				this.byId("searchField2").setBusyIndicatorDelay(0);
				this.byId("searchField2").setBusy(true);

				if (vehicleYear === "0") {
					xsURL = "/products/productfinder/models/" + "?makeID=" + vehicleMakeID + "&lang=" + language;
				} else {
					xsURL = "/products/productfinder/models/" + "?makeID=" + vehicleMakeID + "&year=" + vehicleYear + "&lang=" + language;
				}
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onSearchMakeSuccess, controller.onSearchMakeError);
				// Set first item on the SelectBoxes
				controller.getView().byId("searchField2").setSelectedItemId(0);
				controller.getView().byId("searchField3").setSelectedItemId(0);

				// Enable SelectBox
				controller.getView().byId("searchField2").setEnabled(false);
				controller.getView().byId("searchField3").setEnabled(false);

				// Configure View Content
				controller.getView().byId("applyButton").setVisible(true);
				controller.getView().byId("applyButton").setEnabled(false);
			} else {

				// Configure ApplyButton
				this.getView().byId("applyButton").setVisible(true);
				this.getView().byId("applyButton").setEnabled(false);

				// Enable SelectBox
				this.getView().byId("searchField2").setSelectedItemId(0);
				this.getView().byId("searchField3").setSelectedItemId(0);

				this.getView().byId("searchField2").setEnabled(false);
				this.getView().byId("searchField3").setEnabled(false);
			}

		},

		/**
		 * Function that handles the success case of the ajax call to get the Model
		 * @function onSearchModelSuccess
		 */
		onSearchModelSuccess: function (oController, oData, oPiggyBack) {
			var oModelAux = new sap.ui.model.json.JSONModel();
			var oModel2 = new sap.ui.model.json.JSONModel();
			oController.getView().byId("searchField3").setEnabled(true);
			oController.byId("searchField3").setBusy(false);
			oModelAux.setData({
				modelData: oData
			});
			oModelAux.getData().modelData.Types.unshift({
				id: "0",
				result: oController.getResourceBundle().getText("PRDTSDTtypeTXT")
			});
			oModel2.setData(oModelAux.getData().modelData);
			oModel2.setSizeLimit(999999);
			sap.ui.getCore().setModel(oModel2, "vehicleType");
			oController.getView().setModel(oModel2, "vehicleType");
		},

		/**
		 * Function that handles the error case of the ajax call to get the Model
		 * @function onSearchModelError
		 */
		onSearchModelError: function (oController, oError, oPiggyBack) {
			oController.byId("searchField3").setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRTypes"));

		},

		/**
		 * This function is called when the user selects the "MODEL" selectBox. Than call a service that fills the "TYPE" selectBox.
		 * @function onSearchModel
		 */
		onSearchModel: function () {
			// Checks Session time out
			this.checkSessionTimeout();
			var controller = this;
			var vehicleModelID = this.byId("searchField2").getSelectedKey();
			var vehicleYear = this.byId("searchYear").getSelectedKey();
			var language = sap.ui.getCore().getConfiguration().getLanguage();
			var xsURL;

			if (vehicleModelID !== "0") {
				// Service Call
				this.byId("searchField3").setBusyIndicatorDelay(0);
				this.byId("searchField3").setBusy(true);
				if (vehicleYear === "0") {
					xsURL = "/products/productfinder/types/" + "?modelID=" + vehicleModelID + "&lang=" + language;
				} else {
					xsURL = "/products/productfinder/types/" + "?modelID=" + vehicleModelID + "&year=" + vehicleYear + "&lang=" + language;
				}
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onSearchModelSuccess, controller.onSearchModelError);
				controller.getView().byId("searchField3").setEnabled(false);
				controller.getView().byId("searchField3").setSelectedItemId(0);

				// Configure ApplyButton
				controller.getView().byId("applyButton").setVisible(true);
				controller.getView().byId("applyButton").setEnabled(false);
			} else {
				// Configure ApplyButton
				this.getView().byId("applyButton").setVisible(true);
				this.getView().byId("applyButton").setEnabled(false);
				this.getView().byId("searchField3").setSelectedItemId(0);
				this.getView().byId("searchField3").setEnabled(false);
			}
		},

		/**
		 *  This function is called when the user selects the "TYPE" selectBox. Than call a service that fills the "PRODUCT CATEGORY" selectBox
		 * @function onSearchType
		 */
		onSearchType: function () {
			// Checks Session time out
			this.checkSessionTimeout();
			var vehicleTypeID = this.byId("searchField3").getSelectedKey();

			if (vehicleTypeID !== "0") {
				if (_applyButtonState === "pressed") {
					// Configure ApplyButton
					this.getView().byId("applyButton").setVisible(true);
					this.getView().byId("applyButton").setEnabled(true);
					_applyButtonState = null;
				} else {
					this.getView().byId("applyButton").setEnabled(true);
				}
			} else {
				// Configure ApplyButton
				this.getView().byId("applyButton").setVisible(true);
				this.getView().byId("applyButton").setEnabled(false);
			}
		},

		/**
		 * This function open a new Tab Page for the promotions valvoline page in Products Section.
		 * @function onPressPromotions
		 */
		onPressPromotions: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();
			var promotionsURL = "http://www.valvolinecatalog.com/";
			sap.m.URLHelper.redirect(promotionsURL, true);

			//GTM
			var url = window.location;
			_oController.GTMDataLayer('promobnnerclck',
				'BannerClick',
				url.hash,
				promotionsURL
			);

		},
		/**
		 * This function open a new Tab Page for the filter and wiper information valvoline page in Products Section.
		 * @function onPressFilterInfo
		 */
		onPressFilterInfo: function () {
			if (_oController.checkSessionTimeout()) {
				return;
			}

			var promotionsURL = _oController.getRouter().getURL("productFilterWiperInfo");
			sap.m.URLHelper.redirect("#" + promotionsURL, true);

			//GTM
			var url = window.location;
			_oController.GTMDataLayer('promobnnerclck',
				'BannerClick',
				url.hash,
				promotionsURL
			);
		}
	});
});