sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function (Base) {
	"use strict";

	var _oController;
	var _oView;
	var _oProductDetails;
	var _sProdutCode;
	var _sFileName;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Products.ProductDetails", {
		/** @module ProductDetails */
		/**
		 * This function initializes the controller and attaches a click event to the "go back" icon.
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("productDetails").attachPatternMatched(this._onObjectMatched, this);
			_oController = this;
			_oView = this.getView();

			// Attach Click event on the HBox for the NavBack Button
			this.getView().byId("navBackProductDetails").attachBrowserEvent("click", function () {
				_oController.onNavBack();
			});

			//Adds mouseover and mouseout events on incon
			if (this.onGetDashDevice() === "WIDE") {
				_oView.byId("PiSheetIcon").attachBrowserEvent("mouseover", this.mouseOverBox);
				_oView.byId("PiSheetIcon").attachBrowserEvent("mouseout", this.mouseOutBox);
				_oView.byId("PiSheetLink").attachBrowserEvent("mouseover", this.mouseOverBox);
				_oView.byId("PiSheetLink").attachBrowserEvent("mouseout", this.mouseOutBox);
				_oView.byId("SDSSheetIcon").attachBrowserEvent("mouseover", this.mouseOverBoxSds);
				_oView.byId("SDSSheetIcon").attachBrowserEvent("mouseout", this.mouseOutBoxSds);
				_oView.byId("SDSSheetLink").attachBrowserEvent("mouseover", this.mouseOverBoxSds);
				_oView.byId("SDSSheetLink").attachBrowserEvent("mouseout", this.mouseOutBoxSds);

			}
		},

		onAfterRendering: function () {
			/* Set the menu as active if the app is reloaded */
			$(".js-products-sidenav").addClass("sidenav__item--active");
		},

		/**
		 * Styles the links on mouseover.
		 * @function mouseOverBox
		 */
		mouseOverBox: function () {
			_oView.byId("PiSheetIcon").addStyleClass("iconOtherSites");
			_oView.byId("PiSheetLink").addStyleClass("iconOtherSites");
			_oView.byId("PiSheetIcon").addStyleClass("cursorPointer");
		},
		/**
		 * Styles the links on mouseout
		 * @function mouseOverBox
		 */
		mouseOutBox: function () {
			_oView.byId("PiSheetIcon").removeStyleClass("iconOtherSites");
			_oView.byId("PiSheetLink").removeStyleClass("iconOtherSites");
			_oView.byId("PiSheetIcon").removeStyleClass("cursorPointer");
		},

		/**
		 * Styles the links on mouseover.
		 * @function mouseOverBoxSds
		 */
		mouseOverBoxSds: function () {
			_oView.byId("SDSSheetIcon").addStyleClass("iconOtherSites");
			_oView.byId("SDSSheetLink").addStyleClass("iconOtherSites");
			_oView.byId("SDSSheetIcon").addStyleClass("cursorPointer");
		},

		/**
		 * Styles the links on mouseout
		 * @function mouseOutBoxSds
		 */
		mouseOutBoxSds: function () {
			_oView.byId("SDSSheetIcon").removeStyleClass("iconOtherSites");
			_oView.byId("SDSSheetLink").removeStyleClass("iconOtherSites");
			_oView.byId("SDSSheetIcon").removeStyleClass("cursorPointer");
		},

		/**
		 * This function is called when the page is open. Controlls the browser navigation.
		 * @function _onObjectMatched
		 * @param {Object} oEvent - triggered when navigating to productDetails
		 */
		_onObjectMatched: function (oEvent) {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("PRODUCTFINDER_PAGE")) {
				return;
			}

			//get user info and direct account
			_oController.getUserInformation(_oController);

			this.headerIconAccess();

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			if (this.getView().getParent().getPreviousPage() !== undefined && this.getView().getParent().getPreviousPage().getViewName() ===
				"valvoline.dash.portal.DashPortalWeb.view.Products/ProductsCategory") {
				//if the user is comming from productCategory page subscribe to the event bus
				sap.ui.getCore().getEventBus().subscribeOnce("productFinder", "productDetails", this.onProductDetailEvent, this);
			} else {
				ga('set', 'page', '/ProductsCategory');
				ga('send', 'pageview');
				this.getRouter().navTo("productFinder", {});
			}

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "products"
			});

		},

		/**
		 * This formatter checks if the title of the Open Product is too large, and changes the size of the font.
		 * @function formatProductDetailsTitle
		 * @param {string} sTitle - Product details' title
		 */
		formatProductDetailsTitle: function (sTitle) {
			if (sTitle.length < 100) {
				this.getView().byId("productDetailsTitle").removeStyleClass("pageSubtitle");
				this.getView().byId("productDetailsTitle").addStyleClass("pageTitle");
			} else {
				this.getView().byId("productDetailsTitle").removeStyleClass("pageTitle");
				this.getView().byId("productDetailsTitle").addStyleClass("pageSubtitle");
			}
			return sTitle;
		},
		/**
		 * This function is called when the user clicks on the NavBack Button.
		 * @function onNavBack
		 */
		onNavBack: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			_oController.GTMDataLayer('backclick',
				'Product Details',
				'Back to Product Category',
				"#/" + _oController.getRouter().getRoute("productsCategory").getPattern()
			);

			ga('set', 'page', '/ProductsCategory');
			ga('send', 'pageview');
			this.getRouter().navTo("productsCategory", {}, true);
		},

		/**
		 * This function navigates to the product's PI Sheet
		 * @function toPiSheet
		 */
		toPiSheet: function () {
			var link = _oView.getModel("productDetailsInfo").getData().piSheetLinkEnglish;
			sap.m.URLHelper.redirect(link, true);

			_oController.GTMDataLayer('pdfsheetclick',
				'Product Details',
				'PI Sheet -click',
				link
			);

		},

		/**
		 * This function calls the service to get the List of available SDS Documents.
		 * @function onSearchYear
		 */
		toSDSSheet: function () {
			var language = sap.ui.getCore().getConfiguration().getLanguage();
			var xsURL;
			// Piggyback
			var oPiggyBack = {
				"materialCode": _sProdutCode
			};

			xsURL = "/products/SDSAvailableDocuments" + "?materialCode=" + _sProdutCode + "&lang=" + language;
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onSDSListSuccess, _oController.onSDSListError,
				undefined, oPiggyBack);
			_oController.byId("SDSSheetLink").setBusy(true);
			_oController.byId("SDSSheetLinkTablet").setBusy(true);
			_oController.byId("SDSSheetLinkMobile").setBusy(true);
		},

		/**
		 *  Close and destroy the dialog.
		 * @function closeDialog
		 */
		closeDialog: function () {
			_oController.dialogFragmentSDS.close();
			_oController.dialogFragmentSDS.destroy();
		},

		/**
		 * Function that handles the success case of the ajax call to get the available SDS Documents
		 * @function onSearchYearSuccess
		 */
		onSDSListSuccess: function (oController, oData, oPiggyBack) {
			_oController.byId("SDSSheetLink").setBusy(false);
			_oController.byId("SDSSheetLinkTablet").setBusy(false);
			_oController.byId("SDSSheetLinkMobile").setBusy(false);
			var oModel = new sap.ui.model.json.JSONModel(oData);
			if (oData === "" || oData === undefined) {
				_oController.SDSNoDocumentWarning = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog",
					_oController);
				//Set Warning Buttons
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.onSDSDialogClose);
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("PRDSDSDocumentDialogOkTXT"));
				sap.ui.getCore().byId("button_Dialog_Warning_No").setVisible(false);
				//Set Warning Message
				sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("PRDSDSDocumentDialogWarningText"));
				//Set Warnig Dialog Title
				sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText(
					"PRDSDSDocumentDialogWarningTitle"));
				sap.ui.getCore().byId("icon_Dialog_Warning").setSrc("sap-icon://customfont/alert");
				sap.ui.getCore().byId("icon_Dialog_Warning").addStyleClass("warningMessageDialogIcon");
				_oController.SDSNoDocumentWarning.open();
				_oController._removeDialogResize(_oController.SDSNoDocumentWarning);
			} else {
				_oController.dialogFragmentSDS = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.SdsList", _oController);
				var i18nModel = new sap.ui.model.resource.ResourceModel({
					bundleUrl: "i18n/i18n.properties"
				});
				_oController.dialogFragmentSDS.setModel(i18nModel, "i18n");
				sap.ui.getCore().setModel(oModel, "SDSDocModel");
				_oController.dialogFragmentSDS.setModel(oModel, "SDSDocModel");
				_oController.dialogFragmentSDS.open();

			}
		},

		onSDSDialogClose: function () {
			_oController.SDSNoDocumentWarning.close();
			_oController.SDSNoDocumentWarning.destroy();
		},

		/**
		 * Function that handles the error case of the ajax call to get the available SDS Documents
		 * @function onSearchYearSuccess
		 */
		onSDSListError: function (oController, oError, oPiggyBack) {
			_oController.byId("SDSheetBox").setBusy(false);
			_oController.onShowMessage(_oController, "Error", oError.status, oError.statusText, oError.responseText, _oController.getResourceBundle()
				.getText("ERRModels"));

		},

		/**
		 * Handles the press on the download link on the SDS Document dialog.
		 * @function downloadCaseFile
		 */
		downloadSDSFile: function (oEvent) {
			var language = sap.ui.getCore().getConfiguration().getLanguage();
			var xsURL;
			var sPath = oEvent.getSource().getBindingContext("SDSDocModel").getPath();
			var index = parseInt(sPath.substring(sPath.lastIndexOf('/') + 1));
			var recordNumberSDS = sap.ui.getCore().getModel("SDSDocModel").getData()[index]["recordNumber"];
			var sfileName = sap.ui.getCore().getModel("SDSDocModel").getData()[index]["fileTitle"];
			_sFileName = sfileName;
			// Piggyback
			var oPiggyBack = {
				"recordNumber": recordNumberSDS
			};
			xsURL = "/products/SDSDocument" + "?recordNumber=" + recordNumberSDS + "&lang=" + language;
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onSDSDocDownloadSuccess, _oController.onSDSDocDownloadError,
				undefined, oPiggyBack);
			sap.ui.core.BusyIndicator.show(0);

		},

		/**
		 * Function that handles the success case of the ajax call to download the available SDS Documents
		 * @function onSearchYearSuccess
		 */
		onSDSDocDownloadSuccess: function (oController, oData, oPiggyBack) {
			sap.ui.core.BusyIndicator.hide();
			if (oData !== null && (sap.ui.Device.browser.msie || sap.ui.Device.browser.edge)) {
				var blob = oController.pdfBlobConvesion(oData[0].reportBytes, 'application/pdf');
				window.navigator.msSaveOrOpenBlob(blob, (_sFileName + ".pdf"));
			} else if (oData !== null && sap.ui.Device.browser.safari) {
				window.open(window.URL.createObjectURL(oController.pdfBlobConvesion(oData[0].reportBytes, 'application/pdf'), {
					type: "application/pdf"
				}));
			} else {
				var a = $("<a style='display: none;' target='_self'/>");
				if (oData !== null) {
					var surl = 'data:application/octet-stream;base64,' + oData[0].reportBytes;
					a.attr("href", surl);
				}
				a.attr("download", _sFileName + ".pdf");
				$("body").append(a);
				a[0].click();
				a.remove();
			}

		},
		pdfBlobConvesion: function (b64Data, contentType) {
			contentType = contentType || '';
			var sliceSize = 512;
			b64Data = b64Data.replace(/^[^,]+,/, '');
			b64Data = b64Data.replace(/\s/g, '');
			var byteCharacters = window.atob(b64Data);
			var byteArrays = [];

			for (var offset = 0; offset < byteCharacters.length; offset = offset + sliceSize) {
				var slice = byteCharacters.slice(offset, offset + sliceSize);

				var byteNumbers = new Array(slice.length);
				for (var i = 0; i < slice.length; i++) {
					byteNumbers[i] = slice.charCodeAt(i);
				}
				var byteArray = new Uint8Array(byteNumbers);
				byteArrays.push(byteArray);
			}
			var blob = new Blob(byteArrays, {
				type: contentType
			});
			return blob;
		},

		/**
		 * Function that handles the success case of the ajax call to download the available SDS Documents
		 * @function onSearchYearSuccess
		 */
		onSDSDocDownloadError: function (oController, oError, oPiggyBack) {
			_oController.onShowMessage(_oController, "Error", oError.status, oError.statusText, oError.responseText, _oController.getResourceBundle()
				.getText("ERRModels"));
		},

		/**
		 * This function redirects the users to Hybris Product Page
		 * @function onPressShopNowBt
		 */
		onPressShopNowBt: function (oEvent) {
			// Checks Session time out
			_oController.checkSessionTimeout();
			var productUrl = _oView.getModel("productDetailsInfo").getData().shopUrl;
			if (productUrl) {
				_oController.nav2Hybris(productUrl);
			}

			//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
			_oController.GTMDataLayer('shopNowEvent',
				'Shopping',
				'Product Detail Page - ShopNow -click',
				_oView.getModel("productDetailsInfo").getData().name
			);
		},

		onProductDetailEvent: function (channel, event, data) {

			// if the model is undefined nav back to products
			if (data === undefined) {
				ga('set', 'page', '/ProductsCategory');
				ga('send', 'pageview');
				this.getRouter().navTo("productFinder", {});
			} else {
				_sProdutCode = data.productcode;
				_oProductDetails = data;
				var productSelectedInfo = new sap.ui.model.json.JSONModel();
				productSelectedInfo.setData(_oProductDetails);
				_oView.setModel(productSelectedInfo, "productDetailsInfo");

				ga('set', 'page', '/ProductDetails/' + _oProductDetails.productcode + " ");
				ga('send', 'pageview');

				var oModelVehicleName = sap.ui.getCore().getModel("productsPageVehicleInfo");
				// If the last page is detailsPage it returns to ProductCategory
				if (oModelVehicleName.getProperty("/navigationMode") === "detailsPage") {
					ga('set', 'page', '/ProductsCategory');
					ga('send', 'pageview');
					this.getRouter().navTo("productsCategory", {});
					oModelVehicleName.setProperty("/navigationMode", "detailsPage");
				} else {
					oModelVehicleName.setProperty("/navigationMode", "detailsPage");
				}

				//Sets "PI Sheet" link invisible if link is non-existent
				if (_oView.getModel("productDetailsInfo").getData().piSheetLinkEnglish !== "") {
					_oView.byId("PiSheetBox").setVisible(true);
				} else {
					_oView.byId("PiSheetBox").setVisible(false);
				}

				// Set "SHOP NOW" button invisible if authorization "SHOP_BUTTON" is missing
				if (this.checkDASHLayoutAccess("SHOP_BUTTON")) {
					_oView.byId("shopNowButton").setVisible(true);
				} else {
					_oView.byId("shopNowButton").setVisible(false);
				}
			}
		},

		formatterCapacityFormated: function (sCapacity) {
			if (sCapacity) {
				return sCapacity;
			} else {
				return _oController.getResourceBundle().getText("PPPInoInformationAvailable");
			}
		},

		formatterIntervalFormated: function (sIntervalFormated) {
			if (sIntervalFormated) {
				return sIntervalFormated;
			} else {
				return _oController.getResourceBundle().getText("PPPInoInformationAvailable");
			}
		},

		formatterLanguageFormated: function (filename) {
			return filename + " - " + _oController.getResourceBundle().getText("PRDSDSDocumentDialogLanguageTXT");
		}

	});
});