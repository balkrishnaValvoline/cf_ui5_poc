sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel"
], function (Base, JSONModel) {
	"use strict";
	var _oController, _oView;
	var DASH_MOBILE = "DASH_MOBILE";

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Products.Products", {
		/** @module Products */
		/**
		 * This function initializes the controller and configures the search field and the promotion box.
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oController = this;
			_oView = this.getView();

			var oRouter = this.getRouter();
			oRouter.getRoute("products").attachPatternMatched(this._onObjectMatched, this);

			//Attach Click event on the VBox for image at the bottom of Products page
			_oView.byId("productsBottomImg").attachBrowserEvent("click", function (evt) {
				_oController.onBottomImgClick();
			});

			// Register an event handler to changes of the screen size
			sap.ui.Device.media.attachHandler(_oController.resizeToMobile, this, DASH_MOBILE);
		},

		onAfterRendering: function () {
			/* Set the menu as active if the app is reloaded */
			$(".js-products-sidenav").addClass("sidenav__item--active");
		},

		/**
		 * Function runs everytime the page is opened
		 * @function _onObjectMatchedThis
		 */
		_onObjectMatched: function () {
			_oController = this;

			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page authorization and check layout
			if (!this.checkDASHPageAuthorization("PRODUCTS_PAGE")) {
				return;
			}

			//Load Navigation Cards
			_oController.byId("cardContainerProducts").setViewContext(_oController);
			_oController.populateCardContainer("Products", "11", undefined, this.cardsAccess);

			//get user info and direct account
			_oController.getUserInformation(_oController);

			this.headerIconAccess();

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "products"
			});

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			/* Set the menu as active if the app is reloaded */
			$(".js-products-sidenav").addClass("sidenav__item--active");
		},

		/**
		 * Function to initialize vsibility of cards depending on the roles assigned
		 * @function cardsAccess
		 */
		cardsAccess: function () {
			var obj = _oController.cardAccessRoles();
			if (_oController.onGetDashDevice() === "NARROW") {
				obj = _oController.printLabelDeviceAccess(obj, "mobile");
			}
			var cardModel = new JSONModel(obj);
			_oController.setModel(cardModel, "cardAccess");
		},

		/**
		 * Function to return card roles assigned to the user on Products page
		 * @function cardAccessRoles
		 * @returns {Object} obj - Products Page Cards Roles
		 */
		cardAccessRoles: function () {
			var aCards = ["PRODUCTLABEL_CARD"];
			var i;
			var obj = {
				"PRODUCTLABEL_CARD": false,
				"NotAvailableCard": false
			};
			for (i = 0; i < aCards.length; i++) {
				obj[aCards[i]] = _oController.checkDASHLayoutAccess(aCards[i]);
			}
			return obj;
		},

		/**
		 * Function to control the visibility of "Product Labels" and "Not available on mobile" card depending on the device size
		 * @function printLabelDeviceAccess
		 * @param {Object} obj - Array of roles assigned to user 
		 */
		printLabelDeviceAccess: function (obj, deviceName) {
			if (deviceName === "mobile") {
				var mHybrisParameters = sap.ui.getCore().getModel("HybrisParameters");
				if (mHybrisParameters === undefined) {
					mHybrisParameters = new sap.ui.model.json.JSONModel();
					mHybrisParameters.setData(this.getGlobalParametersByGroup("HybrisParameters"));
					sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
				}
				mHybrisParameters = mHybrisParameters.getData();
				if (this.byId("cardContainerProducts").getContent().length === 1 && this.byId("cardContainerProducts").getContent()[0].getCardButtonTarget() ===
					"/my-account/drum-label-detail") {
					if (mHybrisParameters.showPrintLabelMobile === "false") {
						obj.NotAvailableCard = true;
					} else {
						obj.NotAvailableCard = false;
					}
				} else {
					obj.NotAvailableCard = false;
					if (mHybrisParameters.showPrintLabelMobile === "false") {
						obj.PRODUCTLABEL_CARD = false;
					}
				}
			} else {
				obj.NotAvailableCard = false;
				obj.PRODUCTLABEL_CARD = _oController.checkDASHLayoutAccess("PRODUCTLABEL_CARD");
			}
			return obj;
		},

		/**
		 * Function to control the visbility of "Products Label" card when device size changes
		 * @function resizeToMobile
		 * @params {Object} - contains info relating to device size
		 */
		resizeToMobile: function (mParams) {
			var mHybrisParameters = sap.ui.getCore().getModel("HybrisParameters");
			if (mHybrisParameters === undefined) {
				mHybrisParameters = new sap.ui.model.json.JSONModel();
				mHybrisParameters.setData(this.getGlobalParametersByGroup("HybrisParameters"));
				sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
			}
			mHybrisParameters = mHybrisParameters.getData();
			if (mHybrisParameters.showPrintLabelMobile === "false") {
				var cardAccessRoles = _oController.cardAccessRoles();
				if (mParams.name === "mobile") {
					if (this.byId("cardContainerProducts").getContent().length === 1 && this.byId("cardContainerProducts").getContent()[0].getCardButtonTarget() ===
						"/my-account/drum-label-detail") {
						cardAccessRoles = _oController.printLabelDeviceAccess(cardAccessRoles, "mobile");
					}
				} else {
					if (this.byId("cardContainerProducts").getContent().length === 1 && this.byId("cardContainerProducts").getContent()[0].getCardButtonTarget() ===
						"/my-account/drum-label-detail") {
						cardAccessRoles = _oController.printLabelDeviceAccess(cardAccessRoles, "other");
					}
				}
				if (this.getModel("cardAccess")) {
					this.getModel("cardAccess").setData(cardAccessRoles);
				}
			}
		},

		/**
		 * This method is invoked when image at the bottom with text "Keeping the world moving since 1866" is clicked"
		 * @function onBottomImgClick
		 */
		onBottomImgClick: function () {
			this.getRouter().navTo("products");
		},
		/**
		 * sets the visibility of the card container
		 * @function cardContainerVisibility
		 */
		cardContainerVisibility: function (bool) {
			return !bool;
		},
		/**
		 * Triggers the GTM tags, also handles the price list exception
		 * @function gtmHandling
		 * @param {String} callToActionURL - Route/link or fragment
		 **/
		gtmHandling: function (oEvent) {
			var callToActionURL = oEvent.getParameter("callToActionURL");
			var cardName = _oController.getKey("keys_gtm", "keyProducts_" + callToActionURL.replace(/-|\//g, "") + "_eventLabel");

			var sEvent = _oController.getKey("keys_gtm", "keyProducts_cardClick_event");
			var sEventCategory = _oController.getKey("keys_gtm", "keyProducts_cardClick_eventCategory");
			var sEventAction = _oController.getKey("keys_gtm", "keyProducts_cardClick_eventAction");
			if (sEvent && sEventCategory && sEventAction && cardName) {
				//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
				_oController.GTMDataLayer(sEvent,
					sEventCategory,
					sEventAction,
					cardName + '-click'
				);
			}
		}
	});
});