sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/m/Button",
	"sap/m/Dialog"

], function(Base) {
	"use strict";

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Products.ProductOEM", {

		/**
		 * Runs one time when the page is loaded
		 */
		onInit: function() {
			Base.prototype.onInit.call(this);
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("productOEM").attachPatternMatched(this._onObjectMatched, this);

			// Set Page layout
			sap.ui.getCore().byId("__xmlview0--vbox_background_main").setVisible(false);
			sap.ui.getCore().byId("__xmlview0--menuMobileHeader").setVisible(false);
		},

		/**
		 * Run when navigats to page
		 * @ parameter (oEvent)
		 */
		_onObjectMatched: function(oEvent) {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("PRODUCTFINDER_PAGE")) {
				return;
			}

			//set language to page
			var language = oEvent.getParameter("arguments").language;
			sap.ui.getCore().getConfiguration().setLanguage(language);

			// Get the Index of the Selected Product
			var vehicleID = oEvent.getParameter("arguments").vehicleID;
			this.loadOEMData(vehicleID);

		},

		/**
		 * Hide all menu icons
		 */
		onAfterRendering: function() {
			$('#loading').remove();
		},

		/**
		 * Generate the Table Header used in WIDE and MEDIUM views
		 */
		generateOEMTableHeader: function() {
			// Row Container
			var rowOEM = new sap.m.HBox({
				width: "100%"
			}).addStyleClass("rowOEM");

			// Category Text
			var categoryTextOEM = new sap.m.Text({
				text: this.getResourceBundle().getText("PRDTSDTcategoryOEMTXT")
			}).addStyleClass("oemTableFontBold OEMTableMobileDisplayNone");

			// Category Text Container
			var categoryTextContainerOEM = new sap.m.HBox({}).addStyleClass("categoryTextContainerOEM");

			// Category Main Container
			var categoryContainerOEM = new sap.m.VBox({
				width: "100%"
			}).addStyleClass("categoryContainerOEM");

			categoryTextContainerOEM.addItem(categoryTextOEM);
			rowOEM.addItem(categoryTextContainerOEM);
			rowOEM.addItem(categoryContainerOEM);

			// Use Text
			var useTextOEM = new sap.m.Text({
				text: this.getResourceBundle().getText("PRDTSDTuseOEMTXT")
			}).addStyleClass("oemTableFontBold OEMTableMobileDisplayNone");

			// Use Text Container
			var useTextContainerOEM = new sap.m.HBox({}).addStyleClass("useTextContainerOEM");

			// Use Main Container
			var useContainerOEM = new sap.m.HBox({
				width: "100%"
			});

			useTextContainerOEM.addItem(useTextOEM);
			useContainerOEM.addItem(useTextContainerOEM);
			categoryContainerOEM.addItem(useContainerOEM);

			var recommendationContainerOEM = new sap.m.VBox({
				width: "100%"
			});
			var recommendationTextOEM = new sap.m.Text({
				text: this.getResourceBundle().getText("PRDTSDTrecommendationOEMTXT")
			}).addStyleClass("oemTableFontBold OEMTableMobileDisplayNone");

			recommendationContainerOEM.addItem(recommendationTextOEM);
			useContainerOEM.addItem(recommendationContainerOEM);

			return rowOEM;
		},

		/**
		 * Generate the all table Rows and the NARROW header
		 * @ parameter (oModel)
		 */
		generateOEMTableRows: function(oModel) {
			// OEM Info List
			var rowsListOEM = new sap.m.VBox({
				width: "100%"
			});

			var rowInfo = oModel.getProperty("/components");
			var categoryCounter, userCounter, recommendationCounter;

			// OEM Category Cell
			for (categoryCounter = 0; categoryCounter < rowInfo.length; categoryCounter++) {
				// Doesnt Show the Repeated Category
				if (oModel.getProperty("/components/" + (categoryCounter) + "/isOnly")) {
					// Category Text
					var categoryTextHeaderOEM = new sap.m.Text({
						text: this.getResourceBundle().getText("PRDTSDTcategoryOEMTXT")
					}).addStyleClass("oemTableFontBold OEMTableMobileDisplay");

					// Category Text
					var categoryTextOEM = new sap.m.Text({
						text: rowInfo[categoryCounter].name
					}).addStyleClass("oemTableFont");

					// Category Text Container
					var categoryTextContainerOEM = new sap.m.VBox({}).addStyleClass("categoryTextContainerOEM");

					// Category Main Container
					var categoryContainerOEM = new sap.m.VBox({
						width: "100%"
					});

					// Row Container
					var rowOEM = new sap.m.HBox({
						width: "100%"
					}).addStyleClass("rowOEM");

					categoryTextContainerOEM.addItem(categoryTextHeaderOEM);
					categoryTextContainerOEM.addItem(categoryTextOEM);
					rowOEM.addItem(categoryTextContainerOEM);
					rowOEM.addItem(categoryContainerOEM);
					rowsListOEM.addItem(rowOEM);

					// OEM Use Cell
					for (userCounter = 0; userCounter < rowInfo[categoryCounter].uses.length; userCounter++) {

						// Use Header Text
						var useTextHeaderOEM = new sap.m.Text({
							text: this.getResourceBundle().getText("PRDTSDTuseOEMTXT")
						}).addStyleClass("oemTableFontBold OEMTableMobileDisplay");

						// Use Text
						var useTextOEM = new sap.m.Text({
							text: rowInfo[categoryCounter].uses[userCounter].name
						}).addStyleClass("oemTableFont");

						// Use Text Container
						var useTextContainerOEM = new sap.m.VBox({}).addStyleClass("useTextContainerOEM");

						// Use Main Container
						var useContainerOEM = new sap.m.HBox({
							width: "100%"
						});

						useTextContainerOEM.addItem(useTextHeaderOEM);
						useTextContainerOEM.addItem(useTextOEM);
						useContainerOEM.addItem(useTextContainerOEM);
						categoryContainerOEM.addItem(useContainerOEM);

						// OEM Recommendation Cell
						var recommendationContainerOEM = new sap.m.VBox({
							width: "100%"
						}).addStyleClass("oemTableRecommendationContainer");

						// Recommendation Header Text
						var recommendationTextHeaderOEM = new sap.m.Text({
							text: this.getResourceBundle().getText("PRDTSDTrecommendationOEMTXT")
						}).addStyleClass("oemTableFontBold OEMTableMobileDisplay");
						recommendationContainerOEM.addItem(recommendationTextHeaderOEM);

						// Recommendation Header
						var recommendationTextOEM;

						if (rowInfo[categoryCounter].uses[userCounter].OEMRecommendation.length === 0) {
							recommendationTextOEM = new sap.m.Text({
								text: this.getResourceBundle().getText("UMNA")
							}).addStyleClass("oemTableFont");
							recommendationContainerOEM.addItem(recommendationTextOEM);
							useContainerOEM.addItem(recommendationContainerOEM);
						} else {
							for (recommendationCounter = 0; recommendationCounter < rowInfo[categoryCounter].uses[userCounter].OEMRecommendation.length; recommendationCounter++) {
								recommendationTextOEM = new sap.m.Text({
									text: rowInfo[categoryCounter].uses[userCounter].OEMRecommendation[recommendationCounter]
								}).addStyleClass("oemTableFont");
								recommendationContainerOEM.addItem(recommendationTextOEM);
								useContainerOEM.addItem(recommendationContainerOEM);
							}
						}
					}
				}
			}
			return rowsListOEM;
		},

		/**
		 * Main Table generation function
		 * @ parameter (oModel) model with the infromation of the OEM services
		 */
		generateOEMTable: function(oModel) {
			// Loads the Header and all the rows
			var containerOEM = this.getView().byId("vboxOEMInfoContainer");
			containerOEM.addItem(this.generateOEMTableHeader());
			containerOEM.addItem(this.generateOEMTableRows(oModel));
			setTimeout(function() {
				containerOEM.setBusyIndicatorDelay(500);
				containerOEM.setBusy(false);
			}, 0);
		},

		/**
		 * oModel Modifier
		 */
		OEMDataModifier: function(oModelOEM) {
			// OEM Category Cell
			var oModel = oModelOEM;
			for (var categoryCounter = 0; categoryCounter < oModel.getProperty("/components").length; categoryCounter++) {
				oModel.setProperty("/components/" + (categoryCounter) + "/isOnly", true);
			}
			this.generateOEMTable(oModel);
		},

		/**
		 * Function that handles the success case of the ajax call that loads the OEM data from the vehicle ID
		 * @function onLoadOEMDataSuccess
		 */
		onLoadOEMDataSuccess: function(oController, oData, oPiggyBack) {
			var oModel = new sap.ui.model.json.JSONModel();
			oModel.setData(oData);
			oController.getView().setModel(oModel, "oModelVehicleProperties");
			oController.OEMDataModifier(oModel);
		},

		/**
		 * Function that handles the error case of the ajax call that loads the OEM data from the vehicle ID
		 * @function onLoadOEMDataError
		 */
		onLoadOEMDataError: function(oController, oError, oPiggyBack) {
			var containerOEM = oController.getView().byId("vboxOEMInfoContainer");
			containerOEM.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText);
		},

		/**
		 * Load OEM data from the vehicle ID
		 * @ parameter (vehicleID)
		 */
		loadOEMData: function(vehicleID) {
			var controller = this;
			var containerOEM = this.getView().byId("vboxOEMInfoContainer");
			containerOEM.setBusyIndicatorDelay(0);
			containerOEM.setBusy(true);
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			var xsURL = "/products/productfinder" + "/OEMList?typeID=" + vehicleID + "&lang=" + selectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadOEMDataSuccess, controller.onLoadOEMDataError);
		}
	});
});