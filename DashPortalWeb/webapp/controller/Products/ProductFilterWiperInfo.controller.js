sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel"

], function (Base, JSONModel) {
	"use strict";
	var _oController, _oView;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Products.ProductFilterWiperInfo", {

		/**
		 * Runs one time when the page is loaded
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oController = this;
			_oView = this.getView();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("productFilterWiperInfo").attachPatternMatched(this._onObjectMatched, this);

			// // Set Page layout
			sap.ui.getCore().byId("__xmlview0--vbox_background_main").setVisible(false);
			sap.ui.getCore().byId("__xmlview0--menuMobileHeader").setVisible(false);
			sap.ui.getCore().byId("__xmlview0--sideNavScrollButton").setVisible(false);
		},

		/**
		 * Run when navigats to page
		 * @ parameter (oEvent)
		 */
		_onObjectMatched: function (oEvent) {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// // Page authorization and layout check
			if (!this.checkDASHPageAuthorization("FILTERS_PAGE")) {
				_oController.getRouter().navTo("notAuthorized");
			}
			
			// Local Filter & Wiper Model
			var oFilterModel = new JSONModel();
			oFilterModel.setProperty("/filterGuide", []);
			oFilterModel.setProperty("/filterInfo", []);
			oFilterModel.setProperty("/filterBulletin", []);
			oFilterModel.setProperty("/wiperGuide", []);
			oFilterModel.setProperty("/wiperInfo", []);
			oFilterModel.setProperty("/wiperBulletin", []);
			_oView.setModel(oFilterModel, "filtersModel");
			
			//Fetch Attachment Links 
			_oController.getAttachmentLinks();

		},
		
		getAttachmentLinks: function(){
			var filterGroupId = "ProdFiltersDoc";
			var wiperGroupId = "ProdWipersDoc";
			_oController.getGlobalParameter("guideFilterURL", filterGroupId, _oController.onFilterGuideSuccess, _oController.onFilterPageError);
			_oController.getGlobalParameter("prodinfoFilterURL", filterGroupId, _oController.onFilterInfoSuccess, _oController.onFilterPageError);
			_oController.getGlobalParameter("bulletinFilterURL", filterGroupId, _oController.onFilterBulletinSuccess, _oController.onFilterPageError);
			_oController.getGlobalParameter("guideWiperURL", wiperGroupId, _oController.onWiperGuideSuccess, _oController.onFilterPageError);
			_oController.getGlobalParameter("prodInfoWiperURL", wiperGroupId, _oController.onWiperInfoSuccess, _oController.onFilterPageError);
			_oController.getGlobalParameter("bulletinWiperURL", wiperGroupId, _oController.onWiperBulletinSuccess, _oController.onFilterPageError);
		},


		attachHoverEvents: function (sId) {
			var aItems = _oView.byId(sId).getItems();
			for (var i = 0; i < aItems.length; i++) {
				var aHbox = aItems[i].getItems();
				for (var j = 0; j < aHbox.length; j++) {
					aHbox[j].attachBrowserEvent("mouseover", _oController.mouseOverBox);
					aHbox[j].attachBrowserEvent("mouseout", _oController.mouseOutBox);
				}
			}
		},

		/**
		 * Styles the links on mouseover.
		 * @function mouseOverBox
		 * @param {Object} oEvent - trigged by the mouseover on link or icon
		 */
		mouseOverBox: function (oEvent) {
			var OSid = oEvent.target.id;
			sap.ui.getCore().byId(OSid).getParent().addStyleClass("filterPageLinkHover");
		},

		/**
		 * Styles the links on mouseout
		 * @function mouseOverBox
		 * @param {Object} oEvent - trigged by the mouseout on link or icon
		 */
		mouseOutBox: function (oEvent) {
			var OSid = oEvent.target.id;
			sap.ui.getCore().byId(OSid).getParent().removeStyleClass("filterPageLinkHover");
		},
		
		onFilterGuideSuccess: function (oController, oData, oPiggyBack) {
			_oView.getModel("filtersModel").setProperty("/filterGuide", oData);
			// For desktop, attachBrowserEvent function to the links
			if (_oController.onGetDashDevice() === "WIDE") {
				setTimeout(_oController.attachHoverEvents, 0, "filterGuideSectionId");
			}
		},

		onFilterInfoSuccess: function (oController, oData, oPiggyBack) {
			_oView.getModel("filtersModel").setProperty("/filterInfo", oData);
			if (_oController.onGetDashDevice() === "WIDE") {
				setTimeout(_oController.attachHoverEvents, 0, "filterInfoSectionId");
			}
		},

		onFilterBulletinSuccess: function (oController, oData, oPiggyBack) {
			_oView.getModel("filtersModel").setProperty("/filterBulletin", oData);
			if (_oController.onGetDashDevice() === "WIDE") {
				setTimeout(_oController.attachHoverEvents, 0, "filterBulletinSectionId");
			}
		},

		onWiperGuideSuccess: function (oController, oData, oPiggyBack) {
			_oView.getModel("filtersModel").setProperty("/wiperGuide", oData);
			if (_oController.onGetDashDevice() === "WIDE") {
				setTimeout(_oController.attachHoverEvents, 0, "wiperGuideSectionId");
			}

		},

		onWiperInfoSuccess: function (oController, oData, oPiggyBack) {
			_oView.getModel("filtersModel").setProperty("/wiperInfo", oData);
			if (_oController.onGetDashDevice() === "WIDE") {
				setTimeout(_oController.attachHoverEvents, 0, "wiperInfoSectionId");
			}
		},

		onWiperBulletinSuccess: function (oController, oData, oPiggyBack) {
			_oView.getModel("filtersModel").setProperty("/wiperBulletin", oData);
			if (_oController.onGetDashDevice() === "WIDE") {
				setTimeout(_oController.attachHoverEvents, 0, "wiperBulletinSectionId");
			}
		},
		
		onFilterPageError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRFilters"));
		},
		
		/**
		 * function to hanlde link or icon press.
		 * @function filterPageLinkClick
		 * @param {Object} oEvent - link or icon press function
		 */
		filterPageLinkClick: function (oEvent) {
			var sUrl = oEvent.getSource().data("url");

			//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
			_oController.GTMDataLayer('filterwiperinfolinks',
									  'Filter and Wiper Information Page',
									   oEvent.getSource().data("cat") + ' -PDF Linkclick',
									   oEvent.getSource().getText()
			);
				
			if (sUrl) {
				window.open(sUrl, "_blank");
			}
		},

		/**
		 * formatter function to hanlde visible property.
		 * @function formatSectionVisible
		 * @param {Array} aData - formatter function to set visible
		 */
		formatSectionVisible: function (aData) {
			return Boolean(aData && aData.length);
		}
	});
});