sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel"
], function(Base, JSONModel) {
	"use strict";

	var _oController;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Products.ProductVPS", {
		/** @module Services */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function() {
			Base.prototype.onInit.call(this);
			var oRouter = this.getRouter();
			oRouter.getRoute("productvps").attachPatternMatched(this._onObjectMatched, this);
			var oModelVPS = new JSONModel();
			oModelVPS.loadData("model/productsVPS.json");
			this.getView().setModel(oModelVPS);
			_oController = this;

			// Set Page layout
			sap.ui.getCore().byId("__xmlview0--vbox_background_main").setVisible(false);
			sap.ui.getCore().byId("__xmlview0--menuMobileHeader").setVisible(false);
		},

		/**
		 * Verify authorization and layout access
		 * @function  _onObjectMatched
		 * @param {Object} event - Triggered when navigating to Solutions Page
		 */
		_onObjectMatched: function() {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("PRODUCTFINDER_PAGE")) {
				return;
			}

			//Calls function
			this.onBuildTableModel();
			this.onBuildTableModelMobile1();
			this.onBuildTableModelMobile2();
		},

		/**
		 * This function populates table
		 *
		 */
		onBuildTableModel: function() {
			var oModelVPS = new sap.ui.model.json.JSONModel();
			var vpsData = [{
				"30kmiles": this.getResourceBundle().getText("ServicesCoveredC1L1"),
				"15kmiles": this.getResourceBundle().getText("ServicesCoveredC2L1"),
				"5kmiles": this.getResourceBundle().getText("ServicesCoveredC3L1")
			}, {
				"30kmiles": this.getResourceBundle().getText("ServicesCoveredC1L2"),
				"15kmiles": this.getResourceBundle().getText("ServicesCoveredC2L2"),
				"5kmiles": this.getResourceBundle().getText("ServicesCoveredC3L2")
			}, {
				"30kmiles": this.getResourceBundle().getText("ServicesCoveredC1L3"),
				"15kmiles": this.getResourceBundle().getText("ServicesCoveredC2L3"),
				"5kmiles": ""

			}, {
				"30kmiles": this.getResourceBundle().getText("ServicesCoveredC1L4"),
				"15kmiles": this.getResourceBundle().getText("ServicesCoveredC2L4"),
				"5kmiles": ""

			}, {
				"30kmiles": this.getResourceBundle().getText("ServicesCoveredC1L5"),
				"15kmiles": this.getResourceBundle().getText("ServicesCoveredC2L5"),
				"5kmiles": ""

			}, {
				"30kmiles": this.getResourceBundle().getText("ServicesCoveredC1L6"),
				"15kmiles": this.getResourceBundle().getText("ServicesCoveredC2L6"),
				"5kmiles": ""

			}, {
				"30kmiles": this.getResourceBundle().getText("ServicesCoveredC1L7"),
				"15kmiles": "",
				"5kmiles": ""

			}, {
				"30kmiles": this.getResourceBundle().getText("ServicesCoveredC1L8"),
				"15kmiles": "",
				"5kmiles": ""

			}, {
				"30kmiles": this.getResourceBundle().getText("ServicesCoveredC1L9"),
				"15kmiles": "",
				"5kmiles": ""

			}];
			oModelVPS.setData(vpsData);
			this.getView().setModel(oModelVPS, "vpsTable");

		},
		onBuildTableModelMobile1: function() {
			var oModelVPSmobile = new sap.ui.model.json.JSONModel();
			var vpsDatam = [{
					"15kmiles": this.getResourceBundle().getText("ServicesCoveredC2L1")
				}, {
					"15kmiles": this.getResourceBundle().getText("ServicesCoveredC2L2")
				}, {
					"15kmiles": this.getResourceBundle().getText("ServicesCoveredC2L3")

				}, {
					"15kmiles": this.getResourceBundle().getText("ServicesCoveredC2L4")

				}, {
					"15kmiles": this.getResourceBundle().getText("ServicesCoveredC2L5")
				}, {
					"15kmiles": this.getResourceBundle().getText("ServicesCoveredC2L6")

				}

			];
			oModelVPSmobile.setData(vpsDatam);
			this.getView().setModel(oModelVPSmobile, "vpsTablem1");
		},
		onBuildTableModelMobile2: function() {
			var oModelVPSmobile2 = new sap.ui.model.json.JSONModel();
			var vpsDatam2 = [{
					"5kmiles": this.getResourceBundle().getText("ServicesCoveredC3L1")
				}, {
					"5kmiles": this.getResourceBundle().getText("ServicesCoveredC3L2")
				}

			];
			oModelVPSmobile2.setData(vpsDatam2);
			this.getView().setModel(oModelVPSmobile2, "vpsTablem2");
		},

		/**
		 * This function the i18n value of the given element
		 * @function i18nFormatter
		 * @param {string} sParam - i18 element
		 * @returns i18 value
		 *
		 */
		i18nFormatter: function(sParam) {
			var i18n = this.getResourceBundle();
			return i18n.getText(sParam);
		},

		vpsvisit: function() {
			window.open(this.getResourceBundle().getText("DashGuaranteePageUrl"));

			_oController.GTMDataLayer('linkclickvps',
				'VPS Page Activity',
				this.getResourceBundle().getText("VisitLink"),
				this.getResourceBundle().getText("DashGuaranteePageUrl")
			);
		},

		vpsplus: function() {
			window.open(this.getResourceBundle().getText("ValvolineServicePlus"));

			_oController.GTMDataLayer('linkclickvps',
				'VPS Page Activity',
				this.getResourceBundle().getText("VPSLinkText"),
				this.getResourceBundle().getText("OTHSIwarrantyText1LK")
			);
		}

	});
});