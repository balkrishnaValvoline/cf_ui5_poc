sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function (Base) {
	"use strict";
	var _oRouter;
	var _oController;
	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Home", {
		/** @module Home */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function () {
			_oController = this;
			Base.prototype.onInit.call(_oController);
			_oRouter = sap.ui.core.UIComponent.getRouterFor(_oController);
			_oRouter.getRoute("home").attachPatternMatched(_oController._onObjectMatched, _oController);
		},
		/**
		 * Check which pages the user is authorized to access
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {
			// HDFI-879 check if user is impersonate
			var impParam = jQuery.sap.getUriParameters().get("p");

			if (impParam !== null) {
				impParam = impParam.replace(new RegExp("\\+", "g"), "%2B");
				impParam = impParam.replace(new RegExp("\\/", "g"), "%2F");
				_oController.changeToImpersonate(impParam);
			} else {
				window.localStorage.setItem("impersonateError", false);
				_oRouter.navTo("feed");
			}
		},

		/**
		 * HDFI-879 Change the user to impersonate
		 * @function changeToImpersonate
		 * @param {string} impParam - Parameter's impParam.
		 */
		changeToImpersonate: function (impParam) {
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			var xsURL = "/service-layer-dash/core/changeToImpersonate";
			$.ajax({
				beforeSend: function (request) {
					request.setRequestHeader("Content-Type", "application/json");
				},
				type: "GET",
				dataType: "json",
				url: xsURL + "?p=" + impParam + "&lang=" + selectedLanguage,
				async: false,
				success: function () {
					window.localStorage.setItem("impersonateError", false);
					_oRouter.navTo("feed");
				},
				error: function (oError) {
					window.localStorage.setItem("impersonateError", true);
					var errorMessage = {
						"data": oError.responseJSON.fault.faultstring
					};
					var oModelImpersonateError = new sap.ui.model.json.JSONModel(errorMessage);
					sap.ui.getCore().setModel(oModelImpersonateError, "oModelImpersonateError");
					_oRouter.navTo("unableToImpersonate");
				}
			});
		}
	});
});