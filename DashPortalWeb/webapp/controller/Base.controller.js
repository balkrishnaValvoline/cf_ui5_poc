sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"sap/m/Text",
	"jquery.sap.global",
	"valvoline/dash/portal/DashPortalWeb/controls/Card",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
	"sap/ui/core/format/NumberFormat",
	"valvoline/dash/portal/DashPortalWeb/model/keys/keys"
], function (
	Controller,
	History,
	Text,
	jQuery,
	CardControl,
	MessageBox,
	MessageToast,
	NumberFormat,
	Keys
) {
	"use strict";

	var _requestOrigin,
		_oController,
		_oRouter,
		_oMenuMobileState,
		_timer,
		_stopCountTime,
		_oCaseFile = {},
		_oHowToVideosDialog,
		_isSessionTimeOut = false;

	return Controller.extend("valvoline.dash.portal.DashPortalWeb.controller.Base", {
		/** @module Base*/
		/**
		 * This function initializes the controller and checks the user's authorization.
		 * @function @onInit
		 */
		onInit: function () {
			_oController = this;
			_oRouter = this.getRouter();
			_oMenuMobileState = false;
			_oController.getHowToVideos();

			//add bottom space in footer if there are two buttons and scroll down
			setTimeout(function () {
				var sInsightsPage = "valvoline.dash.portal.DashPortalWeb.view.Insights/Insights";
				var sDistributorInsightsPage = "valvoline.dash.portal.DashPortalWeb.view.Insights/DistributorInsights";
				var sLearnPage = "valvoline.dash.portal.DashPortalWeb.view.Learn/Learn";
				if (_oController.getView().byId("footerVersion") && (_oController.getView().getViewName() === sDistributorInsightsPage ||
						_oController.getView().getViewName() === sInsightsPage || _oController.getView().getViewName() === sLearnPage)) {
					_oController.getView().byId("footerVersion").addStyleClass("reusable-margin-blank-bottom");
				}
			}, 0);

			_oController.getView().addEventDelegate({
				onAfterRendering: function () {
					//Scroll to the top of the page button logic
					$("#" + _oController.getScrollElement().containerId + _oController.getScrollElement().dependant).scroll(function () {
						var oScrollElemnt = _oController.getScrollElement();
						var oHowToVideoButton = _oController.getView().byId("howToVideo");
						if ($("#" + oScrollElemnt.containerId + oScrollElemnt.dependant).scrollTop() > 100) {
							_oController.getView().byId("scrollButton").setVisible(true);
							if (oHowToVideoButton !== undefined) {
								oHowToVideoButton.addStyleClass("reusable-howtovideo-button-bottom");
							}
						} else {
							_oController.getView().byId("scrollButton").setVisible(false);
							if (oHowToVideoButton !== undefined) {
								oHowToVideoButton.removeStyleClass("reusable-howtovideo-button-bottom");
							}
						}
					});
				}
			}, _oController);
		},

		/**
		 * This function returns the scroll element for the current page.
		 * @function onScrollUp
		 */
		getScrollElement: function () {
			var oScrollElemnt;
			if (_oController.getView().getViewName().indexOf("Feed") > -1) {
				//Specific for feed page to point the scroller to the lazyLoading one
				oScrollElemnt = {
					"containerId": _oController.getView().byId("LazyLoadingContainer").getId(),
					"dependant": "",
					"element": ""
				};
			} else {
				oScrollElemnt = {
					"containerId": _oController.getView().getContent()[0].getId(),
					"dependant": " section",
					"element": _oController.getView().getContent()[0]
				};
			}
			return oScrollElemnt;
		},
		/**
		 * This function scrolls to the top of the page.
		 * @function onScrollUp
		 */
		onScrollUp: function () {
			var oScrollElemnt = _oController.getScrollElement();
			if (oScrollElemnt.element === "") {
				//Aplied in pages that have a specific scroll container, ex:feed lazyloading container
				$("#" + oScrollElemnt.containerId).animate({
					scrollTop: 0
				}, 700);
			} else {
				oScrollElemnt.element.scrollTo(0, 700);
			}
		},

		/**
		 * Convenience method for accessing the router.
		 * @function getRouter
		 * @returns the router for this component
		 */
		getRouter: function () {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		/**
		 * Convenience method for getting the view model by name.
		 * @function getModel
		 * @param {string} sName the model name
		 * @returns The model instance
		 */
		getModel: function (sName) {
			return this.getView().getModel(sName);
		},

		/**
		 * Convenience method for setting the view model.
		 * @function setModel
		 * @param {sap.ui.model.Model} oModel the model instance
		 * @param {string} sName the model name
		 * @returns The view instance
		 */
		setModel: function (oModel, sName) {
			return this.getView().setModel(oModel, sName);
		},

		/**
		 * Getter for the resource bundle.
		 * @function getResourceBundle
		 * @returns The resourceModel of the component
		 */
		getResourceBundle: function () {
			return _oController.getOwnerComponent().getModel("i18n").getResourceBundle();
		},

		/**
		 * Show How To Video.
		 * @function showVideo
		 */
		showVideo: function (oEvent) {
			if (!_oHowToVideosDialog || _oHowToVideosDialog.bIsDestroyed) {
				_oHowToVideosDialog = sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.HowToVideosDialog", _oController);
				this.getView().addDependent(_oHowToVideosDialog);
			}
			_oHowToVideosDialog.open();

			var URL;
			var viewName = this.getView().getViewName().split("/").reverse()[0];
			switch (viewName) {
			case ("DistributorInsights"):
				URL = _oController.getView().getModel("HowToVideos").getData().HowToDistInsightsURL;
				break;
			case ("Insights"):
				URL = _oController.getView().getModel("HowToVideos").getData().HowToInsightsURL;
				break;
			case ("Learn"):
				URL = _oController.getView().getModel("HowToVideos").getData().HowToValUniversityURL;
				break;
			}
			if (URL !== undefined) {
				var video = document.getElementById('howToVideosDialog');
				var source = document.createElement('source');

				source.setAttribute('src', URL);

				video.appendChild(source);
			}
			var sClickText;
			try {
				//Click was on the top button
				sClickText = oEvent.getSource().getText();
			} catch (e) {
				//Click was on the side hover button
				sClickText = oEvent.getSource().getAlt();
			}
			//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
			_oController.GTMDataLayer('videolinkclk',
				'Show Me How -Video',
				'Link click',
				sClickText
			);
		},

		/**
		 * Close Dialog How To Videos
		 * @function onHowToVideosClose
		 */
		onHowToVideosClose: function (oEvent) {
			if (_oHowToVideosDialog) {
				_oHowToVideosDialog.close();
				_oController.getView().removeDependent(_oHowToVideosDialog);
				_oHowToVideosDialog.destroy();
			}
			//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
			//Tracking for howToVideos "Close" - Insights, Distributor Insights and Valvoline University (learn)
			_oController.GTMDataLayer('vdbuttonclick',
				'Show Me How -Video',
				'Button click',
				oEvent.getSource().getText()
			);
		},

		/**
		 * Get GlobalParameters and set the Model
		 * @function getHowToVideos
		 */
		getHowToVideos: function () {
			var mHowToVideos = _oController.getView().getModel("HowToVideos");
			if (mHowToVideos === undefined) {
				mHowToVideos = new sap.ui.model.json.JSONModel();
				mHowToVideos.setData(this.getGlobalParametersByGroup("HowToVideos"));
				_oController.getView().setModel(mHowToVideos, "HowToVideos");
			}
		},

		/**
		 * Function that handles the success case of the ajax call that sets the model that groups the accounts by district
		 * @function onGetAccountsByDistrictSuccess
		 */
		onGetAccountsByDistrictSuccess: function (oController, oData, oPiggyBack) {
			var sLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			var comboBox = oController.byId(oPiggyBack.comboBoxID);
			var oAccountsByDistrictModel = new sap.ui.model.json.JSONModel();
			comboBox.setBusy(false);

			var k = 0;
			for (var i = 0; i < oData.length; i++) {
				oPiggyBack.model.push({
					id: "district-" + oData[i].description,
					districtName: oData[i].description,
					other: oData[i].other
				});
				k++;
				for (var j = 0; j < oData[i].accounts.length; j++) {
					oPiggyBack.model.push(oData[i].accounts[j]);
					k++;
					oPiggyBack.model[k].districtName = "";
				}
			}

			// Set the Model in the View
			oAccountsByDistrictModel.setData(oPiggyBack.model);
			oAccountsByDistrictModel.setSizeLimit(oPiggyBack.model.length);

			sap.ui.getCore().setModel(oAccountsByDistrictModel, "oAccountsByDistrict");
			oController.getView().setModel(oAccountsByDistrictModel, "oAccountsByDistrict");
			sap.ui.getCore().getModel("oAccountsByDistrict").setProperty("/sLanguage", sLanguage);

			var sSelectedKey = comboBox.getSelectedKey();
			var sShippingStreet, sSelectedAccountId, sSelectedInfo, sOtherAccountId, sSelectedInfoMobile;

			if (sSelectedKey !== "") {
				for (var w = 0; w < oAccountsByDistrictModel.getData().length; w++) {
					if (sSelectedKey === oAccountsByDistrictModel.getData()[w].id) {
						sSelectedAccountId = oController.formatAcountNumber(oAccountsByDistrictModel.getData()[w].accountnum,
							oAccountsByDistrictModel.getData()[w].districtName);
						sShippingStreet = oAccountsByDistrictModel.getData()[w].shippingstreet;

						if (oAccountsByDistrictModel.getData()[w].districtName === "") {

							sSelectedInfo = oController.formatterAddress(
								oAccountsByDistrictModel.getData()[w].billingcity,
								oAccountsByDistrictModel.getData()[w].billingstate,
								oAccountsByDistrictModel.getData()[w].billingpostalcode);
						}
						if (oAccountsByDistrictModel.getData()[w].districtName !== "") {

							sSelectedInfoMobile = _oController.getResourceBundle().getText("MAresultsTXT");

						}
					}
					if (!sSelectedAccountId && oAccountsByDistrictModel.getData()[w].other === true) {
						sOtherAccountId = oController.formatAcountNumber(oAccountsByDistrictModel.getData()[w].accountnum,
							oAccountsByDistrictModel.getData()[w].districtName);
					}
				}
				if (!sSelectedAccountId && sOtherAccountId) {
					sSelectedAccountId = sOtherAccountId;
				}

				comboBox.setValue(sSelectedAccountId);
				if (sShippingStreet && sSelectedInfo) {
					oController.getView().byId("accountAddress1Desktop").setText(sShippingStreet + '  ' +
						sSelectedInfo);
				}

				if (sSelectedInfoMobile) {
					oController.getView().byId("accountAddress2Mobile").setText(sSelectedInfoMobile);
				}

			}
			if (oPiggyBack.fToRunOnSuccess) {
				oPiggyBack.fToRunOnSuccess(oController);
			}
		},

		/**
		 * Function that handles the error case of the ajax call that sets the model that groups the accounts by district
		 * @function onGetAccountsByDistrictError
		 */
		onGetAccountsByDistrictError: function (oController, oError, oPiggyBack) {
			var comboBox = oController.byId(oPiggyBack.comboBoxID);
			comboBox.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRAccByDistrict"));
		},

		/**
		 * Sets the model that groups the accounts by district
		 * @function getAccountsByDistrict
		 *
		 */
		getAccountsByDistrict: function (controller, comboBoxID, reset, fToRunOnSuccess) {
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			var oAccountsByDistrict = sap.ui.getCore().getModel("oAccountsByDistrict");
			var comboBox = controller.byId(comboBoxID);
			var sLanguage;
			if (oAccountsByDistrict !== undefined) {
				sLanguage = oAccountsByDistrict.getProperty("/sLanguage");
			}
			if (oAccountsByDistrict && !reset && sLanguage && sLanguage === selectedLanguage) {
				oAccountsByDistrict.setSizeLimit(oAccountsByDistrict.getData().length);
				// Configure the ComboBox
				controller.getView().setModel(oAccountsByDistrict, "oAccountsByDistrict");

				if (fToRunOnSuccess) {
					fToRunOnSuccess(controller);
				}
			} else {

				comboBox.setBusyIndicatorDelay(0);
				comboBox.setBusy(true).addStyleClass("comboBoxFilterBusyIndicator");
				var xsURL = "/usermgmt/user/accountsByDistrict?lang=" + selectedLanguage;
				var model = [];
				model[0] = {
					id: "0",
					districtName: controller.getResourceBundle().getText("MAstoresTXT"),
					accbillingcity: controller.getResourceBundle().getText("MAresultsTXT")
				};
				var oPiggyBack = {
					"model": model,
					"comboBoxID": comboBoxID,
					"fToRunOnSuccess": fToRunOnSuccess
				};
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onGetAccountsByDistrictSuccess, controller.onGetAccountsByDistrictError,
					undefined, oPiggyBack);
			}
		},

		/**
		 * Takes the user back to the previous page.
		 * @function onNavBack
		 *
		 */
		onNavBack: function () {
			var oHistory, sPreviousHash;
			oHistory = History.getInstance();
			sPreviousHash = oHistory.getPreviousHash();
			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				this.getRouter().navTo("feed", {}, true /*no history*/ );
				ga('set', 'page', '/Feed');
				ga('send', 'pageview');
			}
		},

		/**
		 * Navigation function.
		 * @function stopVideo
		 * @param {array} page - URL parameters
		 */
		stopVideo: function () {
			$(".videoForStoping").each(function () {
				$(this).remove();
			});
		},

		/**
		 * Navigation function.
		 * @function goToPage
		 * @param {array} page - URL parameters
		 */
		goToPage: function (page) {
			var statsURL = page.getSource().data("page");
			var pageURL = page.getSource().data("url");
			var mobile = page.getSource().data("mobile");

			if (mobile === 'true') {
				this.getView().byId("menuMobileBar").setVisible(false);
				this.getView().byId("menuMobileButton").setIcon("sap-icon://menu2");

				//GTM
				_oController.GTMDataLayer('leftnavclick',
					'Left Navigation Mobile',
					pageURL + '-click',
					"#/" + _oRouter.getRoute(pageURL).getURL()
				);
			}

			// Checks Session time out
			this.checkSessionTimeout();

			if (mobile != 'true') {
				//GTM
				_oController.GTMDataLayer('headerlinksclick',
					'Header-click',
					statsURL,
					"#/" + _oRouter.getRoute(pageURL).getURL()
				);
			}

			ga('set', 'page', '/' + statsURL + '');
			ga('send', 'pageview');

			//stop videos on changing page
			_oController.stopVideo();

			if (pageURL === 'MyAccountProfile') {
				this.resetButtons();
			}

			if (pageURL === 'myAccountCases') {
				this.getRouter().navTo(pageURL, {
					caseId: "0"
				});
			} else if (pageURL === 'insights') {
				if (this.checkDASHPageAuthorization("DISTRIBUTOR_INSIGHTS")) {
					//GTM
					_oController.GTMDataLayer('leftnavclick',
						'Left Navigation',
						'Insights-click',
						"#/" + _oRouter.getRoute("distributorInsights").getURL()
					);
					this.getRouter().navTo("distributorInsights");

				} else if (this.checkDASHPageAuthorization("INSIGHTSHD_PAGE")) {
					//GTM
					_oController.GTMDataLayer('leftnavclick',
						'Left Navigation',
						'Insights-click',
						"#/" + _oRouter.getRoute("insightsHD").getURL()
					);

					_oRouter.navTo("insightsHD");
				} else {
					this.getRouter().navTo(pageURL);
				}
			} else if (pageURL === 'orders') {
				var mHybrisParameters = sap.ui.getCore().getModel("HybrisParameters");
				if (mHybrisParameters === undefined) {
					mHybrisParameters = new sap.ui.model.json.JSONModel();
					mHybrisParameters.setData(this.getGlobalParametersByGroup("HybrisParameters"));
					sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
				}
				var bFullFeatures = (mHybrisParameters.getData().showQuickOrder === "true" || mHybrisParameters.getData().showDeliveryTickets ===
					"true" || mHybrisParameters.getData().showInvoices === "true" || mHybrisParameters.getData().showApproveOrder === "true");

				if (bFullFeatures) {
					//If Mobile on hybris is enabled for one or more cards navigation should be to Order
					this.getRouter().navTo(pageURL);
				} else {
					//If Mobile on hybris is disabled for all cards navigation should be to OrderStatus
					this.getRouter().navTo("orderStatus");
				}
			} else {
				this.getRouter().navTo(pageURL);
			}
		},

		/**
		 * Navigation function.
		 * @function goToPageFooter
		 * @param {array} page - URL parameters
		 */
		goToPageFooter: function (page) {
			var statsURL = page.getSource().data("page");
			var pageURL = page.getSource().data("url");

			// Checks Session time out
			this.checkSessionTimeout();

			//GTM
			_oController.GTMDataLayer('footerlinkclick',
				'Footer-click',
				statsURL,
				"#/" + _oRouter.getRoute(pageURL).getURL()
			);

			ga('set', 'page', '/' + statsURL + '');
			ga('send', 'pageview');

			//stop videos on changing page
			_oController.stopVideo();

			this.getRouter().navTo(pageURL);
		},

		quickLinks: function () {
			//GTM
			_oController.GTMDataLayer('footerlinkclick',
				'Footer-click',
				'Quick Links',
				'https://sds.valvoline.com'
			);

			window.open("https://sds.valvoline.com/", "_blank");
		},

		toSocialMediaLink: function (oEvent) {
			//GTM
			switch (oEvent.getParameter("id").split('--').pop()) {
			case "facebook":
				_oController.GTMDataLayer('footersocialicon',
					'Footer-click',
					'Social Icon click',
					'Faceebok'
				);
				window.open("https://www.facebook.com/valvoline", "_blank");
				break;
			case "twitter":
				_oController.GTMDataLayer('footersocialicon',
					'Footer-click',
					'Social Icon click',
					'Twitter'
				);
				window.open("https://twitter.com/valvoline", "_blank");
				break;
			case "instagram":
				_oController.GTMDataLayer('footersocialicon',
					'Footer-click',
					'Social Icon click',
					'Instagram'
				);
				window.open("https://www.instagram.com/valvoline/?hl=en", "_blank");
				break;
			case "youtube":
				_oController.GTMDataLayer('footersocialicon',
					'Footer-click',
					'Social Icon click',
					'Youtube'
				);
				window.open("https://www.youtube.com/user/valvoline", "_blank");
				break;
			default:
				break;
			}
		},

		/**
		 * Hides busy Indicator loading graphical display.
		 * @function hideBusyIndicator
		 *
		 */
		hideBusyIndicator: function () {
			sap.ui.core.BusyIndicator.hide();
		},

		/**
		 * Shows busy Indicator loading graphical display.
		 * @function showBusyIndicator
		 * @param {number} iDuration - duration of the graphical display
		 * @param {number} iDelay - timer delay to show the graphical display
		 */
		showBusyIndicator: function (iDuration, iDelay) {
			sap.ui.core.BusyIndicator.show(iDelay);

			if (iDuration && iDuration > 0) {
				if (this._sTimeoutId) {
					jQuery.sap.clearDelayedCall(this._sTimeoutId);
					this._sTimeoutId = null;
				}

				this._sTimeoutId = jQuery.sap.delayedCall(iDuration, this, function () {
					this.hideBusyIndicator();
				});
			}
		},

		/**
		 * Formats a float value to 2 decimal cases
		 * 
		 * @function formatFloat
		 */
		formatFloat: function (value) {
			if (value !== undefined && value !== null) {
				var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
				var oFormatOptions = {
					style: "standard",
					decimals: 2,
					groupingEnabled: "true"
				};
				var oFloatFormat = sap.ui.core.format.NumberFormat.getFloatInstance(oFormatOptions, oLocale);
				var cGroupSeparator = oFloatFormat.oLocaleData.getNumberSymbol("group");
				value = value.split(cGroupSeparator).join("");

				// Removal of Leading ZEROS ("01" -> "1")
				if (!isNaN(parseFloat(value.replace(",", "."))) && value !== "") {
					// Adding 0.000001 is a scaling technique due to JS crazy floats, otherwise values like 1.005 would round to 1.00 instead of 1.01 
					value = (Math.round((parseFloat(value.replace(",", ".")) + 0.000001) * 100) / 100) + "";
					return oFloatFormat.format(value).toString();
				}
			}
			return value;
		},

		parse: function (node, j) {
			var controller = this;
			var nodeName = node.nodeName.replace(/^.+:/, "").toLowerCase();
			var cur = null;
			var text = $(node).contents().filter(function () {
				return this.nodeType === 3;
			});
			if (text[0] && text[0].nodeValue.trim()) {
				cur = text[0].nodeValue;
			} else {
				cur = {};
				$.each(node.attributes, function () {
					if (this.name.indexOf("xmlns:") !== 0) {
						cur[this.name.replace(/^.+:/, "")] = this.value;
					}
				});
				$.each(node.children, function () {
					controller.parse(this, cur);
				});
			}
			j[nodeName] = cur;
		},

		/**
		 * Parses XML to JSON
		 * @function xmlToJson
		 * @param {string} xml - XML input
		 * @returns json
		 */
		xmlToJson: function (xml) {
			var roots = $(xml);
			var root = roots[roots.length - 1];
			var json = {};
			this.parse(root, json);

			return json;
		},

		/**
		 * Sets the logOut Button Visible or not depending on the impersonation status.
		 * @function checkImpersonate
		 */
		checkImpersonate: function (firstname, impersonateStatus) {
			if (impersonateStatus === true) {
				return false;
			}
			return true;
		},

		/**
		 * Sets the user icon with red color or not depending on the impersonation status.
		 * @function checkImpersonationUser
		 */
		checkImpersonationUser: function (impersonateStatus) {
			this.impersonateDetails(_oController, null);
			return "sap-icon://customfont/user";
		},

		/**
		 * Log user out when logout button is pressed.
		 * @function logUserOut
		 */
		logUserOut: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			_oController.deleteCartCookie();
			//GTM
			var url = window.location;
			_oController.GTMDataLayer('headerlinksclick',
				'Header-click',
				'Logout',
				url.hash
			);
			/*sap.m.URLHelper.redirect("logout.html", false);*/
			sap.m.URLHelper.redirect("/do/logout?appid=PortalTest", false);
		},

		/**
		 * Function that handles the success case of the ajax call that returns the received data
		 * @function onAjaxSuccessJustReturn
		 */
		onAjaxSuccessJustReturn: function (oController, oData, oPiggyBack) {
			return oData;
		},

		/**
		 * Function that handles the error case of the ajax call that gets the domain's info given the value
		 * @function onGetDomainsByNameError
		 */
		onGetDomainsByNameError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRDomainListByName"));
			return null;
		},

		/**
		 * Gets the domain's info given the value.
		 * @function getDomainByName
		 * @param {string} value - Domain's name.
		 * @returns Domain's value
		 */
		getDomainsByName: function (name) {
			var controller = this;
			var selectedLanguage;
			if (sap.ui.getCore().getModel("selectedLanguage") === undefined) {
				selectedLanguage = this.getSelectedLanguageForLangCode();
			} else {
				selectedLanguage = sap.ui.getCore().getModel("selectedLanguage").getData().selectedLanguage;
			}

			var xsURL = "/core/domainListByName" + "?name=" + name + "&lang=" + selectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			var dataReturn = controller.handleAjaxJSONCall(controller, false, xsURL, "GET", controller.onAjaxSuccessJustReturn, controller.onGetDomainsByNameError);
			return dataReturn;
		},

		/**
		 * Function that handles the error case of the ajax call that gets the global parameter info given its name
		 * @function onGetDomainsByNameError
		 */
		onGetGlobalParameterError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRParameterByName"));
			return null;
		},

		/**
		 *  Gets the global parameter info given its name.
		 * @function getGlobalParamete
		 * @param {string} nameId - Parameter's nameID.
		 * @returns Global parameter data
		 */
		getGlobalParameter: function (nameId, groupId, successFunc, errorFunc) {
			var controller = this;
			var xsURL = "/core/parameterListByNameID" + "?nameID=" + nameId;

			if (groupId !== undefined) {
				xsURL = xsURL + "&?groupID=" + groupId;
			}

			if (successFunc !== undefined || errorFunc !== undefined) {
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				controller.handleAjaxJSONCall(controller, true, xsURL, "GET", successFunc, errorFunc);
			} else {
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				var dataReturn = controller.handleAjaxJSONCall(controller, false, xsURL, "GET", controller.onAjaxSuccessJustReturn, controller.onGetDomainsByNameError);
				return dataReturn;
			}

		},

		/**
		 * Function that handles the success case of the ajax call that gets the global parameter info given its group name
		 * @function onGetGlobalParametersByGroupSuccess
		 */
		onGetGlobalParametersByGroupSuccess: function (oController, oData, oPiggyBack) {
			var dataReturn = oData;
			if (oPiggyBack.groupId === "CookieParameters") {
				var reqHeaderLang = new sap.ui.model.json.JSONModel();
				reqHeaderLang.getData().AcceptLanguage = oData.AcceptLanguage.split(';')[0].split(',')[0];
				sap.ui.getCore().setModel(reqHeaderLang, "reqHeaderLang");
			}
			return dataReturn;
		},

		/**
		 * Function that handles the error case of the ajax call that gets the global parameter info given its group name
		 * @function onGetGlobalParametersByGroupError
		 */
		onGetGlobalParametersByGroupError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRParameterByName"));
		},

		/**
		 * Gets the global parameter info given its group name.
		 * @function getGlobalParametersByGroup
		 * @param {string} groupId - Parameter's groupId.
		 * @returns Global parameter data
		 */
		getGlobalParametersByGroup: function (groupId) {
			var controller = this;
			var oPiggyBack = {
				"groupId": groupId
			};
			var xsURL = "/core/parametersByGroupID" + "?groupID=" + groupId;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			var dataReturn = controller.handleAjaxJSONCall(controller, false, xsURL, "GET", controller.onGetGlobalParametersByGroupSuccess,
				controller.onGetGlobalParametersByGroupError,
				undefined, oPiggyBack);
			return dataReturn;
		},

		/**
		 * Gets the cookies and sets it in a model
		 * @function getCookies
		 */
		getCookies: function () {
			//Get cookie parameters
			var cookies = this.getGlobalParametersByGroup("CookieParameters");
			var oModelCookies = new sap.ui.model.json.JSONModel(cookies);
			sap.ui.getCore().setModel(oModelCookies, "cookies");
		},

		/**
		 * Function that distinguishes the environment
		 * @function gtmEnvironment
		 */
		gtmEnvironment: function () {
			var environment = window.location.host;
			if (environment.indexOf("d58f9c935") > -1) {
				return "Dev";
			} else if (environment.indexOf("-qa") > -1) {
				return "QA";
			} else if (environment.indexOf("dash.valvoline") > -1) {
				return "Production";
			}
		},

		/**
		 * Function that handles the success case of the ajax call that gets all the accounts associated with the logged user.
		 * @function onGetAccountsSuccess
		 */
		onGetAccountsSuccess: function (oController, oData, oPiggyBack) {
			var model;
			var oUserModel = new sap.ui.model.json.JSONModel();
			model = oData;
			oUserModel.setData(model);
			oUserModel.setSizeLimit(oData.length);
			if (oUserModel.getData().accounts === undefined || oUserModel.getData().accounts.length === 0 || oUserModel.getData().accounts[0].id !==
				oUserModel.getData()
				.directAccount) {
				oController.getRouter().navTo("directAccountDeactivated");
			} else {
				oUserModel.setProperty("/accDetailType", "expanded");
				oController.getView().setModel(oUserModel, "oAccounts");
				sap.ui.getCore().setModel(oUserModel, "oAccounts");
				var oPrimAccountModel = new sap.ui.model.json.JSONModel(oUserModel.getProperty("/accounts")[0]);
				oController.getView().setModel(oPrimAccountModel, "oDirectAccount");
				sap.ui.getCore().setModel(oPrimAccountModel, "oDirectAccount");

				ga('set', 'userId', sap.ui.getCore().getModel("oAccounts").getData().uuid);
				ga('set', 'dimension2', sap.ui.getCore().getModel("oAccounts").getData().uuid);
			}
			oPiggyBack.resolve();
			if (oPiggyBack.fToRunOnSuccess) {
				oPiggyBack.fToRunOnSuccess(oController);
			}
		},

		/**
		 * Function that handles the error case of the ajax call that gets all the accounts associated with the logged user.
		 * @function onGetAccountsError
		 */
		onGetAccountsError: function (oController, oError, oPiggyBack) {
			var oSQL = new sap.ui.model.json.JSONModel();
			oSQL.setProperty("/firstname", oController.getResourceBundle().getText("UMUser"));
			oController.getView().setModel(oSQL, "oAccounts");
			oController.onShowMessage(oController, "Error", oError.status,
				oError.statusText, oError.responseText, oController.getResourceBundle().getText("ERRAccounts"));
		},

		/**
		 * Get all the accounts associated with the logged user.
		 * @function getAccounts
		 * @param {Object} controller - controller where the function is called
		 * @param {Function} fToRunOnSuccess - Optional parameter, it may contain a function to run on success
		 */
		getAccounts: function (controller, fToRunOnSuccess) {
			var promisseImpersonateDetails;
			var promisseGetAccounts;

			// Impersonate Check
			if (!sap.ui.getCore().getModel("oModelImpersonate")) {
				promisseImpersonateDetails = new Promise(function (resolve, reject) {
					controller.impersonateDetails(controller, resolve);
				});
			} else {
				controller.getView().setModel(sap.ui.getCore().getModel("oModelImpersonate"), "oModelImpersonate");
			}

			promisseGetAccounts = new Promise(function (resolve, reject) {
				// GetAccounts
				var oAccounts = sap.ui.getCore().getModel("oAccounts");
				if (!oAccounts || oAccounts.getData().accDetailType === "direct") {
					var xsURL = "/usermgmt/user/accounts";
					var oPiggyBack = {
						"resolve": resolve,
						"fToRunOnSuccess": fToRunOnSuccess
					};
					//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
					controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onGetAccountsSuccess, controller.onGetAccountsError,
						null, oPiggyBack);
				} else {
					controller.getView().setModel(oAccounts, "oAccounts");
					controller.getView().getModel("oAccounts").setSizeLimit(oAccounts.length);
					if (fToRunOnSuccess) {
						fToRunOnSuccess();
					}
				}
			});
			Promise.all([promisseImpersonateDetails, promisseGetAccounts]).then(function () {
				var sUrl = window.location;
				var oModelImpersonate = sap.ui.getCore().getModel("oModelImpersonate");
				if (oModelImpersonate.getData().impersonateStatus === true) {
					window.dataLayer.push({
						'event': 'VirtualPageview',
						'virtualPageUrl': sUrl,
						'environment': controller.gtmEnvironment(),
						'virtualPageTitle': controller.getView().byId("title").getProperty("text"),
						'Impersonation': 'Yes',
						'ImpersonatorID': oModelImpersonate.getData().UuidImpersonator,
						'uuid': sap.ui.getCore().getModel("oAccounts").getData().uuid,
						'industryCode': sap.ui.getCore().getModel("oAccounts").getData().accounts[0] && sap.ui.getCore().getModel("oAccounts").getData()
							.accounts[0].sapindustrykey,
						'industryDescription': sap.ui.getCore().getModel("oAccounts").getData().accounts[0] && sap.ui.getCore().getModel(
								"oAccounts")
							.getData().accounts[0].sapindustrykeydesc
					});
				} else {
					window.dataLayer.push({
						'event': 'VirtualPageview',
						'virtualPageUrl': sUrl,
						'environment': controller.gtmEnvironment(),
						'virtualPageTitle': controller.getView().byId("title").getProperty("text"),
						'Impersonation': 'No',
						'ImpersonatorID': '',
						'uuid': sap.ui.getCore().getModel("oAccounts").getData().uuid,
						'industryCode': sap.ui.getCore().getModel("oAccounts").getData().accounts[0] && sap.ui.getCore().getModel("oAccounts").getData()
							.accounts[0].sapindustrykey,
						'industryDescription': sap.ui.getCore().getModel("oAccounts").getData().accounts[0] && sap.ui.getCore().getModel(
								"oAccounts")
							.getData().accounts[0].sapindustrykeydesc
					});
				}
			});
		},

		/**
		 * Function that handles the success case of the ajax call that gets the logged user information and its direct account if active.
		 * @function getUserInformationSuccess
		 */
		getUserInformationSuccess: function (oController, oData, oPiggyBack) {
			var oAccounts = sap.ui.getCore().getModel("oAccounts");
			if (oAccounts) {
				oController.getView().setModel(oAccounts, "oAccounts");
				oController.getView().getModel("oAccounts").setSizeLimit(oAccounts.length);
			} else {
				var oUserModel = new sap.ui.model.json.JSONModel();
				oUserModel.setData(oData);
				oUserModel.setSizeLimit(oData.length);
				if (oUserModel.getData().accounts === undefined || oUserModel.getData().accounts.length === 0 || oUserModel.getData().accounts[0]
					.id !==
					oUserModel.getData()
					.directAccount) {
					oController.getRouter().navTo("directAccountDeactivated");
				} else {
					oUserModel.setProperty("/accDetailType", "direct");
					oController.getView().setModel(oUserModel, "oAccounts");
					sap.ui.getCore().setModel(oUserModel, "oAccounts");
					var oPrimAccountModel = new sap.ui.model.json.JSONModel(oUserModel.getProperty("/accounts")[0]);
					oController.getView().setModel(oPrimAccountModel, "oDirectAccount");
					sap.ui.getCore().setModel(oPrimAccountModel, "oDirectAccount");

					ga('set', 'userId', sap.ui.getCore().getModel("oAccounts").getData().uuid);
					ga('set', 'dimension2', sap.ui.getCore().getModel("oAccounts").getData().uuid);
					oPiggyBack.resolve();
					if (oPiggyBack.fToRunOnSuccess) {
						oPiggyBack.fToRunOnSuccess(oController);
					}
				}
			}
			oController.byId("userButton").setBusy(false);
		},

		/**
		 * Function that handles the error case of the ajax call that gets the logged user information and its direct account if active.
		 * @function getUserInformationError
		 */
		getUserInformationError: function (oController, oError, oPiggyBack) {
			oController.getRouter().navTo("notAuthorized");
			oController.onShowMessage(oController, "Error", oError.status,
				oError.statusText, oError.responseText, oController.getResourceBundle().getText("ERRAccounts"));
		},

		/**
		 * Get user details and direct account if active.
		 * @function getUserInformation
		 * @param {Object} controller - controller where the function is called
		 * @param {Function} fToRunOnSuccess - Optional parameter, it may contain a function to run on success
		 */
		getUserInformation: function (controller, fToRunOnSuccess) {
			var promiseImpersonateDetails;
			var promiseGetUserInfo;
			var promiseUserRole;
			// Impersonate Check
			if (!sap.ui.getCore().getModel("oModelImpersonate")) {
				promiseImpersonateDetails = new Promise(function (resolve, reject) {
					controller.impersonateDetails(controller, resolve);
				});
			} else {
				controller.getView().setModel(sap.ui.getCore().getModel("oModelImpersonate"), "oModelImpersonate");
			}
			if (!sap.ui.getCore().getModel("layoutAccess")) {
				promiseUserRole = new Promise(function (resolve, reject) {
					var oUserRole = sap.ui.getCore().getModel("layoutAccess") && sap.ui.getCore().getModel("layoutAccess").getData();
					if (!oUserRole) {
						controller.byId("userButton").setBusy(true);
						var oPiggyBack1 = {
							"resolve": resolve,
							"fToRunOnSuccess": fToRunOnSuccess
						};
						var xsURL = "/auth/dashAccessList";
						//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
						_oController.handleAjaxJSONCall(_oController, false, xsURL, "GET", _oController.onCheckDASHLayoutAccessSuccess,
							_oController.onCheckDASHLayoutAccessError, oPiggyBack1);
					}
				});
			}

			promiseGetUserInfo = new Promise(function (resolve, reject) {
				var oAccounts = sap.ui.getCore().getModel("oAccounts");
				if (oAccounts) {
					controller.getView().setModel(oAccounts, "oAccounts");
					controller.getView().getModel("oAccounts").setSizeLimit(oAccounts.length);
					if (fToRunOnSuccess) {
						fToRunOnSuccess(controller);
					}
				} else {
					controller.byId("userButton").setBusy(true);
					var oPiggyBack = {
						"resolve": resolve,
						"fToRunOnSuccess": fToRunOnSuccess
					};
					var xsURL = "/usermgmt/user/information";
					//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
					controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.getUserInformationSuccess, controller.getUserInformationError,
						undefined, oPiggyBack);
				}
			});
			Promise.all([promiseImpersonateDetails, promiseGetUserInfo, promiseUserRole]).then(function () {
				var sUrl = window.location;
				var oModelImpersonate = sap.ui.getCore().getModel("oModelImpersonate");
				if (oModelImpersonate.getData().impersonateStatus === true) {
					window.dataLayer.push({
						'event': 'VirtualPageview',
						'virtualPageUrl': sUrl,
						'environment': controller.gtmEnvironment(),
						'virtualPageTitle': controller.getView().byId("title").getProperty("text"),
						'Impersonation': 'Yes',
						'ImpersonatorID': oModelImpersonate.getData().UuidImpersonator,
						'uuid': sap.ui.getCore().getModel("oAccounts").getData().uuid,
						'industryCode': sap.ui.getCore().getModel("oAccounts").getData().accounts[0] && sap.ui.getCore().getModel("oAccounts").getData()
							.accounts[0].sapindustrykey,
						'industryDescription': sap.ui.getCore().getModel("oAccounts").getData().accounts[0] && sap.ui.getCore().getModel(
								"oAccounts")
							.getData().accounts[0].sapindustrykeydesc,

						'userrole': sap.ui.getCore().getModel("layoutAccess").getData().AccessList.join()
					});
				} else {
					window.dataLayer.push({
						'event': 'VirtualPageview',
						'virtualPageUrl': sUrl,
						'environment': controller.gtmEnvironment(),
						'virtualPageTitle': controller.getView().byId("title").getProperty("text"),
						'Impersonation': 'No',
						'ImpersonatorID': '',
						'uuid': sap.ui.getCore().getModel("oAccounts").getData().uuid,
						'industryCode': sap.ui.getCore().getModel("oAccounts").getData().accounts[0] && sap.ui.getCore().getModel("oAccounts").getData()
							.accounts[0].sapindustrykey,
						'industryDescription': sap.ui.getCore().getModel("oAccounts").getData().accounts[0] && sap.ui.getCore().getModel(
								"oAccounts")
							.getData().accounts[0].sapindustrykeydesc,

						'userrole': sap.ui.getCore().getModel("layoutAccess").getData().AccessList.join()
					});
				}
			});
		},

		/**
		 * Find account by ID.
		 * @function findAccount
		 * @param {string} id - Id of an account
		 * @returns Account data
		 */
		findAccount: function (id, controller) {
			if (id === "0") {
				var newObject = [];
				newObject[0] = {
					id: "0",
					accountnum: controller.getResourceBundle().getText("MAstoresTXT"),
					billingcity: controller.getResourceBundle().getText("MAresultsTXT"),
					billingstate: "",
					billingpostalcode: ""
				};
				return newObject[0];
			} else {

				// Get the User Accounts
				if (!controller.getAccounts(controller)) {
					controller.getRouter().navTo("notAuthorized");
					return;
				}
				var oAccounts = sap.ui.getCore().getModel("oAccounts").getData().accounts;
				for (var i = 0; i < oAccounts.length; i++) {
					if (oAccounts[i].id === id) {
						return oAccounts[i];
					}
				}
			}

		},

		/**
		 * Find account by ID from accountsByDistrict.
		 * @function
		 * @param {string} id - Id of an account
		 * @returns Account data
		 */
		findAccountByDistrict: function (id, controller) {
			if (id === "0") {
				var newObject = [];
				newObject[0] = {
					id: "0",
					accountnum: controller.getResourceBundle().getText("MAstoresTXT"),
					billingcity: controller.getResourceBundle().getText("MAresultsTXT"),
					billingstate: "",
					billingpostalcode: ""
				};
				return newObject[0];
			} else {
				var oAccountsByDistrict = sap.ui.getCore().getModel("oAccountsByDistrict");
				if (oAccountsByDistrict) {
					var iAcc;
					for (iAcc = 0; iAcc < oAccountsByDistrict.getData().length; iAcc++) {
						if (oAccountsByDistrict.getData()[iAcc].id === id) {
							return oAccountsByDistrict.getData()[iAcc];
						}
					}
				}
			}
			//Account was not found, or it was not a part of the initial setting
			return;
		},

		pad: function (n, width, z) {
			z = z || "0";
			n = n + "";
			return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
		},

		/**
		 * Function that handles the success case of the ajax call that loads the information about the Cards from the DB.
		 * @function onPopulateCardContainerSuccess
		 */
		onPopulateCardContainerSuccess: function (oController, oData, oPiggyBack) {
			var size = null;
			var pageSize = null;
			if (oController.byId("feedFilter")) {
				oController.byId("feedFilter").setEnabled(true);
			}

			//remove previous content and disable busy indicator
			oPiggyBack.cardContainer.destroyContent();
			oPiggyBack.cardContainer.setBusy(false);

			var cardTemplate;

			// get the json result data
			var data = oData.cardList;
			pageSize = parseInt(oData.pageSize, 10);

			//Display Recruiting Card
			if (oPiggyBack.section === "6") {
				var recruitingCard = oController.logicDisplayRecruitingCard(i);
				if (recruitingCard) {
					data.push(recruitingCard);
					pageSize++;
				}
			}
			// If there is Data generate the Cards if no set the "NO CARD INFORMATION."
			if (data.length > 0) {
				if (data.length > pageSize && oPiggyBack.section === "1") {
					size = pageSize;
				} else {
					size = data.length;
				}

				// Run all the Data
				for (var i = 0; i < size; i++) {
					if (oPiggyBack.categoryId !== "All" && data[i].cardCategoryId !== oPiggyBack.categoryId && oPiggyBack.categoryId !== undefined) {
						continue;
					} else {
						// If is mobile is the HERO card will be converted in Image card
						if (oController.onGetDashDevice() === "NARROW") {
							if (data[i].cardTypeId === "500") {
								cardTemplate = new CardControl("", {
									CardId: data[i].UUID,
									CardTitle: data[i].title,
									CardType: 200
								});
							} else {
								cardTemplate = new CardControl("", {
									CardId: data[i].UUID,
									CardTitle: data[i].title,
									CardType: parseInt(data[i].cardTypeId, 10)
								});
							}
						} else {
							cardTemplate = new CardControl("", {
								CardId: data[i].UUID,
								CardTitle: data[i].title,
								CardType: parseInt(data[i].cardTypeId, 10)
							});
						}
						oPiggyBack.cardContainer.insertContent(cardTemplate, i);
					}
				}
			} else {
				oController.byId("CardScreen").setVisible(false);
				if (oController.byId("NoCardsScreen")) {
					oController.byId("NoCardsScreen").setVisible(true);
				}
			}
			var oModel = new sap.ui.model.json.JSONModel(data);
			oPiggyBack.cardContainer.setModel(oModel);
			oPiggyBack.cardContainer.setCardCategory(oPiggyBack.categoryId);
			sap.ui.getCore().setModel(oModel, "cards");
			sap.ui.getCore().setModel(new sap.ui.model.json.JSONModel({
				"pageSize": pageSize
			}), "pageSize");

			setTimeout(function () {
				if (oPiggyBack.fToRunAfterGettingCards) {
					oPiggyBack.fToRunAfterGettingCards();
				}
				if ($(".sapMScrollContV")[0] !== undefined) {
					$(".sapMScrollContV")[0].scrollTop = 0;
				} else {
					return;
				}
			}, 0);
		},

		/**
		 * Function that handles the error case of the ajax call that loads the information about the Cards from the DB.
		 * @function onPopulateCardContainerError
		 */
		onPopulateCardContainerError: function (oController, oError, oPiggyBack) {
			if (oController.byId("feedFilter")) {
				oController.byId("feedFilter").setEnabled(true);
			}
			oPiggyBack.cardContainer.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRCards"));
			setTimeout(function () {
				if ($(".sapMScrollContV")[0] !== undefined) {
					$(".sapMScrollContV")[0].scrollTop = 0;
				} else {
					return;
				}
			}, 0);
		},

		/**
		 * This functions loads the information about the Cards from the DB.
		 * @function populateCardContainer
		 *
		 * @param {string} containerDesc - need for the loading animation and to set the cards in the right container
		 * @param {string} section  - section number for get the cards in the right container
		 * @param {string} categoryId - the category Id needed for the filter in the Feed Page
		 * @fabiano.a.rosa - 28/04/17
		 *
		 */
		populateCardContainer: function (containerDesc, section, categoryId, fToRunAfterGettingCards) {
			// Resets the Card Container Screen
			this.byId("CardScreen").setVisible(true);
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			if (this.byId("NoCardsScreen")) {
				this.byId("NoCardsScreen").setVisible(false);
			}

			if (this.byId("feedFilter")) {
				this.byId("feedFilter").setEnabled(false);
			}

			var xsURL = "/core/containerCardsBySection" + "?section=" + section + "&lang=" + selectedLanguage;

			if (categoryId !== "All" && categoryId !== null && categoryId !== undefined) {
				xsURL += "&cardCategory=" + categoryId;
			}

			var pageDesc = containerDesc.split(" ").join("");
			var cardContainer = this.getView().byId("cardContainer" + pageDesc);
			var oPiggyBack = {
				"cardContainer": cardContainer,
				"section": section,
				"categoryId": categoryId,
				"fToRunAfterGettingCards": fToRunAfterGettingCards
			};
			cardContainer.setBusyIndicatorDelay(0);
			cardContainer.setBusy(true);
			cardContainer.destroyContent();

			var controller = this;

			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onPopulateCardContainerSuccess, controller.onPopulateCardContainerError,
				undefined, oPiggyBack);
		},

		/**
		 * Concatenates several fields of the account's address
		 * @function formatterAddress
		 * @param {string} accbillingcity - billing city of the account
		 * @param {string} accbillingstate - billing state of the account
		 * @param {string} accbillingpostalcode - billing postalcode of the account
		 * @returns Formatted address
		 */
		formatterAddress: function (accbillingcity, accbillingstate, accbillingpostalcode) {
			if (accbillingstate && accbillingpostalcode) {
				return accbillingcity + ", " + accbillingstate + " " + accbillingpostalcode;
			} else {
				if (accbillingstate) {
					return accbillingcity + ", " + accbillingstate;
				} else if (accbillingpostalcode) {
					return accbillingcity + " " + accbillingpostalcode;
				} else {
					return accbillingcity;
				}
			}
		},

		/**
		 * Function concatenates the location address for the filters
		 * @function formatAddressFilter
		 * @param {string} accshippingstreet - shipping street of the account
		 * @param {string} accbillingcity - billing city of the account
		 * @param {string} accbillingstate - billing state of the account
		 * @param {string} accbillingpostalcode - billing postalcode of the account
		 * @returns Formatted address
		 */
		formatAddressFilter: function (id, accshippingstreet, accbillingcity, accbillingstate, accbillingpostalcode) {
			var address = "";
			if (accbillingcity !== "Showing results for:") {
				if (accshippingstreet !== undefined && accshippingstreet !== null) {
					address += accshippingstreet;
				}
				if (accbillingcity !== undefined && accbillingcity !== null) {
					address += " " + accbillingcity;
				}
				if (accbillingstate !== undefined && accbillingstate !== null) {
					address += " " + accbillingstate;
				}
				if (accbillingpostalcode !== undefined && accbillingpostalcode !== null) {
					address += " " + accbillingpostalcode;
				}
			} else {
				address = accbillingcity;
			}
			return address;
		},

		/**
		 * This function concatenates the account primary address.
		 * @function formatterContactAddress
		 * @param {string} mailingstreet - mailing street of the account
		 * @param {string} mailingcity - mailing city of the account
		 * @param {string} mailingstate - mailing state of the account
		 * @param {string} mailingpostalcode  - mailing postalcode of the account
		 * @returns Full formatted address
		 */
		formatterContactAddress: function (mailingstreet, mailingcity, mailingstate, mailingpostalcode) {
			var locationString = "";
			if (mailingstate && mailingpostalcode) {
				locationString = mailingcity + ", " + mailingstate + " " + mailingpostalcode;
			} else {
				if (mailingstate) {
					locationString = mailingcity + ", " + mailingstate;
				} else if (mailingpostalcode) {
					locationString = mailingcity + " " + mailingpostalcode;
				} else {
					locationString = mailingcity;
				}
			}

			if (mailingstreet) {
				locationString = mailingstreet + "\n " + locationString;
			}

			return locationString;
		},

		/**
		 * Set the combobox item type.
		 * @function formatAccountType
		 * @param {string} sValue - other property.
		 * @returns {string} type - Active or Inactive
		 */
		formatAccountType: function (sValue) {
			if (sValue) {
				return "Inactive";
			}
			return "Active";
		},

		/**
		 * Formatter for Account Number displayed on price list search field.
		 * @function formatAcountNumberPriceList
		 * @param {string} accNumber - Account Number.
		 * @returns {string} accNumberFormatted - Account Number formatted (e.g #000123456)
		 */
		formatAcountNumberPriceList: function (accNumber) {
			if (accNumber) {
				if (accNumber !== "All Locations") {
					var accNumberFormatted = accNumber;
					if (accNumberFormatted.match(/[a-z]/i)) {
						accNumberFormatted = "#" + accNumberFormatted;
					} else {
						accNumberFormatted = "#" + parseInt(accNumberFormatted, 10);
					}

				} else {
					accNumberFormatted = accNumber;
				}
				return accNumberFormatted;
			} else {
				return "";
			}
		},

		/**
		 * Formatter for Account Number displayed on my account search field.
		 * @function formatAcountNumber
		 * @param {string} accNumber - Account Number.
		 * @param {string} districtName - District Name.
		 * @returns {string} accNumberFormatted - Account Number formatted (e.g #000123456)
		 */
		formatAcountNumber: function (accNumber, districtName) {
			if (districtName === "") {
				var accNumberFormatted = accNumber;
				if (accNumberFormatted && accNumberFormatted.match(/[a-z]/i)) {
					accNumberFormatted = "\u00A0 \u00A0 \u00A0 \u00A0" + "#" + accNumberFormatted;
				} else {
					accNumberFormatted = "\u00A0 \u00A0 \u00A0 \u00A0" + "#" + parseInt(accNumberFormatted, 10);
				}
			} else {
				accNumberFormatted = districtName;
			}
			return accNumberFormatted;
		},

		/**
		 * Changes timestamp format to MM/DD/YYYY (excludes hours)
		 * @function formatDateUTCtoLocal
		 * @param {string} dateUTCString - timestamp
		 * @returns Formatted date
		 */
		formatDateUTCtoLocal: function (dateUTCString) {
			if (dateUTCString === "UNCONFIRMED") {
				return dateUTCString;
			} else if (!dateUTCString) {
				return _oController.getResourceBundle().getText("UMNA");
			} else {
				var newdate = new Date(dateUTCString);
				var localDate = new Date(newdate.valueOf() - (newdate.getTimezoneOffset()) * 60000);
				var options = {
					year: "numeric",
					month: "2-digit",
					day: "2-digit"
				};
				var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
				var slocaleID = ((oLocale.sLocaleId === "en") ? "en-US" : oLocale.sLocaleId);
				var datestring = localDate.toLocaleDateString(slocaleID, options);

				return datestring;
			}
		},
		/**
		 * Changes timestamp format to MM/DD/YYYY (excludes hours)
		 * @function formatDateSimple
		 * @param {string} dateString - timestamp
		 * @returns Formatted date (no locale offset)
		 */
		formatDateSimple: function (dateString) {
			var reg = /^\d*[/]\d*[/]\d*$/;
			if (dateString === "UNCONFIRMED") {
				return dateString;
			} else if (!dateString) {
				return _oController.getResourceBundle().getText("UMNA");
			} else if (isNaN(new Date(dateString).getDate())) {
				return dateString;
			} else if (dateString.match(reg)) {
				return dateString;
			} else {
				var localDate = new Date(dateString);
				var dateFormat;
				switch (sap.ui.getCore().getConfiguration().getLanguage()) {
				case ("en"):
					dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
						pattern: "MM/dd/YYYY"
					});
					break;
				default:
					dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
						pattern: "dd/MM/YYYY"
					});

				}
				var dateFormatted = dateFormat.format(localDate);

				return dateFormatted;
			}
		},

		/**
		 * Returns date in the format "MMM dd"
		 * @function formatDate
		 * @param {string} [sDate] - A string with a date info
		 * @returns - Formatted date
		 */
		formatDate: function (sDate) {
			if (!sDate || sDate === "UNCONFIRMED") {
				return this.getResourceBundle().getText("MAORDUnconfirmed");
			} else {
				var day = parseInt(sDate.split("-")[2], 10);
				var month = parseInt(sDate.split("-")[1], 10) - 1;
				var monthNames = [this.getResourceBundle().getText("MJAN"),
					this.getResourceBundle().getText("MFEB"),
					this.getResourceBundle().getText("MMAR"),
					this.getResourceBundle().getText("MAPR"),
					this.getResourceBundle().getText("MMAY"),
					this.getResourceBundle().getText("MJUN"),
					this.getResourceBundle().getText("MJUL"),
					this.getResourceBundle().getText("MAUG"),
					this.getResourceBundle().getText("MSEP"),
					this.getResourceBundle().getText("MOCT"),
					this.getResourceBundle().getText("MNOV"),
					this.getResourceBundle().getText("MDEC")
				];
				return monthNames[month] + " " + day;
			}
		},

		/**
		 * Returns date in the local format
		 * @function formatDate2
		 * @param {string} [sDate] - A string with a date info
		 * @returns Formatted Date
		 */
		formatDate2: function (sDate) {
			if (!sDate || sDate === "UNCONFIRMED") {
				return this.getResourceBundle().getText("MAORDUnconfirmed");
			} else {
				var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
				var slocaleID = ((oLocale.sLocaleId === "en") ? "en-US" : oLocale.sLocaleId);

				var day = parseInt(sDate.split("-")[2], 10);
				var month = parseInt(sDate.split("-")[1], 10) - 1;
				var year = sDate.split("-")[0];

				var timestamp = new Date(year, month, day);
				var datestring = timestamp.toLocaleDateString(slocaleID);

				return datestring;
			}
		},

		/**
		 * Returns date in the local format
		 * @function formatDate2EstORD
		 * @param {string} [sDate] - A string with a date info
		 * @param {string} [sTime] - A string with a time info
		 * @returns Formatted Date
		 */
		formatDate2EstOrd: function (sDate, sTime) {
			if (sDate) {
				var oDate = new Date(sDate);
				if (!isNaN(oDate.getTime())) {
					if (!sTime || sTime === "000000") {
						sTime = "T12:00:00";
					}
					var dateUTCString = sDate.concat(sTime, "Z");
					var newdate = new Date(dateUTCString);
					var localDate = new Date(newdate.valueOf() - (newdate.getTimezoneOffset()) * 60000);
					var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
					var slocaleID = ((oLocale.sLocaleId === "en") ? "en-US" : oLocale.sLocaleId);
					var datestring = localDate.toLocaleDateString(slocaleID);

					return datestring;
				}
			}
			return "";
		},

		/**
		 * Returns date in the local format
		 * @function formatDate2Ord
		 * @param {string} [sDate] - A string with a date info
		 * @param {string} [sTime] - A string with a time info
		 * @returns Formatted Date
		 */
		formatDate2Ord: function (sDate, sTime) {
			if (!sDate || sDate === "UNCONFIRMED") {
				return this.getResourceBundle().getText("MAORDUnconfirmed");
			} else {
				if (!sTime) {
					sTime = "T12:00:00";
				} else {
					sTime = "".concat("T", sTime);
				}

				var dateUTCString = sDate.concat(sTime, "Z");
				var newdate = new Date(dateUTCString);
				var localDate = new Date(newdate.valueOf() - (newdate.getTimezoneOffset()) * 60000);
				var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
				var slocaleID = ((oLocale.sLocaleId === "en") ? "en-US" : oLocale.sLocaleId);
				var datestring = localDate.toLocaleDateString(slocaleID);

				return datestring;
			}
		},

		/**
		 * Opens Dialog to create new case
		 * @function toRequestS
		 * @param {string} type - Request type ("changeStoreList" or "changeAccount")
		 */
		toRequest: function (type) {
			// Checks Session time out
			this.checkSessionTimeout();

			var nPhoneNumber = sap.ui.getCore().getModel("oAccounts").getData().phone;
			ga('set', 'page', '/Web2Case');
			ga('send', 'pageview');
			this.dialogFragment = new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.DialogNewCase", this);
			this.dialogFragment.open();

			this.dialogFragment.focus();

			sap.ui.getCore().byId("accname").setText(sap.ui.getCore().getModel("oDirectAccount").getProperty("/accountname"));
			sap.ui.getCore().byId("accountnumber").setText(parseInt(sap.ui.getCore().getModel("oDirectAccount").getProperty("/accountnum"), 10));
			sap.ui.getCore().byId("phone").setValue(nPhoneNumber);

			this.getResourceBundle();

			switch (type) {
			case "changeStoreList":
				sap.ui.getCore().byId("dialogSentence").setText(this.getResourceBundle().getText("DNCchangeTXT"));
				_requestOrigin = "changeStoreList";
				break;
			case "changeAccount":
				_requestOrigin = "changeAccount";
				sap.ui.getCore().byId("dialogSentence").setText(this.getResourceBundle().getText("DNCchangeTXT"));
				break;
			default:
				_requestOrigin = "newCase";

				//GTM
				_oController.GTMDataLayer('creatnwcasebtnclck',
					'Support Requests',
					'Create New Support Request -click',
					sap.ui.getCore().getModel("oAccounts").getData().uuid
				);

				//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
				sap.ui.getCore().byId("fileUploadCases").attachBrowserEvent("click", function () {
					_oController.GTMDataLayer('attchfileEvnt',
						'Cases',
						'AttachFile Button -Activity',
						'ATTACH FILE -ButtonClick'
					);
				});

				break;
			}
			//Show or hide the request type drop-down list depending on the origin
			sap.ui.getCore().byId("requestTypeSection").setVisible(_requestOrigin === "newCase");

			var i18nModel = new sap.ui.model.resource.ResourceModel({
				bundleUrl: "i18n/i18n.properties"
			});

			//get the request types options from the domains
			var oRequestTypeModel = new sap.ui.model.json.JSONModel(_oController.getDomainsByName("Web2CaseReqType"));

			var aRequest = oRequestTypeModel.getData();
			if (aRequest && aRequest.length > 0) {
				var oDefaultTxt = {
					id: 0,
					lang: aRequest[0].lang,
					value: "0",
					description: _oController.getResourceBundle().getText("DNCselectTypeTXT"),
					name: aRequest[0].name
				};
				aRequest.unshift(oDefaultTxt);
			}

			this.dialogFragment.setModel(i18nModel, "i18n");
			this.dialogFragment.setModel(oRequestTypeModel, "requestTypeItems");
		},

		/**
		 * Function that handles the success case of the ajax call that creates a form and submit it via Web2Cases
		 * @function onSubmitWeb2CaseSuccess
		 */
		onSubmitWeb2CaseSuccess: function (oController, oData, oPiggyBack) {
			setTimeout(function () {
				_oController.DataLossWarning =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", _oController);
				//Set Warning Buttons
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.onSubmitWeb2CaseDialogSuccessClose);
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWcloseTXT"));
				sap.ui.getCore().byId("button_Dialog_Warning_No").setVisible(false);
				//Set Warning Message
				sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWCaseSubmitedSuccessfully"));
				//Set Warnig Dialog Title
				sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText(
					"DLWCaseSubmitedSuccessfullyTitle"));
				sap.ui.getCore().byId("icon_Dialog_Warning").setSrc("sap-icon://customfont/success");
				sap.ui.getCore().byId("icon_Dialog_Warning").removeStyleClass("warningMessageDialogIcon");
				sap.ui.getCore().byId("icon_Dialog_Warning").addStyleClass("successMessageDialogIcon");
				_oController.DataLossWarning.open();
				_oController._removeDialogResize(_oController.DataLossWarning);
			}, 0);

			//GTM
			_oController.GTMDataLayer('reqmalwarescanning',
				'Support Requests',
				'Attached File Malware Scanning',
				'Success'
			);
		},

		onSubmitWeb2CaseDialogSuccessClose: function () {
			var dialogNC = sap.ui.getCore().byId("dialogNewCase");
			dialogNC.setBusy(false);
			_oController.closeDialog();

			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();
		},

		/**
		 * Function that handles the error case of the ajax call that creates a form and submit it via Web2Case
		 * @function onSubmitWeb2CaseError
		 */
		onSubmitWeb2CaseError: function (oController, oError, oPiggyBack) {
			setTimeout(function () {
				_oController.DataLossWarning =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", _oController);
				if (oError && oError.responseJSON && oError.responseJSON.fault && oError.responseJSON
					.fault.detail && oError.responseJSON.fault.detail.errorcode === "4001") {
					//Malicious file was detected!!
					//Set Warning Message
					sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWCaseMaliciousFileError"));
					_oCaseFile = {};
					sap.ui.getCore().byId("fileNameContainer").setVisible(false);
					sap.ui.getCore().byId("fileName").setText("");
					sap.ui.getCore().byId("fileExtension").setText("");

					//GTM
					oController.GTMDataLayer('reqmalwarescanning',
						'Support Requests',
						'Attached File Malware Scanning',
						'Identified as Malicious'
					);
				} else {
					//Set Warning Message
					sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWCaseSubmitedError"));
				}

				//Set Warning Buttons
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.onSubmitWeb2CaseDialogErrorClose);
				sap.ui.getCore().byId(
					"button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWcloseTXT"));
				sap.ui.getCore().byId(
					"button_Dialog_Warning_No").setVisible(false);
				//Set Warnig Dialog Title
				sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWCaseSubmitedErrorTitle"));
				sap
					.ui.getCore().byId("icon_Dialog_Warning").setSrc("sap-icon://customfont/error");
				sap.ui.getCore().byId("icon_Dialog_Warning").removeStyleClass(
					"warningMessageDialogIcon");
				sap.ui.getCore().byId("icon_Dialog_Warning").addStyleClass("errorMessageDialogIcon");
				_oController.DataLossWarning
					.open();
				_oController._removeDialogResize(_oController.DataLossWarning);
			}, 0);
		},

		onSubmitWeb2CaseDialogErrorClose: function () {
			var dialogNC = sap.ui.getCore().byId("dialogNewCase");
			dialogNC.setBusy(false);

			var dialog = sap.ui.getCore().byId("warningDialog");
			dialog.close();
			dialog.destroy();
		},

		/**
		 * Create a form and submit it via Web2Case
		 * @function submitWeb2CaseS
		 * @param {string} type - Request type ("changeStoreList" or "changeAccount" or "newCase")
		 */
		submitWeb2Case: function () {
			var dialog = sap.ui.getCore().byId("dialogNewCase");
			dialog.setBusyIndicatorDelay(0);
			dialog.setBusy(true);
			var controller = this;

			// Checks Session time out
			this.checkSessionTimeout();

			//GTM
			_oController.GTMDataLayer('creatnwcasebtnclck',
				'Cases',
				'Submit-click',
				sap.ui.getCore().getModel("oAccounts").getData().uuid
			);

			var sizeDescription = sap.ui.getCore().byId("description").getValue().length;
			var sizeSubject = sap.ui.getCore().byId("subject").getValue().length;
			var selectedType = sap.ui.getCore().byId("supportRequestType").getSelectedKey();

			if (sizeDescription !== 0 && sizeSubject !== 0 && sizeDescription <= 31616 && sizeSubject <= 255 && ((_requestOrigin ===
					"newCase" &&
					selectedType !== "0") || (_requestOrigin !== "newCase" && selectedType === "0"))) {

				// Send all the Parameters
				var newCase = {};
				newCase.origin = _requestOrigin;
				if (_requestOrigin === "newCase") {
					newCase.requestType = sap.ui.getCore().byId("supportRequestType").getSelectedKey();
				} else {
					newCase.requestType = null;
				}
				newCase.company = sap.ui.getCore().byId("accname").getText();
				newCase.accNumber = sap.ui.getCore().byId("accountnumber").getText();
				newCase.name = sap.ui.getCore().byId("name").getText();
				newCase.email = sap.ui.getCore().byId("email").getText();
				newCase.phone = sap.ui.getCore().byId("phone").getValue();
				newCase.subject = sap.ui.getCore().byId("subject").getValue();
				newCase.description = sap.ui.getCore().byId("description").getValue();
				if (_oCaseFile.base64 !== undefined && _oCaseFile.base64 !== "") {
					newCase.file = _oCaseFile.base64;
					newCase.fileName = _oCaseFile.file.name;
				}

				var caseJsonString = JSON.stringify(newCase);
				var xsURL = "/salesforce/createCase";
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				controller.handleAjaxJSONCall(controller, true, xsURL, "POST", controller.onSubmitWeb2CaseSuccess, controller.onSubmitWeb2CaseError,
					caseJsonString);
			} else {

				if (sizeDescription === 0) {
					MessageToast.show(this.getResourceBundle().getText("DNCemptyDescriptionTXT"));
				} else if (sizeDescription > 31616) {
					MessageToast.show(this.getResourceBundle().getText("DNClongDescriptionTXT"));
				} else {
					return;
				}

				if (sizeSubject === 0) {
					MessageToast.show(this.getResourceBundle().getText("DNCemptySubjectTXT"));
				} else if (sizeSubject > 255) {
					MessageToast.show(this.getResourceBundle().getText("DNClongSubjectTXT"));
				} else {
					return;
				}
			}
		},

		/**
		 * Closes and destroys dialog.
		 * @function closeDialog
		 */
		closeDialog: function () {
			this.dialogFragment.close();
			this.dialogFragment.destroy();
		},

		/**
		 * Verify input fields during live change on Web2Case submission.
		 * @function verifyInput
		 * @param {Object} event - triggered on live change of input fields on web2case dialog
		 */
		verifyInput: function (event) {
			var sPhoneValue = sap.ui.getCore().byId("phone").getValue(),
				bValPhone = true,
				bValSubject = true,
				bValDescription = true,
				bValRequestType = true,
				regex = /^[+]*[(]{0,1}[0-9]{0,4}[)]{0,1}[0-9][-\s\.0-9]*$/; //Regular expression that matches the structure of a phone number

			//Validation of the fields
			if (sPhoneValue.length !== 0) {
				bValPhone = regex.test(sap.ui.getCore().byId("phone").getValue());
			}
			bValSubject = sap.ui.getCore().byId("subject").getValue() !== "";
			bValDescription = sap.ui.getCore().byId("description").getValue() !== "";
			if (_requestOrigin === "newCase") {
				bValRequestType = sap.ui.getCore().byId("supportRequestType").getSelectedKey() !== "0";
			}

			//sets field to normal state
			event.getSource().setValueState(sap.ui.core.ValueState.None);

			switch (event.getSource().getId()) {
			case "phone":
				if (!bValPhone) {
					event.getSource().setValueState(sap.ui.core.ValueState.Error);
				}
				break;
			case "subject":
				if (!bValSubject && bValDescription) {
					event.getSource().setValueState(sap.ui.core.ValueState.Error);
				}
				break;
			case "description":
				if (bValSubject && !bValDescription) {
					event.getSource().setValueState(sap.ui.core.ValueState.Error);
				}
				break;
			default:
				break;
			}

			if (bValPhone && bValSubject && bValDescription && bValRequestType) {
				sap.ui.getCore().byId("submitWeb2Case").setEnabled(true);
			} else {
				sap.ui.getCore().byId("submitWeb2Case").setEnabled(false);
			}
		},

		/**
		 * Handler for the New Case Dialog file load completion. Adds the download link to the dialog and sets the link style.
		 * @function handleUploadCaseFileComplete
		 */
		handleUploadCaseFileComplete: function (oEvent) {
			var oFiles = oEvent.getParameter("files");
			_oCaseFile.file = oFiles[0];

			if (oFiles && _oCaseFile) {
				var reader = new FileReader();

				reader.onload = function (readerEvt) {
					var sCase64String = reader.result.split(",").pop();
					_oCaseFile.base64 = sCase64String;

					var sfileNameExt = _oCaseFile.file.name;
					var afileNameExtArray = sfileNameExt.split(".");
					var sfileName = afileNameExtArray.slice(0, afileNameExtArray.length - 1).join(".");
					var sfileExtension = "." + afileNameExtArray[afileNameExtArray.length - 1];

					sap.ui.getCore().byId("fileNameContainer").setVisible(true);
					sap.ui.getCore().byId("fileName").setText(sfileName);
					sap.ui.getCore().byId("fileExtension").setText(sfileExtension);

					//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
					_oController.GTMDataLayer('attchfileEvnt',
						'Cases',
						'AttachFile Button -Activity',
						'ATTACH FILE -Success'
					);
				};

				reader.onError = function (readerEvt) {
					sap.ui.getCore().byId("fileNameContainer").setVisible(false);
					setTimeout(function () {
						_oController.DataLossWarning =
							new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", _oController);
						//Set Warning Buttons
						sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.onSubmitWeb2CaseDialogErrorClose);
						sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWcloseTXT"));
						sap.ui.getCore().byId("button_Dialog_Warning_No").setVisible(false);
						//Set Warning Message
						sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWCaseFileLoadError"));
						//Set Warnig Dialog Title
						sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWCaseFileLoadErrorTitle"));
						sap.ui.getCore().byId("icon_Dialog_Warning").setSrc("sap-icon://customfont/error");
						sap.ui.getCore().byId("icon_Dialog_Warning").removeStyleClass("warningMessageDialogIcon");
						sap.ui.getCore().byId("icon_Dialog_Warning").addStyleClass("errorMessageDialogIcon");
						_oController.DataLossWarning.open();
						_oController._removeDialogResize(_oController.DataLossWarning);
					}, 0);
					//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
					_oController.GTMDataLayer('attchfileEvnt',
						'Cases',
						'AttachFile Button -Activity',
						'ATTACH FILE -Error'
					);
				};
				reader.readAsDataURL(_oCaseFile.file);

			}
		},

		/**
		 * Handles the press on the download link on the New case dialog.
		 * @function downloadCaseFile
		 */
		downloadCaseFile: function () {
			_oController.saveFile(_oCaseFile.file.name, _oCaseFile.file.type, _oCaseFile.file);
			sap.ui.getCore().byId("fileName").getDomRef().blur();
			sap.ui.getCore().byId("fileExtension").getDomRef().blur();
		},

		/**
		 * Downloands a fale based on the name, type and data provided
		 * @function saveFile
		 */
		saveFile: function (name, type, data) {
			if (data !== null && (sap.ui.Device.browser.msie || sap.ui.Device.browser.edge)) {
				return navigator.msSaveBlob(new Blob([data], {
					type: type
				}), name);
			} else if (data !== null && sap.ui.Device.browser.safari) {
				window.open(window.URL.createObjectURL(data, {
					type: type
				}));
			} else {
				var url = window.URL.createObjectURL(new Blob([data], {
					type: type
				}));
				var a = $("<a style='display: none;' target='_self'/>");

				a.attr("href", url);
				a.attr("download", name);
				$("body").append(a);
				a[0].click();
				window.URL.revokeObjectURL(url);
				a.remove();
			}
		},

		/**
		 * Displays an error when max file size is exceeded on new dialog case
		 * @function handleCasesFileSizeExceeded
		 */
		handleCasesFileSizeExceeded: function () {
			setTimeout(function () {
				_oController.DataLossWarning =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", _oController);
				//Set Warning Buttons
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.onSubmitWeb2CaseDialogErrorClose);
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWcloseTXT"));
				sap.ui.getCore().byId("button_Dialog_Warning_No").setVisible(false);
				//Set Warning Message
				sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWCaseFileSizeExceededError"));
				//Set Warnig Dialog Title
				sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText(
					"DLWCaseFileSizeExceededTitleError"));
				sap.ui.getCore().byId("icon_Dialog_Warning").setSrc("sap-icon://customfont/error");
				sap.ui.getCore().byId("icon_Dialog_Warning").removeStyleClass("warningMessageDialogIcon");
				sap.ui.getCore().byId("icon_Dialog_Warning").addStyleClass("errorMessageDialogIcon");
				_oController.DataLossWarning.open();
				_oController._removeDialogResize(_oController.DataLossWarning);
			}, 0);

			//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
			_oController.GTMDataLayer('attchfileEvnt',
				'Cases',
				'AttachFile Button -Activity',
				'ATTACH FILE -Error'
			);
		},

		/**
		 * Function that handles the success case of the ajax call that checks the SAML session timeout
		 * @function onCheckSessionTimeoutSuccess
		 */
		onCheckSessionTimeoutSuccess: function (oController, oData, oPiggyBack) {
			//No action needed, timout would already be handled by the handleAjaxJSONCall
		},

		/**
		 * Function that handles the error case of the ajax call that checks the SAML session timeout
		 * @function onCheckSessionTimeoutError
		 */
		onCheckSessionTimeoutError: function (oController, oError, oPiggyBack) {
			//No action needed, timout would already be handled by the handleAjaxJSONCall
		},

		/**
		 * Check the SAML session timeout
		 * @function checkSessionTimeout
		 */
		checkSessionTimeout: function () {
			var xsURL = "/core/serverTimestamp";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onCheckSessionTimeoutSuccess,
				_oController.onCheckSessionTimeoutError);
		},

		/**
		 * Check the SAML session timeout for Services used in the error function of the ajax call
		 * @function checkSessionTimeoutServices
		 * @param {Object} oError - Service error
		 */
		checkSessionTimeoutServices: function (oError) {
			var isTimeout = false;
			if (oError.getResponseHeader("com.sap.cloud.security.login")) {
				isTimeout = true;
				if (!_isSessionTimeOut) {
					sap.m.MessageBox.show(
						"Session is expired, page will be reloaded!", {
							icon: sap.m.MessageBox.Icon.INFORMATION,
							title: "Information",
							actions: [sap.m.MessageBox.Action.CLOSE],
							onClose: function () {
								window.location.reload();
							}
						}
					);
				}
				_isSessionTimeOut = true;
			}
			return isTimeout;
		},

		padding_right: function (s, c, n) {
			if (!s || !c || s.length >= n) {
				return s;
			}
			var max = (n - s.length) / c.length;
			for (var i = 0; i < max; i++) {
				s += c;
			}
			return s;
		},

		contains: function (a, obj) {
			var i = a.length;
			while (i--) {
				if (a[i] === obj) {
					return true;
				}
			}
			return false;
		},

		/**
		 * Remove left Zeros
		 * @function removeZeros
		 * @param {number} number - number to parse.
		 */
		removeZeros: function (number) {
			var formattedNumber;
			if (number !== "") {
				formattedNumber = "" + parseInt(number, 10);
				return formattedNumber;
			} else {
				return formattedNumber;
			}

		},

		/**
		 * Format the keys for Order your Oil Kit and Create a new Support Request
		 * 
		 * @function formatSelectedKey
		 */
		formatSelectedKey: function (sValue) {
			return isNaN(sValue) ? "0" : sValue;
		},

		/**
		 * Navigates to Learn Help page.
		 * @function toHelp
		 */
		toHelp: function (oEvent) {
			// Checks Session time out
			this.checkSessionTimeout();

			var sUrl = "https://www.valvoline.com/dash/help";
			var sSourceId = oEvent.getSource().getId();
			if (sSourceId === "buttonHowToVideosMoreInfo") {
				//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
				//Tracking for howToVideos "More Info" - Insights, Distributor Insights and Valvoline University (learn)
				_oController.GTMDataLayer('vdbuttonclick',
					'Show Me How -Video',
					'Button click',
					oEvent.getSource().getText()
				);
			} else if (sSourceId === "helpButton" || sSourceId === "mobileHelp") {
				//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
				//Tracking for navigation bar/header help - Main and HeaderUserInformation
				_oController.GTMDataLayer('headerlinksclick',
					'Header-click',
					'Help',
					'#/help'
				);
			}
			ga('set', 'page', '/help');
			ga('send', 'pageview');

			sap.m.URLHelper.redirect(sUrl, true);
		},

		/**
		 * Reset (clear) all the pressed menu icons
		 * @function resetMenuButtons
		 */
		resetMenuButtons: function () {
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "home"
			});
		},

		/**
		 * Join the first and last name
		 * @function joinNames
		 * @param {string} - fstName
		 * @param {string} - lastName
		 */
		joinNames: function (fstName, lastName) {
			var string;
			if (fstName && lastName) {
				string = fstName + " " + lastName;
			} else if (fstName) {
				string = fstName;
			} else if (lastName) {
				string = lastName;
			} else {
				string = "";
			}

			return string;
		},

		/**
		 * Check DASH Page Access Authorization
		 * @function checkDASHPageAuthorization
		 * @param {string} - pageCode (e.g. ORDERS_PAGE)
		 */
		checkDASHPageAuthorization: function (pageCode) {
			var isAuth = this.checkDASHLayoutAccess(pageCode);
			if (!isAuth) {
				ga('set', 'page', '/NotAuthorized');
				ga('send', 'pageview');
				_oRouter.navTo("notAuthorized");
				return false;
			} else {
				return true;
			}
		},

		/**
		 * Function that handles the success case of the ajax call that checks DASH Layout Access
		 * @function onCheckDASHLayoutAccessSuccess
		 */
		onCheckDASHLayoutAccessSuccess: function (oController, oData, oPiggyBack) {
			var model;
			if (oData !== undefined) {
				if (oData.AccessList.length === 1 && oData.AccessList[0] === "WCMT_PAGE") {
					ga('set', 'page', '/NotAuthorized');
					ga('send', 'pageview');
					_oRouter.navTo("notAuthorized");
					return;
				} else {
					model = oData;
					var oSQL = new sap.ui.model.json.JSONModel(model);
					sap.ui.getCore().setModel(oSQL, "layoutAccess");
				}

			}
		},

		/**
		 * Function that handles the error case of the ajax call that checks DASH Layout Access
		 * @function onCheckDASHLayoutAccessError
		 */
		onCheckDASHLayoutAccessError: function (oController, oError, oPiggyBack) {
			if (oError.status === 401) {
				//not authorized
				ga('set', 'page', '/NotAuthorized');
				ga('send', 'pageview');
				_oRouter.navTo("notAuthorized");
				return false;
			} else {
				ga('set', 'page', '/UnexpectedError');
				ga('send', 'pageview');
				_oRouter.navTo("unexpectedError");
				return false;
			}
		},

		/**
		 * Check DASH Layout Access
		 * @function checkDASHLayoutAccess
		 * @param {string} - elementCode (e.g. SHOP_ICON)
		 */
		checkDASHLayoutAccess: function (elementCode) {
			//check if we have a model
			var arrayReturned = sap.ui.getCore().getModel("layoutAccess");
			if (!arrayReturned) {

				var xsURL = "/auth/dashAccessList";
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				var bReturnData = _oController.handleAjaxJSONCall(_oController, false, xsURL, "GET", _oController.onCheckDASHLayoutAccessSuccess,
					_oController.onCheckDASHLayoutAccessError);
				if (bReturnData === false) {
					return bReturnData;
				}
			}

			arrayReturned = sap.ui.getCore().getModel("layoutAccess");
			if (!arrayReturned) {
				return false;
			} else {
				var displayElements = arrayReturned.getData().AccessList;

				for (var i = 0; i < displayElements.length; i++) {
					if (elementCode === displayElements[i] || elementCode === "COMMON_PAGE" || elementCode === "MAIN_MENU_PAGE") {
						return true;
					}
				}
				return false;
			}
		},

		/**
		 * Check DASH Header Layout Access
		 * @function headerIconAccess
		 * @param {string} - elementCode (e.g. QUICKORDER_ICON)
		 */
		headerIconAccess: function () {
			var components = [{
				"Description": "QUICKORDER_ICON",
				"Id": "quickOrderIcon"
			}, {
				"Description": "CART_ICON",
				"Id": "quickCartItems"
			}, {
				"Description": "CART_VALUE",
				"Id": "quickCartValue"
			}, {
				"Description": "CART_ICON",
				"Id": "quickCartIcon"
			}];

			for (var i = 0; i < components.length; i++) {
				this.getView().byId(components[i].Id).setVisible(this.checkDASHLayoutAccess(components[i].Description));
			}
		},

		/**
		 * Returns the Device Type
		 * @function onGetDashDevice
		 * @returns noSupported, NARROW, MEDIUM or WIDE
		 */
		onGetDashDevice: function () {
			var width = sap.ui.Device.resize.width;
			var deviceScreen = "noSupported";

			if (width < 768) {
				deviceScreen = "NARROW";
			}
			if (width >= 768 && width < 1024) {
				deviceScreen = "MEDIUM";
			}
			if (width >= 1024) {
				deviceScreen = "WIDE";
			}
			return deviceScreen;
		},

		/**
		 * Account the time the Chat Live is open
		 * @function initializeCountTimeChatLive
		 */
		initializeCountTimeChatLive: function () {
			_timer = 0;

			function setTime() {
				++_timer;
			}

			_stopCountTime = setInterval(setTime, 1000);
		},

		/**
		 * Account the time the Chat Live is open
		 * @function endCountTimeChatLive
		 */
		endCountTimeChatLive: function () {
			clearInterval(_stopCountTime);

			var calculateSeconds = (_timer / 60);
			var roundSeconds = calculateSeconds.toFixed(0);

			//GTM
			_oController.GTMDataLayer('livechattime',
				'Live Chat Support',
				'Chat Time Duration',
				roundSeconds + "-Minutes"
			);

		},

		/**
		 * Closes the Valvoline Live Chat Support
		 * Its called in everyPage
		 * @function closeChatSupport
		 */
		closeChatSupport: function () {
			this.getParent().getParent().removeAllItems();
			_oController.endCountTimeChatLive();

			//GTM
			_oController.GTMDataLayer('livchatbttn',
				'Live Chat Support',
				'End Chat -click',
				sap.ui.getCore().getModel("oAccounts").getData().uuid
			);
		},

		/**
		 * Checks in the hybris mobile parameters if hybrish cart features should be shown
		 * @function shouldCartBeVisible
		 * @returns bVisible {bolean}
		 */
		shouldCartBeVisible: function (value) {
			var bVisibleQuickOrder, bVisibleShopCart;
			var mHybrisParameters = sap.ui.getCore().getModel("HybrisParameters");
			if (mHybrisParameters === undefined) {
				mHybrisParameters = new sap.ui.model.json.JSONModel();
				mHybrisParameters.setData(this.getGlobalParametersByGroup("HybrisParameters"));
				sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
			}
			bVisibleQuickOrder = mHybrisParameters.getData().showQuickOrder;
			bVisibleShopCart = mHybrisParameters.getData().showShop;
			if (bVisibleQuickOrder === "true") {
				this.getView().byId("quickOrderIcon").removeStyleClass("hidden-in-mobile");
			} else {
				this.getView().byId("quickOrderIcon").addStyleClass("hidden-in-mobile");
			}

			if (bVisibleShopCart === "true") {
				$("mobileQuickCartItems").removeClass("hidden-in-mobile");
				$("mobileQuickCartIcon").removeClass("hidden-in-mobile");
			} else {
				$("mobileQuickCartItems").addClass("hidden-in-mobile");
				$("mobileQuickCartIcon").addClass("hidden-in-mobile");
			}
			return true;
		},

		/**
		 * Navigation to hybris page trough header Icons
		 * @function onHeaderHybrisIcons
		 * @param {Object} oEvent - Triggered when clicking on header Icons
		 *
		 */
		onHeaderHybrisIcons: function (oEvent) {
			var hybrisLinks = this.getGlobalParametersByGroup("HybrisParameters");
			var IconPressed = oEvent.getSource().getId().split("quick")[1];
			var endpoint;
			switch (IconPressed) {
			case "OrderIcon":
				//GTM
				_oController.GTMDataLayer('headerlinksclick',
					'Header-click',
					'Quick Order',
					"#" + hybrisLinks["HybrisQuickForm"].replace("/my-account/order", "")
				);
				endpoint = hybrisLinks.HybrisQuickForm;
				break;
			case "CartIcon":
				//GTM
				_oController.GTMDataLayer('headerlinksclick',
					'Header-click',
					'Cart',
					"#" + hybrisLinks["HybrisCart"].replace("/my-account/order", "")
				);
				endpoint = hybrisLinks.HybrisCart;
				break;
			default:
				break;
			}
			_oController.nav2Hybris(hybrisLinks.Hybris + endpoint);
		},

		/**
		 * Get hybris shopping cart cookie values
		 * @function getShoppingCartCookie
		 */
		getShoppingCartCookie: function (controller) {
			var number = 0.00;

			var cartModel = {
				cartItem: "0",
				cartValue: number.toLocaleString(this.getResourceBundle().getText("LanguageCode"), {
					style: 'currency',
					currency: this.getResourceBundle().getText("CurrencyCode")
				})
			};
			var cartMoneyValue;
			if (sap.ui.getCore().getModel("cookies")) {
				var Cookie = sap.ui.getCore().getModel("cookies").getData().CartCookie;
				var ca = document.cookie.split('; ');

				for (var i = 0; i < ca.length; i++) {
					var c = ca[i];
					if (c.indexOf(Cookie) === 0) {
						var value = ca[i].split('=');
						var cartsplit = value[1].split('"');
						var cart = cartsplit[1].split(':');
						var cartvaluesplit = cart[1].split(cart[1][0]);
						cartMoneyValue = parseFloat(cartvaluesplit[1].replace(/[^\d\.\-]/g, ""));
						//						for (var j = 2; j < cart.length; j++) {
						//							cartMoneyValue = cartMoneyValue + "," + cart[j];
						//						}
						cartModel = {
							cartItem: cart[0],
							cartValue: cartMoneyValue.toLocaleString(this.getResourceBundle().getText("LanguageCode"), {
								style: 'currency',
								currency: this.getResourceBundle().getText("CurrencyCode")
							})
						};

					}

				}
			}
			var oModelCartValues = new sap.ui.model.json.JSONModel(cartModel);
			controller.getView().setModel(oModelCartValues, "cartValues");

			// Update the Mobile Cart Icon Model
			var sMobileCartId = $(".icon-quick-cart-items-mobile") && $(".icon-quick-cart-items-mobile")[0] && $(
				".icon-quick-cart-items-mobile")[0].id;
			if (sMobileCartId) {
				var oMobileCartControl = sap.ui.getCore().byId(sMobileCartId);
				if (oMobileCartControl) {
					oMobileCartControl.setModel(oModelCartValues, "cartValues");
				}
			}
		},

		/**
		 * Set hybris shopping cart cookie values
		 * @function setShoppingCartCookie
		 */
		setShoppingCartCookie: function (oController, itotalItems, itotalCartPrice) {
			var mCookie = sap.ui.getCore().getModel("cookies").getData();
			var Cookie = mCookie.CartCookie;
			var expiresText = ";expires=";
			// Allowing valvoline.com and all its subdomains
			var sDomainCookie = ";domain=" + mCookie.LanguageCookieDomain;
			// Set the cookie to expire in 1 year
			var sExpires = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toUTCString();
			var sCookieValue = '"' + itotalItems + ":" + itotalCartPrice + '"';
			document.cookie = Cookie + "=" + sCookieValue + expiresText + sExpires + sDomainCookie;
			oController.getShoppingCartCookie(oController);
		},

		/**
		 * Get Language from hybris shared language cookie
		 * @function getSelectedLanguage
		 */
		getSelectedLanguage: function () {
			var language = sap.ui.getCore().getModel("selectedLanguage");
			if (language !== undefined) {
				language = language.getData().selectedLanguage;
			} else {
				language = this.getLanguageFromCookie();
			}
			return language;
		},

		/**
		 * Get Language from hybris shared language cookie
		 * @function getSelectedLanguageForLangCode
		 */
		getSelectedLanguageForLangCode: function () {
			var language = sap.ui.getCore().getModel("selectedLanguage");
			if (language !== undefined) {
				language = language.getData().selectedLanguage;
			} else {
				var languageSettings = sap.ui.getCore().getModel("languageSettings");
				if (languageSettings === undefined) {
					var groupID = "LanguageParameters";
					languageSettings = new sap.ui.model.json.JSONModel(this.getGlobalParametersByGroup(groupID));
					sap.ui.getCore().setModel(languageSettings, "languageSettings");
					this.getView().setModel(languageSettings, "languageSettings");
					language = languageSettings.getData().defaultLanguage;
				} else {
					language = languageSettings.getData().defaultLanguage;
				}
			}
			return language;
		},

		/**
		 * Get Language from hybris shared language cookie
		 * @function getLanguageFromCookie
		 */
		getLanguageFromCookie: function () {
			var Cookie = sap.ui.getCore().getModel("cookies").getData().LanguageCookie;
			var language = "";
			var decodedCookie = decodeURIComponent(document.cookie);
			var ca = decodedCookie.split(' ').join('').split(';');
			for (var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) === ' ') {
					c = c.substring(1);
				}
				if (c.indexOf(Cookie) === 0) {
					language = c.substring(Cookie.length + 1, c.length);
				}
			}
			var languages = sap.ui.getCore().getModel("languages");
			if (languages === undefined) {
				languages = new sap.ui.model.json.JSONModel(this.getDomainsByName("LanguageCode"));
				sap.ui.getCore().setModel(languages, "languages");
				this.getView().setModel(languages, "languages");
			}
			var validLang = false;
			for (i = 0; i < languages.getData().length; i++) {
				if (languages.getData()[i].value === language) {
					validLang = true;
				}
			}

			if (validLang === false) {
				var deviceLang;
				if (sap.ui.Device.browser.msie) {
					//Set the desired language as the request-header most desired language
					deviceLang = sap.ui.getCore().getModel("reqHeaderLang").getData().AcceptLanguage;

				} else {
					//Set the language as the SAP language preferences recognizes
					deviceLang = sap.ui.getCore().getConfiguration().getLanguage();
				}
				var groupID, languageSettings;
				if (deviceLang !== undefined) {
					for (i = 0; i < languages.getData().length; i++) {
						if (languages.getData()[i].value === deviceLang) {
							validLang = true;
						}
					}
					if (validLang) {
						//Set browser Language as Dash language
						language = deviceLang;
					} else {
						languageSettings = sap.ui.getCore().getModel("languageSettings");
						if (languageSettings === undefined) {
							groupID = "LanguageParameters";
							languageSettings = new sap.ui.model.json.JSONModel(this.getDomainsByName(groupID));
							sap.ui.getCore().setModel(languageSettings, "languageSettings");
							this.getView().setModel(languageSettings, "languageSettings");
							language = languageSettings.getData().defaultLanguage;
						} else {
							language = languageSettings.getData().defaultLanguage;
						}
					}
				} else {
					languageSettings = sap.ui.getCore().getModel("languageSettings");
					if (languageSettings === undefined) {
						groupID = "LanguageParameters";
						languageSettings = new sap.ui.model.json.JSONModel(this.getDomainsByName(groupID));
						sap.ui.getCore().setModel(languageSettings, "languageSettings");
						this.getView().setModel(languageSettings, "languageSettings");
						language = languageSettings.getData().defaultLanguage;
					} else {
						language = languageSettings.getData().defaultLanguage;
					}
				}
			}
			return language;
		},

		/**
		 * Set hybris shared language cookie
		 * @function setLanguageCookie
		 */
		setLanguageCookie: function (cvalue) {
			var mCookie = sap.ui.getCore().getModel("cookies").getData();
			var Cookie = mCookie.LanguageCookie;
			var expiresText = ";expires=";
			// Allowing valvoline.com and all its subdomains
			var domainCookie = ";domain=" + mCookie.LanguageCookieDomain;
			// Set the cookie to expire in 1 year
			var expires = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toUTCString();
			document.cookie = Cookie + "=" + cvalue + expiresText + expires + domainCookie;
		},

		/**
		 * Shows a message when a error occurs
		 * @function showServerErrorMsg OLD
		 * @function onShowMessage
		 * @param {string} controller
		 * @param {string} statusCode - Error status code
		 * @param {string} message - Error message
		 * @param {string} responseText - Error response text
		 * @param {string} serviceLocation - Error service location
		 */
		onShowMessage: function (controller, type, statusCode, message, responseText, serviceLocation, classContainer) {
			var faultstring = "";
			var errorcode = "";
			var isValidJSON = true;
			var responseTextJSON = null;

			// Error Data Configuration
			if (!statusCode) {
				statusCode = "";
			}
			if (!message) {
				message = "";
			}
			if (!responseText) {
				responseText = "";
			}
			if (!serviceLocation) {
				serviceLocation = "";
			}

			try {
				responseTextJSON = JSON.parse(responseText);
			} catch (e) {
				isValidJSON = false;
			}

			if (isValidJSON) {
				if (responseTextJSON.fault) {
					faultstring = responseTextJSON.fault.faultstring;
					errorcode = responseTextJSON.fault.detail.errorcode;
				} else if (responseTextJSON.error) {
					faultstring = responseTextJSON.error.message.value;
					errorcode = responseTextJSON.error.code;
				} else {
					faultstring = message;
					errorcode = statusCode;
				}
			} else {
				faultstring = message;
				errorcode = statusCode;
			}

			// 1 Message Strip HBox
			var messageStrip = new sap.m.HBox({
				justifyContent: "Center",
				alignItems: "Center"
			}).addStyleClass("messageContainer");

			// 2 Message Container HBox
			var messageStripInformationContainer_main = new sap.m.HBox({
				justifyContent: "SpaceBetween"
			}).addStyleClass("minHeightMessages");

			// Adding element width
			messageStripInformationContainer_main.addStyleClass("reusable-dash-message-container");

			// 3. Message Left Container
			var messageStripInformationContainer_0 = new sap.m.HBox({
				alignItems: "Center"
			});

			// 3.1 Left "X" Icon
			var mainIcon = new sap.ui.core.Icon({
				src: "sap-icon://decline",
				color: "white"
			}).addStyleClass("messageStripIconSize sapUiSmallMarginStart");

			// 3.2 Container for the Text
			var vbox = new sap.m.VBox({
				width: "100%"
			});
			var oHBox = new sap.m.HBox({
				width: "100%",
				renderType: "Bare"
			});

			// 3.2.1 Upper Text
			var Text_0 = new sap.m.Text({
				text: controller.getResourceBundle().getText("SCVPInfoMsgTXT")
			}).addStyleClass("messageContainerText");

			// 3.2.2 Lower Text
			var Text_1 = new sap.m.Text({}).addStyleClass("messageContainerText");
			var oLink_1 = new sap.m.Link({
				press: controller.goToContacts
			}).addStyleClass("messageContainerLink");

			// Set 3.2.1 and 3.2.2 in 3.2 container
			vbox.addItem(Text_0);
			oHBox.addItem(Text_1);
			oHBox.addItem(oLink_1);
			vbox.addItem(oHBox);

			// Set 3.1 and 3.2 in 3. container
			messageStripInformationContainer_0.addItem(mainIcon);
			messageStripInformationContainer_0.addItem(vbox);

			// 4 Message Right Container
			var messageStripInformationContainer_1 = new sap.m.HBox({
				alignItems: "Center"
			});

			// 4.1 Close Icon
			var closeIcon = new sap.ui.core.Icon({
				src: "sap-icon://decline",
				color: "white",
				press: this.clearMessageStrip,
				useIconTooltip: false
			});

			// Set 4.1 in 4 container
			messageStripInformationContainer_1.addItem(closeIcon);

			// Set 3. and 4. container in 2. box
			messageStripInformationContainer_main.addItem(messageStripInformationContainer_0);
			messageStripInformationContainer_main.addItem(messageStripInformationContainer_1);

			// Set 2. container in 1. Main container
			messageStrip.addItem(messageStripInformationContainer_main);

			// Add Message to the Container
			var messageContainer = controller.byId("messageContainer");
			if (messageContainer) {
				messageContainer.addItem(messageStrip);
			}

			messageContainer.rerender();

			//re-add the message container shadow
			messageContainer.removeStyleClass("message-container-no-shadow");
			// Message Type Configuration
			switch (type) {
			case "Success":
				//  Message Strip Color
				messageStrip.addStyleClass("messageContainerSuccessColor");
				// Main Icon
				mainIcon.setSrc("sap-icon://customfont/success");
				// Text
				Text_0.setText(controller.getResourceBundle().getText("SCVPSuccessTXT")).addStyleClass("hidden");
				if (message === "") {
					Text_1.setText(controller.getResourceBundle().getText("SCVPSuccessTXTMSG"));
				} else {
					Text_1.setText(message);
				}
				break;
			case "SuccessSmall":
				//  Message Strip Color
				messageStrip.addStyleClass("messageContainerSmallSuccessColor");
				// Main Icon
				mainIcon.setSrc("sap-icon://customfont/success");
				// Text
				Text_0.setText(controller.getResourceBundle().getText("SCVPSuccessTXT")).addStyleClass("hidden");
				if (message === "") {
					Text_1.setText(controller.getResourceBundle().getText("SCVPSuccessTXTMSG"));
				} else {
					Text_1.setText(message);
				}
				//remove the message container shadow
				messageContainer.addStyleClass("message-container-no-shadow");
				// Auto Close Success Message
				jQuery.sap.delayedCall(10000, controller, function () {
					messageStrip.destroy();
					messageContainer.removeItem(messageStrip);
					var iTotalMessages = messageContainer.getItems().length;
					if (iTotalMessages > 0 && !messageContainer.getItems()[iTotalMessages - 1].hasStyleClass("messageContainerSmallSuccessColor")) {
						//re-add the message container shadow
						messageContainer.removeStyleClass("message-container-no-shadow");
					}
				});
				break;
			case "ErrorContactUs":
				oHBox.addStyleClass("message-strip-error-contact-us");
				//  Message Strip Color
				messageStrip.addStyleClass("messageContainerErrorColor");
				// Main Icon
				mainIcon.setSrc(sap.ui.core.IconPool.getIconURI("close", "customfont"));
				// Text
				Text_0.addStyleClass("hidden");
				if (controller.onGetDashDevice() === "NARROW") {
					Text_1.addStyleClass("hidden");
					oLink_1.setText(message);
					oLink_1.addStyleClass("message-strip-error-contact-us messageContainerLink");
				} else {
					var sInitialMsg = message.substring(0, message.lastIndexOf(controller.getResourceBundle().getText(
						"PSSAdd2CartErrorContactUsMsg")));
					Text_1.setText(sInitialMsg + " ");
					oLink_1.setText(controller.getResourceBundle().getText("PSSAdd2CartErrorContactUsMsg"));
				}
				break;
			case "Error":
				//  Message Strip Color
				messageStrip.addStyleClass("messageContainerErrorColor");
				// Main Icon
				mainIcon.setSrc(sap.ui.core.IconPool.getIconURI("close", "customfont"));
				// Text
				Text_0.setText(controller.getResourceBundle().getText("SCVPErrorTXT")).addStyleClass("hidden");
				Text_1.setText(controller.getResourceBundle().getText("SCVPErrorMessageTXT") + " " + faultstring + " | " + controller.getResourceBundle()
					.getText("SCVPDescriptionTXT") + " " + message + " (" + controller.getResourceBundle().getText("SCVPErrorCodeTXT") + " " +
					errorcode + ")");
				break;
			case "WarningFade":
				//  Message Strip Color
				messageStrip.addStyleClass("messageContainerWarningColor");
				// Main Icon
				mainIcon.setSrc(sap.ui.core.IconPool.getIconURI("alert", "customfont"));
				// Text
				Text_0.addStyleClass("hidden");
				Text_1.setText(message);
				// Auto Close Success Message
				jQuery.sap.delayedCall(10000, controller, function () {
					messageStrip.destroy();
					messageContainer.removeItem(messageStrip);
					var iTotalMessages = messageContainer.getItems().length;
					if (iTotalMessages > 0 && messageContainer.getItems()[iTotalMessages - 1].hasStyleClass("messageContainerSmallSuccessColor")) {
						//re-add the message container shadow
						messageContainer.addStyleClass("message-container-no-shadow");
					}
				});
				break;
			case "Warning":
				//  Message Strip Color
				messageStrip.addStyleClass("messageContainerWarningColor");
				// Main Icon
				mainIcon.setSrc(sap.ui.core.IconPool.getIconURI("alert", "customfont"));
				// Text
				Text_0.setText(controller.getResourceBundle().getText("SCVPWarningTXT")).addStyleClass("hidden");
				Text_1.setText(message);
				break;
			default:
				break;
			}
		},

		/**
		 * Clear the server error message generated before
		 */
		clearMessageStrip: function (oEvent) {
			oEvent.getSource().getParent().addStyleClass("hidden").getParent().destroy();
		},

		/**
		 * Clear the server error message generated before
		 * @function clearServerErrorMsg
		 * @param {Object} controller - controller of the page where the function is called
		 */
		clearServerErrorMsg: function (controller) {
			var messageContainer = controller.byId("messageContainer");
			if (messageContainer) {
				messageContainer.removeAllItems();
			}
		},

		clearServerErrorMsgStrip: function () {
			var messageContainer = sap.ui.getCore().byId("messageStripContainer");
			if (messageContainer) {
				messageContainer.removeAllItems();
			}
		},

		/**
		 * Generate a message Strip to be displayed in Fragments
		 * @function onShowMessageStrip
		 * @param {string} text - Text to be displayed
		 * @param {string} type - Type of message
		 */
		onShowMessageStrip: function (text, type) {
			var messageStripContainer = sap.ui.getCore().byId("messageStripContainer");
			var messageStrip = new sap.m.MessageStrip({
				text: text,
				type: type,
				width: "100%",
				showCloseButton: true,
				showIcon: true
			});
			messageStripContainer.addItem(messageStrip);
		},

		/**
		 * onChangeLanguage
		 * @ parameter (oEvent)
		 */
		onChangeLanguage: function (oEvent) {
			var selectedLanguage, oLanguages, language, oModelDefaultLanguage;
			_oController = this;

			if (!oEvent) {

				if (!sap.ui.getCore().getModel("selectedLanguage")) {
					// Set the default language at start up
					var enable;
					var languageSettings = sap.ui.getCore().getModel("languageSettings");
					if (languageSettings === undefined) {
						//Retrieve language Settings if they are not yet available
						var groupID = "LanguageParameters";
						languageSettings = new sap.ui.model.json.JSONModel(this.getGlobalParametersByGroup(groupID));
						sap.ui.getCore().setModel(languageSettings, "languageSettings");
						this.getView().setModel(languageSettings, "languageSettings");
						enable = languageSettings.getData().languageEnabled;
					} else {
						enable = languageSettings.getData().languageEnabled;
					}
					if (enable === 'false' && sap.ui.getCore().getConfiguration().getLanguage() !== languageSettings.getData().defaultLanguage) {
						selectedLanguage = languageSettings.getData().defaultLanguage;
						sap.ui.getCore().getConfiguration().setLanguage(selectedLanguage);
					} else {
						//check if the cookie is availablabe, otherwise retrieve the default language from service
						selectedLanguage = this.getLanguageFromCookie();
					}
					language = {
						selectedLanguage: selectedLanguage
					};
					oModelDefaultLanguage = new sap.ui.model.json.JSONModel(language);
					sap.ui.getCore().setModel(oModelDefaultLanguage, "selectedLanguage");
					this.getView().setModel(oModelDefaultLanguage, "selectedLanguage");

					//Set the model for the language dropdown
					sap.ui.getCore().getConfiguration().setLanguage(selectedLanguage);
					oLanguages = new sap.ui.model.json.JSONModel(this.getDomainsByName("LanguageCode"));
					sap.ui.getCore().setModel(oLanguages, "languages");
					this.getView().setModel(oLanguages, "languages");
				} else {
					selectedLanguage = sap.ui.getCore().getModel("selectedLanguage").getData().selectedLanguage;
					language = {
						selectedLanguage: selectedLanguage
					};
					oModelDefaultLanguage = new sap.ui.model.json.JSONModel(language);
					sap.ui.getCore().setModel(oModelDefaultLanguage, "selectedLanguage");
					this.getView().setModel(oModelDefaultLanguage, "selectedLanguage");

					this.getView().byId("selectLanguage").setSelectedKey(selectedLanguage);
					sap.ui.getCore().getConfiguration().setLanguage(selectedLanguage);
					oLanguages = new sap.ui.model.json.JSONModel(sap.ui.getCore().getModel("languages").getData());
					this.getView().setModel(oLanguages, "languages");
				}
			} else {

				sap.ui.getCore().byId("dashShell").setBusyIndicatorDelay(0);
				sap.ui.getCore().byId("dashShell").setBusy(true);
				// Clears all error message
				_oController.clearServerErrorMsg(_oController);

				//Set selected language based on the dropdown selection
				selectedLanguage = oEvent.languageToSet || oEvent.getSource().getSelectedKey();
				setTimeout(function () {
					sap.ui.getCore().getConfiguration().setLanguage(selectedLanguage);
					language = {
						selectedLanguage: selectedLanguage
					};
					var oModelLanguage = new sap.ui.model.json.JSONModel(language);
					sap.ui.getCore().setModel(oModelLanguage, "selectedLanguage");
					_oController.getView().setModel(oModelLanguage, "selectedLanguage");

					// Functions to be called to Reset Language in accordance with route 
					var currentRoute = _oController.getRouter()._oRouter._prevMatchedRequest.toLowerCase();
					switch (currentRoute) {
					case "feed":
						_oController.fillSelect();
						_oController.populateCardContainer("Feed", "1", "All");
						_oController.checkMonthlyPaymentWarn(_oController);
						break;
					case "insights":
						_oController.populateCardContainer("Insights", "3", "All");
						_oController.getAccountsByDistrict(_oController, "comboBoxInsights", true);
						break;
					case "insights/engineoil":
					case "insights/vps":
						_oController.getAccountsByDistrict(_oController, "comboBoxInsightsGraphDetails", true);
						break;
					case "insightsdetails/premiumoil":
					case "insightsdetails/wiperunits":
					case "insightsdetails/antifreeze":
					case "insightsdetails/filterunits":
						_oController.getAccountsByDistrict(_oController, "comboBoxInsights", true);
						break;
					case "learn":
						_oController.populateCardContainer("Learn VU", "4", "All");
						break;
					case "promote":
						_oController.populateCardContainer("Promote", "7", "All");
						break;
					case "solutions":
						_oController.populateCardContainer("Service", "6");
						break;
					case "promote/summary":
						_oController.populateCardContainer("PromotionsSummary", "8", "All");
						break;
					case "myaccount":
						_oController.checkMonthlyPaymentWarn(_oController);
						_oController.populateCardContainer("My Account", "9");
						break;
					case "myaccount/supplementalrate":
						_oController.checkMonthlyPaymentWarn(_oController);
						_oController.loadModelSupplementalRates();
						break;
					case "myaccount/supplementalvalidationclaims/false":
						_oController.claimStatusFilterValues(false);
						_oController.loadModelValidationClaims();
						break;
					case "myaccount/supplementalvalidationclaims/true":
						_oController.claimStatusFilterValues(false);
						break;
					case "myaccount/supplementalvalidationclaims":
						_oController.claimStatusFilterValues(false);
						break;
					case "myaccount/usermanagement":
						_oController.userInformationFunctionalRolesModel();
						break;
					case "myaccount/equipment":
						_oController.getAccountsByDistrict(_oController, "comboBoxAccountEquipment", true);
						break;
					case "myaccount/oilanalysis":
						_oController.rebuildCharts();
						_oController.getAccountsByDistrict(_oController, "comboBoxAccountOilAnalysis", true);
						//If The browser is Safari the download export file does not ocour if the model is not preloaded, so a pre-download of the model is needed
						if (sap.ui.Device.browser.safari) {
							_oController.prepareExport();
						} else {
							_oController.getView().setModel(new sap.ui.model.json.JSONModel({}), "exportDataModel");
						}
						break;
					case "myaccount/cases/0":
						_oController.casesListPagination(true);
						_oController.casesListPagination(false);
						_oController.setSearchResultMessage();
						_oController.getAccountsByDistrict(_oController, "comboBoxAccountCases", true);
						break;
					case "orders":
						_oController.populateCardContainer("Orders", "10");
						break;
					case "orderstatus":
						_oController.getAccountsByDistrict(_oController, "comboBoxAccountOrders", true, _oController.toRunAfterGettingUserInfo);
						break;
					case "recruiting":
						_oController.getPricingOptions();
						break;
					case "insightshd":
						_oController.getAccountsByDistrict(_oController, "comboBoxInsightsHD", true);
						_oController.YearCookieValues();
						break;
					case "products":
						_oController.populateCardContainer("Products", "11");
						break;
					case "products/stockingsolution":
						_oController.getStockingSolution(_oController.getView().getModel("oSelectedShipTo").getData().accountnum);
						break;
					case "products/productfinder":
						_oController.onPlaceHolderSelectBox();
						_oController.onClearInputFields();
						_oController.onConfigVehicleCategoryButtonUnpress();
						break;
					case "productscategory":
						_oController.onPlaceHolderSelectBox();
						break;
					case "routeschedule":
						_oController.loadWarehouses();
						break;
					default:
						if (currentRoute.indexOf("myaccount/cases") !== -1) {
							_oController.getAccountsByDistrict(_oController, "comboBoxAccountCases", true);
						}
						break;
					}
					_oController.getShoppingCartCookie(_oController);
					//set language cookie
					_oController.setLanguageCookie(sap.ui.getCore().getModel("selectedLanguage").getData().selectedLanguage);
					//set model to footer dropdown
					oLanguages = new sap.ui.model.json.JSONModel(_oController.getDomainsByName("LanguageCode"));
					sap.ui.getCore().setModel(oLanguages, "languages");
					_oController.getView().setModel(oLanguages, "languages");
					sap.ui.getCore().byId("dashShell").setBusy(false);
				}, 0);
				// Clear the Search History for the products page
				//Initialize Storage
				/*jQuery.sap.require('jquery.sap.storage');
				var oStore = jQuery.sap.storage(jQuery.sap.storage.Type.local);
				oStore.remove("SearchHistory");
				if(this.getView().getModel("vehicleBrand") !== undefined){
					this.getView().setModel(undefined,"vehicleBrand");
				}*/

				//GTM
				if (!oEvent.languageToSet) {
					_oController.GTMDataLayer('langselect',
						'Website Language',
						'DropDown Language Selection -click',
						oEvent.getSource().getSelectedKey()
					);
				}
			}

			return selectedLanguage;
		},

		/**
		 * Define if language combobox is enabled
		 */
		checkLanguageEnabled: function () {
			var enable;
			var languageSettings = sap.ui.getCore().getModel("languageSettings");
			if (languageSettings === undefined) {
				//Retrieve language Settings if they are not yet available
				var groupID = "LanguageParameters";
				languageSettings = new sap.ui.model.json.JSONModel(this.getGlobalParametersByGroup(groupID));
				sap.ui.getCore().setModel(languageSettings, "languageSettings");
				this.getView().setModel(languageSettings, "languageSettings");
				enable = languageSettings.getData().languageEnabled;
			} else {
				enable = languageSettings.getData().languageEnabled;
			}
			if (enable === 'false' && sap.ui.getCore().getConfiguration().getLanguage() !== languageSettings.getData().defaultLanguage) {
				sap.ui.getCore().getConfiguration().setLanguage(languageSettings.getData().defaultLanguage);
			}
			return (enable === 'true');
		},

		/**
		 * Hide all menu icons
		 */
		hideAllIcons: function () {
			//disable all sidebar buttons
			$(".js-shop-sidenav").addClass("hidden");
			$(".js-productsEcom-sidenav").addClass("hidden");
			$(".js-products-sidenav").addClass("hidden");
			$(".js-orders-sidenav").addClass("hidden");
			$(".js-insights-sidenav").addClass("hidden");
			$(".js-learn-sidenav").addClass("hidden");
			$(".js-services-sidenav").addClass("hidden");
			$(".js-promote-sidenav").addClass("hidden");
		},

		/**
		 * Formats values with grouping separator.
		 * @function formatValuesGroupingSeparator
		 * @param {string} sValue - value to be formatted
		 * @returns Formatted values
		 */
		formatValuesGroupingSeparator: function (sValue) {
			jQuery.sap.require("sap.ui.core.format.NumberFormat");
			if (sValue === "NaN") {
				return "";
			} else {
				var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
				var oFormatOptions = {
					style: "standard",
					decimals: 0,
					shortDecimals: 0
				};
				var oFloatFormat = sap.ui.core.format.NumberFormat.getFloatInstance(oFormatOptions, oLocale);
				var sValueFormatted = oFloatFormat.format(sValue);
				return sValueFormatted;
			}
		},

		/**
		 * Formats Values with grouping and decimal separators
		 * @function formatValuesGroupingDecimalSeparators
		 * @param sValue {sValue} - Value to be formatted
		 * @returns formatted sValue
		 */
		formatValuesGroupingDecimalSeparators: function (sValue) {
			jQuery.sap.require("sap.ui.core.format.NumberFormat");
			if (sValue === "NaN") {
				return "";
			} else {
				var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
				var oFormatOptions = {
					style: "standard",
					maxFractionDigits: 2,
					decimals: 2,
					shortDecimals: 2
				};
				var oFloatFormat = sap.ui.core.format.NumberFormat.getFloatInstance(oFormatOptions, oLocale);
				var sValueFormatted = oFloatFormat.format(sValue);
				return sValueFormatted;
			}
		},

		/**
		 * Formats Values with grouping and decimal separators
		 * @function formatValuesGroupingDecimalSeparatorsUOM
		 * @param sValue {sValue} - Value to be formatted
		 * @param sUOM {sUOM} - UOM to be concatenated to the sValue formatted.
		 * @returns formatted sValue concatenated with sUOM
		 */
		formatValuesGroupingDecimalSeparatorsUOM: function (sValue, sUOM) {
			jQuery.sap.require("sap.ui.core.format.NumberFormat");
			if (sValue === "NaN") {
				return "";
			} else {
				var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
				var oFormatOptions = {
					style: "standard",
					maxFractionDigits: 2,
					decimals: 2,
					shortDecimals: 2
				};
				var oFloatFormat = sap.ui.core.format.NumberFormat.getFloatInstance(oFormatOptions, oLocale);
				var sValueFormatted = oFloatFormat.format(sValue);
				if (sUOM !== undefined && sUOM.length !== 0) {
					return sValueFormatted + " " + sUOM;
				} else {
					return sValueFormatted;
				}
			}
		},

		/**
		 * Function that handles the success case of the ajax call that gets the ImpersonateDetails
		 * @function onImpersonateDetailsSuccess
		 */
		onImpersonateDetailsSuccess: function (oController, oData, oPiggyBack) {
			var oModelImpersonate = new sap.ui.model.json.JSONModel(oData);

			if (oData.UsernameImpersonator) {
				oData.impersonateStatus = true;
				var toolTipText = oController.getResourceBundle().getText("ImperUser") +
					oModelImpersonate.getProperty("/UsernameImpersonator") + "\n" +
					oController.getResourceBundle().getText("ImperUuid") + oModelImpersonate.getProperty("/UuidImpersonator");
				var userButton = oController.byId("userButton");
				userButton.setTooltip(toolTipText);
				userButton.addStyleClass("impersonateIconChanges");

				ga('set', 'dimension3', "Impersonation Yes");
				ga('set', 'dimension4', oData.UsernameImpersonator);

			} else {
				oData.impersonateStatus = false;
				if (oController.byId("userButton")) {
					oController.byId("userButton").removeStyleClass("impersonateIconChanges");
				}
				ga('set', 'dimension3', "NOT Impersonation");
				ga('set', 'dimension4', "");
			}
			sap.ui.getCore().setModel(oModelImpersonate, "oModelImpersonate");
			oController.getView().setModel(oModelImpersonate, "oModelImpersonate");
			oPiggyBack.resolve();
		},

		/**
		 * Function that handles the error case of the ajax call that gets the ImpersonateDetails
		 * @function onImpersonateDetailsError
		 */
		onImpersonateDetailsError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText);
		},

		/**
		 * ImpersonateDetails
		 * @ parameter {object} controller
		 * @ parameter {object} resolve
		 */
		impersonateDetails: function (controller, resolve) {
			var oModelImpersonate = sap.ui.getCore().getModel("oModelImpersonate");
			var userButton;
			var toolTipText;

			/**
			 * If there is Model set the tooltip and classStyle,
			 * If not, call the service
			 */
			if (oModelImpersonate) {
				if (oModelImpersonate.getProperty("/impersonateStatus")) {
					toolTipText = controller.getResourceBundle().getText("ImperUser") +
						oModelImpersonate.getProperty("/UsernameImpersonator") + "\n" +
						controller.getResourceBundle().getText("ImperUuid") + oModelImpersonate.getProperty("/UuidImpersonator");
					userButton = controller.byId("userButton");
					userButton.setTooltip(toolTipText);
					userButton.addStyleClass("impersonateIconChanges");
				}
			} else {
				var xsURL = "/core/impersonateDetails";
				var oPiggyBack = {
					"resolve": resolve
				};
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onImpersonateDetailsSuccess, _oController.onImpersonateDetailsError,
					null, oPiggyBack);
			}
		},

		/**
		 * Configure Mobile Menu
		 * if there is User Joins the first Name and Last name to be showed in the first button
		 * if there is no accounts disable all buttons
		 */
		configureMobileMenu: function () {
			if (sap.ui.getCore().getModel("oAccounts")) {
				var oAccounts = sap.ui.getCore().getModel("oAccounts");
				this.getView().byId("menuMobileBar").setVisible(true);
				this.getView().byId("mobileUser").setTitle(this.joinNames(oAccounts.getProperty("/firstname"), oAccounts.getProperty(
					"/lastname")));
			}
			var impersonateModel = sap.ui.getCore().getModel("oModelImpersonate");
			if (impersonateModel !== undefined && impersonateModel.getData().impersonateStatus === true) {
				this.getView().byId("mobileLogout").setVisible(false);
			}

			var mHybrisParameters = sap.ui.getCore().getModel("HybrisParameters");
			if (mHybrisParameters === undefined) {
				mHybrisParameters = new sap.ui.model.json.JSONModel();
				mHybrisParameters.setData(this.getGlobalParametersByGroup("HybrisParameters"));
				sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
			}

			// Configure Mobile Menu
			if (!sap.ui.getCore().getModel("oAccounts") || (sap.ui.getCore().getModel("oAccounts").getData().directAccount !== sap.ui.getCore()
					.getModel("oAccounts").getData().accounts[0].id)) {
				this.getView().byId("menuMobileBar").setVisible(true);
				this.getView().byId("mobileShop").setVisible(false);
				this.getView().byId("mobileOrders").setVisible(false);
				this.getView().byId("mobileInsights").setVisible(false);
				this.getView().byId("mobileLearn").setVisible(false);
				this.getView().byId("mobileServices").setVisible(false);
				this.getView().byId("mobilePromote").setVisible(false);
				this.getView().byId("mobileProducts").setVisible(false);
				this.getView().byId("mobileProductsEcom").setVisible(false);
				this.getView().byId("mobileUser").setVisible(false);
			}
		},

		/**
		 * This function show/hide the Mobile Menu
		 * @function showMobileMenu
		 */
		showMobileMenu: function () {
			if (_oMenuMobileState === true) {
				this.getView().byId("menuMobileBar").setVisible(false);
				this.getView().byId("menuMobileButton").setIcon("sap-icon://menu2");
				_oMenuMobileState = false;
			} else {
				this.configureMobileMenu();
				this.getView().byId("menuMobileBar").setVisible(true);
				this.getView().byId("menuMobileButton").setIcon("sap-icon://decline");
				_oMenuMobileState = true;
			}
		},

		/**
		 * Expires the cart cookie.
		 */
		deleteCartCookie: function () {
			//delete cart cookie
			var cookieName = sap.ui.getCore().getModel("cookies").getData().CartCookie;
			document.cookie = cookieName + '="0:$0.00"; expires=Thu, 18 Dec 2013 12:00:00 UTC; domain=.valvoline.com;';
		},

		/**
		 * Function that handles the success case of the ajax call that sets if/which recruiting card should appear on solutions page
		 * @function onLogicDisplayRecruitingCardSuccess
		 */
		onLogicDisplayRecruitingCardSuccess: function (oController, oData, oPiggyBack) {
			if (oData !== undefined) {
				var cardToShow;
				for (var i = 0; i < oData.AccessList.length; i++) {
					if (oData.AccessList[i] === "RECRUITING_ACCESS_CARD") {
						cardToShow = "RECRUITING_ACCESS_CARD";
					} else if (oData.AccessList[i] === "RECRUITING_INFO_CARD") {
						cardToShow = "RECRUITING_INFO_CARD";
					}
				}
			} else {
				return;
			}
			return cardToShow;
		},

		/**
		 * Function that handles the error case of the ajax call that sets if/which recruiting card should appear on solutions page
		 * @function onLogicDisplayRecruitingCardError
		 */
		onLogicDisplayRecruitingCardError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"RPerrorsubTXT"));
		},

		/**
		 * Function that handles the success case of the ajax call to display recruiting access card
		 * @function onParameterRecruitingAccessCardSuccess
		 */
		onParameterRecruitingAccessCardSuccess: function (oController, oData, oPiggyBack) {
			var card = {
				title: oController.getResourceBundle().getText("RCTitle"),
				cardTypeId: "200",
				contentCardSections: [{
					subtitle: oController.getResourceBundle().getText("RCSubtitle"),
					description: oController.getResourceBundle().getText("RCDescription1"),
					callToActionLabel: oController.getResourceBundle().getText("RCCallToActionLabel1"),
					callToActionURL: oData[0].value,
					urlTarget: "New Tab",
					mediaURL: "resources/img/RecruitingCard.jpg",
					mediatype: "0",
					isBackToDash: "0"
				}]
			};
			return card;
		},

		/**
		 * Function that handles the error case of the ajax call to display recruiting access card
		 * @function onParameterRecruitingAccessCardError
		 */
		onParameterRecruitingAccessCardError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"RPerrorsubTXT"));
		},

		/**
		 * This function choose if/which recruiting card should appear on solutions page
		 * @function logicDisplayRecruitingCard
		 */
		logicDisplayRecruitingCard: function () {
			var controller = this;
			var card;
			var xsURL = "/auth/recruitingAccessList";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			var cardToShow = controller.handleAjaxJSONCall(controller, false, xsURL, "GET", controller.onLogicDisplayRecruitingCardSuccess,
				controller.onLogicDisplayRecruitingCardError);

			if (cardToShow === "RECRUITING_ACCESS_CARD") {
				var nameID = "recruitingURL";
				xsURL = "/core/parameterListByNameID" + "?nameID=" + nameID;
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				card = _oController.handleAjaxJSONCall(_oController, false, xsURL, "GET", _oController.onParameterRecruitingAccessCardSuccess,
					_oController.onParameterRecruitingAccessCardError);
			} else if (cardToShow === "RECRUITING_INFO_CARD") {
				card = {
					title: _oController.getResourceBundle().getText("RCTitle"),
					cardTypeId: "200",
					contentCardSections: [{
						subtitle: _oController.getResourceBundle().getText("RCSubtitle"),
						description: _oController.getResourceBundle().getText("RCDescription2"),
						callToActionLabel: _oController.getResourceBundle().getText("RCCallToActionLabel2"),
						callToActionURL: "recruiting",
						urlTarget: "Route",
						mediaURL: "resources/img/RecruitingCard.jpg",
						mediatype: "0",
						isBackToDash: "0"
					}]
				};
			}

			return card;

		},

		/**
		 * Function that handles the success case of the ajax call that checks if is the last day of the month and the user hasn't submited
		 * a request for monthly payment, if so, show a warning message
		 * @function onCheckMonthlyPaymentWarnSuccess
		 */
		onCheckMonthlyPaymentWarnSuccess: function (oController, oData, oPiggyBack) {
			if (oData !== "false") {
				if (oController.getView().getViewName().indexOf("MyAccountSupplementalRate") === -1) {
					_oController.onShowMessage(oController, "Warning", null, oController.getResourceBundle().getText(
						"MASCMonthPayReminderTXT") + " " + oPiggyBack.remainingDays + " " + oController.getResourceBundle().getText(
						"MASCMonthPayReminderTXT_1"));
				} else {
					_oController.onShowMessage(oController, "Warning", null, oController.getResourceBundle().getText(
						"MASCMonthPayReminderTXT") + " " + oPiggyBack.remainingDays + " " + oController.getResourceBundle().getText(
						"MASCMonthPayReminderTXT_1"), undefined, undefined);
				}
			}
		},

		/**
		 * Function that handles the error case of the ajax call that checks if is the last day of the month and the user hasn't submited
		 * a request for monthly payment, if so, show a warning message
		 * @function onCheckMonthlyPaymentWarnError
		 */
		onCheckMonthlyPaymentWarnError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"UMLoadUserManagement"));
		},

		/**
		 * Checks if is the last day of the month and the user hasn't submited a request for monthly payment, if so, show a warning message.
		 * @function checkMonthlyPaymentWarn
		 **/
		checkMonthlyPaymentWarn: function (controller) {
			var today = _oController.getServerTime();

			var thisMonth = today.getMonth();

			var fstDayNextMont = new Date(today.getFullYear(), thisMonth + 1, 1);

			var timeDiff = Math.abs(fstDayNextMont.getTime() - today.getTime());
			var remainingDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) - 1;

			var limitDays = _oController.getGlobalParameter("daysSubmitPayment");
			if (_oController.checkDASHLayoutAccess("SUPPLEMENTAL_CLAIM_PAGE") && limitDays !== undefined && limitDays !== null &&
				remainingDays <=
				parseInt(limitDays[0].value, 10)) {
				//check if user hasn't submited monthly payment
				var xsURL = "/salesforce/submitClaimsPaymentAble";
				var oPiggyBack = {
					"remainingDays": remainingDays
				};
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onCheckMonthlyPaymentWarnSuccess, _oController.onCheckMonthlyPaymentWarnError,
					undefined, oPiggyBack);
			}
		},

		/**
		 * Function that makes a ajax call to get the server timestamp
		 * @function getServerTime
		 */
		getServerTime: function (oController, aSync, fSuccess, fError, oPiggyBack) {
			var dServerTime, oReturn;

			if (oController === undefined) {
				oController = _oController;
			}
			if (aSync === undefined) {
				aSync = false;
			}

			if (fSuccess === undefined) {
				fSuccess = _oController.onGetServerTimeSuccess;
			}

			if (fError === undefined) {
				fError = _oController.onGetServerTimeError;
			}

			$.ajax({
				beforeSend: function (request) {
					request.setRequestHeader("Content-Type", "application/json");
					// Enables XSS filtering. Rather than sanitizing the page, the browser will prevent rendering of the page if an attack is detected.
					request.setRequestHeader("X-Xss-Protection", " 1; mode=block");
					//Stops the browser from trying to MIME-sniff the content type and forces it to stick with the declared content-type
					request.setRequestHeader("X-Content-Type-Options", "nosniff");
				},
				type: "GET",
				dataType: "text",
				url: "/service-layer-dash/core/serverTimestamp",
				async: aSync,
				success: function (oData, textStatus, xhr) {
					if (oPiggyBack === undefined) {
						oPiggyBack = {};
					}
					oPiggyBack.textStatus = textStatus;
					oPiggyBack.xhr = xhr;
					oReturn = fSuccess(oController, oData, oPiggyBack);
				},
				error: function (oError) {
					// Service TimeOut Check
					if (oController.checkSessionTimeoutServices(oError)) {
						return;
					}
					oReturn = fError(oController, oError, oPiggyBack);
				}
			});
			if (!aSync) {
				return oReturn;
			}
			return dServerTime;
		},

		/**
		 * Function that handles the success case of the ajax call that returns the server timestamp
		 * @function onCheckSessionTimeoutSuccess
		 */
		onGetServerTimeSuccess: function (oController, oData, oPiggyBack) {
			return new Date(oData);
		},

		/**
		 * Function that handles the error case of the ajax call that returns the server timestamp
		 * @function onCheckSessionTimeoutError
		 */
		onGetServerTimeError: function (oController, oError, oPiggyBack) {
			return new Date();
		},

		/**
		 * Remove commas from a number
		 * @function checkMonthlyPaymentWarn
		 **/
		deCommaFy: function (num) {
			if (num !== undefined && num !== null) {
				var str = num.toString().split(',').join('');
				return parseInt(str);
			} else {
				return num;
			}
		},

		/**
		 * Remove commas from a float
		 * @function checkMonthlyPaymentWarn
		 **/
		deCommaFyFloats: function (num) {
			if (num !== undefined && num !== null && num !== "") {
				var oFormatOptions = {
					style: "standard",
					decimals: 2,
					groupingEnabled: "true"
				};
				var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
				var oFloatFormat = sap.ui.core.format.NumberFormat.getFloatInstance(oFormatOptions, oLocale);
				var decimalDelimiter = oFloatFormat.oLocaleData.getNumberSymbol("decimal");
				var groupDelimiter = oFloatFormat.oLocaleData.getNumberSymbol("group");
				var last = "";
				var str;

				var parts = num.toString().split(decimalDelimiter);
				if (parts !== undefined && parts.length > 1) {
					last = parts.pop();
					parts = parts.toString().split(groupDelimiter).join('');
				} else if (parts !== undefined && parts.length === 1) {
					parts = parts.toString().split(groupDelimiter).join('');
				}

				if (last === "") {
					str = parts;
				} else {
					str = parts + "." + last;
				}
				return str;
			} else {
				return num;
			}
		},

		/**
		 * Formats a number to be displayed with commas at every 3 digits
		 * @function checkMonthlyPaymentWarn
		 **/
		commaFy: function (num) {
			var str = num.toString().split('.');
			if (str[0].length >= 4) {
				str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
			}
			return str.join('.');
		},

		/**
		 * This function receives and stores a search object to the search History 
		 * @function addToSearchHistory
		 */
		addToSearchHistory: function (o2AddSearchHistory) {
			var oldSearchHistory = _oController.retrieveSearchHistory();
			var isDuplicate = false;
			var duplicatePos;
			//Check if the current searched item is already in the History
			if (oldSearchHistory !== undefined && oldSearchHistory !== null) {
				for (var i = 0; i < oldSearchHistory.length; i++) {
					if (o2AddSearchHistory.id === oldSearchHistory[i].id) {
						isDuplicate = true;
						duplicatePos = i;
					}
				}
			} else {
				oldSearchHistory = [];
			}
			//if searched item is already in the History, remove the previous entrance
			if (isDuplicate) {
				oldSearchHistory.splice(duplicatePos, 1);
			}

			//Check if the Search History already has 20 entries
			if (oldSearchHistory.length >= 20) {
				//remove list head
				oldSearchHistory.shift();
				//add to the list tail
				oldSearchHistory.push(o2AddSearchHistory);
			} else {
				//add to the list tail
				oldSearchHistory.push(o2AddSearchHistory);
			}
			_oController.storeSearchHistory(oldSearchHistory);
		},

		/**
		 * This function receives and stores the JSON of search History 
		 * @function storeSearchHistory
		 */
		storeSearchHistory: function (aSearchHistory) {
			var oStore = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			oStore.put("SearchHistory", aSearchHistory);
		},

		/**
		 * This function retrieves the search History stored in the local storage 
		 * @function retrieveSearchHistory
		 */
		retrieveSearchHistory: function () {
			var oStore = jQuery.sap.storage(jQuery.sap.storage.Type.local);
			var aSearchHistory = oStore.get("SearchHistory");
			return aSearchHistory;
		},

		/**
		 * Generic function to send dataLayer properties to GTM - Google Tag Manager  
		 * @function GTMDataLayer
		 **/
		GTMDataLayer: function (data1, data2, data3, data4) {
			window.dataLayer.push({
				'event': data1,
				'eventCategory': data2,
				'eventAction': data3,
				'eventLabel': data4
			});
		},

		/**
		 * Generic function to handle Ajax JSON Calls
		 * @function handleAjaxJSONCall
		 * @param {Object} oController - Sets the context for the code.
		 * @param {Bolean} aSync - Indicates if the call should be asynchronous or not.
		 * @param {String} sEndPoint - Call endPoint and url parameters.
		 * @param {String} sCallType - Indicates if it is a POST or a GET Call.
		 * @param {Function} fSuccess - Function to execute if the call is successfull.
		 * @param {Function} fError - Function to execute if the call is unsuccessfull.
		 * @param {Object} oDataToSend - Data to be sent if it is a POST call, undefined otherwise.
		 * @param {Object] oPiggyBack - Auxiliary object to transport data from the calling function to the success function
		 **/
		handleAjaxJSONCall: function (oController, aSync, sEndPoint, sCallType, fSuccess, fError, oDataToSend, oPiggyBack) {
			var oReturn;
			//Reset the service call idle time counter
			window.nServiceIdleTime = 0;
			$.ajax({
				beforeSend: function (request) {
					request.setRequestHeader("Content-Type", "application/json");
					// Enables XSS filtering. Rather than sanitizing the page, the browser will prevent rendering of the page if an attack is detected.
					request.setRequestHeader("X-Xss-Protection", " 1; mode=block");
					//Stops the browser from trying to MIME-sniff the content type and forces it to stick with the declared content-type
					request.setRequestHeader("X-Content-Type-Options", "nosniff");
				},
				type: sCallType,
				data: oDataToSend,
				url: "/service-layer-dash" + sEndPoint,
				async: aSync,
				success: function (oData, textStatus, xhr) {
					// Service TimeOut Check
					if (oController.checkSessionTimeoutServices(xhr)) {
						oReturn = true;
						return;
					}
					if (oPiggyBack === undefined) {
						oPiggyBack = {};
					}
					oPiggyBack.textStatus = textStatus;
					oPiggyBack.xhr = xhr;
					oReturn = fSuccess(oController, oData, oPiggyBack);
				},
				error: function (oError) {
					// Service TimeOut Check
					if (oController.checkSessionTimeoutServices(oError)) {
						oReturn = true;
						return;
					}
					oReturn = fError(oController, oError, oPiggyBack);
				}
			});
			if (!aSync) {
				return oReturn;
			}
		},

		formatterAddressMobile: function (accbillingcity, accbillingstate, accbillingpostalcode) {
			if (accbillingstate && accbillingpostalcode) {
				return accbillingcity + ", " + accbillingstate + " " + accbillingpostalcode;
			} else {
				if (accbillingstate) {
					return accbillingcity + ", " + accbillingstate;
				} else if (accbillingpostalcode) {
					return accbillingcity + " " + accbillingpostalcode;
				} else {
					return accbillingcity;
				}
			}
		},

		/**
		 * Round number to beautify graphic range scales
		 * @function roundNumberForGraphRange
		 * @param {string} value - Number to round
		 */
		roundNumberForGraphRange: function (value) {
			var RoundedValue = Math.round(value);
			var splitedValue = RoundedValue.toString().split("");
			var firstDigit = splitedValue[0];
			var rangeValue;
			var padZeros;
			if (RoundedValue === 0) {
				return RoundedValue;
			} else {
				if (firstDigit === "-") {
					rangeValue = "-" + (parseInt(splitedValue[1], 10) + 1).toString();
					padZeros = splitedValue.length - 2;
				} else {
					if (RoundedValue < 100000 && RoundedValue > 50000) {
						return 100000;
					} else if (RoundedValue < 100000 && RoundedValue > 0) {
						if (parseInt(firstDigit, 10) % 2 === 0) {
							rangeValue = (parseInt(firstDigit, 10) + 2).toString();
						} else {
							rangeValue = (parseInt(firstDigit, 10) + 1).toString();
						}
						padZeros = splitedValue.length - 1;
					} else {
						rangeValue = (parseInt(firstDigit, 10) + 1).toString();
						padZeros = splitedValue.length - 1;
					}
				}
				for (var i = 0; i < padZeros; i++) {
					rangeValue = rangeValue + "0";
				}
				return parseInt(rangeValue, 10);
			}

		},

		/**
		 * Remove Selected Style Class from links
		 * 
		 * @function removeSearchResultsStyleClass
		 **/
		removeSearchResultsStyleClass: function (elementId, aList, oView) {
			for (var i = 0; i < aList.length; i++) {
				if (oView.byId(elementId + aList[i]).hasStyleClass("numberOfResultsLinkSelected")) {
					oView.byId(elementId + aList[i]).removeStyleClass("numberOfResultsLinkSelected");
				}
			}
		},

		/**
		 * Navigation to Hybris.
		 * @function nav2Hybris
		 * @param {string} sPath - The hybris url to open on the iframe
		 */
		nav2Hybris: function (sPath) {
			var mHybrisParameters = sap.ui.getCore().getModel("HybrisParameters");
			if (mHybrisParameters === undefined) {
				mHybrisParameters = new sap.ui.model.json.JSONModel();
				mHybrisParameters.setData(this.getGlobalParametersByGroup("HybrisParameters"));
				sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
			}
			var oHybrisParameters = mHybrisParameters.getData();
			if (oHybrisParameters.showHybrisFrame && oHybrisParameters.showHybrisFrame === "true") {
				_oRouter.navTo("store", {
					'hybrisHash*': (sPath.split(oHybrisParameters.Hybris)[1] || "")
				});
			} else {
				window.open(sPath, "_self");
			}
		},

		/**
		 * Sets the source to the iframe and adds the needed event listener
		 * @function setHybrisInIframe
		 * @param {string} sPath - The hybris url to open on the iframe
		 */
		setHybrisInIframe: function (sPath) {
			window.addEventListener("message", _oController.hybrisMessageHandler);
			//Needed so that we never it the "blank" page when using the browser back
			var iFrame = $('#Hybris');
			var iFrameParent = iFrame.parent();
			iFrame.remove();
			iFrame.attr("src", sPath);
			//Ipad resquesting desktop sites is identified as macOS
			if ((sap.ui.Device.os.ios || sap.ui.Device.os.macintosh) && (sap.ui.Device.system.phone || sap.ui.Device.system.tablet)) {
				iFrame.attr("scrolling", "yes");
				iFrameParent.addClass("ios");
			}
			iFrameParent.append(iFrame);
			sap.ui.getCore().byId("dashShell").setBusyIndicatorDelay(0).setBusy(true);
		},

		/**
		 * Handler for the messages sent from the Child frame.
		 * @function hybrisMessageHandler
		 */
		hybrisMessageHandler: function (oEvent) {
			var mHybrisParameters = sap.ui.getCore().getModel("HybrisParameters");
			if (mHybrisParameters === undefined) {
				mHybrisParameters = new sap.ui.model.json.JSONModel();
				mHybrisParameters.setData(this.getGlobalParametersByGroup("HybrisParameters"));
				sap.ui.getCore().setModel(mHybrisParameters, "HybrisParameters");
			}
			var oHybrisParameters = mHybrisParameters.getData();
			if (oEvent.origin === oHybrisParameters.Hybris) {
				var oEventData = JSON.parse(oEvent.data) || "";
				sap.ui.getCore().byId("dashShell").setBusy(false);
				var sHybrisLang = _oController.getLanguageFromCookie();
				if (sHybrisLang !== sap.ui.getCore().getConfiguration().getLanguage()) {
					if (sap.ui.getCore().getModel("selectedLanguage")) {
						sap.ui.getCore().getModel("selectedLanguage").getData().selectedLanguage = sHybrisLang;
					}
					_oController.onChangeLanguage({
						"languageToSet": sHybrisLang
					});
				}
				if (oEventData.idleTime !== undefined) {
					//if the received idle time is smaller, replace the current idle time
					if (oEventData.idleTime < window.nIdleTime) {
						window.nIdleTime = oEventData.idleTime;
					}
				} else if (oEventData.route) {
					_oRouter.navTo(oEventData.route);
					$("#content").show();
					$("#Hybris").hide();
					window.removeEventListener("message", _oController.hybrisMessageHandler);
				} else if (oEventData.hybrisHash) {
					history.replaceState(undefined, undefined, "#/store" + oEventData.hybrisHash);
					$("#Hybris").show();
					$("#content").hide();
					if (sap.ui.Device.os.ios && (sap.ui.Device.system.phone || sap.ui.Device.system.tablet)) {
						$("#focusableInput")[0].focus();
						$("#focusableInput")[0].blur();
					}
				} else {
					//defensive programming: no default action needed, unrecognized message
				}
			}
		},
		/**
		 * Navigates to 'Contact Us' page.
		 * @function toLinkContacts
		 */
		goToContacts: function () {
			// Checks Session time out
			_oController.checkSessionTimeout();

			if (sap.ui.getCore().byId("accountAccessFragment")) {
				sap.ui.getCore().byId("accountAccessFragment").destroy();
			}

			ga('set', 'page', '/Contacts');
			ga('send', 'pageview');
			_oController.getRouter().navTo("contacts");
		},

		/**
		 * De-register the resize hander for Edge browser
		 * 
		 * @function _removeDialogResize
		 */
		_removeDialogResize: function (oControl) {
			var oDevice = sap.ui.Device;
			if (oControl && oControl.isOpen()) {
				if (oDevice.system.desktop && oDevice.browser.name === "ed") {
					oControl._deregisterResizeHandler();
					oControl._deregisterContentResizeHandler();
				}
			}
		},

		/**
		 * Create and set model for Export Types
		 * 
		 * @function _createExportTypeModel
		 */
		_createExportTypeModel: function (oControl) {
			var oModel = new sap.ui.model.json.JSONModel();
			var aData = [];
			aData.push({
				id: 0,
				name: _oController.getResourceBundle().getText("MASCExportPLH"),
				enabledProperty: true
			});
			aData.push({
				id: 1,
				name: "CSV",
				enabledProperty: true
			});
			aData.push({
				id: 2,
				name: "Excel",
				enabledProperty: true
			});
			oModel.setData(aData);
			oControl.setModel(oModel, "oExportTypes");
		},

		/**
		 * method to convert json to workbook
		 * @function _jsonToWorkbook
		 * @param {data} Excel Data
		 * @param {sheetName} Sheet Name
		 * @returns The workbook
		 */
		_jsonToWorkbook: function (data, sheetName) {
			var ws = XLSX.utils.json_to_sheet(data);
			var wb = XLSX.utils.book_new();
			XLSX.utils.book_append_sheet(wb, ws, sheetName);
			return wb;
		},

		/**
		 * save file in xlxs format
		 * @function _saveFileToExcel
		 * @param {wb} Workbook
		 * @param {name} Workbook name
		 */
		_saveFileToExcel: function (wb, name) {
			XLSX.writeFile(wb, name + ".xlsx");
		},

		/**
		 * returns the filling value based on the order stats
		 * @function orderStatusValue
		 * @param {sIcon} Icon status
		 */
		orderStatusValue: function (sIcon) {
			if (sIcon !== "COMPLETE" || sIcon !== "UNAVAILABLE") {
				var iValue = 0;
				switch (sIcon) {
				case "RECEIVED":
					iValue = 20;
					break;
				case "SHIPPED":
					iValue = 100;
					break;
				case "IN PROCESS":
					iValue = 60;
					break;
				default:
					break;
				}
			}
			return iValue;
		},
		/**
		 * Getter function that retrieves the value for the received key
		 * @function getKey
		 * @param {String} - sType
		 * @param {String} - sPropertyKey
		 * @returns {String}
		 */
		getKey: function (sType, sPropertyKey) {
			var value = Keys[sType][sPropertyKey];
			return value;
		},
		/**
		 * Given a route uses the instantiaded router to navigate
		 * @function dashNavTo
		 * @param {String} - sRoute
		 * @param {Bolean} - bNewTab
		 */
		dashNavTo: function (sRoute, bNewTab) {
			// Checks Session time out
			_oController.checkSessionTimeout();
			var oParameter = {};
			//get the needed endpoint using getURL function of Router
			var sRouteEndPoint = _oRouter.getRoute(sRoute).getPattern();
			if (sRoute === "myAccountCases") {
				sRouteEndPoint = sRouteEndPoint.replace("{caseId}", "0");
				oParameter = {
					caseId: "0"
				};
			}

			//register the pageview on GA
			ga('set', 'page', sRouteEndPoint);
			ga('send', 'pageview');
			if (bNewTab) {
				//Add a # before the URL to use the Mother Url
				sap.m.URLHelper.redirect("#" + sRouteEndPoint, true);
			} else {
				_oController.getRouter().navTo(sRoute, oParameter);
			}
		}

	});
});