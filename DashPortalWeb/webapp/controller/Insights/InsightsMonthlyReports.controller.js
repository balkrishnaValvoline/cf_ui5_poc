sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/core/format/NumberFormat"

], function(Base) {
	"use strict";

	var _oController, _oModel, _oAccountNumber, _oDistrict, _oMonth, _oYear;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Insights.InsightsMonthlyReports", {
		/** @module InsightsMonthlyReports */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function() {
			Base.prototype.onInit.call(this);
			_oController = this;

			sap.ui.getCore().byId("__xmlview0--vbox_background_main").setVisible(false);
			sap.ui.getCore().byId("__xmlview0--menuMobileHeader").setVisible(false);

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("insightsMonthlyReports").attachPatternMatched(this._onObjectMatched, this);
		},

		/**
		 * Set the menu as active if the app is reloaded and makes an element in the filter disabled
		 * @function onAfterRendering
		 */
		onAfterRendering: function() {
			/* Set the menu as active if the app is reloaded */
			$(".js-insights-sidenav").addClass("sidenav__item--active");

			//Remove the focus of the input
			$(':focus').blur();
		},

		/**
		 * This function runs everytime the page is opened and gets the selected insight parameters
		 * @function _onObjectMatched
		 * @param {Object} oEvent - oEvent required to get the diferent values
		 */
		_onObjectMatched: function(oEvent) {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page Layout configuration
			if (!this.checkDASHPageAuthorization("INSIGHTS_PAGE")) {
				return;
			}

			_oMonth = oEvent.getParameter("arguments").month;
			_oYear = oEvent.getParameter("arguments").year;
			_oAccountNumber = oEvent.getParameter("arguments").account;
			_oDistrict = oEvent.getParameter("arguments").district;
			_oDistrict = _oDistrict.replace(/__/g,"/");

			_oController.configureData(_oAccountNumber, _oDistrict, _oMonth, _oYear);
			this.configureAddresLabel(_oAccountNumber, _oDistrict);

		},

		/**
		 * This function gets the Address of the AccountNumber
		 * @function configureAddresLabel
		 * @param {string} accountNumber - number of the account
		 */
		configureAddresLabel: function(accountNumber, district) {
			if (accountNumber !== "0" && district === "0") {
				var controller = this;
				var xsURL = "/usermgmt/user/accounts";
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				var model = controller.handleAjaxJSONCall(controller, false, xsURL, "GET", controller.onConfigureAddresLabelSuccess, controller.onConfigureAddresLabelError);

				var oAccounts = new sap.ui.model.json.JSONModel(model.accounts);
				var oAddress = new sap.ui.model.json.JSONModel();
				for (var i = 0; i < oAccounts.getData().length; i++) {
					if (oAccounts.getData()[i].accountnumber === accountNumber) {
						oAddress.setData(oAccounts.getData()[i]);
					}
				}
				//save direct Account in a model
				this.getView().setModel(oAddress, "oSelectedAccount");
			} else if (accountNumber === "0" && district === "0") {
				this.getView().byId("accountAddress2").setText(this.getResourceBundle().getText("MAstoresTXT"));
			} else {
				this.getView().byId("accountAddress2").setText(district);
			}

		},

		/**
		 * Function that handles the success case of the ajax call to configure the address label
		 * @function onConfigureAddresLabelSuccess
		 */
		onConfigureAddresLabelSuccess: function(oController, oData, oPiggyBack) {
			var model;
			if (oData === undefined) {
				model = [];
			} else {
				model = oData;
			}
			return model;
		},

		/**
		 * Function that handles the error case of the ajax call to configure the address label
		 * @function onConfigureAddresLabelError
		 */
		onConfigureAddresLabelError: function(oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, "contactAccounts");
			return null;
		},

		/**
		 * This function configure the Data for each service
		 * @function configureData
		 * @param {string} accountNumber - userAccount ID for loading the graphs
		 * @param {string} year - year for the Reports details
		 * @param {string} month - month for the Reports details
		 */
		configureData: function(accountNumber, district, month, year) {
			var title = this.setMonthName(month);
			var serviceName = "reports?month=" + month + "&year=" + year + "&";

			this.onLoadTableColumns();
			this.onLoadTableRows(serviceName, accountNumber, district);
			this.getView().byId("insightsDetailsSubTitle").setText(title);
		},

		/**
		 * This function converts the Month Number to Month Text.
		 * @function setMonthName
		 * @param {string} month - month for the Reports details
		 */
		setMonthName: function(monthNumber) {
			var monthText;
			switch (monthNumber) {
				case "01":
					monthText = _oController.getResourceBundle().getText("INGTSText12LK");
					break;
				case "02":
					monthText = _oController.getResourceBundle().getText("INGTSText11LK");
					break;
				case "03":
					monthText = _oController.getResourceBundle().getText("INGTSText10LK");
					break;
				case "04":
					monthText = _oController.getResourceBundle().getText("INGTSText9LK");
					break;
				case "05":
					monthText = _oController.getResourceBundle().getText("INGTSText8LK");
					break;
				case "06":
					monthText = _oController.getResourceBundle().getText("INGTSText7LK");
					break;
				case "07":
					monthText = _oController.getResourceBundle().getText("INGTSText6LK");
					break;
				case "08":
					monthText = _oController.getResourceBundle().getText("INGTSText5LK");
					break;
				case "09":
					monthText = _oController.getResourceBundle().getText("INGTSText4LK");
					break;
				case "10":
					monthText = _oController.getResourceBundle().getText("INGTSText3LK");
					break;
				case "11":
					monthText = _oController.getResourceBundle().getText("INGTSText2LK");
					break;
				case "12":
					monthText = _oController.getResourceBundle().getText("INGTSText1LK");
					break;
				default:
					break;
			}
			return monthText;
		},

		/**
		 * This function loads the Columns Configuration for each graph.
		 * @function onLoadTableColumns
		 */
		onLoadTableColumns: function() {

			var oModel = new sap.ui.model.json.JSONModel();
			var columnsLayout;

			columnsLayout = {
				"columns": [{
					"name": _oController.getResourceBundle().getText("IPdateTable")
				}, {
					"name": _oController.getResourceBundle().getText("IPsynpower")
				}, {
					"name": _oController.getResourceBundle().getText("IPmaxlife")
				}, {
					"name": _oController.getResourceBundle().getText("IPdurablend")
				}]
			};

			oModel.setData(columnsLayout);
			this.getView().setModel(oModel, "insightsTableColumns");
		},

		/**
		 * Function that handles the success case of the ajax call to load the row configuration for each graph
		 * @function onLoadTableRowsSuccess
		 */
		onLoadTableRowsSuccess: function(oController, oData) {
			var insightsDetailsTable = oController.getView().byId("insightsDetailsTable");
			insightsDetailsTable.setBusy(false);
			_oModel.setData(oData);

			oController.getView().setModel(_oModel, "insightsTable");
		},

		/**
		 * Function that handles the error case of the ajax call to load the row configuration for each graph
		 * @function onLoadTableRowsError
		 */
		onLoadTableRowsError: function(oController, oError) {
			var insightsDetailsTable = oController.getView().byId("insightsDetailsTable");
			insightsDetailsTable.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRReports"));
		},

		/**
		 * This function loads the Rows Configuration for each graph
		 * @function onLoadTableRows
		 * @param {string} serviceName
		 * @param {string} AccountNumber
		 */
		onLoadTableRows: function(serviceName, accountNumber, district) {
			var controller = this;
			var insightsDetailsTable = this.getView().byId("insightsDetailsTable");
			insightsDetailsTable.setBusyIndicatorDelay(0);
			insightsDetailsTable.setBusy(true);

			var xsURL = "/insights/" + serviceName;

			if (accountNumber === "0" && district === "0") {
				//dont change URL
			} else if (accountNumber !== "0" && district === "0") {
				xsURL += "ShipTo=" + accountNumber;
			} else if (accountNumber === "0" && district !== "0") {
				xsURL += "customerreportinggroup=" + district;
			} else {
				//dont change URL
			}

			_oModel = new sap.ui.model.json.JSONModel();
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadTableRowsSuccess, controller.onLoadTableRowsError,
				undefined);
		},

		/**
		 * Format values
		 * @function formatValuesDetails
		 * @param {string} sValue - value to be formatted
		 * @returns formatted value contatenated with value measure
		 */
		formatValuesDetails: function(sValue) {

			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			var oLocale = new sap.ui.core.Locale(selectedLanguage);

			var oFormatOptions = {
				style: "standard",
				decimals: 0,
				shortDecimals: 0
			};
			var oFloatFormat = sap.ui.core.format.NumberFormat.getFloatInstance(oFormatOptions, oLocale);
			var sValueFormatted = oFloatFormat.format(sValue);
			var valueMeasure = _oModel.getData().rows[0].valueMeasure;
			return sValueFormatted + " " + valueMeasure;
		}

	});
});