sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function (Base) {
	"use strict";

	var _oController, _oRouter;
	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Insights.MainInsights", {

		/** @module MainInsights */
		/**
		 * This function is called when the fist rendered.
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oController = this;
			_oRouter = this.getRouter();
			_oRouter.getRoute("mainInsights").attachPatternMatched(this._onObjectMatched, this);
		}, 
		
		/**
		 * This function is called everytime the page is loaded, and refresh the data and the page opening.
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function() {
			
			if (_oController.checkDASHLayoutAccess("DISTRIBUTOR_INSIGHTS")) {
				//GTM
				_oController.GTMDataLayer('leftnavclick',
					'Left Navigation',
					'Insights-click',
					"#/" + _oRouter.getRoute("distributorInsights").getURL()
				);

				_oRouter.navTo("distributorInsights");
			} else if (_oController.checkDASHLayoutAccess("INSIGHTSHD_PAGE")) {
				//GTM
				_oController.GTMDataLayer('leftnavclick',
					'Left Navigation',
					'Insights-click',
					"#/" + _oRouter.getRoute("insightsHD").getURL()
				);

				_oRouter.navTo("insightsHD");
			} else {
				//GTM
				_oController.GTMDataLayer('leftnavclick',
					'Left Navigation',
					'Insights-click',
					"#/" + _oRouter.getRoute("insights").getURL()
				);

				_oRouter.navTo("insights");
			}
		}
	});
});