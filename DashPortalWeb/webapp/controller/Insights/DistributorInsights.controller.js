sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/core/mvc/Controller"
], function (Base, Controller) {
	"use strict";

	// Global variable used in PUT service
	var changedInputValues = [];
	var _oView;
	var _oController;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Insights.DistributorInsights", {
		/** @module Insights Distributor */
		/**
		 * This function is called when the fist rendered. Set the Account models, and builds all the diferent Graphs displayed in the view.
		 * @function onInit
		 */
		onInit: function () {
			_oView = this.getView();
			Base.prototype.onInit.call(this);
			_oController = this;
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("distributorInsights").attachPatternMatched(this._onObjectMatched, this);
			this.onDataLossPreventingBrowserAlert();
		},

		/**
		 * onDataLossPreventingBrowserAlert
		 * Sets a browser alert that prevents closing if there is values to submit
		 */
		onDataLossPreventingBrowserAlert: function () {
			// Runs when the user tries to close the "distributorInsights" page
			$(window).bind('beforeunload', function () {
				var route = sap.ui.core.routing.HashChanger.getInstance().getHash();
				if (_oController.valueChangeInputVerification() === true && route === "distributorInsights") {
					return true;
				}
			});
		},

		/**
		 * Set the menu as active if the app is reloaded and makes an element in the filter disabled
		 * @function onAfterRendering
		 */
		onAfterRendering: function () {
			$(".js-insights-sidenav").addClass("sidenav__item--active");

			//see if URL is available
			var sURL = _oView.getModel("HowToVideos").getData().HowToDistInsightsURL;
			if (sURL !== undefined) {
				this.getView().byId("howToVideosButton").setVisible(true);
				this.getView().byId("howToVideo").setVisible(true);
			}
		},

		/**
		 * This function is called everytime the page is loaded, and refresh the data and the page opening.
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {
			// Clears all error message
			this.clearServerErrorMsg(this);
			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "insights"
			});
			// Page Layout Configuration
			if (!this.checkDASHPageAuthorization("DISTRIBUTOR_INSIGHTS")) {
				return;
			}
			this.headerIconAccess();

			//get user info and direct account
			this.getUserInformation(this);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);
			//Load Data
			this.onLoadDataCardGraph();
		},

		/**
		 * This function loads the data for all the charts on the page.
		 * @function onLoadDataCardGraph
		 */
		onLoadDataCardGraph: function () {
			var controller = this;
			jQuery.sap.delayedCall(0, controller, function () {

				// VRI Section
				if (_oController.valueChangeInputVerification() === false) {
					controller.onLoadVRITracking();
					controller.onLoadVRILabels();
				} else {
					this.DataLossWarning =
						new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", _oController);
					//Set Warning Buttons
					sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.onRefreshDialog_Refresh);
					sap.ui.getCore().byId("button_Dialog_Warning_No").attachPress(_oController.onRefreshDialog_Close);
					sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWreloadTXT"));
					sap.ui.getCore().byId("button_Dialog_Warning_No").setText(_oController.getResourceBundle().getText("DLWeditTXT"));
					//Set Warning Message
					sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWSubmitTitle"));
					sap.ui.getCore().byId("text2_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWSubmtitDescription"));
					//Set Warnig Dialog Title
					sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWMainTitle"));
					this.DataLossWarning.open();
					_oController._removeDialogResize(_oController.DataLossWarning);
				}

				// Insights Section
				controller.onLoadDataDistributorLubricantVolume();
				controller.onLoadDataDistributorAntiFreezeVolume();
				controller.onLoadDataDistributorNonLubricantRevenue();
				controller.onLoadDataDistributorNonLubricantVolume();
				controller.onLoadDataDistributorDeliveryVolume();
				controller.onLoadDataDistributorDeliveryAllowance();
			});
		},

		/**
		 * Formatts the value with 0 decimal digits
		 * @(parameter) (SValue)
		 * return (sValueFormatted)
		 */
		valueChangeFormatter: function (sValue) {
			var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
			var oFormatOptions = {
				style: "standard",
				maxFractionDigits: 2,
				decimals: 0
			};
			var oFloatFormat = sap.ui.core.format.NumberFormat.getFloatInstance(oFormatOptions, oLocale);
			var sValueFormatted = oFloatFormat.format(sValue);
			return sValueFormatted;
		},

		/**
		 * Function that returns True if there is valid data stored, or False if there is no data or is invalid
		 * valueChangeInputVerification
		 */
		valueChangeInputVerification: function () {
			var submitButtonState = true;
			if (changedInputValues.length === 0) {
				submitButtonState = false;
			} else {
				for (var i = 0; i < changedInputValues.length; i++) {
					if (changedInputValues[i].value === "Invalid") {
						submitButtonState = false;
					}
				}
			}
			return submitButtonState;
		},

		/**
		 * This function saves the input values into a array
		 * @ function valueChangeInputTable
		 * @ parameter (oEvent)
		 */
		valueChangeInput: function (oEvent) {
			// Get the Id of the Input and is Index
			var inputField = oEvent.getSource();
			var inputID = inputField.getId().split("-")[0];

			// Get Column Values / Get Selected Row Values
			var columnIndex = inputID.split("valueColumn")[1];
			var oModelColumn = this.getModel("VRITrackingReportColumns");
			var selectedFiscalYear = oModelColumn.getProperty("/" + columnIndex + "/year");
			var selectedFiscalMonth = oModelColumn.getProperty("/" + columnIndex + "/fiscalMonthNumber");
			// Row
			var oModelRow = this.getModel("VRITrackingReportRows");
			var row = oEvent.getSource().getBindingContext().getPath();
			var vriTypeSelected = oModelRow.getProperty(row + "/vriType");

			// Get Input Value and Original Value
			var inputValue = oEvent.getParameter("value");
			inputValue = inputValue.split(",").join("");
			inputValue = inputValue.split(".").join("");
			// Removal of Leading ZEROS ("01" -> "1")
			if (!isNaN(inputValue) && inputValue !== "") {
				inputValue = parseFloat(inputValue) + "";
			}
			var inputValueFormated = _oController.valueChangeFormatter(inputValue).toString();
			var originalStored = oModelRow.getProperty(row + "/valueColumn" + columnIndex + "/originalStored").toString();
			var originalFormatedStored = oModelRow.getProperty(row + "/valueColumn" + columnIndex + "/originalFormatedStored").toString();

			// If input value is empty or is equal to the original value
			if (isNaN(inputValue)) {
				inputValue = "Invalid";
				inputField.setValueStateText(_oController.getResourceBundle().getText("INGTSDISTInvalidNumber"));
				inputField.setValueState("Error");
			} else if (inputValue.length > 10) {
				inputValue = "Invalid";
				inputField.setValueStateText(_oController.getResourceBundle().getText("INGTSDIST10DigitsNumber"));
				inputField.setValueState("Error");
			} else {
				if (inputValue === "") {
					inputValue = "0";
					inputField.setValue("0");
					inputField.setValueState("None");
					if (inputValue === originalStored) {
						inputField.removeStyleClass("VRIInputFieldEdited");
					} else {
						inputField.addStyleClass("VRIInputFieldEdited");
					}
				} else if (inputValue === originalStored) {
					inputValue = originalStored;
					inputField.setValue(originalFormatedStored);
					inputField.setValueState("None");
					inputField.removeStyleClass("VRIInputFieldEdited");
				} else {
					inputField.addStyleClass("VRIInputFieldEdited");
					inputField.setValueState("None");
					inputField.setValue(inputValueFormated);
				}
			}

			// Build and set the body for the PUT in network
			var userChange = {
				"year": selectedFiscalYear,
				"fiscalMonthNumber": selectedFiscalMonth,
				"vriType": vriTypeSelected,
				"value": inputValue
			};

			var canPushValue;
			// If the array is empty and the input value is equal to the originalStored, does nothing
			if (changedInputValues.length === 0 && inputValue === originalStored) {
				canPushValue = false;
			} else {
				canPushValue = true;
				// If the array has some values check all the values
				for (var i = 0; i < changedInputValues.length; i++) {
					// If the edited value already is in the array, remove it or udpated it and doesnt add
					if (changedInputValues[i].year === selectedFiscalYear && changedInputValues[i].fiscalMonthNumber === selectedFiscalMonth &&
						changedInputValues[i].vriType === vriTypeSelected) {
						canPushValue = false;
						if (inputValue === originalStored) {
							changedInputValues.splice(i, 1);
						} else {
							changedInputValues[i].value = inputValue;
						}
					} else {
						// If there isnt in the array add it
						canPushValue = true;
					}
				}
			}

			// Checks if it can push the value into the "changedInputValues" array
			if (canPushValue) {
				changedInputValues.push(userChange);
			}

			// Check the SUBMIT button state
			_oController.byId("submitVRILabelsButton").setEnabled(_oController.valueChangeInputVerification());
		},

		/**
		 * Function that handles the success case of the ajax call to submit Claims
		 * @function onLoadVRITrackingSuccess
		 */
		onLoadVRITrackingSuccess: function (oController, oData, oPiggyBack) {
			var VRITrackingReportTable = oController.getView().byId("VRITrackingReportTable");
			if (oData !== undefined && oData !== null && oData.rows !== undefined && oData.rows !== null && oData.columns !== undefined &&
				oData
				.columns !== null) {
				var oModelVRIRows = new sap.ui.model.json.JSONModel();
				var oModelVRIColumns = new sap.ui.model.json.JSONModel();
				oModelVRIRows.setDefaultBindingMode(sap.ui.model.BindingMode.OneWay);
				oModelVRIRows.setData(oData.rows);
				oModelVRIColumns.setData(oData.columns);
				oController.setModel(oModelVRIRows, "VRITrackingReportRows");
				oController.setModel(oModelVRIColumns, "VRITrackingReportColumns");
				oController.onVRITableTrackingBuild();

			} else {
				oController.getView().byId("submitVRILabelsButton").setVisible(false);
				oController.getView().byId("VRILabelsTable").setVisible(false);
				oController.getView().byId("VRIpayoutTable").setVisible(false);
				oController.getView().byId("VRITrackingNoData").setVisible(true);
				oController.getView().byId("VRITrackingReportTableContainer").setVisible(false);
			}
			setTimeout(function () {
				VRITrackingReportTable.setBusy(false);
			}, 0);
		},

		/**
		 * Function that handles the error case of the ajax call to submit Claims
		 * @function onLoadVRITrackingError
		 */
		onLoadVRITrackingError: function (oController, oError, oPiggyBack) {
			var VRITrackingReportTable = oController.getView().byId("VRITrackingReportTable");
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText);
			VRITrackingReportTable.setBusy(false);
		},

		/**
		 * VRI TRACKING Data Loading
		 * @ onLoadVRITracking
		 */
		onLoadVRITracking: function () {
			var controller = this;

			var VRITrackingReportTable = this.getView().byId("VRITrackingReportTable");
			VRITrackingReportTable.setBusyIndicatorDelay(0);
			VRITrackingReportTable.setBusy(true);
			var xsURL = "/distributorInsights/vriTracking";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadVRITrackingSuccess, controller.onLoadVRITrackingError);
		},

		/**
		 * VRI Tracking Table Configuration
		 * @ onVRITableTrackingConfig
		 */
		onVRITableTrackingBuild: function () {
			// Get all ROWS and COLUMNS information
			var oModelRows = this.getModel("VRITrackingReportRows");
			var oModelColumns = this.getModel("VRITrackingReportColumns");
			var oDataColumns = oModelColumns.getData();
			var refreshTrackingReport = this.getView().byId("refreshTrackingReport");
			var VRILabelsTable = this.getView().byId("VRILabelsTable");
			var VRIpayoutTable = this.getView().byId("VRIpayoutTable");
			var VRISubmitButton = this.getView().byId("submitVRILabelsButton");
			var VRITrackingTable = this.getView().byId("VRITrackingReportTable");
			var VRITrackingNoData = this.getView().byId("VRITrackingNoData");

			if (oDataColumns.length === 0) {
				VRILabelsTable.setVisible(false);
				VRIpayoutTable.setVisible(false);
				VRISubmitButton.setVisible(false);
				refreshTrackingReport.setVisible(false);
				VRITrackingTable.setVisible(false);
				VRITrackingNoData.setVisible(true);

			} else {
				VRITrackingNoData.setVisible(false);
				// TABLE COLUMN AND ROWS CONSTRUCTOR
				var oTable = this.getView().byId("VRITrackingReportTable");
				oTable.destroyColumns();
				//Exception added for the Edge browser, there is a bug where having the fixed column triggers infinite renders
				if (!sap.ui.Device.browser.edge && oTable.getFixedColumnCount() !== 1) {
					oTable.setFixedColumnCount(1);
				}
				oTable.setModel(oModelRows).bindRows("/");
				for (var c = 0; c < oDataColumns.length; c++) {
					// Build 1st Fixed Label Component
					if (c === 0) {
						// Build Fixed Label Component
						var Label_Fixed = new sap.m.Label({
							text: oDataColumns[c].labelColumn
						}).addStyleClass("infoBold");

						// Build Label Component
						var Label_2_Fixed = new sap.m.Text({
							width: "100%",
							textAlign: "Right",
							text: "{valueColumn" + c + "}"
						}).addStyleClass("bodyCopy2");

						// Add Column to Table
						oTable.addColumn(new sap.ui.table.Column({
							resizable: false,
							width: "8.5rem",
							label: (Label_Fixed),
							template: (Label_2_Fixed)
						}));
					} else {
						for (var row = 0; row < oModelRows.getData().length; row++) {
							var originalValue = oModelRows.getProperty("/" + row + "/valueColumn" + c);
							var formatedValue = _oController.formatValuesGroupingSeparator(originalValue);
							var composedValue = {
								original: originalValue,
								originalFormated: formatedValue,
								originalStored: originalValue,
								originalFormatedStored: formatedValue
							};
							oModelRows.setProperty("/" + row + "/valueColumn" + c, composedValue);
						}
						// Other Columns
						// Builds the Label component
						var Label = new sap.m.Label({
							text: oDataColumns[c].labelColumn
						}).addStyleClass("infoBold");

						// Builds the Input Field component
						var Input;
						if (oDataColumns[c].editable) {
							Input = new sap.m.Input({
								liveChange: this.valueChangeInput,
								id: "valueColumn" + c,
								placeholder: "{valueColumn" + c + "/originalFormatedStored}" + " ",
								value: "{valueColumn" + c + "/originalFormatedStored}",
								editable: oDataColumns[c].editable
							}).addStyleClass("VRIInputField");
						} else {
							Input = new sap.m.Input({
								id: "valueColumn" + c,
								value: "{valueColumn" + c + "/originalFormated}",
								editable: oDataColumns[c].editable
							}).addStyleClass("VRIInputField");
						}
						// Add Column to Table
						oTable.addColumn(new sap.ui.table.Column({
							hAlign: "Right",
							resizable: false,
							width: "9.5rem",
							label: (Label),
							template: (Input)
						}));
					}
				}

				// Set the Last Input of the table selected
				oTable.addEventDelegate({
					"onAfterRendering": function () {
						var oTableCells = oTable.getRows()[0].getAggregation("cells");
						var inputTableId = oTableCells[oTableCells.length - 1].getId();
						$("#" + inputTableId + "-inner").focus();
					}
				}, this);

				// Submit Button Disabled
				_oController.getView().byId("submitVRILabelsButton").setEnabled(false);
			}
		},

		/**
		 * Function that handles the success case of the ajax call to submit Claims
		 * @function onPressSubmitSuccess
		 */
		onPressSubmitSuccess: function (oController, oData, oPiggyBack) {
			var VRITrackingReportTable = oController.getView().byId("VRITrackingReportTable");
			if (oData !== undefined && oData !== null && oData.rows !== undefined && oData.rows !== null && oData.columns !== undefined &&
				oData
				.columns !== null) {
				var oModelVRIRows = new sap.ui.model.json.JSONModel();
				var oModelVRIColumns = new sap.ui.model.json.JSONModel();
				oModelVRIRows.setDefaultBindingMode(sap.ui.model.BindingMode.OneWay);
				changedInputValues = [];
				oModelVRIRows.setData(oData.rows);
				oModelVRIColumns.setData(oData.columns);
				oController.setModel(oModelVRIRows, "VRITrackingReportRows");
				oController.setModel(oModelVRIColumns, "VRITrackingReportColumns");
				oController.onVRITableTrackingBuild();
				oController.onLoadVRILabels();
				oController.onShowMessage(oController, "Success", "", oController.getResourceBundle().getText("INGTSDISTVRISuccess"));
			} else {
				oController.getView().byId("submitVRILabelsButton").setVisible(false);
				oController.getView().byId("DistributorLubricantVolumeTable2").setVisible(false);
				oController.getView().byId("VRIpayoutTable").setVisible(false);
			}
			VRITrackingReportTable.setBusy(false);
		},

		/**
		 * Function that handles the error case of the ajax call to submit Claims
		 * @function onPressSubmitError
		 */
		onPressSubmitError: function (oController, oError, oPiggyBack) {
			var VRITrackingReportTable = oController.getView().byId("VRITrackingReportTable");
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText);
			VRITrackingReportTable.setBusy(false);
		},

		/**
		 * On press submit
		 * @ onPressSubmit
		 */
		onPressSubmit: function () {
			var controller = this;

			var VRITrackingReportTable = this.getView().byId("VRITrackingReportTable");
			VRITrackingReportTable.setBusyIndicatorDelay(0);
			VRITrackingReportTable.setBusy(true);
			var xsURL = "/distributorInsights/vriTracking";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "PUT", controller.onPressSubmitSuccess, controller.onPressSubmitError,
				JSON.stringify(changedInputValues));
		},

		/**
		 * Function that handles the success case of the ajax call to get the VRI Labels
		 * @function onLoadVRILabelsSuccess
		 */
		onLoadVRILabelsSuccess: function (oController, oData, oPiggyBack) {
			var oModelVRIRows = new sap.ui.model.json.JSONModel();
			var oModelVRIColumns = new sap.ui.model.json.JSONModel();
			var oModelEstimatedPayout = new sap.ui.model.json.JSONModel();
			var VRILabelsTable = oController.getView().byId("VRILabelsTable");
			var VRIpayoutTable = oController.getView().byId("VRIpayoutTable");
			VRILabelsTable.setBusy(false);
			VRIpayoutTable.setBusy(false);
			if (oData !== undefined && oData !== null && oData.rows !== undefined && oData.rows !== null && oData.columns !== undefined &&
				oData
				.columns !== null) {
				var estimatedPayout = {
					"value": oData.estimatedPayout,
					"percent": oData.estimatedPayoutPercent
				};
				oModelEstimatedPayout.setData(estimatedPayout);
				oModelVRIRows.setData(oData.rows);
				oModelVRIColumns.setData(oData.columns);
				oController.setModel(oModelVRIRows, "VRITable2Rows");
				oController.setModel(oModelVRIColumns, "VRITable2Columns");
				oController.setModel(oModelEstimatedPayout, "estimatedPayout");

				oController.onBuildGraphVRI();

			} else {
				VRILabelsTable.setVisible(false);
				VRIpayoutTable.setVisible(false);
				oController.getView().byId("submitVRILabelsButton").setVisible(false);
				oController.getView().byId("DistributorLubricantVolumeTable2").setVisible(false);
				oController.getView().byId("VRIpayoutTable").setVisible(false);
			}
			setTimeout(function () {
				VRILabelsTable.setBusy(false);
				VRIpayoutTable.setBusy(false);
			}, 0);
		},

		/**
		 * Function that handles the error case of the ajax call get the VRI labels
		 * @function onLoadVRILabelsError
		 */
		onLoadVRILabelsError: function (oController, oError, oPiggyBack) {
			var VRILabelsTable = oController.getView().byId("VRILabelsTable");
			var VRIpayoutTable = oController.getView().byId("VRIpayoutTable");
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText);
			VRILabelsTable.setBusy(false);
			VRIpayoutTable.setBusy(false);
		},

		/**
		 * VRI LABELS Data Loading
		 * @ onLoadVRILabels
		 */
		onLoadVRILabels: function () {
			var controller = this;
			var VRILabelsTable = this.getView().byId("VRILabelsTable");
			VRILabelsTable.setBusyIndicatorDelay(0);
			VRILabelsTable.setBusy(true);

			var VRIpayoutTable = this.getView().byId("VRIpayoutTable");
			VRIpayoutTable.setBusyIndicatorDelay(0);
			VRIpayoutTable.setBusy(true);

			var xsURL = "/distributorInsights/vriLabels";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadVRILabelsSuccess, controller.onLoadVRILabelsError);
		},

		/**
		 * Refresh Icon pressed to refresh data
		 * @ onRefreshTable
		 */
		onRefreshTable: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			// If there are changed Input values
			if (changedInputValues.length > 0) {
				this.DataLossWarning =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", _oController);
				//Set Warning Dialog Buttons
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(_oController.onRefreshDialog_Refresh);
				sap.ui.getCore().byId("button_Dialog_Warning_No").attachPress(_oController.onRefreshDialog_Close);
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("DLWyesTXT"));
				sap.ui.getCore().byId("button_Dialog_Warning_No").setText(_oController.getResourceBundle().getText("DLWnoTXT"));
				//Set Warning Dialog Message
				sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWRefreshTitle"));
				sap.ui.getCore().byId("text2_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWRefreshDescription"));
				//Set Warnig Dialog Title
				sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("DLWMainTitle"));
				this.DataLossWarning.open();
				_oController._removeDialogResize(_oController.DataLossWarning);
			} else {
				this.onRefreshDialog_Refresh();
			}
		},

		/**
		 * On Refresh Data on Dialog Warning Messages
		 */
		onRefreshDialog_Refresh: function () {
			if (_oController.DataLossWarning) {
				_oController.DataLossWarning.close();
				_oController.DataLossWarning.destroyContent();
				_oController.DataLossWarning.destroy();
			}
			// Clear Edited Input Values and reload Data
			changedInputValues = [];
			_oController.onLoadVRITracking();
			_oController.onLoadVRILabels();
		},

		/**
		 * On Close Dialog Warning Messages
		 */
		onRefreshDialog_Close: function () {
			if (_oController.DataLossWarning) {
				_oController.DataLossWarning.close();
				_oController.DataLossWarning.destroyContent();
				_oController.DataLossWarning.destroy();
			}
		},

		/**
		 * Generate VRI graphs
		 * @function onBuildDistributorLubricantVolumeTYGraph
		 */
		onBuildGraphVRI: function () {
			var vriLabelsModel = this.getModel("VRITable2Rows");
			var mainDiv = this.getView().byId("vriDynamicGraphsContainer");
			mainDiv.destroyItems();

			var i;
			var gap;
			var colorPalette = [{
				color: ['#c9c9c9', '#009cd9']
			}, {
				color: ['#c9c9c9', '#e1261c']
			}, {
				color: ['#c9c9c9', '#085e8f']
			}];

			var ngraphsDrawnRow = 0;
			for (i = 0; i < vriLabelsModel.getData().length; i++) {

				if (vriLabelsModel.getData().length >= (i + 1) && vriLabelsModel.getData()[i].valueColumn2 <= 0) {
					gap = 0;
				} else {
					gap = vriLabelsModel.getData()[i].valueColumn2;
				}

				var chartModelData = {
					"items": [{
						"value": vriLabelsModel.getData()[i].valueColumn0,
						"anual": vriLabelsModel.getData()[i].valueColumn1,
						"gap": gap
					}]
				};

				var chartModel = new sap.ui.model.json.JSONModel();
				chartModel.setData(chartModelData);

				var vriColumnsModel = this.getModel("VRITable2Columns");
				var Anual = vriColumnsModel.getData()[1].labelColumn;

				var GapToTarget;
				if (chartModelData.items[0].gap === 0) {
					GapToTarget = this.getResourceBundle().getText("INGTSDISTVRIOver");
				} else {
					GapToTarget = this.getResourceBundle().getText("INGTSDISTVRIGap");
				}

				//build graph divs
				var graphContainer = this.getView().byId("VRI" + ngraphsDrawnRow);
				if (graphContainer === undefined) {
					graphContainer = new sap.m.VBox({
						"id": "VRI" + ngraphsDrawnRow
					}).addStyleClass("insightsCardFrame_VRI");
					mainDiv.addItem(graphContainer);
				}

				//build graph header
				var graphHeader = new sap.m.VBox().addStyleClass("insightsCardTitle");
				graphContainer.addItem(graphHeader);

				var graphTitle = new sap.m.Text({
					text: vriLabelsModel.getData()[i].valueColumn0,
					textAlign: sap.ui.core.TextAlign.Left
				}).addStyleClass("pageSubtitle graphicalCardText-distributors");
				graphHeader.addItem(graphTitle);

				var dividerLine = new sap.m.HBox().addStyleClass("dividerLine dividerLine_distributors");
				graphHeader.addItem(dividerLine);

				//build graph
				var graphVBox = new sap.m.VBox().addStyleClass("insightsCardGraphContainer");
				graphContainer.addItem(graphVBox);

				var graph = this.getView().byId("VRI" + i + "frame");
				if (graph === undefined) {
					graph = new sap.viz.ui5.controls.VizFrame({
						vizType: "column",
						id: "VRI" + i + "frame",
						width: "90%",
						legendVisible: "true"
					}).addStyleClass("insightsCardGraph");
					graphVBox.addItem(graph);
				}

				ngraphsDrawnRow++;

				graph.destroyFeeds();
				graph.destroyDataset();
				var oDataset = new sap.viz.ui5.data.FlattenedDataset({
					dimensions: [{
						name: 'Value',
						value: "{value}"
					}],

					measures: [{
						name: GapToTarget,
						value: '{gap}'
					}, {
						name: Anual,
						value: '{anual}'
					}],

					data: {
						path: "/items"
					}
				});

				graph.setDataset(oDataset);
				graph.setVizType('stacked_column');
				graph.setModel(chartModel);
				var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [GapToTarget, Anual]
					}),
					feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Value"]
					});
				graph.addFeed(feedValueAxis);
				graph.addFeed(feedCategoryAxis);

				// Layout configuration file
				graph.setVizProperties({
					plotArea: {
						colorPalette: colorPalette[(i) % 3].color
					},
					valueAxis: {
						title: {
							visible: false
						}
					},
					categoryAxis: {
						title: {
							visible: false
						}
					},
					title: {
						visible: false
					},
					legendGroup: {
						layout: {
							position: 'top',
							maxWidth: 100,
							height: 50,
							alignment: "start"
						}
					},
					legend: {
						visible: true,
						title: {
							visible: false
						},
						label: {
							style: {
								fontSize: "12px",
								fontWeight: "lighter",
								fontFamily: "Roboto"
							}
						}
					}
				});
			}
		},

		/**
		 * Function that handles the success case of the ajax call that loads the Distributor Lubricant Volume
		 * @function onLoadDistributorLubVolSuccess
		 */
		onLoadDistributorLubVolSuccess: function (oController, oData, oPiggyBack) {
			var graphContainer = oController.getView().byId("LubricantVolumeGraphContainer");
			var cardGraphID1 = oController.getView().byId("cardGraphDistributorLubricantVolumeTY");
			var cardGraphID2 = oController.getView().byId("cardGraphDistributorLubricantVolumeYTD");
			var table = oController.getView().byId("DistributorLubricantVolumeTable");
			var oModelRows = new sap.ui.model.json.JSONModel();
			var oModelColumns = new sap.ui.model.json.JSONModel();
			var oModelGraph = new sap.ui.model.json.JSONModel();
			if (oData !== undefined && oData !== null && oData.rows !== undefined && oData.rows !== null && oData.columns !== undefined &&
				oData
				.columns !== null) {
				oModelColumns.setData(oData.columns);
				oModelRows.setData(oData.rows);
				oModelGraph.setData(oData.rows.slice(0, oData.rows.length - 1));
				cardGraphID1.setBusy(false);
				cardGraphID1.setModel(oModelGraph);
				cardGraphID2.setBusy(false);
				cardGraphID2.setModel(oModelGraph);
				table.setBusy(false);
				_oView.setModel(oModelRows, "DistributorLubricantVolume");
				_oView.setModel(oModelColumns, "DistributorLubricantVolumeColumns");

				oController.onBuildDistributorLubricantVolumeTYGraph();
				oController.onBuildDistributorLubricantVolumeYTDGraph();

				var minValue = oData.minValue;
				var maxValue = oData.maxValue;

				var PreviousYear1 = "";
				if (oData.columns[1]) {
					PreviousYear1 = oData.columns[1].columnName;
				}
				var CurrentYear1 = "";
				if (oData.columns[2]) {
					CurrentYear1 = oData.columns[2].columnName;
				}
				var PreviousYear2 = "";
				if (oData.columns[3]) {
					PreviousYear2 = oData.columns[3].columnName;
				}

				var CurrentYear2 = "";
				if (oData.columns[4]) {
					CurrentYear2 = oData.columns[4].columnName;
				}

				oController.setGraphRange(cardGraphID1, cardGraphID2, minValue, maxValue);
				oController.setGraphLegend(cardGraphID1, cardGraphID2, PreviousYear1, CurrentYear1, PreviousYear2, CurrentYear2);
			} else {
				_oView.setModel(new sap.ui.model.json.JSONModel(), "DistributorLubricantVolume");
				graphContainer.setVisible(false);
				cardGraphID1.setBusy(false);
				cardGraphID2.setBusy(false);
				table.setBusy(false);
				table.removeStyleClass("insightsTable_Distributor_Total");
			}
		},

		/**
		 * Function that handles the error case of the ajax call that loads the Distributor Lubricant Volume
		 * @function onLoadDistributorLubVolError
		 */
		onLoadDistributorLubVolError: function (oController, oError, oPiggyBack) {
			var graphContainer = oController.getView().byId("LubricantVolumeGraphContainer");
			var cardGraphID1 = oController.getView().byId("cardGraphDistributorLubricantVolumeTY");
			var cardGraphID2 = oController.getView().byId("cardGraphDistributorLubricantVolumeYTD");
			var table = oController.getView().byId("DistributorLubricantVolumeTable");
			graphContainer.setVisible(false);
			cardGraphID1.setBusy(false);
			cardGraphID2.setBusy(false);
			table.setBusy(false);
			table.removeStyleClass("insightsTable_Distributor_Total");
		},

		/**
		 * This function loads the Data for the Distributor Lubricant Volume
		 * @function onLoadDataDistributorLubricantVolume
		 */
		onLoadDataDistributorLubricantVolume: function () {
			var graphContainer = this.getView().byId("LubricantVolumeGraphContainer");
			graphContainer.setVisible(true);
			var cardGraphID1 = this.getView().byId("cardGraphDistributorLubricantVolumeTY");
			cardGraphID1.setBusyIndicatorDelay(0);
			cardGraphID1.setBusy(true);

			var cardGraphID2 = this.getView().byId("cardGraphDistributorLubricantVolumeYTD");
			cardGraphID2.setBusyIndicatorDelay(0);
			cardGraphID2.setBusy(true);

			var table = this.getView().byId("DistributorLubricantVolumeTable");
			table.addStyleClass("insightsTable_Distributor_Total");

			/* Service Call */
			var controller = this;
			var xsURL = "/distributorInsights/lubricant";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadDistributorLubVolSuccess, controller.onLoadDistributorLubVolError);
		},

		/**
		 * This function generates the Distributor Lubricant Volume Total Year Graph in the page.
		 * @function onBuildDistributorLubricantVolumeTYGraph
		 */
		onBuildDistributorLubricantVolumeTYGraph: function () {
			var PreviousYear = "placeholder1";
			var CurrentYear = "placeholder2";
			var cardGraphID = this.getView().byId("cardGraphDistributorLubricantVolumeTY");
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Distributors',
					value: "{DistributorDetails}"
				}],

				measures: [{
					name: PreviousYear,
					value: '{PreviousYear-Total}'
				}, {
					name: CurrentYear,
					value: '{CurrentYear-Total}'
				}],

				data: {
					path: "/"
				}
			});

			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [PreviousYear, CurrentYear]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Distributors"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);

			// Layout configuration file
			cardGraphID.setVizProperties({
				plotArea: {
					colorPalette: ['#009cd9', '#e1261c']
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					},
					label: {
						angle: 30
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						fontSize: 20,
						position: 'top',
						maxWidth: 100,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * This function generates the VPS Graph Graph in the page
		 * @function onBuildDistributorLubricantVolumeYTDGraph
		 */
		onBuildDistributorLubricantVolumeYTDGraph: function () {
			var PreviousYear = "placeholder1";
			var CurrentYear = "placeholder2";

			var cardGraphID = this.getView().byId("cardGraphDistributorLubricantVolumeYTD");

			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Distributors',
					value: "{DistributorDetails}"
				}],

				measures: [{
					name: PreviousYear,
					value: '{PreviousYear-YTD}'
				}, {
					name: CurrentYear,
					value: '{CurrentYear-YTD}'
				}],

				data: {
					path: "/"
				}
			});

			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [PreviousYear, CurrentYear]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Distributors"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);

			// Layout configuration file
			cardGraphID.setVizProperties({
				plotArea: {
					colorPalette: ['#009cd9', '#e1261c']
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					},
					label: {
						angle: 30
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						fontSize: 20,
						position: 'top',
						maxWidth: 100,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * Function that handles the success case of the ajax call that loads the distributor anti freeze volume data
		 * @function onLoadDataDistributorAntiFreezeVolumeSuccess
		 */
		onLoadDataDistributorAntiFreezeVolumeSuccess: function (oController, oData, oPiggyBack) {
			var oModelRows = new sap.ui.model.json.JSONModel();
			var oModelColumns = new sap.ui.model.json.JSONModel();
			var oModelGraph = new sap.ui.model.json.JSONModel();
			var graphContainer = oController.getView().byId("AntiFreezeVolumeGraphContainer");
			var cardGraphID1 = oController.getView().byId("cardGraphDistributorAntiFreezeVolumeTY");
			var cardGraphID2 = oController.getView().byId("cardGraphDistributorAntiFreezeVolumeYTD");
			var table = oController.getView().byId("DistributorAntiFreezeVolumeTable");

			if (oData !== undefined && oData !== null && oData.rows !== undefined && oData.rows !== null && oData.columns !== undefined &&
				oData
				.columns !== null) {
				oModelColumns.setData(oData.columns);
				oModelRows.setData(oData.rows);
				oModelGraph.setData(oData.rows.slice(0, oData.rows.length - 1));
				cardGraphID1.setBusy(false);
				cardGraphID1.setModel(oModelGraph);
				cardGraphID2.setBusy(false);
				cardGraphID2.setModel(oModelGraph);
				table.setBusy(false);
				_oView.setModel(oModelRows, "DistributorAntiFreezeVolume");
				_oView.setModel(oModelColumns, "DistributorAntiFreezeVolumeColumns");

				oController.onBuildDistributorAntiFreezeVolumeTYGraph();
				oController.onBuildDistributorAntiFreezeVolumeYTDGraph();

				var minValue = oData.minValue;
				var maxValue = oData.maxValue;

				var PreviousYear1 = "";
				if (oData.columns[1]) {
					PreviousYear1 = oData.columns[1].columnName;
				}
				var CurrentYear1 = "";
				if (oData.columns[2]) {
					CurrentYear1 = oData.columns[2].columnName;
				}
				var PreviousYear2 = "";
				if (oData.columns[3]) {
					PreviousYear2 = oData.columns[3].columnName;
				}

				var CurrentYear2 = "";
				if (oData.columns[4]) {
					CurrentYear2 = oData.columns[4].columnName;
				}

				oController.setGraphRange(cardGraphID1, cardGraphID2, minValue, maxValue);
				oController.setGraphLegend(cardGraphID1, cardGraphID2, PreviousYear1, CurrentYear1, PreviousYear2, CurrentYear2);
			} else {
				_oView.setModel(new sap.ui.model.json.JSONModel(), "DistributorAntiFreezeVolume");
				graphContainer.setVisible(false);
				cardGraphID1.setBusy(false);
				cardGraphID2.setBusy(false);
				table.setBusy(false);
				table.removeStyleClass("insightsTable_Distributor_Total");
			}

		},

		/**
		 * Function that handles the error case of the ajax call that loads the distributor anti freeze volume data
		 * @function onLoadDataDistributorAntiFreezeVolumeError
		 */
		onLoadDataDistributorAntiFreezeVolumeError: function (oController, oError, oPiggyBack) {
			var graphContainer = oController.getView().byId("AntiFreezeVolumeGraphContainer");
			var cardGraphID1 = oController.getView().byId("cardGraphDistributorAntiFreezeVolumeTY");
			var cardGraphID2 = oController.getView().byId("cardGraphDistributorAntiFreezeVolumeYTD");
			var table = oController.getView().byId("DistributorAntiFreezeVolumeTable");

			graphContainer.setVisible(false);
			cardGraphID1.setBusy(false);
			cardGraphID2.setBusy(false);
			table.setBusy(false);
			table.removeStyleClass("insightsTable_Distributor_Total");
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRAntifreeze"));

		},

		/**
		 * This functions Loads Data for the Anti Freeze Gallons Purchased Card (2th Graph type Card).
		 * @function onLoadDataAntiFreezeGraph
		 */
		onLoadDataDistributorAntiFreezeVolume: function () {
			var graphContainer = this.getView().byId("AntiFreezeVolumeGraphContainer");
			graphContainer.setVisible(true);
			var cardGraphID1 = this.getView().byId("cardGraphDistributorAntiFreezeVolumeTY");
			cardGraphID1.setBusyIndicatorDelay(0);
			cardGraphID1.setBusy(true);

			var cardGraphID2 = this.getView().byId("cardGraphDistributorAntiFreezeVolumeYTD");
			cardGraphID2.setBusyIndicatorDelay(0);
			cardGraphID2.setBusy(true);

			var table = this.getView().byId("DistributorAntiFreezeVolumeTable");
			table.addStyleClass("insightsTable_Distributor_Total");

			/* Service Call */
			var controller = this;

			var xsURL = "/distributorInsights/antifreeze";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(_oController, true, xsURL, "GET", controller.onLoadDataDistributorAntiFreezeVolumeSuccess, controller
				.onLoadDataDistributorAntiFreezeVolumeError);
		},

		/**
		 * This function generates the Premium Motor Oil Gallons Purchased Graph (1th Graph type Card).
		 * @function onBuildDistributorAntiFreezeVolumeTYGraph
		 */
		onBuildDistributorAntiFreezeVolumeTYGraph: function () {
			var PreviousYear = "placeholder1";
			var CurrentYear = "placeholder2";

			var cardGraphID = this.getView().byId("cardGraphDistributorAntiFreezeVolumeTY");
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Distributors',
					value: "{DistributorDetails}"
				}],

				measures: [{
					name: PreviousYear,
					value: '{PreviousYear-Total}'
				}, {
					name: CurrentYear,
					value: '{CurrentYear-Total}'
				}],

				data: {
					path: "/"
				}
			});

			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [PreviousYear, CurrentYear]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Distributors"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);

			// Layout configuration file
			cardGraphID.setVizProperties({
				plotArea: {
					colorPalette: ['#009cd9', '#e1261c']
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						fontSize: 20,
						position: 'top',
						maxWidth: 100,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * This function generates the Anti Freeze Graph (2th Graph type Card).
		 * @function onBuildDistributorAntiFreezeVolumeYTDGraph
		 */
		onBuildDistributorAntiFreezeVolumeYTDGraph: function () {
			var PreviousYear = "placeholder1";
			var CurrentYear = "placeholder2";

			var cardGraphID = this.getView().byId("cardGraphDistributorAntiFreezeVolumeYTD");
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Distributors',
					value: "{DistributorDetails}"
				}],

				measures: [{
					name: PreviousYear,
					value: '{PreviousYear-YTD}'
				}, {
					name: CurrentYear,
					value: '{CurrentYear-YTD}'
				}],

				data: {
					path: "/"
				}
			});

			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [PreviousYear, CurrentYear]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Distributors"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);

			// Layout configuration file
			cardGraphID.setVizProperties({
				plotArea: {
					colorPalette: ['#009cd9', '#e1261c']
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						fontSize: 20,
						position: 'top',
						maxWidth: 100,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * Function that handles the success case of the ajax call that loads the distributor non lubricant revenue data
		 * @function onLoadDataDistributorNonLubricantRevenueSuccess
		 */
		onLoadDataDistributorNonLubricantRevenueSuccess: function (oController, oData, oPiggyBack) {
			var graphContainer = oController.getView().byId("NonLubricantRevenueGraphContainer");
			var cardGraphID1 = oController.getView().byId("cardGraphDistributorNonLubricantRevenueTY");
			var cardGraphID2 = oController.getView().byId("cardGraphDistributorNonLubricantRevenueYTD");
			var table = oController.getView().byId("DistributorNonLubricantRevenueTable");
			var oModelRows = new sap.ui.model.json.JSONModel();
			var oModelColumns = new sap.ui.model.json.JSONModel();
			var oModelGraph = new sap.ui.model.json.JSONModel();
			if (oData !== undefined && oData !== null && oData.rows !== undefined && oData.rows !== null && oData.columns !== undefined &&
				oData
				.columns !== null) {
				oModelColumns.setData(oData.columns);
				oModelRows.setData(oData.rows);
				oModelGraph.setData(oData.rows);
				cardGraphID1.setBusy(false);
				cardGraphID1.setModel(oModelGraph);
				cardGraphID2.setBusy(false);
				cardGraphID2.setModel(oModelGraph);
				table.setBusy(false);
				_oView.setModel(oModelRows, "DistributorNonLubricantRevenue");
				_oView.setModel(oModelColumns, "DistributorNonLubricantRevenueColumns");

				oController.onBuildDistributorNonLubricantRevenueTYCard();
				oController.onBuildDistributorNonLubricantRevenueYTDCard();

				var minValue = oData.minValue;
				var maxValue = oData.maxValue;

				var PreviousYear1 = "";
				if (oData.columns[1]) {
					PreviousYear1 = oData.columns[1].columnName;
				}
				var CurrentYear1 = "";
				if (oData.columns[2]) {
					CurrentYear1 = oData.columns[2].columnName;
				}
				var PreviousYear2 = "";
				if (oData.columns[3]) {
					PreviousYear2 = oData.columns[3].columnName;
				}

				var CurrentYear2 = "";
				if (oData.columns[4]) {
					CurrentYear2 = oData.columns[4].columnName;
				}

				oController.setGraphRange(cardGraphID1, cardGraphID2, minValue, maxValue);
				oController.setGraphLegend(cardGraphID1, cardGraphID2, PreviousYear1, CurrentYear1, PreviousYear2, CurrentYear2);
			} else {
				_oView.setModel(new sap.ui.model.json.JSONModel(), "DistributorNonLubricantRevenue");
				graphContainer.setVisible(false);
				cardGraphID1.setBusy(false);
				cardGraphID2.setBusy(false);
				table.setBusy(false);
			}
		},

		/**
		 * Function that handles the error case of the ajax call that loads the distributor non lubricant revenue data
		 * @function onLoadDataDistributorNonLubricantRevenueError
		 */
		onLoadDataDistributorNonLubricantRevenueError: function (oController, oError, oPiggyBack) {
			var graphContainer = oController.getView().byId("NonLubricantRevenueGraphContainer");
			var cardGraphID1 = oController.getView().byId("cardGraphDistributorNonLubricantRevenueTY");
			var cardGraphID2 = oController.getView().byId("cardGraphDistributorNonLubricantRevenueYTD");
			var table = oController.getView().byId("DistributorNonLubricantRevenueTable");
			graphContainer.setVisible(false);
			cardGraphID1.setBusy(false);
			cardGraphID2.setBusy(false);
			table.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRWipers"));
		},

		/**
		 * This function Loads Data for the Wiper Units Purchased  (3th Graph type Card).
		 * @function onLoadDataWiperUnitsCard -
		 */
		onLoadDataDistributorNonLubricantRevenue: function () {
			var graphContainer = this.getView().byId("NonLubricantRevenueGraphContainer");
			graphContainer.setVisible(true);
			var cardGraphID1 = this.getView().byId("cardGraphDistributorNonLubricantRevenueTY");
			cardGraphID1.setBusyIndicatorDelay(0);
			cardGraphID1.setBusy(true);

			var cardGraphID2 = this.getView().byId("cardGraphDistributorNonLubricantRevenueYTD");
			cardGraphID2.setBusyIndicatorDelay(0);
			cardGraphID2.setBusy(true);

			/* Service Call */
			var controller = this;
			var xsURL = "/distributorInsights/nonLubricantRev";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadDataDistributorNonLubricantRevenueSuccess,
				controller.onLoadDataDistributorNonLubricantRevenueError);
		},

		/**
		 * This function generates the Distributor Non Lubricant Revenue Total Year
		 * @function onBuildDistributorNonLubricantRevenueTYCard
		 */
		onBuildDistributorNonLubricantRevenueTYCard: function () {
			var PreviousYear = "placeholder1";
			var CurrentYear = "placeholder2";

			var cardGraphID = this.getView().byId("cardGraphDistributorNonLubricantRevenueTY");
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Distributors',
					value: "{DistributorDetails}"
				}],

				measures: [{
					name: PreviousYear,
					value: '{PreviousYear-Total}'
				}, {
					name: CurrentYear,
					value: '{CurrentYear-Total}'
				}],

				data: {
					path: "/"
				}
			});

			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [PreviousYear, CurrentYear]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Distributors"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);

			// Layout configuration file
			cardGraphID.setVizProperties({
				plotArea: {
					colorPalette: ['#009cd9', '#e1261c']
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						fontSize: 20,
						position: 'top',
						maxWidth: 100,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * This function generates the Filter Units Card  (4th Graph type Card).
		 * @function onBuildFilterUnitsGraph
		 */
		onBuildDistributorNonLubricantRevenueYTDCard: function () {
			var PreviousYear = "placeholder1";
			var CurrentYear = "placeholder2";

			var cardGraphID = this.getView().byId("cardGraphDistributorNonLubricantRevenueYTD");

			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Distributors',
					value: "{DistributorDetails}"
				}],

				measures: [{
					name: PreviousYear,
					value: '{PreviousYear-YTD}'
				}, {
					name: CurrentYear,
					value: '{CurrentYear-YTD}'
				}],

				data: {
					path: "/"
				}
			});

			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [PreviousYear, CurrentYear]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Distributors"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);

			// Layout configuration file
			cardGraphID.setVizProperties({
				plotArea: {
					colorPalette: ['#009cd9', '#e1261c']
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						fontSize: 20,
						position: 'top',
						maxWidth: 100,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * Function that handles the success case of the ajax call that loads the distributor non lubricant volume data
		 * @function onLoadDataDistributorNonLubricantVolumeSuccess
		 */
		onLoadDataDistributorNonLubricantVolumeSuccess: function (oController, oData, oPiggyBack) {
			var graphContainer = oController.getView().byId("NonLubricantVolumeGraphContainer");
			var cardGraphID1 = oController.getView().byId("cardGraphDistributorNonLubricantVolumeTY");
			var cardGraphID2 = oController.getView().byId("cardGraphDistributorNonLubricantVolumeYTD");
			var table = oController.getView().byId("DistributorNonLubricantVolumeTable");
			var oModelRows = new sap.ui.model.json.JSONModel();
			var oModelColumns = new sap.ui.model.json.JSONModel();
			var oModelGraph = new sap.ui.model.json.JSONModel();
			if (oData !== undefined && oData !== null && oData.rows !== undefined && oData.rows !== null && oData.columns !== undefined &&
				oData
				.columns !== null) {
				oModelColumns.setData(oData.columns);
				oModelRows.setData(oData.rows);
				oModelGraph.setData(oData.rows);
				cardGraphID1.setBusy(false);
				cardGraphID1.setModel(oModelGraph);
				cardGraphID2.setBusy(false);
				cardGraphID2.setModel(oModelGraph);
				table.setBusy(false);
				_oView.setModel(oModelRows, "DistributorNonLubricantVolume");
				_oView.setModel(oModelColumns, "DistributorNonLubricantVolumeColumns");

				oController.onBuildDistributorNonLubricantVolumeTYCard();
				oController.onBuildDistributorNonLubricantVolumeYTDCard();

				var minValue = oData.minValue;
				var maxValue = oData.maxValue;

				var PreviousYear1 = "";
				if (oData.columns[1]) {
					PreviousYear1 = oData.columns[1].columnName;
				}
				var CurrentYear1 = "";
				if (oData.columns[2]) {
					CurrentYear1 = oData.columns[2].columnName;
				}
				var PreviousYear2 = "";
				if (oData.columns[3]) {
					PreviousYear2 = oData.columns[3].columnName;
				}

				var CurrentYear2 = "";
				if (oData.columns[4]) {
					CurrentYear2 = oData.columns[4].columnName;
				}

				oController.setGraphRange(cardGraphID1, cardGraphID2, minValue, maxValue);
				oController.setGraphLegend(cardGraphID1, cardGraphID2, PreviousYear1, CurrentYear1, PreviousYear2, CurrentYear2);
			} else {
				_oView.setModel(new sap.ui.model.json.JSONModel(), "DistributorNonLubricantVolume");
				graphContainer.setVisible(false);
				cardGraphID1.setBusy(false);
				cardGraphID2.setBusy(false);
				table.setBusy(false);
			}

		},

		/**
		 * Function that handles the error case of the ajax call that loads the distributor non lubricant volume data
		 * @function onLoadDataDistributorNonLubricantVolumeError
		 */
		onLoadDataDistributorNonLubricantVolumeError: function (oController, oError, oPiggyBack) {
			var graphContainer = oController.getView().byId("NonLubricantVolumeGraphContainer");
			var cardGraphID1 = oController.getView().byId("cardGraphDistributorNonLubricantVolumeTY");
			var cardGraphID2 = oController.getView().byId("cardGraphDistributorNonLubricantVolumeYTD");
			var table = oController.getView().byId("DistributorNonLubricantVolumeTable");
			graphContainer.setVisible(false);
			cardGraphID1.setBusy(false);
			cardGraphID2.setBusy(false);
			table.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRWipers"));

		},

		/**
		 * This function Loads Data for the Wiper Units Purchased  (3th Graph type Card).
		 * @function onLoadDataWiperUnitsCard -
		 */
		onLoadDataDistributorNonLubricantVolume: function () {

			var graphContainer = this.getView().byId("NonLubricantVolumeGraphContainer");
			graphContainer.setVisible(true);

			var cardGraphID1 = this.getView().byId("cardGraphDistributorNonLubricantVolumeTY");
			cardGraphID1.setBusyIndicatorDelay(0);
			cardGraphID1.setBusy(true);

			var cardGraphID2 = this.getView().byId("cardGraphDistributorNonLubricantVolumeYTD");
			cardGraphID2.setBusyIndicatorDelay(0);
			cardGraphID2.setBusy(true);

			/* Service Call */
			var controller = this;
			var xsURL = "/distributorInsights/nonLubricantVol";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", _oController.onLoadDataDistributorNonLubricantVolumeSuccess,
				controller.onLoadDataDistributorNonLubricantVolumeError);
		},

		/**
		 * This function generates the Distributor Non Lubricant Revenue Total Year
		 * @function onBuildDistributorNonLubricantRevenueTYCard
		 */
		onBuildDistributorNonLubricantVolumeTYCard: function () {
			var PreviousYear = "placeholder1";
			var CurrentYear = "placeholder2";

			var cardGraphID = this.getView().byId("cardGraphDistributorNonLubricantVolumeTY");

			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Distributors',
					value: "{DistributorDetails}"
				}],

				measures: [{
					name: PreviousYear,
					value: '{PreviousYear-Total}'
				}, {
					name: CurrentYear,
					value: '{CurrentYear-Total}'
				}],

				data: {
					path: "/"
				}
			});

			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [PreviousYear, CurrentYear]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Distributors"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);

			// Layout configuration file
			cardGraphID.setVizProperties({
				plotArea: {
					colorPalette: ['#009cd9', '#e1261c']
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						fontSize: 20,
						position: 'top',
						maxWidth: 100,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * This function generates the Filter Units Card  (4th Graph type Card).
		 * @function onBuildFilterUnitsGraph
		 */
		onBuildDistributorNonLubricantVolumeYTDCard: function () {
			var PreviousYear = "placeholder1";
			var CurrentYear = "placeholder2";

			var cardGraphID = this.getView().byId("cardGraphDistributorNonLubricantVolumeYTD");

			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Distributors',
					value: "{DistributorDetails}"
				}],

				measures: [{
					name: PreviousYear,
					value: '{PreviousYear-YTD}'
				}, {
					name: CurrentYear,
					value: '{CurrentYear-YTD}'
				}],

				data: {
					path: "/"
				}
			});

			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [PreviousYear, CurrentYear]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Distributors"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);

			// Layout configuration file
			cardGraphID.setVizProperties({
				plotArea: {
					colorPalette: ['#009cd9', '#e1261c']
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						fontSize: 20,
						position: 'top',
						maxWidth: 100,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * Function that handles the success case of the ajax call that loads the distributor delivery volume data
		 * @function onLoadDataDistributorDeliveryVolumeSuccess
		 */
		onLoadDataDistributorDeliveryVolumeSuccess: function (oController, oData, oPiggyBack) {
			var graphContainer = oController.getView().byId("DeliveryVolumeGraphContainer");
			var cardGraphID1 = oController.getView().byId("cardGraphDistributorDeliveryVolumeTY");
			var cardGraphID2 = oController.getView().byId("cardGraphDistributorDeliveryVolumeYTD");
			var table = oController.getView().byId("DistributorDeliveryVolumeTable");
			var oModelRows = new sap.ui.model.json.JSONModel();
			var oModelColumns = new sap.ui.model.json.JSONModel();
			var oModelGraph = new sap.ui.model.json.JSONModel();

			if (oData !== undefined && oData !== null && oData.rows !== undefined && oData.rows !== null && oData.columns !== undefined &&
				oData
				.columns !== null) {
				oModelColumns.setData(oData.columns);
				oModelRows.setData(oData.rows);
				oModelGraph.setData(oData.rows.slice(0, oData.rows.length - 1));
				cardGraphID1.setBusy(false);
				cardGraphID1.setModel(oModelGraph);
				cardGraphID2.setBusy(false);
				cardGraphID2.setModel(oModelGraph);
				table.setBusy(false);
				_oView.setModel(oModelRows, "DistributorDeliveryVolume");
				_oView.setModel(oModelColumns, "DistributorDeliveryVolumeColumns");

				oController.onBuildDistributorDeliveryVolumeTYCard();
				oController.onBuildDistributorDeliveryVolumeYTDCard();

				var minValue = oData.minValue;
				var maxValue = oData.maxValue;

				var PreviousYear1 = "";
				if (oData.columns[1]) {
					PreviousYear1 = oData.columns[1].columnName;
				}
				var CurrentYear1 = "";
				if (oData.columns[2]) {
					CurrentYear1 = oData.columns[2].columnName;
				}
				var PreviousYear2 = "";
				if (oData.columns[3]) {
					PreviousYear2 = oData.columns[3].columnName;
				}

				var CurrentYear2 = "";
				if (oData.columns[4]) {
					CurrentYear2 = oData.columns[4].columnName;
				}

				oController.setGraphRange(cardGraphID1, cardGraphID2, minValue, maxValue);
				oController.setGraphLegend(cardGraphID1, cardGraphID2, PreviousYear1, CurrentYear1, PreviousYear2, CurrentYear2);
			} else {
				_oView.setModel(new sap.ui.model.json.JSONModel(), "DistributorDeliveryVolume");
				graphContainer.setVisible(false);
				cardGraphID1.setBusy(false);
				cardGraphID2.setBusy(false);
				table.setBusy(false);
				table.removeStyleClass("insightsTable_Distributor_Total");
			}

		},

		/**
		 * Function that handles the error case of the ajax call that loads the distributor delivery volume data
		 * @function onLoadDataDistributorDeliveryVolumeError
		 */
		onLoadDataDistributorDeliveryVolumeError: function (oController, oError, oPiggyBack) {
			var graphContainer = oController.getView().byId("DeliveryVolumeGraphContainer");
			var cardGraphID1 = oController.getView().byId("cardGraphDistributorDeliveryVolumeTY");
			var cardGraphID2 = oController.getView().byId("cardGraphDistributorDeliveryVolumeYTD");
			var table = oController.getView().byId("DistributorDeliveryVolumeTable");

			graphContainer.setVisible(false);
			cardGraphID1.setBusy(false);
			cardGraphID2.setBusy(false);
			table.setBusy(false);
			table.removeStyleClass("insightsTable_Distributor_Total");
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRWipers"));
		},

		/**
		 * This function Loads Data for the Distributor Delivery Volume.
		 * @function onLoadDataDistributorDeliveryVolume -
		 */
		onLoadDataDistributorDeliveryVolume: function () {

			var graphContainer = this.getView().byId("DeliveryVolumeGraphContainer");
			graphContainer.setVisible(true);

			var cardGraphID1 = this.getView().byId("cardGraphDistributorDeliveryVolumeTY");
			cardGraphID1.setBusyIndicatorDelay(0);
			cardGraphID1.setBusy(true);

			var cardGraphID2 = this.getView().byId("cardGraphDistributorDeliveryVolumeYTD");
			cardGraphID2.setBusyIndicatorDelay(0);
			cardGraphID2.setBusy(true);

			var table = this.getView().byId("DistributorDeliveryVolumeTable");

			table.addStyleClass("insightsTable_Distributor_Total");

			/* Service Call */
			var controller = this;
			var xsURL = "/distributorInsights/delivery";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(_oController, true, xsURL, "GET", controller.onLoadDataDistributorDeliveryVolumeSuccess, controller.onLoadDataDistributorDeliveryVolumeError);
		},

		/**
		 * Function that handles the success case of the ajax call that loads the distributor delivery allowance data
		 * @function onLoadDataDistributorDeliveryAllowanceSuccess
		 */
		onLoadDataDistributorDeliveryAllowanceSuccess: function (oController, oData, oPiggyBack) {
			var graphContainer = oController.getView().byId("DeliveryAllowanceGraphContainer");
			var cardGraphID1 = oController.getView().byId("cardGraphDistributorDeliveryAllowanceTY");
			var cardGraphID2 = oController.getView().byId("cardGraphDistributorDeliveryAllowanceYTD");
			var table = oController.getView().byId("DistributorDeliveryAllowanceTable");
			var oModelRows = new sap.ui.model.json.JSONModel();
			var oModelColumns = new sap.ui.model.json.JSONModel();
			var oModelGraph = new sap.ui.model.json.JSONModel();

			if (oData !== undefined && oData !== null && oData.rows !== undefined && oData.rows !== null && oData.columns !== undefined &&
				oData
				.columns !== null) {
				oModelColumns.setData(oData.columns);
				oModelRows.setData(oData.rows);
				oModelGraph.setData(oData.rows);
				cardGraphID1.setBusy(false);
				cardGraphID1.setModel(oModelGraph);
				cardGraphID2.setBusy(false);
				cardGraphID2.setModel(oModelGraph);
				table.setBusy(false);
				_oView.setModel(oModelRows, "DistributorDeliveryAllowance");
				_oView.setModel(oModelColumns, "DistributorDeliveryAllowanceColumns");

				oController.onBuildDistributorDeliveryAllowanceTYCard();
				oController.onBuildDistributorDeliveryAllowanceYTDCard();

				var maxValue = oData.maxValue;

				var allowanceName = oData.columns[0].columnName;

				var PreviousYear1 = "";
				if (oData.columns[1]) {
					PreviousYear1 = oData.columns[1].columnName;
				}
				var CurrentYear1 = "";
				if (oData.columns[2]) {
					CurrentYear1 = oData.columns[2].columnName;
				}
				var PreviousYear2 = "";
				if (oData.columns[3]) {
					PreviousYear2 = oData.columns[3].columnName;
				}

				var CurrentYear2 = "";
				if (oData.columns[4]) {
					CurrentYear2 = oData.columns[4].columnName;
				}

				oController.setGraphRangeDeliveryAllowance(cardGraphID1, cardGraphID2, maxValue);
				oController.setGraphLegendDeliveryAllowance(cardGraphID1, cardGraphID2, allowanceName, PreviousYear1, CurrentYear1,
					PreviousYear2, CurrentYear2);
			} else {
				_oView.setModel(new sap.ui.model.json.JSONModel(), "DistributorDeliveryAllowance");
				graphContainer.setVisible(false);
				cardGraphID1.setBusy(false);
				cardGraphID2.setBusy(false);
				table.setBusy(false);
			}

		},

		/**
		 * Function that handles the error case of the ajax call that loads the distributor delivery allowance data
		 * @function onLoadDataDistributorDeliveryAllowanceError
		 */
		onLoadDataDistributorDeliveryAllowanceError: function (oController, oError, oPiggyBack) {
			var graphContainer = oController.getView().byId("DeliveryAllowanceGraphContainer");
			var cardGraphID1 = oController.getView().byId("cardGraphDistributorDeliveryAllowanceTY");
			var cardGraphID2 = oController.getView().byId("cardGraphDistributorDeliveryAllowanceYTD");
			var table = oController.getView().byId("DistributorDeliveryAllowanceTable");

			graphContainer.setVisible(false);
			cardGraphID1.setBusy(false);
			cardGraphID2.setBusy(false);
			table.setBusy(false);
			table.removeStyleClass("insightsTable_Distributor_Total");
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRWipers"));

		},

		/**
		 * This function Loads Data for the Distributor Delivery Allowance.
		 * @function onLoadDataDistributorDeliveryAllowance -
		 */
		onLoadDataDistributorDeliveryAllowance: function () {

			var graphContainer = this.getView().byId("DeliveryAllowanceGraphContainer");
			graphContainer.setVisible(true);

			var cardGraphID1 = this.getView().byId("cardGraphDistributorDeliveryAllowanceTY");
			cardGraphID1.setBusyIndicatorDelay(0);
			cardGraphID1.setBusy(true);

			var cardGraphID2 = this.getView().byId("cardGraphDistributorDeliveryAllowanceYTD");
			cardGraphID2.setBusyIndicatorDelay(0);
			cardGraphID2.setBusy(true);

			/* Service Call */
			var controller = this;

			var xsURL = "/distributorInsights/deliveryAllowance";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(_oController, true, xsURL, "GET", controller.onLoadDataDistributorDeliveryAllowanceSuccess,
				controller.onLoadDataDistributorDeliveryAllowanceError);
		},

		/**
		 * This function generates the Distributor Non Lubricant Revenue Total Year
		 * @function onBuildDistributorNonLubricantRevenueTYCard
		 */
		onBuildDistributorDeliveryVolumeTYCard: function () {
			var PreviousYear = "placeholder1";
			var CurrentYear = "placeholder2";

			var cardGraphID = this.getView().byId("cardGraphDistributorDeliveryVolumeTY");
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Distributors',
					value: "{DistributorDetails}"
				}],

				measures: [{
					name: PreviousYear,
					value: '{PreviousYear-Total}'
				}, {
					name: CurrentYear,
					value: '{CurrentYear-Total}'
				}],

				data: {
					path: "/"
				}
			});

			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [PreviousYear, CurrentYear]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Distributors"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);

			// Layout configuration file
			cardGraphID.setVizProperties({
				plotArea: {
					colorPalette: ['#009cd9', '#e1261c']
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						fontSize: 20,
						position: 'top',
						maxWidth: 100,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * This function generates the Distributor delivery Allowance Total Year
		 * @function onBuildDistributorDeliveryAllowanceTYCard
		 */
		onBuildDistributorDeliveryAllowanceTYCard: function () {
			var PreviousYear = "placeholder1";
			var CurrentYear = "placeholder2";

			var cardGraphID = this.getView().byId("cardGraphDistributorDeliveryAllowanceTY");
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Distributors',
					value: "{valueColumn0}"
				}],

				measures: [{
					name: PreviousYear,
					value: '{valueColumn1}'
				}, {
					name: CurrentYear,
					value: '{valueColumn2}'
				}],

				data: {
					path: "/"
				}
			});

			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [PreviousYear, CurrentYear]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Distributors"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);

			// Layout configuration file
			cardGraphID.setVizProperties({
				plotArea: {
					colorPalette: ['#009cd9', '#e1261c']
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						fontSize: 20,
						position: 'top',
						maxWidth: 100,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * This function generates the Filter Units Card  (4th Graph type Card).
		 * @function onBuildFilterUnitsGraph
		 */
		onBuildDistributorDeliveryVolumeYTDCard: function () {
			var PreviousYear = "placeholder1";
			var CurrentYear = "placeholder2";

			var cardGraphID = this.getView().byId("cardGraphDistributorDeliveryVolumeYTD");

			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Distributors',
					value: "{valueColumn0}"
				}],

				measures: [{
					name: PreviousYear,
					value: '{valueColumn3}'
				}, {
					name: CurrentYear,
					value: '{valueColumn4}'
				}],

				data: {
					path: "/"
				}
			});

			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [PreviousYear, CurrentYear]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Distributors"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);

			// Layout configuration file
			cardGraphID.setVizProperties({
				plotArea: {
					colorPalette: ['#009cd9', '#e1261c']
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						fontSize: 20,
						position: 'top',
						maxWidth: 100,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * This function generates the Filter Units Card  (4th Graph type Card).
		 * @function onBuildFilterUnitsGraph
		 */
		onBuildDistributorDeliveryAllowanceYTDCard: function () {
			var PreviousYear = "placeholder1";
			var CurrentYear = "placeholder2";

			var cardGraphID = this.getView().byId("cardGraphDistributorDeliveryAllowanceYTD");

			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Distributors',
					value: "{valueColumn0}"
				}],

				measures: [{
					name: PreviousYear,
					value: '{valueColumn3}'
				}, {
					name: CurrentYear,
					value: '{valueColumn4}'
				}],

				data: {
					path: "/"
				}
			});

			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [PreviousYear, CurrentYear]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Distributors"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);

			// Layout configuration file
			cardGraphID.setVizProperties({
				plotArea: {
					colorPalette: ['#009cd9', '#e1261c']
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						fontSize: 20,
						position: 'top',
						maxWidth: 100,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * Builds the graphics legend
		 * @function setGraphLegend
		 * @param {string} cardGraphID1 - ID of graphic Total
		 * @param {string} cardGraphID2 - ID of graphic YTD
		 * @param {string} PreviousYear1 - Legend for graphic Total
		 * @param {string} CurrentYear1 - Legend for graphic Total
		 * @param {string} PreviousYear2 - Legend for graphic YTD
		 * @param {string} CurrentYear2 - Legend for graphic YTD
		 */
		setGraphLegend: function (cardGraphID1, cardGraphID2, PreviousYear1, CurrentYear1, PreviousYear2, CurrentYear2) {

			if (PreviousYear1 && CurrentYear1) {
				var oDataset1 = new sap.viz.ui5.data.FlattenedDataset({
					dimensions: [{
						name: 'Distributors',
						value: "{DistributorDetails}"
					}],
					measures: [{
						name: PreviousYear1,
						value: '{PreviousYear-Total}'
					}, {
						name: CurrentYear1,
						value: '{CurrentYear-Total}'
					}],
					data: {
						path: "/"
					}
				});

				cardGraphID1.setDataset(oDataset1);

				var feedValueAxis1 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [PreviousYear1, CurrentYear1]
					}),
					feedCategoryAxis1 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Distributors"]
					});
				cardGraphID1.removeAllFeeds();
				cardGraphID1.addFeed(feedValueAxis1);
				cardGraphID1.addFeed(feedCategoryAxis1);
			} else if (PreviousYear1) {
				var oDataset2 = new sap.viz.ui5.data.FlattenedDataset({
					dimensions: [{
						name: 'Distributors',
						value: "{DistributorDetails}"
					}],
					measures: [{
						name: PreviousYear1,
						value: '{PreviousYear-Total}'
					}],
					data: {
						path: "/"
					}
				});

				cardGraphID1.setDataset(oDataset2);

				var feedValueAxis2 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [PreviousYear1]
					}),
					feedCategoryAxis2 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Distributors"]
					});
				cardGraphID1.removeAllFeeds();
				cardGraphID1.addFeed(feedValueAxis2);
				cardGraphID1.addFeed(feedCategoryAxis2);
			} else if (CurrentYear1) {
				var oDataset3 = new sap.viz.ui5.data.FlattenedDataset({
					dimensions: [{
						name: 'Distributors',
						value: "{DistributorDetails}"
					}],
					measures: [{
						name: CurrentYear1,
						value: '{CurrentYear-Total}'
					}],
					data: {
						path: "/"
					}
				});

				cardGraphID1.setDataset(oDataset3);

				var feedValueAxis3 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [CurrentYear1]
					}),
					feedCategoryAxis3 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Distributors"]
					});
				cardGraphID1.removeAllFeeds();
				cardGraphID1.addFeed(feedValueAxis3);
				cardGraphID1.addFeed(feedCategoryAxis3);
			} else {
				var noDataContainer1 = this.getView().byId(cardGraphID1.getId() + "-noData");
				cardGraphID1.addStyleClass("hidden");
				cardGraphID1.addStyleClass("hidden");
				noDataContainer1.removeStyleClass("hidden");

			}

			if (PreviousYear2 && CurrentYear2) {
				//graphID2
				var oDataset4 = new sap.viz.ui5.data.FlattenedDataset({
					dimensions: [{
						name: 'Distributors',
						value: "{DistributorDetails}"
					}],
					measures: [{
						name: PreviousYear2,
						value: '{PreviousYear-YTD}'
					}, {
						name: CurrentYear2,
						value: '{CurrentYear-YTD}'
					}],
					data: {
						path: "/"
					}
				});

				cardGraphID2.setDataset(oDataset4);

				var feedValueAxis4 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [PreviousYear2, CurrentYear2]
					}),
					feedCategoryAxis4 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Distributors"]
					});
				cardGraphID2.removeAllFeeds();
				cardGraphID2.addFeed(feedValueAxis4);
				cardGraphID2.addFeed(feedCategoryAxis4);
			} else if (PreviousYear2) {
				//graphID2
				var oDataset5 = new sap.viz.ui5.data.FlattenedDataset({
					dimensions: [{
						name: 'Distributors',
						value: "{DistributorDetails}"
					}],
					measures: [{
						name: PreviousYear2,
						value: '{PreviousYear-YTD}'
					}],
					data: {
						path: "/"
					}
				});

				cardGraphID2.setDataset(oDataset5);

				var feedValueAxis5 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [PreviousYear2]
					}),
					feedCategoryAxis5 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Distributors"]
					});
				cardGraphID2.removeAllFeeds();
				cardGraphID2.addFeed(feedValueAxis5);
				cardGraphID2.addFeed(feedCategoryAxis5);
			} else if (CurrentYear2) {
				//graphID2
				var oDataset6 = new sap.viz.ui5.data.FlattenedDataset({
					dimensions: [{
						name: 'Distributors',
						value: "{DistributorDetails}"
					}],
					measures: [{
						name: CurrentYear2,
						value: '{CurrentYear-YTD}'
					}],
					data: {
						path: "/"
					}
				});

				cardGraphID2.setDataset(oDataset6);

				var feedValueAxis6 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [CurrentYear2]
					}),
					feedCategoryAxis6 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Distributors"]
					});
				cardGraphID2.removeAllFeeds();
				cardGraphID2.addFeed(feedValueAxis6);
				cardGraphID2.addFeed(feedCategoryAxis6);
			} else {
				var noDataContainer2 = this.getView().byId(cardGraphID2.getId() + "-noData");
				cardGraphID2.addStyleClass("hidden");
				noDataContainer2.removeStyleClass("hidden");
			}
		},

		/**
		 * Builds the graphics legend
		 * @function setGraphLegendDeliveryAllowance
		 * @param {string} cardGraphID1 - ID of graphic Total
		 * @param {string} cardGraphID2 - ID of graphic YTD
		 * @param {string} PreviousYear1 - Legend for graphic Total
		 * @param {string} CurrentYear1 - Legend for graphic Total
		 * @param {string} PreviousYear2 - Legend for graphic YTD
		 * @param {string} CurrentYear2 - Legend for graphic YTD
		 */
		setGraphLegendDeliveryAllowance: function (cardGraphID1, cardGraphID2, allowanceName, PreviousYear1, CurrentYear1,
			PreviousYear2,
			CurrentYear2) {
			if (PreviousYear1 && CurrentYear1) {
				var oDataset1 = new sap.viz.ui5.data.FlattenedDataset({
					dimensions: [{
						name: 'Distributors',
						value: allowanceName
					}],
					measures: [{
						name: PreviousYear1,
						value: '{valueColumn1}'
					}, {
						name: CurrentYear1,
						value: '{valueColumn2}'
					}],
					data: {
						path: "/"
					}
				});

				cardGraphID1.setDataset(oDataset1);

				var feedValueAxis1 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [PreviousYear1, CurrentYear1]
					}),
					feedCategoryAxis1 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Distributors"]
					});
				cardGraphID1.removeAllFeeds();
				cardGraphID1.addFeed(feedValueAxis1);
				cardGraphID1.addFeed(feedCategoryAxis1);
			} else if (PreviousYear1) {
				var oDataset2 = new sap.viz.ui5.data.FlattenedDataset({
					dimensions: [{
						name: 'Distributors',
						value: allowanceName
					}],
					measures: [{
						name: PreviousYear1,
						value: '{valueColumn1}'
					}],
					data: {
						path: "/"
					}
				});

				cardGraphID1.setDataset(oDataset2);

				var feedValueAxis2 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [PreviousYear1]
					}),
					feedCategoryAxis2 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Distributors"]
					});
				cardGraphID1.removeAllFeeds();
				cardGraphID1.addFeed(feedValueAxis2);
				cardGraphID1.addFeed(feedCategoryAxis2);
			} else if (CurrentYear1) {
				var oDataset3 = new sap.viz.ui5.data.FlattenedDataset({
					dimensions: [{
						name: 'Distributors',
						value: allowanceName
					}],
					measures: [{
						name: CurrentYear1,
						value: '{valueColumn2}'
					}],
					data: {
						path: "/"
					}
				});

				cardGraphID1.setDataset(oDataset3);

				var feedValueAxis3 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [CurrentYear1]
					}),
					feedCategoryAxis3 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Distributors"]
					});
				cardGraphID1.removeAllFeeds();
				cardGraphID1.addFeed(feedValueAxis3);
				cardGraphID1.addFeed(feedCategoryAxis3);
			} else {
				var noDataContainer1 = this.getView().byId(cardGraphID1.getId() + "-noData");
				cardGraphID1.addStyleClass("hidden");
				noDataContainer1.removeStyleClass("hidden");
			}

			if (PreviousYear2 && CurrentYear2) {
				//graphID2
				var oDataset4 = new sap.viz.ui5.data.FlattenedDataset({
					dimensions: [{
						name: 'Distributors',
						value: allowanceName
					}],
					measures: [{
						name: PreviousYear2,
						value: '{valueColumn3}'
					}, {
						name: CurrentYear2,
						value: '{valueColumn4}'
					}],
					data: {
						path: "/"
					}
				});

				cardGraphID2.setDataset(oDataset4);

				var feedValueAxis4 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [PreviousYear2, CurrentYear2]
					}),
					feedCategoryAxis4 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Distributors"]
					});
				cardGraphID2.removeAllFeeds();
				cardGraphID2.addFeed(feedValueAxis4);
				cardGraphID2.addFeed(feedCategoryAxis4);
			} else if (PreviousYear2) {
				//graphID2
				var oDataset5 = new sap.viz.ui5.data.FlattenedDataset({
					dimensions: [{
						name: 'Distributors',
						value: allowanceName
					}],
					measures: [{
						name: PreviousYear2,
						value: '{valueColumn3}'
					}],
					data: {
						path: "/"
					}
				});

				cardGraphID2.setDataset(oDataset5);

				var feedValueAxis5 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [PreviousYear2]
					}),
					feedCategoryAxis5 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Distributors"]
					});
				cardGraphID2.removeAllFeeds();
				cardGraphID2.addFeed(feedValueAxis5);
				cardGraphID2.addFeed(feedCategoryAxis5);
			} else if (CurrentYear2) {
				//graphID2
				var oDataset6 = new sap.viz.ui5.data.FlattenedDataset({
					dimensions: [{
						name: 'Distributors',
						value: allowanceName
					}],
					measures: [{
						name: CurrentYear2,
						value: '{valueColumn4}'
					}],
					data: {
						path: "/"
					}
				});

				cardGraphID2.setDataset(oDataset6);

				var feedValueAxis6 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [CurrentYear2]
					}),
					feedCategoryAxis6 = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "categoryAxis",
						'type': "Dimension",
						'values': ["Distributors"]
					});
				cardGraphID2.removeAllFeeds();
				cardGraphID2.addFeed(feedValueAxis6);
				cardGraphID2.addFeed(feedCategoryAxis6);
			} else {
				var noDataContainer2 = this.getView().byId(cardGraphID2.getId() + "-noData");
				cardGraphID2.addStyleClass("hidden");
				noDataContainer2.removeStyleClass("hidden");
			}
		},

		/**
		 * Defines the graphics range
		 * @function setGraphRange
		 * @param {string} cardGraphID1 - ID of graphic Total
		 * @param {string} cardGraphID2 - ID of graphic YTD
		 * @param {integer} minValue - minimum range
		 * @param {integer} maxValue - maximum range
		 */
		setGraphRange: function (cardGraphID1, cardGraphID2, minValue, maxValue) {
			var min = this.roundNumberForGraphRange(minValue);
			var max = this.roundNumberForGraphRange(maxValue);
			cardGraphID1.setVizProperties({
				yAxis: {
					scale: {
						fixedRange: true,
						minValue: min,
						maxValue: max
					}
				}
			});
			cardGraphID2.setVizProperties({
				yAxis: {
					scale: {
						fixedRange: true,
						minValue: min,
						maxValue: max
					}
				}
			});
		},

		/**
		 * Defines the graphics range
		 * @function setGraphRangeDeliveryAllowance
		 * @param {string} cardGraphID1 - ID of graphic Total
		 * @param {string} cardGraphID2 - ID of graphic YTD
		 * @param {integer} maxValue - maximum range
		 */
		setGraphRangeDeliveryAllowance: function (cardGraphID1, cardGraphID2, maxValue) {
			var max = this.roundNumberForGraphRange(maxValue);
			cardGraphID1.setVizProperties({
				yAxis: {
					scale: {
						fixedRange: true,
						maxValue: max
					}
				}
			});
			cardGraphID2.setVizProperties({
				yAxis: {
					scale: {
						fixedRange: true,
						maxValue: max
					}
				}
			});
		},

		/**
		 * Checks if the values of the columns are lesser than 0, if so returns a message instead of the value
		 **/
		formatGapToTarget: function (sValue) {
			if (sValue !== undefined && sValue <= 0) {
				return this.getResourceBundle().getText("INGTSDISTVRIOver");
			} else {
				return this.formatValuesGroupingSeparator(sValue);
			}
		},

		/**
		 * Checks if the (USD) mark shoud be added to a column or not.
		 **/
		formatShowUSD: function (columnName) {
			return !(columnName === "Non-Lubricant Revenue");
		},

		/**
		 * Sets the number of rows to show in the initial VRI table
		 * @function formatVRITableRowNum
		 **/
		formatVRITableRowNum: function () {
			if (_oController.getModel("VRITable2Rows") !== undefined && _oController.getModel("VRITable2Rows").getData() !== undefined) {
				return _oController.getModel("VRITable2Rows").getData().length;
			}
		},
		/**
		 * Check if table have Data
		 * @function formatterNoInsightsVisiblity
		 **/
		formatterNoInsightsVisiblity: function (model, table) {
			if (Object.keys(model).length === 0) {
				_oController.getView().byId(table).addStyleClass("fix-border-distrIns");
				return true;
			}
			return false;
		}
	});
});