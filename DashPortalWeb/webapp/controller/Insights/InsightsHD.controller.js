sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function (Base) {
	"use strict";

	var _oView, _oController, _oContact, _oAccount, _iLBLoading, _iPBLoading, _iAFLoading, _iCHLoading;
	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Insights.InsightsHD", {

		onInit: function () {
			Base.prototype.onInit.call(this);
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("insightsHD").attachPatternMatched(this._onObjectMatched, this);
		},

		/**
		 * Set the menu as active if the app is reloaded and makes an element in the filter disabled
		 * @function onAfterRendering
		 */
		onAfterRendering: function () {
			/* Set the menu as active if the app is reloaded */
			$(".js-insights-sidenav").addClass("sidenav__item--active");

			//Remove the focus of the input
			$(':focus').blur();
		},

		/**
		 * This function is called everytime the page is loaded, and refresh the data and the page opening.
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {

			_oController = this;
			_oView = this.getView();
			_iLBLoading = 0;
			_iPBLoading = 0;
			_iAFLoading = 0;
			_iCHLoading = 0;
			// Clears all error message
			_oController.clearServerErrorMsg(_oController);

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "insights"
			});

			// Page Layout Configuration
			//
			if (!_oController.checkDASHPageAuthorization("INSIGHTSHD_PAGE")) {
				return;
			}

			//get user info and direct account
			this.getUserInformation(this, this.toRunAfterGettingUserInfo);

			// Check Language and set selected Key in combo box
			_oController.onChangeLanguage();

			_oController.getAccountsByDistrict(_oController, "comboBoxInsightsHD", false, this.reconfigAddressLabel);

			_oController.headerIconAccess();

			// Shopping Cart Values
			_oController.getShoppingCartCookie(_oController);
			// Year Values
			_oController.YearCookieValues();

			//Setup BDF
			if (_oController.checkDASHLayoutAccess("HD_BDF")) {
				_oView.byId("BusinessDevelopmentFunds").setVisible(true);
				_oController.getBDFData("", "");
			} else {
				_oView.byId("BusinessDevelopmentFunds").setVisible(false);
			}
			//SetUp Lubricant, Premium Blue, AntiFreeze && Chemicals
			if (_oController.checkDASHLayoutAccess("INSIGHTSHD_GRAPH")) {
				_oView.byId("Lubricant").setVisible(true);
				_oView.byId("PremBlue").setVisible(true);
				_oView.byId("Antifreeze").setVisible(true);
				_oView.byId("Chemicals").setVisible(true);
				_oController.getLubricantData("", "");
				_oController.getPremBlueData("", "");
				_oController.getAntiFreezeData("", "");
				_oController.getChemicalsData("", "");
			} else {
				_oView.byId("Lubricant").setVisible(false);
				_oView.byId("PremBlue").setVisible(false);
				_oView.byId("Antifreeze").setVisible(false);
				_oView.byId("Chemicals").setVisible(false);
			}
			//Remove the focus of the input
			$(':focus').blur();
		},

		/**
		 * This function handles the logic that requires the user information to be available.
		 * @function toRunAfterGettingUserInfo
		 * @param {object} controller - the page context
		 */
		toRunAfterGettingUserInfo: function (controller) {
			_oContact = sap.ui.getCore().getModel("oAccounts").getData();
			if (_oContact.accounts.length > 0) {
				var oAccount = new sap.ui.model.json.JSONModel(_oContact.accounts[0]);
				sap.ui.getCore().setModel(oAccount, "oSelectedAccount");
				_oView.setModel(oAccount, "oSelectedAccount");
			} else {
				_oController.noDataPageLayout();
			}
		},

		/**
		 * Function that handles the success case of the ajax call that gets the BDF Data
		 * @function getBDFDataSuccess
		 */
		getBDFDataSuccess: function (oController, oData, oPiggyBack) {
			var i, oCells, sBindingPath, oText;
			oCells = _oView.byId("BusinessDevelopmentFundsTableCells");
			oCells.destroyCells();
			_oView.byId("BusinessDevelopmentFundsTable").destroyItems();
			if (oData !== undefined && oData.header !== undefined && oData.header !== "") {
				_oView.byId("INGTSHDBDFTableTitle").setText(oData.header);
				_oView.byId("BusDevFundsToolbar").setVisible(true);
				_oView.byId("BusinessDevelopmentFundsTableNoData").setVisible(false);
				_oView.byId("BusinessDevelopmentFundsTable").setVisible(true);
			} else {
				_oView.byId("BusDevFundsToolbar").setVisible(false);
				_oView.byId("BusinessDevelopmentFundsTableNoData").setVisible(true);
				_oView.byId("BusinessDevelopmentFundsTable").setVisible(false);
			}
			if (oPiggyBack.xhr.status === 204) {
				if (_oView.getModel("BusinessDevelopmentFunds") !== undefined) {
					_oView.getModel("BusinessDevelopmentFunds").setData({});
				} else {
					_oView.setModel(new sap.ui.model.json.JSONModel({}), "BusinessDevelopmentFunds");
				}
				_oView.byId("BusinessDevelopmentFundsTable").setBusyIndicatorDelay(0).setBusy(false);
			} else {
				if (oData !== undefined) {
					for (i = 0; i < oData.columns.length; i++) {
						sBindingPath = "BusinessDevelopmentFunds>" + oData.columns[i].columnName;
						oText = new sap.m.Text({
							text: {
								path: sBindingPath,
								formatter: _oController.numberFormatter
							}
						});
						oText.addStyleClass("table-cell-number-align-hd-insights");
						oText.addStyleClass("bodyCopy2 marginRightTables");
						oCells.addCell(oText);
					}
					_oView.setModel(new sap.ui.model.json.JSONModel(oData), "BusinessDevelopmentFunds");
					_oView.byId("BusinessDevelopmentFundsTable").setBusyIndicatorDelay(0).setBusy(false);
				}
			}
		},

		/**
		 * Function that handles the error case of the ajax call that gets the BDF Data
		 * @function getBDFDataError
		 */
		getBDFDataError: function (oController, oError, oPiggyBack) {
			_oView.byId("BusinessDevelopmentFundsTable").setBusyIndicatorDelay(0).setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"INGTSHDBDFError"));
			return null;
		},

		/**
		 * Function responsible for retrieving the BDF data
		 * @ getBDFData
		 */
		getBDFData: function (AccountNumber, districtFlag) {
			_oView.byId("BusinessDevelopmentFundsTable").setBusyIndicatorDelay(0).setBusy(true);
			var selectedLanguage, selectedPeriod, selectedLocation;
			if (sap.ui.getCore().getModel("selectedLanguage") === undefined) {
				selectedLanguage = this.getSelectedLanguageForLangCode();
			} else {
				selectedLanguage = sap.ui.getCore().getModel("selectedLanguage").getData().selectedLanguage;
			}
			selectedPeriod = _oView.byId("comboBoxYearLabel").getSelectedKey();
			if (AccountNumber !== "0" && districtFlag === "0") {
				selectedLocation = "&ShipTo=" + AccountNumber;
			} else if (AccountNumber === "0" && districtFlag !== "0") {
				selectedLocation = "&customerreportinggroup=" + districtFlag;
			} else {
				selectedLocation = "";
			}

			var xsURL = "/hdInsights/hdBusinessDevFunds?lang=" + selectedLanguage + "&period=" + selectedPeriod + selectedLocation;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.getBDFDataSuccess, _oController.getBDFDataError);
		},

		/**
		 * Function that handles the success case of the ajax call that gets the Lubricant Data
		 * @function getLubricantDataSuccess
		 */
		getLubricantDataSuccess: function (oController, oData, oPiggyBack) {
			var i, j, oCells, sBindingPath, oText, bColCheck, aHideColumns;
			oCells = _oView.byId("LubricantTableCells");
			oCells.destroyCells();
			_oView.byId("LubricantTable").destroyItems();
			aHideColumns = [];
			if (oData !== undefined && oData.rows !== undefined) {
				for (i = 0; i < oData.columns.length; i++) {
					bColCheck = false;
					for (j = 0; j < oData.rows.length; j++) {
						if (oData.rows[j][oData.columns[i].columnName] !== undefined && oData.rows[j][oData.columns[i].columnName] !== "") {
							bColCheck = true;
							j = oData.rows.length;
						}
					}

					sBindingPath = "Lubricant>" + oData.columns[i].columnName;
					oText = new sap.m.Text({
						text: {
							path: sBindingPath,
							formatter: _oController.numberFormatter
						}
					});
					oText.addStyleClass("table-cell-number-align-hd-insights");
					oText.addStyleClass("bodyCopy2 marginRightTables");
					oCells.addCell(oText);
					if (bColCheck === false) {
						aHideColumns.push(i);
					} else if (_oView.byId("LubricantTable").getColumns()[i] !== undefined) {
						_oView.byId("LubricantTable").getColumns()[i].setVisible(true);
					}
				}
				if (_oView.getModel("Lubricant") !== undefined) {
					_oView.getModel("Lubricant").setData(oData);
					_oView.byId("LubricantTable").updateItems();
				} else {
					_oView.setModel(new sap.ui.model.json.JSONModel(oData), "Lubricant");
				}
				for (i = 0; i < aHideColumns.length; i++) {
					_oView.byId("LubricantTable").getColumns()[aHideColumns[i]].setVisible(false);
				}
				_oView.byId("LubricantTable").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphLubricantYTD").setVisible(true);
				_oView.byId("cardGraphLubricantYTD-noData").setVisible(false);
				_oView.byId("cardGraphLubricantTY").setVisible(true);
				_oView.byId("cardGraphLubricantTY-noData").setVisible(false);
				_oController.onBuildGraphs("Lubricant", "TY", aHideColumns);
				_oController.onBuildGraphs("Lubricant", "YTD", aHideColumns);

				_oView.byId("LubricantTableNoData").setVisible(false);

			} else {
				if (_oView.getModel("Lubricant") !== undefined) {
					_oView.getModel("Lubricant").setData({});
				} else {
					_oView.setModel(new sap.ui.model.json.JSONModel({}), "Lubricant");
				}
				_oView.byId("LubricantTable").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphLubricantYTD").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphLubricantTY").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphLubricantYTD").setVisible(false);
				_oView.byId("cardGraphLubricantYTD-noData").setVisible(true);
				_oView.byId("cardGraphLubricantTY").setVisible(false);
				_oView.byId("cardGraphLubricantTY-noData").setVisible(true);
				_oView.byId("LubricantTableNoData").setVisible(true);
			}
		},

		/**
		 * Function that handles the error case of the ajax call that gets the Lubricant Data
		 * @function getLubricantDataError
		 */
		getLubricantDataError: function (oController, oError, oPiggyBack) {
			_oView.byId("LubricantTable").setBusyIndicatorDelay(0).setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"INGTSHDLubricantError"));
			return null;
		},

		/**
		 * Function responsible for retrieving the Lubricant data
		 * @ getLubricantData
		 */
		getLubricantData: function (AccountNumber, districtFlag) {
			if (_iLBLoading > 0) {
				var selectedLanguage, selectedLocation, selectedPeriod;
				_oView.byId("LubricantTable").setBusyIndicatorDelay(0).setBusy(true);
				_oView.byId("cardGraphLubricantTY").setBusyIndicatorDelay(0).setBusy(true);
				_oView.byId("cardGraphLubricantYTD").setBusyIndicatorDelay(0).setBusy(true);
				if (sap.ui.getCore().getModel("selectedLanguage") === undefined) {
					selectedLanguage = this.getSelectedLanguageForLangCode();
				} else {
					selectedLanguage = sap.ui.getCore().getModel("selectedLanguage").getData().selectedLanguage;
				}
				selectedPeriod = _oView.byId("comboBoxYearLabel").getSelectedKey();

				if (AccountNumber !== "0" && districtFlag === "0") {
					selectedLocation = "&ShipTo=" + AccountNumber;
				} else if (AccountNumber === "0" && districtFlag !== "0") {
					selectedLocation = "&customerreportinggroup=" + districtFlag;
				} else {
					selectedLocation = "";
				}

				var xsURL = "/hdInsights/hdlubricant" + "?lang=" + selectedLanguage + "&period=" + selectedPeriod + selectedLocation;
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.getLubricantDataSuccess, _oController.getLubricantDataError);
			} else {
				_iLBLoading++;
			}
		},

		/**
		 * Function that handles the success case of the ajax call that gets the PremBlue Data
		 * @function getPremBlueDataSuccess
		 */
		getPremBlueDataSuccess: function (oController, oData, oPiggyBack) {
			var i, j, bColCheck, aHideColumns, pbTreeModel, oChardData;

			aHideColumns = [];
			if (oData !== undefined && oData.rows !== undefined) {
				pbTreeModel = oController.convertToTreeTable(oData);
				oChardData = oController.pbShortenLabels(oData);
				for (i = 0; i < 6; i++) {
					bColCheck = false;
					for (j = 0; j < oData.rows.length; j++) {
						if (oData.columns[i] === undefined) {
							continue;
						}
						if (oData.rows[j][oData.columns[i].columnName] !== undefined && oData.rows[j][oData.columns[i].columnName] !== "") {
							bColCheck = true;
							j = oData.rows.length;
						}
					}
					if (bColCheck === false) {
						aHideColumns.push(i);
					} else if (_oView.byId("PremBlueTable").getColumns()[i] !== undefined) {
						_oView.byId("PremBlueTable").getColumns()[i].setVisible(true);
					}
				}
				if (_oView.getModel("PremBlue") !== undefined) {
					_oView.getModel("PremBlueTree").setData(pbTreeModel);
					_oView.getModel("PremBlue").setData(oChardData);
				} else {
					_oView.setModel(new sap.ui.model.json.JSONModel(pbTreeModel), "PremBlueTree");
					_oView.setModel(new sap.ui.model.json.JSONModel(oChardData), "PremBlue");
				}
				for (i = 0; i < aHideColumns.length; i++) {
					_oView.byId("PremBlueTable").getColumns()[aHideColumns[i]].setVisible(false);
				}

				_oView.byId("PremBlueTable").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("PremBlueTable").setVisible(true);
				_oView.byId("PremBlueTableNoData").setVisible(false);
				_oView.byId("cardGraphPremBlueYTD").setVisible(true);
				_oView.byId("cardGraphPremBlueYTD-noData").setVisible(false);
				_oView.byId("cardGraphPremBlueTY").setVisible(true);
				_oView.byId("cardGraphPremBlueTY-noData").setVisible(false);
				_oController.onBuildGraphs("PremBlue", "TY", aHideColumns);
				_oController.onBuildGraphs("PremBlue", "YTD", aHideColumns);

			} else {
				if (_oView.getModel("PremBlue") !== undefined) {
					_oView.getModel("PremBlue").setData({});
					_oView.getModel("PremBlueTree").setData({});

				} else {
					_oView.setModel(new sap.ui.model.json.JSONModel({}), "PremBlue");
					_oView.setModel(new sap.ui.model.json.JSONModel({}), "PremBlueTree");
				}
				_oView.getModel("PremBlueTree").getData().nRows = 1;
				_oView.byId("PremBlueTable").setVisible(false);
				_oView.byId("PremBlueTableNoData").setVisible(true);
				_oView.byId("PremBlueTable").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphPremBlueYTD").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphPremBlueTY").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphPremBlueYTD").setVisible(false);
				_oView.byId("cardGraphPremBlueYTD-noData").setVisible(true);
				_oView.byId("cardGraphPremBlueTY").setVisible(false);
				_oView.byId("cardGraphPremBlueTY-noData").setVisible(true);
			}
		},

		/**
		 * Function that handles the error case of the ajax call that gets the PremBlue Data
		 * @function getPremBlueDataError
		 */
		getPremBlueDataError: function (oController, oError, oPiggyBack) {
			_oView.byId("PremBlueTable").setBusyIndicatorDelay(0).setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"INGTSHDPremBlueError"));
			return null;
		},

		/**
		 * Function responsible for retrieving the PremBlue data
		 * @ getPremBlueData
		 */
		getPremBlueData: function (AccountNumber, districtFlag) {
			if (_iPBLoading > 0) {
				var selectedLanguage, selectedLocation, selectedPeriod;
				_oView.byId("PremBlueTable").removeStyleClass("pbTreeTableNoData");
				_oView.byId("PremBlueTable").setBusyIndicatorDelay(0).setBusy(true);
				_oView.byId("cardGraphPremBlueTY").setBusyIndicatorDelay(0).setBusy(true);
				_oView.byId("cardGraphPremBlueYTD").setBusyIndicatorDelay(0).setBusy(true);
				if (sap.ui.getCore().getModel("selectedLanguage") === undefined) {
					selectedLanguage = this.getSelectedLanguageForLangCode();
				} else {
					selectedLanguage = sap.ui.getCore().getModel("selectedLanguage").getData().selectedLanguage;
				}
				selectedPeriod = _oView.byId("comboBoxYearLabel").getSelectedKey();

				if (AccountNumber !== "0" && districtFlag === "0") {
					selectedLocation = "&ShipTo=" + AccountNumber;
				} else if (AccountNumber === "0" && districtFlag !== "0") {
					selectedLocation = "&customerreportinggroup=" + districtFlag;
				} else {
					selectedLocation = "";
				}

				var xsURL = "/hdInsights/hdpremiumblue" + "?lang=" + selectedLanguage + "&period=" + selectedPeriod + selectedLocation;
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.getPremBlueDataSuccess, _oController.getPremBlueDataError);
			} else {
				_iPBLoading++;
			}
		},
		/**
		 * Function that handles the success case of the ajax call that gets the AntiFreeze Data
		 * @function getAntiFreezeDataSuccess
		 */
		getAntiFreezeDataSuccess: function (oController, oData, oPiggyBack) {
			var i, j, oCells, sBindingPath, oText, bColCheck, aHideColumns;
			oCells = _oView.byId("AntifreezeTableCells");
			oCells.destroyCells();
			_oView.byId("AntifreezeTable").destroyItems();
			aHideColumns = [];
			if (oData !== undefined && oData.rows !== undefined) {
				for (i = 0; i < oData.columns.length; i++) {
					bColCheck = false;
					for (j = 0; j < oData.rows.length; j++) {
						if (oData.rows[j][oData.columns[i].columnName] !== undefined && oData.rows[j][oData.columns[i].columnName] !== "") {
							bColCheck = true;
							j = oData.rows.length;
						}
					}

					sBindingPath = "Antifreeze>" + oData.columns[i].columnName;
					oText = new sap.m.Text({
						text: {
							path: sBindingPath,
							formatter: _oController.numberFormatter
						}
					});
					oText.addStyleClass("table-cell-number-align-hd-insights");
					oText.addStyleClass("bodyCopy2 marginRightTables");
					oCells.addCell(oText);
					if (bColCheck === false) {
						aHideColumns.push(i);
					} else if (_oView.byId("AntifreezeTable").getColumns()[i] !== undefined) {
						_oView.byId("AntifreezeTable").getColumns()[i].setVisible(true);
					}
				}
				if (_oView.getModel("Antifreeze") !== undefined) {
					_oView.getModel("Antifreeze").setData(oData);
					_oView.byId("AntifreezeTable").updateItems();
				} else {
					_oView.setModel(new sap.ui.model.json.JSONModel(oData), "Antifreeze");
				}
				for (i = 0; i < aHideColumns.length; i++) {
					_oView.byId("AntifreezeTable").getColumns()[aHideColumns[i]].setVisible(false);
				}
				_oView.byId("AntifreezeTable").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphAntifreezeYTD").setVisible(true);
				_oView.byId("cardGraphAntifreezeYTD-noData").setVisible(false);
				_oView.byId("cardGraphAntifreezeTY").setVisible(true);
				_oView.byId("cardGraphAntifreezeTY-noData").setVisible(false);
				_oController.onBuildGraphs("Antifreeze", "TY", aHideColumns);
				_oController.onBuildGraphs("Antifreeze", "YTD", aHideColumns);

				_oView.byId("AntifreezeTableNoData").setVisible(false);

			} else {
				if (_oView.getModel("Antifreeze") !== undefined) {
					_oView.getModel("Antifreeze").setData({});
				} else {
					_oView.setModel(new sap.ui.model.json.JSONModel({}), "Antifreeze");
				}
				_oView.byId("AntifreezeTable").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphAntifreezeYTD").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphAntifreezeTY").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphAntifreezeYTD").setVisible(false);
				_oView.byId("cardGraphAntifreezeYTD-noData").setVisible(true);
				_oView.byId("cardGraphAntifreezeTY").setVisible(false);
				_oView.byId("cardGraphAntifreezeTY-noData").setVisible(true);
				_oView.byId("AntifreezeTableNoData").setVisible(true);
			}
		},

		/**
		 * Function that handles the error case of the ajax call that gets the AntiFreeze Data
		 * @function getAntiFreezeDataError
		 */
		getAntiFreezeDataError: function (oController, oError, oPiggyBack) {
			_oView.byId("AntifreezeTable").setBusyIndicatorDelay(0).setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"INGTSHDAntFrzError"));
			return null;
		},

		/**
		 * Function responsible for retrieving the AntiFreeze data
		 * @ getAntiFreezeData
		 */
		getAntiFreezeData: function (AccountNumber, districtFlag) {
			if (_iAFLoading > 0) {
				var selectedLanguage, selectedLocation, selectedPeriod;
				_oView.byId("AntifreezeTable").setBusyIndicatorDelay(0).setBusy(true);
				_oView.byId("cardGraphAntifreezeTY").setBusyIndicatorDelay(0).setBusy(true);
				_oView.byId("cardGraphAntifreezeYTD").setBusyIndicatorDelay(0).setBusy(true);
				if (sap.ui.getCore().getModel("selectedLanguage") === undefined) {
					selectedLanguage = this.getSelectedLanguageForLangCode();
				} else {
					selectedLanguage = sap.ui.getCore().getModel("selectedLanguage").getData().selectedLanguage;
				}
				selectedPeriod = _oView.byId("comboBoxYearLabel").getSelectedKey();

				if (AccountNumber !== "0" && districtFlag === "0") {
					selectedLocation = "&ShipTo=" + AccountNumber;
				} else if (AccountNumber === "0" && districtFlag !== "0") {
					selectedLocation = "&customerreportinggroup=" + districtFlag;
				} else {
					selectedLocation = "";
				}

				var xsURL = "/hdInsights/hdantifreeze" + "?lang=" + selectedLanguage + "&period=" + selectedPeriod + selectedLocation;
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.getAntiFreezeDataSuccess, _oController.getAntiFreezeDataError);
			} else {
				_iAFLoading++;
			}
		},

		/**
		 * Function that handles the success case of the ajax call that gets the Chemicals Data
		 * @function getChemicalsDataSuccess
		 */
		getChemicalsDataSuccess: function (oController, oData, oPiggyBack) {
			var i, j, oCells, sBindingPath, oText, bColCheck, aHideColumns;
			oCells = _oView.byId("ChemicalsTableCells");
			oCells.destroyCells();
			_oView.byId("ChemicalsTable").destroyItems();
			aHideColumns = [];
			if (oData !== undefined && oData.rows !== undefined) {
				for (i = 0; i < oData.columns.length; i++) {
					bColCheck = false;
					for (j = 0; j < oData.rows.length; j++) {
						if (oData.rows[j][oData.columns[i].columnName] !== undefined && oData.rows[j][oData.columns[i].columnName] !== "") {
							bColCheck = true;
							j = oData.rows.length;
						}
					}
					if (bColCheck === true) {
						sBindingPath = "Chemicals>" + oData.columns[i].columnName;
						oText = new sap.m.Text({
							text: {
								path: sBindingPath,
								formatter: _oController.numberFormatter
							}
						});
						oText.addStyleClass("bodyCopy2 marginRightTables");
						oText.addStyleClass("table-cell-number-align-hd-insights");
						oCells.addCell(oText);
						if (_oView.byId("ChemicalsTable").getColumns()[i] !== undefined) {
							_oView.byId("ChemicalsTable").getColumns()[i].setVisible(true);
						}
					} else {
						aHideColumns.push(i);
					}
				}
				if (_oView.getModel("Chemicals") !== undefined) {
					_oView.getModel("Chemicals").setData(oData);
					_oView.byId("ChemicalsTable").updateItems();
				} else {
					_oView.setModel(new sap.ui.model.json.JSONModel(oData), "Chemicals");
				}
				for (i = 0; i < aHideColumns.length; i++) {
					_oView.byId("ChemicalsTable").getColumns()[aHideColumns[i]].setVisible(false);
				}
				_oView.byId("ChemicalsTable").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphChemicalsYTD").setVisible(true);
				_oView.byId("cardGraphChemicalsYTD-noData").setVisible(false);
				_oView.byId("cardGraphChemicalsTY").setVisible(true);
				_oView.byId("cardGraphChemicalsTY-noData").setVisible(false);
				_oController.onBuildGraphs("Chemicals", "TY", aHideColumns);
				_oController.onBuildGraphs("Chemicals", "YTD", aHideColumns);
				_oView.byId("ChemicalsTableNoData").setVisible(false);

			} else {
				if (_oView.getModel("Chemicals") !== undefined) {
					_oView.getModel("Chemicals").setData({});
				} else {
					_oView.setModel(new sap.ui.model.json.JSONModel({}), "Chemicals");
				}
				_oView.byId("ChemicalsTable").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphChemicalsYTD").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphChemicalsTY").setBusyIndicatorDelay(0).setBusy(false);
				_oView.byId("cardGraphChemicalsYTD").setVisible(false);
				_oView.byId("cardGraphChemicalsYTD-noData").setVisible(true);
				_oView.byId("cardGraphChemicalsTY").setVisible(false);
				_oView.byId("cardGraphChemicalsTY-noData").setVisible(true);
				_oView.byId("ChemicalsTableNoData").setVisible(true);
			}
		},

		/**
		 * Function that handles the error case of the ajax call that gets the Chemicals Data
		 * @function getChemicalsDataError
		 */
		getChemicalsDataError: function (oController, oError, oPiggyBack) {
			_oView.byId("ChemicalsTable").setBusyIndicatorDelay(0).setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"INGTSHDError"));
			return null;
		},

		/**
		 * Function responsible for retrieving the Chemicals data
		 * @ getChemicalsData
		 */
		getChemicalsData: function (AccountNumber, districtFlag) {
			if (_iCHLoading > 0) {
				var selectedLanguage, selectedLocation, selectedPeriod;
				_oView.byId("ChemicalsTable").setBusyIndicatorDelay(0).setBusy(true);
				_oView.byId("cardGraphChemicalsTY").setBusyIndicatorDelay(0).setBusy(true);
				_oView.byId("cardGraphChemicalsYTD").setBusyIndicatorDelay(0).setBusy(true);
				if (sap.ui.getCore().getModel("selectedLanguage") === undefined) {
					selectedLanguage = this.getSelectedLanguageForLangCode();
				} else {
					selectedLanguage = sap.ui.getCore().getModel("selectedLanguage").getData().selectedLanguage;
				}
				selectedPeriod = _oView.byId("comboBoxYearLabel").getSelectedKey();

				if (AccountNumber !== "0" && districtFlag === "0") {
					selectedLocation = "&ShipTo=" + AccountNumber;
				} else if (AccountNumber === "0" && districtFlag !== "0") {
					selectedLocation = "&customerreportinggroup=" + districtFlag;
				} else {
					selectedLocation = "";
				}

				var xsURL = "/hdInsights/hdchemicals" + "?lang=" + selectedLanguage + "&period=" + selectedPeriod + selectedLocation;
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.getChemicalsDataSuccess, _oController.getChemicalsDataError);
			} else {
				_iCHLoading++;
			}
		},

		/**
		 * This function resets the AddressLabel when you navigate to the page.
		 * @function reconfigAddressLabel
		 */
		reconfigAddressLabel: function () {
			_oController.getView().byId("comboBoxInsightsHD").setSelectedKey(0);
			_oAccount = _oController.findAccountByDistrict("0", _oController);
			sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oController.getView().byId("accountInfoContainerDesktop").setVisible(false);
			_oController.onChangeAccountComboBox("");
		},

		/**
		 * This function configures the noData Page Layout.
		 * @function noDataPageLayout
		 */
		noDataPageLayout: function () {
			//TODO
		},

		/**
		 * This function checks for the year cookie. If needed, it creates the cookie using DASH default parameters.
		 * @function YearCookieValues
		 */
		YearCookieValues: function (oEvent) {
			// Checks Session time out
			_oController.checkSessionTimeout();

			_oView.setModel(new sap.ui.model.json.JSONModel(
				[{
					"id": "FY",
					"yearPeriod": _oController.getResourceBundle().getText("INGTSHDYearPeriodFY")
				}, {
					"id": "CY",
					"yearPeriod": _oController.getResourceBundle().getText("INGTSHDYearPeriodCY")
				}]
			), "yearModel");

			if (sap.ui.getCore().getModel("cookies")) {
				var yearCookie;
				var Cookie = sap.ui.getCore().getModel("cookies").getData().InsightsHDPeriodCookie;
				var decodedCookie = decodeURIComponent(document.cookie);
				var ca = decodedCookie.split(' ').join('').split(';');
				for (var i = 0; i < ca.length; i++) {
					var c = ca[i];
					while (c.charAt(0) === ' ') {
						c = c.substring(1);
					}
					if (c.indexOf(Cookie) === 0) {
						yearCookie = c.substring(Cookie.length + 1, c.length);
					}
				}
				if (yearCookie !== undefined) {
					_oController.byId("comboBoxYearLabel").setSelectedKey(yearCookie);
				} else {
					_oController.byId("comboBoxYearLabel").setSelectedKey("FY");
				}
			}
		},

		/**
		 * Handles the year change event for the combobox
		 * @function onChangeYearComboBox
		 */
		onChangeYearComboBox: function (oEvent) {
			// Checks Session time out
			this.checkSessionTimeout();

			var accountNumber;
			var selectedKey = _oController.byId("comboBoxYearLabel").getSelectedKey();
			if (sap.ui.getCore().getModel("oSelectedAccount").getData().id === "0") {
				accountNumber = "0";
			} else {
				accountNumber = sap.ui.getCore().getModel("oSelectedAccount").getData().accountnumber;
			}
			var selectedKeyLocation = _oController.byId("comboBoxInsightsHD").getSelectedKey();
			var districtFlag;
			if (selectedKeyLocation.indexOf("district") === -1) {
				districtFlag = "0";
			} else {
				districtFlag = _oController.byId("comboBoxInsightsHD").getValue();
				accountNumber = "0";
			}
			_oController.setYearCookie(selectedKey);
			//Setup BDF
			if (_oController.checkDASHLayoutAccess("HD_BDF")) {
				_oView.byId("BusinessDevelopmentFunds").setVisible(true);
				_oController.getBDFData(accountNumber, districtFlag);
			} else {
				_oView.byId("BusinessDevelopmentFunds").setVisible(false);
			}
			//SetUp Lubricant, Premium Blue, AntiFreeze && Chemicals
			if (_oController.checkDASHLayoutAccess("INSIGHTSHD_GRAPH")) {
				_oView.byId("Lubricant").setVisible(true);
				_oView.byId("PremBlue").setVisible(true);
				_oView.byId("Antifreeze").setVisible(true);
				_oView.byId("Chemicals").setVisible(true);
				_oController.getLubricantData(accountNumber, districtFlag);
				_oController.getPremBlueData(accountNumber, districtFlag);
				_oController.getAntiFreezeData(accountNumber, districtFlag);
				_oController.getChemicalsData(accountNumber, districtFlag);

			} else {
				_oView.byId("Lubricant").setVisible(false);
				_oView.byId("PremBlue").setVisible(false);
				_oView.byId("Antifreeze").setVisible(false);
				_oView.byId("Chemicals").setVisible(false);
			}
		},

		/**
		 * Sets year cookie to the selected Value
		 * @function setLanguageCookie
		 */
		setYearCookie: function (cvalue) {
			var mCookie = sap.ui.getCore().getModel("cookies").getData();
			var Cookie = mCookie.InsightsHDPeriodCookie;
			var expiresText = ";expires=";
			// Allowing valvoline.com and all its subdomains
			var domainCookie = ";domain=" + mCookie.LanguageCookieDomain;
			// Set the cookie to expire in 1 year
			var expires = new Date(new Date().setFullYear(new Date().getFullYear() + 1)).toUTCString();
			document.cookie = Cookie + "=" + cvalue + expiresText + expires + domainCookie;
		},

		/**
		 * This function gets the account id and loads all the graphs.
		 * @function onChangeAccountComboBox
		 */
		onChangeAccountComboBox: function (oEvent) {
			// Checks Session time out
			this.checkSessionTimeout();

			if (oEvent !== "" && oEvent.getParameter("newValue") === "") {
				_oView.byId("comboBoxInsightsHD").setSelectedKey("0");
			}
			var selectedKey = _oController.byId("comboBoxInsightsHD").getSelectedKey();
			var districtFlag;
			var accountNumber;
			if (selectedKey.indexOf("district") === -1) {
				_oAccount = _oController.findAccountByDistrict(selectedKey, _oController);
				if (_oAccount === undefined) {
					_oView.byId("comboBoxInsightsHD").setSelectedKey("0");
					_oAccount = _oController.findAccountByDistrict("0", _oController);
				}
				if (_oAccount.id === "0") {
					_oView.byId("comboBoxInsightsHD").setSelectedKey("0");
					_oView.byId("accountAddress1Mobile").setVisible(false);
					accountNumber = "0";
				} else {
					_oView.byId("accountAddress1Mobile").setVisible(true);
					accountNumber = _oAccount.accountnumber;
				}
				sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
				_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
				districtFlag = "0";
			} else if (selectedKey.indexOf("district") !== -1) {
				_oAccount = [];
				_oAccount.id = "district-" + oEvent.getParameters().value;
				_oAccount.billingcity = _oController.getResourceBundle().getText("MAresultsTXT");
				sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
				_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
				accountNumber = "0";
				districtFlag = oEvent.getParameters().value;
			} else {
				return;
			}
			if (_oView.byId("comboBoxInsightsHD").getSelectedKey() === "0" || _oView.byId("comboBoxInsightsHD").getSelectedKey().indexOf(
					"district") !== -1) {
				_oView.byId("accountInfoContainerDesktop").setVisible(false);
			} else {
				_oView.byId("accountInfoContainerDesktop").setVisible(true);
			}
			//Setup BDF
			if (_oController.checkDASHLayoutAccess("HD_BDF")) {
				_oView.byId("BusinessDevelopmentFunds").setVisible(true);
				_oController.getBDFData(accountNumber, districtFlag);
			} else {
				_oView.byId("BusinessDevelopmentFunds").setVisible(false);
			}
			//SetUp Lubricant, Premium Blue, AntiFreeze && Chemicals
			if (_oController.checkDASHLayoutAccess("INSIGHTSHD_GRAPH")) {
				_oView.byId("Lubricant").setVisible(true);
				_oView.byId("PremBlue").setVisible(true);
				_oView.byId("Antifreeze").setVisible(true);
				_oView.byId("Chemicals").setVisible(true);
				_oController.getLubricantData(accountNumber, districtFlag);
				_oController.getPremBlueData(accountNumber, districtFlag);
				_oController.getAntiFreezeData(accountNumber, districtFlag);
				_oController.getChemicalsData(accountNumber, districtFlag);

			} else {
				_oView.byId("Lubricant").setVisible(false);
				_oView.byId("PremBlue").setVisible(false);
				_oView.byId("Antifreeze").setVisible(false);
				_oView.byId("Chemicals").setVisible(false);
			}
		},

		/**
		 * This function generates a specified Graph in the page.
		 * @function onBuildGraphs
		 *
		 */
		onBuildGraphs: function (sGraphGroup, sGraphTermination, aHideColumns) {
			var bShowTwoPreviousYear, twoPreviousYear, bShowPreviousYear, previousYear, s2PrevTotal, sPrevTotal, oModel, cardGraph, cardGraphID,
				sName, oDataset, feedValueAxis, feedCategoryAxis, i, iEmptyEntries, minValue, maxValue, aMeasures;

			oModel = new sap.ui.model.json.JSONModel(JSON.parse(JSON.stringify(_oView.getModel(sGraphGroup).getData())));
			iEmptyEntries = 0;
			if (sGraphTermination === "TY") {
				for (i = 0; i < oModel.getData().rows.length; i++) {
					if ((oModel.getData().rows[i][oModel.getData().columns[1].columnName] === undefined || oModel.getData().rows[i][oModel.getData().columns[
							1].columnName] === "") && (oModel.getData().rows[i][oModel.getData().columns[2].columnName] === undefined || oModel.getData().rows[
							i][oModel.getData().columns[2].columnName] === "")) {
						iEmptyEntries++;
					}
				}
				twoPreviousYear = oModel.getData().columns[1].columnName;
				if (aHideColumns.indexOf(1) === -1) {
					bShowTwoPreviousYear = true;
				} else {
					bShowTwoPreviousYear = false;
				}
				previousYear = oModel.getData().columns[2].columnName;
				if (aHideColumns.indexOf(2) === -1) {
					bShowPreviousYear = true;
				} else {
					bShowPreviousYear = false;
				}
				s2PrevTotal = "{" + oModel.getData().columns[1].columnName + "}";
				sPrevTotal = "{" + oModel.getData().columns[2].columnName + "}";
			} else if (sGraphTermination === "YTD") {
				for (i = 0; i < oModel.getData().rows.length; i++) {
					if ((oModel.getData().rows[i][oModel.getData().columns[3].columnName] === undefined || oModel.getData().rows[i][oModel.getData().columns[
							3].columnName] === "") && (oModel.getData().rows[i][oModel.getData().columns[4].columnName] === undefined || oModel.getData().rows[
							i][oModel.getData().columns[4].columnName] === "")) {
						iEmptyEntries++;
					}
				}
				twoPreviousYear = oModel.getData().columns[3].columnName;
				if (aHideColumns.indexOf(3) === -1) {
					bShowTwoPreviousYear = true;
				} else {
					bShowTwoPreviousYear = false;
				}
				previousYear = oModel.getData().columns[4].columnName;
				if (aHideColumns.indexOf(4) === -1) {
					bShowPreviousYear = true;
				} else {
					bShowPreviousYear = false;
				}
				s2PrevTotal = "{" + oModel.getData().columns[3].columnName + "}";
				sPrevTotal = "{" + oModel.getData().columns[4].columnName + "}";
			}
			cardGraphID = "cardGraph" + sGraphGroup + sGraphTermination;
			cardGraph = _oView.byId(cardGraphID);

			sName = "{" + oModel.getData().columns[0].columnName + "}";
			if (oModel.getData() === undefined || (oModel.getData().rows === undefined || oModel.getData().rows.length <= 1) || oModel.getData()
				.rows.length === iEmptyEntries) {
				cardGraph.setVisible(false);
				_oView.byId(cardGraphID + "-noData").setVisible(true);
			} else {
				cardGraph.setVisible(true);
				_oView.byId(cardGraphID + "-noData").setVisible(false);
				if (bShowPreviousYear && bShowTwoPreviousYear) {
					feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [twoPreviousYear, previousYear]
					});
					aMeasures = [{
						name: twoPreviousYear,
						value: s2PrevTotal
					}, {
						name: previousYear,
						value: sPrevTotal
					}];
				} else if (bShowPreviousYear) {
					feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [previousYear]
					});
					aMeasures = [{
						name: previousYear,
						value: sPrevTotal
					}];
				} else if (bShowTwoPreviousYear) {
					feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
						'uid': "valueAxis",
						'type': "Measure",
						'values': [twoPreviousYear]
					});
					aMeasures = [{
						name: twoPreviousYear,
						value: s2PrevTotal
					}];
				}
				oDataset = new sap.viz.ui5.data.FlattenedDataset({
					dimensions: [{
						name: oModel.getData().columns[0].columnName,
						value: sName
					}],

					measures: aMeasures,

					data: {
						path: "/rows/"
					}
				});
				cardGraph.removeAllFeeds();
				cardGraph.destroyDataset();

				cardGraph.setDataset(oDataset);
				cardGraph.setVizType('column');

				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': [oModel.getData().columns[0].columnName]
				});

				cardGraph.addFeed(feedValueAxis);
				cardGraph.addFeed(feedCategoryAxis);

				// Layout configuration file
				cardGraph.setVizProperties({
					plotArea: {
						colorPalette: ['#009cd9', '#e1261c']
					},
					valueAxis: {
						title: {
							visible: false
						}
					},
					categoryAxis: {
						title: {
							visible: false
						},
						label: {
							angle: 30
						}
					},
					title: {
						visible: false
					},
					legendGroup: {
						layout: {
							fontSize: 20,
							position: 'top',
							maxWidth: 100,
							height: 50,
							alignment: 'center'
						}
					},
					legend: {
						visible: true,
						title: {
							visible: false
						},
						label: {
							style: {
								fontSize: "12px",
								fontWeight: "lighter",
								fontFamily: "Roboto"
							}
						}
					}
				});
				oModel.getData().rows.pop();
				if (oModel.getData().rows.length > 0) {
					cardGraph.setModel(oModel);
					minValue = oModel.getData().min;
					maxValue = oModel.getData().max;
					_oController.setGraphRange(cardGraph, minValue, maxValue);
				} else {
					cardGraph.setVisible(false);
					_oView.byId(cardGraphID + "-noData").setVisible(true);
				}
			}
			cardGraph.setBusyIndicatorDelay(0).setBusy(false);
		},

		/**
		 * Defines the graphics range
		 * @function setGraphRange
		 * @param {string} cardGraph - Graph element
		 * @param {integer} minValue - minimum range
		 * @param {integer} maxValue - maximum range
		 */
		setGraphRange: function (cardGraph, minValue, maxValue) {
			var min = _oController.roundNumberForGraphRange(minValue);
			var max = _oController.roundNumberForGraphRange(maxValue);
			cardGraph.setVizProperties({
				yAxis: {
					scale: {
						fixedRange: true,
						minValue: min,
						maxValue: max
					}
				}
			});
		},

		convertToTreeTable: function (oData) {
			var oTreeData = {};
			var oTableData = JSON.parse(JSON.stringify(oData));
			var types = [];
			var length = oTableData.rows.length;
			var nRows = length;

			oTreeData.rows = {};
			oTreeData.rows["descendants"] = [];
			oTreeData.columns = oTableData.columns;

			for (var i = 0; i < length; i++) {
				var desc = oTableData.rows[i];
				var type = desc[oTreeData.columns[0].columnName];

				for (var j = 0; j < oTreeData.columns.length; j++) {
					desc["Value" + j] = _oController.numberFormatter(desc[oTreeData.columns[j].columnName]);
					delete desc[oTreeData.columns[j].columnName];
				}

				if (desc.Container === "Total" || desc.Container === undefined) {
					oTreeData.rows.descendants.push(desc);
					continue;
				}

				desc["Value0"] = desc.Container;
				delete desc.Container;
				if (types[type] === undefined) {
					var oParent = {};
					oParent["Value0"] = type;
					oParent["descendants"] = [desc];
					var rowsArraylength = oTreeData.rows.descendants.push(oParent);
					types[type] = rowsArraylength - 1;
					nRows++;
				} else {
					oTreeData.rows.descendants[types[type]].descendants.push(desc);
				}
			}

			oTreeData.nRows = nRows;

			return oTreeData;
		},

		/**
		 * Swapps full version to a short version of the Premium blue labels to fit in charts
		 * 
		 * @function pbShortenLabels
		 */
		pbShortenLabels: function (oData) {
			var oTableData = JSON.parse(JSON.stringify(oData));
			var length = oTableData.rows.length;

			for (var i = 0; i < length; i++) {
				var desc = oTableData.rows[i];
				desc[oTableData.columns[0].columnName] = desc.TypeSmall + " - " + desc.Container;
				oTableData.rows[i] = desc;
			}

			return oTableData;
		},

		numberFormatter: function (oVal) {
			if (isNaN(oVal) === false && oVal !== "") {
				oVal = _oController.formatValuesGroupingSeparator(Math.ceil(oVal));
			}
			return ((oVal === undefined || oVal === "") ? _oController.getResourceBundle()
				.getText("UMNA") : oVal);
		}
	});
});