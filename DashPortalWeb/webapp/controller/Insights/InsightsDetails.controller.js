sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/core/format/NumberFormat"

], function (Base) {
	"use strict";

	var _oController, _oModel, _oContact, _oAccount, _oView, _oAccountNumber, _oDistrict, _graphType;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Insights.InsightsDetails", {
		/** @module InsightsDetails */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oController = this;
			_oView = this.getView();
			_oAccount = "0";

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("insightsDetails").attachPatternMatched(this._onObjectMatched, this);
		},

		/**
		 * Set the menu as active if the app is reloaded and makes an element in the filter disabled
		 * @function onAfterRendering
		 */
		onAfterRendering: function () {
			/* Set the menu as active if the app is reloaded */
			$(".js-insights-sidenav").addClass("sidenav__item--active");

			//Remove the focus of the input
			$(':focus').blur();
		},

		/**
		 * Gets data from event bus and configures the insight details page accordingly.
		 **/
		configureDataInitial: function (channel, event, data) {
			if (channel === "navToInsightsDetails" && event === "navToInsightsDetails") {
				_oAccountNumber = data.accountNumber;
				_oDistrict = data.district;
				_oController.configureData(_oAccountNumber, _oDistrict, _graphType);
			}
		},

		/**
		 * This function runs everytime the page is opened and gets the selected insight parameters
		 * @function _onObjectMatched
		 * @param {Object} oEvent - oEvent required to get the diferent values
		 */
		_onObjectMatched: function (oEvent) {
			// Clears all error message
			this.clearServerErrorMsg(this);

			this.getView().byId("insightsDetailsTable").setBusy(true);

			// Page Layout configuration
			if (!this.checkDASHPageAuthorization("INSIGHTS_PAGE")) {
				return;
			}

			//get user info and direct account
			this.getUserInformation(this, this.toRunAfterGettingUserInfo);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			this.getAccountsByDistrict(this, "comboBoxInsights", false, this.toRunAfterGettingAccountsbyDistrict);

			// Shopping Cart Values
			this.getShoppingCartCookie(this);

			//set page title
			_graphType = oEvent.getParameter("arguments").graphType;
			this.setPageTitle(_graphType);

			var oEventBus = sap.ui.getCore().getEventBus();

			if (this.getView().getParent().getPreviousPage() !== undefined && this.getView().getParent().getCurrentPage().getViewName() ===
				"valvoline.dash.portal.DashPortalWeb.view.Insights/Insights") {
				//if the user is comming from insights page subscribe to the event bus
				oEventBus.subscribe("navToInsightsDetails", "navToInsightsDetails", this.configureDataInitial,
					this);
			}
		},

		/**
		 * This function handles the logic that requires the Accounts information to be available.
		 * @function toRunAfterGettingAccountsbyDistrict
		 * @param {object} controller - the page context
		 */
		toRunAfterGettingAccountsbyDistrict: function (controller) {
			_oContact = sap.ui.getCore().getModel("oAccountsByDistrict").getData();
			controller.onLoadAccountInfo();
			if (!(controller.getView().getParent().getPreviousPage() !== undefined && controller.getView().getParent().getCurrentPage().getViewName() ===
					"valvoline.dash.portal.DashPortalWeb.view.Insights/Insights")) {
				//if the user is not comming from insights page, get data for all accounts
				setTimeout(function () {
					_oController.getView().byId("comboBoxInsights").fireChange();
				}, 0);
			}
		},

		/**
		 * This function gets the account id and loads all the graphs.
		 * @function onChangeAccountComboBox
		 */
		onChangeAccountComboBox: function (oEvent) {
			// Checks Session time out
			this.checkSessionTimeout();

			if (oEvent.getParameter("newValue") === "") {
				_oView.byId("comboBoxInsights").setSelectedKey("0");
			}

			var selectedKey = this.byId("comboBoxInsights").getSelectedKey();
			if (selectedKey.indexOf("district") !== -1) {
				if (oEvent !== "") {
					_oDistrict = oEvent.getParameters().value;
				} else {
					_oDistrict = _oController.byId("comboBoxInsights").getValue();
				}
			} else if (selectedKey === "0") {
				_oDistrict = "0";
			} else {
				_oDistrict = "0";
			}

			_oAccount = this.findAccountByDistrict(selectedKey, _oController);

			if (_oAccount === undefined) {
				_oView.byId("comboBoxInsights").setSelectedKey("0");
				_oAccount = this.findAccountByDistrict("0", _oController);
			}
			if (_oAccount.id === "0") {
				_oView.byId("comboBoxInsights").setSelectedKey("0");
				_oView.byId("accountAddress1Mobile").setVisible(false);
			} else {
				_oView.byId("accountAddress1Mobile").setVisible(true);
			}

			if (_oAccount) {
				if (_oAccount.accountnumber === undefined) {
					_oAccountNumber = "0";
				} else {
					_oAccountNumber = _oAccount.accountnumber;
				}
			} else {
				_oAccountNumber = "0";
				_oAccount = [];
				_oAccount.id = "district-" + _oDistrict;
				_oAccount.districtName = _oDistrict;
				_oAccount.billingcity = _oController.getResourceBundle().getText("MAresultsTXT");
			}

			_oController.configureData(_oAccountNumber, _oDistrict, _graphType, "0", "0");

			sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
			if (this.getView().byId("comboBoxInsights").getSelectedKey() === "0" || this.getView().byId(
					"comboBoxInsights").getSelectedKey().indexOf("district") !== -1) {
				this.getView().byId("accountInfoContainerDesktop").setVisible(false);
				this.getView().byId("accountAddress1Mobile").setVisible(false);
			} else {
				this.getView().byId("accountInfoContainerDesktop").setVisible(true);
			}
		},

		backToInsights: function () {
			_oController.getRouter().navTo("insights");
			sap.ui.getCore().getEventBus().publish("backToInsights", "backToInsights", {
				selectedKey: this.byId("comboBoxInsights").getSelectedKey()
			});
		},

		/**
		 * This function Loads the Account information to be used in the page. Get the account model from the core.
		 * @function onLoadAccountInfo
		 */
		onLoadAccountInfo: function () {
			var selectedAccountId;

			if (sap.ui.getCore().getModel("oSelectedAccount") !== undefined) {
				if (sap.ui.getCore().getModel("oSelectedAccount").getData() !== undefined) {
					selectedAccountId = sap.ui.getCore().getModel("oSelectedAccount").getData().id;
				}
			}

			if (selectedAccountId === "0" || selectedAccountId === undefined) {
				_oAccount = [];
				_oAccountNumber = "0";
				_oDistrict = "0";
			} else if (selectedAccountId.indexOf("district") !== -1) {
				_oAccount = sap.ui.getCore().getModel("oSelectedAccount").getData();
				_oAccountNumber = "0";
				_oDistrict = sap.ui.getCore().getModel("oSelectedAccount").getData().districtName;
			} else {
				_oAccount = sap.ui.getCore().getModel("oSelectedAccount").getData();
				_oAccountNumber = _oAccount.accountnumber;
				_oDistrict = "0";
			}

			_oView = this.getView();
			// If there are Accounts, the "oSelectedAccount" model is set in the View and the the address showed is the "Showing results: All Locations"
			// else there the arent Accounts,  model is set in the View and the address is hidden and the combobox is disabled
			var oAccount;
			if (_oContact.length > 0) {
				oAccount = new sap.ui.model.json.JSONModel(_oAccount);
				sap.ui.getCore().setModel(oAccount, "oSelectedAccount");
				_oView.setModel(oAccount, "oSelectedAccount");

				// Loads all the data of the page
				this.reconfigAddressLabel(_oAccount.id);
			} else {
				this.noDataPageLayout();
				var account;
				if (this.onGetDashDevice() === "NARROW") {
					account = {
						id: "-1",
						billingcity: this.getResourceBundle().getText("MAresultsTXT")
					};
				} else {
					account = {
						id: "-1",
						billingcity: ""
					};
				}
				oAccount = new sap.ui.model.json.JSONModel(account);
				_oView.setModel(oAccount, "oSelectedAccount");
			}
		},

		/**
		 * This function resets the AddressLabel when you navigate to the page.
		 * @function reconfigAddressLabel
		 */
		reconfigAddressLabel: function (accountId) {
			if (accountId === undefined) {
				accountId = "0";
			}
			this.getView().byId("comboBoxInsights").setSelectedKey(accountId);
			_oAccount = this.findAccountByDistrict(accountId, _oController);
			if (!_oAccount) {
				_oAccount = [];
				_oAccount.id = accountId;
				_oAccount.districtName = accountId.split("-")[1];
				_oAccount.billingcity = _oController.getResourceBundle().getText("MAresultsTXT");
			}
			sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
			if (accountId === "0" || accountId.indexOf("district") !== -1) {
				this.getView().byId("accountInfoContainerDesktop").setVisible(false);
			} else {
				this.getView().byId("accountInfoContainerDesktop").setVisible(true);
			}
		},

		/**
		 * Set page title according to information to display
		 * @function setPageTitle
		 * @param {string} graphType - Info about which details are being displayed
		 */
		setPageTitle: function (graphType) {
			if (graphType === "PremiumOil") {
				this.getView().byId("title").bindProperty("text", "i18n>INGTSPremOilTXT");
			} else if (graphType === "Antifreeze") {
				this.getView().byId("title").bindProperty("text", "i18n>INGTSAntifreezeTXT");
			} else if (graphType === "WiperUnits") {
				this.getView().byId("title").bindProperty("text", "i18n>INGTSWiperTXT");
			} else if (graphType === "FilterUnits") {
				this.getView().byId("title").bindProperty("text", "i18n>INGTSFilterTXT");
			} else {
				this.getView().byId("title").bindProperty("text", "i18n>INGTSDTLTitleTXT");
			}
		},

		/**
		 * Function that handles the success case of the ajax call to configure the address label
		 * @function onConfigureAddresLabelSuccess
		 */
		onConfigureAddresLabelSuccess: function (oController, oData, oPiggyBack) {
			var model;
			if (oData === undefined) {
				model = [];
			} else {
				model = oData;
			}
			return model;
		},

		/**
		 * Function that handles the error case of the ajax call to configure the address label
		 * @function onConfigureAddresLabelError
		 */
		onConfigureAddresLabelError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, "contactAccounts");
			return null;
		},

		/**
		 * This function configure the Data for each service
		 * @function configureData
		 * @param {string} accountNumber - userAccount ID for loading the graphs
		 * @param {string} districut - district for loading the graphs
		 * @param {string} graphType - graph type name
		 */
		configureData: function (accountNumber, district, graphType) {

			var title;
			var detailsType;
			var serviceName;

			switch (graphType) {
			case "PremiumOil":
				title = "i18n>insightsPagePMO";
				detailsType = "OilShortLabel";
				serviceName = "engineOilPremiumDetails?";
				break;

			case "Antifreeze":
				title = "i18n>INGTSSubtitle4TXT";
				detailsType = "MonthQuantity";
				serviceName = "antifreezeByMonth?";
				break;

			case "WiperUnits":
				title = "i18n>INGTSSubtitle5TXT";
				detailsType = "MonthQuantity";
				serviceName = "wipersByMonth?";
				break;

			case "FilterUnits":
				title = "i18n>INGTSSubtitle6TXT";
				detailsType = "FilterUnits";
				serviceName = "filtersDetails?";
				break;
			default:
				break;
			}

			this.onLoadTableColumns(detailsType);
			this.onLoadTableRows(serviceName, accountNumber, district);
			this.getView().byId("insightsDetailsSubTitle").bindProperty("text", title);
		},

		/**
		 * This function loads the Columns Configuration for each graph.
		 * @function onLoadTableColumns
		 * @param {string} detailsType - the name of the configuration pretended
		 */
		onLoadTableColumns: function (detailsType) {

			var oModel = new sap.ui.model.json.JSONModel();
			var columnsLayout;

			switch (detailsType) {
			case "OilLongLabel":
				columnsLayout = {
					"columns": [{
						"name": "IPdateTable"
					}, {
						"name": "IPconventionalTable"
					}, {
						"name": "IPhighMileageTable"
					}, {
						"name": "IPsyntethicTable"
					}]
				};
				break;
			case "OilShortLabel":
				columnsLayout = {
					"columns": [{
						"name": "IPdateTable"
					}, {
						"name": "IPmaxlife"
					}, {
						"name": "IPdurablend"
					}]
				};
				break;

			case "MonthQuantity":
				columnsLayout = {
					"columns": [{
						"name": "IPdateTable"
					}, {
						"name": "IPquantityTable"
					}]
				};
				break;

			case "FilterUnits":
				columnsLayout = {
					"columns": [{
						"name": "IPdateTable"
					}, {
						"name": "IPAirFiltersTable"
					}, {
						"name": "IPOilFiltersTable"
					}, {
						"name": "IPCabinAirFiltersTable"
					}]
				};
				break;
			default:
				break;
			}

			oModel.setData(columnsLayout);
			this.getView().setModel(oModel, "insightsTableColumns");
		},

		/**
		 * Function that handles the success case of the ajax call to load the row configuration for each graph
		 * @function onLoadTableRowsSuccess
		 */
		onLoadTableRowsSuccess: function (oController, oData, oPiggyBack) {
			var insightsDetailsTable = oController.getView().byId("insightsDetailsTable");
			insightsDetailsTable.setBusy(false);

			if (oPiggyBack.serviceName === "antifreezeByMonth?" || oPiggyBack.serviceName === "wipersByMonth?" || oPiggyBack.serviceName ===
				"engineOilPremiumDetails?") {
				_oModel.setProperty("/rows", oData);
			} else {
				_oModel.setData(oData);
			}

			oController.getView().setModel(_oModel, "insightsTable");
		},

		/**
		 * Function that handles the error case of the ajax call to load the row configuration for each graph
		 * @function onLoadTableRowsError
		 */
		onLoadTableRowsError: function (oController, oError, oPiggyBack) {
			var insightsDetailsTable = oController.getView().byId("insightsDetailsTable");
			insightsDetailsTable.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRReports"));
		},

		/**
		 * This function loads the Rows Configuration for each graph
		 * @function onLoadTableRows
		 * @param {string} serviceName
		 * @param {string} AccountNumber
		 */
		onLoadTableRows: function (serviceName, accountNumber, district) {
			var controller = this;
			var insightsDetailsTable = this.getView().byId("insightsDetailsTable");
			insightsDetailsTable.setBusyIndicatorDelay(0);
			insightsDetailsTable.setBusy(true);

			var oPiggyBack = {
				"serviceName": serviceName
			};

			var xsURL = "/insights/" + serviceName;

			if (accountNumber === "0" && district === "0") {
				//dont change URL
			} else if (accountNumber !== "0" && district === "0") {
				xsURL += "ShipTo=" + accountNumber;
			} else if (accountNumber === "0" && district !== "0") {
				xsURL += "customerreportinggroup=" + district;
			} else {
				//dont change URL
			}

			_oModel = new sap.ui.model.json.JSONModel();
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadTableRowsSuccess, controller.onLoadTableRowsError,
				undefined, oPiggyBack);
		},

		/**
		 * Format values
		 * @function formatValuesDetails
		 * @param {string} sValue - value to be formatted
		 * @returns formatted value contatenated with value measure
		 */
		formatValuesDetails: function (sValue) {
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			var oLocale = new sap.ui.core.Locale(selectedLanguage);

			var oFormatOptions = {
				style: "standard",
				decimals: 0,
				shortDecimals: 0
			};
			var oFloatFormat = sap.ui.core.format.NumberFormat.getFloatInstance(oFormatOptions, oLocale);
			var sValueFormatted = oFloatFormat.format(sValue);
			var valueMeasure = _oModel.getData().rows[0].valueMeasure;
			return sValueFormatted + " " + valueMeasure;
		},

		/**
		 * Receives an i18n tag and gets the respective text.
		 **/
		formatColumnName: function (value) {
			if (value) {
				value = _oController.getResourceBundle().getText(value);
			} else {
				value = "";
			}
			return value;
		}

	});
});