sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/core/routing/History"

], function (Base, History) {
	"use strict";

	var _oView, _oAccount, _oContact, _oAccountNumber, _oDistrict, _oController, _graphType, _oAccountId,
		_oSettingsData, _oRouter, _pLoadPremiumData;
	var _motorOilTotalGallons = 0;
	var _premiumMotorOilPrcnt = 0;
	var _vpsChemicalUnt = 0;
	var _referenceLineOilPrcntNumb = 0;
	var _motorOilTotalGallonsLY = 0;
	var _premiumMotorOilPrcntLY = 0;
	var _oSettingsDataLY;
	var _vpsChemicalUntLY = 0;
	var _referenceLineOilPrcntNumbLY = 0;
	var _premiumDetails;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Insights.InsightsGraphicDetails", {
		/** @module Insights */
		/**
		 * This function is called when the fist rendered. Set the Account model, and builds all the diferent Graphs displayed in the view.
		 * @function onInit
		 */
		onInit: function () {
			_oView = this.getView();
			_oController = this;
			Base.prototype.onInit.call(this);

			var controller = this;
			controller.onBuildEngineOilGraph();
			controller.onBuildVPSGraph();

			_oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			_oRouter.getRoute("insightsGraphicDetails").attachPatternMatched(this._onObjectMatched, this);

			// Configure the reports links
			this.monthLinksConfiguration();
			_referenceLineOilPrcntNumb = 0;
		},

		/**
		 * This function is called everytime the page is loaded, and refresh the data and the page opening.
		 * @function _onObjectMatched
		 * @param {Object} oEvent - oEvent required to get the diferent values
		 */
		_onObjectMatched: function (oEvent) {
			// Clears all error message
			this.clearServerErrorMsg(this);

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "insights"
			});

			// Page Layout Configuration
			if (!this.checkDASHPageAuthorization("INSIGHTS_PAGE")) {
				return;
			}
			_graphType = oEvent.getParameter("arguments").graphType;
			this.graphToDisplay(_graphType);

			//get user info and direct account
			this.getUserInformation(this, this.toRunAfterGettingUserInfo);

			this.headerIconAccess();

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			_oController.getView().byId("labelContainerVPSGoals").setBusyIndicatorDelay(0).setBusy(true);

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			var oEventBus = sap.ui.getCore().getEventBus();
			if (this.getView().getParent().getPreviousPage() !== undefined && this.getView().getParent().getPreviousPage().getViewName() ===
				"valvoline.dash.portal.DashPortalWeb.view.Insights/Insights") {
				if (_graphType === "EngineOil" && oEventBus._mChannels.motorOilGoals === undefined) {
					oEventBus.subscribe("motorOilGoals", "motorOilGoals", this.onReceiveGoals, this);
				} else if (_graphType === "VPS" && oEventBus._mChannels.vpsUnitsGoals === undefined) {
					oEventBus.subscribe("vpsUnitsGoals", "vpsUnitsGoals", this.onReceiveVPSGoals, this);
				}
			} else {
				_oController.getGoalData();
			}

			// Sets the model oAccountsByDistrict
			var controller = this;
			this.getAccountsByDistrict(controller, "comboBoxInsightsGraphDetails", false, this.reconfigAddressLabel);
			$(':focus').blur();
		},

		/**
		 * This function handles the logic that requires the user information to be available.
		 * @function toRunAfterGettingUserInfo
		 * @param {object} controller - the page context
		 */
		toRunAfterGettingUserInfo: function (controller) {
			_oContact = sap.ui.getCore().getModel("oAccounts").getData();
			// Load account info and calls all the graphs data
			_oController.onLoadAccountInfo();
		},

		/**
		 * Set the menu as active if the app is reloaded and makes an element in the filter disabled
		 * @function onAfterRendering
		 */
		onAfterRendering: function () {
			if (!(this.getView().getParent().getPreviousPage() !== undefined && this.getView().getParent().getCurrentPage().getViewName() ===
					"valvoline.dash.portal.DashPortalWeb.view.Insights/InsightsGraphicDetails")) {
				_oController.getView().byId("comboBoxInsightsGraphDetails").fireChange();
			}
			/* Set the menu as active if the app is reloaded */
			$(".js-insights-sidenav").addClass("sidenav__item--active");

			//Remove the focus of the input
			$(':focus').blur();
		},

		/**
		 * Handler for the VPS Chemical Inits EventBus.
		 * @function onReceiveVPSGoals
		 */
		onReceiveVPSGoals: function (channel, event, data) {
			_vpsChemicalUnt = data.vpsChemicalUnt;
			_oSettingsData = data.goalsData;
			_oDistrict = data.district;
			if (_oController.deCommaFy(_vpsChemicalUnt) > 0) {
				_oController.getView().byId("vpsGoalSettingLableContainer").setVisible(true);
				var curYear = new Date().getFullYear();
				_oController.getView().byId("vpsYearGoal").setText(curYear + " " + _oController.getResourceBundle().getText(
					"INGTSCurrentGoalLabelTXT") + " " + _oController.commaFy(_oController.deCommaFy(_vpsChemicalUnt)));
				var today = new Date();
				var bYear = new Date(today.getFullYear(), 0, 1);
				var one_day = 1000 * 60 * 60 * 24;
				var daysSinceB = Math.ceil((today.getTime() - bYear.getTime()) / (one_day));
				_oController.getView().byId("vpsYTDGoal").setText(_oController.getResourceBundle().getText("INGTSYTDGoalLabelTXT") + " " + (
					_oController.commaFy(Math.round(_oController.deCommaFy(_vpsChemicalUnt) * (daysSinceB / 365)))));
				_oController.getView().byId("labelContainerVPSGoals").setBusy(false);
			} else {
				_oController.getView().byId("vpsGoalSettingLableContainer").setVisible(false);
			}
		},

		/**
		 * Goal Lable Values of the current year for a specific account
		 * @function setgoalsForAccCY
		 */
		setgoalsForAccCY: function (accFieldToMatch, aDescendants) {
			var i;
			if (_oDistrict === "0" && aDescendants !== undefined) {

				if (aDescendants.accountId === accFieldToMatch) {
					if (aDescendants.storeTotal !== undefined) {
						_motorOilTotalGallons = _oController.commaFy(_oController.deCommaFy(aDescendants.storeTotal));
					} else {
						_motorOilTotalGallons = 0;
					}
					if (aDescendants.storeTotalVPS !== undefined) {
						_vpsChemicalUnt = _oController.commaFy(_oController.deCommaFy(aDescendants.storeTotalVPS));
					} else {
						_vpsChemicalUnt = 0;
					}
					if (aDescendants.storePrcnt !== undefined) {
						_premiumMotorOilPrcnt = _oController.commaFy(_oController.deCommaFy(aDescendants.storePrcnt));
						_referenceLineOilPrcntNumb = 1;
					} else {
						_premiumMotorOilPrcnt = 0;
					}
				} else if (aDescendants.descendants !== undefined && aDescendants.descendants.length > 0) {
					for (i = 0; i < aDescendants.descendants.length; i++) {
						_oController.setgoalsForAccCY(accFieldToMatch, aDescendants.descendants[i]);
					}
				}
			} else if (aDescendants !== undefined) {
				if (aDescendants.district === accFieldToMatch) {
					if (aDescendants.storeTotal !== undefined) {
						_motorOilTotalGallons = _oController.commaFy(_oController.deCommaFy(_motorOilTotalGallons) + _oController.deCommaFy(aDescendants
							.storeTotal));
					} else if (_motorOilTotalGallons === undefined) {
						_motorOilTotalGallons = 0;
					}
					if (aDescendants.storeTotalVPS !== undefined) {
						_vpsChemicalUnt = _oController.commaFy(_oController.deCommaFy(_vpsChemicalUnt) + _oController.deCommaFy(aDescendants.storeTotalVPS));
					} else if (_vpsChemicalUnt === undefined) {
						_vpsChemicalUnt = 0;
					}
					if (aDescendants.storePrcnt !== undefined) {
						_premiumMotorOilPrcnt = _oController.commaFy(_oController.deCommaFy(_premiumMotorOilPrcnt) + _oController.deCommaFy(aDescendants
							.storePrcnt));
						_referenceLineOilPrcntNumb = _referenceLineOilPrcntNumb + 1;
					} else if (_premiumMotorOilPrcnt === undefined) {
						_premiumMotorOilPrcnt = 0;
					}
					if (aDescendants.descendants !== undefined && aDescendants.descendants.length > 0) {
						for (i = 0; i < aDescendants.descendants.length; i++) {
							_oController.setgoalsForAccCY(accFieldToMatch, aDescendants.descendants[i]);
						}
					}
				} else if (aDescendants.descendants !== undefined && aDescendants.descendants.length > 0) {
					for (i = 0; i < aDescendants.descendants.length; i++) {
						_oController.setgoalsForAccCY(accFieldToMatch, aDescendants.descendants[i]);
					}
				}
			}
		},

		/**
		 * Handler for the Motor Oil EventBus.
		 * @function onReceiveGoals
		 */
		onReceiveGoals: function (channel, event, data) {
			_motorOilTotalGallons = data.motorOilTotalGallons;
			_premiumMotorOilPrcnt = data.premiumMotorOilPrcnt;
			_oSettingsData = data.goalsData;
			_oDistrict = data.district;
			_oAccountNumber = data.accountNumber;
			_oController.loadPremiumOilData();
		},

		backToInsights: function () {
			_oRouter.navTo("insights");
			sap.ui.getCore().getEventBus().publish("backToInsights", "backToInsights", {
				selectedKey: this.byId("comboBoxInsightsGraphDetails").getSelectedKey()
			});
		},

		/**
		 * This function configures the graph that is displayed and page title.
		 * @function graphToDisplay
		 * @param {string} graphType - Name of the graph to display
		 */
		graphToDisplay: function (graphType) {
			if (graphType === "EngineOil") {
				this.getView().byId("vizFrameEngineOilContainer").setVisible(true);
				this.getView().byId("vizFrameVPSContainer").setVisible(false);
				this.getView().byId("reportsHeroCard").setVisible(true);
				this.getView().byId("title").setText(this.getResourceBundle().getText("INGTSLubTitleTXT"));
			} else if (graphType === "VPS") {
				this.getView().byId("vizFrameEngineOilContainer").setVisible(false);
				this.getView().byId("vizFrameVPSContainer").setVisible(true);
				this.getView().byId("reportsHeroCard").setVisible(false);
				this.getView().byId("title").setText(this.getResourceBundle().getText("INGTSNonLubTitleTXT"));
			} else {
				this.getView().byId("vizFrameEngineOilContainer").setVisible(false);
				this.getView().byId("vizFrameVPSContainer").setVisible(false);
				this.getView().byId("reportsHeroCard").setVisible(false);
				this.getView().byId("title").setText(this.getResourceBundle().getText("INGTSTitleTXT"));
			}

		},

		/**
		 * This function configures the Reports' Data.
		 * @function reportsDataSetup
		 */
		reportsDataSetup: function () {
			var currentYear = new Date().getFullYear();
			var firstYear = new Date();
			firstYear.setMonth(firstYear.getMonth() - 24);
			firstYear = firstYear.getFullYear();
			var yearArray = [];

			for (var year = firstYear; year <= currentYear; year++) {
				yearArray.push({
					"year": year
				});
			}

			var yearReports = new sap.ui.model.json.JSONModel(yearArray);
			this.getView().setModel(yearReports, "yearReports");
			this.onDisableMonthsReports(currentYear);
			this.getView().byId("reportsYearField").setSelectedKey(currentYear);
		},

		/**
		 * This function configures the noData Page Layout.
		 * @function noDataPageLayout
		 */
		noDataPageLayout: function () {
			_oView.byId("comboBoxInsightsGraphDetails").setEnabled(false);
			_oView.byId("headerContainerMotorOil").setVisible(false);
			_oView.byId("headerContainerVPS").setVisible(false);

			for (var i = 1; i <= 12; i++) {
				var monthID = this.byId("month" + i);
				monthID.setEnabled(false);
				monthID.addStyleClass("noHoverFooter");
			}
		},

		/**
		 * This function resets the AddressLabel when you navigate to the page.
		 * @function reconfigAddressLabel
		 */
		reconfigAddressLabel: function () {
			var accountId = _oAccount.id;
			if (accountId === undefined) {
				accountId = "0";
			}
			_oView.byId("comboBoxInsightsGraphDetails").setSelectedKey(accountId);
			_oAccount = _oController.findAccountByDistrict(accountId, _oController);
			if (!_oAccount) {
				_oAccount = [];
				_oAccount.id = accountId;
				_oAccount.districtName = accountId.split("-")[1];
				_oAccount.billingcity = _oController.getResourceBundle().getText("MAresultsTXT");
			}
			sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
			if (accountId === "0" || accountId.indexOf("district") !== -1) {
				_oView.byId("accountInfoContainerDesktop").setVisible(false);
			} else {
				_oView.byId("accountInfoContainerDesktop").setVisible(true);
			}
		},

		/**
		 * This function loads the data for all the charts on the page.
		 * @function onLoadDataLargeGraph
		 * @param {string} AccountNumber - logged account number
		 */
		onLoadDataLargeGraph: function (AccountNumber, districtIsSelected) {
			// Load Data for Engine Oil Graph and VPS Graph
			if (_graphType === "EngineOil") {
				this.onLoadDataEngineOilGraph(AccountNumber, districtIsSelected);
			} else if (_graphType === "VPS") {
				this.onLoadDataVPSGraph(AccountNumber, districtIsSelected);
			} else {
				return;
			}
		},

		/**
		 * Function that handles the success case of the ajax call to load premium oil data
		 * @function loadPremiumOilDataSuccess
		 */
		loadPremiumOilDataSuccess: function (oController, oData, oPiggyBack) {
			_premiumDetails = oData;
			oPiggyBack.resolve();
		},

		/**
		 * Function that handles the error case of the ajax call to load premium oil data
		 * @function loadPremiumOilDataError
		 */
		loadPremiumOilDataError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText);
		},

		/**
		 * This function calls the service to retrieve premium oil data
		 * @function loadPremiumOilData
		 */
		loadPremiumOilData: function () {
			var xsURL = "/insights/engineOilPremiumLastYear?";
			if (_oAccountNumber === "0" && _oDistrict === "0") {
				//dont change URL
			} else if (_oAccountNumber !== "0" && _oDistrict === "0") {
				xsURL += "ShipTo=" + _oAccountNumber;
			} else if (_oAccountNumber === "0" && _oDistrict !== "0") {
				xsURL += "customerreportinggroup=" + _oDistrict;
			} else {
				//dont change URL
			}
			_pLoadPremiumData = new Promise(function (resolve, reject) {
				var oPiggyBack = {
					"resolve": resolve
				};
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.loadPremiumOilDataSuccess, _oController.loadPremiumOilDataError,
					null, oPiggyBack);
			});
		},

		/**
		 * This function Loads the Account information to be used in the page. Get the account model from the core.
		 * @function onLoadAccountInfo
		 */
		onLoadAccountInfo: function () {
			var selectedAccountId;

			if (sap.ui.getCore().getModel("oSelectedAccount") !== undefined) {
				if (sap.ui.getCore().getModel("oSelectedAccount").getData() !== undefined) {
					selectedAccountId = sap.ui.getCore().getModel("oSelectedAccount").getData().id;
				}
			}

			if (selectedAccountId === "0" || selectedAccountId === undefined) {
				_oAccount = [];
				_oAccountNumber = "0";
				_oDistrict = "0";
			} else if (selectedAccountId.indexOf("district") !== -1) {
				_oAccount = sap.ui.getCore().getModel("oSelectedAccount").getData();
				_oAccountNumber = "0";
				_oDistrict = sap.ui.getCore().getModel("oSelectedAccount").getData().districtName;
			} else {
				_oAccount = sap.ui.getCore().getModel("oSelectedAccount").getData();
				_oAccountNumber = _oAccount.accountnumber;
				_oDistrict = "0";
			}

			_oView = this.getView();
			// If there are Accounts, the "oSelectedAccount" model is set in the View and the the address showed is the "Showing results: All Locations"
			// else there the arent Accounts,  model is set in the View and the address is hidden and the combobox is disabled
			var oAccount;
			if (_oContact.accounts.length > 0) {
				oAccount = new sap.ui.model.json.JSONModel(_oAccount);
				sap.ui.getCore().setModel(oAccount, "oSelectedAccount");
				_oView.setModel(oAccount, "oSelectedAccount");

				// Loads all the data of the page
				this.onLoadDataLargeGraph(_oAccountNumber, _oDistrict);
				this.reportsDataSetup();
				this.headerDateGenerator();
			} else {
				this.noDataPageLayout();
				var account;
				if (this.onGetDashDevice() === "NARROW") {
					account = {
						id: "-1",
						billingcity: this.getResourceBundle().getText("MAresultsTXT")
					};
				} else {
					account = {
						id: "-1",
						billingcity: ""
					};
				}
				oAccount = new sap.ui.model.json.JSONModel(account);
				_oView.setModel(oAccount, "oSelectedAccount");
			}
		},

		/**
		 * This function gets the account id and loads all the graphs.
		 * @function onChangeAccountComboBox
		 */
		onChangeAccountComboBox: function (oEvent) {
			// Checks Session time out
			this.checkSessionTimeout();

			if (oEvent.getParameter("newValue") === undefined || oEvent.getParameter("newValue") === "") {
				_oView.byId("comboBoxInsightsGraphDetails").setSelectedKey("0");
			}
			_motorOilTotalGallons = 0;
			_premiumMotorOilPrcnt = 0;
			_vpsChemicalUnt = 0;
			_referenceLineOilPrcntNumb = 0;
			_motorOilTotalGallonsLY = 0;
			_premiumMotorOilPrcntLY = 0;
			_vpsChemicalUntLY = 0;
			_referenceLineOilPrcntNumbLY = 0;
			var selectedKey = this.byId("comboBoxInsightsGraphDetails").getSelectedKey();
			var i;

			_oAccount = this.findAccountByDistrict(selectedKey, _oController);

			if (_oAccount === undefined) {
				_oView.byId("comboBoxInsightsGraphDetails").setSelectedKey("0");
				_oAccount = this.findAccountByDistrict("0", _oController);
			}
			if (_oAccount.id === "0") {
				_oView.byId("comboBoxInsightsGraphDetails").setSelectedKey("0");
				_oView.byId("accountAddress1Mobile").setVisible(false);
			} else {
				_oView.byId("accountAddress1Mobile").setVisible(true);
			}
			if (_oAccount) {
				if (_oAccount.id !== undefined) {
					_oAccountId = _oAccount.id;
				}
				if (_oAccount.accountnumber === undefined) {
					_oAccountNumber = "0";
				} else {
					_oAccountNumber = _oAccount.accountnumber;
				}

			} else {
				_oAccountNumber = "0";
				_oAccount = [];
				_oAccount.id = "district-" + _oDistrict;
				_oAccount.districtName = _oDistrict;
				_oAccount.billingcity = _oController.getResourceBundle().getText("MAresultsTXT");
			}

			if (selectedKey.indexOf("district") !== -1) {
				_oDistrict = oEvent.getSource().getProperty("value");
				_oController.loadPremiumOilData();
				for (i = 0; i < _oSettingsData.length; i++) {
					_oController.setgoalsForAccCY(_oDistrict, _oSettingsData[i]);
				}
			} else if (selectedKey === "0") {
				_oDistrict = "0";
				_oController.loadPremiumOilData();
				if (_oSettingsData !== undefined && _oSettingsData.length !== 0) {
					_oController.calcDescendantsTotal(_oSettingsData);
				}
			} else {
				_oDistrict = "0";
				_oController.loadPremiumOilData();
				if (_oSettingsData !== undefined && _oSettingsData.length !== 0) {
					for (i = 0; i < _oSettingsData.length; i++) {
						_oController.setgoalsForAccCY(selectedKey, _oSettingsData[i]);
					}
				}
			}

			if (_graphType === "VPS" && _oController.deCommaFy(_vpsChemicalUnt) > 0) {
				_oController.getView().byId("vpsGoalSettingLableContainer").setVisible(true);
				_oController.getView().byId("labelContainerVPSGoals").setBusyIndicatorDelay(0).setBusy(true);
				var curYear = new Date().getFullYear();
				_oController.getView().byId("vpsYearGoal").setText(curYear + " " + _oController.getResourceBundle().getText(
					"INGTSCurrentGoalLabelTXT") + " " + _oController.commaFy(_oController.deCommaFy(_vpsChemicalUnt)));
				var today = new Date();
				var bYear = new Date(today.getFullYear(), 0, 1);
				var one_day = 1000 * 60 * 60 * 24;
				var daysSinceB = Math.ceil((today.getTime() - bYear.getTime()) / (one_day));
				_oController.getView().byId("vpsYTDGoal").setText(_oController.getResourceBundle().getText("INGTSYTDGoalLabelTXT") + " " + (
					_oController.commaFy(Math.round(_oController.deCommaFy(_vpsChemicalUnt) * (daysSinceB / 365)))));
				_oController.getView().byId("labelContainerVPSGoals").setBusy(false);
			} else {
				_oController.getView().byId("vpsGoalSettingLableContainer").setVisible(false);
				_oController.getView().byId("MotorOilGoalsTable").setVisible(false);
			}

			this.onLoadDataLargeGraph(_oAccountNumber, _oDistrict);
			this.reportsDataSetup();
			if (sap.ui.getCore().getModel("oSelectedAccount") === undefined) {
				sap.ui.getCore().setModel(new sap.ui.model.json.JSONModel(), "oSelectedAccount");
			}
			if (_oView.getModel("oSelectedAccount") === undefined) {
				_oView.setModel(new sap.ui.model.json.JSONModel(), "oSelectedAccount");
			}
			sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
			if (this.getView().byId("comboBoxInsightsGraphDetails").getSelectedKey() === "0" || this.getView().byId(
					"comboBoxInsightsGraphDetails").getSelectedKey().indexOf("district") !== -1) {
				this.getView().byId("accountInfoContainerDesktop").setVisible(false);
				this.getView().byId("accountAddress1Mobile").setVisible(false);
			} else {
				this.getView().byId("accountInfoContainerDesktop").setVisible(true);
			}
			//Remove the focus of the input
			$(':focus').blur();
		},

		/**
		 * ReferenceLines Value Sum
		 * @function calcDescendantsTotal
		 */
		calcDescendantsTotal: function (aDescendants) {
			for (var i = 0; i < aDescendants.length; i++) {
				if (aDescendants[i].storeTotal !== undefined && aDescendants[i].storeTotal !== null) {
					_motorOilTotalGallons = _oController.commaFy(_oController.deCommaFy(_motorOilTotalGallons) + _oController.deCommaFy(aDescendants[
						i].storeTotal));
				}
				if (aDescendants[i].storeTotalVPS !== undefined && aDescendants[i].storeTotalVPS !== null) {
					_vpsChemicalUnt = _oController.commaFy(_oController.deCommaFy(_vpsChemicalUnt) + _oController.deCommaFy(aDescendants[i].storeTotalVPS));
				}
				if (aDescendants[i].storePrcnt !== undefined && aDescendants[i].storePrcnt !== null) {
					_premiumMotorOilPrcnt = _oController.commaFy(_oController.deCommaFy(_premiumMotorOilPrcnt) + _oController.deCommaFy(aDescendants[
						i].storePrcnt));
					_referenceLineOilPrcntNumb = _referenceLineOilPrcntNumb + 1;
				}
				if (aDescendants[i].descendants !== undefined && aDescendants[i].descendants.length !== 0) {
					_oController.calcDescendantsTotal(aDescendants[i].descendants);
				}
			}
		},

		/**
		 * Goals Value Sum
		 * @function calcDescendantsTotalLY
		 */
		calcDescendantsTotalLY: function (aDescendants) {
			for (var i = 0; i < aDescendants.length; i++) {
				if (aDescendants[i].storeTotal !== undefined && aDescendants[i].storeTotal !== null) {
					_motorOilTotalGallonsLY = _oController.commaFy(_oController.deCommaFy(_motorOilTotalGallonsLY) + _oController.deCommaFy(
						aDescendants[i].storeTotal));
				}
				if (aDescendants[i].storeTotalVPS !== undefined && aDescendants[i].storeTotalVPS !== null) {
					_vpsChemicalUntLY = _oController.commaFy(_oController.deCommaFy(_vpsChemicalUntLY) + _oController.deCommaFy(aDescendants[i].storeTotalVPS));
				}
				if (aDescendants[i].storePrcnt !== undefined && aDescendants[i].storePrcnt !== null) {
					_premiumMotorOilPrcntLY = _oController.commaFy(_oController.deCommaFy(_premiumMotorOilPrcntLY) + _oController.deCommaFy(
						aDescendants[i].storePrcnt));
					_referenceLineOilPrcntNumbLY = _referenceLineOilPrcntNumbLY + 1;
				}
				if (aDescendants[i].descendants !== undefined && aDescendants[i].descendants.length !== 0) {
					_oController.calcDescendantsTotalLY(aDescendants[i].descendants);
				}
			}
		},

		/**
		 * This function generates the Engine Oil Graph Graph in the page.
		 * @function onBuildEngineOilGraph
		 */
		onBuildEngineOilGraph: function () {

			var conventional = this.getResourceBundle().getText("conventionalLB");
			var highMileage = this.getResourceBundle().getText("highMileageLB");
			var synthetic = this.getResourceBundle().getText("syntheticLB");

			var vizFrameEngineOil = this.getView().byId("vizFrameEngineOil");
			var oVizFrameEngineOilData = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Month',
					value: "{CalendarMonthYear}"
				}],

				measures: [{
					name: conventional,
					value: '{GallonsConventional}'
				}, {
					name: highMileage,
					value: '{GallonsHighMileage}'
				}, {
					name: synthetic,
					value: '{GallonsSynthetic}'
				}],

				data: {
					path: "/rows"
				}
			});
			vizFrameEngineOil.setDataset(oVizFrameEngineOilData);
			vizFrameEngineOil.setVizType('line');

			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [conventional, highMileage, synthetic]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Month"]
				});

			vizFrameEngineOil.addFeed(feedValueAxis);
			vizFrameEngineOil.addFeed(feedCategoryAxis);

			// Layout configuration file
			vizFrameEngineOil.setVizProperties({

				plotArea: {
					colorPalette: ["black", "#e1261c", "#009cd9"]
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						position: 'top',
						maxWidth: 100,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "16px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * Function that handles the success case of the ajax call to load the data for the Engine oil
		 * graph
		 * @function onLoadDataEngineOilGraphSuccess
		 */
		onLoadDataEngineOilGraphSuccess: function (oController, oData, oPiggyBack) {
			var oModel = new sap.ui.model.json.JSONModel();
			var oVizFrameEngineOil = oController.getView().byId("vizFrameEngineOil");
			//Remove the focus of the input
			$(':focus').blur();
			oModel.setData(oData);
			oVizFrameEngineOil.setBusy(false);
			oVizFrameEngineOil.setModel(oModel);
			oController.loadMotorOilGoalsTable(oData);
			oController.onLoadDataEngineOilGraphHeader(oPiggyBack.AccountNumber, oPiggyBack.districtIsSelected);
		},

		/**
		 * Function that handles the error case of the ajax call to load the data for the Engine oil
		 * graph
		 * @function onLoadDataEngineOilGraphError
		 */
		onLoadDataEngineOilGraphError: function (oController, oError, oPiggyBack) {
			var oVizFrameEngineOil = oController.getView().byId("vizFrameEngineOil");
			oVizFrameEngineOil.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText,
				oController.getResourceBundle().getText("ERREngineOil"));
		},

		/**
		 * This function loads the Data for the Engine Oil Graph
		 * @function onLoadDataEngineOilGraph
		 * @param {string} AccountNumber - the account number required to load the graph
		 */
		onLoadDataEngineOilGraph: function (AccountNumber, districtIsSelected) {
			var oVizFrameEngineOil = this.getView().byId("vizFrameEngineOil");
			oVizFrameEngineOil.setBusyIndicatorDelay(0);
			oVizFrameEngineOil.setBusy(true);

			var controller = this;
			var xsURL = "/insights/engineOil";
			var oPiggyBack = {
				"AccountNumber": AccountNumber,
				"districtIsSelected": districtIsSelected
			};

			if (AccountNumber === "0" && districtIsSelected !== "0") {
				//When a district is selected
				xsURL += "?customerreportinggroup=" + districtIsSelected;
			} else if (AccountNumber !== "0" && districtIsSelected === "0") {
				//When an accounts is selected
				xsURL += "?ShipTo=" + AccountNumber;
			} else {
				//If All Locations is selected
				//Don't alter the URL
			}
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadDataEngineOilGraphSuccess, controller.onLoadDataEngineOilGraphError,
				undefined, oPiggyBack);
		},

		/**
		 * Function that handles the success case of the ajax call tload motor oil goals data
		 * @function onLoadMotorOilGoalsTableSuccess
		 */
		onLoadMotorOilGoalsTableSuccess: function (oController, oData, oPiggyBack) {
			var oModelGoalsRows = new sap.ui.model.json.JSONModel();
			var oModelGoalsColumns = new sap.ui.model.json.JSONModel();
			var row = [];
			var columns = [];
			var lubeGallonAttainment = [];
			var lubeGallonGoal = [];
			var percentageGoal = [];
			var percentageAttainment = [];
			_motorOilTotalGallonsLY = 0;
			_vpsChemicalUntLY = 0;
			_premiumMotorOilPrcntLY = 0;
			_referenceLineOilPrcntNumbLY = 0;
			lubeGallonAttainment.push("Lube gallon attainment");
			lubeGallonGoal.push("Lube gallon goal");
			percentageGoal.push("Percentage goal");
			percentageAttainment.push("Percentage attainment");
			columns.push("");
			_oSettingsDataLY = oData;
			var j, prcntAttained, totalPremium;
			if (_oDistrict !== "0") {
				//district is selected
				for (j = 0; j < _oSettingsDataLY.length; j++) {
					oController.setGoalsForAccLY(_oDistrict, _oSettingsDataLY[j]);
				}
			} else if (_oAccountNumber === "0") {
				if (_oSettingsDataLY !== undefined && _oSettingsDataLY.length !== 0) {
					oController.calcDescendantsTotalLY(_oSettingsDataLY);
				}
			} else {
				for (j = 0; j < _oSettingsDataLY.length; j++) {
					oController.setGoalsForAccLY(_oAccountId, _oSettingsDataLY[j]);
				}
			}
			_pLoadPremiumData.then(function () {
				for (var i = 1; i <= oPiggyBack.data.rows.length; i++) {
					lubeGallonAttainment.push(oController.commaFy(parseInt(oPiggyBack.data.rows[i - 1].GallonsConventional) + parseInt(oPiggyBack.data
							.rows[i - 1].GallonsHighMileage) +
						parseInt(
							oPiggyBack.data.rows[i - 1].GallonsSynthetic)));
					//Check selected distric and calculate goals accordingly

					if (i - 1 <= (12 - (new Date().getMonth() + 1))) {
						//use last year
						if (_motorOilTotalGallonsLY === undefined || _motorOilTotalGallonsLY === null) {
							_motorOilTotalGallonsLY = 0;
						}
						lubeGallonGoal.push(oController.commaFy(Math.round(oController.deCommaFy(_motorOilTotalGallonsLY) / 12)));

						if (_premiumMotorOilPrcntLY === undefined || _premiumMotorOilPrcntLY === null) {
							_premiumMotorOilPrcntLY = 0;
						}
						if (_referenceLineOilPrcntNumbLY === undefined || _referenceLineOilPrcntNumbLY === null || _referenceLineOilPrcntNumbLY ===
							0) {
							percentageGoal.push(0);
						} else {
							percentageGoal.push(Math.round(_premiumMotorOilPrcntLY / _referenceLineOilPrcntNumbLY));
						}
					} else {
						//use current year
						if (_motorOilTotalGallons === undefined || _motorOilTotalGallons === "") {
							_motorOilTotalGallons = 0;
						}
						if (_premiumMotorOilPrcnt === undefined || _premiumMotorOilPrcnt === "") {
							_premiumMotorOilPrcnt = 0;
						}
						lubeGallonGoal.push(oController.commaFy(Math.round(oController.deCommaFy(_motorOilTotalGallons) / 12)));
						if (_referenceLineOilPrcntNumb === undefined || _referenceLineOilPrcntNumb === null || _referenceLineOilPrcntNumb === 0) {
							percentageGoal.push(0);
						} else {
							percentageGoal.push(Math.round(_premiumMotorOilPrcnt / _referenceLineOilPrcntNumb));
						}
					}
					if (lubeGallonAttainment[i] !== undefined && lubeGallonAttainment[i] !== null && oController.deCommaFy(lubeGallonAttainment[
							i]) > 0) {
						totalPremium = parseInt(_premiumDetails[i - 1].valueColumn2) + parseInt(_premiumDetails[i - 1].valueColumn3);
						prcntAttained = totalPremium * 100 / oController.deCommaFy(lubeGallonAttainment[i]);
						percentageAttainment.push(oController.commaFy(Math.round(prcntAttained)));
					} else {
						percentageAttainment.push(0);
					}
					columns.push(oPiggyBack.data.rows[i - 1].CalendarMonthYear);
				}
				row.push(lubeGallonGoal);
				row.push(lubeGallonAttainment);
				row.push(percentageGoal);
				row.push(percentageAttainment);
				oModelGoalsRows.setData(row);
				oModelGoalsColumns.setData(columns);
				oController.setModel(oModelGoalsRows, "MotorOilGoalsRows");
				oController.setModel(oModelGoalsColumns, "MotorOilGoalsColumns");
				if (oController.deCommaFy(row[0][1]) > 0 || oController.deCommaFy(row[0][13]) > 0 || oController.deCommaFy(row[2][1]) > 0 ||
					oController.deCommaFy(row[2][13]) > 0) {
					oController.getView().byId("MotorOilGoalsTable").setVisible(true);
					oController.buildMotorOilGoalsTable();
				}
			});

		},

		/**
		 * Function that handles the error case of the ajax call tload motor oil goals data
		 * @function onLoadMotorOilGoalsTableError
		 */
		onLoadMotorOilGoalsTableError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText);
		},

		/**
		 * This function loads the Data for the Goals Motor Oil Table
		 * @function loadMotorOilGoalsTable
		 * @param {object} data - an object with the current year data
		 */
		loadMotorOilGoalsTable: function (data) {
			_oController.getView().byId("MotorOilGoalsTable").setVisible(false);
			_oController.getView().byId("MotorOilGoalsTable").setBusyIndicatorDelay(0).setBusy(true);

			//Get last year Values
			var lastYear = new Date().getFullYear() - 1;
			var oPiggyBack = {
				"data": data
			};
			var xsURL = "/goalsetting/accountyear?year=" + lastYear;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onLoadMotorOilGoalsTableSuccess, _oController.onLoadMotorOilGoalsTableError,
				undefined, oPiggyBack);
		},

		/**
		 * Function that handles the success case of the ajax call that gets the goal data
		 * @function onGetGoalDataSuccess
		 */
		onGetGoalDataSuccess: function (oController, oData, oPiggyBack) {
			_oSettingsData = oData;
			_referenceLineOilPrcntNumb = 0;

			if (oData !== undefined && oData.length !== 0) {
				oController.calcDescendantsTotal(oData);
			}
		},

		/**
		 * Function that handles the error case of the ajax call that gets the goal data
		 * @function onGetGoalDataError
		 */
		onGetGoalDataError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText);
		},

		/**
		 * Retrieves Goal data from the service
		 * @function getGoalData
		 */
		getGoalData: function () {
			_motorOilTotalGallons = 0;
			_vpsChemicalUnt = 0;
			_premiumMotorOilPrcnt = 0;
			var currentYear = new Date().getFullYear();
			var xsURL = "/goalsetting/accountyear?year=" + currentYear;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, false, xsURL, "GET", _oController.onGetGoalDataSuccess, _oController.onGetGoalDataError);
		},

		/**
		 * ReferenceLines Values for a specific account
		 * @function setGoalsForAccLY
		 */
		setGoalsForAccLY: function (accFieldToMatch, aDescendants) {
			var i;
			if (_oDistrict === "0" && aDescendants !== undefined) {

				if (aDescendants.accountId === accFieldToMatch) {
					if (aDescendants.storeTotal !== undefined && aDescendants.storeTotal !== null) {
						_motorOilTotalGallonsLY = _oController.deCommaFy(aDescendants.storeTotal);
					}
					if (aDescendants.storeTotalVPS !== undefined && aDescendants.storeTotalVPS !== null) {
						_vpsChemicalUntLY = _oController.deCommaFy(aDescendants.storeTotalVPS);
					}
					if (aDescendants.storePrcnt !== undefined && aDescendants.storePrcnt !== null) {
						_premiumMotorOilPrcntLY = _oController.deCommaFy(aDescendants.storePrcnt);
						_referenceLineOilPrcntNumbLY = 1;
					}
				} else if (aDescendants.descendants !== undefined && aDescendants.descendants.length > 0) {
					for (i = 0; i < aDescendants.descendants.length; i++) {
						_oController.setGoalsForAccLY(accFieldToMatch, aDescendants.descendants[i]);
					}
				}
			} else if (aDescendants !== undefined) {
				if (aDescendants.district === accFieldToMatch) {
					if (aDescendants.storeTotal !== undefined && aDescendants.storeTotal !== null) {
						_motorOilTotalGallonsLY = _motorOilTotalGallonsLY + _oController.deCommaFy(aDescendants.storeTotal);
					}
					if (aDescendants.storeTotalVPS !== undefined && aDescendants.storeTotalVPS !== null) {
						_vpsChemicalUntLY = _vpsChemicalUntLY + _oController.deCommaFy(aDescendants.storeTotalVPS);
					}
					if (aDescendants.storePrcnt !== undefined && aDescendants.storePrcnt !== null) {
						_premiumMotorOilPrcntLY = _premiumMotorOilPrcntLY + _oController.deCommaFy(aDescendants.storePrcnt);
						_referenceLineOilPrcntNumbLY = _referenceLineOilPrcntNumbLY + 1;
					}
					if (aDescendants.descendants !== undefined && aDescendants.descendants.length > 0) {
						for (i = 0; i < aDescendants.descendants.length; i++) {
							_oController.setGoalsForAccLY(accFieldToMatch, aDescendants.descendants[i]);
						}
					}
				} else if (aDescendants.descendants !== undefined && aDescendants.descendants.length > 0) {
					for (i = 0; i < aDescendants.descendants.length; i++) {
						_oController.setGoalsForAccLY(accFieldToMatch, aDescendants.descendants[i]);
					}
				}
			}
		},

		/**
		 * Motor Oil Goals Table build
		 * @ buildMotorOilGoalsTable
		 */
		buildMotorOilGoalsTable: function () {
			// Get all ROWS and COLUMNS information
			var oModelRows = this.getModel("MotorOilGoalsRows");
			var oModelColumns = this.getModel("MotorOilGoalsColumns");
			var oDataColumns = oModelColumns.getData();

			// TABLE COLUMN AND ROWS CONSTRUCTOR
			var oTable = this.getView().byId("MotorOilGoalsTable");
			oTable.destroyColumns();
			if (oTable.getFixedColumnCount() !== 1) {
				oTable.setFixedColumnCount(1);
			}
			oTable.setModel(oModelRows).bindRows("/");
			for (var c = 0; c < oDataColumns.length; c++) {
				for (var row = 0; row < oModelRows.getData().length; row++) {
					oModelRows.setProperty("/" + row + "/valueColumn" + c, oModelRows.getProperty("/" + row)[c]);
				}
				// Build 1st Fixed Label Component
				if (c === 0) {
					// Build Fixed Label Component
					var Label_Fixed = new sap.m.Label({
						text: oDataColumns[c]
					}).addStyleClass("leftLableGoalTable");

					// Build Label Component
					var Label_2_Fixed = new sap.m.Text({
						width: "100%",
						textAlign: "Right",
						text: "{valueColumn" + c + "}"
					}).addStyleClass("bodyCopy2");

					// Add Column to Table
					oTable.addColumn(new sap.ui.table.Column({
						resizable: false,
						width: "7rem",
						label: (Label_Fixed),
						template: (Label_2_Fixed)
					}));
				} else {
					// Other Columns
					// Builds the Label component
					var Label = new sap.m.Label({
						text: oDataColumns[c]
					}).addStyleClass("infoBold");

					// Builds the text Field component
					var text;
					text = new sap.m.Text({
						id: "valueColumn" + c,
						width: "100%",
						textAlign: "Right",
						text: "{valueColumn" + c + "}"
					}).addStyleClass("VRIInputField");

					// Add Column to Table
					oTable.addColumn(new sap.ui.table.Column({
						hAlign: "Right",
						resizable: false,
						width: "7rem",
						label: (Label),
						template: (text)
					}));
				}
			}

			// Set the Last text of the table selected
			oTable.addEventDelegate({
				"onAfterRendering": function () {
					var oTableCells = oTable.getRows()[0].getAggregation("cells");
					var row2 = oTable.getRows()[1].getAggregation("cells");
					var row3 = oTable.getRows()[2].getAggregation("cells");
					var row4 = oTable.getRows()[3].getAggregation("cells");
					for (var i = 1; i < oTableCells.length; i++) {
						if (parseInt(row2[i].getText().toString().split(',').join('')) >= parseInt(oTableCells[i].getText().toString().split(',').join(
								''))) {
							row2[i].addStyleClass("insightGoalPassedContainerColorSuccess");
						}
						if (parseInt(row4[i].getText().toString().split(',').join('')) >= parseInt(row3[i].getText().toString().split(',').join(''))) {
							row4[i].addStyleClass("insightGoalPassedContainerColorSuccess");
						}
					}
					sap.ui.getCore().byId($('[id*="MotorOilGoalsTable"]')[1].id)._getScrollExtension().getHorizontalScrollbar().scrollLeft = 999;
					oTable.setBusy(false);
				}
			}, this);
		},

		/**
		 * Function that handles the success case of the ajax call to load the data for the Engine oil
		 * graph header 
		 * @function onLoadDataEngineOilGraphHeaderSuccess
		 */
		onLoadDataEngineOilGraphHeaderSuccess: function (oController, oData, oPiggyBack) {
			var oModelHeader = new sap.ui.model.json.JSONModel();
			var headerContainer = oController.getView().byId("headerContainerMotorOil");
			headerContainer.setBusy(false);
			oModelHeader.setData(oData);
			_oView.setModel(oModelHeader, "engineOilHeader");
		},

		/**
		 * Function that handles the error case of the ajax call to load the data for the Engine oil
		 * graph header 
		 * @function onLoadDataEngineOilGraphHeaderError
		 */
		onLoadDataEngineOilGraphHeaderError: function (oController, oError, oPiggyBack) {
			var headerContainer = oController.getView().byId("headerContainerMotorOil");
			headerContainer.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERREngineOilHeader"));
		},

		/**
		 * This function loads the Data for the Engine Oil Graph Header
		 * @function onLoadDataEngineOilGraphHeader
		 * @param {string} AccountNumber - the account number required to load the graph
		 */
		onLoadDataEngineOilGraphHeader: function (AccountNumber, districtIsSelected) {
			//Remove the focus of the input
			$(':focus').blur();
			var controller = this;
			var headerContainer = this.getView().byId("headerContainerMotorOil");
			var oPiggyBack = {
				"AccountNumber": AccountNumber,
				"districtIsSelected": districtIsSelected
			};
			headerContainer.setBusyIndicatorDelay(0);
			headerContainer.setBusy(true);

			var xsURL = "/insights/engineOilHeader";

			if (AccountNumber === "0" && districtIsSelected !== "0") {
				//When a district is selected
				xsURL += "?customerreportinggroup=" + districtIsSelected;
			} else if (AccountNumber !== "0" && districtIsSelected === "0") {
				//When an accounts is selected
				xsURL += "?ShipTo=" + AccountNumber;
			} else {
				//If All Locations is selected
				//Don't alter the URL
			}
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadDataEngineOilGraphHeaderSuccess, controller.onLoadDataEngineOilGraphHeaderError,
				undefined, oPiggyBack);
		},

		/**
		 * This function generates the VPS Graph Graph in the page
		 * @function onBuildVPSGraph
		 */
		onBuildVPSGraph: function () {
			var priorYTD = this.getResourceBundle().getText("priorYTD");
			var currentYTD = this.getResourceBundle().getText("currentYTD");

			var oVizFrameVPS = this.getView().byId("vizFrameVPS");
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Product',
					value: "{GP_Category2}"
				}],

				measures: [{
					name: priorYTD,
					value: "{GallonsPriorYear}"
				}, {
					name: currentYTD,
					value: "{GallonsCurrentYear}"
				}],

				data: {
					path: "/"
				}
			});
			oVizFrameVPS.setDataset(oDataset);
			oVizFrameVPS.setVizType('column');

			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [priorYTD, currentYTD]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Product"]
				});
			oVizFrameVPS.addFeed(feedValueAxis);
			oVizFrameVPS.addFeed(feedCategoryAxis);

			// Layout configuration file
			oVizFrameVPS.setVizProperties({
				plotArea: {
					colorPalette: ["#009cd9", "#e1261c"]
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						fontSize: 3,
						position: 'top',
						maxWidth: 100,
						alignment: 'center'
					}
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "16px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * Function that handles the success case of the ajax call that loads the VPS graph data
		 * @function onLoadDataVPSGraphSuccess
		 */
		onLoadDataVPSGraphSuccess: function (oController, oData, oPiggyBack) {
			var oModelVPS = new sap.ui.model.json.JSONModel();
			var oVizFrameVPS = oController.getView().byId("vizFrameVPS");
			//Remove the focus of the input
			$(':focus').blur();
			oModelVPS.setData(oData);
			oVizFrameVPS.setBusy(false);
			oVizFrameVPS.setModel(oModelVPS);
			oController.onLoadDataVPSGraphHeader(oPiggyBack.AccountNumber, oPiggyBack.districtIsSelected);
		},

		/**
		 * Function that handles the error case of the ajax call that loads the VPS graph data
		 * @function onLoadDataVPSGraphError
		 */
		onLoadDataVPSGraphError: function (oController, oError, oPiggyBack) {
			var oVizFrameVPS = oController.getView().byId("vizFrameVPS");
			oVizFrameVPS.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRVps"));
		},

		/**
		 * This function loads the Data for the VPS Graph.
		 * @function onLoadDataVPSGraph
		 * @param {string} AccountNumber - the account number required to load the graph
		 */
		onLoadDataVPSGraph: function (AccountNumber, districtIsSelected) {
			var oVizFrameVPS = this.getView().byId("vizFrameVPS");
			oVizFrameVPS.setBusyIndicatorDelay(0);
			oVizFrameVPS.setBusy(true);

			var controller = this;
			var xsURL = "/insights/vps";
			var oPiggyBack = {
				"AccountNumber": AccountNumber,
				"districtIsSelected": districtIsSelected
			};

			if (AccountNumber === "0" && districtIsSelected !== "0") {
				//When a district is selected
				xsURL += "?customerreportinggroup=" + districtIsSelected;
			} else if (AccountNumber !== "0" && districtIsSelected === "0") {
				//When an accounts is selected
				xsURL += "?ShipTo=" + AccountNumber;
			} else {
				//If All Locations is selected
				//Don't alter the URL
			}
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadDataVPSGraphSuccess, controller.onLoadDataVPSGraphError,
				undefined, oPiggyBack);
		},

		/**
		 * Function that handles the success case of the ajax call to load the Data for the VPS Graph 
		 * Header.
		 * @function onLoadDataVPSGraphHeaderSuccess
		 */
		onLoadDataVPSGraphHeaderSuccess: function (oController, oData, oPiggyBack) {
			var oModelHeader = new sap.ui.model.json.JSONModel();
			var headerContainer = oController.getView().byId("headerContainerVPS");
			headerContainer.setBusy(false);
			oModelHeader.setData(oData);
			_oView.setModel(oModelHeader, "vpsHeader");
		},

		/**
		 * Function that handles the error case of the ajax call to load the Data for the VPS Graph 
		 * Header.
		 * @function onLoadDataVPSGraphHeaderError
		 */
		onLoadDataVPSGraphHeaderError: function (oController, oError, oPiggyBack) {
			var headerContainer = oController.getView().byId("headerContainerVPS");
			headerContainer.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRVpsHeader"));
		},

		/**
		 * This function loads the Data for the VPS Graph Header.
		 * @function onLoadDataVPSGraphHeader
		 * @param {string} AccountNumber - the account number required to load the graph
		 */
		onLoadDataVPSGraphHeader: function (AccountNumber, districtIsSelected) {
			//Remove the focus of the input
			$(':focus').blur();
			var headerContainer = this.getView().byId("headerContainerVPS");
			headerContainer.setBusyIndicatorDelay(0);
			headerContainer.setBusy(true);

			var controller = this;
			var xsURL = "/insights/vpsHeader";
			var oPiggyBack = {
				"AccountNumber": AccountNumber,
				"districtIsSelected": districtIsSelected
			};

			if (AccountNumber === "0" && districtIsSelected !== "0") {
				//When a district is selected
				xsURL += "?customerreportinggroup=" + districtIsSelected;
			} else if (AccountNumber !== "0" && districtIsSelected === "0") {
				//When an accounts is selected
				xsURL += "?ShipTo=" + AccountNumber;
			} else {
				//If All Locations is selected
				//Don't alter the URL
			}
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadDataVPSGraphHeaderSuccess, controller.onLoadDataVPSGraphHeaderError,
				undefined, oPiggyBack);

			//Hide keyboard when slecting box
			$(':focus').blur();
		},

		/**
		 * This function opens a new tab for the insights details.
		 * @function onNavToNewTab
		 * @param {string} accountNumber - Number of the account
		 * @param {string} district - district name
		 * @param {string} graphType - Type of the graph
		 * @param {string} year - Year
		 * @param {string} month - Month
		 */
		onNavToNewTab: function (account, district, year, month) {
			// Checks Session time out
			this.checkSessionTimeout();

			district = district.replace("/", "__");

			var sUrl = this.getRouter().getURL("insightsMonthlyReports", {
				account: account,
				district: district,
				year: year,
				month: month,
				language: sap.ui.getCore().getConfiguration().getLanguage()
			});

			// Opens a new tab SAP UI5 function
			sap.m.URLHelper.redirect("#" + sUrl, true);
		},

		/**
		 * This function select the Year of the Report and disable the months before the selected year and current date.
		 * @function onChangeYearReports
		 */
		onChangeYearReports: function () {
			var selectedYear = parseInt(this.byId("reportsYearField").getSelectedKey());
			this.onDisableMonthsReports(selectedYear);

			//GTM
			_oController.GTMDataLayer('yearselection',
				'Insights',
				'DropDown_Year_Selection',
				selectedYear
			);

		},

		/**
		 * This function disables the months before current date.
		 * @function onDisableMonthsReports
		 * @param {number} selectedYear - current year
		 */
		onDisableMonthsReports: function (selectedYear) {
			var currentDate = new Date();
			var currentMonth = currentDate.getMonth() + 1;
			var currentYear = currentDate.getFullYear();
			var firstDate = new Date();
			firstDate.setMonth(currentDate.getMonth() - 24);
			var firstYear = firstDate.getFullYear();
			var firstMonth = firstDate.getMonth();
			var monthID;
			var i;

			if (currentYear === selectedYear) {
				for (i = currentMonth; i <= 12; i++) {
					monthID = this.byId("month" + i);
					monthID.setEnabled(false);
					monthID.addStyleClass("noHoverFooter");
				}
				for (i = 1; i <= currentMonth; i++) {
					monthID = this.byId("month" + i);
					monthID.setEnabled(true);
					monthID.addStyleClass("noHoverFooter");
				}
			} else if (selectedYear === firstYear) {
				for (i = 1; i <= firstMonth; i++) {
					monthID = this.byId("month" + i);
					monthID.setEnabled(false);
					monthID.removeStyleClass("noHoverFooter");
				}
				for (i = firstMonth + 1; i <= 12; i++) {
					monthID = this.byId("month" + i);
					monthID.setEnabled(true);
					monthID.removeStyleClass("noHoverFooter");
				}
			} else {
				for (i = 1; i <= 12; i++) {
					monthID = this.byId("month" + i);
					monthID.setEnabled(true);
					monthID.removeStyleClass("noHoverFooter");
				}
			}
		},

		/**
		 * This function attaches a press event to the month links
		 * @function monthLinksConfiguration
		 */
		monthLinksConfiguration: function () {
			for (var i = 1; i <= 12; i++) {
				this.getView().byId("month" + i).attachPress("click", _oController.onClickMonthLinks);
			}
		},

		/**
		 * Fired when someone presses the month links
		 * @function onClickMonthLinks
		 **/
		onClickMonthLinks: function (oEvent) {
			var monthLinkID = oEvent.getParameters().id.split("--");
			var monthSelected = monthLinkID[1].split("h");
			var monthSelectedNumber = monthSelected[1];

			if (monthSelectedNumber.length === 1) {
				monthSelectedNumber = "0" + monthSelectedNumber;
			}

			var yearSelected = _oController.byId("reportsYearField").getSelectedKey();

			_oController.onNavToNewTab(_oAccountNumber, _oDistrict, yearSelected, monthSelectedNumber);

			//GTM
			_oController.GTMDataLayer('quaterlyrprtclick',
				'Insights',
				'Quarter Month -click',
				oEvent.getSource().getText()
			);
		},
		/**
		 * This functions generates the header date.
		 * @function headerDateGenerator
		 */
		headerDateGenerator: function () {
			var currentDate = new Date();
			var currentDay = currentDate.getDate();
			var currentMonth = currentDate.getMonth();
			var currentMonthName = [
				_oController.getResourceBundle().getText("Mjan"),
				_oController.getResourceBundle().getText("Mfeb"),
				_oController.getResourceBundle().getText("Mmar"),
				_oController.getResourceBundle().getText("Mapr"),
				_oController.getResourceBundle().getText("Mmay"),
				_oController.getResourceBundle().getText("Mjun"),
				_oController.getResourceBundle().getText("Mjul"),
				_oController.getResourceBundle().getText("Maug"),
				_oController.getResourceBundle().getText("Msep"),
				_oController.getResourceBundle().getText("Moct"),
				_oController.getResourceBundle().getText("Mnov"),
				_oController.getResourceBundle().getText("Mdec")
			];

			var date = new sap.ui.model.json.JSONModel();
			date.setProperty("/date", _oController.getResourceBundle().getText(
				"1Mjan") + " - " + currentDay + " " + currentMonthName[currentMonth]);
			this.getView().setModel(date, "headerDate");
		},

		/**
		 * Formats values.
		 * @function formatValues
		 * @param {string} sValue - value to be formatted
		 * @returns Formatted values
		 */
		formatValues: function (sValue) {
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			var oLocale = new sap.ui.core.Locale(selectedLanguage);

			var oFormatOptions = {
				style: "standard",
				decimals: 0,
				shortDecimals: 0
			};
			var oFloatFormat = sap.ui.core.format.NumberFormat.getFloatInstance(oFormatOptions, oLocale);
			var sValueFormatted = oFloatFormat.format(sValue);
			return sValueFormatted;
		}
	});
});