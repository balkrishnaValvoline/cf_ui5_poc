sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function (Base) {
	"use strict";

	var _oView, _oAccount, _oContact, _oAccountNumber, _oController, _oDistrict, _oRouter, _pReferenceLineData;
	var _2BarGraphLayout;
	var _referenceLineTotalGallons = 0;
	var _referenceLineVPS = 0;
	var _referenceLineOilPrcnt = 0;
	var _referenceLineOilPrcntNumb = 0;
	var _oSettingsData;

	const VALV_RED = "#E1261D"; //rgb(225,38,28)
	const VALV_DARK_BLUE = "#041372"; //rgb(4,19,114)
	const VALV_LIGHT_BLUE = "#009cd9"; //rgb(0,156,217)
	const VALV_LIGHT_GREY = "#979797"; //rgb(151,151,151)

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Insights.Insights", {

		/**
		 * In this file you can configure the Graph Layout.
		 * It containes all the VizFrames propreties
		 */
		graphLayoutConfiguration: function () {
			_2BarGraphLayout = {
				plotArea: {
					dataPointStyle: {
						rules: [{
							dataContext: {
								Year: "Current YTD"
							},
							properties: {
								color: VALV_RED
							}
						}],
						others: {
							properties: {
								color: VALV_LIGHT_BLUE
							}
						}
					},
					colorPalette: [VALV_LIGHT_GREY, VALV_RED, VALV_LIGHT_BLUE]
				},
				valueAxis: {
					title: {
						visible: false
					}
				},
				categoryAxis: {
					title: {
						visible: false
					},
					dataLabe: {
						visible: false
					}
				},
				title: {
					visible: false
				},
				legendGroup: {
					layout: {
						position: 'top',
						maxWidth: 100,
						height: 50,
						alignment: "start"
					}
				},
				legend: {
					visible: false,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "14px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			};
		},

		/** @module Insights */
		/**
		 * This function is called when the fist rendered. Set the Account models, and builds all the diferent Graphs displayed in the view.
		 * @function onInit
		 */
		onInit: function () {
			_oView = this.getView();
			_oController = this;
			Base.prototype.onInit.call(this);
			_oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			_oRouter.getRoute("insights").attachPatternMatched(this._onObjectMatched, this);
			_oAccount = "0";
			_oAccountNumber = "0";
			this.graphLayoutConfiguration();
		},

		/**
		 * Set the menu as active if the app is reloaded and makes an element in the filter disabled
		 * @function onAfterRendering
		 */
		onAfterRendering: function () {
			/* Set the menu as active if the app is reloaded */
			$(".js-insights-sidenav").addClass("sidenav__item--active");

			//Remove the focus of the input
			$(':focus').blur();

			//see if URL is available
			var sURL = _oView.getModel("HowToVideos").getData().HowToInsightsURL;
			if (sURL !== undefined) {
				this.getView().byId("howToVideosButton").setVisible(true);
				this.getView().byId("howToVideo").setVisible(true);
			}
		},

		/**
		 * Sets the selected item on the combobox, based on the item that was previously selected on the graphicDetails
		 * @function onBackToInsights
		 */
		onBackToInsights: function (channel, event, data) {
			var selectedKey = data.selectedKey;
			_oController.getView().byId("comboBoxInsights").setSelectedKey(selectedKey);
			_oController.onChangeAccountComboBox("");
		},

		/**
		 * This function is called everytime the page is loaded, and refresh the data and the page opening.
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {

			// Clears all error message
			_oController.clearServerErrorMsg(_oController);
			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "insights"
			});

			// Page Layout Configuration
			if (!_oController.checkDASHPageAuthorization("INSIGHTS_PAGE")) {
				return;
			}

			_referenceLineTotalGallons = 0;
			_referenceLineVPS = 0;
			_referenceLineOilPrcnt = 0;
			_referenceLineOilPrcntNumb = 0;
			_oSettingsData = null;
			_pReferenceLineData = null;
			_oController.headerIconAccess();

			//Getting the Reference Line Data
			_oController.getReferenceLineData();

			//get user info and direct account
			_oController.getUserInformation(_oController, _oController.toRunAfterGettingUserInfo);

			// Check Language and set selected Key in combo box
			_oController.onChangeLanguage();

			_oController.getAccountsByDistrict(_oController, "comboBoxInsights", false, _oController.reconfigAddressLabel);

			var oEventBus = sap.ui.getCore().getEventBus();
			if (oEventBus._mChannels.motorOilGoals === undefined) {
				oEventBus.subscribe("backToInsights", "backToInsights", _oController.onBackToInsights, _oController);
			}

			// Shopping Cart Values
			_oController.getShoppingCartCookie(_oController);

			_oController.byId("cardContainerInsights").setViewContext(_oController);
			_oController.populateCardContainer("Insights", "3", "All");

			//Remove the focus of the input
			$(':focus').blur();
		},

		/**
		 * This function handles the logic that requires the user information to be available.
		 * @function toRunAfterGettingUserInfo
		 * @param {object} controller - the page context
		 */
		toRunAfterGettingUserInfo: function (controller) {
			_oContact = sap.ui.getCore().getModel("oAccounts").getData();

			if (_oContact.accounts.length > 0) {
				var oAccount = new sap.ui.model.json.JSONModel(_oContact.accounts[0]);
				sap.ui.getCore().setModel(oAccount, "oSelectedAccount");
				controller.getView().setModel(oAccount, "oSelectedAccount");
			} else {
				controller.noDataPageLayout();
			}
		},

		/**
		 * Function that handles the success case of the ajax call to get the reference line data
		 * @function onGetReferenceLineDataSuccess
		 */
		onGetReferenceLineDataSuccess: function (oController, oData, oPiggyBack) {
			_oSettingsData = oData;
			_referenceLineTotalGallons = 0;
			_referenceLineVPS = 0;
			_referenceLineOilPrcnt = 0;
			_referenceLineOilPrcntNumb = 0;
			oPiggyBack.resolve();

			if (oData !== undefined && oData.length !== 0) {
				oController.calcDescendantsTotal(oData);
			}
		},

		/**
		 * Function that handles the error case of the ajax call to get the reference line data
		 * @function onGetReferenceLineDataError
		 */
		onGetReferenceLineDataError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText);
		},

		/**
		 * Retrieves reference data from the service
		 * @function getReferenceLineData
		 */
		getReferenceLineData: function () {
			var currentYear = new Date().getFullYear();
			var xsURL = "/goalsetting/accountyear?year=" + currentYear;
			_pReferenceLineData = new Promise(function (resolve, reject) {
				var oPiggyBack = {
					"resolve": resolve
				};
				//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
				_oController.handleAjaxJSONCall(_oController, true, xsURL, "GET", _oController.onGetReferenceLineDataSuccess, _oController.onGetReferenceLineDataError,
					null, oPiggyBack);
			});
		},

		/**
		 * ReferenceLines Value Sum
		 * @function calcDescendantsTotal
		 */
		calcDescendantsTotal: function (aDescendants) {
			for (var i = 0; i < aDescendants.length; i++) {
				if (aDescendants[i].storeTotal !== undefined && aDescendants[i].storeTotal !== null) {
					_referenceLineTotalGallons = _referenceLineTotalGallons + _oController.deCommaFy(aDescendants[i].storeTotal);
				}
				if (aDescendants[i].storeTotalVPS !== undefined && aDescendants[i].storeTotalVPS !== null) {
					_referenceLineVPS = _referenceLineVPS + _oController.deCommaFy(aDescendants[i].storeTotalVPS);
				}
				if (aDescendants[i].storePrcnt !== undefined && aDescendants[i].storePrcnt !== null) {
					_referenceLineOilPrcnt = _referenceLineOilPrcnt + _oController.deCommaFy(aDescendants[i].storePrcnt);
					_referenceLineOilPrcntNumb = _referenceLineOilPrcntNumb + 1;
				}
				if (aDescendants[i].descendants !== undefined && aDescendants[i].descendants.length !== 0) {
					_oController.calcDescendantsTotal(aDescendants[i].descendants);
				}
			}
		},

		/**
		 * ReferenceLines Values for a specific account
		 * @function setReferenceLineForAcc
		 */
		setReferenceLineForAcc: function (accFieldToMatch, aDescendants) {
			var i;
			if (_oDistrict === "0" && aDescendants !== undefined) {

				if (aDescendants.accountId === accFieldToMatch) {
					if (aDescendants.storeTotal !== undefined && aDescendants.storeTotal !== null) {
						_referenceLineTotalGallons = _oController.deCommaFy(aDescendants.storeTotal);
					}
					if (aDescendants.storeTotalVPS !== undefined && aDescendants.storeTotalVPS !== null) {
						_referenceLineVPS = _oController.deCommaFy(aDescendants.storeTotalVPS);
					}
					if (aDescendants.storePrcnt !== undefined && aDescendants.storePrcnt !== null) {
						_referenceLineOilPrcnt = _oController.deCommaFy(aDescendants.storePrcnt);
						_referenceLineOilPrcntNumb = 1;
					}
				} else if (aDescendants.descendants !== undefined && aDescendants.descendants.length > 0) {
					for (i = 0; i < aDescendants.descendants.length; i++) {
						_oController.setReferenceLineForAcc(accFieldToMatch, aDescendants.descendants[i]);
					}
				}
			} else if (aDescendants !== undefined) {
				if (aDescendants.district === accFieldToMatch) {
					if (aDescendants.storeTotal !== undefined && aDescendants.storeTotal !== null) {
						_referenceLineTotalGallons = _referenceLineTotalGallons + _oController.deCommaFy(aDescendants.storeTotal);
					}
					if (aDescendants.storeTotalVPS !== undefined && aDescendants.storeTotalVPS !== null) {
						_referenceLineVPS = _referenceLineVPS + _oController.deCommaFy(aDescendants.storeTotalVPS);
					}
					if (aDescendants.storePrcnt !== undefined && aDescendants.storePrcnt !== null) {
						_referenceLineOilPrcnt = _referenceLineOilPrcnt + _oController.deCommaFy(aDescendants.storePrcnt);
						_referenceLineOilPrcntNumb = _referenceLineOilPrcntNumb + 1;
					}
					if (aDescendants.descendants !== undefined && aDescendants.descendants.length > 0) {
						for (i = 0; i < aDescendants.descendants.length; i++) {
							_oController.setReferenceLineForAcc(accFieldToMatch, aDescendants.descendants[i]);
						}
					}
				} else if (aDescendants.descendants !== undefined && aDescendants.descendants.length > 0) {
					for (i = 0; i < aDescendants.descendants.length; i++) {
						_oController.setReferenceLineForAcc(accFieldToMatch, aDescendants.descendants[i]);
					}
				}
			}
		},

		/**
		 * This function configures the noData Page Layout.
		 * @function noDataPageLayout
		 */
		noDataPageLayout: function () {
			_oView.byId("comboBoxInsights").setVisible(false);
			_oView.byId("cardGraphPremiumOilButton").setEnabled(false);
			_oView.byId("cardGraphAntiFreezeButton").setEnabled(false);
			_oView.byId("cardGraphWiperUnitsButton").setEnabled(false);
			_oView.byId("cardGraphFilterUnitsButton").setEnabled(false);
			_oView.byId("cardGraphEngineOilButton").setEnabled(false);
			_oView.byId("cardGraphVPSButton").setEnabled(false);
			_oView.byId("headerEngineOil").removeStyleClass("insightsCardTitle__header");
			_oView.byId("headerVPS").removeStyleClass("insightsCardTitle__header");
		},

		/**
		 * This function resets the AddressLabel when you navigate to the page.
		 * @function reconfigAddressLabel
		 */
		reconfigAddressLabel: function () {
			_oController.getView().byId("comboBoxInsights").setSelectedKey(0);
			_oAccount = _oController.findAccountByDistrict("0", _oController);
			sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oController.getView().byId("accountInfoContainerDesktop").setVisible(false);
			_oController.onChangeAccountComboBox("");
		},

		/**
		 * This function loads the data for all the charts on the page.
		 * @function onLoadDataConfiguration
		 * @param {string} AccountNumber - logged account number
		 * @param {string} districtIsSelected - selected district (if applicable)
		 */
		onLoadDataConfiguration: function (AccountNumber, districtIsSelected) {
			// Another Test
			var controller = this;
			controller.onLoadDataEngineOilGraphHeader(AccountNumber, districtIsSelected);
			controller.onLoadDataVPSGraphHeader(AccountNumber, districtIsSelected);
			controller.onLoadDataPremiumMotorOilGraph(AccountNumber, districtIsSelected);
			controller.onLoadDataAntiFreezeGraph(AccountNumber, districtIsSelected);
			controller.onLoadDataWiperUnitsCard(AccountNumber, districtIsSelected);
			controller.onLoadDataFilterUnitsGraph(AccountNumber, districtIsSelected);
		},

		/**
		 * This function Loads the Account information to be used in the page. Get the account model from the core.
		 * @function onLoadAccountInfo
		 */
		onLoadAccountInfo: function () {
			_oAccount = "0";
			_oAccountNumber = "0";
			_oDistrict = "0";
			_oView = this.getView();

			/**
			 * If there are Accounts, the "oSelectedAccount" model is set in the View and the the address showed is the "Showing results: All Locations" else there the arent Accounts,
			 * model is set in the View and the address is hidden and the combobox is disabled
			 */
			var oAccount;
			if (_oContact.accounts.length > 0) {
				oAccount = new sap.ui.model.json.JSONModel(_oContact.accounts[0]);
				sap.ui.getCore().setModel(oAccount, "oSelectedAccount");
				_oView.setModel(oAccount, "oSelectedAccount");
				this.onLoadDataConfiguration(_oAccount, _oDistrict);
				this.reconfigAddressLabel();
			} else {
				this.noDataPageLayout();
			}
		},

		/**
		 * This function gets the account id and loads all the graphs.
		 * @function onChangeAccountComboBox
		 */
		onChangeAccountComboBox: function (oEvent) {
			// Checks Session time out
			this.checkSessionTimeout();

			if (oEvent !== "" && oEvent.getParameter("newValue") === "") {
				_oView.byId("comboBoxInsights").setSelectedKey("0");
			}

			var selectedKey = this.byId("comboBoxInsights").getSelectedKey();
			var i;
			if (selectedKey.indexOf("district") !== -1) {
				if (oEvent !== "") {
					_oDistrict = oEvent.getParameters().value;
				} else {
					_oDistrict = _oController.byId("comboBoxInsights").getValue();
				}
				_pReferenceLineData.then(function () {
					_referenceLineTotalGallons = 0;
					_referenceLineVPS = 0;
					_referenceLineOilPrcnt = 0;
					_referenceLineOilPrcntNumb = 0;
					for (i = 0; i < _oSettingsData.length; i++) {
						_oController.setReferenceLineForAcc(_oDistrict, _oSettingsData[i]);
					}
				});
			} else if (selectedKey === "0") {
				_oDistrict = "0";
				_pReferenceLineData.then(function () {
					if (_oSettingsData !== undefined && _oSettingsData.length !== 0) {
						_referenceLineTotalGallons = 0;
						_referenceLineVPS = 0;
						_referenceLineOilPrcnt = 0;
						_referenceLineOilPrcntNumb = 0;
						_oController.calcDescendantsTotal(_oSettingsData);
						_oController.onBuildVPSGraph();
						_oController.onBuildEngineOilGraph();
						_oController.onBuildPremiumMotorOilGraph();
					}
				});
			} else {
				_oDistrict = "0";
				_pReferenceLineData.then(function () {
					_referenceLineTotalGallons = 0;
					_referenceLineVPS = 0;
					_referenceLineOilPrcnt = 0;
					_referenceLineOilPrcntNumb = 0;
					for (i = 0; i < _oSettingsData.length; i++) {
						_oController.setReferenceLineForAcc(selectedKey, _oSettingsData[i]);
					}
				});
			}

			_oAccount = this.findAccountByDistrict(selectedKey, _oController);

			if (_oAccount === undefined) {
				_oView.byId("comboBoxInsights").setSelectedKey("0");
				_oAccount = this.findAccountByDistrict("0", _oController);
			}
			if (_oAccount.id === "0") {
				_oView.byId("comboBoxInsights").setSelectedKey("0");
				_oView.byId("accountAddress1Mobile").setVisible(false);
			} else {
				_oView.byId("accountAddress1Mobile").setVisible(true);
			}
			if (_oAccount) {
				if (_oAccount.accountnumber === undefined) {
					_oAccountNumber = "0";
				} else {
					_oAccountNumber = _oAccount.accountnumber;
				}
			} else {
				_oAccountNumber = "0";
				_oAccount = [];
				_oAccount.id = "district-" + _oDistrict;
				_oAccount.districtName = _oDistrict;
				_oAccount.billingcity = _oController.getResourceBundle().getText("MAresultsTXT");
			}

			this.onLoadDataConfiguration(_oAccountNumber, _oDistrict);

			sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);

			if (this.getView().byId("comboBoxInsights").getSelectedKey() === "0" || this.getView().byId("comboBoxInsights").getSelectedKey()
				.indexOf(
					"district") !== -1) {
				this.getView().byId("accountInfoContainerDesktop").setVisible(false);
				this.getView().byId("accountAddress1Mobile").setVisible(false);
			} else {
				this.getView().byId("accountInfoContainerDesktop").setVisible(true);
			}

			//Remove the focus of the input
			$(':focus').blur();
		},

		/**
		 * Configure GraphYAxis
		 * function name: configureRangeGraphYAxiss
		 * @parameters (cardGraphID, rangeFixedEnabled)
		 */
		configureRangeGraphYAxis: function (cardGraphType, data, cardGraphID) {
			// If the data from the server is equal a "0", set the range from 0 to 100. There are 3 types of graphn in this page
			switch (cardGraphType) {
			case "Type1":
				if (data["Prior YTD"].GallonsYear === "0" && data["Current YTD"].GallonsYear === "0") {
					this.fixedRangeGraphYAxis(cardGraphID, true);
				} else {
					this.fixedRangeGraphYAxis(cardGraphID, false);
				}
				break;
			case "Type2":
				if (data[0].GallonsYear === "0" && data[1].GallonsYear === "0") {
					this.fixedRangeGraphYAxis(cardGraphID, true);
				} else {
					this.fixedRangeGraphYAxis(cardGraphID, false);
				}
				break;
			case "Type3":
				if (data[0].GallonsAirFilter === "0" && data[0].GallonsOilFilter === "0" && data[0].GallonsCabinFilter === "0" && data[1].GallonsAirFilter ===
					"0" && data[1].GallonsOilFilter === "0" && data[1].GallonsCabinFilter === "0") {
					this.fixedRangeGraphYAxis(cardGraphID, true);
				} else {
					this.fixedRangeGraphYAxis(cardGraphID, false);
				}
				break;
			default:
				break;
			}
		},

		/**
		 * Disable GraphYAxis
		 * @parameters (cardGraphID, rangeFixedEnabled)
		 */
		fixedRangeGraphYAxis: function (cardGraphID, rangeFixedEnabled) {
			switch (rangeFixedEnabled) {
			case true:
				cardGraphID.setVizProperties({
					yAxis: {
						scale: {
							fixedRange: true,
							minValue: 0,
							maxValue: 100
						}
					}
				});
				break;
			case false:
				cardGraphID.setVizProperties({
					yAxis: {
						scale: {
							fixedRange: false
						}
					}
				});
				break;
			default:
				break;
			}
		},

		/**
		 * This function generates the Engine Oil Graph Graph in the page.
		 * @function onBuildEngineOilGraph
		 */
		onBuildEngineOilGraph: function () {
			var cardGraphID = this.getView().byId("cardGraphEngineOil");
			var graphicalCardDataLegend = this.getResourceBundle().getText("motorOilLabel");
			cardGraphID.destroyFeeds();
			cardGraphID.destroyDataset();

			// Data Set for the graph
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Year',
					value: "{CalendarMonthYear}"
				}],

				measures: [{
					name: graphicalCardDataLegend,
					value: '{GallonsYear}'
				}],

				data: {
					path: "/"
				}
			});
			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');

			// Feed values
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [graphicalCardDataLegend]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Year"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);
			cardGraphID.setVizProperties(_2BarGraphLayout);

			//Set ReferenceLines
			//Motor Oil Total Gallons
			if (_referenceLineTotalGallons > 0) {
				var i18n = _oController.getResourceBundle();
				this.getView().byId("motorOilTotGallonsGoalValue").setVisible(true).setText(i18n.getText("INGTSCurrentGoalLabelTXT1") + " " +
					_oController.commaFy(_referenceLineTotalGallons) + " " + i18n.getText("INGTSCurrentGoalTypeLabelTXT1"));
				cardGraphID.setVizProperties({
					plotArea: {
						colorPalette: [VALV_LIGHT_BLUE],
						referenceLine: {
							line: {
								valueAxis: [{
									value: _referenceLineTotalGallons,
									visible: true,
									color: VALV_DARK_BLUE,
									size: 5
								}]
							}
						}
					}
				});
			} else {
				this.getView().byId("motorOilTotGallonsGoalValue").setVisible(false);
				cardGraphID.setVizProperties({
					plotArea: {
						colorPalette: [VALV_LIGHT_BLUE, VALV_RED],
						referenceLine: {
							line: {
								valueAxis: [{
									visible: false
								}]
							}
						}
					}
				});
			}

		},

		/**
		 * Function that handles the success case of the ajax call that loads the Data for the Engine oil graph header
		 * @function onLoadDataEngineOilGraphHeaderSuccess
		 */
		onLoadDataEngineOilGraphHeaderSuccess: function (oController, oData, oPiggyBack) {
			//Validate if response is for select account
			if (_oAccount && (_oAccount.accountnumber === oPiggyBack.AccountNumber ||
					//or for select district
					oPiggyBack.districtIsSelected === _oAccount.districtName ||
					//or for "All accounts"
					(oPiggyBack.AccountNumber === oPiggyBack.districtIsSelected && oPiggyBack.districtIsSelected === _oAccount.id))) {
				var cardGraphID = oController.getView().byId("cardGraphEngineOil");
				var oModelMO = new sap.ui.model.json.JSONModel();
				oController.onBuildEngineOilGraph();
				oModelMO.setData(oData);
				cardGraphID.setBusy(false);
				_oView.byId("cardGraphEngineOil").setModel(oModelMO);
				_oView.byId("cardGraphEngineOilButton").setEnabled(true);
				_oView.setModel(oModelMO, "engineOilHeader");
				oController.configureRangeGraphYAxis("Type1", oData, cardGraphID);
			}
		},

		/**
		 * Function that handles the error case of the ajax call that loads the Data for the Engine oil graph header
		 * @function onLoadDataEngineOilGraphHeaderError
		 */
		onLoadDataEngineOilGraphHeaderError: function (oController, oError, oPiggyBack) {
			var cardGraphID = oController.getView().byId("cardGraphEngineOil");
			cardGraphID.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERREngineOilHeader"));
			_oView.byId("cardGraphEngineOilButton").setEnabled(false);
			_oView.byId("headerEngineOil").removeStyleClass("insightsCardTitle__header");
		},

		/**
		 * This function loads the Data for the Engine Oil Graph Header
		 * @function onLoadDataEngineOilGraphHeader
		 * @param {string} AccountNumber - the account number required to load the graph
		 * @param {string} districtIsSelected - selected district (if applicable)
		 */
		onLoadDataEngineOilGraphHeader: function (AccountNumber, districtIsSelected) {
			var cardGraphID = this.getView().byId("cardGraphEngineOil");
			cardGraphID.setBusyIndicatorDelay(0);
			cardGraphID.setBusy(true);

			/* Service Call */
			var controller = this;

			var xsURL = "/insights/engineOilHeader";

			if (AccountNumber === "0" && districtIsSelected !== "0") {
				//When a district is selected
				xsURL += "?customerreportinggroup=" + districtIsSelected;
			} else if (AccountNumber !== "0" && districtIsSelected === "0") {
				//When an accounts is selected
				xsURL += "?ShipTo=" + AccountNumber;
			} else {
				//If All Locations is selected
				//Don't alter the URL
			}
			var oPiggyBack = {
				"AccountNumber": AccountNumber,
				"districtIsSelected": districtIsSelected
			};
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadDataEngineOilGraphHeaderSuccess, controller.onLoadDataEngineOilGraphHeaderError,
				null, oPiggyBack);
			//Remove the focus of the input
			$(':focus').blur();
		},

		/**
		 * This function generates the VPS Graph Graph in the page
		 * @function onBuildVPSGraph
		 */
		onBuildVPSGraph: function () {
			var cardGraphID = this.getView().byId("cardGraphVPS");
			var graphicalCardDataLegend = this.getResourceBundle().getText("vpslLabel");
			cardGraphID.destroyFeeds();
			cardGraphID.destroyDataset();
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Year',
					value: "{CalendarMonthYear}"
				}],

				measures: [{
					name: graphicalCardDataLegend,
					value: '{GallonsYear}'
				}],

				data: {
					path: "/"
				}
			});
			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');

			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [graphicalCardDataLegend]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Year"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);
			cardGraphID.setVizProperties(_2BarGraphLayout);
			// Set Reference line VPS
			if (_referenceLineVPS > 0) {
				var i18n = _oController.getResourceBundle();
				this.getView().byId("VPSUnitsGoalValue").setVisible(true).setText(i18n.getText("INGTSCurrentGoalLabelTXT1") + " " +
					_oController.commaFy(_referenceLineVPS) + " " + i18n.getText("INGTSCurrentGoalTypeLabelTXT2"));
				cardGraphID.setVizProperties({
					plotArea: {
						colorPalette: [VALV_RED],
						referenceLine: {
							line: {
								valueAxis: [{
									value: _referenceLineVPS,
									visible: true,
									color: VALV_DARK_BLUE,
									size: 5
								}]
							}
						}
					}
				});
			} else {
				this.getView().byId("VPSUnitsGoalValue").setVisible(false);
				cardGraphID.setVizProperties({
					plotArea: {
						colorPalette: [VALV_RED],
						referenceLine: {
							line: {
								valueAxis: [{
									visible: false
								}]
							}
						}
					}
				});
			}
		},

		/**
		 * Function that handles the success case of the ajax call to  load the VPS Graph header data
		 * @function onLoadDataVPSGraphHeaderSuccess
		 */
		onLoadDataVPSGraphHeaderSuccess: function (oController, oData, oPiggyBack) {
			//Validate if response is for select account
			if (_oAccount && (_oAccount.accountnumber === oPiggyBack.AccountNumber ||
					//or for select district
					oPiggyBack.districtIsSelected === _oAccount.districtName ||
					//or for "All accounts"
					(oPiggyBack.AccountNumber === oPiggyBack.districtIsSelected && oPiggyBack.districtIsSelected === _oAccount.id))) {
				var oModelVPS = new sap.ui.model.json.JSONModel();
				var cardGraphID = oController.getView().byId("cardGraphVPS");
				oController.onBuildVPSGraph();
				oModelVPS.setData(oData);
				cardGraphID.setBusy(false);
				_oView.byId("cardGraphVPS").setModel(oModelVPS);
				_oView.byId("cardGraphVPSButton").setEnabled(true);
				_oView.setModel(oModelVPS, "vpsHeader");
				oController.configureRangeGraphYAxis("Type1", oData, cardGraphID);
			}
		},

		/**
		 * Function that handles the error case of the ajax call to  load the VPS Graph header data
		 * @function onLoadDataVPSGraphHeaderError
		 */
		onLoadDataVPSGraphHeaderError: function (oController, oError, oPiggyBack) {
			var cardGraphID = oController.getView().byId("cardGraphVPS");
			cardGraphID.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRVpsHeader"));
			_oView.byId("cardGraphVPSButton").setEnabled(false);
			_oView.byId("headerVPS").removeStyleClass("insightsCardTitle__header");
		},

		/**
		 * This function loads the Data for the VPS Graph Header.
		 * @function onLoadDataVPSGraphHeader
		 * @param {string} AccountNumber - the account number required to load the graph
		 * @param {string} districtIsSelected - selected district (if applicable)
		 */
		onLoadDataVPSGraphHeader: function (AccountNumber, districtIsSelected) {
			var cardGraphID = this.getView().byId("cardGraphVPS");
			cardGraphID.setBusyIndicatorDelay(0);
			cardGraphID.setBusy(true);

			/* Service Call */
			var controller = this;
			var xsURL = "/insights/vpsHeader";

			if (AccountNumber === "0" && districtIsSelected !== "0") {
				//When a district is selected
				xsURL += "?customerreportinggroup=" + districtIsSelected;
			} else if (AccountNumber !== "0" && districtIsSelected === "0") {
				//When an accounts is selected
				xsURL += "?ShipTo=" + AccountNumber;
			} else {
				//If All Locations is selected
				//Don't alter the URL
			}
			var oPiggyBack = {
				"AccountNumber": AccountNumber,
				"districtIsSelected": districtIsSelected
			};
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadDataVPSGraphHeaderSuccess, controller.onLoadDataVPSGraphHeaderError,
				null, oPiggyBack);
		},

		/**
		 * This function generates the Premium Motor Oil Gallons Purchased Graph (1th Graph type Card).
		 * @function onBuildPremiumMotorOilGraph
		 */
		onBuildPremiumMotorOilGraph: function () {
			var cardGraphID = this.getView().byId("cardGraphPremiumOil");
			var graphicalCardDataLegend = this.getResourceBundle().getText("premiumEngineOilLabel");
			cardGraphID.destroyFeeds();
			cardGraphID.destroyDataset();
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Year',
					value: "{CalendarMonthYear}"
				}],

				measures: [{
					name: graphicalCardDataLegend,
					value: '{GallonsYear}'
				}],

				data: {
					path: "/"
				}
			});
			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');

			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [graphicalCardDataLegend]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Year"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);
			cardGraphID.setVizProperties(_2BarGraphLayout);
			//Premium Oil reference line calculation
			var prcntToDisplay;
			if (_referenceLineOilPrcnt > 0) {
				prcntToDisplay = Math.round(_referenceLineOilPrcnt / _referenceLineOilPrcntNumb);
				var i18n = _oController.getResourceBundle();
				this.getView().byId("motorOilTotPrcntGoalValue").setVisible(true).setText(i18n.getText("INGTSCurrentGoalLabelTXT1") + " " +
					_oController.commaFy(prcntToDisplay) + " " + i18n.getText("INGTSCurrentGoalTypeLabelTXT3"));
				cardGraphID.setVizProperties({
					plotArea: {
						colorPalette: [VALV_DARK_BLUE],
						referenceLine: {
							line: {
								valueAxis: [{
									value: (prcntToDisplay),
									visible: true,
									color: VALV_DARK_BLUE,
									size: 5
								}]
							}
						}
					},
					legend: {
						visible: false,
						title: {
							visible: false
						},
						label: {
							style: {
								fontSize: "14px",
								fontWeight: "lighter",
								fontFamily: "Roboto"
							}
						}
					}
				});
			} else {
				this.getView().byId("motorOilTotPrcntGoalValue").setVisible(false);
				cardGraphID.setVizProperties({
					plotArea: {
						colorPalette: [VALV_DARK_BLUE],
						referenceLine: {
							line: {
								valueAxis: [{
									visible: false
								}]
							}
						}
					},
					legend: {
						visible: false,
						title: {
							visible: false
						},
						label: {
							style: {
								fontSize: "14px",
								fontWeight: "lighter",
								fontFamily: "Roboto"
							}
						}
					}
				});
			}
		},

		/**
		 * Function that handles the success case of the ajax call to load the premium motor oil graph data
		 * @function onLoadDataPremiumMotorOilGraphSuccess
		 */
		onLoadDataPremiumMotorOilGraphSuccess: function (oController, oData, oPiggyBack) {
			//Validate if response is for select account
			if (_oAccount && (_oAccount.accountnumber === oPiggyBack.AccountNumber ||
					//or for select district
					oPiggyBack.districtIsSelected === _oAccount.districtName ||
					//or for "All accounts"
					(oPiggyBack.AccountNumber === oPiggyBack.districtIsSelected && oPiggyBack.districtIsSelected === _oAccount.id))) {
				var cardGraphID = oController.getView().byId("cardGraphPremiumOil");
				var oModelMO = new sap.ui.model.json.JSONModel();
				oController.onBuildPremiumMotorOilGraph();
				oModelMO.setData(oData);
				cardGraphID.setBusy(false);
				_oView.byId("cardGraphPremiumOil").setModel(oModelMO);
				_oView.byId("cardGraphPremiumOilButton").setEnabled(true);
				oController.configureRangeGraphYAxis("Type2", oData, cardGraphID);
			}
		},

		/**
		 * Function that handles the error case of the ajax call to load the premium motor oil graph data
		 * @function onLoadDataPremiumMotorOilGraphError
		 */
		onLoadDataPremiumMotorOilGraphError: function (oController, oError, oPiggyBack) {
			var cardGraphID = oController.getView().byId("cardGraphPremiumOil");
			cardGraphID.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERREngineOilPremium"));
			_oView.byId("cardGraphPremiumOilButton").setEnabled(false);
		},

		/**
		 * This function loads the Data for the Premium Motor Oil Graph (1st Graphical).
		 * @function onLoadDataPremiumMotorOilGraph
		 * @param {string} AccountNumber - the account number required to load the graph
		 * @param {string} districtIsSelected - selected district (if applicable)
		 */
		onLoadDataPremiumMotorOilGraph: function (AccountNumber, districtIsSelected) {
			var cardGraphID = this.getView().byId("cardGraphPremiumOil");
			cardGraphID.setBusyIndicatorDelay(0);
			cardGraphID.setBusy(true);

			/* Service Call */
			var controller = this;
			var xsURL = "/insights/engineOilPremium";

			if (AccountNumber === "0" && districtIsSelected !== "0") {
				//When a district is selected
				xsURL += "?customerreportinggroup=" + districtIsSelected;
			} else if (AccountNumber !== "0" && districtIsSelected === "0") {
				//When an accounts is selected
				xsURL += "?ShipTo=" + AccountNumber;
			} else {
				//If All Locations is selected
				//Don't alter the URL
			}
			var oPiggyBack = {
				"AccountNumber": AccountNumber,
				"districtIsSelected": districtIsSelected
			};
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadDataPremiumMotorOilGraphSuccess, controller.onLoadDataPremiumMotorOilGraphError,
				null, oPiggyBack);
		},

		/**
		 * This function generates the Anti Freeze Graph (2th Graph type Card).
		 * @function onBuildAntiFreezeGraph
		 */
		onBuildAntiFreezeGraph: function () {
			var cardGraphID = this.getView().byId("cardGraphAntiFreeze");
			var graphicalCardDataLegend = this.getResourceBundle().getText("antifreezeLabel");
			cardGraphID.destroyFeeds();
			cardGraphID.destroyDataset();
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Year',
					value: "{CalendarMonthYear}"
				}],

				measures: [{
					name: graphicalCardDataLegend,
					value: '{GallonsYear}'
				}],

				data: {
					path: "/"
				}
			});
			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');

			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [graphicalCardDataLegend]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Year"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);
			cardGraphID.setVizProperties(_2BarGraphLayout);
			cardGraphID.setVizProperties({
				plotArea: {
					colorPalette: [VALV_RED]
				},
				legend: {
					visible: false,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "14px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * Function that handles the success case of the ajax call to get the anti freeze graph data
		 * @function onLoadDataAntiFreezeGraphSuccess
		 */
		onLoadDataAntiFreezeGraphSuccess: function (oController, oData, oPiggyBack) {
			//Validate if response is for select account
			if (_oAccount && (_oAccount.accountnumber === oPiggyBack.AccountNumber ||
					//or for select district
					oPiggyBack.districtIsSelected === _oAccount.districtName ||
					//or for "All accounts"
					(oPiggyBack.AccountNumber === oPiggyBack.districtIsSelected && oPiggyBack.districtIsSelected === _oAccount.id))) {
				var cardGraphID = oController.getView().byId("cardGraphAntiFreeze");
				var oModelAF = new sap.ui.model.json.JSONModel();
				oController.onBuildAntiFreezeGraph();
				oModelAF.setData(oData);
				cardGraphID.setBusy(false);
				cardGraphID.setModel(oModelAF);
				_oView.byId("cardGraphAntiFreezeButton").setEnabled(true);
				oController.configureRangeGraphYAxis("Type2", oData, cardGraphID);
			}
		},

		/**
		 * Function that handles the error case of the ajax call to get the anti freeze graph data
		 * @function onLoadDataAntiFreezeGraphError
		 */
		onLoadDataAntiFreezeGraphError: function (oController, oError, oPiggyBack) {
			var cardGraphID = oController.getView().byId("cardGraphAntiFreeze");
			cardGraphID.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRAntifreeze"));
			_oView.byId("cardGraphAntiFreezeButton").setEnabled(false);
		},

		/**
		 * This functions Loads Data for the Anti Freeze Gallons Purchased Card (2th Graph type Card).
		 * @function onLoadDataAntiFreezeGraph
		 * @param {string} AccountNumber - the account number required to load the graph
		 * @param {string} districtIsSelected - selected district (if applicable)
		 */
		onLoadDataAntiFreezeGraph: function (AccountNumber, districtIsSelected) {
			var cardGraphID = this.getView().byId("cardGraphAntiFreeze");
			cardGraphID.setBusyIndicatorDelay(0);
			cardGraphID.setBusy(true);

			/* Service Call */
			var controller = this;
			var xsURL = "/insights/antifreeze";

			if (AccountNumber === "0" && districtIsSelected !== "0") {
				//When a district is selected
				xsURL += "?customerreportinggroup=" + districtIsSelected;
			} else if (AccountNumber !== "0" && districtIsSelected === "0") {
				//When an accounts is selected
				xsURL += "?ShipTo=" + AccountNumber;
			} else {
				//If All Locations is selected
				//Don't alter the URL
			}
			var oPiggyBack = {
				"AccountNumber": AccountNumber,
				"districtIsSelected": districtIsSelected
			};
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadDataAntiFreezeGraphSuccess, controller.onLoadDataAntiFreezeGraphError,
				null, oPiggyBack);
		},

		/**
		 * This function generates the Wiper Units Purchased  (3th Graph type Card).
		 * @function onBuildWiperUnitsCard
		 */
		onBuildWiperUnitsCard: function () {
			var cardGraphID = this.getView().byId("cardGraphWiperUnits");
			var graphicalCardDataLegend = this.getResourceBundle().getText("wipersLabel");
			cardGraphID.destroyFeeds();
			cardGraphID.destroyDataset();
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Year',
					value: "{CalendarMonthYear}"
				}],

				measures: [{
					name: graphicalCardDataLegend,
					value: '{GallonsYear}'
				}],

				data: {
					path: "/"
				}
			});
			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('column');

			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [graphicalCardDataLegend]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Year"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);
			cardGraphID.setVizProperties(_2BarGraphLayout);
			cardGraphID.setVizProperties({
				plotArea: {
					colorPalette: [VALV_LIGHT_BLUE]
				},
				legend: {
					visible: false,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "14px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * Function that handles the success case of the ajax call to get wiper units card data
		 * @function onSaveSubmitClaimSuccess
		 */
		onLoadDataWiperUnitsCardSuccess: function (oController, oData, oPiggyBack) {
			//Validate if response is for select account
			if (_oAccount && (_oAccount.accountnumber === oPiggyBack.AccountNumber ||
					//or for select district
					oPiggyBack.districtIsSelected === _oAccount.districtName ||
					//or for "All accounts"
					(oPiggyBack.AccountNumber === oPiggyBack.districtIsSelected && oPiggyBack.districtIsSelected === _oAccount.id))) {
				var cardGraphID = oController.getView().byId("cardGraphWiperUnits");
				var oModelWU = new sap.ui.model.json.JSONModel();
				oController.onBuildWiperUnitsCard();
				oModelWU.setData(oData);
				cardGraphID.setBusy(false);
				cardGraphID.setModel(oModelWU);
				_oView.byId("cardGraphWiperUnitsButton").setEnabled(true);
				oController.configureRangeGraphYAxis("Type2", oData, cardGraphID);
			}
		},

		/**
		 * Function that handles the error case of the ajax call to get wiper units card data
		 * @function onSaveSubmitClaimError
		 */
		onLoadDataWiperUnitsCardError: function (oController, oError, oPiggyBack) {
			var cardGraphID = oController.getView().byId("cardGraphWiperUnits");
			cardGraphID.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRWipers"));
			_oView.byId("cardGraphWiperUnitsButton").setEnabled(false);

		},

		/**
		 * This function Loads Data for the Wiper Units Purchased  (3th Graph type Card).
		 * @function onLoadDataWiperUnitsCard -
		 * @param {string} AccountNumber - the account number required to load the graph
		 * @param {string} districtIsSelected - selected district (if applicable)
		 */
		onLoadDataWiperUnitsCard: function (AccountNumber, districtIsSelected) {
			var cardGraphID = this.getView().byId("cardGraphWiperUnits");
			cardGraphID.setBusyIndicatorDelay(0);
			cardGraphID.setBusy(true);

			/* Service Call */
			var controller = this;
			var xsURL = "/insights/wipers";

			if (AccountNumber === "0" && districtIsSelected !== "0") {
				//When a district is selected
				xsURL += "?customerreportinggroup=" + districtIsSelected;
			} else if (AccountNumber !== "0" && districtIsSelected === "0") {
				//When an accounts is selected
				xsURL += "?ShipTo=" + AccountNumber;
			} else {
				//If All Locations is selected
				//Don't alter the URL
			}
			var oPiggyBack = {
				"AccountNumber": AccountNumber,
				"districtIsSelected": districtIsSelected
			};
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadDataWiperUnitsCardSuccess, controller.onLoadDataWiperUnitsCardError,
				null, oPiggyBack);
		},

		/**
		 * This function generates the Filter Units Card  (4th Graph type Card).
		 * @function onBuildFilterUnitsGraph
		 */
		onBuildFilterUnitsGraph: function () {
			var cardGraphID = this.getView().byId("cardGraphFilterUnits");
			cardGraphID.destroyFeeds();
			cardGraphID.destroyDataset();
			var conventional = this.getResourceBundle().getText("airFiltersLB");
			var highMileage = this.getResourceBundle().getText("oilFiltersLB");
			var synthetic = this.getResourceBundle().getText("cabinFiltersLB");
			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: 'Year',
					value: "{CalendarMonthYear}"
				}],

				measures: [{
					name: conventional,
					value: '{GallonsAirFilter}'
				}, {
					name: highMileage,
					value: '{GallonsOilFilter}'
				}, {
					name: synthetic,
					value: '{GallonsCabinFilter}'
				}],

				data: {
					path: "/"
				}
			});

			cardGraphID.setDataset(oDataset);
			cardGraphID.setVizType('stacked_column');
			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "valueAxis",
					'type': "Measure",
					'values': [conventional, highMileage, synthetic]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					'uid': "categoryAxis",
					'type': "Dimension",
					'values': ["Year"]
				});
			cardGraphID.addFeed(feedValueAxis);
			cardGraphID.addFeed(feedCategoryAxis);

			cardGraphID.setVizProperties(_2BarGraphLayout);
			cardGraphID.setVizProperties({
				plotArea: {
					dataPointStyle: {},
					colorPalette: [VALV_LIGHT_GREY, VALV_RED, VALV_LIGHT_BLUE]
				},
				legend: {
					visible: true,
					title: {
						visible: false
					},
					label: {
						style: {
							fontSize: "12px",
							fontWeight: "lighter",
							fontFamily: "Roboto"
						}
					}
				}
			});
		},

		/**
		 * Function that handles the success case of the ajax call to submit Claims
		 * @function onSaveSubmitClaimSuccess
		 */
		onLoadDataFilterUnitsGraphSuccess: function (oController, oData, oPiggyBack) {
			//Validate if response is for select account
			if (_oAccount && (_oAccount.accountnumber === oPiggyBack.AccountNumber ||
					//or for select district
					oPiggyBack.districtIsSelected === _oAccount.districtName ||
					//or for "All accounts"
					(oPiggyBack.AccountNumber === oPiggyBack.districtIsSelected && oPiggyBack.districtIsSelected === _oAccount.id))) {
				var cardGraphID = oController.getView().byId("cardGraphFilterUnits");
				var oModelFU = new sap.ui.model.json.JSONModel();
				oController.onBuildFilterUnitsGraph();
				oModelFU.setData(oData);
				cardGraphID.setBusy(false);
				cardGraphID.setModel(oModelFU);
				_oView.byId("cardGraphFilterUnitsButton").setEnabled(true);
				oController.configureRangeGraphYAxis("Type3", oData, cardGraphID);
			}
		},

		/**
		 * Function that handles the error case of the ajax call to submit Claims
		 * @function onSaveSubmitClaimError
		 */
		onLoadDataFilterUnitsGraphError: function (oController, oError, oPiggyBack) {
			var cardGraphID = oController.getView().byId("cardGraphFilterUnits");
			cardGraphID.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERRFilter"));
			_oView.byId("cardGraphFilterUnitsButton").setEnabled(false);
		},

		/**
		 * This function Loads Data for the Filter Units Purchased  (4th Graph type Card).
		 * @function onLoadDataFilterUnitsGraph
		 * @param {string} AccountNumber - the account number required to load the graph
		 * @param {string} districtIsSelected - selected district (if applicable)
		 */
		onLoadDataFilterUnitsGraph: function (AccountNumber, districtIsSelected) {
			var cardGraphID = this.getView().byId("cardGraphFilterUnits");
			cardGraphID.setBusyIndicatorDelay(0);
			cardGraphID.setBusy(true);

			/* Service Call */
			var controller = this;
			var xsURL = "/insights/filters";

			if (AccountNumber === "0" && districtIsSelected !== "0") {
				//When a district is selected
				xsURL += "?customerreportinggroup=" + districtIsSelected;
			} else if (AccountNumber !== "0" && districtIsSelected === "0") {
				//When an accounts is selected
				xsURL += "?ShipTo=" + AccountNumber;
			} else {
				//If All Locations is selected
				//Don't alter the URL
			}
			var oPiggyBack = {
				"AccountNumber": AccountNumber,
				"districtIsSelected": districtIsSelected
			};
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onLoadDataFilterUnitsGraphSuccess, controller.onLoadDataFilterUnitsGraphError,
				null, oPiggyBack);
		},

		/**
		 * This function Open the Insights Details Page.
		 * @function onPressCardGraphEngineOil
		 */
		onPressCardGraphEngineOil: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			//GTM
			_oController.GTMDataLayer('seedetailsclick',
				'Insights',
				'See Details Button-Click',
				_oController.getResourceBundle().getText("INGTSSubtitle1TXT")
			);

			ga('set', 'page', '/InsightsGraphicDetails-EngineOil');
			ga('send', 'pageview');
			var premiumMotorOilPrcntFinal = Math.round(parseInt(_referenceLineOilPrcnt) / parseInt(_referenceLineOilPrcntNumb));
			if (isNaN(premiumMotorOilPrcntFinal)) {
				premiumMotorOilPrcntFinal = 0;
			}
			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.publish("resetSelect", "resetSelect");
			this.getRouter().navTo("insightsGraphicDetails", {
				graphType: "EngineOil"
			});
			sap.ui.getCore().getEventBus().publish("motorOilGoals", "motorOilGoals", {
				motorOilTotalGallons: _referenceLineTotalGallons,
				premiumMotorOilPrcnt: premiumMotorOilPrcntFinal,
				goalsData: _oSettingsData,
				district: _oDistrict,
				accountNumber: _oAccountNumber
			});

		},

		/**
		 * This function Open the Insights Details Page.
		 * @function onPressCardGraphVPS
		 */
		onPressCardGraphVPS: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			//GTM
			_oController.GTMDataLayer('seedetailsclick',
				'Insights',
				'See Details Button-Click',
				_oController.getResourceBundle().getText("INGTSSubtitle2TXT")
			);

			ga('set', 'page', '/InsightsGraphicDetails-VPS');
			ga('send', 'pageview');

			this.getRouter().navTo("insightsGraphicDetails", {
				graphType: "VPS"
			});
			sap.ui.getCore().getEventBus().publish("vpsUnitsGoals", "vpsUnitsGoals", {
				vpsChemicalUnt: _referenceLineVPS,
				goalsData: _oSettingsData,
				district: _oDistrict
			});
		},

		/**
		 * This function Open the Insights Details Page.
		 * @function onPressCardGraphPremiumOil
		 */
		onPressCardGraphPremiumOil: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			//GTM
			_oController.GTMDataLayer('seedetailsclick',
				'Insights',
				'See Details Button-Click',
				_oController.getResourceBundle().getText("INGTSSubtitle3TXT")
			);

			ga('set', 'page', '/InsightsDetails-PremiumOil');
			ga('send', 'pageview');

			this.getRouter().navTo("insightsDetails", {
				graphType: "PremiumOil"
			});

			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.publish("navToInsightsDetails", "navToInsightsDetails", {
				accountNumber: _oAccountNumber,
				district: _oDistrict,
				graphType: "PremiumOil",
				language: sap.ui.getCore().getConfiguration().getLanguage()
			});

		},

		/**
		 * This function opens Insights Details page for "Antifreeze".
		 * @function onPressCardGraphAntiFreeze
		 */
		onPressCardGraphAntiFreeze: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			//GTM
			_oController.GTMDataLayer('seedetailsclick',
				'Insights',
				'See Details Button-Click',
				_oController.getResourceBundle().getText("INGTSSubtitle4TXT")
			);

			ga('set', 'page', '/InsightsDetails-Antifreze');
			ga('send', 'pageview');

			this.getRouter().navTo("insightsDetails", {
				graphType: "Antifreeze"
			});

			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.publish("navToInsightsDetails", "navToInsightsDetails", {
				accountNumber: _oAccountNumber,
				district: _oDistrict,
				graphType: "Antifreeze",
				language: sap.ui.getCore().getConfiguration().getLanguage()
			});
		},

		/**
		 * This function opens Insights Details page for "WiperUnits".
		 * @function onPressCardGraphWiperUnits
		 */
		onPressCardGraphWiperUnits: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			//GTM
			_oController.GTMDataLayer('seedetailsclick',
				'Insights',
				'See Details Button-Click',
				_oController.getResourceBundle().getText("INGTSSubtitle5TXT")
			);

			ga('set', 'page', '/InsightsDetails-WiperUnits');
			ga('send', 'pageview');

			this.getRouter().navTo("insightsDetails", {
				graphType: "WiperUnits"
			});

			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.publish("navToInsightsDetails", "navToInsightsDetails", {
				accountNumber: _oAccountNumber,
				district: _oDistrict,
				graphType: "WiperUnits",
				language: sap.ui.getCore().getConfiguration().getLanguage()
			});
		},

		/**
		 * This function opens Insights Details page for "FilterUnits".
		 * @function onPressCardGraphFilterUnits
		 */
		onPressCardGraphFilterUnits: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			//GTM
			_oController.GTMDataLayer('seedetailsclick',
				'Insights',
				'See Details Button-Click',
				_oController.getResourceBundle().getText("INGTSSubtitle6TXT")
			);

			ga('set', 'page', '/InsightsDetails-FilterUnits');
			ga('send', 'pageview');

			this.getRouter().navTo("insightsDetails", {
				graphType: "FilterUnits"
			});

			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.publish("navToInsightsDetails", "navToInsightsDetails", {
				accountNumber: _oAccountNumber,
				district: _oDistrict,
				graphType: "FilterUnits",
				language: sap.ui.getCore().getConfiguration().getLanguage()
			});
		}
	});
});