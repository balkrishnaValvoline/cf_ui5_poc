sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel",
	'sap/ui/core/util/Export',
	'sap/ui/core/util/ExportTypeCSV',
	"sap/m/MessageToast",
	"sap/ui/core/mvc/Controller",
	"jquery.sap.global"
], function (Base, JSONModel, Export, ExportTypeCSV, MessageToast, Controller, jQuery) {
	"use strict";
	var _oController;
	var _oRouter;
	var _hasChanged = false;
	var _reader;
	var _routeFile;
	var _aWarehouses;
	var _isValidationPresed = false,
		_isDeletePressed = false;
	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Orders.OrderRouteSchedule", {
		/** @module OrderRouteSchedule */
		/**
		 * This function initializes the controller and defines the card types
		 * to be loaded.
		 * 
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oController = this;
			_oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			_oRouter.getRoute("orderRouteSchedule").attachPatternMatched(this._onObjectMatched, this);

			this.onDataLossPreventingBrowserAlert();

			_reader = new FileReader();
			_reader.addEventListener("load", function (evt) {
				var strCSV = evt.target.result; // string in CSV
				_oController.csvJSON(strCSV);
			});
			_reader.addEventListener("error", function (evt) {
				_oController.onShowMessage(_oController, "Error", evt.target.error.name, "Unable to load the file");
			});

		},

		_onObjectMatched: function (oEvent) {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page Layout Configuration
			if (!this.checkDASHPageAuthorization("ROUTE_SCHEDULES_PAGE")) {
				return;
			}

			// Check the Edit Access
			this.checkEditable();

			//get user info and direct account
			_oController.getUserInformation(_oController);

			// Check Language and set selected Key in combo box
			this.onChangeLanguage();

			this.headerIconAccess();

			// Shopping Cart Values
			this.getShoppingCartCookie(this);

			// Load Warehouses info
			this.loadWarehouses();

			// Load Schedule Routes
			this.loadModelValidationRoutes();
		},

		/**
		 * onDataLossPreventingBrowserAlert Sets a browser alert that prevents
		 * closing if there is values to submit
		 */
		onDataLossPreventingBrowserAlert: function () {
			// Runs when the user tries to close the "distributorInsights" page
			$(window).bind('beforeunload', function () {
				var route = sap.ui.core.routing.HashChanger.getInstance().getHash();
				if (_hasChanged === true && route === "routeSchedule") {
					return true;
				}
			});
		},

		/**
		 * Function to check the Edit authorization for the user
		 * @function checkEditable
		 */
		checkEditable: function () {
			var oModel = new JSONModel();
			var oData = {};
			var isEditAuthorization = _oController.checkDASHLayoutAccess("ROUTE_SCHEDULES_EDIT");
			oData.isEditable = isEditAuthorization;
			oModel.setData(oData);
			_oController.getView().setModel(oModel, "oEditable");
			if (isEditAuthorization) {
				_oController.byId("routeSchedule_MainTable").setSelectionMode("MultiToggle");
			} else {
				_oController.byId("routeSchedule_MainTable").setSelectionMode("None");
			}
			_oController.byId("routeSchedulesSave").setEnabled(false);
		},

		/**
		 * Function called on press of Upload Routes button
		 * @function handleUploadComplete
		 */
		handleUploadComplete: function (oEvent) {
			// Checks Session time out
			this.checkSessionTimeout();
			var mainTable = _oController.byId("routeSchedule_MainTable");
			var rows = mainTable.getRows().length;
			_routeFile = oEvent.getParameter("files")[0];
			if (rows && rows > 0) {
				this.DataLossWarning =
					new sap.ui.xmlfragment("valvoline.dash.portal.DashPortalWeb.fragment.WarningDialog", this);
				// Set Warning Dialog Buttons
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").attachPress(this.onDialogWarnigRefresh_Yes);
				sap.ui.getCore().byId("button_Dialog_Warning_No").attachPress(this.onDialogWarnigRefresh_No);
				sap.ui.getCore().byId("button_Dialog_Warning_Yes").setText(_oController.getResourceBundle().getText("MOARSyesTXT"));
				sap.ui.getCore().byId("button_Dialog_Warning_No").setText(_oController.getResourceBundle().getText("MOARSnoTXT"));
				// Set Warning Dialog Message
				sap.ui.getCore().byId("text1_Dialog_Warning").setText(_oController.getResourceBundle().getText("MOARSUploadWarningTitle"));
				sap.ui.getCore().byId("text2_Dialog_Warning").setText(_oController.getResourceBundle().getText("MOARSUploadWarningDesc"));
				// Set Warnig Dialog Title
				sap.ui.getCore().byId("title_Dialog_Warning").setText(_oController.getResourceBundle().getText("MOARSUploadMainTitle"));
				this.DataLossWarning.open();
				_oController._removeDialogResize(_oController.DataLossWarning);
			} else {
				if (_routeFile && window.FileReader) {
					_reader.readAsText(_routeFile);
				}
			}
		},

		/**
		 * Function that handles the success case of the ajax call that loads the Route Schedules
		 * @function onLoadValidationRoutesSuccess
		 */
		onLoadValidationRoutesSuccess: function (oController, oData, oPiggyBack) {
			var routeSchedules_Container = oController.getView().byId("vbox_card_container_routeSchedule");
			_hasChanged = false;
			_isValidationPresed = false;
			_isDeletePressed = false;
			routeSchedules_Container.setBusy(false);
			oController.oDataConfiguration(oData);
		},

		/**
		 * Function that handles the error case of the ajax call that loads the Route Schedules
		 * @function onLoadValidationRoutesError
		 */
		onLoadValidationRoutesError: function (oController, oError, oPiggyBack) {
			var routeSchedules_Container = oController.getView().byId("vbox_card_container_routeSchedule");
			routeSchedules_Container.setVisible(false);
			routeSchedules_Container.setBusy(false);
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"MOARSLoadRoutesErrorTXT"), null);
		},

		/**
		 * Loads the route schedules
		 * @function loadModelValidationRoutes
		 */
		loadModelValidationRoutes: function () {
			var controller = this;
			var routeSchedules_Container = this.getView().byId("vbox_card_container_routeSchedule");
			routeSchedules_Container.setBusyIndicatorDelay(0);
			routeSchedules_Container.setBusy(true);
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();

			var xsURL = "/routeschedule/routeSchedules" + "?lang=" + selectedLanguage;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend
			_oController.handleAjaxJSONCall(controller, true, xsURL, "GET", _oController.onLoadValidationRoutesSuccess, _oController.onLoadValidationRoutesError);
		},

		/**
		 * Function that handles the success case of the ajax call that loads the Warehouses
		 * @function onLoadWarehousesSuccess
		 */
		onLoadWarehousesSuccess: function (oController, oData, oPiggyBack) {
			var _oModelWarehouses = new JSONModel();
			_aWarehouses = [];
			var obj = {};
			for (var i = 0; i < oData.length; i++) {
				_aWarehouses.push(oData[i].warehouseId.dist_num + "-" + oData[i].warehouseId.dist_location);
			}
			obj.warehouseId = {
				dist_num: "0",
				dist_location: oController.getResourceBundle("i18n").getText("MOARSTWarehouseSelTXT"),
				valvoline_ship_to: "0"
			};
			oData.unshift(obj);
			_oModelWarehouses.setData(oData);
			_oController.getView().setModel(_oModelWarehouses, "oWarehouses");
			sap.ui.getCore().setModel(_oModelWarehouses, "oWarehouses");
		},

		/**
		 * Function that handles the error case of the ajax call that loads the Warehouses
		 * @function onLoadWarehousesError
		 */
		onLoadWarehousesError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"MOARSLoadWarehouseErrorTXT"), null);
		},

		/**
		 * Loads the warehouses and sets the data to the combobox.
		 * @function loadWarehouses
		 */
		loadWarehouses: function () {
			var controller = this;
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();

			var xsURL = "/routeschedule/warehouses" + "?lang=" + selectedLanguage;
			// handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend
			_oController.handleAjaxJSONCall(controller, true, xsURL, "GET", _oController.onLoadWarehousesSuccess, _oController.onLoadWarehousesError);
		},

		/**
		 * Format the warehouse key as Dist_Num-Dist_Location.
		 * @function formatWarehouseKey
		 */
		formatWarehouseKey: function (dist_num, dist_location) {
			if (dist_num === "0") {
				return "0";
			}
			return dist_num + "-" + dist_location;
		},

		/**
		 * Sets the data to the table.
		 * @function oDataConfiguration
		 */
		oDataConfiguration: function (data) {
			var _oModelRouteSchedule = new JSONModel();
			for (var i = 0; i < data.length; i++) {
				if (data[i].effectiveDate) {
					data[i].effectiveDate = _oController.formatEffectiveDate(data[i].effectiveDate);
				}
			}

			// remove error style class from table cells
			_oController.removeErrorStyleClass();
			_oModelRouteSchedule.setData(data);
			_oController.getView().setModel(_oModelRouteSchedule, "oRouteSchedules");
			sap.ui.getCore().setModel(_oModelRouteSchedule, "oRouteSchedules");

			if (_isDeletePressed && data.length === 0) {
				_oController.byId("routeSchedulesValidate").setEnabled(false);
				_oController.byId("routeSchedulesSave").setEnabled(true);
			} else if (_isValidationPresed) {
				setTimeout(function () {
					_oController.onPressValidateRoutes();
					_oController.enableValidateBtn();
				}, 0);
			} else {
				_oController.enableValidateBtn();
			}
		},

		/**
		 * On Close Dialog Warning Messages
		 */
		onDialogWarnigRefresh_No: function () {
			_oController.DataLossWarning.destroy();
			_routeFile = {};
		},

		/**
		 * On Refresh Data on Dialog Warning Messages
		 */
		onDialogWarnigRefresh_Yes: function () {
			_oController.DataLossWarning.destroy();
			if (_routeFile && window.FileReader) {
				_reader.readAsText(_routeFile);
			}
		},

		/**
		 * Function handles the uploaded data from csv file
		 * @function csvJSON
		 */
		csvJSON: function (csv) {
			var lines = csv.split("\n");
			var i18nResourceBundle = this.getView().getModel("i18n");
			var jsonResponseText, errorText;
			var result = [];
			if (lines.length <= 4002) {
				var headers = lines[0].split(",");
				var obj = {};
				var currentline;
				var collumChecker = 0;
				for (var j = 0; j < headers.length; j++) {
					switch (j) {
					case 0:
						headers[j] = "warehouse";
						collumChecker++;
						break;
					case 1:
						headers[j] = "zipCode";
						collumChecker++;
						break;
					case 2:
						headers[j] = "packageType";
						collumChecker++;
						break;
					case 3:
						headers[j] = "effectiveDate";
						collumChecker++;
						break;
					case 4:
						headers[j] = "cutoffTime";
						collumChecker++;
						break;
					case 5:
						headers[j] = "sundayDelivery";
						collumChecker++;
						break;
					case 6:
						headers[j] = "mondayDelivery";
						collumChecker++;
						break;
					case 7:
						headers[j] = "tuesdayDelivery";
						collumChecker++;
						break;
					case 8:
						headers[j] = "wednesdayDelivery";
						collumChecker++;
						break;
					case 9:
						headers[j] = "thursdayDelivery";
						collumChecker++;
						break;
					case 10:
						headers[j] = "fridayDelivery";
						collumChecker++;
						break;
					case 11:
						headers[j] = "saturdayDelivery";
						collumChecker++;
						break;
					default:
						break;
					}
				}
				if (collumChecker === 12) {
					for (var i = 1; i < lines.length; i++) {
						obj = {};
						currentline = lines[i].split(",");
						if (currentline.length >= 6) {
							for (j = 0; j < collumChecker; j++) {
								if (j === 0) {
									obj[headers[j]] = _oController.validateWarehouse(currentline[j].replace(/(\r|\t|\n)/gm, ""));
								} else {
									obj[headers[j]] = currentline[j].replace(/(\r|\t|\n)/gm, "");
								}
							}
							result.push(obj);
						}
					}
				}
			} else {
				errorText = i18nResourceBundle.getProperty("ErrorDescriptionLines");
			}
			if (result.length) {
				_hasChanged = true;
				_isValidationPresed = false;
				_isDeletePressed = false;
				_oController.oDataConfiguration(result);
			} else {
				errorText = i18nResourceBundle.getProperty("MOARSErrorDescriptionLines");
			}
			if (errorText) {
				jsonResponseText = '{"fault":{"faultstring": "' + i18nResourceBundle.getProperty("ErrorMessageLines") +
					'","detail":{"errorcode": "' + i18nResourceBundle.getProperty("ErrorCodeLines") + '"}}}';
				this.onShowMessage(this, "Error", i18nResourceBundle.getProperty("ErrorCodeLines"),
					errorText, jsonResponseText, null);
			}
		},

		/**
		 * Remove the error class from table cells.
		 * @function removeErrorStyleClass
		 */
		removeErrorStyleClass: function () {
			var mainTable = _oController.byId("routeSchedule_MainTable");
			var rows = mainTable.getRows();
			for (var i = 0; i < rows.length; i++) {
				var column = rows[i].getCells();
				for (var j = 0; j < column.length; j++) {
					if (column[j].hasStyleClass("colorError")) {
						column[j].removeStyleClass("colorError");
					}
				}
			}
		},

		/**
		 * Formats the number of rows of the table.
		 * @function formatTableRowNumber
		 */
		formatTableRowNumber: function (routes) {
			if (routes.length >= 10) {
				return 10;
			} else {
				return routes.length;
			}
		},

		/**
		 * gets the indexes of the selected table rows
		 * @function getSelectedIndexes
		 */
		getSelectedIndexes: function () {
			var mainTable = _oController.byId("routeSchedule_MainTable");
			var selectedRows = mainTable.getSelectedIndices();
			return selectedRows;
		},

		/**
		 * Executes when the user agrees to delete routes.
		 * @function onDeleteRoutes
		 **/
		onDeleteRoutes: function () {
			var mainTable = _oController.byId("routeSchedule_MainTable");
			var oModel_MainTable = mainTable.getModel("oRouteSchedules");
			var oData = oModel_MainTable.getData();
			var selectedRows = _oController.getSelectedIndexes();

			if (selectedRows === null || selectedRows === undefined || selectedRows.length === 0) {
				return;
			}

			for (var x = 0; x < selectedRows.length; x++) {
				oData[selectedRows[x]].isDeleted = true;
			}
			var oNewData = [];
			for (var j = 0; j < oData.length; j++) {
				if (!oData[j].isDeleted) {
					oNewData.push(oData[j]);
				}
			}
			_hasChanged = true;
			_isDeletePressed = true;
			_oController.oDataConfiguration(oNewData);
		},

		/**
		 * Exports routes schedule
		 * @function onPressExportRoutes
		 */
		onPressExportRoutes: sap.m.Table.prototype.exportData || function (oEvent) {
			// Checks Session time out
			this.checkSessionTimeout();
			var oModel = this.getView().getModel("oRouteSchedules");
			var i18nResourceBundle = this.getView().getModel("i18n");
			var oExport = new Export({

				exportType: new ExportTypeCSV({
					fileExtension: "csv",
					separatorChar: ","
				}),
				models: oModel,
				rows: {
					path: "/"
				},
				columns: [{
					name: i18nResourceBundle.getProperty("MOARSTemplateCol1"),
					template: {
						content: {
							path: "warehouse",
							formatter: function (sValue) {
								if (sValue) {
									return sValue.split("-")[1];
								}
							}
						}
					}
				}, {
					name: i18nResourceBundle.getProperty("MOARSTemplateCol2"),
					template: {
						content: "{zipCode}"
					}
				}, {
					name: i18nResourceBundle.getProperty("MOARSTemplateCol3"),
					template: {
						content: "{packageType}"
					}
				}, {
					name: i18nResourceBundle.getProperty("MOARSTemplateCol4"),
					template: {
						content: "{effectiveDate}"
					}
				}, {
					name: i18nResourceBundle.getProperty("MOARSTemplateCol5"),
					template: {
						content: "{cutoffTime}"
					}
				}, {
					name: i18nResourceBundle.getProperty("MOARSTemplateCol6"),
					template: {
						content: "{sundayDelivery}"
					}
				}, {
					name: i18nResourceBundle.getProperty("MOARSTemplateCol7"),
					template: {
						content: "{mondayDelivery}"
					}
				}, {
					name: i18nResourceBundle.getProperty("MOARSTemplateCol8"),
					template: {
						content: "{tuesdayDelivery}"
					}
				}, {
					name: i18nResourceBundle.getProperty("MOARSTemplateCol9"),
					template: {
						content: "{wednesdayDelivery}"
					}

				}, {
					name: i18nResourceBundle.getProperty("MOARSTemplateCol10"),
					template: {
						content: "{thursdayDelivery}"
					}

				}, {
					name: i18nResourceBundle.getProperty("MOARSTemplateCol11"),
					template: {
						content: "{fridayDelivery}"
					}

				}, {
					name: i18nResourceBundle.getProperty("MOARSTemplateCol12"),
					template: {
						content: "{saturdayDelivery}"
					}

				}]
			});
			var today = new Date();
			var month = _oController.pad(today.getMonth() + 1, 2, "0");
			var fileName = "RouteSchedules_" + today.getFullYear() + today.getDate() + month + today.getHours() + today.getMinutes() +
				today.getSeconds();
			oExport.saveFile(fileName).catch(function () {}).then(function () {
				oExport.destroy();
			});
		},

		/**
		 * This function is called when input field is changed
		 * valueChangeInput @ parameter (oEvent)
		 */
		valueChangeInput: function (oEvent) {
			_hasChanged = true;
			_oController.enableValidateBtn();
			if (oEvent.getParameter("newValue")) {
				oEvent.getSource().setValue(oEvent.getParameter("newValue").toUpperCase());
			}
		},

		/**
		 * This function is called to enable or disable Validate Button
		 * enableValidateBtn
		 */
		enableValidateBtn: function () {
			_oController.byId("routeSchedulesValidate").setEnabled(_hasChanged);
			_oController.byId("routeSchedulesSave").setEnabled(false);
		},

		/**
		 * This function adds the new route schedule
		 * onAddNewRoute 
		 */
		onAddNewRoute: function () {
			var mainTable = _oController.byId("routeSchedule_MainTable");
			var oModel_MainTable = mainTable.getModel("oRouteSchedules");
			var oData = oModel_MainTable.getData();
			var obj = {
				"warehouse": "0",
				"zipCode": "",
				"packageType": "",
				"cutoffTime": "",
				"cutoffTimeZone": "",
				"sundayDelivery": ""
			};
			oData.unshift(obj);
			_hasChanged = true;
			_isValidationPresed = false;
			_isDeletePressed = false;
			_oController.oDataConfiguration(oData);
		},

		/**
		 * This function validates the route schedule
		 * onPressValidateRoutes 
		 */
		onPressValidateRoutes: function () {
			var mainTable = _oController.byId("routeSchedule_MainTable");
			var isValid = false;
			_isValidationPresed = true;
			var isValidRecord = [];
			var _aColumns = ["warehouse", "zipCode", "packageType", "effectiveDate", "cutoffTime", "sundayDelivery", "mondayDelivery",
				"tuesdayDelivery", "wednesdayDelivery", "thursdayDelivery",
				"fridayDelivery", "saturdayDelivery"
			];
			var rows = mainTable.getRows().length;
			var _aDuplicates = [];
			var _aDeliveryFlag = [];
			var sValue, iDelivery = 0;
			var _currentRow, _duplicateRow;
			if (rows > 0) {
				isValid = true;
				for (var i = 0; i < rows; i++) {
					_currentRow = mainTable.getRows()[i].getCells();
					sValue = "";
					for (var j = 0; j < _aColumns.length; j++) {
						if (j === 0) {
							if (_currentRow[j].getSelectedKey() === "0") {
								isValid = false;
							}
							sValue += _currentRow[j].getSelectedKey();
						} else if (j <= 4) {
							if (!_currentRow[j].getValue() || _currentRow[j].getValueState() === "Error") {
								isValid = false;
								_currentRow[j].addStyleClass("colorError");
							} else {
								_currentRow[j].removeStyleClass("colorError");
							}
							sValue += _currentRow[j].getValue();
						} else {
							if (_currentRow[j].getValue()) {
								if (_currentRow[j].getValue() === "X" || _currentRow[j].getValue() === "B") {
									_aDeliveryFlag[iDelivery] = true;
									_currentRow[j].removeStyleClass("colorError");
								} else {
									isValid = false;
									_currentRow[j].addStyleClass("colorError");
								}
							} else {
								_currentRow[j].removeStyleClass("colorError");
							}
						}
					}
					if (_aDeliveryFlag.length === iDelivery + 1) {
						iDelivery++;
					}

					// Check duplicates comparing warehouse, zipcode, package type and effective date
					if (_aDuplicates[sValue]) {
						isValid = false;
						_duplicateRow = mainTable.getRows()[i].getCells();
						// Set zipcode, package type and effective date in error state for duplicate rows
						for (var k = 1; k < 4; k++) {
							_duplicateRow[k].addStyleClass("colorError");
						}
					} else {
						_aDuplicates[sValue] = true;
					}
					if (!isValid) {
						isValidRecord[i] = isValid;
					}
				}

				// Enable or disable Save btn based on validations. Each route must have atleast one delivery
				if (isValidRecord.length === 0 && _aDeliveryFlag.length === rows) {
					_oController.byId("routeSchedulesSave").setEnabled(true);
				} else {
					_oController.byId("routeSchedulesSave").setEnabled(false);
				}
			}
		},

		/**
		 * This function validates the warehouse based on Location and returns the combination of Number-Location
		 * validateWarehouse @ parameter sWarehouse
		 */
		validateWarehouse: function (sWarehouse) {
			if (sWarehouse && sWarehouse.split("-").length === 2) {
				for (var i = 0; i < _aWarehouses.length; i++) {
					if (_aWarehouses[i] === sWarehouse) {
						return sWarehouse;
					}
				}
			} else if (sWarehouse && sWarehouse.split("-").length === 1) {
				for (var j = 0; j < _aWarehouses.length; j++) {
					if (_aWarehouses[j].split("-")[1] === sWarehouse) {
						return _aWarehouses[j];
					}
				}
			}
			return "0";
		},

		/**
		 * This function defaults the cutoff time as 12:00 PM
		 * formatDefaultCutoffTime @ parameter sTime
		 */
		formatDefaultCutoffTime: function (sTime) {
			return "12:00 PM";
		},

		/**
		 * This function is called on change of warehouse. It defaults to selected key '0', if no value matches
		 * onWarehouseChange @ parameter oEvent
		 */
		onWarehouseChange: function (oEvent) {
			if (!oEvent.getSource().getSelectedKey()) {
				oEvent.getSource().setSelectedKey("0");
			}
			_hasChanged = true;
			_oController.enableValidateBtn();
		},

		/**
		 * This function is used to check the valid Date
		 * isValidDate @ parameter sDate
		 */
		isValidDate: function (sDate) {
			// First check for the pattern
			if (!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(sDate)) {
				return false;
			}

			// Parse the date parts to integers
			var parts = sDate.split("/");
			var day = parseInt(parts[1], 10);
			var month = parseInt(parts[0], 10);
			var year = parseInt(parts[2], 10);

			// Check the ranges of month and year
			if (year < 1000 || year > 3000 || month == 0 || month > 12) {
				return false;
			}
			var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

			// Adjust for leap years
			if (year % 400 == 0 || (year % 100 != 0 && year % 4 == 0)) {
				monthLength[1] = 29;
			}
			// Check the range of the day
			return day > 0 && day <= monthLength[month - 1];
		},

		/**
		 * This function validates the Package Type
		 * valueStatePackageType @ parameter sValue
		 */
		valueStatePackageType: function (sValue) {
			if (sValue && (sValue !== "P" && sValue !== "B")) {
				return "Error";
			} else {
				return "None";
			}
		},

		/**
		 * This function validates the Zip Code
		 * valueStateZipCode @ parameter sValue
		 */
		valueStateZipCode: function (sValue) {
			if (sValue && (sValue.length !== 5 && sValue.length !== 6)) {
				return "Error";
			} else {
				return "None";
			}
		},

		/**
		 * This function validates the Delivery
		 * valueStateDelivery @ parameter sValue
		 */
		valueStateDelivery: function (sValue) {
			if (sValue && (sValue !== "X" && sValue !== "B")) {
				return "Error";
			} else {
				return "None";
			}
		},

		/**
		 * This function validates the Effective Date
		 * valueStateEffectiveDate @ parameter sValue
		 */
		valueStateEffectiveDate: function (sValue) {
			if (sValue && !_oController.isValidDate(sValue)) {
				return "Error";
			} else {
				return "None";
			}
		},

		/**
		 * This function is called on live change of input fields. 
		 * removeErrorStyle @ parameter oEvent
		 */
		removeErrorStyle: function (oEvent) {
			if (oEvent.getSource().hasStyleClass("colorError")) {
				oEvent.getSource().removeStyleClass("colorError");
			}
			_hasChanged = true;
			_oController.enableValidateBtn();
		},

		/**
		 * Function that handles the success case of the ajax call to insert routes
		 * @function oninsertRoutesSuccess
		 */
		onInsertRoutesSuccess: function (oController, oData, oPiggyBack) {
			var RouteSchedules_Container = _oController.getView().byId("routeSchedule_MainTable");
			// Clears all error message
			oController.clearServerErrorMsg(oController);
			_hasChanged = false;
			_isValidationPresed = false;
			_isDeletePressed = false;
			oController.oDataConfiguration(oData);
			RouteSchedules_Container.setBusy(false);
		},

		/**
		 * Function that handles the error case of the ajax call to insert routes
		 * @function onInsertError
		 */
		onInsertError: function (oController, oError, oPiggyBack) {
			var RouteSchedules_Container = _oController.getView().byId("routeSchedule_MainTable");
			RouteSchedules_Container.setBusy(false);
			_oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oController.getResourceBundle().getText(
				"MOARSSaveRoutesErrorTXT"), null);
		},

		/**
		 * Function is called on press of Save Btn.
		 * @function onPressSubmitRoutes
		 */
		onPressSubmitRoutes: function () {
			var mainTable = _oController.byId("routeSchedule_MainTable");
			var oModel_MainTable = mainTable.getModel("oRouteSchedules");
			var oData = oModel_MainTable.getData();
			var oDate, sDate;
			for (var i = 0; i < oData.length; i++) {
				if (!oData[i].id) {
					oData[i].cutoffTime = "12:00 PM"; // cutoff time is defaulted to 12:00 PM
					oData[i].cutoffTimeZone = Intl.DateTimeFormat().resolvedOptions().timeZone;
				}
				oDate = new Date(oData[i].effectiveDate);
				oDate.setHours(12, 0, 0);
				var utcDate = new Date(oDate.valueOf() + (oDate.getTimezoneOffset() * 60000));
				var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
					pattern: "yyyy-MM-dd'T'HH:mm:ss"
				});
				sDate = dateFormat.format(utcDate);
				oData[i].effectiveDate = sDate;
			}
			var selectedLanguage = sap.ui.getCore().getConfiguration().getLanguage();
			mainTable.setBusyIndicatorDelay(0);
			mainTable.setBusy(true);
			var xsURL = "/routeschedule/insertRouteSchedules" + "?lang=" + selectedLanguage;

			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			_oController.handleAjaxJSONCall(_oController, true, xsURL, "POST", _oController.onInsertRoutesSuccess, _oController.onInsertError,
				JSON.stringify(oData));
		},

		/**
		 * Function to format the effective date in MM/dd/YYYY format
		 * @function formatEffectiveDate @ parameters sDate
		 */
		formatEffectiveDate: function (sDate) {
			if (!sDate) {
				return "";
			} else {
				var newdate = new Date(sDate);
				var localDate = new Date(newdate.valueOf() - (newdate.getTimezoneOffset()) * 60000);
				var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
					pattern: "MM/dd/YYYY"
				});
				var dateFormatted = dateFormat.format(localDate);
				return dateFormatted;
			}
		},

		/**
		 * Function to handle uploaded file type missmatch
		 * @function handleTypeMissmatch @ parameters oEvent
		 */
		handleTypeMissmatch: function (oEvent) {
			var aFileTypes = oEvent.getSource().getFileType();
			var i18nResourceBundle = this.getView().getModel("i18n");
			jQuery.each(aFileTypes, function (key, value) {
				aFileTypes[key] = "*." + value;
			});
			var sSupportedFileTypes = aFileTypes.join(", ");
			var jsonResponseText = '{"fault":{"faultstring": "' + i18nResourceBundle.getProperty("WarningMessageFileType") +
				'","detail":{"errorcode": "' + i18nResourceBundle.getProperty("WarningCodeFileType") + '"}}}';
			this.onShowMessage(this, "Error", i18nResourceBundle.getProperty("WarningCodeFileType"),
				i18nResourceBundle.getProperty("WarningDescriptionFileType1") + oEvent.getParameter("fileType") + " " +
				i18nResourceBundle.getProperty("WarningDescriptionFileType2") + sSupportedFileTypes, jsonResponseText, null);
		}

	});
});