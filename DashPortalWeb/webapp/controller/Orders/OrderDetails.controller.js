sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/m/Button",
	"sap/m/Dialog"

], function (Base) {
	"use strict";
	var _oView, _oPrintContentHolder, _oController;
	var _aCarrier = [];
	var _sUPSCarrier;
	var _sUPSDate;
	var _sUPSTime;
	var _aValidChampions = [];

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Orders.OrderDetails", {
		/** @module OrderDetails */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oView = this.getView();
			_oController = this;

			sap.ui.getCore().byId("__xmlview0--vbox_background_main").setVisible(false);
			sap.ui.getCore().byId("__xmlview0--menuMobileHeader").setVisible(false);

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("orderDetails").attachPatternMatched(this._onObjectMatched, this);

			// detect IE8+ and edge browsers to define and attach printing events
			if (document.documentMode || /Edge/.test(navigator.userAgent)) {
				var beforePrint = function () {
					//Adding the temporary printing page elements and detaching the original content from the DOM
					var sIdPageTitleContainer = _oView.byId("pageTitleContainerOrderDetails").getId();
					var sIdOrderHeaderItems = _oView.byId("orderHeaderItems").getId();
					var sIdTable = _oView.byId("idOrdersList").getId();

					$('#' + sIdPageTitleContainer).clone().attr("id", "tempPrintPageTitleContainer").insertBefore('#content');
					$('#' + sIdOrderHeaderItems).clone().attr("id", "tempPrintOrderHeaderItems").insertBefore('#content');
					$('#' + sIdTable).clone().attr("id", "tempPrintTable").insertBefore('#content');
					_oPrintContentHolder = $('#content').detach();
				};
				var afterPrint = function () {
					//Removing the temporary printing page elements and placing the original content back in the DOM
					_oPrintContentHolder.appendTo("body");
					$('#tempPrintPageTitleContainer').remove();
					$('#tempPrintOrderHeaderItems').remove();
					$('#tempPrintTable').remove();
				};
				//attaching actions to the printing events
				window.onbeforeprint = beforePrint;
				window.onafterprint = afterPrint;
			}
		},

		/**
		 * Get the parameters passed from Main Orders Page and passed it to the initialization of local model.
		 * @function _onObjectMatched
		 * @param {Object} oEvent -  triggered when navigating to OrderDetails view
		 */
		_onObjectMatched: function (oEvent) {

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("ORDERSTATUS_PAGE")) {
				return;
			}
			var oSVGS = new sap.ui.model.json.JSONModel({
				"inProcess": _oController.getKey("keys_svg", "keyShape_inProcess"),
				"orderReceived": _oController.getKey("keys_svg", "keyShape_orderReceived"),
				"shipped": _oController.getKey("keys_svg", "keyShape_shipped"),
				"rightArrow": _oController.getKey("keys_svg", "keyShape_rightArrow")
			});
			_oView.setModel(oSVGS, "oSVGS");

			this.displayLayoutAccess();

			//set language to page
			var language = oEvent.getParameter("arguments").language;
			sap.ui.getCore().getConfiguration().setLanguage(language);

			var orderUUID = oEvent.getParameter("arguments").orderUUID;
			this.initLocalModel(orderUUID);

			_aValidChampions = _oController.getGlobalParameter("urlTracking");

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "orders"
			});
		},

		/**
		 * Configure sidebar icons to display/hide
		 * @function displayLayoutAccess
		 */
		displayLayoutAccess: function () {
			this.getView().byId("orderDetailsSubtotals").setVisible(this.checkDASHLayoutAccess("ORDERDETAILS_SUBTOTALS"));
			this.getView().byId("orderDetailsPrice").setVisible(this.checkDASHLayoutAccess("ORDERDETAILS_SUBTOTALS"));
		},

		/**
		 * Function that handles the success case of the ajax call that loads data to the initial order details model
		 * @function onInitLocalModelSuccess
		 */
		onInitLocalModelSuccess: function (oController, oData, oPiggyBack) {

			var busyContainer = oController.getView().byId("contentContainer");
			if (oData) {

				_sUPSCarrier = oData.carrierName;
				_sUPSDate = oData.estimatedDeliveryDate;
				_sUPSTime = oData.estimatedDeliveryTime;

				var modelHeader = {
					"Currency": oData.products[0].currency,
					"EstimatedDeliveryDate": oData.estimatedDeliveryDate,
					"EstimatedDeliveryTime": oData.estimatedDeliveryTime,
					"OrderNumber": oData.orderNumber,
					"ShipToPartyID": oData.shipToPartyID,
					"OrderDate": oData.orderDate,
					"OrderTime": oData.orderTime,
					"HeaderStatus": oData.headerStatus
				};

				var oSQL = new sap.ui.model.json.JSONModel(modelHeader);
				_oView.setModel(oSQL, "HeaderItems");

				var modelProductItems = [];
				var itemStatus;
				var upsInfoFetchFlag = false;
				for (var i = 0; i < oData.products.length; i++) {

					if (oData.products[i].itemStatus === "") {
						itemStatus = 'UNAVAILABLE';
					} else {
						itemStatus = oData.products[i].itemStatus;
					}

					modelProductItems.push({
						"OrderQuantity": oData.products[i].orderQuantity,
						"OrderQuantityUnit": oData.products[i].orderQuantityUnit,
						"Product": oData.products[i].product,
						"ProductName": oData.products[i].productName,
						"ScheduleLineSalesUnit": oData.products[i].scheduleLineSalesUnit,
						"ConfirmedQty": oData.products[i].confirmedQty,
						"ItemStatus": itemStatus,
						"NetValue": oData.products[i].netValue,
						"Price": oData.products[i].netPrice,
						"NetPriceUOM": oData.products[i].netPriceUOM,
						"CarrierTrackingStatus": _oController.getCarrierStatus(oData.products[i].carrierTracking, oData.carrier), // Set tracking status if carrier tracking available
						"CarrierTracking": oData.products[i].carrierTracking

					});

					/*
						Determine if Tracking info is available, then set flag to fetch UPS tracking info from DB
					*/
					if (!upsInfoFetchFlag && modelProductItems[i].CarrierTrackingStatus) {
						upsInfoFetchFlag = true;
					}
				}

				/* 
					Fetch UPS Tracking Info Params from DB 
				*/
				var trackingControl;

				if (upsInfoFetchFlag) {
					if (oData.carrier) {
						trackingControl = _oController.getGlobalParametersByGroup(oData.carrier);
					} else {
						var oValidChampion;
						for (var j in _aCarrier) {
							oValidChampion = _oController.getGlobalParametersByGroup(_aCarrier[j]);
							if (oValidChampion.urlTracking) {
								trackingControl = oValidChampion;
								break;
							}
						}
					}
					if (trackingControl) {
						trackingControl.upsTrackingFlag = (trackingControl.urlTracking) ? true : false;
						var sysType = sap.ui.Device.system;
						if (sysType.phone) {
							trackingControl.phone = true;
						} else {
							trackingControl.phone = false;
						}
					}
				}

				if (!trackingControl) {
					trackingControl = {
						"upsTrackingFlag": true
					};
				}

				var upsModel = new sap.ui.model.json.JSONModel(trackingControl);
				_oView.setModel(upsModel, "upsInfoModel");

				var oSQL2 = new sap.ui.model.json.JSONModel(modelProductItems);
				_oView.setModel(oSQL2, "ProductItems");

				/* 
					Logic for displaying Date range if it exist 
				*/
				var oModel = _oView.getModel("ProductItems").getData();
				var sLen = oModel.length;
				var sCheck;
				var aCompare = [];
				for (var l = 0; l < sLen; l++) {
					for (var k = 0; k < oModel[l].CarrierTracking.length; k++) {
						sCheck = _oController.removenonCarChamp(oModel[l].CarrierTracking[k].carrier);
						if (sCheck === true) {
							aCompare.push(parseInt(oModel[l].CarrierTracking[k].estimatedDeliveryDate));
						}
					}
				}
				if (aCompare.length !== 0) {
					var sMinDateInt = Math.min.apply(null, aCompare);
					var sMaxDateInt = Math.max.apply(null, aCompare);
				}
				var oModelHeader = _oView.getModel("HeaderItems").getData();
				var sDate = oModelHeader.EstimatedDeliveryDate;
				var oLocale, slocaleID, dateUTCString, sTime;
				/* 
					Logic for displaying Date if Estimated Delivary date and Carrier exist 
				*/
				if (oData.carrier !== "" || oData.estimatedDeliveryDate !== "") {
					if (_sUPSTime === "" || _sUPSTime === "000000") {
						sTime = "T12:00:00";
						dateUTCString = sDate.concat(sTime, "Z");
					} else {
						dateUTCString = sDate.concat(_sUPSTime, "Z");
					}
					var newdate = new Date(dateUTCString);
					var localDate = new Date(newdate.valueOf() - (newdate.getTimezoneOffset()) * 60000);
					oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
					slocaleID = ((oLocale.sLocaleId === "en") ? "en-US" : oLocale.sLocaleId);
					var dateFormatted = localDate.toLocaleDateString(slocaleID);
					_oController.getView().byId("EDDField").setText(dateFormatted);
				}

				var oResponseMin, sMinDate, dateFormattedMin, oResponseMax, sMaxDate, dateFormattedMax;
				/* 
					Logic for displaying Date if Estimated Delivary date exist but no Carrier is available 
				*/
				if (oData.carrier === "" && oData.estimatedDeliveryDate !== "") {
					if (sMinDateInt === undefined || sMaxDateInt === undefined || sMinDateInt === "" || sMaxDateInt === "") {
						_oController.getView().byId("EDDField").setText("");
					} else {
						oResponseMin = _oController.formatDateEstOrdTrackDetailsChamp(sMinDateInt);
						sMinDate = oResponseMin.date || null;
						dateFormattedMin = oResponseMin.dateFormatted || null;

						oResponseMax = _oController.formatDateEstOrdTrackDetailsChamp(sMaxDateInt);
						sMaxDate = oResponseMax.date || null;
						dateFormattedMax = oResponseMax.dateFormatted || null;
					}
				}
				/* 
					Logic for displaying Date if Estimated Delivary date exist but Carrier is neither UPS nor a valid champion carrier 
				*/
				if (oData.carrier === "" && oData.estimatedDeliveryDate !== "" && sMinDateInt === undefined && sMaxDateInt === undefined) {

					if (_sUPSTime === "" || _sUPSTime === "000000") {
						_sUPSTime = "T12:00:00";
					}
					var dateUTCStringEst = sDate.concat(_sUPSTime, "Z");
					var newdateEst = new Date(dateUTCStringEst);
					var localDateEst = new Date(newdateEst.valueOf() - (newdateEst.getTimezoneOffset()) * 60000);
					oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
					slocaleID = ((oLocale.sLocaleId === "en") ? "en-US" : oLocale.sLocaleId);
					var dateFormattedEst = localDateEst.toLocaleDateString(slocaleID);
					_oController.getView().byId("EDDField").setText(dateFormattedEst);

					if (oData.estimatedDeliveryDate === "UNCONFIRMED") {
						_oController.getView().byId("EDDField").setText("");
					}
				}

				if (sMaxDate && sMinDate) {
					var oFinMaxDate = dateFormattedMax;
					var sFinMinDate = dateFormattedMin;
				}
				if (oFinMaxDate) {
					var sDateFinal = sFinMinDate + " - " + oFinMaxDate;
					_oController.getView().byId("EDDField").setText(sDateFinal);
				} else if (sFinMinDate) {
					_oController.getView().byId("EDDField").setText(sFinMinDate);
				}

			}

			var modelProducts = {
				"City": oData.shippingAddress.city,
				"Country": oData.shippingAddress.country,
				"HouseNumber": oData.shippingAddress.houseNumber,
				"Name": oData.shippingAddress.name,
				"Number": oData.shippingAddress.number,
				"PostalCodeStreet": oData.shippingAddress.postalCodeStreet,
				"State": oData.shippingAddress.state,
				"Street": oData.shippingAddress.street

			};
			var oSQL3 = new sap.ui.model.json.JSONModel(modelProducts);
			_oView.setModel(oSQL3, "ShippingAddress");

			var modelPONumber = {
				"PONumber": oData.ponumber,
				"ShipsFrom": oData.provider
			};
			var oSQL4 = new sap.ui.model.json.JSONModel(modelPONumber);
			_oView.setModel(oSQL4, "OrderMainInfo");

			var oProvider = {
				Visible: true
			};
			var oProviderrModel = new sap.ui.model.json.JSONModel(oProvider);
			_oView.setModel(oProviderrModel, "Provider");

			if (oData.provider === "") {
				_oView.getModel("Provider").setProperty("/Visible", false);
			} else {
				_oView.getModel("Provider").setProperty("/Visible", true);
			}

			if (oData.products[0].itemStatus === "CANCELLED") {
				_oView.byId("EDDContainer").setVisible(false);
			}

			busyContainer.setBusy(false);
			//Intended nesting of timeout 0. To ensure that in the ipad the scroll to top works properly
			setTimeout(function () {
				setTimeout(function () {
					var oScrollElemnt = _oController.getScrollElement();
					if (oScrollElemnt.element === "") {
						//Aplied in pages that have a specific scroll container, ex:feed lazyloading container
						$("#" + oScrollElemnt.containerId).animate({
							scrollTop: 0
						}, 0);
					} else {
						oScrollElemnt.element.scrollTo(0, 0);
					}
				}, 0);
			}, 0);

		},

		/**
		 * Function that handles the error case of the ajax call that loads data to the initial order details model
		 * @function onInitLocalModelError
		 */
		onInitLocalModelError: function (oController, oError, oPiggyBack) {
			var busyContainer = oController.getView().byId("contentContainer");
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText);
			busyContainer.setBusy(false);
		},

		/**
		 * Initialization of local modelfor order details.
		 * @function initLocalModel
		 * @param {array} oData -  order details data
		 */
		initLocalModel: function (orderUUID) {

			// set Busy indicator to header
			var busyContainer = this.getView().byId("contentContainer");
			busyContainer.setBusyIndicatorDelay(0);
			busyContainer.setBusy(true);

			var controller = this;

			var xsURL = "/orders/orderDetail?orderUUID=" + orderUUID;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", controller.onInitLocalModelSuccess, controller.onInitLocalModelError);
		},

		/**
		 * Date formatted to MM/DD/YY.
		 * @function formatDate 
		 * @param {string} sDate - A string with a date info
		 * @returns Formatted date
		 */
		formatDate: function (sDate) {
			if (!sDate || sDate === "UNCONFIRMED") {
				return "";
			} else {
				var day = sDate.split("-")[2];
				var month = sDate.split("-")[1];
				var year = sDate.split("-")[0];
				return this.pad(month, 2) + "/" + this.pad(day, 2) + "/" + year.toString().substring(2, 4);
			}
		},

		/**
		 * Date formatted to MM/DD/YY.
		 * @function formatDateEstOrd
		 * @param {string} sDate - A string with a date info
		 * @param {string} sTime - A string with a time info
		 * @returns Formatted date
		 */
		formatDateEstOrd: function (sDate, sTime) {
			if (!sDate || sDate === "UNCONFIRMED") {
				return "";
			} else {
				if (!sTime || sTime === "000000") {
					sTime = "T12:00:00";
				}
				var dateUTCString = sDate.concat(sTime, "Z");
				var newdate = new Date(dateUTCString);
				var localDate = new Date(newdate.valueOf() - (newdate.getTimezoneOffset()) * 60000);
				var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
					pattern: "MM/dd/YY"
				});
				var dateFormatted = dateFormat.format(localDate);
				return dateFormatted;
			}
		},

		/**
		 * Date formatted to MM/DD/YY.
		 * @function formatDateEstOrdTrackDetailsChamp
		 * @param {string} sDate - A string with a date info
		 * @returns Formatted date
		 */
		formatDateEstOrdTrackDetailsChamp: function (sDateInt) {
			var oResponse = {};
			if (sDateInt !== undefined && !isNaN(sDateInt)) {
				var sDate = sDateInt.toString();
				var sTime = "T12:00:00";

				var sDay = sDate.substring(6, 8);
				var sMonth = sDate.substring(4, 6);
				var sYear = sDate.substring(0, 4);
				sDate = sYear + "-" + sMonth + "-" + sDay;
				var sDateUTC = sDate.concat(sTime, "Z");
				var newdate = new Date(sDateUTC);
				var localDate = new Date(newdate.valueOf() - (newdate.getTimezoneOffset()) * 60000);
				var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
				var slocaleID = ((oLocale.sLocaleId === "en") ? "en-US" : oLocale.sLocaleId);
				var sDateResponse = localDate.toLocaleDateString(slocaleID);
				oResponse.dateFormatted = sDateResponse;
				oResponse.date = sDate;
			}
			return oResponse;
		},

		/**
		 * Date formatted to MM/DD/YY.
		 * @function formatDateEstDeli
		 * @param {string} sDate - A string with a date info
		 * @returns Formatted date
		 */
		formatDateEstDeli: function (sDate) {

			var dateUTCStringUPS, newdateUPS, localDateUPS;
			var dateFormattedUPS, sDay, sMonth, sYear, sFinalDate, oLocale, slocaleID;

			if (_sUPSDate !== "") {
				if (_sUPSDate === "UNCONFIRMED" && sDate === "") {
					return "";
				}
				if (_sUPSTime === "" || _sUPSTime === "000000") {
					_sUPSTime = "T12:00:00";
				}
				if (_sUPSDate === "UNCONFIRMED" || sDate !== "") {
					sDay = sDate.substring(6, 8);
					sMonth = sDate.substring(4, 6);
					sYear = sDate.substring(0, 4);
					sFinalDate = sYear + "-" + sMonth + "-" + sDay;
					dateUTCStringUPS = sFinalDate.concat(_sUPSTime, "Z");
					newdateUPS = new Date(dateUTCStringUPS);
					localDateUPS = new Date(newdateUPS.valueOf() - (newdateUPS.getTimezoneOffset()) * 60000);
					oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
					slocaleID = ((oLocale.sLocaleId === "en") ? "en-US" : oLocale.sLocaleId);
					dateFormattedUPS = localDateUPS.toLocaleDateString(slocaleID);
					return dateFormattedUPS;
				} else { //(_sUPSDate !== "UNCONFIRMED" || sDate === "")
					dateUTCStringUPS = _sUPSDate.concat(_sUPSTime, "Z");
					newdateUPS = new Date(dateUTCStringUPS);
					localDateUPS = new Date(newdateUPS.valueOf() - (newdateUPS.getTimezoneOffset()) * 60000);
					oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
					slocaleID = ((oLocale.sLocaleId === "en") ? "en-US" : oLocale.sLocaleId);
					dateFormattedUPS = localDateUPS.toLocaleDateString(slocaleID);
					return dateFormattedUPS;
				}
			}
			return sDate;

		},

		/**
		 * Formatter for Carrier Code. Hide the Carrier if its not valid Champion carrier
		 * @function removenonCarChamp
		 * @param {string} sCarrier - A string with a Carrier Code
		 * @returns Bollean Value
		 */

		removenonCarChamp: function (sCarrier) {
			if (sCarrier) {
				for (var i = 0; i < _aValidChampions.length; i++) {
					if (sCarrier === _aValidChampions[i].groupID) {
						return true;
					}
				}
				return false;
			}
			return true;
		},

		/**
		 * Formatter for UPS Carrier Code. Show the UPS Carrier Name if it is Valvoline UPS carrier
		 * @function removenonCarChamp
		 * @param {string} sUPS - A string with a Carrier Code
		 * @returns Bollean Value
		 */

		formatUpsCarrier: function (sLabel, sCarrier) {

			if (_sUPSCarrier === "") {
				var sNonUPS = sCarrier;
				return sLabel + " " + sNonUPS;
			} else if (_sUPSCarrier !== "") {
				var sUPS = _sUPSCarrier;
				return sLabel + " " + sUPS;
			}

		},

		/**
		 * Date formatted to MM/DD/YY.
		 * @function formatDateOrd
		 * @param {string} sDate - A string with a date info
		 * @param {string} sTime - A string with a time info
		 * @returns Formatted date
		 */
		formatDateOrd: function (sDate, sTime) {

			if (!sDate || sDate === "UNCONFIRMED") {
				return "";
			} else {
				if (!sTime) {
					sTime = "T12:00:00";
				} else {
					sTime = "".concat("T", sTime);
				}

				var dateUTCString = sDate.concat(sTime, "Z");
				var newdate = new Date(dateUTCString);
				var localDate = new Date(newdate.valueOf() - (newdate.getTimezoneOffset()) * 60000);
				var oLocale = new sap.ui.core.Locale(sap.ui.getCore().getConfiguration().getLanguage());
				var slocaleID = ((oLocale.sLocaleId === "en") ? "en-US" : oLocale.sLocaleId);
				var datestring = localDate.toLocaleDateString(slocaleID);
				return datestring;
			}

		},

		/**
		 * Formatter for PONumber. Disables the PONumber if its empty
		 * @function formatPONumber
		 * @param {string} sPONumber - A string with a PONumber
		 * @returns PO Number formatted
		 */
		formatPONumber: function (sPONumber) {
			if (!sPONumber) {
				this.getView().byId("PONumber").setVisible(false);
			} else {
				this.getView().byId("PONumber").setVisible(true);
				return sPONumber;
			}
		},

		/**
		 * Returns order status "SHIPPED" as "Shipped" or "Expected"
		 * @function formatShipped
		 * @param  {string} estDelDate - estimated delivery date
		 * @param {string} headerstatus - order status
		 */
		formatShipped: function (estDelDate, headerstatus) {
			var estDate = new Date(estDelDate);
			var headerStatus = headerstatus;

			if (headerStatus === "SHIPPED" && !isNaN(estDate.getTime()) && estDate <= new Date()) {
				return this.getResourceBundle().getText("MAORshippedLB");
			} else {
				return this.getResourceBundle().getText("MAORDshippedTXT");
			}
		},

		/**
		 *	Function to open UPS tracking in a separate tab
		 * @function onTrackingURLPress
		 */
		onTrackingURLPress: function (oEvt) {
			var trackingURL;
			var trackingNum = oEvt.getSource().getText();
			var sCarrier = oEvt.getSource().getCustomData()[0].getValue();
			var trackingInfoCheck = _oController.getGlobalParametersByGroup(sCarrier);

			var upsInfoData = this.getModel("upsInfoModel").getData();
			if (trackingInfoCheck.urlTrackingNumberParameter) {
				trackingURL = trackingInfoCheck.urlTracking + trackingInfoCheck.urlTrackingNumberParameter + trackingNum;
			} else if (trackingInfoCheck.urlTrackingNumberParameter === undefined) {
				if (upsInfoData.urlTrackingNumberParameter) {
					trackingURL = upsInfoData.urlTracking + upsInfoData.urlTrackingNumberParameter + trackingNum;
				} else {
					trackingURL = trackingInfoCheck.urlTracking;
				}
			}
			if (upsInfoData.urlLocaleParameter) {
				var deviceLocale = sap.ui.getCore().getConfiguration().getLanguage();
				trackingURL = trackingURL + "&" + upsInfoData.urlLocaleParameter + deviceLocale;

			}
			window.open(trackingURL, "_blank");
			// GTM
			_oController.GTMDataLayer('ordertrckEvent',
				'Order Status',
				'Tracking ID -linkclick',
				'Tracking Details -ID click'
			);

		},
		/**
		 *	Function to Check the valid carrier champion
		 * @function getCarrierStatus
		 */
		getCarrierStatus: function (aData, Carrier) {
			var sCarrier;
			if (aData && aData.length) {
				for (var i = 0; i < aData.length; i++) {
					sCarrier = "";
					if (aData[i].carrier) {
						sCarrier = aData[i].carrier;
					} else if (Carrier) {
						sCarrier = Carrier;
					}
					if (sCarrier) {
						for (var j = 0; j < _aValidChampions.length; j++) {
							if (sCarrier === _aValidChampions[j].groupID) {
								return true;
							}
						}
					}
				}
				return false;
			} else {
				return false;
			}
		},

		/**
		 *	Deppending on the status shows or hides shapes
		 * @function showShapes
		 */
		showShapes: function (sStatus) {
			if (sStatus === "RECEIVED" || sStatus === "IN PROCESS" || sStatus === "SHIPPED") {
				return true;
			}
			return false;
		}
	});
});