sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function (Base) {
	"use strict";
	var _oRouter, _oView, _oController, _oAccount, i, _iLengthActive, _iLengthCompleted, _oOrderMdlActive, _oOrderMdlCompleted,
		_beginActive, _endActive, _endCompleted, _beginCompleted, _resultsCompleted, _resultsActive, _resultsCompletedModel,
		_resultsActiveModel, _totalResultsActive, _totalResultsCompleted,
		_sortedActive, _sortedCompleted, _sortedActiveEstDel, _sSearch, _selectedActvResult, _selectedCompResult;
	var _1stRender = true;
	var _resultsList = [5, 10, 50, 100, 200];
	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Orders.OrderStatus", {
		/** @module OrderStatus */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			_oView = this.getView();
			_oController = this;
			_oRouter = this.getRouter();
			_oRouter.getRoute("orderStatus").attachPatternMatched(this._onObjectMatched, this);
		},
		/**
		 * If it is the first render, calls the the function to attach the focus out Events
		 * @function _onObjectMatched
		 **/
		onBeforeRendering: function () {
			if (_1stRender) {
				setTimeout(function () {
					_oController.inputSearchOrderFunctions();
				}, 0);
				_1stRender = false;
			}
		},

		/**
		 * Initializes the model. Makes an element in the filter disabled (if applicable).
		 * @function onAfterRendering
		 */
		onAfterRendering: function () {
			/* Set the menu as active if the app is reloaded */
			$(".js-orders-sidenav").addClass("sidenav__item--active");
		},

		/**
		 * Set the filter for the selected account and initialize order tables
		 * @function _onObjectMatched
		 * @param {Object} oEvent - Triggered when navigating to Orders view
		 */
		_onObjectMatched: function () {
			// Clears all error message
			this.clearServerErrorMsg(this);

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("ORDERSTATUS_PAGE")) {
				return;
			}
			var oSVGS = new sap.ui.model.json.JSONModel({
				"inProcess": _oController.getKey("keys_svg", "keyShape_inProcess"),
				"orderReceived": _oController.getKey("keys_svg", "keyShape_orderReceived"),
				"shipped": _oController.getKey("keys_svg", "keyShape_shipped"),
				"rightArrow": _oController.getKey("keys_svg", "keyShape_rightArrow")
			});
			_oView.setModel(oSVGS, "oSVGS");

			if (sap.ui.getCore().getModel("oAccountsByDistrict")) {
				_oController.reconfigAddressLabel();
			}

			//get user info and direct account
			_oController.getUserInformation(_oController, this.toRunAfterGettingUserInfo);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			// Sets the model oAccountsByDistrict
			this.getAccountsByDistrict(this, "comboBoxAccountOrders", false, this.reconfigAddressLabel);

			//reset sort status and filters
			_sortedActive = "down";
			_sortedCompleted = "down";
			_sortedActiveEstDel = "both";

			if (_oController.getView().byId("sortButton_ActiveOrders")) {
				_oController.getView().byId("sortButton_ActiveOrders").setSrc("resources/icon/sort_desc.png");
			}
			if (_oController.getView().byId("sortButton_CompletedOrders")) {
				_oController.getView().byId("sortButton_CompletedOrders").setSrc("resources/icon/sort_desc.png");
			}

			if (_oController.getView().byId("sortButtonEstDel_ActiveOrders")) {
				_oController.getView().byId("sortButtonEstDel_ActiveOrders").setSrc("resources/icon/sort_both.png");
			}

			var today = new Date();
			_oController.getView().byId("filterOrdersDesktop").setValue("");

			_oController.getView().byId("dateRangeSelectionDesktop").setValue("");
			_oController.getView().byId("dateRangeSelectionDesktop").setMaxDate(new Date(today.toDateString()));
			_oController.getView().byId("dateRangeSelectionDesktop").addEventDelegate({
				onAfterRendering: function () {
					var oDateInner = this.$().find('.sapMInputBaseInner');
					var oID = oDateInner[0].id;
					$('#' + oID).attr("disabled", "disabled");
				}
			}, _oController.getView().byId("dateRangeSelectionDesktop"));

			this.headerIconAccess();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "orders"
			});
			_oView.byId("accountInfoContainerDesktop").setVisible(false);

			_totalResultsActive = new sap.ui.model.json.JSONModel();
			_totalResultsCompleted = new sap.ui.model.json.JSONModel();

			setTimeout(_oController.removeActiveFiltersClasses(), 0);
		},

		/**
		 * This function resets the AddressLabel when you navigate to the page.
		 * @function reconfigAddressLabel
		 */
		reconfigAddressLabel: function () {
			_oController.getView().byId("comboBoxAccountOrders").setSelectedKey(0);
			_oAccount = _oController.findAccountByDistrict("0", _oController);
			sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
			_oController.getView().byId("accountInfoContainerDesktop").setVisible(false);
		},

		/**
		 * This function handles the logic that requires the user information to be available.
		 * @function toRunAfterGettingUserInfo
		 * @param {object} controller - the page context
		 */
		toRunAfterGettingUserInfo: function (controller) {
			if (sap.ui.getCore().getModel("oAccounts")) {
				var oAccount;
				controller.removeActiveFiltersClasses();
				if (sap.ui.getCore().getModel("oSelectedAccount") === undefined) {
					//set selected account to a fake account
					var fakeAccount = {
						id: "0",
						billingcity: _oController.getResourceBundle().getText("MAresultsTXT")
					};
					oAccount = new sap.ui.model.json.JSONModel(fakeAccount);
					sap.ui.getCore().setModel(oAccount, "oSelectedAccount");
					_oView.setModel(oAccount, "oSelectedAccount");
					controller.initializeTables("0", "0");
				} else {
					oAccount = sap.ui.getCore().getModel("oSelectedAccount").getData();
					sap.ui.getCore().getModel("oSelectedAccount").setData(oAccount);
					_oView.setModel(sap.ui.getCore().getModel("oSelectedAccount"), "oSelectedAccount");
					var accountNum, districtFlag;
					_oAccount = controller.findAccountByDistrict(oAccount.id, controller);
					if (_oAccount.id && _oAccount.id.indexOf("district") === -1) {
						accountNum = _oAccount.accountnum;
						districtFlag = "0";
					} else if (_oAccount.id && _oAccount.id.indexOf("district") !== -1) {
						accountNum = "0";
						districtFlag = _oAccount.id.replace(/district-/, "");
					} else {
						accountNum = "0";
						districtFlag = "0";
					}
					controller.initializeTables(accountNum, districtFlag);
				}
			} else {
				controller.getView().byId("vbox_Orders_container").setVisible(false);
			}
		},

		/**
		 * Function that removes the Css Classes on the order received/in process/shipped filters
		 * @function removeActiveFiltersClasses
		 */
		removeActiveFiltersClasses: function () {
			//Order Received
			$("#" + _oController.getView().byId("filterOrderReceivedIcon").getId()).removeClass("statusOrdersIcon");
			$("#" + _oController.getView().byId("filterOrderReceivedLink").getId()).removeClass("statusOrdersLabel").addClass("bodyCopy2");
			//In Process
			$("#" + _oController.getView().byId("filterInProcessIcon").getId()).removeClass("statusOrdersIcon");
			$("#" + _oController.getView().byId("filterInProcessLink").getId()).removeClass("statusOrdersLabel").addClass("bodyCopy2");
			//Shipped
			$("#" + _oController.getView().byId("filterShippedIcon").getId()).removeClass("statusOrdersIcon");
			$("#" + _oController.getView().byId("filterShippedLink").getId()).removeClass("statusOrdersLabel").addClass("bodyCopy2");
			$(':focus').blur();
		},

		/**
		 * Function that handles the success case of the ajax call that gets the data to initialyze the orders table
		 * @function onInitializeTablesSuccess
		 */
		onInitializeTablesSuccess: function (oController, oData, oPiggyBack) {
			_resultsActive = oData.activeOrders;
			_resultsActiveModel.setData(_resultsActive);

			_resultsCompleted = oData.completeOrders;
			_resultsCompletedModel.setData(_resultsCompleted);

			_iLengthActive = _resultsActive.length;
			var fstPageActive = [],
				k;
			_beginActive = 0;

			if (_iLengthActive > 5) {
				for (k = 0; k < 5; k++) {
					fstPageActive.push(_resultsActive[k]);
				}
				_oView.byId("idTextCount").setText(1 + "-" + 5 + " " + oController.getResourceBundle()
					.getText("of") + " " + _iLengthActive);
				_endActive = 5;

				_oView.byId("idActiveNav").setVisible(true);
				_oView.byId("idRightNav").setEnabled(true);
				_oView.byId("actvOrdersResultsPerPage").setVisible(true);
				_oController.removeSearchResultsStyleClass("actvOrdersResults", _resultsList, _oView);
				_oView.byId("actvOrdersResults5").addStyleClass("numberOfResultsLinkSelected");
			} else {
				_endActive = _iLengthActive;
				for (k = 0; k < _iLengthActive; k++) {
					fstPageActive.push(_resultsActive[k]);
				}
				if (_iLengthActive === 0) {
					//none
				} else {
					_oView.byId("idRightNav").setEnabled(false);
					_oView.byId("idTextCount").setText(1 + "-" + _iLengthActive + " " + oController.getResourceBundle()
						.getText("of") + " " + _iLengthActive);

					_oView.byId("idActiveNav").setVisible(true);
				}
				_oView.byId("actvOrdersResultsPerPage").setVisible(false);
			}

			_oView.byId("actvOrdersResults50").setVisible(_iLengthActive > 10);
			_oView.byId("actvOrdersResults100").setVisible(_iLengthActive > 50);
			_oView.byId("actvOrdersResults200").setVisible(_iLengthActive > 100);

			var oOrderMdlActivePag = new sap.ui.model.json.JSONModel();
			oOrderMdlActivePag.setData(fstPageActive);

			sap.ui.getCore().setModel(oOrderMdlActivePag, "oOrdersActivePaginated");
			oController.getView().setModel(oOrderMdlActivePag, "oOrdersActivePaginated");

			//create first page completed
			_iLengthCompleted = _resultsCompleted.length;

			var fstPageCompleted = [],
				t;
			_beginCompleted = 0;
			if (_iLengthCompleted > 5) {
				_endCompleted = 5;
				for (t = 0; t < 5; t++) {
					fstPageCompleted.push(_resultsCompleted[t]);
				}
				_oView.byId("idTextCountCompleted").setText(1 + "-" + 5 + " " + _oController.getResourceBundle()
					.getText("of") + " " + _iLengthCompleted);

				_oView.byId("idCompletedNav").setVisible(true);
				_oView.byId("idRightNavCompleted").setEnabled(true);
				_oView.byId("compOrdersResultsPerPage").setVisible(true);
				_oController.removeSearchResultsStyleClass("compOrdersResults", _resultsList, _oView);
				_oView.byId("compOrdersResults5").addStyleClass("numberOfResultsLinkSelected");
			} else {
				_endCompleted = _iLengthCompleted;
				for (t = 0; t < _iLengthCompleted; t++) {
					fstPageCompleted.push(_resultsCompleted[t]);
				}
				if (_iLengthCompleted === 0) {
					//None
				} else {
					_oView.byId("idRightNavCompleted").setEnabled(false);
					_oView.byId("idTextCountCompleted").setText(1 + "-" + _iLengthCompleted + " " + _oController.getResourceBundle()
						.getText("of") + " " + _iLengthCompleted);
					_oView.byId("idCompletedNav").setVisible(true);
				}
				_oView.byId("compOrdersResultsPerPage").setVisible(false);
			}

			_oView.byId("compOrdersResults50").setVisible(_iLengthCompleted > 10);
			_oView.byId("compOrdersResults100").setVisible(_iLengthCompleted > 50);
			_oView.byId("compOrdersResults200").setVisible(_iLengthCompleted > 100);

			var oOrderMdlCompletedPag = new sap.ui.model.json.JSONModel();
			oOrderMdlCompletedPag.setData(fstPageCompleted);

			sap.ui.getCore().setModel(oOrderMdlCompletedPag, "oOrdersCompletedPaginated");
			oController.getView().setModel(oOrderMdlCompletedPag, "oOrdersCompletedPaginated");

			_totalResultsActive.setData(oData.activeOrders);
			_totalResultsCompleted.setData(oData.completeOrders);

			_oOrderMdlActive.setData(_resultsActive);
			_oOrderMdlCompleted.setData(_resultsCompleted);

			sap.ui.getCore().setModel(_oOrderMdlActive, "oActiveOrders");
			sap.ui.getCore().setModel(_oOrderMdlCompleted, "oCompletedOrders");

			// disable or enable navigation arrows
			if (_iLengthActive === 0) {
				_oView.byId("idActiveNav").setVisible(false);
			}

			if (_iLengthCompleted === 0) {
				_oView.byId("idCompletedNav").setVisible(false);
			}

			_oView.byId("idLeftNav").setEnabled(false);
			_oView.byId("idRightNav").setEnabled(_iLengthActive > 5);

			_oView.byId("idLeftNavCompleted").setEnabled(false);
			_oView.byId("idRightNavCompleted").setEnabled(_iLengthCompleted > 5);

			_oView.byId("comboBoxAccountOrders").setEnabled(true);
			_oView.byId("vbox_Orders_container").setBusy(false);
		},

		/**
		 * Function that handles the error case of the ajax call that gets the data to initialyze the orders table
		 * @function onInitializeTablesError
		 */
		onInitializeTablesError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, _oController.getResourceBundle()
				.getText(
					"ERROrders"));
			_oView.byId("idActiveNav").setVisible(false);
			_oView.byId("idCompletedNav").setVisible(false);

			_oView.byId("idLeftNav").setEnabled(false);
			_oView.byId("idRightNav").setEnabled(false);

			_oView.byId("idLeftNavCompleted").setEnabled(false);
			_oView.byId("idRightNavCompleted").setEnabled(false);

			_oView.byId("comboBoxAccountOrders").setEnabled(true);
			_oView.byId("vbox_Orders_container").setBusy(false);
		},

		/**
		 * Populates active and completed orders.
		 * @function initializeTables
		 * @param oAcctNum - account's number
		 */
		initializeTables: function (oAcctNum, districtFlag) {
			_resultsCompleted = [];
			_resultsActive = [];

			_oOrderMdlActive = new sap.ui.model.json.JSONModel();
			_oOrderMdlCompleted = new sap.ui.model.json.JSONModel();
			_resultsCompletedModel = new sap.ui.model.json.JSONModel();
			_resultsActiveModel = new sap.ui.model.json.JSONModel();
			_beginActive = 0;
			_beginCompleted = 0;
			_resultsCompleted = [];
			_resultsActive = [];
			_selectedActvResult = 5;
			_selectedCompResult = 5;

			_oView.byId("idLeftNav").setEnabled(false);
			_oView.byId("idRightNav").setEnabled(false);

			_oView.byId("idLeftNavCompleted").setEnabled(false);
			_oView.byId("idRightNavCompleted").setEnabled(false);

			_oView.byId("actvOrdersResultsPerPage").setVisible(false);
			_oView.byId("compOrdersResultsPerPage").setVisible(false);

			_oView.byId("comboBoxAccountOrders").setEnabled(false);
			_oView.byId("vbox_Orders_container").setBusy(true);

			if (oAcctNum === _oController.getResourceBundle().getText("MAstoresTXT")) {
				oAcctNum = "0";
			}

			var queryParam;
			if (oAcctNum !== "0" && districtFlag === "0") {
				queryParam = "?accountnum=" + oAcctNum;
			} else if (oAcctNum === "0" && districtFlag !== "0") {
				queryParam = "?customerreportinggroup=" + districtFlag;
			} else {
				queryParam = "";
			}

			var controller = this;

			var xsURL = "/orders/orderStatusPage" + queryParam;
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", _oController.onInitializeTablesSuccess, controller.onInitializeTablesError);
		},

		/**
		 * Formatter for the order part number.
		 * @function formatPartNumber
		 * @param orderPart {string} - part number out the total order count number
		 * @param orderCount {string} - number of orders
		 */
		formatPartNumber: function (orderPart, orderCount) {
			var infoDisplayed;
			if (orderPart !== 1 || orderCount !== 1) {
				_oView.byId("PONumberBoxMobile").addStyleClass("sapUiTinyMarginTop");
				infoDisplayed = "(" + orderPart + " " + _oController.getResourceBundle().getText("of") + " " + orderCount + ")";
				return infoDisplayed;
			} else {
				_oView.byId("PONumberBoxMobile").removeStyleClass("sapUiTinyMarginTop");
			}
		},

		/**
		 * Navigate to right on active orders.
		 * @function toRightActive
		 */
		toRightActive: function () {
			var activeOrders = _resultsActive;
			var begin = _endActive;

			var end = begin + _selectedActvResult;

			_oView.byId("idLeftNav").setEnabled(true);
			if (end >= _iLengthActive) {
				end = _iLengthActive;
				_oView.byId("idRightNav").setEnabled(false);
			} else {
				_oView.byId("idRightNav").setEnabled(true);
			}

			_oView.byId("idTextCount").setText(begin + 1 + "-" + end + " " + _oController.getResourceBundle()
				.getText("of") + " " + _iLengthActive);

			var paginatedActive = [];
			for (i = begin; i < end; i++) {
				paginatedActive.push(activeOrders[i]);
			}
			_beginActive = begin;
			_endActive = end;

			var oOrderMdlActivePag = new sap.ui.model.json.JSONModel();
			oOrderMdlActivePag.setData(paginatedActive);
			if (_selectedActvResult > 100) {
				oOrderMdlActivePag.setSizeLimit(_selectedActvResult);
			}

			sap.ui.getCore().setModel(oOrderMdlActivePag, "oOrdersActivePaginated");
			_oView.setModel(oOrderMdlActivePag, "oOrdersActivePaginated");

			//GTM matches the orderNumber search
			_oController.GTMDataLayer('paginationclick',
				'Search',
				'OrderStatus Pagination click - ACTIVE',
				begin + 1 + '-' + end
			);
			var elmnt = _oView.byId("activeOrdersLabel");
			if (sap.ui.Device.system.phone === true) {
				setTimeout(function () {
					var b = $("#" + _oView.byId("idAct").getId());
					b[0].scrollIntoView({
						behavior: "smooth", // or "auto" or "instant"
						block: "start"
					});
				}, 0);
			} else if (sap.ui.Device.browser.internet_explorer === true || sap.ui.Device.system.tablet === true) {
				setTimeout(function () {
					var b = $("#" + _oView.byId("activeOrdersLabel").getId());
					b[0].scrollIntoView({
						behavior: "smooth", // or "auto" or "instant"
						block: "start"
					});
				}, 0);
			} else {
				_oController.getScrollElement().element.scrollToElement(elmnt);
			}

		},

		/**
		 * Navigate to left on active orders.
		 * @function toLeftActive
		 */
		toLeftActive: function () {
			var activeOrders = _resultsActive;

			var end = _beginActive;

			var begin = end - _selectedActvResult;

			_oView.byId("idRightNav").setEnabled(true);
			if (begin <= 0) {
				begin = 0;
				_oView.byId("idLeftNav").setEnabled(false);
			} else {
				_oView.byId("idLeftNav").setEnabled(true);
			}

			_oView.byId("idTextCount").setText(begin + 1 + "-" + end + " " + _oController.getResourceBundle()
				.getText("of") + " " + _iLengthActive);

			var paginatedActive = [];
			for (i = begin; i < end; i++) {
				paginatedActive.push(activeOrders[i]);
			}

			_beginActive = begin;
			_endActive = end;

			var oOrderMdlActivePag = new sap.ui.model.json.JSONModel();
			oOrderMdlActivePag.setData(paginatedActive);
			if (_selectedActvResult > 100) {
				oOrderMdlActivePag.setSizeLimit(_selectedActvResult);
			}

			sap.ui.getCore().setModel(oOrderMdlActivePag, "oOrdersActivePaginated");
			_oView.setModel(oOrderMdlActivePag, "oOrdersActivePaginated");

			//GTM matches the orderNumber search
			_oController.GTMDataLayer('paginationclick',
				'Search',
				'OrderStatus Pagination click - ACTIVE',
				begin + 1 + '-' + end
			);

			var elmnt = _oView.byId("activeOrdersLabel");

			if (sap.ui.Device.system.phone === true) {
				setTimeout(function () {
					var b = $("#" + _oView.byId("idAct").getId());
					b[0].scrollIntoView({
						behavior: "smooth", // or "auto" or "instant"
						block: "start"
					});
				}, 0);
			} else if (sap.ui.Device.browser.internet_explorer === true || sap.ui.Device.system.tablet === true) {
				setTimeout(function () {
					var b = $("#" + _oView.byId("activeOrdersLabel").getId());
					b[0].scrollIntoView({
						behavior: "smooth", // or "auto" or "instant"
						block: "start"
					});
				}, 0);
			} else {
				_oController.getScrollElement().element.scrollToElement(elmnt);
			}

		},

		/**
		 * Navigate to right on completed orders.
		 * @function toRightCompleted
		 */
		toRightCompleted: function () {
			var completedOrders = _resultsCompleted;
			var begin = _endCompleted;
			var end = begin + _selectedCompResult;

			_oView.byId("idLeftNavCompleted").setEnabled(true);
			if (end >= _iLengthCompleted) {
				end = _iLengthCompleted;
				_oView.byId("idRightNavCompleted").setEnabled(false);
			} else {
				_oView.byId("idRightNavCompleted").setEnabled(true);
			}

			_oView.byId("idTextCountCompleted").setText(begin + 1 + "-" + end + " " + _oController.getResourceBundle()
				.getText("of") + " " + _iLengthCompleted);

			var paginatedCompleted = [];
			for (i = begin; i < end; i++) {
				paginatedCompleted.push(completedOrders[i]);
			}

			_beginCompleted = begin;
			_endCompleted = end;

			var oOrderMdlCompletedPag = new sap.ui.model.json.JSONModel();
			oOrderMdlCompletedPag.setData(paginatedCompleted);
			if (_selectedCompResult > 100) {
				oOrderMdlCompletedPag.setSizeLimit(_selectedCompResult);
			}

			sap.ui.getCore().setModel(oOrderMdlCompletedPag, "oOrdersCompletedPaginated");
			_oView.setModel(oOrderMdlCompletedPag, "oOrdersCompletedPaginated");

			//GTM matches the orderNumber search
			_oController.GTMDataLayer('paginationclick',
				'Search',
				'OrderStatus Pagination click - COMPLETED',
				begin + 1 + '-' + end
			);

			var elmnt = _oView.byId("completedOrdersLabel");
			if (sap.ui.Device.system.phone === true) {
				setTimeout(function () {
					var b = $("#" + _oView.byId("actvOrdersResults200").getId());
					b[0].scrollIntoView({
						behavior: "smooth", // or "auto" or "instant"
						block: "start"
					});
				}, 0);
			} else if (sap.ui.Device.browser.internet_explorer === true || sap.ui.Device.system.tablet === true) {
				setTimeout(function () {
					var b = $("#" + _oView.byId("completedOrdersLabel").getId());
					b[0].scrollIntoView({
						behavior: "smooth", // or "auto" or "instant"
						block: "start"
					});
				}, 0);
			} else {
				_oController.getScrollElement().element.scrollToElement(elmnt);
			}
		},

		/**
		 * Navigate to left on completed orders.
		 * @function toLeftCompleted
		 */
		toLeftCompleted: function () {
			var completedOrders = _resultsCompleted;

			var end = _beginCompleted;
			var begin = end - _selectedCompResult;

			_oView.byId("idRightNavCompleted").setEnabled(true);
			if (begin <= 0) {
				begin = 0;
				_oView.byId("idLeftNavCompleted").setEnabled(false);
			} else {
				_oView.byId("idLeftNavCompleted").setEnabled(true);
			}

			_oView.byId("idTextCountCompleted").setText(begin + 1 + "-" + end + " " + _oController.getResourceBundle()
				.getText("of") + " " + _iLengthCompleted);

			var paginatedCompleted = [];
			for (i = begin; i < end; i++) {
				paginatedCompleted.push(completedOrders[i]);
			}

			_beginCompleted = begin;
			_endCompleted = end;

			var oOrderMdlCompletedPag = new sap.ui.model.json.JSONModel();
			oOrderMdlCompletedPag.setData(paginatedCompleted);
			if (_selectedCompResult > 100) {
				oOrderMdlCompletedPag.setSizeLimit(_selectedCompResult);
			}

			sap.ui.getCore().setModel(oOrderMdlCompletedPag, "oOrdersCompletedPaginated");
			_oView.setModel(oOrderMdlCompletedPag, "oOrdersCompletedPaginated");

			//GTM matches the orderNumber search
			_oController.GTMDataLayer('paginationclick',
				'Search',
				'OrderStatus Pagination click - COMPLETED',
				begin + 1 + '-' + end
			);
			var elmnt = _oView.byId("completedOrdersLabel");
			if (sap.ui.Device.system.phone === true) {
				setTimeout(function () {
					var b = $("#" + _oView.byId("actvOrdersResults200").getId());
					b[0].scrollIntoView({
						behavior: "smooth", // or "auto" or "instant"
						block: "start"
					});
				}, 0);
			} else if (sap.ui.Device.browser.internet_explorer === true || sap.ui.Device.system.tablet === true) {
				setTimeout(function () {
					var b = $("#" + _oView.byId("completedOrdersLabel").getId());
					b[0].scrollIntoView({
						behavior: "smooth", // or "auto" or "instant"
						block: "start"
					});
				}, 0);
			} else {
				_oController.getScrollElement().element.scrollToElement(elmnt);
			}
		},

		/**
		 * Loads page with the next active orders.
		 * @function fnNextPageActive
		 * @param {Object} oEvent - page to load
		 */
		fnNextPageActive: function (oEvent) {
			_oController.fnNextPage(oEvent, "oOrdersActivePaginated");
			//GTM
			_oController.GTMDataLayer('ordersearchrclck',
				'Search',
				'SearchOrder-Active-click',
				oEvent.getSource().getText().replace(_oController.getResourceBundle().getText("MAORorderNumberTXT") + " ", ""));
		},

		/**
		 * Loads page with the next completed orders.
		 * @function fnNextPageCompleted
		 * @param {Object} oEvent - page to load
		 */
		fnNextPageCompleted: function (oEvent) {
			_oController.fnNextPage(oEvent, "oOrdersCompletedPaginated");
			//GTM
			_oController.GTMDataLayer('ordersearchrclck',
				'Search',
				'SearchOrder-Completed-click',
				oEvent.getSource().getText().replace(_oController.getResourceBundle().getText("MAORorderNumberTXT") + " ", "")
			);
		},

		/**
		 * Loads page with the next orders.
		 * @function fnNextPage
		 * @param {Object} oEvent - page to load
		 */
		fnNextPage: function (oEvent, oModel) {
			// Checks Session time out
			this.checkSessionTimeout();
			//Get the selected Item from Binding Context of Model oOrderHeaders
			var oSelItem = oEvent.getSource().getBindingContext(oModel).getObject();

			//Complete the needed URL using getURL function of Router, and align this to the Routing that is initialized on manifest.json
			var sUrl = this.getRouter().getURL("orderDetails", {
				orderUUID: oSelItem.uuid,
				language: sap.ui.getCore().getConfiguration().getLanguage()
			});

			//Execute a window.open to create a new window, Add a # before the URL to use the Mother Url
			window.open("#" + sUrl);
		},

		/**
		 * Sets estimated delivery date string
		 * @function formatEstDateLbl
		 * @param  {string} sDate- estimated delivery date
		 * @returns Est. Delivery Date string
		 */
		formatEstDateLbl: function (sDate) {
			var sNewDate;
			if (sDate === null) {
				sNewDate = "";
			} else {
				sNewDate = "Est. Delivery Date";
			}
			return sNewDate;
		},

		/**
		 * Defines is the carrier should be visible or not
		 * @function formatterCarrierVisible
		 * @param {string} headerStatus - order's header status
		 */
		formatterCarrierVisible: function (headerStatus) {
			return (headerStatus === "SHIPPED");
		},

		/**
		 * Change the orders selection by changing the selection on the combobox.
		 * @function onChangeAccountComboBox
		 */
		onChangeAccountComboBox: function (oEvent) {
			// Checks Session time out
			this.checkSessionTimeout();

			if (oEvent.getParameter("newValue") === "") {
				_oView.byId("comboBoxAccountOrders").setSelectedKey("0");
			}

			_oView.byId("vbox_Orders_container").setBusyIndicatorDelay(0).setBusy(true);

			var selectedKey = this.byId("comboBoxAccountOrders").getSelectedKey();
			var districtFlag;
			var accountNum;
			_oView.byId("idRightNav").setEnabled(true);
			if (selectedKey.indexOf("district") === -1) {

				_oAccount = this.findAccountByDistrict(selectedKey, _oController);
				if (_oAccount === undefined) {
					_oView.byId("comboBoxAccountOrders").setSelectedKey("0");
					_oAccount = this.findAccountByDistrict("0", _oController);
				}
				if (_oAccount.id === "0") {
					_oView.byId("comboBoxAccountOrders").setSelectedKey("0");
					_oView.byId("accountAddress1Mobile").setVisible(false);
				} else {
					_oView.byId("accountAddress1Mobile").setVisible(true);
				}
				sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
				_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
				accountNum = _oAccount.accountnum;
				districtFlag = "0";
			} else if (selectedKey.indexOf("district") !== -1) {
				_oAccount = [];
				_oAccount.id = "district-" + oEvent.getParameters().value;
				_oAccount.billingcity = _oController.getResourceBundle().getText("MAresultsTXT");
				sap.ui.getCore().getModel("oSelectedAccount").setProperty("/", _oAccount);
				_oView.getModel("oSelectedAccount").setProperty("/", _oAccount);
				accountNum = "0";
				districtFlag = oEvent.getParameters().value;
			} else {
				return;
			}

			//Initialize Orders tables
			this.initializeTables(accountNum, districtFlag);

			if (_oView.byId("comboBoxAccountOrders").getSelectedKey() === "0" || _oView.byId("comboBoxAccountOrders").getSelectedKey()
				.indexOf("district") !== -1) {
				_oView.byId("accountInfoContainerDesktop").setVisible(false);
			} else {
				_oView.byId("accountInfoContainerDesktop").setVisible(true);
			}

			if (_oController.getView().byId("sortButton_ActiveOrders")) {
				_oController.getView().byId("sortButton_ActiveOrders").setSrc("resources/icon/sort_desc.png");
			}
			if (_oController.getView().byId("sortButton_CompletedOrders")) {
				_oController.getView().byId("sortButton_CompletedOrders").setSrc("resources/icon/sort_desc.png");
			}

			if (_oController.getView().byId("sortButtonEstDel_ActiveOrders")) {
				_oController.getView().byId("sortButtonEstDel_ActiveOrders").setSrc("resources/icon/sort_both.png");
			}

			_oController.getView().byId("filterOrdersDesktop").setValue("");
			_oController.getView().byId("dateRangeSelectionDesktop").setValue("");

			// Remove "statusOrdersLabel" class css in Order Status filter
			var link;
			var icon;
			link = $('*[id*=Link]:visible');
			icon = $('*[id*=Icon]:visible');
			if (link.hasClass("statusOrdersLabel")) {
				link.removeClass("statusOrdersLabel").addClass("bodyCopy2");
				icon.removeClass("statusOrdersIcon");
				$(':focus').blur();
			}
		},

		handleCalendarIcon: function () {
			//GTM
			_oController.GTMDataLayer('calendarclick',
				'Search',
				'Calendar Interaction',
				'Calender icon -click'
			);
		},

		/**
		 * Searches first by date range
		 * @function onSearchDateRange
		 **/
		onSearchDateRange: function (beginDate, endDate) {
			var modelToReturnActive = new sap.ui.model.json.JSONModel();
			var modelToReturnCompleted = new sap.ui.model.json.JSONModel();
			if (!beginDate && !endDate) {
				// Clone models
				_oOrderMdlActive.setData(JSON.parse(JSON.stringify(_totalResultsActive.getData())));
				_oOrderMdlCompleted.setData(JSON.parse(JSON.stringify(_totalResultsActive.getData())));
			} else {
				// Filtering the oModel
				modelToReturnActive.setData(this.JSONFilterOrdersDate(beginDate, endDate, _totalResultsActive));
				modelToReturnCompleted.setData(this.JSONFilterOrdersDate(beginDate, endDate, _totalResultsCompleted));
			}

			var dateRangeFilter;

			dateRangeFilter = _oController.getView().byId("dateRangeSelectionDesktop");

			if (dateRangeFilter) {
				dateRangeFilter.setValueState(sap.ui.core.ValueState.None);
			}

			var retObj = {
				"active": modelToReturnActive.getData(),
				"completed": modelToReturnCompleted.getData()
			};

			return retObj;
		},

		/**
		 * Searches first by by order number and po number
		 * @function onSearchFilter
		 **/
		onSearchFilter: function (newValue) {

			var modelToReturnActive = new sap.ui.model.json.JSONModel();
			var modelToReturnCompleted = new sap.ui.model.json.JSONModel();
			if (!newValue) {
				modelToReturnActive.setData(JSON.parse(JSON.stringify(_totalResultsActive.getData())));
				modelToReturnCompleted.setData(JSON.parse(JSON.stringify(_totalResultsCompleted.getData())));
			} else {
				// Filtering the oModel
				modelToReturnActive.setData(this.JSONFilterOrders(newValue, _totalResultsActive));
				modelToReturnCompleted.setData(this.JSONFilterOrders(newValue, _totalResultsCompleted));
			}

			var retObj = {
				"active": modelToReturnActive.getData(),
				"completed": modelToReturnCompleted.getData()
			};

			return retObj;
		},

		/**
		 * Sorts active orders by order date
		 * @function  onSortingActive
		 **/
		onSortingActive: function () {
			_oController.onSorting("orderDate", "orderTime", _oOrderMdlActive, "active");
		},

		/**
		 * Sorts active orders by order date
		 * @function  onSortingActive
		 **/
		onSortingCompleted: function () {
			_oController.onSorting("orderDate", "orderTime", _oOrderMdlCompleted, "completed");
		},

		onSortingActiveEstDel: function () {
			_oController.onSorting("estimatedDeliveryDate", "estimatedDeliveryTime", _oOrderMdlActive, "active");
		},

		/**
		 * Sorts a model by order date
		 * @function onSorting
		 **/
		onSorting: function (oFilterVariable, oFilterVariableTime, oModel, type) {
			var iconAct;
			var iconDeact;
			var sort;

			if (type === "active") {
				if (oFilterVariable === "estimatedDeliveryDate") {
					sort = _sortedActiveEstDel;
					iconAct = _oController.getView().byId("sortButtonEstDel_ActiveOrders");
					iconDeact = _oController.getView().byId("sortButton_ActiveOrders");
				} else {
					sort = _sortedActive;
					iconAct = _oController.getView().byId("sortButton_ActiveOrders");
					iconDeact = _oController.getView().byId("sortButtonEstDel_ActiveOrders");
				}

			} else {
				sort = _sortedCompleted;
				iconAct = _oController.getView().byId("sortButton_CompletedOrders");
			}
			switch (sort) {
			case "both":
			case "up":
				iconAct.setSrc("resources/icon/sort_desc.png");
				if (iconDeact) {
					iconDeact.setSrc("resources/icon/sort_both.png");
				}
				if (type === "active") {
					if (oFilterVariable === "estimatedDeliveryDate") {
						_sortedActiveEstDel = "down";
						_sortedActive = "both";

						if (_sortedActiveEstDel) {
							//GTM sort by Descending Est Delivery Date Active
							_oController.GTMDataLayer('sortingvent',
								'Order Status',
								'Est Delivery Date | Active | Sort',
								"Descending"
							);
						}
					} else {
						_sortedActive = "down";
						_sortedActiveEstDel = "both";
						//GTM sort by Descending Order Date Active
						_oController.GTMDataLayer('sortingvent',
							'Order Status',
							'Order Date | Active | Sort',
							"Descending"
						);
					}
				} else {
					_sortedCompleted = "down";
					//GTM sort by Descending Order Date Completed
					_oController.GTMDataLayer('sortingvent',
						'Order Status',
						'Order Date | Completed | Sort',
						"Descending"
					);
				}

				this.onFilteroModel(oModel, "descending", oFilterVariable, oFilterVariableTime);
				break;
			case "down":
				iconAct.setSrc("resources/icon/sort_asc.png");
				if (iconDeact) {
					iconDeact.setSrc("resources/icon/sort_both.png");
				}
				if (type === "active") {
					if (oFilterVariable === "estimatedDeliveryDate") {
						_sortedActiveEstDel = "up";
						_sortedActive = "both";
						if (_sortedActiveEstDel) {
							//GTM sort by Ascending Est Delivery Date Active
							_oController.GTMDataLayer('sortingvent',
								'Order Status',
								'Est Delivery Date | Active | Sort',
								"Ascending"
							);
						}
					} else {
						_sortedActive = "up";
						_sortedActiveEstDel = "both";
						//GTM sort by Ascending Order Date Active
						_oController.GTMDataLayer('sortingvent',
							'Order Status',
							'Order Date | Active | Sort',
							"Ascending"
						);
					}
				} else {
					_sortedCompleted = "up";
					//GTM sort by Ascending Order Date Completed
					_oController.GTMDataLayer('sortingvent',
						'Order Status',
						'Order Date | Completed | Sort',
						"Ascending"
					);
				}
				this.onFilteroModel(oModel, "ascending", oFilterVariable, oFilterVariableTime);
				break;
			}
			this.updateTables(_oOrderMdlActive, _oOrderMdlCompleted);
		},

		/**
		 * Sorts both orders for current ordering status, not changing arrows
		 * @function onSortingBothNoChange
		 **/
		onSortingBothNoChange: function () {
			switch (_sortedActive) {
			case "both":
				break;
			case "up":
				this.onFilteroModel(_oOrderMdlActive, "ascending", "orderDate", "orderTime");
				break;
			case "down":
				this.onFilteroModel(_oOrderMdlActive, "descending", "orderDate", "orderTime");
				break;
			}

			switch (_sortedCompleted) {
			case "both":
				break;
			case "up":
				this.onFilteroModel(_oOrderMdlCompleted, "ascending", "orderDate", "orderTime");
				break;
			case "down":
				this.onFilteroModel(_oOrderMdlCompleted, "descending", "orderDate", "orderTime");
				break;
			}

			switch (_sortedActiveEstDel) {
			case "both":
				break;
			case "up":
				this.onFilteroModel(_oOrderMdlActive, "ascending", "estimatedDeliveryDate", "estimatedDeliveryTime");
				break;
			case "down":
				this.onFilteroModel(_oOrderMdlActive, "descending", "estimatedDeliveryDate", "estimatedDeliveryTime");
				break;
			}

			this.updateTables(_oOrderMdlActive, _oOrderMdlCompleted);
		},

		/**
		 * JSONFilter function that receives the oData and filters
		 * @ parameter (filterValue, oModel)
		 */
		JSONFilterOrders: function (filterValue, oModel) {
			// DataJSON original and filtered
			var oDataJSON = oModel.getData();
			var oDataJSONFiltered = [];
			for (var j = 0; j < oDataJSON.length; j++) {
				// JSON Parameters to be filtered
				var poNumber = oDataJSON[j].ponumber.toLowerCase();
				var orderNumber = oDataJSON[j].orderNumber.toLowerCase();
				if (poNumber.indexOf(filterValue.toLowerCase()) !== -1 || orderNumber.indexOf(filterValue.toLowerCase()) !== -1) {
					oDataJSONFiltered.push(oDataJSON[j]);
				}
			}
			return oDataJSONFiltered;
		},

		/**
		 * JSONFilter function that receives the oData and filters by order date
		 * @ parameter (filterValue, oModel)
		 */
		JSONFilterOrdersDate: function (beginDate, endDate, oModel) {
			// DataJSON original and filtered
			var oDataJSON = oModel.getData();
			var oDataJSONFiltered = [];

			/*			beginDate.setHours(0, 0, 0, 0);
						endDate.setHours(0, 0, 0, 0);
		   */

			for (var j = 0; j < oDataJSON.length; j++) {
				// JSON Parameters to be filtered
				var dateUTCString, newdate, localDate;
				if (!oDataJSON[j].orderTime) {
					dateUTCString = oDataJSON[j].orderDate.concat("T12:00:00Z");
				} else {
					dateUTCString = oDataJSON[j].orderDate.concat("T", oDataJSON[j].orderTime, "Z");
				}
				newdate = new Date(dateUTCString);
				localDate = new Date(newdate.valueOf() - (newdate.getTimezoneOffset()) * 60000);
				if (localDate >= beginDate && localDate <= endDate) {
					oDataJSONFiltered.push(oDataJSON[j]);
				}
			}
			return oDataJSONFiltered;
		},

		/**
		 * Populates active and completed orders after filter.
		 * @function updateTables
		 * @param oAcctNum - account's number
		 */
		updateTables: function (active, completed) {

			_beginActive = 0;
			_beginCompleted = 0;
			_resultsCompleted = completed.getData();
			_resultsActive = active.getData();

			_oView.byId("idLeftNav").setEnabled(false);
			_oView.byId("idLeftNavCompleted").setEnabled(false);

			_iLengthActive = _resultsActive.length;
			var fstPageActive = [],
				k;

			if (_iLengthActive > 5) {

				if (_iLengthActive > _selectedActvResult) {
					_endActive = _selectedActvResult;
				} else {
					_endActive = _iLengthActive;
				}
				for (k = 0; k < _endActive; k++) {
					fstPageActive.push(_resultsActive[k]);
				}
				_oView.byId("idTextCount").setText(1 + "-" + _endActive + " " + _oController.getResourceBundle()
					.getText("of") + " " + _iLengthActive);

				_oView.byId("idActiveNav").setVisible(true);
				_oView.byId("idRightNav").setEnabled(true);
				_oView.byId("actvOrdersResultsPerPage").setVisible(true);
			} else {
				_endActive = _iLengthActive;
				for (k = 0; k < _iLengthActive; k++) {
					fstPageActive.push(_resultsActive[k]);
				}
				if (_iLengthActive === 0) {
					// none
				} else {
					_oView.byId("idRightNav").setEnabled(false);
					_oView.byId("idTextCount").setText(1 + "-" + _iLengthActive + " " + _oController.getResourceBundle()
						.getText("of") + " " + _iLengthActive);

					_oView.byId("idActiveNav").setVisible(true);

				}
				_oView.byId("actvOrdersResultsPerPage").setVisible(false);
			}

			_oView.byId("idLeftNav").setEnabled(false);

			if (_endActive >= _iLengthActive) {
				_endActive = _iLengthActive;
				_oView.byId("idRightNav").setEnabled(false);
			} else {
				_oView.byId("idRightNav").setEnabled(true);
			}

			var oOrderMdlActivePag = new sap.ui.model.json.JSONModel();
			oOrderMdlActivePag.setData(fstPageActive);
			if (_endActive > 100) {
				oOrderMdlActivePag.setSizeLimit(_endActive);
			}
			sap.ui.getCore().setModel(oOrderMdlActivePag, "oOrdersActivePaginated");
			_oController.getView().setModel(oOrderMdlActivePag, "oOrdersActivePaginated");

			//create first page completed
			_iLengthCompleted = _resultsCompleted.length;

			var fstPageCompleted = [],
				t;
			_beginCompleted = 0;
			if (_iLengthCompleted > 5) {
				if (_iLengthCompleted > _selectedCompResult) {
					_endCompleted = _selectedCompResult;
				} else {
					_endCompleted = _iLengthCompleted;
				}
				for (t = 0; t < _endCompleted; t++) {
					fstPageCompleted.push(_resultsCompleted[t]);
				}
				_oView.byId("idTextCountCompleted").setText(1 + "-" + _endCompleted + " " + _oController.getResourceBundle()
					.getText("of") + " " + _iLengthCompleted);

				_oView.byId("idCompletedNav").setVisible(true);
				_oView.byId("idRightNavCompleted").setEnabled(true);
				_oView.byId("compOrdersResultsPerPage").setVisible(true);
			} else {
				_endCompleted = _iLengthCompleted;
				for (t = 0; t < _iLengthCompleted; t++) {
					fstPageCompleted.push(_resultsCompleted[t]);
				}
				if (_iLengthCompleted === 0) {
					//None
				} else {
					_oView.byId("idRightNavCompleted").setEnabled(false);
					_oView.byId("idTextCountCompleted").setText(1 + "-" + _iLengthCompleted + " " + _oController.getResourceBundle()
						.getText("of") + " " + _iLengthCompleted);
					_oView.byId("idCompletedNav").setVisible(true);
				}
				_oView.byId("compOrdersResultsPerPage").setVisible(false);
			}

			_oView.byId("idLeftNavCompleted").setEnabled(false);
			if (_endCompleted >= _iLengthCompleted) {
				_endCompleted = _iLengthCompleted;
				_oView.byId("idRightNavCompleted").setEnabled(false);
			} else {
				_oView.byId("idRightNavCompleted").setEnabled(true);
			}

			var oOrderMdlCompletedPag = new sap.ui.model.json.JSONModel();
			oOrderMdlCompletedPag.setData(fstPageCompleted);

			sap.ui.getCore().setModel(oOrderMdlCompletedPag, "oOrdersCompletedPaginated");
			_oController.getView().setModel(oOrderMdlCompletedPag, "oOrdersCompletedPaginated");

			_oOrderMdlActive.setData(_resultsActive);
			_oOrderMdlCompleted.setData(_resultsCompleted);

			sap.ui.getCore().setModel(_oOrderMdlActive, "oActiveOrders");
			sap.ui.getCore().setModel(_oOrderMdlCompleted, "oCompletedOrders");

			// disable or enable navigation arrows
			if (_iLengthActive === 0) {
				_oView.byId("idActiveNav").setVisible(false);
				_oView.byId("actvOrdersResultsPerPage").setVisible(false);
			}

			if (_iLengthCompleted === 0) {
				_oView.byId("idCompletedNav").setVisible(false);
				_oView.byId("compOrdersResultsPerPage").setVisible(false);
			}
		},

		/**
		 * Resets de date range selection
		 * @function resetDateRange
		 **/
		resetDateRange: function () {
			var searchFilter;
			_oView.byId("dateRangeSelectionDesktop").setSecondDateValue(null);
			_oView.byId("dateRangeSelectionDesktop").setDateValue(null);
			searchFilter = _oController.getView().byId("filterOrdersDesktop");

			// Clone models
			_oOrderMdlActive.setData(JSON.parse(JSON.stringify(_totalResultsActive.getData())));
			_oOrderMdlCompleted.setData(JSON.parse(JSON.stringify(_totalResultsCompleted.getData())));

			var modelResultsCompletedFiltered = _oOrderMdlCompleted;
			var modelResultsActiveFiltered = _oOrderMdlActive;

			if (searchFilter && searchFilter.getValue() !== "") {
				var filteredOrders = _oController.onSearchFilter(searchFilter.getValue());
				modelResultsCompletedFiltered.setData(filteredOrders.completed);
				modelResultsActiveFiltered.setData(filteredOrders.active);
			}

			this.updateTables(modelResultsActiveFiltered, modelResultsCompletedFiltered);
			this.onSortingBothNoChange();

			_oController.GTMDataLayer('resetfilter',
				'Search',
				'Reset Filter -click',
				''
			);

			_oController.filterByAllFilters();
		},

		/**
		 * Filters by order number of the pressed order
		 * @function filterByOrderNumber
		 **/
		filterByOrderNumber: function (oEvent) {
			var sPath = oEvent.getSource().getBinding("text").getBindings()[0].getContext().getPath();

			var order = sap.ui.getCore().getModel("oOrdersActivePaginated").getProperty(sPath);
			var orderNumber;
			if (order) {
				orderNumber = order.orderNumber;
			}

			_oView.byId("filterOrdersDesktop").setValue(orderNumber);
			_oView.byId("dateRangeSelectionDesktop").setSecondDateValue(null);
			_oView.byId("dateRangeSelectionDesktop").setDateValue(null);

			// Setup all needed models
			var oModelOrdersFilteredActive = new sap.ui.model.json.JSONModel();
			var oModelOrdersFilteredCompleted = new sap.ui.model.json.JSONModel();

			// Filtering the oModel
			oModelOrdersFilteredActive.setData(this.JSONFilterOrders(orderNumber, _oOrderMdlActive));
			oModelOrdersFilteredCompleted.setData(this.JSONFilterOrders(orderNumber, _oOrderMdlCompleted));
			// Equals to the Pagination Model
			_oOrderMdlActive = oModelOrdersFilteredActive;
			_oOrderMdlCompleted = oModelOrdersFilteredCompleted;

			// Remove "statusOrdersLabel" class css in Order Status filter
			var link;
			var icon;
			link = $('*[id*=Link]:visible');
			icon = $('*[id*=Icon]:visible');
			if (link.hasClass("statusOrdersLabel")) {
				link.removeClass("statusOrdersLabel").addClass("bodyCopy2");
				icon.removeClass("statusOrdersIcon");
			}

			this.updateTables(oModelOrdersFilteredActive, oModelOrdersFilteredCompleted);
		},

		/**
		 * Ordering Algorithm for dates
		 @ parameter (oModeo,orderMode,objectName,objectNameTime)
		 */
		onFilteroModel: function (oModel, orderMode, objectName, objectNameTime) {
			oModel.getData().sort(function (a, b) {
				var dateUTCStringA, dateUTCStringB;
				if (!a.valueOf()[objectNameTime] || a.valueOf()[objectNameTime] === "000000") {
					dateUTCStringA = a.valueOf()[objectName].concat("T12:00:00Z");
				} else {
					dateUTCStringA = a.valueOf()[objectName].concat("T", a.valueOf()[objectNameTime], "Z");
				}
				var newdateA = new Date(dateUTCStringA);
				var localDateA = new Date(newdateA.valueOf() - (newdateA.getTimezoneOffset()) * 60000);

				if (!b.valueOf()[objectNameTime] || b.valueOf()[objectNameTime] === "000000") {
					dateUTCStringB = b.valueOf()[objectName].concat("T12:00:00Z");
				} else {
					dateUTCStringB = b.valueOf()[objectName].concat("T", b.valueOf()[objectNameTime], "Z");
				}
				var newdateB = new Date(dateUTCStringB);
				var localDateB = new Date(newdateB.valueOf() - (newdateB.getTimezoneOffset()) * 60000);

				switch (orderMode) {
				case "ascending":
					if (isNaN(localDateA.getTime()) && !isNaN(localDateB.getTime())) {
						return 1;
					} else if (!isNaN(localDateA.getTime()) && isNaN(localDateB.getTime())) {
						return -1;
					}
					if (localDateA < localDateB) {
						return -1;
					} else if (localDateA > localDateB) {
						return 1;
					} else {
						return 0;
					}
					break;
				case "descending":
					if (isNaN(localDateA.getTime()) && !isNaN(localDateB.getTime())) {
						return 1;
					} else if (!isNaN(localDateA.getTime()) && isNaN(localDateB.getTime())) {
						return -1;
					}
					if (localDateA < localDateB) {
						return 1;
					} else if (localDateA > localDateB) {
						return -1;
					} else {
						return 0;
					}
					break;
				default:
					break;
				}
			});
		},

		/**
		 * 
		 */
		inputSearchOrderFunctions: function () {
			var inputSearchOrderDesktop = _oView.byId("filterOrdersDesktop").getDomRef();
			inputSearchOrderDesktop.addEventListener("focusout", function () {
				_oController.gtmInputSearchOrdersFunctions(_resultsActiveModel, "handleLiveSearch");
			});
		},

		/**
		 * 
		 */
		handleSearch: function (oEvent) {
			if (oEvent.getParameters().clearButtonPressed === true) {
				_oController.gtmInputSearchOrdersFunctions(_resultsActiveModel, "handleSearchClear");
			}
		},

		/**
		 * 
		 */
		gtmInputSearchOrdersFunctions: function (resultsActiveModel, sCaller) {
			if (sCaller !== "handleSearchClear") {
				var sSearch = _oController.getView().byId("filterOrdersDesktop").getValue();
				if (sSearch !== "" && sSearch !== null) {
					_sSearch = sSearch;
				}
				if (_sSearch === "" || _sSearch === null || sSearch === "") {
					return;
				}
			}
			// DataJSON original and filtered
			var oDataJSON = resultsActiveModel.getData();
			var bPONumberAndOrdNumb = false;
			var bPONumber = false;
			var bOrdNumb = false;
			for (var j = 0; j < oDataJSON.length; j++) {
				// JSON Parameters to be filtered
				var poNumber = oDataJSON[j].ponumber.toLowerCase();
				var orderNumber = oDataJSON[j].orderNumber.toLowerCase();
				if (poNumber.indexOf(_sSearch.toLowerCase()) !== -1 && orderNumber.indexOf(_sSearch.toLowerCase()) !== -1) {
					//If matches the 2 types of search	
					bPONumberAndOrdNumb = true;
					j = oDataJSON.length;
				} else if (poNumber.indexOf(_sSearch.toLowerCase()) !== -1) {
					//If matches the poNumber search
					bPONumber = true;
				} else if (orderNumber.indexOf(_sSearch.toLowerCase()) !== -1) {
					//If matches the orderNumber search
					bOrdNumb = true;
				}
			}

			if (bPONumberAndOrdNumb || (bPONumber && bOrdNumb)) {
				//GTM matches the 2 types of search
				_oController.GTMDataLayer('ordersearch',
					'Search',
					'Search by OrderNumber && PO-Number',
					_sSearch
				);
			} else if (bPONumber) {
				//GTM matches the poNumber search
				_oController.GTMDataLayer('ordersearch',
					'Search',
					'Search by PO-Number',
					_sSearch
				);
			} else if (bOrdNumb) {
				//GTM matches the orderNumber search
				_oController.GTMDataLayer('ordersearch',
					'Search',
					'Search by OrderNumber',
					_sSearch
				);
			} else if (_sSearch !== bPONumberAndOrdNumb || _sSearch !== bPONumber || _sSearch !== bOrdNumb) {
				//GTM matches for another type
				_oController.GTMDataLayer('ordersearch',
					'Search',
					'Search by other',
					_sSearch
				);
			}
		},

		/**
		 * JSONFilter function that receives the oData and filters
		 * @ parameter (filterValue, oModel)
		 */
		JSONFilterByStatusOrder: function (filterValue, oModel) {
			// DataJSON original and filtered
			var oDataJSON = oModel.getData();
			var oDataJSONFiltered = [];
			for (var j = 0; j < oDataJSON.length; j++) {
				// JSON Parameters to be filtered
				var headerStatus = oDataJSON[j].headerStatus;
				if (headerStatus.indexOf(filterValue) !== -1) {
					oDataJSONFiltered.push(oDataJSON[j]);
				}
			}
			return oDataJSONFiltered;
		},

		filterByStatusOrders: function (oEvent) {
			var filterId;

			//Find ID of Order Status Press
			if (oEvent.getSource().getId().indexOf("filterOrderReceivedLink") > -1 || oEvent.getSource().getId().indexOf(
					"filterOrderReceivedIcon") > -1) {
				filterId = "filterOrderReceived";
			} else if (oEvent.getSource().getId().indexOf("filterInProcessLink") > -1 || oEvent.getSource().getId().indexOf(
					"filterInProcessIcon") > -1) {
				filterId = "filterInProcess";
			} else if (oEvent.getSource().getId().indexOf("filterShippedLink") > -1 ||
				oEvent.getSource().getId().indexOf("filterShippedIcon") > -1) {
				filterId = "filterShipped";
			}

			// Add css Class to verify if are pressed or not
			if ($("#" + _oView.byId(filterId + "Link").getId()).hasClass("statusOrdersLabel")) {
				$("#" + _oView.byId(filterId + "Link").getId()).removeClass("statusOrdersLabel").addClass("bodyCopy2");
				$("#" + _oView.byId(filterId + "Icon").getId()).removeClass("statusOrdersIcon");
				$("#" + _oView.byId(filterId).getId()).addClass(
					"statusOrder-hover");
			} else {
				$("#" + _oView.byId(filterId + "Link").getId()).removeClass("bodyCopy2").addClass("statusOrdersLabel");
				$("#" + _oView.byId(filterId + "Icon").getId()).addClass("statusOrdersIcon");
				$("#" + _oView.byId(filterId).getId()).removeClass("statusOrder-hover");

				if (filterId === "filterOrderReceived") {
					//GTM sort filter by Order Received
					_oController.GTMDataLayer('statusevnt',
						'Orders Status',
						'OrderStatus filter -click',
						"Order Received"
					);
				} else if (filterId === "filterInProcess") {
					//GTM sort filter by In Process
					_oController.GTMDataLayer('statusevnt',
						'Orders Status',
						'OrderStatus filter -click',
						"In Process"
					);
				} else if (filterId === "filterShipped") {
					//GTM sort filter by Shipped
					_oController.GTMDataLayer('statusevnt',
						'Orders Status',
						'OrderStatus filter -click',
						"Shipped"
					);
				}
			}
			_oController.filterByAllFilters();
		},

		filterStatusOrders: function (newValue, cloneModelActive, cloneModelCompleted) {

			// Filtering the oModel
			cloneModelActive.setData(this.JSONFilterByStatusOrder(newValue, cloneModelActive));
			if (newValue === "SHIPPED") {
				cloneModelCompleted.setData(this.JSONFilterByStatusOrder(newValue, cloneModelCompleted));
			} else {
				cloneModelCompleted = new sap.ui.model.json.JSONModel([]);
			}
			// Equals to the Pagination Model
			_oOrderMdlActive = cloneModelActive;
			_oOrderMdlCompleted = cloneModelCompleted;

			var cloneModel = {
				"Active": cloneModelActive,
				"Completed": cloneModelCompleted
			};

			return cloneModel;
		},

		filterByAllFilters: function (oEvent) {
			var filterValue, k;
			var bReceived, bInProcess, bShipped;
			var cloneModelActiveReceived = new sap.ui.model.json.JSONModel();
			var cloneModelActiveInProgress = new sap.ui.model.json.JSONModel();
			var cloneModelActiveShipped = new sap.ui.model.json.JSONModel();
			var cloneModelCompleted = new sap.ui.model.json.JSONModel();
			var oModelsData;

			cloneModelActiveReceived.setData(JSON.parse(JSON.stringify(_resultsActiveModel.getData())));
			cloneModelActiveInProgress.setData(JSON.parse(JSON.stringify(_resultsActiveModel.getData())));
			cloneModelActiveShipped.setData(JSON.parse(JSON.stringify(_resultsActiveModel.getData())));

			cloneModelCompleted.setData(JSON.parse(JSON.stringify(_resultsCompletedModel.getData())));

			// Find ID to apply filter
			bReceived = $("#" + _oView.byId("filterOrderReceivedLink").getId()).hasClass("statusOrdersLabel");
			bInProcess = $("#" + _oView.byId("filterInProcessLink").getId()).hasClass("statusOrdersLabel");
			bShipped = $("#" + _oView.byId("filterShippedLink").getId()).hasClass("statusOrdersLabel");

			// Filter by Order Received					
			if (bReceived) {
				filterValue = "RECEIVED";
				oModelsData = _oController.filterStatusOrders(filterValue, cloneModelActiveReceived);
				cloneModelActiveReceived = oModelsData.Active;
			} else {
				cloneModelActiveReceived.setData([]);
			}
			// Filter by In Process
			if (bInProcess) {
				filterValue = "IN PROCESS";
				oModelsData = _oController.filterStatusOrders(filterValue, cloneModelActiveInProgress);
				cloneModelActiveInProgress = oModelsData.Active;
			} else {
				cloneModelActiveInProgress.setData([]);
			}
			// Filter by Shipped
			if (bShipped) {
				filterValue = "SHIPPED";
				oModelsData = _oController.filterStatusOrders(filterValue, cloneModelActiveShipped, cloneModelCompleted);
				cloneModelActiveShipped = oModelsData.Active;
				cloneModelCompleted = oModelsData.Completed;
			} else {
				cloneModelActiveShipped.setData([]);
				cloneModelCompleted.setData([]);
			}

			if (bReceived || bInProcess || bShipped) {
				// Compare Recived with In Progress
				for (k = 0; k < cloneModelActiveInProgress.getData().length; k++) {
					cloneModelActiveReceived.getData().push(cloneModelActiveInProgress.getData()[k]);
				}
				// Compare Recived with Shipped
				for (k = 0; k < cloneModelActiveShipped.getData().length; k++) {
					cloneModelActiveReceived.getData().push(cloneModelActiveShipped.getData()[k]);
				}

			} else {
				cloneModelActiveReceived.setData(JSON.parse(JSON.stringify(_resultsActiveModel.getData())));
				cloneModelCompleted.setData(JSON.parse(JSON.stringify(_resultsCompletedModel.getData())));
			}

			var sSearch, dateRangeFilter;

			sSearch = _oController.getView().byId("filterOrdersDesktop").getValue();
			if (sSearch !== undefined && sSearch !== "") {
				_sSearch = sSearch;
			}
			dateRangeFilter = _oController.getView().byId("dateRangeSelectionDesktop");

			if (dateRangeFilter.getDateValue() !== null && dateRangeFilter.getSecondDateValue() !== null) {
				cloneModelActiveReceived.setData(_oController.JSONFilterOrdersDate(dateRangeFilter.getDateValue(), dateRangeFilter.getSecondDateValue(),
					cloneModelActiveReceived));
				cloneModelCompleted.setData(_oController.JSONFilterOrdersDate(dateRangeFilter.getDateValue(), dateRangeFilter.getSecondDateValue(),
					cloneModelCompleted));

				_oController.GTMDataLayer('calendarclick',
					'Search',
					'Calendar Interaction',
					'Selected Date Range'
				);
			}

			if (sSearch !== null && sSearch !== undefined && sSearch !== "") {
				cloneModelActiveReceived.setData(_oController.JSONFilterOrders(sSearch, cloneModelActiveReceived));
				cloneModelCompleted.setData(_oController.JSONFilterOrders(sSearch, cloneModelCompleted));
			}

			//Update Table  by filters
			this.updateTables(cloneModelActiveReceived, cloneModelCompleted);
			this.onSortingBothNoChange();
		},

		/**
		 *  Select the number of results displayed on a page
		 *  @selectActvResultsPerPage
		 */
		selectActvResultsPerPage: function (oEvent) {
			var sValue = oEvent.getSource().getText();
			var oSelectedLink = _oView.byId("actvOrdersResults" + sValue);
			_oController.removeSearchResultsStyleClass("actvOrdersResults", _resultsList, _oView);
			oSelectedLink.addStyleClass("numberOfResultsLinkSelected");
			var activeOrders = _resultsActive;
			var begin = 0;
			var end = parseInt(sValue, 10);
			_selectedActvResult = end;

			this.getView().byId("idLeftNav").setEnabled(false);
			if (end >= _iLengthActive) {
				end = _iLengthActive;
				this.getView().byId("idRightNav").setEnabled(false);
			} else {
				this.getView().byId("idRightNav").setEnabled(true);
			}

			_oView.byId("idTextCount").setText(begin + 1 + "-" + end + " " + _oController.getResourceBundle()
				.getText("of") + " " + _iLengthActive);

			var paginatedActive = [];
			for (i = begin; i < end; i++) {
				paginatedActive.push(activeOrders[i]);
			}
			_beginActive = begin;
			_endActive = end;

			var oOrderMdlActivePag = new sap.ui.model.json.JSONModel();
			oOrderMdlActivePag.setData(paginatedActive);
			if (end > 100) {
				oOrderMdlActivePag.setSizeLimit(end);
			}

			sap.ui.getCore().setModel(oOrderMdlActivePag, "oOrdersActivePaginated");
			_oController.getView().setModel(oOrderMdlActivePag, "oOrdersActivePaginated");
		},

		/**
		 *  Select the number of results displayed on a page
		 *  @selectCompResultsPerPage
		 */
		selectCompResultsPerPage: function (oEvent) {
			var sValue = oEvent.getSource().getText();
			var oSelectedLink = _oView.byId("compOrdersResults" + sValue);
			_oController.removeSearchResultsStyleClass("compOrdersResults", _resultsList, _oView);
			oSelectedLink.addStyleClass("numberOfResultsLinkSelected");
			var completedOrders = _resultsCompleted;
			var begin = 0;
			var end = parseInt(sValue, 10);
			_selectedCompResult = end;

			this.getView().byId("idLeftNavCompleted").setEnabled(false);
			if (end >= _iLengthCompleted) {
				end = _iLengthCompleted;
				this.getView().byId("idRightNavCompleted").setEnabled(false);
			} else {
				this.getView().byId("idRightNavCompleted").setEnabled(true);
			}

			_oView.byId("idTextCountCompleted").setText(begin + 1 + "-" + end + " " + _oController.getResourceBundle()
				.getText("of") + " " + _iLengthCompleted);

			var paginatedCompleted = [];
			for (i = begin; i < end; i++) {
				paginatedCompleted.push(completedOrders[i]);
			}
			_beginCompleted = begin;
			_endCompleted = end;

			var oOrderMdlCompletedPag = new sap.ui.model.json.JSONModel();
			oOrderMdlCompletedPag.setData(paginatedCompleted);
			if (end > 100) {
				oOrderMdlCompletedPag.setSizeLimit(end);
			}

			sap.ui.getCore().setModel(oOrderMdlCompletedPag, "oOrdersCompletedPaginated");
			_oController.getView().setModel(oOrderMdlCompletedPag, "oOrdersCompletedPaginated");
		}
	});
});