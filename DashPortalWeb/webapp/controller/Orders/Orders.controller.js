sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel"
], function (Base, JSONModel) {
	"use strict";
	var _oRouter, _oView, _oController;
	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Orders.Orders", {
		/** @module Orders */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function () {

			Base.prototype.onInit.call(this);
			_oView = this.getView();
			_oController = this;
			_oRouter = this.getRouter();
			_oRouter.getRoute("orders").attachPatternMatched(this._onObjectMatched, this);

			if (this.onGetDashDevice() === "NARROW") {
				this.getView().byId("OrdersIconTitleForNonMobile").setVisible(false);
				this.getView().byId("idListOrders").setVisible(false);

			} else {
				this.getView().byId("OrdersTitleMobile").setVisible(false);
				this.getView().byId("OrdersIconMobile").setVisible(false);
				this.getView().byId("idListOrders2").setVisible(false);
			}
		},

		/**
		 * Set the filter for the selected account and initialize order tables
		 * @function _onObjectMatched
		 */
		_onObjectMatched: function () {
			// Clears all error message
			this.clearServerErrorMsg(this);

			//hide the seeAll button before rendering
			this.getView().byId("seeAllOrders").setVisible(false);

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("ORDERS_PAGE")) {
				return;
			}
			var oSVGS = new sap.ui.model.json.JSONModel({
				"inProcess": _oController.getKey("keys_svg", "keyShape_inProcess"),
				"orderReceived": _oController.getKey("keys_svg", "keyShape_orderReceived"),
				"shipped": _oController.getKey("keys_svg", "keyShape_shipped"),
				"rightArrow": _oController.getKey("keys_svg", "keyShape_rightArrow")
			});
			_oView.setModel(oSVGS, "oSVGS");
			this.headerIconAccess();

			//get user info and direct account
			this.getUserInformation(this, this.toRunAfterGettingUserInfo);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);

			//Load Navigation Cards
			_oController.byId("cardContainerOrders").setViewContext(_oController);
			_oController.populateCardContainer("Orders", "10");

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "orders"
			});
		},

		/**
		 * This function handles the logic that requires the user information to be available.
		 * @function toRunAfterGettingUserInfo
		 * @param {object} controller - the page context
		 */
		toRunAfterGettingUserInfo: function (controller) {
			if (sap.ui.getCore().getModel("oAccounts")) {
				if (controller.checkDASHLayoutAccess("ORDERSTATUS_LIST")) {
					controller.initializeTables();
				} else {
					controller.getView().byId("viewOrdersContainer").setVisible(false);
				}
			}
		},

		/**
		 * Navigates to Order Status page when clicking on see all
		 * @function onSeeAllOrders
		 */
		onSeeAllOrders: function () {
			// Checks Session time out
			this.checkSessionTimeout();

			//GTM
			_oController.GTMDataLayer('orderseeall',
				'Orders',
				'See All',
				'See All click'
			);

			ga('set', 'page', '/MyAccountOrderStatus');
			ga('send', 'pageview');
			this.getRouter().navTo("orderStatus");
		},

		/**
		 * Function that handles the success case of the ajax call that gets the data to initialyze the orders table
		 * @function onInitializeTablesSuccess
		 */
		onInitializeTablesSuccess: function (oController, oData, oPiggyBack) {
			oController.getView().byId("seeAllOrders").setVisible(oData.isShowAll);

			var oOrder = new sap.ui.model.json.JSONModel();
			oOrder.setData(oData.activeOrders);

			_oController.getView().setModel(oOrder, "oOrdersActive");

			//disable busy indicator
			if (oController.onGetDashDevice() === "NARROW") {
				oController.getView().byId("idListOrders2").setBusy(false);
			} else {
				oController.getView().byId("idListOrders").setBusy(false);
			}
		},

		/**
		 * Function that handles the error case of the ajax call that gets the data to initialyze the orders table
		 * @function onInitializeTablesError
		 */
		onInitializeTablesError: function (oController, oError, oPiggyBack) {
			oController.onShowMessage(oController, "Error", oError.status, oError.statusText, oError.responseText, oController.getResourceBundle()
				.getText(
					"ERROrders"));
			if (oController.onGetDashDevice() === "NARROW") {
				oController.getView().byId("idListOrders2").setBusy(false);
			} else {
				oController.getView().byId("idListOrders").setBusy(false);
			}
		},

		/**
		 * Populates Three Orders List.
		 * @function initializeTables
		 */
		initializeTables: function () {
			if (this.onGetDashDevice() === "NARROW") {
				this.getView().byId("idListOrders2").setBusyIndicatorDelay(0);
				this.getView().byId("idListOrders2").setBusy(true);
			} else {
				this.getView().byId("idListOrders").setBusyIndicatorDelay(0);
				this.getView().byId("idListOrders").setBusy(true);
			}

			var controller = this;
			var xsURL = "/orders/orderPage";
			//handleAjaxJSONCall - Parameters: oController, aSync, xsURL, sCallType, fSuccess, fError, oDataToSend, oPiggyBack
			controller.handleAjaxJSONCall(controller, true, xsURL, "GET", _oController.onInitializeTablesSuccess, controller.onInitializeTablesError);
		},

		/**
		 * Formatter for the order part number.
		 * @function formatPartNumber
		 * @param orderPart {string} - part number out the total order count number
		 * @param orderCount {string} - number of orders
		 */
		formatPartNumber: function (orderPart, orderCount) {
			var infoDisplayed;
			if (orderPart !== 1 || orderCount !== 1) {
				_oView.byId("PONumberBoxMobile").addStyleClass("sapUiTinyMarginTop");
				infoDisplayed = "(" + orderPart + " " + _oController.getResourceBundle().getText("MAofTXT") + " " + orderCount + ")";
				return infoDisplayed;
			}
		},

		/**
		 * Loads page with the next active cases.
		 * @function fnNextPageActive
		 * @param {Object} oEvent - page to load
		 */
		fnNextPageActive: function (oEvent) {
			// Checks Session time out
			this.checkSessionTimeout();
			//Get the selected Item from Binding Context of Model oOrderHeaders
			var oSelItem = oEvent.getSource().getBindingContext("oOrdersActive").getObject();

			var sUrl = this.getRouter().getURL("orderDetails", {
				orderUUID: oSelItem.uuid,
				language: sap.ui.getCore().getConfiguration().getLanguage()
			});

			//Execute a window.open to create a new window, Add a # before the URL to use the Mother Url
			window.open("#" + sUrl);
		},
		/**
		 * Sets estimated delivery date string
		 * @function formatEstDateLbl
		 * @param  {string} sDate- estimated delivery date
		 * @returns Est. Delivery Date string
		 */
		formatEstDateLbl: function (sDate) {
			var sNewDate;
			if (sDate === null) {
				sNewDate = "";
			} else {
				sNewDate = "Est. Delivery Date";
			}
			return sNewDate;
		},
		/**
		 * Defines is the carrier should be visible or not
		 * @function formatterCarrierVisible
		 * @param {string} headerStatus - order's header status
		 */
		formatterCarrierVisible: function (headerStatus) {
			return (headerStatus === "SHIPPED");
		},
		// ******************************************************************************************

		/**
		 * Initializes the model.
		 * @function onAfterRendering
		 */
		onAfterRendering: function () {
			/* Set the menu as active if the app is reloaded */
			$(".js-orders-sidenav").addClass("sidenav__item--active");

		},
		/**
		 * Triggers the GTM tags, also handles the price list exception
		 * @function gtmHandling
		 * @param {String} callToActionURL - Route/link or fragment
		 **/
		gtmHandling: function (oEvent) {
			var callToActionURL = oEvent.getParameter("callToActionURL");
			var sEvent = _oController.getKey("keys_gtm", "keyOrders_cardClick_event");
			var sEventCategory = _oController.getKey("keys_gtm", "keyOrders_cardClick_eventCategory");
			var sEventAction = _oController.getKey("keys_gtm", "keyOrders_cardClick_" + callToActionURL.replace(/-|\//g, "") + "_eventAction");
			if (sEvent && sEventCategory && sEventAction) {
				//GTM Data Layer Push (event, eventCategory, eventAction, eventLabel)
				_oController.GTMDataLayer(sEvent,
					sEventCategory,
					sEventAction,
					"#" + callToActionURL
				);
			}
		}
	});
});