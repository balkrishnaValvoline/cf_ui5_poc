sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller",
	"sap/ui/model/json/JSONModel"
], function (Base, JSONModel) {
	"use strict";
	var _oController;
	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Promote.Promote", {
		/** @module Promote */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);
			var oRouter = this.getRouter();
			_oController = this;
			oRouter.getRoute("promote").attachPatternMatched(this._onObjectMatched, this);
		},

		/**
		 * Verify authorization and layout access
		 * @function  _onObjectMatched
		 */
		_onObjectMatched: function () {
			// Clears all error message
			this.clearServerErrorMsg(this);

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "promote"
			});

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("PROMOTE_PAGE")) {
				return;
			}

			//get user info and direct account
			this.getUserInformation(this);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			this.headerIconAccess();

			// Calls the populate Card function
			_oController.byId("cardContainerPromote").setViewContext(_oController);
			this.populateCardContainer("Promote", "7", "All");

			//Shopping Cart Values
			this.getShoppingCartCookie(this);
		},

		/**
		 * Set the model for the view after rendering the page
		 * @function onAfterRendering
		 */
		onAfterRendering: function () {
			/* Set the menu as active if the app is reloaded */
			$(".js-promote-sidenav").addClass("sidenav__item--active");
		}
	});
});