sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/controller/Base.controller"
], function (Base) {
	"use strict";
	var _oController;

	return Base.extend("valvoline.dash.portal.DashPortalWeb.controller.Promote.PromotionsSummary", {
		/** @module PromotionsSummary */
		/**
		 * This function initializes the controller
		 * @function onInit
		 */
		onInit: function () {
			Base.prototype.onInit.call(this);

			var oRouter = this.getRouter();
			_oController = this;
			oRouter.getRoute("promoteSummary").attachPatternMatched(this._onObjectMatched, this);
			oRouter.getRoute("promoteSummary").attachPatternMatched(this.populateCards, this);
			oRouter.getRoute("promoteSummary").attachPatternMatched(this.removeHighlightSideMenu, this);
		},

		/**
		 * Verify authorization and layout access
		 * @function  _onObjectMatched
		 */
		_onObjectMatched: function () {
			// Clears all error message
			this.clearServerErrorMsg(this);

			//set the navbar icon as selected
			sap.ui.getCore().getEventBus().publish("routeMatched", "routeMatched", {
				page: "promote"
			});

			// Page authorization and layout check
			if (!this.checkDASHPageAuthorization("PROMOTESUMMARY_PAGE")) {
				return;
			}

			//get user info and direct account
			this.getUserInformation(this);

			//check Language and set selected Key in combo box
			this.onChangeLanguage();

			this.headerIconAccess();

			//Shopping Cart Values
			this.getShoppingCartCookie(this);
		},
		/**
		 * Remove highlight from promote icon in sidebar.
		 * @function removeHighlightSideMenu
		 */
		removeHighlightSideMenu: function () {
			$(".sidenav__item").removeClass("sidenav__item--active");
		},

		/**
		 * Populate Cards for Promotions Summary view.
		 * @function populateCards
		 */
		populateCards: function () {
			_oController.byId("cardContainerPromotionsSummary").setViewContext(_oController);
			_oController.populateCardContainer("PromotionsSummary", "8", "All");
		}
	});
});