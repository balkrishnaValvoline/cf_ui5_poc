 sap.ui.define([
 	"sap/ui/core/UIComponent",
 	"sap/ui/Device",
 	"valvoline/dash/portal/DashPortalWeb/model/models",
 	"sap/ui/core/routing/HashChanger"
 ], function (UIComponent, Device, models, HashChanger) {
 	"use strict";

 	return UIComponent.extend("valvoline.dash.portal.DashPortalWeb.Component", {

 		metadata: {
 			manifest: "json"
 		},

 		/**
 		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
 		 * @public
 		 * @override
 		 */
 		init: function () {
 			//Hide Hybris container
 			$("#Hybris").hide();
 	

 			// call the base component's init function
 			UIComponent.prototype.init.apply(this, arguments);
 			//Init dash range set 
 			/**
 			 * Initializes the range sets for dash. 
 			 * The first one 'DASH_DEFAULT' is to be used to detect changes in all device size  ranges supported by dash. 
 			 * The second one 'DASH_MOBILE' is to be used to detect changes in and out of the Mobile range. This can be used when a change is needed only when going in and aut of mobile. 
 			 */
 			sap.ui.Device.media.initRangeSet("DASH_DEFAULT", [768, 1024], "px", ["mobile", "tablet", "desktop"]);
 			sap.ui.Device.media.initRangeSet("DASH_MOBILE", [768], "px", ["mobile", "other"]);

 			// set the device model
 			this.setModel(models.createDeviceModel(), "device");
			if(sap.ui.Device.browser.msie){
 			// Logic added to handle the back button/trigger after going trough the
 			//mainInsights Page on IE. IE does not have the newHash and oldHash for the hashchange event
 			//bellow code relies on SAPUI5 HashChanger "polyfill"
				HashChanger.getInstance().attachEvent("hashChanged", function(oEvent) {
					if(oEvent){
			 			var sNewHash = oEvent.getParameter("newHash");
			 			var sOldHash = oEvent.getParameter("oldHash");
			 			if (sNewHash === "mainInsights" && (sOldHash === "insights" || sOldHash === "distributorInsights" || sOldHash === "insightsHD")) {
			 				// Code to handle back navigation if on an Insights page and the previous page
			 				//is the MainInsights 
			 				setTimeout(function(){history.go(-2);}, 0);
			 			}
		 			}
				});
			}
 			// create the views based on the url/hash
 			this.getRouter().initialize();
 			window.addEventListener('onpopstate', this.listenerHistoryChange);
 			window.addEventListener('hashchange', this.listenerHistoryChange);
 		},

 		/**
 		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
 		 * @public
 		 * @override
 		 */
 		listenerHistoryChange: function (oEvent) {
 			// Logic added to handle the back button/trigger after going trough the
 			//mainInsights Page.
 			if(!sap.ui.Device.browser.msie && oEvent && oEvent.newURL && oEvent.oldURL){
	 			var sNewHash = oEvent.newURL.split('#').pop();
	 			var sOldHash = oEvent.oldURL.split('#').pop();
	 			if (sNewHash === "/mainInsights" && (sOldHash === "/insights" || sOldHash === "/distributorInsights" || sOldHash === "/insightsHD")) {
	 				// Code to handle back navigation if on an Insights page and the previous page
	 				//is the MainInsights 
	 				window.history.go(-2);
	 			}
 			}
 			if (window.location.href.indexOf("/store") === -1) {
 				sap.ui.getCore().byId("dashShell").setBusy(false);
 				$("#content").show();
 				$("#Hybris").hide();
 			}
 		}
 	});
 });