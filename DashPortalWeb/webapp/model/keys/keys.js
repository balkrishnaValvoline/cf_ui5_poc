sap.ui.define([
	"valvoline/dash/portal/DashPortalWeb/model/keys/keys_svg",
	"valvoline/dash/portal/DashPortalWeb/model/keys/keys_gtm"
], function (KeySVG, KeyGTM) {
	"use strict";
	return {
		keys_svg: KeySVG,
		keys_gtm: KeyGTM
	};
});