sap.ui.define(function () {
	"use strict";
	return {
		/* My Account Profile */
		keyMyAccP_cardClick_event: "myacntcrdclck",
		keyMyAccP_cardClick_eventCategory: "My Account Page",
		keyMyAccP_myAccountSupplementalRate_eventAction: "Supplemental Rate-Click",
		keyMyAccP_myAccountCases_eventAction: "Support Requests -click",
		keyMyAccP_myAccountGoalSetting_eventAction: "Goal Settings-Click",
		keyMyAccP_myAccountUserManagement_eventAction: "User Management-Click",
		keyMyAccP_myAccountOilAnalysis_eventAction: "Oil Analysis -click",
		keyMyAccP_myAccountEquipment_eventAction: "Equipment-Click",
		keyMyAccP_myAccountSellinData_eventAction: "Miller Pro-Click",
		/* Products */
		keyProducts_cardClick_event: "prodCardEvent",
		keyProducts_cardClick_eventCategory: "Products Page -CardClick",
		keyProducts_cardClick_eventAction: "Card click",
		keyProducts_productFinder_eventLabel: "Product Finder",
		keyProducts_products_eventLabel: "Product Catalog",
		keyProducts_myaccountdrumlabeldetail_eventLabel: "Product Labels",
		keyProducts_productFilterWiperInfo_eventLabel: "Filters and Wipers Information",
		keyProducts_stockingSolution_eventLabel: "Stocking Solution",
		/* Orders */
		keyOrders_cardClick_event: "ordercardclick",
		keyOrders_cardClick_eventCategory: "Orders",
		keyOrders_cardClick_myaccountapprovaldashboard_eventAction: "Approve Order-click",
		keyOrders_cardClick_myaccountdeliverytickets_eventAction: "Delivery Tickets-click",
		keyOrders_cardClick_myaccountinvoicehistory_eventAction: "Invoices-click",
		keyOrders_cardClick_myaccountorderquickorder_eventAction: "Quick Order-click"
	};
});