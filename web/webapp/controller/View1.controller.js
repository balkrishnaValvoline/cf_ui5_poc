sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("poc.dash.web.controller.View1", {
		onInit: function () {

		},

		onPressBtn: function () {
			$.ajax({
				method: "GET",
				"url": "/backend/rfc/test",
				success: function (data) {
					console.log(data);
					alert("Successful");
				},
				error: function (xhr) {
					console.error(xhr);
					alert("failed");
				}
			});
		},

		onPressNW: function () {
			var url = "/northwind/V4/Northwind/Northwind.svc/Customers?$top=10&$format=json";
			$.ajax({
				method: "GET",
				"url": url,
				success: function (data) {
					console.log(data);
					alert("Successful");
				},
				error: function (xhr) {
					console.error(xhr);
					alert("failed");
				}
			});
		},
		
		onLogout:function(){
			sap.m.URLHelper.redirect("/web1/do/logout",false);
		}
	});
});